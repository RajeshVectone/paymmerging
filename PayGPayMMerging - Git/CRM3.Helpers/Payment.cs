﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.IO;
using System.Web;
using NLog;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Collections.Specialized;
using System.Xml;
using CyberSource.Clients.SoapWebReference;
using Payment.Cybersource;
using Newtonsoft.Json;
using CRM_API.Models._3rdParty.CTP;
using System.Data.SqlClient;
using System.Data;
using Dapper;


namespace CRM_API.Helpers
{

    public class Payment
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private string paymentUrl = System.Configuration.ConfigurationManager.AppSettings["PaymentUrl"];

        public string MerchantReferenceCode { get; private set; }
        public string ReasonCode { get; private set; }
        public string Description { get; private set; }
        public string Decision { get; private set; }
        public string AcsURL { get; private set; }
        public string PaReq { get; private set; }
        public string SubscriptionId { get; private set; }

        public void Authorize(Models.Payment.CardDetails cardInfo, string siteCode, string mobileNo, string emailAddress, string currency, double price, string countryCode)
        {
            //Authorize(cardInfo, siteCode, mobileNo, string.Format("2SIMC{0:00}", price), emailAddress, currency, price);

            Authorize(cardInfo, siteCode, mobileNo, string.Format("2SIMC{0:00}", price), emailAddress, currency, price, countryCode);

            //string paymentSiteCode = "MMI";
            //string AppliactionCode = "VMUK";
            //if (siteCode == "MFR")
            //{
            //    AppliactionCode = "VMFR";
            //    paymentSiteCode = "FR2";
            //}
            //else if (siteCode == "MPT")
            //{
            //    AppliactionCode = "VMPT";
            //    paymentSiteCode = "PT2";
            //}
            //else if (siteCode == "MFI")
            //{
            //    AppliactionCode = "VMFI";
            //    paymentSiteCode = "FI1";
            //}
            //var soapRequest = new StringBuilder(string.Format("site-code={0}", paymentSiteCode)).AppendLine();
            //soapRequest.AppendFormat("application-code={0}", AppliactionCode).AppendLine();
            //soapRequest.AppendFormat("product-code=2SIMC{0:00}", price).AppendLine();
            //soapRequest.Append("payment-agent=CRM3").AppendLine();
            //soapRequest.Append("service-type=MVNO").AppendLine();
            //soapRequest.Append("payment-mode=DEF").AppendLine();
            //soapRequest.AppendFormat("account-id={0}", mobileNo).AppendLine();
            //soapRequest.AppendFormat("cc-no={0}", cardInfo.card_number).AppendLine();
            //soapRequest.AppendFormat("cc-cvv={0}", cardInfo.card_verf_code).AppendLine();
            //soapRequest.AppendFormat("cvv={0}", cardInfo.card_verf_code).AppendLine();
            //soapRequest.AppendFormat("exp-date={0}{1}", cardInfo.card_expiry_year, cardInfo.card_expiry_month).AppendLine();
            //soapRequest.AppendFormat("first-name={0}", cardInfo.card_first_name).AppendLine();
            //soapRequest.AppendFormat("last-name={0}", cardInfo.card_last_name).AppendLine();
            //soapRequest.AppendFormat("street-name={0}", cardInfo.card_address).AppendLine();
            //soapRequest.AppendFormat("city={0}", cardInfo.card_city).AppendLine();
            //soapRequest.AppendFormat("post-code={0}", cardInfo.card_post_code).AppendLine();
            //soapRequest.AppendFormat("country={0}", cardInfo.card_country).AppendLine();
            //soapRequest.AppendFormat("email={0}", emailAddress).AppendLine();
            //soapRequest.AppendFormat("amount={0:0.00}", price).AppendLine();
            //soapRequest.AppendFormat("client-ip={0}", HttpContext.Current.Request.UserHostAddress).AppendLine();
            //soapRequest.AppendFormat("currency={0}", currency).AppendLine();
            //soapRequest.Append("check-enroll=0").AppendLine();
            //soapRequest.Append("capture=1").AppendLine();

            //using (HttpClient client = new HttpClient())
            //{
            //    client.BaseAddress = new Uri(paymentUrl);

            //    var content = new StringContent(soapRequest.ToString());
            //    content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");

            //    try
            //    {
            //        Log.Debug(string.Format("Payment.Authorize: Request={0}", soapRequest));

            //        var response = client.PostAsync("/transaction.svc/enrollment/authorize", content).Result;
            //        string responseBody = response.Content.ReadAsStringAsync().Result;
            //        NameValueCollection infoCollection = new NameValueCollection();
            //        if (!string.IsNullOrEmpty(responseBody))
            //        {
            //            string[] data = responseBody.Split("\r\n".ToCharArray());
            //            for (int i = 0; i < data.Length; i++)
            //            {
            //                string[] chunk;
            //                if (i == 0)
            //                {
            //                    chunk = data[i].Split(":".ToCharArray(), 3);
            //                    infoCollection.Add("Description", chunk.Length == 3 ? chunk[2] : "Invalid data");
            //                }
            //                else
            //                {
            //                    chunk = data[i].Split("=:".ToCharArray(), 2);
            //                    if (chunk.Length == 2)
            //                        infoCollection.Add(chunk[0], chunk[1]);
            //                }
            //            }
            //        }

            //        this.MerchantReferenceCode = infoCollection["MerchantReferenceCode"] ?? string.Empty;
            //        this.ReasonCode = infoCollection["ReasonCode"] ?? "-1";
            //        this.Description = infoCollection["Description"] ?? "Internal Error";
            //        this.Decision = (infoCollection["Decision"] ?? "ERROR").ToUpper();
            //        this.AcsURL = infoCollection["AcsURL"] ?? string.Empty;
            //        this.PaReq = infoCollection["PaReq"] ?? string.Empty;
            //        this.SubscriptionId = infoCollection["SubscriptionId"] ?? string.Empty;

            //        Log.Debug(string.Format("Payment.Authorize: Response={0}", responseBody));
            //    }
            //    catch (Exception ex)
            //    {
            //        Log.Error(string.Format("Payment.Authorize:{0}", ex.Message));
            //    }
            //}

        }

        public void AuthorizeWithout3DS(Models.Payment.CardDetails cardInfo, string siteCode, string mobileNo, string productCode, string emailAddress, string currency, double price, string accountId, string subscriptionId, string referenceCode, string countryCode)
        {
            string paymentSiteCode = "MMI";
            string ApplicationCode = "VMUK";
            if (siteCode == "MFR")
            {
                ApplicationCode = "VMFR";
                paymentSiteCode = "FR2";
            }
            else if (siteCode == "MPT")
            {
                ApplicationCode = "VMPT";
                paymentSiteCode = "PT2";
            }
            else if (siteCode == "MFI")
            {
                ApplicationCode = "VMFI";
                paymentSiteCode = "FI1";
            }
            else if (siteCode == "BAU")
            {
                ApplicationCode = "VMAT";
                paymentSiteCode = "AT1";
            }
            else if (siteCode == "BNL")
            {
                ApplicationCode = "VMNL";
                paymentSiteCode = "NL1";
            }

           /* if (siteCode == "BAU")
                siteCode = "AT1";
            else if (siteCode == "BNL")
                siteCode = "NL1";
            else if (siteCode == "MFR")
                siteCode = "FR2";
            */
            //For Realex payment process
            //cardInfo.card_country = "UK";
            if (countryCode.ToLower() == "vmuk")
                cardInfo.card_country = "UK";
            else if (countryCode.ToLower() == "vmat")
                cardInfo.card_country = "AT";
            else if (countryCode.ToLower() == "vmfr")
                cardInfo.card_country = "FR";
            else if (countryCode.ToLower() == "vmnl")
                cardInfo.card_country = "NL";
            else
                cardInfo.card_country = "UK";

            if (cardInfo.card_country.ToLower() == "united kingdom")
                cardInfo.card_country = "UK";
            else if (cardInfo.card_country.ToLower() == "austria")
                cardInfo.card_country = "AT";
            else if (cardInfo.card_country.ToLower() == "france")
                cardInfo.card_country = "FR";
            else if (cardInfo.card_country.ToLower() == "netherlands" || cardInfo.card_country.ToLower() == "netherland")
                cardInfo.card_country = "NL";

            var soapRequest = new StringBuilder(string.Format("site-code={0}", paymentSiteCode)).AppendLine();
            //var soapRequest = new StringBuilder(string.Format("site-code={0}", siteCode)).AppendLine();
            soapRequest.AppendFormat("application-code={0}", ApplicationCode).AppendLine();
            soapRequest.AppendFormat("product-code={0}", productCode).AppendLine();
            soapRequest.Append("payment-agent=CRM3").AppendLine();
            soapRequest.Append("service-type=MVNO").AppendLine();
            soapRequest.Append("payment-mode=DEF").AppendLine();
            soapRequest.AppendFormat("amount={0:0.00}", price).AppendLine();
            soapRequest.AppendFormat("currency={0}", currency).AppendLine();
            soapRequest.AppendFormat("client-ip={0}", HttpContext.Current.Request.UserHostAddress).AppendLine();
            soapRequest.AppendFormat("country={0}", cardInfo.card_country).AppendLine();
            soapRequest.AppendFormat("account-id={0}", accountId).AppendLine();
            soapRequest.AppendFormat("reference-code={0}", referenceCode).AppendLine();
            soapRequest.AppendFormat("subscription_id={0}", subscriptionId).AppendLine();

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(paymentUrl);

                var content = new StringContent(soapRequest.ToString());
                content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");

                try
                {
                    Log.Debug(string.Format("Payment.Authorize: Request={0}", soapRequest));
                    var response = client.PostAsync("/transaction.svc/enrollment/receiptin", content).Result;
                    string responseBody = response.Content.ReadAsStringAsync().Result;
                    NameValueCollection infoCollection = new NameValueCollection();

                    if (!string.IsNullOrEmpty(responseBody))
                    {
                        string[] data = responseBody.Split("\r\n".ToCharArray());
                        for (int i = 0; i < data.Length; i++)
                        {
                            string[] chunk;
                            if (i == 0)
                            {
                                chunk = data[i].Split(":".ToCharArray(), 3);
                                infoCollection.Add("Description", chunk.Length == 3 ? chunk[2] : "Invalid data");
                            }
                            else
                            {
                                chunk = data[i].Split("=:".ToCharArray(), 2);
                                if (chunk.Length == 2)
                                    infoCollection.Add(chunk[0], chunk[1]);
                            }
                        }
                    }

                    this.MerchantReferenceCode = infoCollection["MerchantReferenceCode"] ?? string.Empty;
                    this.ReasonCode = infoCollection["ReasonCode"] ?? "-1";
                    this.Description = infoCollection["Description"] ?? "Internal Error";
                    this.Decision = (infoCollection["Decision"] ?? "ERROR").ToUpper();
                    this.AcsURL = infoCollection["AcsURL"] ?? string.Empty;
                    this.PaReq = infoCollection["PaReq"] ?? string.Empty;
                    this.SubscriptionId = infoCollection["SubscriptionId"] ?? string.Empty;

                    Log.Debug(string.Format("Payment.Authorize: Response={0}", responseBody));
                }
                catch (Exception ex)
                {
                    Log.Error(string.Format("Payment.Authorize:{0}", ex.Message));
                    this.Decision = "ERROR";
                    this.Description = ex.Message;
                }
            }
        }

        public void Authorize(Models.Payment.CardDetails cardInfo, string siteCode, string mobileNo, string productCode, string emailAddress, string currency, double price, string countryCode)
        {
            string paymentSiteCode = "MMI";
            string ApplicationCode = "VMUK";
            if (siteCode == "MFR")
            {
                ApplicationCode = "VMFR";
                paymentSiteCode = "FR2";
            }
            else if (siteCode == "MPT")
            {
                ApplicationCode = "VMPT";
                paymentSiteCode = "PT2";
            }
            else if (siteCode == "MFI")
            {
                ApplicationCode = "VMFI";
                paymentSiteCode = "FI1";
            }
            else if (siteCode == "BAU")
            {
                ApplicationCode = "VMAT";
                paymentSiteCode = "AT1";
            }
            else if (siteCode == "BNL")
            { 
                ApplicationCode = "VMNL";
                paymentSiteCode = "NL1";
            }

            if (siteCode == "BAU")
                siteCode = "AT1";
            else if (siteCode == "BNL")
                siteCode = "NL1";
            else if (siteCode == "MFR")
                siteCode = "FR2";

            //For Realex payment process
            //cardInfo.card_country = "UK";
            if (cardInfo.card_country.ToLower() == "united kingdom")
                cardInfo.card_country = "UK";
            else if (cardInfo.card_country.ToLower() == "austria")
                cardInfo.card_country = "AT";
            else if (cardInfo.card_country.ToLower() == "france")
                cardInfo.card_country = "FR";
            else if (cardInfo.card_country.ToLower() == "netherlands" || cardInfo.card_country.ToLower() == "netherland")
                cardInfo.card_country = "NL";

            //var soapRequest = new StringBuilder(string.Format("site-code={0}", paymentSiteCode)).AppendLine();
            var soapRequest = new StringBuilder(string.Format("site-code={0}", siteCode)).AppendLine();
            soapRequest.AppendFormat("application-code={0}", ApplicationCode).AppendLine();
            soapRequest.AppendFormat("product-code={0}", productCode).AppendLine();
            soapRequest.Append("payment-agent=CRM3").AppendLine();
            soapRequest.Append("service-type=MVNO").AppendLine();
            soapRequest.Append("payment-mode=DEF").AppendLine();
            soapRequest.AppendFormat("account-id={0}", mobileNo).AppendLine();
            soapRequest.AppendFormat("cc-no={0}", cardInfo.card_number).AppendLine();
            soapRequest.AppendFormat("cc-cvv={0}", cardInfo.card_verf_code).AppendLine();
            soapRequest.AppendFormat("cvv={0}", cardInfo.card_verf_code).AppendLine();
            soapRequest.AppendFormat("exp-date={0}{1}", cardInfo.card_expiry_year, cardInfo.card_expiry_month).AppendLine();
            soapRequest.AppendFormat("first-name={0}", cardInfo.card_first_name).AppendLine();
            soapRequest.AppendFormat("last-name={0}", cardInfo.card_last_name).AppendLine();
            soapRequest.AppendFormat("street-name={0}", cardInfo.card_address).AppendLine();
            soapRequest.AppendFormat("city={0}", cardInfo.card_city).AppendLine();
            soapRequest.AppendFormat("post-code={0}", cardInfo.card_post_code).AppendLine();
            soapRequest.AppendFormat("country={0}", cardInfo.card_country).AppendLine();
            soapRequest.AppendFormat("email={0}", emailAddress).AppendLine();
            soapRequest.AppendFormat("amount={0:0.00}", price).AppendLine();
            soapRequest.AppendFormat("client-ip={0}", HttpContext.Current.Request.UserHostAddress).AppendLine();
            soapRequest.AppendFormat("currency={0}", currency).AppendLine();
            soapRequest.AppendFormat("check-enroll=0").AppendLine();
            soapRequest.AppendFormat("capture=1").AppendLine();
            soapRequest.AppendFormat("cc-type={0}", cardInfo.card_type).AppendLine();

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(paymentUrl);

                var content = new StringContent(soapRequest.ToString());
                content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");

                try
                {
                    Log.Debug(string.Format("Payment.Authorize: Request={0}", soapRequest));

                    var response = client.PostAsync("/transaction.svc/enrollment/authorize", content).Result;
                    string responseBody = response.Content.ReadAsStringAsync().Result;
                    NameValueCollection infoCollection = new NameValueCollection();
                    if (!string.IsNullOrEmpty(responseBody))
                    {
                        string[] data = responseBody.Split("\r\n".ToCharArray());
                        for (int i = 0; i < data.Length; i++)
                        {
                            string[] chunk;
                            if (i == 0)
                            {
                                chunk = data[i].Split(":".ToCharArray(), 3);
                                infoCollection.Add("Description", chunk.Length == 3 ? chunk[2] : "Invalid data");
                            }
                            else
                            {
                                chunk = data[i].Split("=:".ToCharArray(), 2);
                                if (chunk.Length == 2)
                                    infoCollection.Add(chunk[0], chunk[1]);
                            }
                        }
                    }

                    this.MerchantReferenceCode = infoCollection["MerchantReferenceCode"] ?? string.Empty;
                    this.ReasonCode = infoCollection["ReasonCode"] ?? "-1";
                    this.Description = infoCollection["Description"] ?? "Internal Error";
                    this.Decision = (infoCollection["Decision"] ?? "ERROR").ToUpper();
                    this.AcsURL = infoCollection["AcsURL"] ?? string.Empty;
                    this.PaReq = infoCollection["PaReq"] ?? string.Empty;
                    this.SubscriptionId = infoCollection["SubscriptionId"] ?? string.Empty;

                    Log.Debug(string.Format("Payment.Authorize: Response={0}", responseBody));
                }
                catch (Exception ex)
                {
                    Log.Error(string.Format("Payment.Authorize:{0}", ex.Message));
                    this.Decision = "ERROR";
                    this.Description = ex.Message;
                }
            }

        }

        /// <summary>
        /// Charge customer using subscription
        /// </summary>
        /// <param name="mobileNo"></param>
        /// <param name="card_verf_code"></param>
        /// <param name="price"></param>
        public void SubscriptionCharge(string mobileNo, string card_verf_code, string currency, double price)
        {
            var soapRequest = new StringBuilder("?site-code=MMI");
            soapRequest.Append("&application-code=MMI");
            soapRequest.AppendFormat("&product-code=AUT{0}", price);
            soapRequest.Append("&payment-agent=CRM3");
            soapRequest.Append("&service-type=MVNO");
            soapRequest.Append("&payment-mode=SUB");
            soapRequest.AppendFormat("&account-id={0}", mobileNo);
            if (string.IsNullOrEmpty(card_verf_code))
                soapRequest.AppendFormat("&cvv={0}", "NULL");
            else
                soapRequest.AppendFormat("&cvv={0}", card_verf_code);
            soapRequest.AppendFormat("&amount={0:0.00}", price);
            soapRequest.AppendFormat("&currency={0}", currency);
            if (HttpContext.Current != null)
                soapRequest.AppendFormat("&client-ip={0}", HttpContext.Current.Request.UserHostAddress);

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(paymentUrl);

                try
                {
                    Log.Debug(string.Format("Payment.SubscriptionCharge: Request={0}", soapRequest.ToString()));

                    var response = client.GetAsync("payment.svc/subscription/payment" + soapRequest.ToString()).Result;
                    string responseBody = response.Content.ReadAsStringAsync().Result;

                    // Create an XmlReader
                    using (XmlReader reader = XmlReader.Create(new StringReader(responseBody)))
                    {
                        reader.ReadToFollowing("MerchantReferenceCode");
                        this.MerchantReferenceCode = reader.ReadElementContentAsString() ?? string.Empty;

                        reader.ReadToFollowing("GeneralErrorCode");
                        this.ReasonCode = reader.ReadElementContentAsString() ?? "-1";

                        reader.ReadToFollowing("Description");
                        this.Description = reader.ReadElementContentAsString() ?? "Internal Error";

                        reader.ReadToFollowing("SubscriptionId");
                        this.SubscriptionId = reader.ReadElementContentAsString() ?? string.Empty;

                        reader.ReadToFollowing("Decision");
                        this.Description = reader.ReadElementContentAsString() ?? "ERROR";

                        reader.ReadToFollowing("AcsURL");
                        this.AcsURL = reader.ReadElementContentAsString() ?? string.Empty;

                        reader.ReadToFollowing("PaReq");
                        this.PaReq = reader.ReadElementContentAsString() ?? string.Empty;
                    }

                    Log.Debug(string.Format("Payment.SubscriptionCharge: Response={0}", responseBody));
                }
                catch (Exception ex)
                {
                    Log.Error(string.Format("Payment.SubscriptionCharge:{0}", ex.Message));
                }
            }

        }

        /// <summary>
        /// Subscribe the card information into cybersource information
        /// Currently defaulted to GBP and uk01login
        /// </summary>
        /// <param name="cardInfo"></param>
        /// <param name="mobileNo"></param>
        /// <param name="emailAddress"></param>
        /// <param name="clientIP"></param>
        public void CybersourceSubscribe(Models.Payment.CardDetails cardInfo, string mobileNo, string emailAddress, string clientIP)
        {
            try
            {
                string merchantid = "uk01login";
                string currency = "GBP";

                // Card info
                Card card = new Card();
                card.accountNumber = cardInfo.card_number;
                card.cvNumber = cardInfo.card_verf_code;
                card.cvIndicator = "1";
                card.expirationYear = cardInfo.card_expiry_year;
                card.expirationMonth = cardInfo.card_expiry_month;

                // Billing info
                BillTo billTo = new BillTo();
                billTo.firstName = cardInfo.card_first_name;
                billTo.lastName = cardInfo.card_last_name;
                billTo.street1 = cardInfo.card_address;
                billTo.city = cardInfo.card_city;
                billTo.postalCode = cardInfo.card_post_code;
                billTo.state = string.Empty;
                billTo.country = cardInfo.card_country;
                billTo.email = emailAddress;
                billTo.ipAddress = string.IsNullOrEmpty(clientIP) ? HttpContext.Current.Request.UserHostAddress : clientIP;
                billTo.customerID = mobileNo;
                
                // Run transaction
                SOAP soap = new SOAP();
                //result = soap.Enrollment_Authorize(
                //    referenceId,
                //    accountId,
                //    card,
                //    billTo,
                //    payment,
                //    checkEnroll.Equals("1"),
                //    BusinessFacade.DecisionManager,
                //    routing.MerchantID,
                //    capture == "1");
            }
            catch (Exception ex)
            {
                Log.Debug(string.Format("Payment.CybersourceSubscribe: Error {0}", ex.Message));
            }
        }

        /// <summary>
        /// Charge directly to cybersource
        /// Currently defaulted to GBP and uk01login
        /// </summary>
        /// <param name="merchantField"></param>
        /// <param name="mobileNo"></param>
        /// <param name="subscriptionID"></param>
        /// <param name="currency"></param>
        /// <param name="price"></param>
        //public void CybersourceCharge(string merchantField, string mobileNo, string subscriptionID, string currency, double price)
        //{
        //    try
        //    {
        //        string merchantid = "uk01login";
        //        if (mobileNo.StartsWith("33")) // France
        //            merchantid = "vmch_chf";

        //        string productcode = string.Format("AUT{0}", price * 100);
        //        MerchantReferenceCode = string.Format("MMI-{0}-{1:yyMMddHHmm}", productcode, DateTime.Now);

        //        // Charge it through Cybersource
        //        string errDesc = "";
        //        SOAP soapreq = new SOAP();
        //        ReplyMessage rep = null;
        //        MerchantDefinedData _MerchantDefinedData;
        //        PurchaseTotals _PurchaseTotals;

        //        _MerchantDefinedData = new MerchantDefinedData() { field1 = merchantField };
        //        string amountPaid = price.ToString("N2");
        //        _PurchaseTotals = new PurchaseTotals() { currency = currency, grandTotalAmount = amountPaid };
        //        rep = soapreq.Subscription_Authorize(MerchantReferenceCode, merchantid, subscriptionID, _MerchantDefinedData, _PurchaseTotals, false, ref errDesc);

        //        if (rep.decision.Equals("ACCEPT"))
        //        {
        //            rep = soapreq.Subscription_Capture(MerchantReferenceCode, merchantid, rep.requestID, rep.requestToken, _MerchantDefinedData, _PurchaseTotals, false, ref errDesc);
        //            Log.Debug(string.Format("Payment.CybersourceCharge: Payment Ref={0}; Response={1}", MerchantReferenceCode, rep.decision));

        //            this.ReasonCode = rep.reasonCode;
        //            this.Description = "";
        //            this.Decision = rep.decision;
        //            this.AcsURL = "";
        //            this.PaReq = rep.requestToken;
        //            this.SubscriptionId = subscriptionID;

        //            if (rep.decision.Equals("ACCEPT"))
        //            {
        //                SOAPLog.TransactionPayment("MMI", productcode, "CRM3", "MVNO", "SUB", 7, 3, mobileNo, string.Empty,
        //                    price, currency, subscriptionID, merchantid, "CS", ReasonCode, "100");
        //            }
        //            else
        //            {
        //                SOAPLog.TransactionPayment("MMI", productcode, "CRM3", "MVNO", "SUB", 7, -1, mobileNo, string.Empty,
        //                    price, currency, subscriptionID, merchantid, "CS", ReasonCode, rep.decision);
        //            }
        //        }
        //        else
        //        {
        //            this.Decision = rep.decision;
        //            Log.Debug(string.Format("Payment.CybersourceCharge: Payment Ref={0}; Response={1}", MerchantReferenceCode, rep.decision));
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.Debug(string.Format("Payment.CybersourceCharge: Error {0}", ex.Message));
        //        this.Decision = "ERROR";
        //        this.Description = ex.Message;
        //    }
        //}
        
        /// <summary>
        /// Charge directly to cybersource
        /// </summary>
        /// <param name="merchantField"></param>
        /// <param name="mobileNo"></param>
        /// <param name="sitecode"></param>
        /// <param name="applicationcode"></param>
        /// <param name="productcode"></param>
        /// <param name="subscriptionID"></param>
        /// <param name="currency"></param>
        /// <param name="price"></param>
        //public void CybersourceCharge(string merchantField, string mobileNo, string sitecode, string applicationcode, string productcode, string subscriptionID, string currency, double price)
        //{
        //    try
        //    {
        //        string merchantid = "uk01login";
        //        if (mobileNo.StartsWith("33")) // France
        //            merchantid = "vmch_chf";

        //        MerchantReferenceCode = string.Format("{0}-{1}-{2:yyMMddHHmm}", applicationcode, productcode, DateTime.Now);

        //        // Charge it through Cybersource
        //        string errDesc = "";
        //        SOAP soapreq = new SOAP();
        //        ReplyMessage rep = null;
        //        MerchantDefinedData _MerchantDefinedData;
        //        PurchaseTotals _PurchaseTotals;

        //        _MerchantDefinedData = new MerchantDefinedData() { field1 = merchantField };
        //        string amountPaid = price.ToString("N2");
        //        _PurchaseTotals = new PurchaseTotals() { currency = currency, grandTotalAmount = amountPaid };
        //        rep = soapreq.Subscription_Authorize(merchantField, merchantid, subscriptionID, _MerchantDefinedData, _PurchaseTotals, false, ref errDesc);

        //        if (rep.decision.Equals("ACCEPT"))
        //        {
        //            rep = soapreq.Subscription_Capture(merchantField, merchantid, rep.requestID, rep.requestToken, _MerchantDefinedData, _PurchaseTotals, false, ref errDesc);
        //            Log.Debug(string.Format("Payment.CybersourceCharge: Payment Ref={0}; Response={1}", MerchantReferenceCode, rep.decision));

        //            this.ReasonCode = rep.reasonCode;
        //            this.Description = "";
        //            this.Decision = rep.decision;
        //            this.AcsURL = "";
        //            this.PaReq = rep.requestToken;
        //            this.SubscriptionId = subscriptionID;

        //            if (rep.decision.Equals("ACCEPT"))
        //            {
        //                if (merchantField.Contains("DDPPCC"))
        //                {
        //                    TransactionPayment_v1(sitecode, productcode, "CRM3", "MVNO", "SUB", 7, 1, mobileNo, string.Empty,
        //                        price, currency, subscriptionID, merchantid, "CS", ReasonCode, merchantField, "100");
                            
        //                }
        //                else
        //                {
        //                    TransactionPayment_v1(sitecode, productcode, "CRM3", "MVNO", "SUB", 7, 1, mobileNo, string.Empty,
        //                      price, currency, subscriptionID, merchantid, "CS", ReasonCode, merchantField, "100");
        //                    //Modify payment data mismatch 

        //                    //TransactionPayment(sitecode, productcode, "CRM3", "MVNO", "SUB", 7, 1, mobileNo, string.Empty,
        //                    //    price, currency, subscriptionID, merchantid, "CS", ReasonCode, "100");
        //                }

        //            }
        //            else
        //            {
        //                //TransactionPayment(sitecode, productcode, "CRM3", "MVNO", "SUB", 7, 0, mobileNo, string.Empty,
        //                //    price, currency, subscriptionID, merchantid, "CS", ReasonCode, rep.decision);
        //                TransactionPayment_v1(sitecode, productcode, "CRM3", "MVNO", "SUB", 7, 0, mobileNo, string.Empty,
        //                      price, currency, subscriptionID, merchantid, "CS", ReasonCode, merchantField, "100");
        //            }
        //        }
        //        else
        //        {
        //            this.Decision = rep.decision;
        //            Log.Debug(string.Format("Payment.CybersourceCharge: Payment Ref={0}; Response={1}", MerchantReferenceCode, rep.decision));
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.Debug(string.Format("Payment.CybersourceCharge: Error {0}", ex.Message));
        //        this.Decision = "ERROR";
        //        this.Description = ex.Message;
        //    }
        //}


        public static int TransactionPayment(string sitecode, string productcode, string paymentagent, string servicetype,
     string paymentmode, int paymentstep, int paymentstatus, string accountid,
     string last6digitccno, double totalamount, string currency,
     string subscriptionid, string merchantid, string providercode, string csreasoncode, string generalerrorcode)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.PaymentConnection))
                {
                    conn.Open();

                    var result = conn.Query<int>(
                            "csPaymentInsert", new
                            {
                                @sitecode = sitecode,
                                @productcode = productcode,
                                @paymentagent = paymentagent,
                                @servicetype = servicetype,
                                @paymentmode = paymentagent,
                                @paymentstep = paymentstep,
                                @paymentstatus = paymentstatus,
                                @accountid = accountid,
                                @last6digitccno = last6digitccno,
                                @totalamount = totalamount,
                                @currency = currency,
                                @subscriptionid = subscriptionid,
                                @merchantid = merchantid,
                                @providercode = providercode,
                                @csreasoncode = csreasoncode,
                                @generalerrorcode = generalerrorcode
                            }, commandType: CommandType.StoredProcedure);

                    return result.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }


        public  int TransactionPayment_v1(string sitecode, string productcode, string paymentagent, string servicetype,
     string paymentmode, int paymentstep, int paymentstatus, string accountid,
     string last6digitccno, double totalamount, string currency,
     string subscriptionid, string merchantid, string providercode, string csreasoncode,string refid, string generalerrorcode)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.PaymentConnection))
                {
                    conn.Open();

                    var result = conn.Query<int>(
                            "csPaymentInsert_v1", new
                            {
                                @sitecode = sitecode,
                                @productcode = productcode,
                                @paymentagent = paymentagent,
                                @servicetype = servicetype,
                                @paymentmode = paymentmode,
                                @paymentstep = paymentstep,
                                @paymentstatus = paymentstatus,
                                @accountid = accountid,
                                @last6digitccno = last6digitccno,
                                @totalamount = totalamount,
                                @currency = currency,
                                @subscriptionid = subscriptionid,
                                @merchantid = merchantid,
                                @providercode = providercode,
                                @csreasoncode = csreasoncode,
                                @reference_id=refid,
                                @generalerrorcode = generalerrorcode
                            }, commandType: CommandType.StoredProcedure);

                    return result.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public dynamic DoTopup(string AccountId, string PaymentReference, string Sitecode)
        {
            return DoTopup_CTP(AccountId, PaymentReference);
        }

        private static dynamic DoTopup_CTP(string AccountId, string PaymentReference)
        {
            string url = "https://sws.vectone.com";
            TopupResponse result = new TopupResponse();
            CRM_API.Models._3rdParty.CTP.Payment payment = new Models._3rdParty.CTP.Payment() { AccId = AccountId, MerchantReferenceCode = PaymentReference, step = 1 };
            HttpClient client = new HttpClient();
            HttpContent content = new StringContent(JsonConvert.SerializeObject(payment));
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            try
            {
                HttpResponseMessage response = client.PostAsync("api/CTACCdirectTopup", content).Result;
                if (response.IsSuccessStatusCode)
                {
                    result = JsonConvert.DeserializeObject<TopupResponse>(response.Content.ReadAsStringAsync().Result);
                }
                else
                    throw new Exception(response.ReasonPhrase);
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Payment.DoTopup_CTP: Error {0}", e.Message));
                throw new Exception("Payment.DoTopup_CTP: Error " + e.Message);
            }
            return result;
        }
    }
}