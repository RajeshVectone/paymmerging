﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CRM_API.Helpers
{
    public static class ConvertToDataTable
    {
        public static DataTable ToDataTable<T>(this IList<T> data)
        {
            PropertyDescriptorCollection properties =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }
    }
    public class ConvertToExcel
    {
        private int _PageSize = 0;
        private int _PageIndex = 0;
        private string _sheetname = null;
        public int PageSize
        {
            get
            {
                if (_PageSize == 0)
                {
                    _PageSize = 20;
                }
                return _PageSize;
            }
            set
            {
                _PageSize = value;
            }
        }
        public int PageIndex
        {
            get
            {
                return _PageIndex;
            }
            set
            {
                _PageIndex = value;
            }
        }
        public string sheetname
        {
            get
            {
                if (string.IsNullOrEmpty(_sheetname))
                {
                    _sheetname = "Excel Report";
                }
                return _sheetname;
            }
            set
            {
                _sheetname = value;
            }
        }
        public void convertDs(DataSet ds, string excfilename)
        {
            HttpResponse response = HttpContext.Current.Response;
            response.Clear();
            response.Charset = "";
            response.ContentType = "application/vnd.ms-excel";
            response.AddHeader("Content-Disposition", "attachment; filename=" + '"' + excfilename + '"');
            response.Write("<HTML xmlns:x=" + '"' + "urn:schemas-microsoft-com:office:excel" + '"' + ">");
            response.Write("<HEAD>");
            response.Write(@"<!--[if gte mso 9]><xml>
               <x:ExcelWorkbook>
                <x:ExcelWorksheets>
                 <x:ExcelWorksheet>
                  <x:Name>" + this.sheetname + @"</x:Name>
                  <x:WorksheetOptions>
                   <x:Print>
                    <x:ValidPrinterInfo/>
                   </x:Print>
                  </x:WorksheetOptions>
                 </x:ExcelWorksheet>
                </x:ExcelWorksheets>
               </x:ExcelWorkbook>
              </xml><![endif]-->");
            response.Write("</HEAD>");
            response.Write("<BODY>");
            StringWriter stringWrite = new StringWriter();
            HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            GridView gv = new GridView();
            gv.DataSource = ds.Tables[0].DefaultView;
            gv.DataBind();
            gv.RenderControl(htmlWrite);
            response.Write(stringWrite.ToString());
            response.Write("</BODY>");
            response.Write("</HTML>");
            response.End();
        }
        public void convertDv(DataView dv, string excfilename)
        {
            HttpResponse response = HttpContext.Current.Response;
            response.Clear();
            response.Charset = "";
            response.ContentType = "application/vnd.ms-excel";
            response.AddHeader("Content-Disposition", "attachment; filename=" + '"' + excfilename + '"');
            response.Write("<HTML xmlns:x=" + '"' + "urn:schemas-microsoft-com:office:excel" + '"' + ">");
            response.Write("<HEAD>");
            response.Write(@"<!--[if gte mso 9]><xml>
               <x:ExcelWorkbook>
                <x:ExcelWorksheets>
                 <x:ExcelWorksheet>
                  <x:Name>" + this.sheetname + @"</x:Name>
                  <x:WorksheetOptions>
                   <x:Print>
                    <x:ValidPrinterInfo/>
                   </x:Print>
                  </x:WorksheetOptions>
                 </x:ExcelWorksheet>
                </x:ExcelWorksheets>
               </x:ExcelWorkbook>
              </xml><![endif]-->");
            response.Write("</HEAD>");
            response.Write("<BODY>");
            StringWriter stringWrite = new StringWriter();
            HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            GridView gv = new GridView();
            gv.ID = "freesim";
            gv.DataSource = dv;
            gv.DataBind();
            gv.RenderControl(htmlWrite);
            response.Write(stringWrite.ToString());
            response.Write("</BODY>");
            response.Write("</HTML>");
            response.End();
        }

        public void convertDt(DataTable dt, string excfilename)
        {
            HttpResponse response = HttpContext.Current.Response;
            response.Clear();
            response.Charset = "";
            response.ContentType = "application/vnd.ms-excel";

            response.ContentEncoding = Encoding.UTF8;


            response.AddHeader("Content-Disposition", "attachment; filename=" + '"' + excfilename + '"');
            response.Write("<HTML xmlns:x=" + '"' + "urn:schemas-microsoft-com:office:excel" + '"' + ">");
            response.Write("<HEAD>");
            //response.Write(@"<style>.mystyle1 {mso-number-format:0\.00; } </style>");
            response.Write(@"<style>.mystyle1 {mso-number-format:0; } </style>");
            //response.Write("<style>  .mystyle1 " + "\r\n" + "{mso-style-parent:style0;mso-number-format:\"" + @"\@" + "\"" + ";} "
            //+ "\r\n" + "</style>");
            response.Write(@"<!--[if gte mso 9]><xml>
               <x:ExcelWorkbook>
                <x:ExcelWorksheets>
                 <x:ExcelWorksheet>
                  <x:Name>" + this.sheetname + @"</x:Name>
                  <x:WorksheetOptions>
                   <x:Print>
                    <x:ValidPrinterInfo/>
                   </x:Print>
                  </x:WorksheetOptions>
                 </x:ExcelWorksheet>
                </x:ExcelWorksheets>
               </x:ExcelWorkbook>
              </xml><![endif]-->");
            response.Write("</HEAD>");
            response.Write("<BODY>");
            //response.Write("<style>  .mystyle1 " + "\r\n" + "{mso-style-parent:style0;mso-number-format:\""+@"\@"+"\""+";} " 
            //    + "\r\n" +"</style>");
            //response.Write(@"<style>.mystyle1 {mso-number-format:0\.00; } </style>");
            response.Write(@"<style>.mystyle1 {mso-number-format:0; } </style>");
            StringWriter stringWrite = new StringWriter();
            HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            /*
            for (int col = 0; col < dt.Columns.Count; col ++ )
            {
                for(int row = 0; row < dt.Rows.Count; row ++)
                {                                        
                    dt.Rows[row][col] = " " + dt.Rows[row][col] + " ";
                }
            }
            */
            GridView gv = new GridView();

            /*
            //gv.Columns.Clear();
            DataTable dt1 = new DataTable();
            */

            //gv.AutoGenerateColumns = false;
            gv.DataSource = dt.DefaultView;//ds.Tables[0].DefaultView;
            /*
            foreach (DataColumn dcc in dt.Columns)
            {
                BoundField someField = new BoundField();                
                someField.DataField = dcc.Caption;
                someField.HeaderText = dcc.Caption;
                if (dcc.DataType.FullName.Equals("System.Decimal"))
                {
                    someField.DataFormatString = "{0:#,0.00}"; //"{0:c}";
                    someField.HtmlEncode = false; someField.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
                }
                else if (dcc.DataType.FullName.Equals("System.DateTime"))
                {
                    someField.DataFormatString = "{0:dd/MM/yyyy}"; //"{0:c}";
                    someField.HtmlEncode = false; someField.ItemStyle.HorizontalAlign = HorizontalAlign.Left;
                }
                else if (dcc.DataType.FullName.Equals("System.String"))
                {
                    someField.DataFormatString = " {0:d}";//" {0:G}"; //"{0:c}";
                    someField.HtmlEncode = false; someField.ItemStyle.HorizontalAlign = HorizontalAlign.Left;
                }
                else
                {
                    //someField.DataFormatString = "{0:c}"; //"{0:c}";
                    someField.HtmlEncode = false;
                }
                //dt.Columns.Add(someField);
                gv.Columns.Add(someField);
            }
            
            /*
            for (int i = 0; i < gv.Rows.Count - 1;i++ )
            {
                gv.Rows[i].Attributes.Add("class", "text");
                gv.Rows[i].Cells[i].Text                
            }
            */
            gv.DataBind();
            for (int i = 0; i < gv.Rows.Count - 1; i++)
            {
                for (int a = 0; a < dt.Columns.Count; a++)
                {
                    if (dt.Columns[a].DataType.FullName.Equals("System.String"))
                        gv.Rows[i].Cells[a].Attributes.Add("class", "mystyle1");
                }
            }
            gv.RenderControl(htmlWrite);
            //string style = @"<style> .textmode { mso-number-format:\@; } </style>";
            //response.Write(style);
            response.Write(stringWrite.ToString());
            response.Write("</BODY>");
            response.Write("</HTML>");
            response.End();
        }
    }
}
