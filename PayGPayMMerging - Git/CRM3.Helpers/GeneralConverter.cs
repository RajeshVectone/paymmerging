﻿using System;
using System.Data;
using System.Configuration;

namespace CRM_API.Helpers
{


    /// <summary>
    /// General Convertion for returning object from database
    /// </summary>
    public static class GeneralConverter
    {
        /// <summary>
        /// Convert object into bool and return false if the object is DBNull
        /// </summary>
        /// <param name="oBool"></param>
        /// <returns></returns>
        public static bool ToBoolean(object oBool)
        {
            return ToBoolean(oBool, false);
        }

        /// <summary>
        /// Convert object into bool and return defValue if the object is DBNull
        /// </summary>
        /// <param name="oBool"></param>
        /// <param name="defValue"></param>
        /// <returns></returns>
        public static bool ToBoolean(object oBool, bool defValue)
        {
            return Convert.IsDBNull(oBool) ? defValue : Convert.ToBoolean(oBool);
        }

        /// <summary>
        /// Convert object into Int32 and return 0 if the object is DBNull
        /// </summary>
        /// <param name="oInt32"></param>
        /// <returns></returns>
        public static Int32 ToInt32(object oInt32)
        {
            return ToInt32(oInt32, 0);
        }

        /// <summary>
        /// Convert object into Int32 and return defValue if the object is DBNull
        /// </summary>
        /// <param name="oInt32"></param>
        /// <param name="defValue"></param>
        /// <returns></returns>
        public static Int32 ToInt32(object oInt32, Int32 defValue)
        {
            return Convert.IsDBNull(oInt32) ? defValue : Convert.ToInt32(oInt32);
        }

        /// <summary>
        /// Convert object into long and return 0 if the object is DBNull
        /// </summary>
        /// <param name="oInt32"></param>
        /// <returns></returns>
        public static long ToLong(object oLong)
        {
            return ToLong(oLong, 0);
        }

        /// <summary>
        /// Convert object into long and return defValue if the object is DBNull
        /// </summary>
        /// <param name="oInt32"></param>
        /// <param name="defValue"></param>
        /// <returns></returns>
        public static long ToLong(object oLong, long defValue)
        {
            return Convert.IsDBNull(oLong) ? defValue : Convert.ToInt64(oLong);
        }

        /// <summary>
        /// Convert object into DateTime and return DateTime MinValue if the object is DBNull
        /// </summary>
        /// <param name="oDateTime"></param>
        /// <returns></returns>
        public static DateTime ToDateTime(object oDateTime)
        {
            return ToDateTime(oDateTime, DateTime.MinValue);
        }

        /// <summary>
        /// Convert object into DateTime and return Sql DateTime MinValue if the object is DBNull
        /// </summary>
        /// <param name="oDateTime"></param>
        /// <returns></returns>
        public static DateTime ToSqlDateTime(object oDateTime)
        {
            return ToDateTime(oDateTime, System.Data.SqlTypes.SqlDateTime.MinValue.Value);
        }

        /// <summary>
        /// Convert object into DateTime and return defValue if the object is DBNull
        /// </summary>
        /// <param name="oDateTime"></param>
        /// <param name="defValue"></param>
        /// <returns></returns>
        public static DateTime ToDateTime(object oDateTime, DateTime defValue)
        {
            return Convert.IsDBNull(oDateTime) ? defValue : Convert.ToDateTime(oDateTime);
        }

        /// <summary>
        /// Convert object into Single and return 0 if the object is DBNull
        /// </summary>
        /// <param name="oSingle"></param>
        /// <returns></returns>
        public static Single ToSingle(object oSingle)
        {
            return ToSingle(oSingle, 0F);
        }

        /// <summary>
        /// Convert object into Single and return defValue if the object is DBNull
        /// </summary>
        /// <param name="oSingle"></param>
        /// <param name="defValue"></param>
        /// <returns></returns>
        public static Single ToSingle(object oSingle, Single defValue)
        {
            return Convert.IsDBNull(oSingle) ? defValue : Convert.ToSingle(oSingle);
        }

        /// <summary>
        /// Convert object into Double and return 0 if the object is DBNull
        /// </summary>
        /// <param name="oDouble"></param>
        /// <returns></returns>
        public static Double ToDouble(object oDouble)
        {
            return ToDouble(oDouble, 0);
        }

        /// <summary>
        /// Convert object into Double and return defValue if the object is DBNull
        /// </summary>
        /// <param name="oDouble"></param>
        /// <param name="defValue"></param>
        /// <returns></returns>
        public static Double ToDouble(object oDouble, Double defValue)
        {
            return Convert.IsDBNull(oDouble) ? defValue : Convert.ToDouble(oDouble);
        }

        /// <summary>
        /// Convert object into String and return "" if the object is DBNull
        /// </summary>
        /// <param name="oString"></param>
        /// <returns></returns>
        public static String ToString(object oString)
        {
            return ToString(oString, "");
        }

        /// <summary>
        /// Convert object into String and return defValue if the object is DBNull
        /// </summary>
        /// <param name="oString"></param>
        /// <param name="defValue"></param>
        /// <returns></returns>
        public static String ToString(object oString, String defValue)
        {
            return Convert.IsDBNull(oString) ? defValue : Convert.ToString(oString);
        }
    }
}
