﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.IO;
using NLog;
using System.Text;

namespace CRM_API.Helpers
{
    public class cancelLocation
    {
        public static readonly Logger Log = LogManager.GetCurrentClassLogger();
        

        public static string[] CancelLocations(CRM_API.Models.CancelLocation  objCancelLocation)
        {
            string Uri_CancelLocationUpdate = "spanwntask hlr_cancel_loc";
            string cancelSite = "";
            if (objCancelLocation.siteCode == "MCM")
                cancelSite = System.Configuration.ConfigurationManager.AppSettings["UKCancel"];
            else if (objCancelLocation.siteCode == "BAU")
                cancelSite = System.Configuration.ConfigurationManager.AppSettings["ATCancel"];
            else if (objCancelLocation.siteCode == "MBE")
                cancelSite = System.Configuration.ConfigurationManager.AppSettings["BECancel"];
            else if (objCancelLocation.siteCode == "BDK")
                cancelSite = System.Configuration.ConfigurationManager.AppSettings["DKCancel"];
            else if (objCancelLocation.siteCode == "MFI")
                cancelSite = System.Configuration.ConfigurationManager.AppSettings["FICancel"];
            else if (objCancelLocation.siteCode == "MFR")
                cancelSite = System.Configuration.ConfigurationManager.AppSettings["FRCancel"];
            else if (objCancelLocation.siteCode == "BNL")
                cancelSite = System.Configuration.ConfigurationManager.AppSettings["NLCancel"];
            else if (objCancelLocation.siteCode == "MPT")
                cancelSite = System.Configuration.ConfigurationManager.AppSettings["PTCancel"];
            else if (objCancelLocation.siteCode == "BSE")
                cancelSite = System.Configuration.ConfigurationManager.AppSettings["SECancel"];

            if (string.IsNullOrEmpty(objCancelLocation.mobileNo) || string.IsNullOrEmpty(objCancelLocation.mobileNo))
            {
                return new string[] { "-1", "Empty Variables!" };
            }
            if (!string.IsNullOrEmpty(objCancelLocation.GPRS))
                cancelSite = cancelSite + Uri_CancelLocationUpdate + " " + objCancelLocation.IMSIActive + " " + objCancelLocation.SGSNNumber + " 2";
            else
                cancelSite = cancelSite + Uri_CancelLocationUpdate + " " + objCancelLocation.IMSIActive + " " + objCancelLocation.VLRNumber;

            string[] hlrResponseData = { "-1", "Failed connecting to the HLR Service!" };
            try
            { 
                // Generate request
                HttpWebRequest hlrMgt = (HttpWebRequest)WebRequest.Create(cancelSite);
                hlrMgt.Method = "POST";
                hlrMgt.UserAgent = "CRM_API";
                hlrMgt.ContentLength = 0;
                // Log it
                Log.Info(string.Format("CancelLocation:{0}", cancelSite));
                // Send request and get the response data
                HttpWebResponse hlrResponse = (HttpWebResponse)hlrMgt.GetResponse();
                StreamReader hlrResponseReader = new StreamReader(hlrResponse.GetResponseStream());
                hlrResponseData = hlrResponseReader.ReadToEnd().Trim().Split(":".ToCharArray(), 2);
            }
            catch (Exception ex)
            {
                return new string[] { "-1", ex.Message };
            }
            return hlrResponseData;
        }
    }
}
