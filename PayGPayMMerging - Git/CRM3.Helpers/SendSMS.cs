﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.IO;
using System.Web;
using NLog;
using System.Text;

namespace CRM_API.Helpers
{
    public class SendSMS
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        private string UKOTAUrl = System.Configuration.ConfigurationManager.AppSettings["UKOTAUrl"];
        private string UKSMSUrl = System.Configuration.ConfigurationManager.AppSettings["UKSMSUrl"];
        private string FROTAUrl = System.Configuration.ConfigurationManager.AppSettings["FROTAUrl"];
        private string FRSMSUrl = System.Configuration.ConfigurationManager.AppSettings["FRSMSUrl"];
        private string PTOTAUrl = System.Configuration.ConfigurationManager.AppSettings["PTOTAUrl"];
        private string PTSMSUrl = System.Configuration.ConfigurationManager.AppSettings["PTSMSUrl"];
        private string FIOTAUrl = System.Configuration.ConfigurationManager.AppSettings["FIOTAUrl"];
        private string FISMSUrl = System.Configuration.ConfigurationManager.AppSettings["FISMSUrl"];
        private string MMISMSUrl = System.Configuration.ConfigurationManager.AppSettings["MMISMSUrl"];
        private string ATOTAUrl = System.Configuration.ConfigurationManager.AppSettings["ATOTAUrl"];
        private string ATSmsUrl = System.Configuration.ConfigurationManager.AppSettings["ATSmsUrl"];
        private string NLOTAUrl = System.Configuration.ConfigurationManager.AppSettings["NLOTAUrl"];
        private string NLSmsUrl = System.Configuration.ConfigurationManager.AppSettings["NLSmsUrl"];
        private string SEOTAUrl = System.Configuration.ConfigurationManager.AppSettings["NLOTAUrl"];
        private string SESmsUrl = System.Configuration.ConfigurationManager.AppSettings["NLSmsUrl"];
        private string BEOTAUrl = System.Configuration.ConfigurationManager.AppSettings["NLOTAUrl"];
        private string BESmsUrl = System.Configuration.ConfigurationManager.AppSettings["NLSmsUrl"];
        private string DKSMSUrl = System.Configuration.ConfigurationManager.AppSettings["DKSMSUrl"];
        private string DKOTAUrl = System.Configuration.ConfigurationManager.AppSettings["DKOTAUrl"];
        //GPRS Setting SMS Setting string
        private string UKSMSUrl_GPRS = System.Configuration.ConfigurationManager.AppSettings["UKSMSUrl_GPRS"];
        private string DKSMSUrl_GPRS = System.Configuration.ConfigurationManager.AppSettings["DKSMSUrl_GPRS"];
        private string ATSMSUrl_GPRS = System.Configuration.ConfigurationManager.AppSettings["ATSMSUrl_GPRS"];
        private string BESMSUrl_GPRS = System.Configuration.ConfigurationManager.AppSettings["BESMSUrl_GPRS"];
        private string FISMSUrl_GPRS = System.Configuration.ConfigurationManager.AppSettings["FISMSUrl_GPRS"];
        private string FRSMSUrl_GPRS = System.Configuration.ConfigurationManager.AppSettings["FRSMSUrl_GPRS"];
        private string NLSMSUrl_GPRS = System.Configuration.ConfigurationManager.AppSettings["NLSMSUrl_GPRS"];
        private string PLSMSUrl_GPRS = System.Configuration.ConfigurationManager.AppSettings["PLSMSUrl_GPRS"];
        private string PTSMSUrl_GPRS = System.Configuration.ConfigurationManager.AppSettings["PTSMSUrl_GPRS"];
        private string SESMSUrl_GPRS = System.Configuration.ConfigurationManager.AppSettings["SESMSUrl_GPRS"];
        //GPRS Setting SMS Setting Gate Way
        private string UKSMSUrl_GPRSGateway = System.Configuration.ConfigurationManager.AppSettings["UKSMSUrl_GPRSGateway"];
        //Added for Serbia
        private string SEROTAUrl = System.Configuration.ConfigurationManager.AppSettings["SEROTAUrl"];
        private string SERSMSUrl = System.Configuration.ConfigurationManager.AppSettings["SERSMSUrl"];

        //Added for CZ
        private string CZOTAUrl = System.Configuration.ConfigurationManager.AppSettings["CZOTAUrl"];
        private string CZSMSUrl = System.Configuration.ConfigurationManager.AppSettings["CZSMSUrl"];

        private const string Uri_SMS =
            "?service-name=port" +
            "&destination-addr={0}" +
            "&originator-addr-type=5" +
            "&originator-addr={1}" +
            "&payload-type=text" +
            "&message={2}";

        public string[] Send(bool useMMIServer, string destination, string originator, string message, int waitTime)
        {
            if (string.IsNullOrEmpty(originator) || string.IsNullOrEmpty(message))
            {
                return new string[] { "-1", "Empty Variables!" };
            }

            string[] tpgResponseData = { "-1", "Failed connecting to the TPG Service!" };
            try
            {
                string SMSUrl = string.Empty;

                if (useMMIServer)
                    SMSUrl = MMISMSUrl;
                else
                {
                    if (destination.StartsWith("44"))
                        SMSUrl = UKSMSUrl;
                    else if (destination.StartsWith("43"))
                        SMSUrl = ATSmsUrl;
                    else if (destination.StartsWith("33"))
                        SMSUrl = FRSMSUrl;
                    else if (destination.StartsWith("351"))
                        SMSUrl = PTSMSUrl;
                    // else if (destination.StartsWith("358"))
                    //SMSUrl = FISMSUrl;
                    else if (destination.StartsWith("45"))
                        SMSUrl = DKSMSUrl;
                    else if (destination.StartsWith("31"))
                        SMSUrl = NLSmsUrl;
                    else if (destination.StartsWith("32"))
                        SMSUrl = BESmsUrl;
                    else if (destination.StartsWith("46"))
                        SMSUrl = SESmsUrl;
                    //Added for Serbia
                    else if (destination.StartsWith("381"))
                        SMSUrl = SERSMSUrl;
                    else if (destination.StartsWith("420"))
                        SMSUrl = CZSMSUrl;
                }

                // Format the uri string
                string requestUriString =
                 string.Format(SMSUrl + Uri_SMS,
                    destination,
                    originator,
                    HttpUtility.UrlPathEncode(message));

                // Generate request
                HttpWebRequest tpg = (HttpWebRequest)WebRequest.Create(requestUriString);
                tpg.Method = "POST";
                tpg.UserAgent = "CRM_API";
                tpg.ContentLength = 0;

                // Log it
                Log.Info(string.Format("Send SMS,{0}", requestUriString));

                // Send request and get the response data
                HttpWebResponse tpgResponse = (HttpWebResponse)tpg.GetResponse();
                StreamReader tpgResponseReader = new StreamReader(tpgResponse.GetResponseStream());
                tpgResponseData = tpgResponseReader.ReadToEnd().Trim().Split(":".ToCharArray(), 2);
                if (waitTime > 0)
                    System.Threading.Thread.Sleep(waitTime);
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                Log.Info(string.Format("Send SMS Error,{0}", ex.Message));
            }

            return tpgResponseData;
        }

        public string[] SendOTA(string destination, string tpUDL, string tpUD, int waitTime)
        {
            if (string.IsNullOrEmpty(tpUDL) || string.IsNullOrEmpty(tpUD))
            {
                return new string[] { "-1", "Empty Variables!" };
            }

            string[] tpgResponseData = { "-1", "Failed connecting to the TPG Service!" };
            try
            {
                string SMSUrl = string.Empty;
                if (destination.StartsWith("44"))
                    SMSUrl = UKOTAUrl;
                else if (destination.StartsWith("43"))
                    SMSUrl = ATOTAUrl;
                else if (destination.StartsWith("33"))
                    SMSUrl = FROTAUrl;
                else if (destination.StartsWith("351"))
                    SMSUrl = PTOTAUrl;
                else if (destination.StartsWith("31"))
                    SMSUrl = NLOTAUrl;
                else if (destination.StartsWith("32"))
                    SMSUrl = BEOTAUrl;
                else if (destination.StartsWith("45"))
                    SMSUrl = DKOTAUrl;
                else if (destination.StartsWith("46"))
                    SMSUrl = SEOTAUrl;
                //Added for Serbia
                else if (destination.StartsWith("381"))
                    SMSUrl = SEROTAUrl;
                else if (destination.StartsWith("420"))
                    SMSUrl = CZOTAUrl;
                // else if (destination.StartsWith("358"))
                //   SMSUrl = FIOTAUrl;

                // Format the uri string
                string requestUriString =
                    string.Format(SMSUrl,
                    destination,
                    tpUDL,
                    tpUD);

                // Generate request
                HttpWebRequest tpg = (HttpWebRequest)WebRequest.Create(requestUriString);
                tpg.Method = "POST";
                tpg.UserAgent = "CRM_API";
                tpg.ContentLength = 0;

                // Log it
                Log.Info(string.Format("Send OTA:{0}", requestUriString));

                // Send request and get the response data
                HttpWebResponse tpgResponse = (HttpWebResponse)tpg.GetResponse();
                StreamReader tpgResponseReader = new StreamReader(tpgResponse.GetResponseStream());
                tpgResponseData = tpgResponseReader.ReadToEnd().Trim().Split(":".ToCharArray(), 2);
                if (waitTime > 0)
                    System.Threading.Thread.Sleep(waitTime);
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                Log.Info(string.Format("Send OTA Error:{0}", ex.Message));
            }

            return tpgResponseData;
        }
        static string AddCountryCode(string Msisdn)
        {
            // example input from crm : 07892170022
            // example input for hlrdb: 447892170022

            string prefix = "44"; // country code for UK

            if (Msisdn.StartsWith("0"))
                Msisdn = prefix + Msisdn.Substring(1, Msisdn.Length - 1);
            else if (!Msisdn.StartsWith(prefix))
                Msisdn = prefix + Msisdn;

            return Msisdn;
        }
        public string Send_SMSPorting(int brand, string destNumber, string message, string sitecode)
        {
            try
            {
                // add country code
                destNumber = Helpers.Porting.Syniverse.AddCountryCode(destNumber, sitecode);

                string smsOriginator;
                if (brand == (int)Models.Porting.PortingBrand.VectoneBrand)
                    smsOriginator = Helpers.Config.SmsOriginatorVectone;
                else if (brand == (int)Models.Porting.PortingBrand.DelightBrand)
                    smsOriginator = Helpers.Config.SmsOriginatorDelight;
                else if (brand == (int)Models.Porting.PortingBrand.UK1Brand)
                    smsOriginator = Helpers.Config.SmsOriginatorUk1;
                else
                    throw new Exception("Unknown mobile brand");

                string param =
                    "?service-name=port" +
                    "&destination-addr={0}" +
                    "&originator-addr-type=5" +
                    "&originator-addr={1}" +
                    "&payload-type=text" +
                    "&message={2}";

                string tpgResponseData = "-1: Failed connecting to the TPG Service!";

                // Generate request
                HttpWebRequest tpg = (HttpWebRequest)WebRequest.Create(
                    string.Format(
                        Config.SmsServerUrl + param,
                        destNumber,
                        smsOriginator,
                        HttpUtility.UrlPathEncode(message)));

                tpg.Method = "POST";
                tpg.UserAgent = "TPGWeb";
                tpg.ContentLength = 0;

                // Send request and get the response data
                HttpWebResponse tpgResponse = (HttpWebResponse)tpg.GetResponse();
                StreamReader tpgResponseReader = new StreamReader(tpgResponse.GetResponseStream());
                tpgResponseData = tpgResponseReader.ReadToEnd();

                return tpgResponseData;
            }

            catch (Exception e)
            {
                throw new Exception("Exception when sending SMS: " + e.Message);
            }
        }
        //Send GPRS Setting SMS improvements
        public string Send_SMSGPRSSetting(string sitecode, string Desination_number, string iOriginatorAddrType, string address)
        {

            try
            {

                string _sresponsestring = "";
                string sResponse = "";

                string SMSUrl = string.Empty;
                string SMStext = string.Empty;
                string postData = string.Empty;


                if (sitecode == "BDK")
                    SMSUrl = DKSMSUrl_GPRS;
                else if (sitecode == "MCM")
                    SMSUrl = UKSMSUrl_GPRSGateway;
                SMStext = UKSMSUrl_GPRS;
                //else if (sitecode == "BAU")
                //    SMSUrl = ATSMSUrl_GPRS;
                //else if (sitecode == "MBE")
                //    SMSUrl = BESMSUrl_GPRS;

                //else if (sitecode == "MFI")
                //    SMSUrl = FISMSUrl_GPRS;
                //else if (sitecode == "MFR")
                //    SMSUrl = FRSMSUrl_GPRS;
                //else if (sitecode == "BNL")
                //    SMSUrl = NLSMSUrl_GPRS;

                //else if (sitecode == "BPL")
                //    SMSUrl = PLSMSUrl_GPRS;
                //else if (sitecode == "MPT")
                //    SMSUrl = PTSMSUrl_GPRS;
                //else if (sitecode == "BSE")
                //    SMSUrl = SESMSUrl_GPRS;
                int smsCount = 0;
                int lenStr = SMStext.Length;

                if (lenStr % 280 != 0)
                    smsCount = (lenStr / 280) + 1;
                else
                    smsCount = lenStr / 280;

                for (int j = 0; j < smsCount; j++)
                {
                    string smsMsg = "";
                    if (j == smsCount - 1)

                        smsMsg = SMStext.Substring(j * 280, lenStr - (280 * j));


                    //postData = "orig-addr=111" +
                    //      "&orig-noa=0" +
                    //      "&support-long=1" +
                    //      "&dest-addr=" + Desination_number +
                    //      "&dest-noa=1" +
                    //      "&tp-dcs=21" +
                    //      "&tp-pid=245" +
                    //      "&tp-udhi=1" +
                    //      "&tp-ud=" + SMStext +
                    //      "&payload-type=text";

                    postData = "orig-addr=" + iOriginatorAddrType +
                                      "&orig-noa=0" +
                                      "&support-long=1" +
                                      "&dest-addr=" + Desination_number +
                                      "&dest-noa=1" +
                                      "&tp-dcs=21" +
                                      "&tp-pid=245" +
                                      "&tp-udhi=1" +
                                      "&tp-ud=" + smsMsg +
                                      "&payload-type=text";


                    HttpWebRequest smsReq = (HttpWebRequest)WebRequest.Create(SMSUrl);
                    smsReq.Method = "POST";
                    smsReq.ContentType = "application/x-www-form-urlencoded";

                    ASCIIEncoding enc = new ASCIIEncoding();
                    byte[] data = enc.GetBytes(postData);
                    smsReq.ContentLength = data.Length;

                    Stream newStream = smsReq.GetRequestStream();
                    newStream.Write(data, 0, data.Length);
                    newStream.Close();

                    HttpWebResponse smsRes = (HttpWebResponse)smsReq.GetResponse();
                    Stream resStream = smsRes.GetResponseStream();

                    Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                    StreamReader readStream = new StreamReader(resStream, encode);
                    _sresponsestring = readStream.ReadToEnd().Trim();
                    sResponse = _sresponsestring;

                }
                return sResponse;
            }

            catch (Exception e)
            {
                throw new Exception("Exception when sending SMS: " + e.Message);
            }
        }

        public static string SMSSendOurGateway(string sOriAddr, string sDestAddr, string sText, string sServerApiUrl)
        {
            string _sresponsestring;
            string postData = "";
            string sResponse = "";
            try
            {
                int iOriginatorAddrType = sOriAddr.ToUpper() == "VECTONE" ? 5 : 3;
                postData = "delivery-receipt=No" +
                                    "&destination-addr=" + sDestAddr +
                                    "&originator-addr-type=" + iOriginatorAddrType +
                                    "&originator-addr=" + sOriAddr +
                                    "&payload-type=text" +
                                    "&message=" + HttpUtility.UrlEncode(sText);

                HttpWebRequest smsReq = (HttpWebRequest)WebRequest.Create(sServerApiUrl);
                smsReq.Method = "POST";
                smsReq.ContentType = "application/x-www-form-urlencoded";
                smsReq.ServicePoint.Expect100Continue = false;

                ASCIIEncoding enc = new ASCIIEncoding();
                byte[] data = enc.GetBytes(postData);
                smsReq.ContentLength = data.Length;

                Stream newStream = smsReq.GetRequestStream();
                newStream.Write(data, 0, data.Length);
                newStream.Close();

                HttpWebResponse smsRes = (HttpWebResponse)smsReq.GetResponse();
                Stream resStream = smsRes.GetResponseStream();

                Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                StreamReader readStream = new StreamReader(resStream, encode);
                _sresponsestring = readStream.ReadToEnd().Trim();
                sResponse = _sresponsestring;
            }
            catch (Exception ex)
            {
                Log.Error("SMSSendOurGateway : " + ex.Message);
                return "-1";
                //throw;
            }
            Log.Info("sResponse : " + sResponse);
            return sResponse;
        }
    }
}