﻿using System;
using System.Collections.Generic;
using System.Configuration;
//using System.Web.Configuration;
using System.Linq;
using System.Web;

namespace CRM_API.Helpers
{
    public class Config
    {
        //PayG
        public static string DefaultMconnection
        {
           // get
           // {
           //     return ConfigurationManager.ConnectionStrings["DefaultMConnection"].ToString();
           // }
            get
            {
                string str = "";
                //str = "DefaultMConnection";
                try
                {
                    str = (HttpContext.Current.Session != null && Convert.ToString(HttpContext.Current.Session["GlobalProduct"]) != "") ? "DefaultMConnection_" + Convert.ToString(HttpContext.Current.Session["GlobalProduct"]).ToUpper() : "DefaultMConnection";

                }
                catch (Exception)
                {
                    str = "DefaultMConnection";
                }
                return Convert.ToString(ConfigurationManager.ConnectionStrings[str]);
            }
        }

        public static string MCMCRMConnectionMarketing
        {
            get
            {
                string str = "";
                try
                {
                    str = (HttpContext.Current.Session != null && Convert.ToString(HttpContext.Current.Session["GlobalProduct"]) != "") ? "MCMCRMConnectionMarketing_" + Convert.ToString(HttpContext.Current.Session["GlobalProduct"]).ToUpper() : "MCMCRMConnectionMarketing";

                }
                catch (Exception)
                {
                    str = "MCMCRMConnectionMarketing";
                }
                return Convert.ToString(ConfigurationManager.ConnectionStrings[str]);
                //  return ConfigurationManager.ConnectionStrings["MCMCRMConnectionMarketing"].ConnectionString;  // Local API Config Connection 
            }
        }

        public static string PortalConnection
        {
            get
            {
                string str = "PortalConnection";
                return Convert.ToString(ConfigurationManager.ConnectionStrings[str]);
            }
        }

        /// <summary>
        /// Connection to user management database
        /// </summary>
        public static string UserMgtConnection
        {
            //get
            //{
            //    return ConfigurationManager.ConnectionStrings["UserMgtConnection"].ToString();
            //}
            get
            {
                string str = "";
                try
                {
                    str = (HttpContext.Current.Session != null && Convert.ToString(HttpContext.Current.Session["GlobalProduct"]) != "") ? "UserMgtConnection_" + Convert.ToString(HttpContext.Current.Session["GlobalProduct"]).ToUpper() : "UserMgtConnection";

                }
                catch (Exception)
                {
                    str = "UserMgtConnection";
                }
                return Convert.ToString(ConfigurationManager.ConnectionStrings[str]);
            }
        }

        /// <summary>
        /// Connection into CRM_API as a default connection site-less
        /// </summary>
        public static string DefaultConnection
        {
            //get
            //{
            //    return ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            //}
            get
            {
                string str = "";
                try
                {
                    str = (HttpContext.Current.Session != null && Convert.ToString(HttpContext.Current.Session["GlobalProduct"]) != "") ? "DefaultConnection_" + Convert.ToString(HttpContext.Current.Session["GlobalProduct"]).ToUpper() : "DefaultConnection";

                }
                catch (Exception)
                {
                    str = "DefaultConnection";
                }
                return Convert.ToString(ConfigurationManager.ConnectionStrings[str]);
            }
        }

        /// <summary>
        /// Connection into CRM_API as a default connection site-less
        /// </summary>
        public static string CountrySaverConnection
        {
            //get
            //{
            //    return ConfigurationManager.ConnectionStrings["CountrySaverConnection"].ToString();
            //}
            get
            {
                string str = "";
                try
                {
                    str = (HttpContext.Current.Session != null && Convert.ToString(HttpContext.Current.Session["GlobalProduct"]) != "") ? "CountrySaverConnection_" + Convert.ToString(HttpContext.Current.Session["GlobalProduct"]).ToUpper() : "CountrySaverConnection";

                }
                catch (Exception)
                {
                    str = "CountrySaverConnection";
                }
                return Convert.ToString(ConfigurationManager.ConnectionStrings[str]);
            }
        }


        /// <summary>
        /// Connection into CRM_API as a PMBO connection site-less
        /// </summary>
        public static string PMBOConnection
        {
            //get
            //{
            //    return ConfigurationManager.ConnectionStrings["PMBOConnection"].ToString();
            //}
            get
            {
                string str = "";
                try
                {
                    str = (HttpContext.Current.Session != null && Convert.ToString(HttpContext.Current.Session["GlobalProduct"]) != "") ? "PMBOConnection_" + Convert.ToString(HttpContext.Current.Session["GlobalProduct"]).ToUpper() : "PMBOConnection";

                }
                catch (Exception)
                {
                    str = "PMBOConnection";
                }
                return Convert.ToString(ConfigurationManager.ConnectionStrings[str]);
            }
        }



        public static string MMICRMConnection
        {
            //get
            //{
            //    return ConfigurationManager.ConnectionStrings["MMICRMConnection"].ConnectionString;
            //}
            get
            {
                string str = "";
                try
                {
                    str = (HttpContext.Current.Session != null && Convert.ToString(HttpContext.Current.Session["GlobalProduct"]) != "") ? "MMICRMConnection_" + Convert.ToString(HttpContext.Current.Session["GlobalProduct"]).ToUpper() : "MMICRMConnection";

                }
                catch (Exception)
                {
                    str = "MMICRMConnection";
                }
                return Convert.ToString(ConfigurationManager.ConnectionStrings[str]);
            }
        }

        public static string MMIHLRConnection
        {
            //get
            //{
            //    return ConfigurationManager.ConnectionStrings["MMIHLRConnection"].ConnectionString;
            //}
            get
            {
                string str = "";
                try
                {
                    str = (HttpContext.Current.Session != null && Convert.ToString(HttpContext.Current.Session["GlobalProduct"]) != "") ? "MMIHLRConnection_" + Convert.ToString(HttpContext.Current.Session["GlobalProduct"]).ToUpper() : "MMIHLRConnection";

                }
                catch (Exception)
                {
                    str = "MMIHLRConnection";
                }
                return Convert.ToString(ConfigurationManager.ConnectionStrings[str]);
            }
        }

        public static string SMSCampaignConnection
        {
            //get
            //{
            //    return ConfigurationManager.ConnectionStrings["SMSCampaignConnection"].ConnectionString;
            //}
            get
            {
                string str = "";
                try
                {
                    str = (HttpContext.Current.Session != null && Convert.ToString(HttpContext.Current.Session["GlobalProduct"]) != "") ? "SMSCampaignConnection_" + Convert.ToString(HttpContext.Current.Session["GlobalProduct"]).ToUpper() : "SMSCampaignConnection";

                }
                catch (Exception)
                {
                    str = "SMSCampaignConnection";
                }
                return Convert.ToString(ConfigurationManager.ConnectionStrings[str]);
            }
        }

        public static string CRMConnection
        {
            get
            {
                return Session.SiteConnection == null ? string.Empty : ConfigurationManager.ConnectionStrings[Session.SiteConnection.crm_connstring].ConnectionString;
            }
        }

        /// <summary>
        /// Connection into Payment 
        /// </summary>
        public static string PaymentConnection
        {
            //get
            //{
            //    return ConfigurationManager.ConnectionStrings["PaymentConnection"].ToString();
            //}
            get
            {
                string str = "";
                try
                {
                    str = (HttpContext.Current.Session != null && Convert.ToString(HttpContext.Current.Session["GlobalProduct"]) != "") ? "PaymentConnection_" + Convert.ToString(HttpContext.Current.Session["GlobalProduct"]).ToUpper() : "PaymentConnection";

                }
                catch (Exception)
                {
                    str = "PaymentConnection";
                }
                return Convert.ToString(ConfigurationManager.ConnectionStrings[str]);
            }
        }
        public static string MCMESPConnection
        {
            //get
            //{
            //    return ConfigurationManager.ConnectionStrings["MCMESPConnection"].ToString();
            //}
            get
            {
                string str = "";
                try
                {
                    str = (HttpContext.Current.Session != null && Convert.ToString(HttpContext.Current.Session["GlobalProduct"]) != "") ? "MCMESPConnection_" + Convert.ToString(HttpContext.Current.Session["GlobalProduct"]).ToUpper() : "MCMESPConnection";

                }
                catch (Exception)
                {
                    str = "MCMESPConnection";
                }
                return Convert.ToString(ConfigurationManager.ConnectionStrings[str]);
            }
        }

        public static string MCMCRMConnectionVMFR

        {
            get
            {

                return Convert.ToString(ConfigurationManager.ConnectionStrings["MCMCRMConnection_VMFR"]);
            }
        }
        public static string MCMCRMConnectionVMNL
        {
            get
            {

                return Convert.ToString(ConfigurationManager.ConnectionStrings["MCMCRMConnection_VMNL"]);
            }
        }
        public static string MCMCRMConnectionVMAT
        {
            get
            {

                return Convert.ToString(ConfigurationManager.ConnectionStrings["MCMCRMConnection_VMAT"]);
            }
        }
        public static string MCMCRMConnection
        {
            get
            {
                string str = "";
                try
                {
                    //// 07-Aug-2015 : Moorthy Modified to avoid PMBO_Get_ServiceStatusHistory sp not found exception
                    //str = "MCMCRMConnection";
                    str = (HttpContext.Current.Session != null && Convert.ToString(HttpContext.Current.Session["GlobalProduct"]) != "") ? "MCMCRMConnection_" + Convert.ToString(HttpContext.Current.Session["GlobalProduct"]).ToUpper() : "MCMCRMConnection";

                }
                catch (Exception)
                {
                    str = "MCMCRMConnection";
                }
                return Convert.ToString(ConfigurationManager.ConnectionStrings[str]);
            }
        }

        public static string MCMHLRConnection
        {
            //get
            //{
            //    return ConfigurationManager.ConnectionStrings["MCMHLRConnection"].ToString();
            //}
            get
            {
                string str = "";
                try
                {
                    str = (HttpContext.Current.Session != null && Convert.ToString(HttpContext.Current.Session["GlobalProduct"]) != "") ? "MCMHLRConnection_" + Convert.ToString(HttpContext.Current.Session["GlobalProduct"]).ToUpper() : "MCMHLRConnection";

                }
                catch (Exception)
                {
                    str = "MCMHLRConnection";
                }
                return Convert.ToString(ConfigurationManager.ConnectionStrings[str]);
            }
        }
        public static string NetworkOperatorCode
        {
            get
            {
                return ConfigurationManager.AppSettings["NetworkOperatorCode"].ToString();
            }
        }
        public static string RequestHeaderXmlFile
        {
            get
            {
                return ConfigurationManager.AppSettings["RequestHeader"].ToString();
            }
        }

        public static string RequestContentXmlFile
        {
            get
            {
                return ConfigurationManager.AppSettings["RequestContent"].ToString();
            }
        }

        public static string ReportRequestHeaderXmlFile
        {
            get
            {
                return ConfigurationManager.AppSettings["ReportRequestHeader"].ToString();
            }
        }
        public static string SyniverseUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["SyniverseUrl"].ToString();
            }
        }

        public static string SyniverseUsername
        {
            get
            {
                return ConfigurationManager.AppSettings["SyniverseUsername"].ToString();
            }
        }

        public static string SyniversePassword
        {
            get
            {
                return ConfigurationManager.AppSettings["SyniversePassword"].ToString();
            }
        }

        public static string SyniverseCertificateFile
        {
            get
            {
                return ConfigurationManager.AppSettings["SyniverseCertificateFile"].ToString();
            }
        }

        public static string SyniverseCertificatePassword
        {
            get
            {
                return ConfigurationManager.AppSettings["SyniverseCertificatePassword"].ToString();
            }
        }

        public static string SyniverseXmlProblematicString
        {
            get
            {
                return ConfigurationManager.AppSettings["SyniverseXmlProblematicString"].ToString().Replace("^", "\"");
            }
        }
        public static string PortInReportDetailByPacMsisdn
        {
            get
            {
                return ConfigurationManager.AppSettings["PortInReportDetailByPacMsisdn"].ToString();
            }
        }
        public static string SmsOriginatorVectone
        {
            get
            {
                try
                {
                    return ConfigurationManager.AppSettings["SmsOriginatorVectone"].ToString();
                }
                catch (Exception e)
                {
                    throw new Exception("Error retrieving config SmsOriginatorVectone");
                }
            }
        }
        public static string SmsOriginatorDelight
        {
            get
            {
                try
                {
                    return ConfigurationManager.AppSettings["SmsOriginatorDelight"].ToString();
                }
                catch (Exception e)
                {
                    throw new Exception("Error retrieving config SmsOriginatorDelight");
                }
            }
        }
        public static string SmsOriginatorUk1
        {
            get
            {
                try
                {
                    return ConfigurationManager.AppSettings["SmsOriginatorUk1"].ToString();
                }
                catch (Exception e)
                {
                    throw new Exception("Error retrieving config SmsOriginatorUk1");
                }
            }
        }
        public static string SmsServerUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["SmsServerUrl"].ToString();
            }
        }
        public static string HLRCheck
        {
            get
            {
                return ConfigurationManager.AppSettings["HLRCheck"].ToString();
            }
        }
        public static string SoapRecieverWebSvc(string sitecode)
        {
            string key = string.Format("{0}_SOAP_Receiver_Service", sitecode);
            return ConfigurationManager.AppSettings[key].ToString();
        }
        public static string GetVersion(string sitecode)
        {
            string key = string.Format("{0}_Version", sitecode);
            return ConfigurationManager.AppSettings[key].ToString();
        }
        public static int GetEnvFlag(string sitecode)
        {
            string key = string.Format("{0}_EnvFlag", sitecode);
            return Convert.ToInt32(ConfigurationManager.AppSettings[key].ToString());
        }
        public static string GetPrefixFile(string sitecode)
        {
            string key = string.Format("{0}_Operator", sitecode);
            return ConfigurationManager.AppSettings[key].ToString();
        }
        public static string GetPortingXMLFormatPath(string sitecode)
        {
            string key = string.Format("{0}_PortingXMLFormat", sitecode);
            return ConfigurationManager.AppSettings[key].ToString();
        }
        public static string SiteCRMConnection(string sitecode)
        {
            string key = string.Format("{0}CRMConnection", sitecode);
            return ConfigurationManager.ConnectionStrings[key].ToString();
        }
        public static string SiteESPConnection(string sitecode)
        {
            string key = string.Format("{0}ESPConnection", sitecode);
            return ConfigurationManager.ConnectionStrings[key].ToString();
        }
        public static string SiteMVNOConnection(string sitecode)
        {
            string key = string.Format("{0}MVNOConnection", sitecode);
            return ConfigurationManager.ConnectionStrings[key].ToString();
        }

        //12-Feb-2019 : Moorthy : Added for General Report
        public static string ESPMCMConnection
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ESPMCM"].ToString();
            }
        }

        // 07-Sep-2019: Moorthy : Added for AT Sim Activation
        public static string SIMACTIVATION
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["SIMACTIVATION"].ToString();
            }
        }
    }
}