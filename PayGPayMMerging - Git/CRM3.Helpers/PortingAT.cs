﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Helpers.Porting.AT
{
    public class EnumErr
    {
        public enum EnumErrStatus : int
        {
            OK = 0,
            Rufnummernichtaktiv = 101,
            Kundendatenfalsch = 102,
            Kundenkategoriefalsch = 103,
            RNkorrespondiertnichtmitPortiercode = 104,
            Portiercodeungültig = 105,
            Portierdatumnichtmöglich = 106,
            Portiercodeabgelaufen = 107,
            CancellationCodeungültig = 108,
            PointOfNoReturn1erreicht = 109,
            Portierkapazitätüberschritten = 110,
            Portierungbereitsangestoßen = 111,
            KeinPrepaidGuthaben = 112,
            RNungültig = 113,
            NÜVInfoverweigert = 114,
            Portierungverweigert = 115,
            FehlendeHauptMSISDN = 116,
            FehlendeVoiceBoxMSISDN = 117,
            UngültigeRoutingAction = 118,
            FehlerinRoutinglisteaufgetreten = 119,
            FehlenderPortrequest = 120,
            FehlenderRoutingRequest = 121,
            Rückgabenichtvollständig = 122,
            FalscherRangeHolder = 123,
            Durchführungsauftragunvollständig = 124,
            Rückgabenichterlaubt = 125,
            Streckeungültig = 200,
            Nummernichtportierbar = 201,
            GKNÜVInfoverweigert = 202,
            Prüfrufnummernichtgültig = 203,
            Vollmachtnichtgültig = 204,
            EMailfalsch = 205,
            KeinGroßkunde = 206,
            Sonstige = 999
        }
        public static ArrayList RetrieveErrStatus()
        {
            ArrayList nuvInfoStatus = new ArrayList();

            ErrStatus orderEnum = new ErrStatus(0, "OK");
            nuvInfoStatus.Add(orderEnum);

            orderEnum = new ErrStatus(101, "Rufnummer nicht aktiv");
            nuvInfoStatus.Add(orderEnum);

            orderEnum = new ErrStatus(102, "Kundendaten falsch");
            nuvInfoStatus.Add(orderEnum);

            orderEnum = new ErrStatus(103, "Kunden kategorie falsch");
            nuvInfoStatus.Add(orderEnum);

            orderEnum = new ErrStatus(104, "RN korrespondiertnicht mit Portiercode");
            nuvInfoStatus.Add(orderEnum);

            orderEnum = new ErrStatus(105, "Portiercode ungültig");
            nuvInfoStatus.Add(orderEnum);

            orderEnum = new ErrStatus(106, "Portierdatum nicht möglich");
            nuvInfoStatus.Add(orderEnum);

            orderEnum = new ErrStatus(107, "Portiercode abgelaufen");
            nuvInfoStatus.Add(orderEnum);

            orderEnum = new ErrStatus(108, "CancellationCode ungültig");
            nuvInfoStatus.Add(orderEnum);

            orderEnum = new ErrStatus(109, "PointOfNoReturn 1 erreicht");
            nuvInfoStatus.Add(orderEnum);

            orderEnum = new ErrStatus(110, "Portierkapazität überschritten");
            nuvInfoStatus.Add(orderEnum);

            orderEnum = new ErrStatus(111, "Portierung bereits angestoßen");
            nuvInfoStatus.Add(orderEnum);

            orderEnum = new ErrStatus(112, "Kein Prepaid Guthaben");
            nuvInfoStatus.Add(orderEnum);

            orderEnum = new ErrStatus(113, "RN ungültig");
            nuvInfoStatus.Add(orderEnum);

            orderEnum = new ErrStatus(114, "NÜV-Info verweigert");
            nuvInfoStatus.Add(orderEnum);

            orderEnum = new ErrStatus(115, "Portierung verweigert");
            nuvInfoStatus.Add(orderEnum);

            orderEnum = new ErrStatus(116, "Fehlende Haupt-MSISDN");
            nuvInfoStatus.Add(orderEnum);

            orderEnum = new ErrStatus(117, "Fehlende VoiceBox-MSISDN");
            nuvInfoStatus.Add(orderEnum);

            orderEnum = new ErrStatus(118, "Ungültige Routing Action");
            nuvInfoStatus.Add(orderEnum);

            orderEnum = new ErrStatus(119, "Fehler in Routingliste aufgetreten");
            nuvInfoStatus.Add(orderEnum);

            orderEnum = new ErrStatus(120, "Fehler in Routingliste aufgetreten");
            nuvInfoStatus.Add(orderEnum);

            orderEnum = new ErrStatus(121, "Fehlender Routing Request");
            nuvInfoStatus.Add(orderEnum);

            orderEnum = new ErrStatus(122, "Rückgabe nicht vollständig");
            nuvInfoStatus.Add(orderEnum);

            orderEnum = new ErrStatus(123, "Falscher Range Holder");
            nuvInfoStatus.Add(orderEnum);

            orderEnum = new ErrStatus(124, "Durchführungsauftrag unvollständig");
            nuvInfoStatus.Add(orderEnum);

            orderEnum = new ErrStatus(125, "Rückgabe nicht erlaubt");
            nuvInfoStatus.Add(orderEnum);

            orderEnum = new ErrStatus(200, "Strecke ungültig");
            nuvInfoStatus.Add(orderEnum);

            orderEnum = new ErrStatus(201, "Nummer nicht portierbar");
            nuvInfoStatus.Add(orderEnum);

            orderEnum = new ErrStatus(202, "GK NÜVInfo verweigert");
            nuvInfoStatus.Add(orderEnum);

            orderEnum = new ErrStatus(203, "Prüfrufnummer nicht gültig");
            nuvInfoStatus.Add(orderEnum);

            orderEnum = new ErrStatus(204, "Vollmacht nicht gültig");
            nuvInfoStatus.Add(orderEnum);

            orderEnum = new ErrStatus(205, "E-Mail falsch");
            nuvInfoStatus.Add(orderEnum);

            orderEnum = new ErrStatus(206, "Kein Großkunde");
            nuvInfoStatus.Add(orderEnum);

            orderEnum = new ErrStatus(999, "Sonstige");
            nuvInfoStatus.Add(orderEnum);

            return nuvInfoStatus;
        }
        public static ArrayList RetrieveErrStatus(int blankItemPosition, string blankItemText)
        {
            ArrayList ArrOrderEnumErrStatus = RetrieveErrStatus();
            ArrOrderEnumErrStatus.Insert(blankItemPosition, new ErrStatus(-1, blankItemText));
            return ArrOrderEnumErrStatus;
        }
        public class ErrStatus
        {
            private int _value;
            private string _text;

            public ErrStatus(int value, string text)
            {
                _value = value;
                _text = text;
            }
            public int Value
            {
                get { return _value; }
                set { _value = value; }
            }
            public string Text
            {
                get { return _text; }
                set { _text = value; }
            }
        }
        public static string ToText(int oValue)
        {
            ArrayList colPOS = RetrieveErrStatus();
            foreach (ErrStatus oItem in colPOS)
            {
                if (oItem.Value == oValue)
                {
                    return oItem.Text;
                }
            }
            throw new Exception("Value not found.");
        }
        public static int ToValue(string oName)
        {
            ArrayList colPOS = RetrieveErrStatus();
            foreach (ErrStatus oItem in colPOS)
            {
                if (oItem.Text == oName)
                {
                    return oItem.Value;
                }
            }
            throw new Exception("Text not found.");
        }
    }
}
