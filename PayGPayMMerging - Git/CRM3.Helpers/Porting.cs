﻿using System;
using CRM_API.Models.Porting.Syniverse;

namespace CRM_API.Helpers.Porting
{
    public class Syniverse
    {
        public static CRM_API.Models.Porting.Syniverse.SubmitRequest CreateSubmitRequest(string pac, string msisdn, DateTime portDate,
            string contactName, string contactDetails, string accNo, string additionalRef, string bbMsisdn, string ReqCode)
        {
            string NetworkOperatorCode = Config.NetworkOperatorCode;
            CRM_API.Models.Porting.Syniverse.SubmitRequest result = new CRM_API.Models.Porting.Syniverse.SubmitRequest();
            result.ReqCode = NetworkOperatorCode + ReqCode;
            result.EntryKey = new CRM_API.Models.Porting.Syniverse.EntryKey();
            result.EntryKey.Pac = pac;
            result.EntryKey.Msisdn = msisdn;
            result.EntryKey.BbMsisdn = bbMsisdn;
            result.PortDate = portDate;
            result.NoCode = NetworkOperatorCode;
            result.SpOptionalData = new CRM_API.Models.Porting.Syniverse.OptionalData();
            result.SpOptionalData.ContactInfo = new CRM_API.Models.Porting.Syniverse.ContactInfo();
            result.SpOptionalData.ContactInfo.ContactName = contactName;
            result.SpOptionalData.ContactInfo.ContactDetails = contactDetails;
            result.SpOptionalData.PrivateData = new CRM_API.Models.Porting.Syniverse.PrivateData();
            result.SpOptionalData.PrivateData.AccNo = accNo;
            result.SpOptionalData.PrivateData.Ref = additionalRef;
            result.SpOptionalData.PrivateData.SpId = bbMsisdn;

            return result;
        }
        public static string Logon()
        {
            try
            {
                string reqXml = XmlAdapter.CreateLogonXml();
                string resXml = XmlAdapter.send_xml(reqXml);
                string sessionId = XmlAdapter.ReceiveLogonRespXml(resXml).Cookie;
                return sessionId;
            }
            catch (Exception e)
            {
                throw new Exception("Logon Failed: " + e.Message);
            }
        }
        public static Report CreateSubmitRequestReport(string pac, string msisdn, string ReqCode)
        {
            Report reqObj = new Report();

            reqObj.ReqCode = Config.NetworkOperatorCode + ReqCode;
            reqObj.Name = Config.PortInReportDetailByPacMsisdn;
            reqObj.Pac = pac;
            reqObj.Msisdn = msisdn;

            return reqObj;
        }

        public static CRM_API.Models.Porting.Syniverse.ReviseRequest CreateReviseRequest(string pac, string msisdn, DateTime portDate,
            string contactName, string contactDetails, string ReqCode)
        {
            string NetworkOperatorCode = Config.NetworkOperatorCode;
            CRM_API.Models.Porting.Syniverse.ReviseRequest result = new CRM_API.Models.Porting.Syniverse.ReviseRequest();
            result.ReqCode = NetworkOperatorCode + ReqCode;
            result.EntryKey = new CRM_API.Models.Porting.Syniverse.EntryKey();
            result.EntryKey.Pac = pac;
            result.EntryKey.Msisdn = msisdn;
            result.PortDate = portDate;
            result.NoCode = NetworkOperatorCode;
            result.SpOptionalData = new CRM_API.Models.Porting.Syniverse.OptionalData();
            result.SpOptionalData.ContactInfo = new CRM_API.Models.Porting.Syniverse.ContactInfo();
            result.SpOptionalData.ContactInfo.ContactName = contactName;
            result.SpOptionalData.ContactInfo.ContactDetails = contactDetails;

            return result;
        }

        public static CRM_API.Models.Porting.Syniverse.CancelRequest CancelRequest(string pac, string msisdn, DateTime portDate,
            string contactName, string contactDetails, string ReqCode)
        {
            string NetworkOperatorCode = Config.NetworkOperatorCode;
            CRM_API.Models.Porting.Syniverse.CancelRequest result = new CRM_API.Models.Porting.Syniverse.CancelRequest();
            result.ReqCode = NetworkOperatorCode + ReqCode;
            result.CancelAllInPac = false;

            result.EntryKey = new CRM_API.Models.Porting.Syniverse.EntryKey();
            result.EntryKey.Pac = pac;
            result.EntryKey.Msisdn = msisdn;

            return result;
        }
        public static string AddCountryCode(string Msisdn, string Sitecode)
        {
            // example input from crm : 07892170022
            // example input for hlrdb: 447892170022
            string prefix = "44"; // country code for UK
            switch (Sitecode.ToLower().Trim())
            {
                case "mcm": prefix = "44"; break;
                case "bau": prefix = "43"; break;
                case "bdk": prefix = "45"; break;
                case "bnl": prefix = "31"; break;
                case "bse": prefix = "46"; break;
                case "mfi": prefix = "358"; break;
                case "mfr": prefix = "33"; break;
                case "mpt": prefix = "351"; break;
                default: prefix = "44"; break;
            }

            if (Msisdn.StartsWith("0"))
                Msisdn = prefix + Msisdn.Substring(1, Msisdn.Length - 1);
            else if (!Msisdn.StartsWith(prefix))
                Msisdn = prefix + Msisdn;

            return Msisdn;
        }
    }
}
