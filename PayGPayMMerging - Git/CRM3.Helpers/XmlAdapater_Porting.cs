﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using System.Collections.Specialized;

namespace CRM_API.Helpers.Porting
{
    public class XmlAdapter
    {
        static string RequestHeaderXml, RequestContentXml, ReportRequestHeaderXml, CertificateFile, CertificatePassword;
        static string problematicString = Config.SyniverseXmlProblematicString;
        static X509Certificate2 certv2;
        static XmlAdapter()
        {
            RequestHeaderXml = readFile(Config.RequestHeaderXmlFile);
            RequestContentXml = readFile(Config.RequestContentXmlFile);
            ReportRequestHeaderXml = readFile(Config.ReportRequestHeaderXmlFile);
            CertificateFile = HttpContext.Current != null ?
                                HttpContext.Current.Server.MapPath(Config.SyniverseCertificateFile)
                                : Config.SyniverseCertificateFile;
            CertificatePassword = Config.SyniverseCertificatePassword;
            // use relative server path for web application, and use local path for non web application
        }
        public static string CreateReportXml(CRM_API.Models.Porting.Syniverse.Report reportObj)
        {
            string xml = GetXmlRequest("Report");
            xml = xml.Replace("##ReqCode##", reportObj.ReqCode);
            xml = xml.Replace("##Name##", reportObj.Name);

            #region OptionalXml, optional
            string optionalXml = string.Empty;

            if (reportObj.Pac != null)
                optionalXml += "<pac>" + reportObj.Pac + "</pac>";

            if (reportObj.Msisdn != null)
                optionalXml += "<msisdn>" + reportObj.Msisdn + "</msisdn>";

            if (reportObj.PacExpirySelector != null)
            {
                optionalXml += "<pac_expiry_selector><equality_test>";
                optionalXml += reportObj.PacExpirySelector.EqualityTest.ToString();
                optionalXml += "</equality_test><days>";
                optionalXml += reportObj.PacExpirySelector.Days.ToString();
                optionalXml += "</days></pac_expiry_selector>";
            }

            if (reportObj.StatusSelector != CRM_API.Models.Porting.Syniverse.StatusSelector.Null)
                optionalXml += "<status_selector><" + reportObj.StatusSelector.ToString() + " /></status_selector>";

            if (reportObj.BulkTransferSelector != CRM_API.Models.Porting.Syniverse.BulkTransferSelector.Null)
                optionalXml += "<bulk_transfer_selector><" + reportObj.BulkTransferSelector.ToString() + " /></bulk_transfer_selector>";

            if (reportObj.MsisdnQuantitySelector != null)
            {
                optionalXml += "<msisdn_quantity_selector><equality_test><";
                optionalXml += reportObj.MsisdnQuantitySelector.EqualityTest.ToString();
                optionalXml += " /></equality_test><entry_count>";
                optionalXml += reportObj.MsisdnQuantitySelector.EntryCount.ToString();
                optionalXml += "</entry_count></msisdn_quantity_selector>";
            }

            if (reportObj.TransferTypeSelector != CRM_API.Models.Porting.Syniverse.TransferTypeSelector.Null)
                optionalXml += "<transfer_type_selector><" + reportObj.TransferTypeSelector + " /></transfer_type_selector>";

            if (reportObj.PortDueSelector != null)
            {
                optionalXml += "<port_due_selector><equality_test><";
                optionalXml += reportObj.PortDueSelector.EqualityTest.ToString();
                optionalXml += " /></equality_test><days>";
                optionalXml += reportObj.PortDueSelector.Days.ToString();
                optionalXml += "</days></port_due_selector>";
            }

            if (reportObj.PacAgeSelector != null)
            {
                optionalXml += "<pac_age_selector><equality_test><";
                optionalXml += reportObj.PacAgeSelector.EqualityTest.ToString();
                optionalXml += " /></equality_test><days>";
                optionalXml += reportObj.PacAgeSelector.Days.ToString();
                optionalXml += "</days></pac_age_selector>";
            }

            if (reportObj.TaggedSelector != CRM_API.Models.Porting.Syniverse.TaggedSelector.Null)
                optionalXml += "<tagged_selector><" + reportObj.TaggedSelector.ToString() + " /></tagged_selector>";

            xml = xml.Replace("##OptionalParameter##", optionalXml);
            #endregion

            #region OptionalDsp, optional
            string dspXml = string.Empty;

            if (reportObj.DspAccNo != null)
                dspXml += "<dsp_acc_no>" + reportObj.DspAccNo + "</dsp_acc_no>";

            if (reportObj.DspRef != null)
                dspXml += "<dsp_ref>" + reportObj.DspRef + "</dsp_ref>";

            if (reportObj.DspContactName != null)
                dspXml += "<dsp_contact_name>" + reportObj.DspContactName + "</dsp_contact_name>";

            if (reportObj.DspContactDetails != null)
                dspXml += "<dsp_contact_details>" + reportObj.DspContactDetails + "</dsp_contact_details>";

            if (reportObj.DspSelector != null)
            {
                dspXml += "<dsp_selector>";

                if (reportObj.DspSelector.SpCode != null)
                    dspXml += "<sp_code>" + reportObj.DspSelector.SpCode + "</sp_code>";
                else
                    dspXml += "<All/ >";
                dspXml += "</dsp_selector>";
            }

            if (reportObj.DnoSelector != null)
            {
                dspXml += "<dno_selector>";

                if (reportObj.DnoSelector.NoCode != null)
                    dspXml += "<no_code>" + reportObj.DnoSelector.NoCode + "</no_code>";
                else
                    dspXml += "<All/ >";
                dspXml += "</dno_selector>";
            }

            xml = xml.Replace("##OptionalDspData##", dspXml);
            #endregion

            #region OptionalRsp, optional
            string rspXml = string.Empty;

            if (reportObj.RspAccNo != null)
                rspXml += "<rsp_acc_no>" + reportObj.RspAccNo + "</rsp_acc_no>";

            if (reportObj.RspRef != null)
                rspXml += "<rsp_ref>" + reportObj.RspRef + "</rsp_ref>";

            if (reportObj.RspContactName != null)
                rspXml += "<rsp_contact_name>" + reportObj.RspContactName + "</rsp_contact_name>";

            if (reportObj.RspContactDetails != null)
                rspXml += "<rsp_contact_details>" + reportObj.RspContactDetails + "</rsp_contact_details>";

            if (reportObj.RspSelector != null)
            {
                rspXml += "<rsp_selector>";

                if (reportObj.RspSelector.SpCode != null)
                    rspXml += "<sp_code>" + reportObj.RspSelector.SpCode + "</sp_code>";
                else
                    rspXml += "<All/ >";
                rspXml += "</rsp_selector>";
            }

            if (reportObj.RnoSelector != null)
            {
                rspXml += "<rno_selector>";

                if (reportObj.RnoSelector.NoCode != null)
                    rspXml += "<no_code>" + reportObj.RnoSelector.NoCode + "</no_code>";
                else
                    rspXml += "<All/ >";
                rspXml += "</rno_selector>";
            }

            xml = xml.Replace("##OptionalRspData##", rspXml);
            #endregion

            return IndentXMLString(xml);
        }
        public static CRM_API.Models.Porting.Syniverse.SubmitRequestResp ReceiveSubmitRequestRespXml(string respXml)
        {
            CRM_API.Models.Porting.Syniverse.SubmitRequestResp respObj = new CRM_API.Models.Porting.Syniverse.SubmitRequestResp();
            respObj.EntryResult = new CRM_API.Models.Porting.Syniverse.EntryResult();

            XmlDocument xml = new XmlDocument();
            xml.LoadXml(respXml);

            #region Attributes
            XmlNode mainNode = xml.GetElementsByTagName("submit_request_resp")[0];
            respObj.RespCode = mainNode.Attributes["resp_code"].InnerText;

            if (mainNode.Attributes["port_date_warning"] != null)
                respObj.PortDateWarning = mainNode.Attributes["port_date_warning"].InnerText == "True" ? true : false;
            else
                respObj.PortDateWarning = false;
            #endregion

            #region Element
            respObj.EntryResult.Msisdn = xml.GetElementsByTagName("msisdn")[0].InnerText;

            if (xml.GetElementsByTagName("error").Count > 0)
                respObj.EntryResult.Error = ReceiveErrorXml(xml.GetElementsByTagName("error")[0].InnerText);

            respObj.PortDate =
                DateTime.ParseExact(xml.GetElementsByTagName("port_date")[0].InnerText, "yyyyMMdd", null);

            #endregion

            return respObj;
        }

        static CRM_API.Models.Porting.Syniverse.EntryResultList ReceiveEntryResultListXml(string xmlString)
        {
            CRM_API.Models.Porting.Syniverse.EntryResultList obj = new CRM_API.Models.Porting.Syniverse.EntryResultList();
            obj.EntryResult = new List<CRM_API.Models.Porting.Syniverse.EntryResult>();
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(xmlString);

            XmlNodeList entryResultList = xml.GetElementsByTagName("entry_result");
            foreach (XmlNode entryResultXml in entryResultList)
            {
                XmlDocument xmlTemp = new XmlDocument();
                xmlTemp.LoadXml(entryResultXml.OuterXml);
                CRM_API.Models.Porting.Syniverse.EntryResult entryObj = new CRM_API.Models.Porting.Syniverse.EntryResult();

                entryObj.Msisdn = xmlTemp.GetElementsByTagName("msisdn")[0].InnerText;

                if (xmlTemp.GetElementsByTagName("error").Count > 0)
                    entryObj.Error = ReceiveErrorXml(xmlTemp.GetElementsByTagName("error")[0].InnerXml);

                obj.EntryResult.Add(entryObj);
            }

            return obj;
        }

        public static CRM_API.Models.Porting.Syniverse.ReviseRequestResp ReceiveReviseRequestRespXml(string respXml)
        {


            CRM_API.Models.Porting.Syniverse.ReviseRequestResp respObj = new CRM_API.Models.Porting.Syniverse.ReviseRequestResp();
            CRM_API.Models.Porting.Syniverse.SubmitRequestResp tempObj = ReceiveSubmitRequestRespXml(respXml.Replace("revise_request_resp", "submit_request_resp"));

            respObj.RespCode = tempObj.RespCode;
            respObj.PortDateWarning = tempObj.PortDateWarning;
            respObj.EntryResult = tempObj.EntryResult;
            respObj.PortDate = tempObj.PortDate;

            return respObj;
        }

        public static CRM_API.Models.Porting.Syniverse.CancelRequestResp ReceiveCancelRequestRespXml(string respXml)
        {


            CRM_API.Models.Porting.Syniverse.CancelRequestResp respObj = new CRM_API.Models.Porting.Syniverse.CancelRequestResp();
            CRM_API.Models.Porting.Syniverse.CancelEntryResp tempObj = ReceiveCancelEntryRespXml(respXml.Replace("cancel_request_resp", "cancel_entry_resp"));

            respObj.RespCode = tempObj.RespCode;
            respObj.CancelAllInPacRequested = tempObj.CancelAllInPacRequested != null ? tempObj.CancelAllInPacRequested : false;
            respObj.Pac = tempObj.Pac;
            respObj.MultipleEntryResult = tempObj.MultipleEntryResult;

            return respObj;
        }

        public static CRM_API.Models.Porting.Syniverse.CancelEntryResp ReceiveCancelEntryRespXml(string respXml)
        {


            CRM_API.Models.Porting.Syniverse.CancelEntryResp respObj = new CRM_API.Models.Porting.Syniverse.CancelEntryResp();
            respObj.MultipleEntryResult = new CRM_API.Models.Porting.Syniverse.MultipleEntryResult();
            respObj.MultipleEntryResult.ResultCount = new CRM_API.Models.Porting.Syniverse.ResultCount();

            XmlDocument xml = new XmlDocument();
            xml.LoadXml(respXml);

            #region Attributes
            XmlNode mainNode = xml.GetElementsByTagName("cancel_entry_resp")[0];
            respObj.RespCode = mainNode.Attributes["resp_code"].InnerText;

            if (mainNode.Attributes.Count > 1)
                respObj.CancelAllInPacRequested = mainNode.Attributes["cancel_all_in_pac_requested"].InnerText == "True" ? true : false;
            else
                respObj.CancelAllInPacRequested = false;
            #endregion

            #region Element
            respObj.Pac = xml.GetElementsByTagName("pac")[0].InnerText;


            respObj.MultipleEntryResult.EntryResultList =
                ReceiveEntryResultListXml(xml.GetElementsByTagName("entry_result_list")[0].OuterXml);

            respObj.MultipleEntryResult.ResultCount.GoodCount = Convert.ToInt32(xml.GetElementsByTagName("good_count")[0].InnerText);
            respObj.MultipleEntryResult.ResultCount.BadCount = Convert.ToInt32(xml.GetElementsByTagName("bad_count")[0].InnerText);

            #endregion

            return respObj;
        }

        static string readFile(string filePath)
        {
            // use relative server path for web application, and use local path for non web application
            if (HttpContext.Current != null)
                filePath = HttpContext.Current.Server.MapPath(filePath);

            if (!File.Exists(filePath))
                return string.Empty;

            try
            {
                StreamReader streamReader = new StreamReader(filePath);
                string text = streamReader.ReadToEnd();
                streamReader.Close();

                return text;
            }
            catch
            {
                return string.Empty;
            }
        }
        public static string CreateCookieXml(string sessionIdString)
        {
            string xml = GetXmlRequest("Cookie");
            xml = xml.Replace("##SessionId##", sessionIdString);
            return IndentXMLString(xml);
        }
        static string GetXmlRequest(string requestName)
        {
            string fullXmlRequest = RequestContentXml;
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(fullXmlRequest);

            string XmlRequestFormat = doc.GetElementsByTagName(requestName).Item(0).InnerXml;
            return XmlRequestFormat;
        }
        static string IndentXMLString(string xml)
        {
            string outXml = string.Empty;
            MemoryStream ms = new MemoryStream();
            // Create a XMLTextWriter that will send its output to a memory stream (file)
            XmlTextWriter xtw = new XmlTextWriter(ms, Encoding.Unicode);
            XmlDocument doc = new XmlDocument();

            try
            {
                // Load the unformatted XML text string into an instance 
                // of the XML Document Object Model (DOM)
                doc.LoadXml(xml);

                // Set the formatting property of the XML Text Writer to indented
                // the text writer is where the indenting will be performed
                xtw.Formatting = Formatting.Indented;

                // write dom xml to the xmltextwriter
                doc.WriteContentTo(xtw);
                // Flush the contents of the text writer
                // to the memory stream, which is simply a memory file
                xtw.Flush();

                // set to start of the memory stream (file)
                ms.Seek(0, SeekOrigin.Begin);
                // create a reader to read the contents of 
                // the memory stream (file)
                StreamReader sr = new StreamReader(ms);
                // return the formatted string to caller
                return sr.ReadToEnd();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                return ex.ToString(); // return original input xml in case it can't be indented
            }
        }
        public static string AddRequestHeader(string xmlContent)
        {
            string RequestHeader = RequestHeaderXml;
            RequestHeader = RequestHeader.Replace("##Content##", xmlContent);
            return IndentXMLString(RequestHeader);
        }
        public static string CreateLogonXml() // assuming the username and password is in the config
        {
            try
            {
                string xml = GetXmlRequest("Logon");
                xml = xml.Replace("##Username##", Config.SyniverseUsername);
                xml = xml.Replace("##Password##", Config.SyniversePassword);
                return IndentXMLString(xml);
            }
            catch (Exception e)
            {
                throw new Exception("CreateLogonXml: " + e.Message);
            }
        }
        public static string send_xml(string xml)
        {
            try
            {
                // Add request header for all outgoing message
                xml = XmlAdapter.AddRequestHeader(xml);
                return send_receive_xml(xml);
            }
            catch (Exception e)
            {
                throw new Exception("send_xml:" + e.Message);
            }
        }
        static RemoteCertificateValidationCallback callback = delegate(object sender, X509Certificate cert, X509Chain chain, SslPolicyErrors sslError)
        {
            try
            {
                X509Certificate2 certv2 = new X509Certificate2(cert);
                // more code here that sends soap request

                return true;
            }
            catch (Exception e)
            {
                throw new Exception("callback: " + e.Message);
            }
        };
        static string send_receive_xml(string xml)
        {
            try
            {
                // Create the Web Request Object
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Config.SyniverseUrl);
                request.ProtocolVersion = HttpVersion.Version10;

                // Specify that you want to POST data
                request.Method = "POST";
                request.ContentType = "text/xml";
                // not sure if credentials is needed or not
                //request.Credentials = new NetworkCredential(ConfigAccessor.SyniverseUsername, ConfigAccessor.SyniversePassword);

                //ServicePointManager.CertificatePolicy = new CertPolicy();
                

                //<SW0001> 
                try
                {
                    var certFile = new X509Certificate2(System.IO.File.ReadAllBytes(CertificateFile), 
                        CertificatePassword, X509KeyStorageFlags.MachineKeySet);
                    request.ClientCertificates.Add(certFile); //.CreateFromCertFile(@tbFileCert.Text));//SW0001 //just add the password
                    //request.ClientCertificates.Add(certv2);
                    ServicePointManager.ServerCertificateValidationCallback += callback;
                    send_request(request, xml);
                }
                catch (System.Net.WebException exc)
                {
                    //return "*** Error while connecting: " + exc.Message;
                    throw new Exception("*** Error while connecting: " + exc.Message);
                }
                catch (Exception e)
                {
                    throw new Exception("SW0001: " + e.StackTrace);
                }

                string resp;

                try
                {
                    resp = read_response(request);
                    resp = resp.Replace(problematicString, "");  // temp solution
                }
                catch (System.Net.WebException exc)
                {
                    WebResponse response = exc.Response;
                    if (response != null)
                    {
                        Stream responseStream = response.GetResponseStream();
                        StreamReader reader = new StreamReader(responseStream);

                        //return "*** Error while reading response: " + exc.Message + "\r\n\r\n" + reader.ReadToEnd();
                        throw new Exception("*** Error while reading response: " + exc.Message + "\r\n\r\n" + reader.ReadToEnd());
                    }
                    else
                    {
                        //return "*** Error while reading response: " + exc.Message + "\r\n\r\n" + "Got null Reponse";
                        throw new Exception("*** Error while reading response: " + exc.Message + "\r\n\r\n" + "Got null Reponse");
                    }
                }

                return resp;
            }
            catch (Exception e)
            {
                throw new Exception("send_receive_xml: " + e.ToString());
            }
        }
        public static string CreateSubmitRequestXml(CRM_API.Models.Porting.Syniverse.SubmitRequest submitObj)
        {
            string xml = GetXmlRequest("SubmitRequest");

            xml = xml.Replace("##ReqCode##", submitObj.ReqCode);
            xml = xml.Replace("##UseSuggestedDateOnConflict##", submitObj.UseSuggestedDateOnConflict ? "True" : "False");
            xml = xml.Replace("##Pac##", submitObj.EntryKey.Pac);
            xml = xml.Replace("##Msisdn##", submitObj.EntryKey.Msisdn);
            xml = xml.Replace("##NoCode##", submitObj.NoCode);
            xml = xml.Replace("##PortDate##", submitObj.PortDate.ToString("yyyyMMdd"));

            #region SpOptionalData, optional
            string sodXml = string.Empty;

            if (submitObj.SpOptionalData != null)
                sodXml = CreateSpOptionalDataXml(submitObj.SpOptionalData);

            xml = xml.Replace("##SpOptionalData##", sodXml);
            #endregion

            return IndentXMLString(xml);
        }

        public static string CreateReviseRequestXml(CRM_API.Models.Porting.Syniverse.ReviseRequest submitObj)
        {
            string xml = GetXmlRequest("SubmitRequest");

            xml = xml.Replace("##ReqCode##", submitObj.ReqCode);
            xml = xml.Replace("##UseSuggestedDateOnConflict##", submitObj.UseSuggestedDateOnConflict ? "True" : "False");
            xml = xml.Replace("##Pac##", submitObj.EntryKey.Pac);
            xml = xml.Replace("##Msisdn##", submitObj.EntryKey.Msisdn);
            xml = xml.Replace("##NoCode##", submitObj.NoCode);
            xml = xml.Replace("##PortDate##", submitObj.PortDate.ToString("yyyyMMdd"));

            #region SpOptionalData, optional
            string sodXml = string.Empty;

            if (submitObj.SpOptionalData != null)
                sodXml = CreateSpOptionalDataXml(submitObj.SpOptionalData);

            xml = xml.Replace("##SpOptionalData##", sodXml);
            #endregion

            return IndentXMLString(xml);
        }

        public static string CreateCancelRequestXml(CRM_API.Models.Porting.Syniverse.CancelRequest cancelObj)
        {
            string xml = GetXmlRequest("CancelRequest");

            xml = xml.Replace("##ReqCode##", cancelObj.ReqCode);
            xml = xml.Replace("##CancelAllInPac##", cancelObj.CancelAllInPac ? "True" : "False");
            xml = xml.Replace("##Pac##", cancelObj.EntryKey.Pac);
            xml = xml.Replace("##Msisdn##", cancelObj.EntryKey.Msisdn);

            return IndentXMLString(xml);
        }

        public static CRM_API.Models.Porting.Syniverse.Error ReceiveErrorXml(string errorXml)
        {
            errorXml = errorXml.Replace(problematicString, "");

            Models.Porting.Syniverse.Error errorObj = new Models.Porting.Syniverse.Error();
            errorObj.Detail = new List<string>();

            XmlDocument xml = new XmlDocument();
            xml.LoadXml(errorXml);

            #region Attributes
            XmlNode mainNode = xml.GetElementsByTagName("error")[0];
            //errorObj.RespCode = mainNode.Attributes["resp_code"].InnerText;
            errorObj.ErrorCode = Convert.ToInt32(mainNode.Attributes["error_code"].InnerText);
            #endregion

            #region Elements
            errorObj.Text = xml.GetElementsByTagName("text")[0].InnerText;

            XmlNodeList detailNodes = xml.GetElementsByTagName("detail");
            Console.WriteLine(detailNodes.Count);
            foreach (XmlNode detailNode in detailNodes)
            {
                errorObj.Detail.Add(detailNode.InnerText);
            }
            #endregion

            return errorObj;
        }
        static string CreateSpOptionalDataXml(CRM_API.Models.Porting.Syniverse.OptionalData optionalData)
        {
            string sodXml = "<sp_optional_data>";

            #region PrivateData, optional
            if (optionalData.PrivateData != null)
            {
                sodXml += "<private_data>";

                if (optionalData.PrivateData.AccNo != null)
                    sodXml += "<acc_no>" + optionalData.PrivateData.AccNo + "</acc_no>";

                if (optionalData.PrivateData.Ref != null)
                    sodXml += "<ref>" + optionalData.PrivateData.Ref + "</ref>";

                if (optionalData.PrivateData.AccNo != null)
                    sodXml += "<spid>" + optionalData.PrivateData.SpId + "</spid>";

                sodXml += "</private_data>";
            }
            #endregion

            #region ContactInfo, optional
            if (optionalData.ContactInfo != null)
            {
                sodXml += "<contact_info>";

                if (optionalData.ContactInfo.ContactName != null)
                    sodXml += "<contact_name>" + optionalData.ContactInfo.ContactName + "</contact_name>";

                if (optionalData.ContactInfo.ContactDetails != null)
                    sodXml += "<contact_details>" + optionalData.ContactInfo.ContactDetails + "</contact_details>";

                sodXml += "</contact_info>";
            }
            #endregion

            sodXml += "</sp_optional_data>";
            return sodXml;
        }
        static string read_response(HttpWebRequest request)
        {
            // 1. Get the Web Response Object from the request
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            // *** Save the cookies on the session
            if (HttpContext.Current != null && response.Cookies.Count > 0)
            {
                //HttpContext.Current.Session.Contents["cookies"] = response.Cookies;
                Helpers.Cookie.CookieList = response.Cookies;
            }

            // 2. Get the Stream Object from the response
            Stream responseStream = response.GetResponseStream();

            // 3. Create a stream reader and associate it with the stream object
            StreamReader reader = new StreamReader(responseStream);

            // 4. read the entire stream
            return reader.ReadToEnd();
        }
        static void send_request(HttpWebRequest request, string data)
        {
            try
            {
                byte[] bytes = null;

                request.CookieContainer = new CookieContainer();

                if (HttpContext.Current != null)
                {
                    //CookieCollection Cookies = (CookieCollection)HttpContext.Current.Session.Contents["cookies"];
                    CookieCollection Cookies = Helpers.Cookie.CookieList;

                    if (Cookies != null && Cookies.Count > 0)
                        request.CookieContainer.Add(Cookies);
                }

                // Get the data that is being posted(or sent) to the server
                bytes = System.Text.Encoding.ASCII.GetBytes(data);
                request.ContentLength = bytes.Length;

                // 1. Get an output stream from the request object
                Stream outputStream = request.GetRequestStream();

                // 2. Post the data out to the stream
                outputStream.Write(bytes, 0, bytes.Length);

                // 3. Close the output stream and send the data out to the web server
                outputStream.Close();
            }
            catch (Exception e)
            {
                throw new Exception("send_request: " + e.Message);
            }
        }
        public static CRM_API.Models.Porting.Syniverse.LogonResp ReceiveLogonRespXml(string respXml)
        {
            try
            {
                CRM_API.Models.Porting.Syniverse.LogonResp respObj = new CRM_API.Models.Porting.Syniverse.LogonResp();

                XmlDocument xml = new XmlDocument();
                xml.LoadXml(respXml);

                #region Elements
                respObj.Cookie = xml.GetElementsByTagName("cookie")[0].InnerText;

                if (xml.GetElementsByTagName("password_expiry_warning").Count > 0)
                {
                    CRM_API.Models.Porting.Syniverse.PasswordExpiryWarning warning = new CRM_API.Models.Porting.Syniverse.PasswordExpiryWarning();
                    warning.ExpiryDate = "The expiry date";
                    respObj.PasswordExpiryWarning = warning;
                }
                #endregion

                return respObj;
            }
            catch (Exception e)
            {
                throw new Exception("ReceiveLogonRespXml: " + e.Message);
            }
        }
        public static CRM_API.Models.Porting.Syniverse.ReportResp ReceiveReportRespXml(string respXml)
        {
            Models.Porting.Syniverse.ReportResp respObj = new Models.Porting.Syniverse.ReportResp();
            respObj.ReportRecord = new List<Models.Porting.Syniverse.ReportRecord>();

            XmlDocument xml = new XmlDocument();
            xml.LoadXml(respXml);

            #region Attributes
            XmlNode mainNode = xml.GetElementsByTagName("report_resp")[0];
            respObj.RespCode = mainNode.Attributes["resp_code"].InnerText;
            #endregion

            #region Element

            for (int count = 0; count < xml.GetElementsByTagName("report_record").Count; count++)
            {
                string reportRecordXml = xml.GetElementsByTagName("report_record")[count].OuterXml;

                Models.Porting.Syniverse.ReportRecord rcObj = ReceiveReportRecordXml(reportRecordXml);
                respObj.ReportRecord.Add(rcObj);
            }
            #endregion

            return respObj;
        }
        static CRM_API.Models.Porting.Syniverse.ReportRecord ReceiveReportRecordXml(string respXml)
        {
            CRM_API.Models.Porting.Syniverse.ReportRecord respObj = new CRM_API.Models.Porting.Syniverse.ReportRecord();

            XmlDocument xml = new XmlDocument();
            xml.LoadXml(respXml);

            #region Optional Data

            if (xml.GetElementsByTagName("pac").Count > 0)
                respObj.Pac = xml.GetElementsByTagName("pac")[0].InnerText;

            if (xml.GetElementsByTagName("msisdn").Count > 0)
                respObj.Msisdn = xml.GetElementsByTagName("msisdn")[0].InnerText;

            if (xml.GetElementsByTagName("pac_expiry_days").Count > 0)
                respObj.PacExpiryDays = Convert.ToInt32(xml.GetElementsByTagName("pac_expiry_days")[0].InnerText);

            if (xml.GetElementsByTagName("status").Count > 0)
                respObj.Status = (CRM_API.Models.Porting.Syniverse.Status)Enum.Parse(typeof(CRM_API.Models.Porting.Syniverse.Status), xml.GetElementsByTagName("status")[0].InnerXml.Replace("<", "").Replace("/>", ""), true);

            if (xml.GetElementsByTagName("is_bulk").Count > 0)
                respObj.IsBulk = (CRM_API.Models.Porting.Syniverse.IsBulk)Enum.Parse(typeof(CRM_API.Models.Porting.Syniverse.IsBulk), xml.GetElementsByTagName("is_bulk")[0].InnerXml.Replace("<", "").Replace("/>", ""), true);

            if (xml.GetElementsByTagName("entry_count").Count > 0)
                respObj.EntryCount = Convert.ToInt32(xml.GetElementsByTagName("entry_count")[0].InnerText);

            if (xml.GetElementsByTagName("primary_msisdn").Count > 0)
            {
                respObj.PrimaryMsisdn = new CRM_API.Models.Porting.Syniverse.PrimaryMsisdn();
                respObj.PrimaryMsisdn.Msisdn = xml.GetElementsByTagName("primary_msisdn")[0].InnerText;
            }

            if (xml.GetElementsByTagName("msisdn_type").Count > 0)
                respObj.MsisdnType = (CRM_API.Models.Porting.Syniverse.MsisdnType)Enum.Parse(typeof(CRM_API.Models.Porting.Syniverse.MsisdnType), xml.GetElementsByTagName("msisdn_type")[0].InnerXml.Replace("<", "").Replace("/>", ""), true);

            if (xml.GetElementsByTagName("transfer_type").Count > 0)
                respObj.TransferType = (CRM_API.Models.Porting.Syniverse.TransferType)Enum.Parse(typeof(CRM_API.Models.Porting.Syniverse.TransferType), xml.GetElementsByTagName("transfer_type")[0].InnerXml.Replace("<", "").Replace("/>", ""), true);

            if (xml.GetElementsByTagName("port_days").Count > 0)
                respObj.PortDays = xml.GetElementsByTagName("port_days")[0].InnerText;

            if (xml.GetElementsByTagName("port_date").Count > 0 && xml.GetElementsByTagName("port_date")[0].InnerText.Length > 0)
                respObj.PortDate = DateTime.ParseExact(xml.GetElementsByTagName("port_date")[0].InnerText, "yyyyMMdd", null);
            // note: might be error and need to use DateTime.ParseExact

            if (xml.GetElementsByTagName("pac_age").Count > 0)
                respObj.PacAge = Convert.ToInt32(xml.GetElementsByTagName("pac_age")[0].InnerText);

            if (xml.GetElementsByTagName("is_tagged").Count > 0)
                respObj.IsTagged = (CRM_API.Models.Porting.Syniverse.TaggedSelector)Enum.Parse(typeof(CRM_API.Models.Porting.Syniverse.TaggedSelector), xml.GetElementsByTagName("is_tagged")[0].InnerXml.Replace("<", "").Replace("/>", ""), true);

            #endregion

            #region Optional Dsp Data

            if (xml.GetElementsByTagName("dsp_contact_name").Count > 0)
                respObj.DspContactName = xml.GetElementsByTagName("dsp_contact_name")[0].InnerText;

            if (xml.GetElementsByTagName("dsp_contact_details").Count > 0)
                respObj.DspContactDetails = xml.GetElementsByTagName("dsp_contact_details")[0].InnerText;

            if (xml.GetElementsByTagName("dsp_code").Count > 0)
                respObj.DspCode = xml.GetElementsByTagName("dsp_code")[0].InnerText;

            if (xml.GetElementsByTagName("dno_code").Count > 0)
                respObj.DnoCode = xml.GetElementsByTagName("dno_code")[0].InnerText;

            if (xml.GetElementsByTagName("dsp_acc_no").Count > 0)
                respObj.DspAccNo = xml.GetElementsByTagName("dsp_acc_no")[0].InnerText;

            if (xml.GetElementsByTagName("dsp_ref").Count > 0)
                respObj.DspRef = xml.GetElementsByTagName("dsp_ref")[0].InnerText;

            if (xml.GetElementsByTagName("dsp_name").Count > 0)
                respObj.DspName = xml.GetElementsByTagName("dsp_name")[0].InnerText;

            if (xml.GetElementsByTagName("dno_name").Count > 0)
                respObj.DnoName = xml.GetElementsByTagName("dno_name")[0].InnerText;

            #endregion

            #region Optional Rsp Data

            if (xml.GetElementsByTagName("rsp_contact_name").Count > 0)
                respObj.RspContactName = xml.GetElementsByTagName("rsp_contact_name")[0].InnerText;

            if (xml.GetElementsByTagName("rsp_contact_details").Count > 0)
                respObj.RspContactDetails = xml.GetElementsByTagName("rsp_contact_details")[0].InnerText;

            if (xml.GetElementsByTagName("rsp_code").Count > 0)
                respObj.RspCode = xml.GetElementsByTagName("rsp_code")[0].InnerText;

            if (xml.GetElementsByTagName("rno_code").Count > 0)
                respObj.RnoCode = xml.GetElementsByTagName("rno_code")[0].InnerText;

            if (xml.GetElementsByTagName("rsp_acc_no").Count > 0)
                respObj.RspAccNo = xml.GetElementsByTagName("rsp_acc_no")[0].InnerText;

            if (xml.GetElementsByTagName("rsp_ref").Count > 0)
                respObj.RspRef = xml.GetElementsByTagName("rsp_ref")[0].InnerText;

            if (xml.GetElementsByTagName("rsp_name").Count > 0)
                respObj.RspName = xml.GetElementsByTagName("rsp_name")[0].InnerText;

            if (xml.GetElementsByTagName("rno_name").Count > 0)
                respObj.RnoName = xml.GetElementsByTagName("rno_name")[0].InnerText;

            #endregion

            return respObj;
        }
        public static string send_report_xml(string xml)
        {
            // Add request header for all outgoing message
            xml = XmlAdapter.AddReportRequestHeader(xml);
            return send_receive_xml(xml);
        }
        public static string AddReportRequestHeader(string xmlContent)
        {
            string ReportRequestHeader = ReportRequestHeaderXml;
            ReportRequestHeader = ReportRequestHeader.Replace("##Content##", xmlContent);
            return IndentXMLString(ReportRequestHeader);
        }

        public static string GeneratePRXml(Models.Porting.NLSubmitEntry In,string msgidentifier,string sender,string receiver,string NoOfItems,string PortingReqItems,string sXMLDir)
        {
            string Msgtype = "porting_request";

            XmlDocument doc = new XmlDocument();
            XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.AppendChild(docNode);

            XmlNode msgNode = doc.CreateElement("message");

            XmlAttribute msgAttribute1 = doc.CreateAttribute("xsi", "noNamespaceSchemaLocation", "http://www.w3.org/2001/XMLSchema-instance");
            msgAttribute1.Value = Msgtype + ".xsd";
            msgNode.Attributes.Append(msgAttribute1);
            doc.AppendChild(msgNode);

            XmlNode msgHeaderNode = doc.CreateElement("messageheader");
            msgNode.AppendChild(msgHeaderNode);

            XmlNode processNode = doc.CreateElement("process");
            processNode.AppendChild(doc.CreateTextNode("voice"));
            msgHeaderNode.AppendChild(processNode);

            XmlNode pcsversionNode = doc.CreateElement("pcsversion");
            pcsversionNode.AppendChild(doc.CreateTextNode("1.0"));
            msgHeaderNode.AppendChild(pcsversionNode);

            XmlNode msgtypeNode = doc.CreateElement("msgtype");
            msgtypeNode.AppendChild(doc.CreateTextNode(Msgtype));
            msgHeaderNode.AppendChild(msgtypeNode);

            XmlNode msgversionNode = doc.CreateElement("msgversion");
            msgversionNode.AppendChild(doc.CreateTextNode("1.0"));
            msgHeaderNode.AppendChild(msgversionNode);

            XmlNode msgidentifierNode = doc.CreateElement("msgidentifier");
            msgidentifierNode.AppendChild(doc.CreateTextNode(msgidentifier));
            msgHeaderNode.AppendChild(msgidentifierNode);

            XmlNode errorcodeNode = doc.CreateElement("errorcode");
            errorcodeNode.AppendChild(doc.CreateTextNode("0"));
            msgHeaderNode.AppendChild(errorcodeNode);

            XmlNode errorcodedescNode = doc.CreateElement("errorcodedescription");
            errorcodedescNode.AppendChild(doc.CreateTextNode("OK"));
            msgHeaderNode.AppendChild(errorcodedescNode);

            XmlNode senderNode = doc.CreateElement("sender");
            senderNode.AppendChild(doc.CreateTextNode(sender));
            msgHeaderNode.AppendChild(senderNode);

            XmlNode receiverNode = doc.CreateElement("receiver");
            receiverNode.AppendChild(doc.CreateTextNode(receiver));
            msgHeaderNode.AppendChild(receiverNode);

            XmlNode msgdatetimeNode = doc.CreateElement("msgdatetime");
            msgdatetimeNode.AppendChild(doc.CreateTextNode(DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss")));
            msgHeaderNode.AppendChild(msgdatetimeNode);

            XmlNode msgBodyNode = doc.CreateElement("messagebody");
            msgNode.AppendChild(msgBodyNode);

            XmlNode portingidNode = doc.CreateElement("portingid");
            portingidNode.AppendChild(doc.CreateTextNode(msgidentifier));
            msgBodyNode.AppendChild(portingidNode);

            XmlNode rnoNode = doc.CreateElement("recipientnetworkoperator");
            rnoNode.AppendChild(doc.CreateTextNode(In.Rno));
            msgBodyNode.AppendChild(rnoNode);

            XmlNode dnoNode = doc.CreateElement("donornetworkoperator");
            dnoNode.AppendChild(doc.CreateTextNode(In.Dno));
            msgBodyNode.AppendChild(dnoNode);

            XmlNode rspNode = doc.CreateElement("recipientserviceprovider");
            rspNode.AppendChild(doc.CreateTextNode(In.Rsp));
            msgBodyNode.AppendChild(rspNode);

            XmlNode dspNode = doc.CreateElement("donorserviceprovider");
            dspNode.AppendChild(doc.CreateTextNode(In.Dsp));
            msgBodyNode.AppendChild(dspNode);

            XmlNode typeofnumbersNode = doc.CreateElement("typeofnumbers");
            typeofnumbersNode.AppendChild(doc.CreateTextNode("mobile"));
            msgBodyNode.AppendChild(typeofnumbersNode);

            string reqdate = In.RequestDate_Decimal.ToString();
            string year = "", month = "", day = "", hour = "", minute = "", second = "";
            if (reqdate.Length == 14)
            {
                year = reqdate.Substring(0, 4);
                month = reqdate.Substring(4, 2);
                day = reqdate.Substring(6, 2);
                hour = reqdate.Substring(8, 2);
                minute = reqdate.Substring(10, 2);
                second = reqdate.Substring(12, 2);
            }

            XmlNode requesteddateNode = doc.CreateElement("requesteddate");
            requesteddateNode.AppendChild(doc.CreateTextNode(year + "-" + month + "-" + day + "T" + hour + ":" + minute + ":" + second));
            msgBodyNode.AppendChild(requesteddateNode);

            if (!string.IsNullOrEmpty(In.CustID))
            {
                XmlNode customeridNode = doc.CreateElement("customerid");
                customeridNode.AppendChild(doc.CreateTextNode(In.CustID.ToString()));
                msgBodyNode.AppendChild(customeridNode);
            }

            XmlNode initialsNode = doc.CreateElement("initials");
            initialsNode.AppendChild(doc.CreateTextNode(In.Initials));
            msgBodyNode.AppendChild(initialsNode);

            XmlNode prefixNode = doc.CreateElement("prefix");
            prefixNode.AppendChild(doc.CreateTextNode(In.Prefix));
            msgBodyNode.AppendChild(prefixNode);

            XmlNode lastnameNode = doc.CreateElement("lastname");
            lastnameNode.AppendChild(doc.CreateTextNode(In.ContactName));
            msgBodyNode.AppendChild(lastnameNode);

            XmlNode companynameNode = doc.CreateElement("companyname");
            companynameNode.AppendChild(doc.CreateTextNode(In.CompanyName));
            msgBodyNode.AppendChild(companynameNode);

            XmlNode streetNode = doc.CreateElement("street");
            streetNode.AppendChild(doc.CreateTextNode(In.Street));
            msgBodyNode.AppendChild(streetNode);

            XmlNode housenumberNode = doc.CreateElement("housenumber");
            housenumberNode.AppendChild(doc.CreateTextNode(In.HouseNo));
            msgBodyNode.AppendChild(housenumberNode);

            XmlNode housenumberextNode = doc.CreateElement("housenumberext");
            housenumberextNode.AppendChild(doc.CreateTextNode(In.HouseNoExt));
            msgBodyNode.AppendChild(housenumberextNode);

            XmlNode zipcodeNode = doc.CreateElement("zipcode");
            zipcodeNode.AppendChild(doc.CreateTextNode(In.ZipCode));
            msgBodyNode.AppendChild(zipcodeNode);

            XmlNode cityNode = doc.CreateElement("city");
            cityNode.AppendChild(doc.CreateTextNode(In.City));
            msgBodyNode.AppendChild(cityNode);

            if (!string.IsNullOrEmpty(In.NoteOfMsg))
            {
                XmlNode noteNode = doc.CreateElement("note");
                noteNode.AppendChild(doc.CreateTextNode(In.NoteOfMsg));
                msgBodyNode.AppendChild(noteNode);
            }

            XmlNode emailNode = doc.CreateElement("email");
            emailNode.AppendChild(doc.CreateTextNode(In.Email));
            msgBodyNode.AppendChild(emailNode);
            /*
            XmlNode iccNode = doc.CreateElement("simcardnumber");
            iccNode.AppendChild(doc.CreateTextNode(ICCID));
            msgBodyNode.AppendChild(iccNode);
            */
            XmlNode numberofitemsNode = doc.CreateElement("numberofitems");
            numberofitemsNode.AppendChild(doc.CreateTextNode(NoOfItems.ToString()));
            msgBodyNode.AppendChild(numberofitemsNode);

            XmlNode itemNode = doc.CreateElement("item");
            msgBodyNode.AppendChild(itemNode);

            XmlNode portingrequestitemNode = doc.CreateElement("portingrequestitem");
            portingrequestitemNode.AppendChild(doc.CreateTextNode(PortingReqItems.ToString()));
            itemNode.AppendChild(portingrequestitemNode);

            XmlNode telpnoseriestartNode = doc.CreateElement("telephonenumberseriestart");
            telpnoseriestartNode.AppendChild(doc.CreateTextNode(In.TelpNo));
            itemNode.AppendChild(telpnoseriestartNode);

            XmlNode telpnoserieendNode = doc.CreateElement("telephonenumberserieend");
            telpnoserieendNode.AppendChild(doc.CreateTextNode(In.TelpNo));
            itemNode.AppendChild(telpnoserieendNode);

            /* <r by SW0001
             XmlNode iccNode = doc.CreateElement("icc");
             iccNode.AppendChild(doc.CreateTextNode(ICCID));
             itemNode.AppendChild(iccNode);

             * XmlNode optionsNode = doc.CreateElement("options");
             * optionsNode.AppendChild(doc.CreateTextNode(Options));
             * itemNode.AppendChild(optionsNode);
             * SW0001>
            */

            /*string FileName = sXMLDir + Msgtype + " " + DateTime.Now.ToString("yyyy-MM-dd HH.mm.ss") + ".xml";

            doc.Save(FileName);*/

            StringWriter sw = new StringWriter();
            XmlTextWriter xw = new XmlTextWriter(sw);
            doc.WriteTo(xw);

            return sw.ToString();
        }

        public static string GeneratePortingPerformedPRXml(Models.Porting.NLSubmitEntry In, string msgidentifier, string sender, string receiver, string NoOfItems, string PortingReqItems, string sXMLDir)
        {
            string Msgtype = "porting_performed";

            XmlDocument doc = new XmlDocument();
            XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.AppendChild(docNode);

            XmlNode msgNode = doc.CreateElement("message");

            XmlAttribute msgAttribute1 = doc.CreateAttribute("xsi", "noNamespaceSchemaLocation", "http://www.w3.org/2001/XMLSchema-instance");
            msgAttribute1.Value = Msgtype + ".xsd";
            msgNode.Attributes.Append(msgAttribute1);
            doc.AppendChild(msgNode);

            XmlNode msgHeaderNode = doc.CreateElement("messageheader");
            msgNode.AppendChild(msgHeaderNode);

            XmlNode processNode = doc.CreateElement("process");
            processNode.AppendChild(doc.CreateTextNode("voice"));
            msgHeaderNode.AppendChild(processNode);

            XmlNode pcsversionNode = doc.CreateElement("pcsversion");
            pcsversionNode.AppendChild(doc.CreateTextNode("1.0"));
            msgHeaderNode.AppendChild(pcsversionNode);

            XmlNode msgtypeNode = doc.CreateElement("msgtype");
            msgtypeNode.AppendChild(doc.CreateTextNode(Msgtype));
            msgHeaderNode.AppendChild(msgtypeNode);

            XmlNode msgversionNode = doc.CreateElement("msgversion");
            msgversionNode.AppendChild(doc.CreateTextNode("1.0"));
            msgHeaderNode.AppendChild(msgversionNode);

            XmlNode msgidentifierNode = doc.CreateElement("msgidentifier");
            msgidentifierNode.AppendChild(doc.CreateTextNode(msgidentifier));
            msgHeaderNode.AppendChild(msgidentifierNode);

            XmlNode errorcodeNode = doc.CreateElement("errorcode");
            errorcodeNode.AppendChild(doc.CreateTextNode("0"));
            msgHeaderNode.AppendChild(errorcodeNode);

            XmlNode errorcodedescNode = doc.CreateElement("errorcodedescription");
            errorcodedescNode.AppendChild(doc.CreateTextNode("OK"));
            msgHeaderNode.AppendChild(errorcodedescNode);

            XmlNode senderNode = doc.CreateElement("sender");
            senderNode.AppendChild(doc.CreateTextNode(sender));
            msgHeaderNode.AppendChild(senderNode);

            XmlNode receiverNode = doc.CreateElement("receiver");
            receiverNode.AppendChild(doc.CreateTextNode(receiver));
            msgHeaderNode.AppendChild(receiverNode);

            XmlNode msgdatetimeNode = doc.CreateElement("msgdatetime");
            msgdatetimeNode.AppendChild(doc.CreateTextNode(DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss")));
            msgHeaderNode.AppendChild(msgdatetimeNode);

            XmlNode msgBodyNode = doc.CreateElement("messagebody");
            msgNode.AppendChild(msgBodyNode);

            XmlNode rnoNode = doc.CreateElement("recipientnetworkoperator");
            rnoNode.AppendChild(doc.CreateTextNode(In.Rno));
            msgBodyNode.AppendChild(rnoNode);

            XmlNode dnoNode = doc.CreateElement("donornetworkoperator");
            dnoNode.AppendChild(doc.CreateTextNode(In.Dno));
            msgBodyNode.AppendChild(dnoNode);

            XmlNode rspNode = doc.CreateElement("recipientserviceprovider");
            rspNode.AppendChild(doc.CreateTextNode(In.Rsp));
            msgBodyNode.AppendChild(rspNode);

            XmlNode dspNode = doc.CreateElement("donorserviceprovider");
            dspNode.AppendChild(doc.CreateTextNode(In.Dsp));
            msgBodyNode.AppendChild(dspNode);

            XmlNode typeofnumbersNode = doc.CreateElement("typeofnumbers");
            typeofnumbersNode.AppendChild(doc.CreateTextNode("mobile"));
            msgBodyNode.AppendChild(typeofnumbersNode);

            string reqdate = In.RequestDate_Decimal.ToString();
            string year = "", month = "", day = "", hour = "", minute = "", second = "";
            if (reqdate.Length == 14)
            {
                year = reqdate.Substring(0, 4);
                month = reqdate.Substring(4, 2);
                day = reqdate.Substring(6, 2);
                hour = reqdate.Substring(8, 2);
                minute = reqdate.Substring(10, 2);
                second = reqdate.Substring(12, 2);
            }

            XmlNode requesteddateNode = doc.CreateElement("actualdate");
            requesteddateNode.AppendChild(doc.CreateTextNode(year + "-" + month + "-" + day + "T" + hour + ":" + minute + ":" + second));
            msgBodyNode.AppendChild(requesteddateNode);

            XmlNode numberofitemsNode = doc.CreateElement("numberofitems");
            numberofitemsNode.AppendChild(doc.CreateTextNode(NoOfItems.ToString()));
            msgBodyNode.AppendChild(numberofitemsNode);

            XmlNode itemNode = doc.CreateElement("item");
            msgBodyNode.AppendChild(itemNode);

            XmlNode portingrequestitemNode = doc.CreateElement("portingrequestitem");
            portingrequestitemNode.AppendChild(doc.CreateTextNode(PortingReqItems.ToString()));
            itemNode.AppendChild(portingrequestitemNode);

            XmlNode telpnoseriestartNode = doc.CreateElement("telephonenumberseriestart");
            telpnoseriestartNode.AppendChild(doc.CreateTextNode(In.TelpNo));
            itemNode.AppendChild(telpnoseriestartNode);

            XmlNode telpnoserieendNode = doc.CreateElement("telephonenumberserieend");
            telpnoserieendNode.AppendChild(doc.CreateTextNode(In.TelpNo));
            itemNode.AppendChild(telpnoserieendNode);

            /* <r by SW0001
             XmlNode iccNode = doc.CreateElement("icc");
             iccNode.AppendChild(doc.CreateTextNode(ICCID));
             itemNode.AppendChild(iccNode);

             * XmlNode optionsNode = doc.CreateElement("options");
             * optionsNode.AppendChild(doc.CreateTextNode(Options));
             * itemNode.AppendChild(optionsNode);
             * SW0001>
            */

            /*string FileName = sXMLDir + Msgtype + " " + DateTime.Now.ToString("yyyy-MM-dd HH.mm.ss") + ".xml";

            doc.Save(FileName);*/

            StringWriter sw = new StringWriter();
            XmlTextWriter xw = new XmlTextWriter(sw);
            doc.WriteTo(xw);

            return sw.ToString();
        }

        public static string GenerateCancelPortingPRXml(Models.Porting.NLSubmitEntry In, string msgidentifier, string sender, string receiver, string sXMLDir)
        {
            string Msgtype = "cancel";

            XmlDocument doc = new XmlDocument();
            XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.AppendChild(docNode);

            XmlNode msgNode = doc.CreateElement("message");

            XmlAttribute msgAttribute1 = doc.CreateAttribute("xsi", "noNamespaceSchemaLocation", "http://www.w3.org/2001/XMLSchema-instance");
            msgAttribute1.Value = Msgtype + ".xsd";
            msgNode.Attributes.Append(msgAttribute1);
            doc.AppendChild(msgNode);

            XmlNode msgHeaderNode = doc.CreateElement("messageheader");
            msgNode.AppendChild(msgHeaderNode);

            XmlNode processNode = doc.CreateElement("process");
            processNode.AppendChild(doc.CreateTextNode("voice"));
            msgHeaderNode.AppendChild(processNode);

            XmlNode pcsversionNode = doc.CreateElement("pcsversion");
            pcsversionNode.AppendChild(doc.CreateTextNode("1.0"));
            msgHeaderNode.AppendChild(pcsversionNode);

            XmlNode msgtypeNode = doc.CreateElement("msgtype");
            msgtypeNode.AppendChild(doc.CreateTextNode(Msgtype));
            msgHeaderNode.AppendChild(msgtypeNode);

            XmlNode msgversionNode = doc.CreateElement("msgversion");
            msgversionNode.AppendChild(doc.CreateTextNode("1.0"));
            msgHeaderNode.AppendChild(msgversionNode);

            XmlNode msgidentifierNode = doc.CreateElement("msgidentifier");
            msgidentifierNode.AppendChild(doc.CreateTextNode(msgidentifier));
            msgHeaderNode.AppendChild(msgidentifierNode);

            XmlNode errorcodeNode = doc.CreateElement("errorcode");
            errorcodeNode.AppendChild(doc.CreateTextNode("0"));
            msgHeaderNode.AppendChild(errorcodeNode);

            XmlNode errorcodedescNode = doc.CreateElement("errorcodedescription");
            errorcodedescNode.AppendChild(doc.CreateTextNode("OK"));
            msgHeaderNode.AppendChild(errorcodedescNode);

            XmlNode senderNode = doc.CreateElement("sender");
            senderNode.AppendChild(doc.CreateTextNode(sender));
            msgHeaderNode.AppendChild(senderNode);

            XmlNode receiverNode = doc.CreateElement("receiver");
            receiverNode.AppendChild(doc.CreateTextNode(receiver));
            msgHeaderNode.AppendChild(receiverNode);

            XmlNode msgdatetimeNode = doc.CreateElement("msgdatetime");
            msgdatetimeNode.AppendChild(doc.CreateTextNode(DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss")));
            msgHeaderNode.AppendChild(msgdatetimeNode);

            XmlNode msgBodyNode = doc.CreateElement("messagebody");
            msgNode.AppendChild(msgBodyNode);

            XmlNode noteNode = doc.CreateElement("note");
            noteNode.AppendChild(doc.CreateTextNode(In.NoteOfMsg));
            msgBodyNode.AppendChild(noteNode);


            /*string FileName = sXMLDir + Msgtype + " " + DateTime.Now.ToString("yyyy-MM-dd HH.mm.ss") + ".xml";

            doc.Save(FileName);*/

            StringWriter sw = new StringWriter();
            XmlTextWriter xw = new XmlTextWriter(sw);
            doc.WriteTo(xw);

            return sw.ToString();
        }

        public static string GeneratePortingEditPRXml(Models.Porting.NLSubmitEntry In, string msgidentifier, string sender, string receiver, string sXMLDir)
        {
            string Msgtype = "change";

            XmlDocument doc = new XmlDocument();
            XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.AppendChild(docNode);

            XmlNode msgNode = doc.CreateElement("message");

            XmlAttribute msgAttribute1 = doc.CreateAttribute("xsi", "noNamespaceSchemaLocation", "http://www.w3.org/2001/XMLSchema-instance");
            msgAttribute1.Value = Msgtype + ".xsd";
            msgNode.Attributes.Append(msgAttribute1);
            doc.AppendChild(msgNode);

            XmlNode msgHeaderNode = doc.CreateElement("messageheader");
            msgNode.AppendChild(msgHeaderNode);

            XmlNode processNode = doc.CreateElement("process");
            processNode.AppendChild(doc.CreateTextNode("voice"));
            msgHeaderNode.AppendChild(processNode);

            XmlNode pcsversionNode = doc.CreateElement("pcsversion");
            pcsversionNode.AppendChild(doc.CreateTextNode("1.0"));
            msgHeaderNode.AppendChild(pcsversionNode);

            XmlNode msgtypeNode = doc.CreateElement("msgtype");
            msgtypeNode.AppendChild(doc.CreateTextNode(Msgtype));
            msgHeaderNode.AppendChild(msgtypeNode);

            XmlNode msgversionNode = doc.CreateElement("msgversion");
            msgversionNode.AppendChild(doc.CreateTextNode("1.0"));
            msgHeaderNode.AppendChild(msgversionNode);

            XmlNode msgidentifierNode = doc.CreateElement("msgidentifier");
            msgidentifierNode.AppendChild(doc.CreateTextNode(msgidentifier));
            msgHeaderNode.AppendChild(msgidentifierNode);

            XmlNode errorcodeNode = doc.CreateElement("errorcode");
            errorcodeNode.AppendChild(doc.CreateTextNode("0"));
            msgHeaderNode.AppendChild(errorcodeNode);

            XmlNode errorcodedescNode = doc.CreateElement("errorcodedescription");
            errorcodedescNode.AppendChild(doc.CreateTextNode("OK"));
            msgHeaderNode.AppendChild(errorcodedescNode);

            XmlNode senderNode = doc.CreateElement("sender");
            senderNode.AppendChild(doc.CreateTextNode(sender));
            msgHeaderNode.AppendChild(senderNode);

            XmlNode receiverNode = doc.CreateElement("receiver");
            receiverNode.AppendChild(doc.CreateTextNode(receiver));
            msgHeaderNode.AppendChild(receiverNode);

            XmlNode msgdatetimeNode = doc.CreateElement("msgdatetime");
            msgdatetimeNode.AppendChild(doc.CreateTextNode(DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss")));
            msgHeaderNode.AppendChild(msgdatetimeNode);

            XmlNode msgBodyNode = doc.CreateElement("messagebody");
            msgNode.AppendChild(msgBodyNode);

            string reqdate = In.RequestDate_Decimal.ToString();
            reqdate += reqdate.ToString().Substring(4, 2) + "-";
            reqdate += reqdate.ToString().Substring(6, 2) + "T";
            reqdate += reqdate.ToString().Substring(8, 2) + ":"; ;
            reqdate += reqdate.ToString().Substring(10, 2) + ":";
            reqdate += reqdate.ToString().Substring(12, 2);

            XmlNode requesteddateNode = doc.CreateElement("changedate");
            requesteddateNode.AppendChild(doc.CreateTextNode(reqdate));
            msgBodyNode.AppendChild(requesteddateNode);

            XmlNode noteNode = doc.CreateElement("note");
            noteNode.AppendChild(doc.CreateTextNode(In.NoteOfMsg));
            msgBodyNode.AppendChild(noteNode);


            /*string FileName = sXMLDir + Msgtype + " " + DateTime.Now.ToString("yyyy-MM-dd HH.mm.ss") + ".xml";

            doc.Save(FileName);*/

            StringWriter sw = new StringWriter();
            XmlTextWriter xw = new XmlTextWriter(sw);
            doc.WriteTo(xw);

            return sw.ToString();
        }

        public static string ServiceNLPorting(Models.Porting.NLSubmitEntry In, string msgidentifier, string sXMLDir,string PortinState)
        {
            string ResponseFile = string.Empty;
            string XmlString = string.Empty;
            string StatusAck = string.Empty;
            string Param = string.Empty;

            if (PortinState == "submit")
            { 
                Param = GeneratePRXml(In, msgidentifier, "BMNL", "PTXS", "1", "1", sXMLDir);
            }
            else if (PortinState == "perform")
            {
                Param = GeneratePortingPerformedPRXml(In, msgidentifier, "BMNL", "PTXS", "1", "1", sXMLDir);
            }
            else if (PortinState == "cancel")
            {
                Param = GenerateCancelPortingPRXml(In, In.MsgIdentifier, "BMNL", "PTXS", sXMLDir);
            }
            else if (PortinState == "edit")
            {
                Param = GeneratePortingEditPRXml(In, In.MsgIdentifier, "BMNL", "PTXS", sXMLDir);
            }
            else
            {
                Param = "";
            }
            /*CRM_API.Helpers.NLService.ServiceSoapClient srv = new CRM_API.Helpers.NLService.ServiceSoapClient("ServiceSoap");
            XmlString=srv.XMLSend(Param);*/

            using (WebClient client = new WebClient())
            {
                NameValueCollection fields = new NameValueCollection();
                fields.Add("xml", Param);
                byte[] respBytes = client.UploadValues("http://mnp-nl.mundio.com/portingxs/Service.asmx/XMLSend",fields);
                string resp = client.Encoding.GetString(respBytes);
                XmlString = resp.Replace("&lt;", "<");
                XmlString = XmlString.Replace("&gt;", ">");
            }

            
            if (XmlString.Contains("nack.xsd") || XmlString.Contains("<msgtype>nack</msgtype>"))
            {
                StatusAck = "2";
                //ResponseFile = sXMLDir + "nack PR " + DateTime.Now.ToString("dd-MM-yyyy HH.mm.ss") + ".xml";
            }
            else if (XmlString.Contains("ack.xsd") || XmlString.Contains("<msgtype>ack</msgtype>"))
            {
                StatusAck = "1";
               // ResponseFile = sXMLDir + "ack PR " + DateTime.Now.ToString("dd-MM-yyyy HH.mm.ss") + ".xml";
            }
            else
            {
                StatusAck = "0";
            }

            //File.WriteAllText(ResponseFile, XmlString);

            return StatusAck;
        }
    }
}
