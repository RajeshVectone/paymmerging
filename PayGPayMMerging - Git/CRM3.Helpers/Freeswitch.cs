﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Configuration;
using System.IO;
using System.Text;

namespace CRM_API.Helpers.Freeswitch
{
    public class FreeswitchWS
    {
        static string URL = ConfigurationManager.AppSettings["FreeswitchWSUrl"];
        static string username = ConfigurationManager.AppSettings["FreeswitchWSUserName"];
        static string password = ConfigurationManager.AppSettings["FreeswitchWSPwd"];

        public static string ExtCallExt(string destExt, string origExt)
        {
            string ReqTemplate = "originate?user/{0}@3022.vbiz.mundio.com &bridge(user/{1}@3022.vbiz.mundio.com)";
            try
            {
                string command = string.Format(ReqTemplate, destExt, origExt);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL + command);
                string credentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(username + ":" + password));
                request.Headers.Add("Authorization", "Basic " + credentials);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream stream = response.GetResponseStream();
                StreamReader reader = new StreamReader(stream);
                string data = reader.ReadToEnd();
                reader.Close();
                stream.Close();
                return data;
            }
            catch (Exception e)
            {
                throw new Exception("FreeswitchWS.ExtCallExt" + e.Message);
            }

            //return null;
        }
        public static string ExtCallMobile(string destMobile, string origExt)
        {
            string ReqTemplate = "originate?user/{0}@3022.vbiz.mundio.com &bridge(sofia/external/03022{0}00{1}@10.30.3.27)";
            //http://82.113.74.20:8080/webapi/originate?user/414@3022.vbiz.mundio.com%20&bridge(sofia/external/0302241400447574412151@10.30.3.27);
            //http://82.113.74.20:8080/webapi/originate?user/447574412151@3022.vbiz.mundio.com &bridge(sofia/external/0302244757441215100414@10.30.3.27)
            try
            {
                string command = string.Format(ReqTemplate, origExt,destMobile);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL + command);
                string credentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(username + ":" + password));
                request.Headers.Add("Authorization", "Basic " + credentials);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream stream = response.GetResponseStream();
                StreamReader reader = new StreamReader(stream);
                string data = reader.ReadToEnd();
                reader.Close();
                stream.Close();
                return data;
            }
            catch (Exception e)
            {
                throw new Exception("FreeswitchWS.ExtCallMobile" + e.Message);
            }

            //return null;
        }
    }
}
