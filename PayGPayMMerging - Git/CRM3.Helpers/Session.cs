﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace CRM_API.Helpers
{
    /// <summary>
    /// Use this helper class to avoid dynamic typing using session dictionary!
    /// </summary>
    public class Cookie
    {
        public static string Token
        {
            get
            {
                try
                {
                    return System.Web.HttpContext.Current.Request.Cookies["token"].Value;
                }
                catch { /* do nothing */ }

                return "";
            }
            set
            {
                System.Web.HttpContext.Current.Response.SetCookie(new HttpCookie("token", value));
            }
        }
        public static CookieCollection CookieList { get; set; }
    }

    /// <summary>
    /// Use this helper to maintain the sensitive data in server
    /// </summary>
    public class Session
    {
        public static bool NoCurrentUser
        {
            get
            {
                return HttpContext.Current.Session["CurrentUser"] == null;
            }
        }

        public static CRM_API.Models.SessionInfo CurrentUser
        {
            get
            {
                return NoCurrentUser ? new CRM_API.Models.SessionInfo() : (CRM_API.Models.SessionInfo)HttpContext.Current.Session["CurrentUser"];
            }
            set
            {
                HttpContext.Current.Session["CurrentUser"] = value;
            }
        }

        public static IEnumerable<CRM_API.Models.RoleByUser> CurrentRoles
        {
            get
            {
                return NoCurrentUser ? new List<CRM_API.Models.RoleByUser>() : (IEnumerable<CRM_API.Models.RoleByUser>)HttpContext.Current.Session["CurrentRoles"];
            }
            set
            {
                HttpContext.Current.Session["CurrentRoles"] = value;
            }
        }

        public static Models.ConnectionString SiteConnection
        {
            get
            {
                return NoCurrentUser ? new Models.ConnectionString() : (Models.ConnectionString)HttpContext.Current.Session["SiteConnection"];
            }
            set
            {
                HttpContext.Current.Session["SiteConnection"] = value;
            }
        }

        //Permission list
        public static IEnumerable<CRM_API.Models.permission> CurrentUserPermission
        {
            get
            {
                return NoCurrentUser ? new List<CRM_API.Models.permission>() : (IEnumerable<CRM_API.Models.permission>)HttpContext.Current.Session["Permission"];
            }
            set
            {
                HttpContext.Current.Session["Permission"] = value;
            }
        }

        //02-Jan-2019 : Moorthy : Added for stroing GetProductByMobile result in Session
        public static CRM_API.Models.ProductModel GetProductByMobile
        {
            get
            {
                return (CRM_API.Models.ProductModel)HttpContext.Current.Session["GetProductByMobile"];
            }
            set
            {
                HttpContext.Current.Session["GetProductByMobile"] = value;
            }
        }
    }
}