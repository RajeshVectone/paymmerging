﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Helpers.MVNOAccount
{
    public class MVNOAccount
    {
        public static string GetCountryCode(string sitecode)
        {
            string strCountryCode = "";
            if (sitecode == "BSE")
                strCountryCode = "SE";
            else if (sitecode == "MBE")
                strCountryCode = "BE";
            else if (sitecode == "BDK")    
                strCountryCode = "DK";
            else if (sitecode == "BNL")    
                strCountryCode = "NL";
            else if (sitecode == "BAU")    
                strCountryCode = "AT";
            else if (sitecode == "MFI")    
                strCountryCode = "FI";
            else if (sitecode == "MFR")    
                strCountryCode = "FR";
            else if (sitecode == "MPL")    
                strCountryCode = "PL";
            else if (sitecode == "MPT")
                strCountryCode = "PT";
            else if (sitecode == "MCM")   
                strCountryCode = "GB";

            return strCountryCode;
        }
    }
}
