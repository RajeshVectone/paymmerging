﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Collections.Specialized;
using System.Xml;
using System.IO;
using System.Net;
using System.Web;
using System.Data;

namespace CRM_API.Helpers.History
{
    public class CDRHistory
    {
        public static IEnumerable<CRM_API.Models.CDR.CallHistory> GetDataCallHistoryByMsisdn(string msisdn, string month, string dayFrom, string dayTo, string year)
        {
            string path = System.Configuration.ConfigurationManager.AppSettings["CDRServicesUrl"];
            string url = "/cdrservices/CDRInfo.svc/CDRInfoByMSISDN";
            string sUrl = string.Format(path + url + "?msisdn={0}&month={1}&datefrom={2}&dateto={3}&year={4}", msisdn, month, dayFrom, dayTo, year);
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(sUrl);
            req.UserAgent = "Mozilla";
            //req.Headers["user"] = "tester";
            req.Method = "GET";
            req.Timeout = 180000;
            try
            {
                HttpWebResponse res = (HttpWebResponse)req.GetResponse();
                Stream resStream = res.GetResponseStream();
                StreamReader reader = new StreamReader(resStream);

                string result = reader.ReadToEnd();

                res.Close();

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(result);

                DataSet ds = new DataSet();
                XmlNodeReader xnodereader = new XmlNodeReader(doc);
                ds.ReadXml(xnodereader);

                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                        return ds.Tables[0].AsEnumerable().Select(row => new CRM_API.Models.CDR.CallHistory
                        {
                            CallDate = row.Field<string>("CallDate"),
                            CustCode = row.Field<string>("CustCode"),
                            BatchCode = row.Field<string>("BatchCode"),
                            SerialCode = row.Field<string>("SerialCode"),
                            Ani = row.Field<string>("Ani"),
                            DialedNum = row.Field<string>("DialedNum"),
                            DestCode = row.Field<string>("DestCode"),
                            Balance = row.Field<string>("Balance"),
                            TalkTime = row.Field<string>("TalkTime"),
                            TalkCharge = row.Field<string>("TalkCharge"),
                            TotalCons = row.Field<string>("TotalCons"),
                            CurrCode = row.Field<string>("CurrCode"),
                            FreeBalance = row.Field<string>("FreeBalance")
                        }).ToList();

                    else
                        return null;
                }
                else
                    return null;
            }
            catch (WebException ex)
            {
                return new List<Models.CDR.CallHistory>();
            }
        }

        public static IEnumerable<CRM_API.Models.CDR.GPRSHistory> GetDataGPRSByMsisdn( string msisdn, string datefrom, string dateto)
        {
            string path = System.Configuration.ConfigurationManager.AppSettings["CDRServicesUrl"];
            string url = "/cdrservices/CDRInfo.svc/CDRGPRSInfoByMSISDN";
            string sUrl = string.Format(path + url + "?msisdn={0}&datefrom={1}&dateto={2}", msisdn, datefrom,dateto );
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(sUrl);
            req.UserAgent = "Mozilla";
            //req.Headers["user"] = "tester";
            req.Method = "GET";
            req.Timeout = 180000;
            try
            {
                HttpWebResponse res = (HttpWebResponse)req.GetResponse();
                Stream resStream = res.GetResponseStream();
                StreamReader reader = new StreamReader(resStream);

                string result = reader.ReadToEnd();

                res.Close();

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(result);

                DataSet ds = new DataSet();
                XmlNodeReader xnodereader = new XmlNodeReader(doc);
                ds.ReadXml(xnodereader);

                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                        return ds.Tables[0].AsEnumerable().Select(row => new CRM_API.Models.CDR.GPRSHistory
                        {
                            CnxDate = row.Field<string>("CnxDate"),
                            GprsAllocation = row.Field<string>("GprsLocation"),
                            GprsCharge = row.Field<string>("GprsCharge"),
                            TariffClass = row.Field<string>("TariffClass"),
                            RoamingZone = row.Field<string>("RoamingZone"),
                            PackageID = row.Field<string>("PackageId"),
                        }).ToList();

                    else
                        return null;
                }
                else
                    return null;
            }
            catch (WebException ex)
            {
                return new List<Models.CDR.GPRSHistory>();
            }
        }
    }
}
