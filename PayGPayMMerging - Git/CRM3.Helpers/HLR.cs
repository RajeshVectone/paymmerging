﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.IO;
using NLog;
using System.Text;
using CRM_API.Models.CountrySaver;
using System.Net.Http.Headers;
using System.Net.Http;
//using System.Net.Http.Formatting;

namespace CRM_API.Helpers
{
    public class HLR
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        private string UKHRLMgt = System.Configuration.ConfigurationManager.AppSettings["UKHRLMgt"];
        private string FRHRLMgt = System.Configuration.ConfigurationManager.AppSettings["FRHRLMgt"];
        private static string USSD_HRLCallFoward_Key = "{0}UrlForSendingCallForwardData";
        
        private const string Uri_CancelLocationUpdate = "data-type=10&msisdn={0}";
        private static string _sresponsecode;
        private static string _sresponsestring;
        private static string _sitecode;
        
        public string[] CancelLocation(string mobileNo)
        {
            if (string.IsNullOrEmpty(mobileNo) || string.IsNullOrEmpty(mobileNo))
            {
                return new string[] { "-1", "Empty Variables!" };
            }

            string WebHLRMgt = string.Empty;
            if (mobileNo.StartsWith("44"))
                WebHLRMgt = UKHRLMgt;
            else if (mobileNo.StartsWith("33"))
                WebHLRMgt = FRHRLMgt;

            string[] hlrResponseData = { "-1", "Failed connecting to the HLR Service!" };
            try
            {
                // Format the uri string
                string requestUriString =
                    string.Format(WebHLRMgt + Uri_CancelLocationUpdate,
                    mobileNo);

                // Generate request
                HttpWebRequest hlrMgt = (HttpWebRequest)WebRequest.Create(requestUriString);
                hlrMgt.Method = "POST";
                hlrMgt.UserAgent = "CRM_API";
                hlrMgt.ContentLength = 0;

                // Log it
                Log.Info(string.Format("CancelLocation:{0}", requestUriString));

                // Send request and get the response data
                HttpWebResponse hlrResponse = (HttpWebResponse)hlrMgt.GetResponse();
                StreamReader hlrResponseReader = new StreamReader(hlrResponse.GetResponseStream());
                hlrResponseData = hlrResponseReader.ReadToEnd().Trim().Split(":".ToCharArray(), 2);
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                Log.Info(string.Format("CancelLocation Error:{0}", ex.Message));
            }

            return hlrResponseData;
        }
        
        public static bool USSDSend1(string Active ,  string VLRNo ,  string URL)
        {
            try
            {
                using (var client = new HttpClient())
                {
                   
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
                    string mode;
                    int node_type;
                    mode = "http";
                    node_type = 1; // MODEW = 1 
                    string urll;
                    string vlr_number;
                    string imsi_active;
                    imsi_active = Active;
                    vlr_number = VLRNo;
                    urll = URL;
                    string postData = "data-vlr-number=" + vlr_number + "&data-imsi=" + imsi_active + "&mode=" + mode;

                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(urll);
                    request.Method = "POST";
                    request.ContentType = "application/x-www-form-urlencoded";
                    ASCIIEncoding enc = new ASCIIEncoding();
                    byte[] data = enc.GetBytes(postData);
                    request.ContentLength = data.Length;
                    Stream newStream = request.GetRequestStream();
                    newStream.Write(data, 0, data.Length);
                    newStream.Close();
                    HttpWebResponse smsRes = (HttpWebResponse)request.GetResponse();
                    Stream resStream = smsRes.GetResponseStream();
                    Log.Debug("Cancel Location Success");
                    Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                    StreamReader readStream = new StreamReader(resStream, encode);
                    
                    //return Request.CreateResponse(HttpStatusCode.OK, Location);
                    return true;
                }
            }
            catch (Exception e)
            {
                //return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new Models.Common.ErrCodeMsg() { errcode = -1, errmsg = e.Message });
                return false;
            }
        }
        public static bool USSDSend(string DestMobileNo, string DataType, string Sitecode)
        {
            _sitecode = Sitecode;
             DataType = 4.ToString();
            try
            {
                DestMobileNo = DestMobileNo.Trim();
                //return true;
                HttpWebRequest smsReq = (HttpWebRequest)WebRequest.Create(SipServerUssd);

                smsReq.Method = "POST";
                smsReq.ContentType = "application/x-www-form-urlencoded";

                ASCIIEncoding enc = new ASCIIEncoding();

                string postData =
                    "msisdn=" + DestMobileNo + "&" +
                    "data-type=" + DataType;

                byte[] data = enc.GetBytes(postData);
                smsReq.ContentLength = data.Length;

                Stream newStream = smsReq.GetRequestStream();
                newStream.Write(data, 0, data.Length);
                newStream.Close();

                HttpWebResponse smsRes = (HttpWebResponse)smsReq.GetResponse();
                Stream resStream = smsRes.GetResponseStream();

                Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                StreamReader readStream = new StreamReader(resStream, encode);
                
               // if (smsRes.StatusCode != HttpStatusCode.OK)
                //{
                //    // Something went wrong
                //    return false;
               // }
                
                string resBody = readStream.ReadToEnd();
                _sresponsecode = resBody.Substring(0, 1);
                _sresponsestring = resBody.Substring(2);

                if (_sresponsecode != "0")
                {
                    // Something went wrong
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("(500) Internal Server Error"))
                    throw new Exception("Helper USSDSend: " + "Please enter the working phone number");
                else
                    throw new Exception("Helper USSDSend: " + ex.Message);
            }
             
        }
        public static string SipServerUssd
        {
            get
            {
                string _appSetting = string.Format(USSD_HRLCallFoward_Key, _sitecode);
                return System.Configuration.ConfigurationManager.AppSettings[_appSetting];
            }
        }
    }
}