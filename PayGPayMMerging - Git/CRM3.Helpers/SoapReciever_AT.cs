﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.IO;
using NLog;
using System.Text;
using System.Xml.Linq;
using System.Xml;

namespace CRM_API.Helpers.Porting.AT
{
    public class SoapReciever : System.Web.Services.Protocols.SoapHttpClientProtocol
    {
        private static string _sitecode = string.Empty;
        public static string GetRequestID(string Sitecode)
        {

            try
            {
                string Id = "GetRequestID";
                _sitecode = Sitecode;
                HttpWebRequest soapReq = (HttpWebRequest)WebRequest.Create(Config.SoapRecieverWebSvc(Sitecode) + Id);
                soapReq.Method = "POST";
                soapReq.ContentType = "application/x-www-form-urlencoded";

                ASCIIEncoding enc = new ASCIIEncoding();
                string postData = string.Empty;
                byte[] data = enc.GetBytes(postData);
                soapReq.ContentLength = data.Length;

                Stream newStream = soapReq.GetRequestStream();
                newStream.Write(data, 0, data.Length);
                newStream.Close();

                HttpWebResponse soapRes = (HttpWebResponse)soapReq.GetResponse();
                Stream resStream = soapRes.GetResponseStream();

                Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                StreamReader readStream = new StreamReader(resStream, encode);
                string resBody = readStream.ReadToEnd();
                XDocument doc = XDocument.Parse(resBody);

                return string.IsNullOrEmpty(doc.Root.Value) ? null : doc.Root.Value;
            }
            catch (Exception ex)
            {
                throw new Exception("Helper CRM_API.Helpers.SOAP.GetRequestID: " + ex.Message);
            }
        }
        public static string RequestPorting(CRM_API.Models.Porting.AT.SoapReceiver.Transactions.ReqPortingType ReqPorting, string Sitecode)
        {

            try
            {
                string Id = "requestPorting";
                _sitecode = Sitecode;
                HttpWebRequest soapReq = (HttpWebRequest)WebRequest.Create(Config.SoapRecieverWebSvc(Sitecode) + Id);
                soapReq.Method = "POST";
                soapReq.ContentType = "text/xml;charset=utf-8";

                ASCIIEncoding enc = new ASCIIEncoding();
                XmlDocument soapEnvelopeXML = new XmlDocument();
                var ContentXml = readFile(Config.GetPortingXMLFormatPath(Sitecode) + "RequestPorting.xml");
                // TODO : Parse information Goes Here
#if DEBUG
                ContentXml.Replace("##RequestID##", "-1");
                ContentXml.Replace("##Sender##", "test");
                ContentXml.Replace("##Receiver##", "test");
                ContentXml.Replace("##MNP_imp##", "test");
                ContentXml.Replace("##MSP_imp##", "test");
                ContentXml.Replace("##MNP_exp##", "test");
                ContentXml.Replace("##MSP_exp##", "test");
                ContentXml.Replace("##Timestamp##", "test");
                ContentXml.Replace("##EnvFlag##", "test");
                ContentXml.Replace("##Version##", "test");
                ContentXml.Replace("##PortingCode##", "test");
                ContentXml.Replace("##Msisdn##", string.Empty);
                ContentXml.Replace("##DesiredPortingDate##", "test");                

#else
                ContentXml.Replace("##RequestID##", ReqPorting.Header.RequestID.ToString());
                ContentXml.Replace("##Sender##", ReqPorting.Header.Sender);
                ContentXml.Replace("##Receiver##", ReqPorting.Header.Receiver);
                ContentXml.Replace("##MNP_imp##", ReqPorting.Header.MNP_imp);
                ContentXml.Replace("##MSP_imp##", ReqPorting.Header.MSP_imp);
                ContentXml.Replace("##MNP_exp##", ReqPorting.Header.MNP_exp);
                ContentXml.Replace("##MSP_exp##", ReqPorting.Header.MSP_exp);
                ContentXml.Replace("##Timestamp##", ReqPorting.Header.Timestamp.ToString());
                ContentXml.Replace("##EnvFlag##", ReqPorting.Header.EnvFlag.ToString());
                ContentXml.Replace("##Version##", ReqPorting.Header.Version);
                ContentXml.Replace("##PortingCode##", ReqPorting.PortingCode);
                ContentXml.Replace("##Msisdn##", string.Empty);
                ContentXml.Replace("##DesiredPortingDate##", ReqPorting.DesiredPortingDate.ToString());                
#endif
                //
                soapEnvelopeXML.LoadXml(ContentXml);
                byte[] data = enc.GetBytes(ContentXml);
                soapReq.ContentLength = data.Length;


                Stream newStream = soapReq.GetRequestStream();
                newStream.Write(data, 0, data.Length);
                newStream.Close();

                HttpWebResponse soapRes = (HttpWebResponse)soapReq.GetResponse();
                Stream resStream = soapRes.GetResponseStream();

                Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                StreamReader readStream = new StreamReader(resStream, encode);
                string resBody = readStream.ReadToEnd();
                XmlDocument Respdoc = new XmlDocument();
                Respdoc.LoadXml(resBody);

                return Respdoc.GetElementById("AcknowledgeType").Value;
            }
            catch (Exception ex)
            {
                throw new Exception("Helper CRM_API.Helpers.SOAP.GetRequestID: " + ex.Message);
            }
        }
        private static string readFile(string filePath)
        {
            // use relative server path for web application, and use local path for non web application
            if (HttpContext.Current != null)
                filePath = HttpContext.Current.Server.MapPath(filePath);

            if (!File.Exists(filePath))
                return string.Empty;

            try
            {
                StreamReader streamReader = new StreamReader(filePath);
                string text = streamReader.ReadToEnd();
                streamReader.Close();

                return text;
            }
            catch
            {
                return string.Empty;
            }
        }
        private static void InsertSoapEnvelopeIntoWebRequest(XmlDocument soapEnvelopeXml, HttpWebRequest webRequest)
        {

        }
    }
}
