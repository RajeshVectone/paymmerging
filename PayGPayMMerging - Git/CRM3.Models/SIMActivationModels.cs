﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models.SIMActivation
{
    public class SIMActivationModels: Output
    {
        public string date_from { get; set; }
        public string date_to { get; set; }        
    }
    public class Output
    {
        public DateTime act_date { get; set; }
        public string act_date_string { get { return act_date.ToString("dd-MM-yyyy hh:mm:ss tt"); } }
        public string mobileNo { get; set; }
        public string iccid { get; set; }
        public string act_by { get; set; }
        
        public int errcode { get; set; }
        public string errmsg { get; set; }
    }
}
