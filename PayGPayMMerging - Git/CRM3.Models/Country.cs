﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models
{
    public class CountryInfo
    {
        public string Sitecode { get; set; }
        public string product_code { get; set; }
        public string Country { get; set; }
        public string newcountry { get; set; }
    }

}
