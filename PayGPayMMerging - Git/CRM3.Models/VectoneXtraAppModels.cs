﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace CRM_API.Models
{
    
    public class VectoneXtraAppOutput : Common.ErrCodeMsg
    {
        public DateTime signup_date { get; set; }
        public string signup_date_string
        {
            get
            {
                return signup_date.ToString("dd-MM-yyyy hh:mm:ss tt");
            }
        }
        public string create_by { get; set; }
        public string number_type { get; set; }
        public DateTime last_login_date { get; set; }
        public string last_login_date_string
        {
            get
            {
                return last_login_date.ToString("dd-MM-yyyy hh:mm:ss tt");
            }
        }
        public string purchased_vno { get; set; }
        public string purchased_did { get; set; }
        public string free_virtual_no { get; set; }
    }

    //CRM Enhancement 2
    public class VectoneXtraCallHistoryOutput : Common.ErrCodeMsg
    {
        public string History_Type { get; set; }
        public DateTime? Date_Time { get; set; }
        public string Date_Time_string
        {
            get
            {
                return Date_Time != null ? Convert.ToDateTime(Date_Time).ToString("dd-MM-yyyy hh:mm:ss tt") : "";
            }
        }
        public string CLI { get; set; }
        public string Destination_Number { get; set; }
        public string Type { get; set; }
        public string Destination_Code { get; set; }
        public string Duration_Min { get; set; }
        public decimal? Data_Usage { get; set; }
        public string Tariff_Class { get; set; }
        public int? Roaming_Zone { get; set; }
        public string Package_Name { get; set; }
        public string Net_Charges { get; set; }
        public decimal? Balance { get; set; }
    }
}