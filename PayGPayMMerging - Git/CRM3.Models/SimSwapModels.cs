﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models
{
    public class SimSwapSearch:Subscriber.SubsciberInfo
    {
        public int Id { get; set; }
        public string MsisdnOld { get; set; }
        public string MsisdnNew { get; set; }
        public string NewIccidLastDigit { get; set; }
        public string JiraTicketNo { get; set; }
        public decimal SimSwapCharge { get; set; }
        public string VerificationCode { get; set; }
        public SwapType TypeofSwap { get; set; }
        public int IsNewSim { get; set; }
        public string SwapReason { get; set; }
        public string User { get; set; }
        public string Sitecode { get; set; }
        public string SwapType { get; set; }
        public BrandType OriginBrand { get; set; }
        public SimType TypeofSim { get; set; }
        #region Dispatch Information
        public int IsNewAddress { get; set; }
        #endregion
    }
    public enum SwapType
    {
        Validate=  0,
        PortIn = 1,
        LostNumber = 2,
        NewSimType = 3,
        FillSIMReports = 4
    }
    public enum BrandType
    {
        Vectone = 1,
        Delight = 2,
        UnitedFone = 3
    }
    public enum SimType
    {
        Normal = 1,
        Nano = 0, 
        Micro = 11,
    }

    //31-Dec-2018 : Moorthy : Added for SIM Reports
    public class FillSIMReportsRef : Common.ErrCodeMsg
    {

    }

}
