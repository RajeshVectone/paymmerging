﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models
{
    public enum PAYMPlanBundleRequestType
    {
        Default = 0,
        PlanList = 1,
        ListPlanDetails = 2,
        ListPlanHistory = 3,
        ListPlanRef = 4,
        GetPlanDetail = 5,
        GetBundleDetail = 6,
        GetAccountBalance = 7,
        SubscribePlan = 50,
        RequestChangeLowerPlan = 60,
        RequestChangeHigherPlan = 61,
        ScheduleCancelPlan = 70,
        ImmediateCancelPlan = 71,
        //Added on 20 Jan 2015 - by Hari - for Reactivation
        BundleReactivate = 80,
        LLOMReactivate = 81,
        CountrySaverReactivate = 82,
        //End added 

        //Added by Elango for Jira CRMT-216
        RequestCreditLimit = 90,
        GetCreditLimit = 91,
        ApproveCreditLimit = 92,
        RejectCreditLimit = 93,
        GetRestrictExcessUsage = 94,
        InsertRestrictExcessUsage = 95,
        //Added : 25-Dec-2018 - Change Plan
        ListBundleCategory = 96,
        ListBundleByCategoryId = 97,
        ExchangeBundle = 98
    }

    public class PAYMPlanBundleRequest : CRM_API.Models.Payment.CardDetails
    {
        public PAYMPlanBundleRequestType requestType { get; set; }
        public string SiteCode { get; set; }
        public string MobileNo { get; set; }
        // 31-Jul-2015 : Moorthy Added
        public string ProductCode { get; set; }
        public int BundleId { get; set; }
        public string CalledBy { get; set; }
        public DateTime prevplan_end_date { get; set; }
        public DateTime newplan_start_date { get; set; }
        public float oldplan { get; set; }
        public string oldplantext { get; set; }
        public double newplan { get; set; }
        public string newplantext { get; set; }
        public float ordervalue { get; set; }
        public float discount { get; set; }
        public float totalvalue { get; set; }
        //added on 21 Jan 2015 - by Hari - LLOM Bundle Reactivate Payment
        public int SvcId { get; set; }
        public int payment_mode { get; set; }
        public string Reg_Number { get; set; }
        public string username { get; set; }
        //end added
        //added on 20 Jun 2015 - by Hari - Country saver
        public string AccountNo { get; set; }
        //public int SubscriptionID { get; set; }
        //end added

        //Added by Elango for Jira CRMT-216
        public float RequestCreditAmount { get; set; }

        //Added for Restrict Excess Usage
        public int status { get; set; }
        public string updated_by { get; set; }

        //Added : 25-Dec-2018 - Change Plan
        public int CategoryId { get; set; }

        //27-Dec-2018 : Moorthy : Exchange Bundles
        public string calledby { get; set; }
        public int oldplanid { get; set; }
        public int newplanid { get; set; }
        public string mobileno { get; set; }
    }

    public class PAYMPlanAccountInfo : Common.ErrCodeMsg
    {
        public int CustomerId { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public string CurrentPlanName { get; set; }
        public string CurrentBundleId { get; set; }
    }

    public class PAYMPlan : Common.ErrCodeMsg
    {
        public string action { get; set; }
        public int idx { get; set; }
        public int pp_customer_id { get; set; }
        public string plan_name { get; set; }
        public int plan_active_status { get; set; }
        public string plan_active_status_name { get; set; }
        public DateTime plan_start_date { get; set; }
        public string plan_start_date_name
        {
            get
            {
                return plan_start_date.ToString("dd-MM-yyyy");
            }
        }
        public DateTime plan_end_date { get; set; }
        public string plan_end_date_name
        {
            get
            {
                return plan_end_date.ToString("dd-MM-yyyy");
            }
        }
        public int postpaidbundled { get; set; }
        public int paymentplanid { get; set; }

        //Added by Elango for Jira CRMT-216 Start
        public float credit_limit { get; set; }
        public float new_credit_limit { get; set; }
        public float existing_credit_limit { get; set; }

        //Restrict Excess Access
        public string excess_usage_status { get; set; }
        public int excess_usage_status_flag { get; set; }
        //25-Dec-2018 : Change Plan
        public int bundleid { get; set; }
        public float plan_value { get; set; }

        //CRM Enhancement 2
        public string sim_type { get; set; }
        public string assigned_allowance { get; set; }
        public string avaialable_allowance { get; set; }
        public DateTime? paym_conversion { get; set; }
        public string paym_conversion_str
        {
            get
            {
                return paym_conversion != null ? Convert.ToDateTime(paym_conversion).ToString("dd-MM-yyyy") : "";
            }
        }
        public DateTime? plan_cancel_requestdate { get; set; }
        public string plan_cancel_requestdate_str
        {
            get
            {
                return plan_cancel_requestdate != null ? Convert.ToDateTime(plan_cancel_requestdate).ToString("dd-MM-yyyy hh:mm:ss tt") : "";
            }
        }
        public DateTime? plan_cancel_scheduledate { get; set; }
        public string plan_cancel_scheduledate_str
        {
            get
            {
                return plan_cancel_scheduledate != null ? Convert.ToDateTime(plan_cancel_scheduledate).ToString("dd-MM-yyyy hh:mm:ss tt") : "";
            }
        }
        public DateTime? next_billing_date { get; set; }
        public string next_billing_date_str
        {
            get
            {
                return next_billing_date != null ? Convert.ToDateTime(next_billing_date).ToString("dd-MM-yyyy") : "";
            }
        }
        public DateTime? Last_billing_date { get; set; }
        public string Last_billing_date_str
        {
            get
            {
                return Last_billing_date != null ? Convert.ToDateTime(Last_billing_date).ToString("dd-MM-yyyy") : "";
            }
        }
        //CRM Enhancement 2
        public string bundle_plan_name { get; set; }

        //01-Feb-2019 : Moorthy : added for additional output bill_cycle 
        public string bill_period { get; set; }

        //01-Feb-2019: Moorthy : added for Ava Allowance
        public string international_tariff { get; set; }
        public DateTime? startdate { get; set; }
        public int config_id { get; set; }
    }

    public class PAYMPlanDetail : Common.ErrCodeMsg
    {
        public int package_id { get; set; }
        public string package_name { get; set; }
        public DateTime startDate { get; set; }
        public string startDate_name
        {
            get
            {
                return startDate.ToString("dd-MM-yyyy");
            }
        }
        public DateTime expDate { get; set; }
        public string expDate_name
        {
            get
            {
                return expDate.ToString("dd-MM-yyyy");
            }
        }
        public int packagetype { get; set; }
        public string packagetype_name { get; set; }
        public int packageStatusID { get; set; }
        public string status_name { get; set; }
        public float credit_limit { get; set; }
        public float current_excess { get; set; }
        public float available_excess { get; set; }
    }

    public class PAYMPlanHistory : Common.ErrCodeMsg
    {
        public DateTime log_date { get; set; }
        public string log_date_name
        {
            get
            {
                return log_date.ToString("dd-MM-yyyy");
            }
        }
        public string description { get; set; }
        public string plan_details { get; set; }
        public double payment_amount { get; set; }
        public string payment_amount_name
        {
            get
            {
                if (payment_amount > 0)
                    return string.Format("£{0:N2}", payment_amount);
                else
                    return "n/a";
            }
        }
        public string payment_methods { get; set; }
        public int payment_status { get; set; }
        public string payment_status_name { get; set; }

        ////Added by Elango for Jira CRMT-216 Start
        public string request_status { get; set; }

        //01-Feb-2019: Moorthy : Added for CRM Enhancement 2
        public string prev_plan { get; set; }
        public string requested_by { get; set; }
    }

    public class PAYMPlanRef : Common.ErrCodeMsg
    {
        public string tariffclass { get; set; }
        public int bundleid { get; set; }
        public string plan_name { get; set; }
        public float plan_value { get; set; }
        public string ProductSKU { get; set; }
    }

    //Added : 25-Dec-2018 - Change Plan
    public class BundleCategorryRef
    {
        public int category_id { get; set; }
        public string category_name { get; set; }
    }

    //Added : 25-Dec-2018 - Change Plan
    public class BundleListByCategoryIdRef
    {
        public string plan_name { get; set; }
        public string plan_value { get; set; }
        public int bundleid { get; set; }
        public string bundle_call { get; set; }
        public string bundle_sms { get; set; }
        public string bundle_data { get; set; }
    }

    public class PlanDetail : Common.ErrCodeMsg
    {
        public int ItemID { get; set; }
        public float PlanValue { get; set; }
        public string PlanDescription { get; set; }
        public int BundleId { get; set; }
    }

    public class AccountBalance : Common.ErrCodeMsg
    {
        public int packid { get; set; }
        public string custcode { get; set; }
        public int batchcode { get; set; }
        public int serialcode { get; set; }
        public string balance { get; set; }
        public string tariffclass { get; set; }
        public int info_type { get; set; }
        public string info_text { get; set; }
        public int paytype { get; set; }
        public float creditinit { get; set; }
        public int packagetype { get; set; }
        public float usage_sofar { get; set; }
        public float outstandingValue { get; set; }
        public float excessValue { get; set; }
    }

    public class GetRestrictExcessUsageOutput
    {
        public int status { get; set; }
        public string process_by { get; set; }
        public DateTime? last_update { get; set; }
        public string last_update_string { get { return last_update != null ? last_update.Value.ToString("dd/MM/yyyy hh:mm:ss") : ""; } }
    }

    public class ChangePlanOutput
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
        private string _errsubject = "General Error";
        public string errsubject
        {
            get
            {
                return (errcode == 0 || errcode == 1) ? "Result Information" : _errsubject;
            }
            set { _errsubject = value; }
        }
        public string bundleprice { get; set; }
        public string bundlename { get; set; }
        public string bundle_call { get; set; }
        public string bundle_sms { get; set; }
        public string bundle_data { get; set; }
        public string bundle_v2v { get; set; }
        public string billing_date { get; set; }
    }
}
