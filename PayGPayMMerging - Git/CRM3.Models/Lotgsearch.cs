﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models
{
    public class Lotgsearch
    {
        public string Country { get; set; }
        public string Allocated_Numbers { get; set; }
        public string Active_Numbers { get; set; }
        public string Available_Numbers { get; set; }
        public string Reserved_Numbers { get; set; }
        public string Action { get; set; }
        public string Select { get; set; }
       
    }


    public class Lotgpsearch
    {
        public string llom { get; set; }
        public string vectonenumber { get; set; }
        public string country { get; set; }
        public string city { get; set; }
        public string plan { get; set; }
        public string start_date { get; set; }
        public string end_date { get; set; }
        public string duration { get; set; }
        public string status { get; set; }
        public string Action { get; set; }
    }



    public class Lotgwisesearch
    {
        public string LLOM_Number { get; set; }
        public string Vectone_Number { get; set; }
        public string Country { get; set; }
        public string city { get; set; }
        public string plan { get; set; }
        public string Start_Date { get; set; }
        public string End_Date { get; set; }
        public string Status { get; set; }
        public string Duration { get; set; }
        public string Action { get; set; }
    }




    public class Lotgproductview
    {
        public string Product { get; set; }
        public string Active_Numbers { get; set; }
        public string InActive_Numbers { get; set; }
        public string Reserved_Numbers { get; set; }
        public string Action { get; set; }
    }

     public class lotgcityview
    {
        public string City { get; set; }
        public string Area_Code { get; set; }
        public string AllocateNumbers { get; set; }
        public string Available_Number { get; set; }   
         public string Active_Numbers { get; set; }
        public string InActive_Numbers { get; set; }
        public string Reserved_Numbers { get; set; }
        public string Product { get; set; }
        public string Action { get; set; }
    }

     public class lotgcityview_v2
     {
         public string City { get; set; }
         public string AllocateNumbers { get; set; }
         public string Active_Numbers { get; set; }
         public string InActive_Numbers { get; set; }
         public string Reserved_Numbers { get; set; }
         public string Action { get; set; }
        
     }

     public class lotgproductview_v2
     {
         public string City { get; set; }
         public string Product { get; set; }
         public string Active_Numbers { get; set; }
         public string InActive_Numbers { get; set; }
         public string Reserved_Numbers { get; set; }
         public string Action { get; set; }

     }


     public class lotgcountrytoproduct
     {
         public string City { get; set; }
         public string Product { get; set; }
         public string Active_Numbers { get; set; }
         public string InActive_Numbers { get; set; }
         public string Reserved_Numbers { get; set; }
         public string Action { get; set; }

     }


     public class lotgcountrytocity
     {
         public string City { get; set; }
         public string Area_Code { get; set; }
         public string AllocateNumbers { get; set; }
         public string Available_Number { get; set; }
         public string Active_Number { get; set; }
         public string InActive_Numbers { get; set; }
         public string Reserved_Numbers { get; set; }
         public string Product { get; set; }
         public string Action { get; set; }

     }


     public class lotgproductcityview
     {
         public string LLOMNumber { get; set; }
         public string VectoneNumber { get; set; }
         public string Plan { get; set; }
         public string StartDate { get; set; }
         public string RenewalDate { get; set; }
         public string Duration { get; set; }
         public string status { get; set; }
         public string Action { get; set; }

     }


     public class lotgproductcityview_v5
     {
         public string LLOMNumber { get; set; }
         public string VectoneNumber { get; set; }
         public string Country { get; set; }
         public string area_name { get; set; }
         public string Plan { get; set; }
         public string StartDate { get; set; }
         public string EndDate { get; set; }
         public string status { get; set; }
         public string Duration { get; set; }
         public string SubscriptionDate { get; set; }

     }



     public class bugetvalues
     {
         public string UPDATE_DATE { get; set; }
         public string CURRENT_STATUS { get; set; }
         public string USER { get; set; }
         public string REASON { get; set; }        

     }

     public class bubgetvalues
     {
         public string errorcode { get; set; }
         public string errormessage { get; set; }
        

     }

     public class lotgareanameid
     {
         public string LLOMNumber { get; set; }
         public string VectoneNumber { get; set; }
         public string Country { get; set; }
         public string area_name { get; set; }
         public string Plan { get; set; }
         public string StartDate { get; set; }
         public string EndDate { get; set; }
         public string status { get; set; }
         public string Duration { get; set; }
         public string SubscriptionDate { get; set; }

     }





     public class mobvalidation
     {
         public string errcode { get; set; }
         public string errmsg { get; set; }
        

     }

     public class lotgproductcityview_v2
     {
         public string LLOMNumber { get; set; }
         public string VectoneNumber { get; set; }
         public string Country { get; set; }
         public string area_name { get; set; }
         public string Plan { get; set; }
         public string StartDate { get; set; }
         public string EndDate { get; set; }
         public string SubscriptionDate { get; set; }
         public string Duration { get; set; }
         public string status { get; set; }
         public string Action { get; set; }

     }

     public class lotgmapping
     {
         public string errcode { get; set; }
         public string errmsg { get; set; }
         public string Action { get; set; }
         
     }


    
}
