﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models
{
    public class FreeDataBundleInput
    {
        public FreeDataBundleType Type { get; set; }
        
        public string sitecode { get; set; }
        public string product_code { get; set; }
        public int category_id { get; set; }

        //public int usertype { get; set; }//2
        public string userinfo { get; set; }//MobileNo
        public int bundleid { get; set; }
        //public int paymode { get; set; }//3
        //public string processby { get; set; }//CRM
        public string pack_dest { get; set; }//""
        public string subscribed_user { get; set; }//Login User
        public double? bundle_price { get; set; }
        public string bundle_category { get; set; }
        public string bundle_name { get; set; }

        //GetFreelySubscribedBundleReportInput
        public string from_date { get; set; }
        public string to_date { get; set; }
    }

    public class FreeDataBundleOutput
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
    }

    public class GetBundleGroupOutput
    {
        public int category_id { get; set; }
        public string category_name { get; set; }
    }

    public class GetBundleListInfoOutput
    {
        public int bundleid { get; set; }
        public string bundle_name { get; set; }
        public string bundle_price { get; set; }
        public string vec_ussd_prefix { get; set; }
        public string bundle_call { get; set; }
        public string bundle_sms { get; set; }
        public string bundle_data { get; set; }
        public string days_valid { get; set; }
        public string bundle_type_name { get; set; }
        public string ACTION { get { return ""; } }
    }

    public class BundleSubscribeFreeOutput
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
    }

    public class GetFreelySubscribedBundleReportOutput
    {
        public DateTime? logdate { get; set; }
        public string mobileno { get; set; }
        public string bundle_category { get; set; }
        public string bundle_name { get; set; }
        public string bundle_price { get; set; }
        public string subs_user { get; set; }
    }

    public enum FreeDataBundleType
    {
        GetBundleGroup = 1,
        GetBundleListInfo = 2,
        BundleSubscribeFree = 3,
        GetFreelySubscribedBundleReport = 4
    }
}

