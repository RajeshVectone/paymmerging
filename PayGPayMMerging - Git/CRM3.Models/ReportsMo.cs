﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models.ReportsMo
{
   

        public enum Search
        {
            Mob = 1,
            Date = 2,
        }
        public class reportsMobileReq
        {
            public string mobileno { get; set; }
            public string sitecode { get; set; }
            public string startdate { get; set; }
            public string enddate { get; set; }
            public Search Step { get; set; }
        }
        public class reportsMobileRSP
        {

            public string Submit_Date { get; set; }
            public string SubmitBy { get; set; }
            public string Function { get; set; }
            public string Complaint { get; set; }
            public string Issue_Type { get; set; }
            public int Total_Calls { get; set; }
            public string MobileNo { get; set; }
        }
       

        //public enum SearchType
        //{
        //    Mob = 1,
        //    ProblemDetail = 2,
        //    IssueCategory = 3,
        //    IssueCategoryDetail = 4,
        //    InsertIssue = 5,
        //    InsertIssue1 = 12,
        //    UpdateIssue = 6,
        //    IssueHistory = 7,
        //    IssueCategory1 = 8,
        //    IssueCategory2 = 9,
        //    IssueCategory3 = 11,
        //    //IssueCategory4=12
        //}

       
       
    }
