﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models.PAYMPaymentModels
{
    public class PaymentAutoQueue : Common.ErrCodeMsg
    {
        public string pay_reference {get; set;}
        public int pp_customer_id {get; set;}
        public DateTime pay_date {get; set;}
        public string pay_method {get; set;}
        public float amount {get; set;}
        public string updatedby {get; set;}
        public int? status {get; set;}
        public int idx {get; set;}
        public string status_updated_by {get; set;}
        public DateTime? status_updated_date {get; set;}
        public string mobileno {get; set;}
    }
}
