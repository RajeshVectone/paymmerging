﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models.Payment
{
    public class PaymentSearch : Common.ErrCodeMsg
    {
        public string mobileno { get; set; }
        public string sitecode { get; set; }
        public string date_from { get; set; }
        public string date_to { get; set; }
        public string searchvalue { get; set; }
        public string searchby { get; set; }
        public PaymentSearchType paymentSearchType { get; set; }
    }

    public enum PaymentSearchType
    {
        GetCustomerWisePaymentBreakup = 1,
        GetCustomerWisePaymentDetails = 2
    }

    public class CustomerWisePaymentBreakupOutput
    {
        public string account_id { get; set; }
        public string payment_mode { get; set; }
        public float purchase_amount { get; set; }
    }

    public class CustomerWisePaymentDetailsOutput
    {
        public string account_id { get; set; }
        public string payment_mode { get; set; }
        public string reference_id { get; set; }
        public float amount { get; set; }
        public string currency { get; set; }
        public DateTime payment_date { get; set; }
        public string payment_date_string { get { return payment_date.ToString("dd-MM-yyyy hh:mm:ss tt"); } }
        public string email { get; set; }
        public string IPpaymentclient { get; set; }
        public string ReplyMessage { get; set; }
        public string CC_NO { get; set; }
        public int service_added { get; set; }
    }
}
