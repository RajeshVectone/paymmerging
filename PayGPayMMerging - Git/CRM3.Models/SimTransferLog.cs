﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models.SimTransfer
{
    public enum RequestType
    {
        GetLog = 1
    }

    public class RequestModel : Common.ErrCodeMsg
    {
        public string Msisdn { get; set; }
        public string Sitecode { get; set; }
        public RequestType SubType { get; set; }
    }

    public class SimTransferLog : Common.ErrCodeMsg
    {
        // STORED PROCEDURE CONST        
        //private const string simtransfer_log_getby_msisdn = "simtransfer_log_getby_msisdn";
        public decimal Afterbal { get; set; }
        public decimal Balance_Old { get; set; }
        public string Balance_String { get { return decimal.Round(Balance_Old, 2, MidpointRounding.AwayFromZero).ToString(); } set { Balance_Old = decimal.Parse(value); } }
        public int Bc_New { get; set; }
        public int Bc_Old { get; set; }
        public string Cc_New { get; set; }
        public string Cc_Old { get; set; }
        public string Comment { get; set; }
        public DateTime Createdate { get; set; }
        public string Createdate_string
        {
            get
            {
                return Createdate.ToString("dd-MM-yyyy hh:mm:ss tt");
            }
        }

        public string Iccid_New { get; set; }
        public string Iccid_Old { get; set; }
        public string Msisdn { get; set; }
        public int Sc_New { get; set; }
        public int Sc_Old { get; set; }
        public string Transfertypedesc { get; set; }
        public int Transfertypeid { get; set; }

        //Add by BSK for 2 in 1 OTA Status display
        public string Roaming_IMSI_Overseas { get; set; }
        public string Home_IMSI { get; set; }
        public string Roaming_IMSI { get; set; }
        public string Vendor_Name { get; set; }
        public string country { get; set; }
        public DateTime Created_Date { get; set; }
        public string Created_Date_String
        {
            get
            {
                return Created_Date.ToString("dd-MM-yyyy hh:mm:ss tt");
            }
        }
        public string Created_By { get; set; }
        public string OTA_Status { get; set; }
        public string PortOutDate { get; set; }
        public string PortOutNetwork { get; set; }


    }
}
