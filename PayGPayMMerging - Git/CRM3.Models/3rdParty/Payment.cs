﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models._3rdParty.CTP
{
    public class Payment:Common.ErrCodeMsg
    {
        public int step { get; set; }
        public string AccId { get; set;}
        public string MerchantReferenceCode {get;set;}
    }
    public class TopupResponse:Common.ErrCodeMsg
    {
        string AfterBal { get; set; }
        double Amount { get; set; }
        string Currency { get; set; }
        string MerchantRefCode { get; set; }
    }
    public class PaymentPostProcessModel
    {
        public ProcessType ActionType { get; set; }
        public string Sitecode { get; set; }
    }
    public enum ProcessType
    {
        Registration = 1,
        Topup = 2,
        Addons = 3
    }
}
