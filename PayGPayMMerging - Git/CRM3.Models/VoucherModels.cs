﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models
{
    public class VoucherInfo:Common.ErrCodeMsg
    {
        public string SerialNumber { get; set; }
        public string Voucher_Status { get; set; }
        public string CBS { get { return SerialNumber; } }
        public string Pincode {get;set;}
        public string VoucherKey { get; set; }
        public DateTime? UseDate { get; set; }
        public string UsageDate_String { get { return (UseDate.HasValue ? UseDate.Value.ToString("dd/MM/yyyy hh:mm tt") : "Unused"); } }
        public string Voucher_Type { get; set; }
        public decimal price { get; set; }
        public string price_string { get { return String.Format("{0:N}", price).Replace(",", "."); } }
        public string TrffClass { get; set; }
        public string ResellerId { get; set; }
        public string ResellerName { get; set; }
        public DateTime? ActivationDate { get; set; }
        public string ActivationDate_String { get { return (ActivationDate.HasValue ? ActivationDate.Value.ToString("dd/MM/yyyy hh:mm tt") : "N/A"); } }
        public DateTime? ExpDate { get; set; }
        public string ExpiryDate_String { get { return (ExpDate.HasValue ? ExpDate.Value.ToString("dd/MM/yyyy hh:mm tt") : "N/A"); } }
        //04-Mar-2019 : Moorthy : Added for Upload Date
        public DateTime? upload_date { get; set; }
        public string upload_date_String { get { return (upload_date.HasValue ? upload_date.Value.ToString("dd/MM/yyyy hh:mm tt") : "N/A"); } }
    }
    public class CheckVoucher
    {
        public string SearchText { get; set; }
        public CheckBy SearchType { get; set; }
        public string Sitecode { get; set; }
    }
    public enum CheckBy
    {
        CBS = 1,
        PIN = 2
    }
}
