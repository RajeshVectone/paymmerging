﻿using System;

namespace CRM_API.Models.Porting
{
    public enum PortingBrand
    {
        UnknownBrand = -1,
        VectoneBrand = 1,
        DelightBrand = 2,
        UK1Brand = 100
    }
    public class UKSubmitEntry:Common.ErrCodeMsg
    {
        public string Msisdn { get; set; }
        public string LastDigitIccid { get; set; }
        public string ContactName { get; set; }
        public string ContactDetail { get; set; }
        public string PAC { get; set; }
        public string PortinMsisdn { get; set; }
        public DateTime PortDate { get; set; }
        public PortingStep SubType { get; set; }
        public ReportSMSType SMSReportFilter { get; set; }
        public int SmsId { get; set; }
        public string LoggedUser { get; set; }
        public string Sitecode { get; set; }
    }
    public enum PortingStep
    {
        Validate = 1,
        Submit = 2,
        Revise = 3,
        CheckExistingRequest = 4,
        Cancel = 5,
        ReportSMS = 8,
        ReportSMSDetail = 13,
        ReportSMSDetailByMsisdn = 15,
        ReportSMSDetail_Submit = 21,
        ReportSMSDetail_Cancel = 34,
        TestingStep = 55,
        SendSMSManual = 89,
        GetServiceProvider = 100,
        GetStateList = 101,
        TrxList = 102,
        Viewporting = 103,
        PortingPerformed = 104

    }

    public enum ReportSMSType
    {
        UnprocessedSMS = 0,
        ProcessedSMS = 1
    }
    public class ExistingRequest : Common.ErrCodeMsg
    {
        public DateTime port_date { get; set; }
        public string port_date_string 
        {get{return port_date.ToString("dd/MM/yyyy");}}
        public string contact_name { get; set; }
        public string contact_details { get; set; }
    }

    public class CancelRequest :Common.ErrCodeMsg
    {
        public int cancel_req_id { get; set; }
        public string err_msg { get; set; }
    }
    public class PortinSMS
    {
        public int Sms_Id { get; set; }
        public string PAC { get; set; }
        public string Msisdn { get; set; }
        public string BB_Msisdn { get; set; }
        public string Err_Message { get; set; }
        public DateTime TimeStamp { get; set; }
        public string TimeStamp_String { get { return TimeStamp.ToString("dd/MMM/yyyy hh:mm:ss tt"); } }
        public string Service_Number { get; set; }
        public int Validate_Code { get; set; }
        public string Validate_Message { get; set; }
    }

    public class NLSubmitEntry : UKSubmitEntry
    {
        public string Rsp { get; set; }
        public string Rno { get; set; }
        public string Dsp { get; set; }
        public string Dno { get; set; }
        public string Initials { get; set; }
        public string Prefix { get; set; }
        public DateTime RequestDate { get; set; }
        public Decimal RequestDate_Decimal
        {
            get
            {
                return Decimal.Parse(RequestDate.ToString("yyyyMMddHHmmss"));
            }
        }
        public string CompanyName { get; set; }
        public string Street { get; set; }
        public string HouseNo { get; set; }
        public string HouseNoExt { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string NoteOfMsg { get; set; }
        public string TelpNo { get; set; }
        public string Iccid { get; set; }
        public string IsResent { get; set; }
        public string OptType { get; set; }
        public string ProviderCode { get; set; }
        public bool ValidateDate { get; set; }
        public string CustIdOut { get; set; }
        public string CustID { get; set; }
        public string Email { get; set; }
        public string MsgIdentifier { get; set; }
        public DateTime PortingDateTo { get; set; }
        public Decimal PortingDateTo_Decimal
        {
            get
            {
                return PortingDateTo.Year == 0001 ? 0 : Decimal.Parse(PortingDateTo.ToString("yyyyMMddHHmmss")) + 00000000235959;
            }
        }
        public DateTime PortingDateFrom { get; set; }
        public Decimal PortingDateFrom_Decimal
        {
            get
            {
                return PortingDateFrom.Year == 0001 ? 0 : Decimal.Parse(PortingDateFrom.ToString("yyyyMMddHHmmss"));
            }
        }
        public string StateID { get; set; }
    }

    public class NLServiceProviderList
    {
        public string Name { get; set; }
        public string FullName { get; set; }
    }

    public class StateList
    {
        public string StateID { get; set; }
        public string Name { get; set; }
    }

    public class NLPortingList 
    {
        public string TransactionID { get; set; }
        public string portingId { get; set; }
        public string RecServiceProvider { get; set; }
        public string RecNetworkProvider { get; set; }
        public string DonNetworkProvider { get; set; }
        public string DonServiceProvider { get; set; }
        public string RequestDate { get; set; }
        public string RequestDate_format
        {
            get
            {
                if (RequestDate.Length == 14)
                {
                    string DateString = string.Format("{0}{1}", RequestDate.Substring(0, 4), "/");
                    DateString += string.Format("{0}{1}", RequestDate.Substring(4, 2), "/");
                    DateString += string.Format("{0}{1}", RequestDate.Substring(6, 2), " ");
                    DateString += string.Format("{0}{1}", RequestDate.Substring(8, 2), ":");
                    DateString += string.Format("{0}{1}", RequestDate.Substring(10, 2), ":");
                    DateString += RequestDate.Substring(12, 2);//yyyy/mm/dd HH:mm:ss
                    return DateString;
                }
                else
                {
                    return RequestDate;
                }
            }
        }
        public string NoteOfMsg { get; set; }
        public string StateID { get; set; }
        public string MsgIdentifier { get; set; }
        public string NumberOfItems { get; set; }
        public string ChangeDate { get; set; }
        public string PorposedDate { get; set; }
        public string Initial { get; set; }
        public string Prefix { get; set; }
        public string LastName { get; set; }
        public string CompanyName { get; set; }
        public string Street { get; set; }
        public string HouseNumber { get; set; }
        public string HouseNumberText { get; set; }
        public string HouseNumberExt { get; set; }
        public string Zipcode { get; set; }
        public string City { get; set; }
        public string PortingReqItem { get; set; }
        public string TelpNoSerieStart { get; set; }
        public string TelpNoSerieEnd { get; set; }
        public string NoteOfItem { get; set; }
        public string ICC { get; set; }
        public string Options { get; set; }
        public string BlockingCode { get; set; }
        public string TransitStateID { get; set; }
        public string BlockingText { get; set; }
        public string VectoneNo { get; set; }
        public string networkprovider { get; set; }
        public string statusack { get; set; }
        public string actualdate { get; set; }
        public string requestdate { get; set; }
        public string TypeOfNumbers {get;set;}
    }
}
