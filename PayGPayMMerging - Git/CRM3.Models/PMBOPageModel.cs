﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models
{
    public class PMBOPageModel : CommonModels
    {
        public string ActiveTab { get; set; }
        public string MobileNo { get; set; }
        public string Product_Code { get; set; }
        public string Sitecode { get; set; }
    }
}
