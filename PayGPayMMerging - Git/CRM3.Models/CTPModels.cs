﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models.CTP
{
    public enum Action
    {
        OptionList =1,
        SearchList = 2,
        DetailInfo = 3
    }
    public class SearchEntry
    {
        /*
         * {"Product_Code":"VMUK","SearchBy":"MobileNo","SearchText":"","Search_Type":1}
         */
        public Action ActionType { get; set; }
        public int IdSearchBy { get; set; }
        public string SearchBy { get; set; }
        public string SearchText { get; set; }
        public string Product_Code { get; set; }
        public string Sitecode { get; set; }
    }
    public class SearchOption
    {
        public int Id { get; set; }
        public string Label { get; set; }
        public string Description { get; set; }
    }
}
