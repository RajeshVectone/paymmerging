﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models
{
    public class FreeCreditInput
    {
        public string sitecode { get; set; } 
    }
    public class FreeCreditSubscribeInput  
    {
        public string sitecode { get; set; }
        public string mobileno { get; set; }
        public string iccid { get; set; }
        public int bundleid { get; set; }
        public int paymode { get; set; }
        public int is_promo { get; set; }
        public string process_by { get; set; }
        public string crm_user { get; set; }
        public string price { get; set; }
    }
    public class FreeCreditSubscribeOutput
    {
        public int errcode  { get; set; }
        public string errmsg  { get; set; }
    }
    public class FreeCreditOutput
    {
        public int bundleid { get; set; }
        public string name { get; set; }
        public string price { get; set; }
        public string offer_type { get; set; }
        public string renewal_delay { get; set; }
        public string is_promo { get; set; }
        public string ACTION { get { return ""; } }
    }
}
  
