﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models
{
    public class EmailSettingsModel : CommonModels
    {
        public string template_name { get; set; }
        public int queue_type { get; set; }
        public int queue_per_user { get; set; }
        public DateTime last_update { get; set; }
        public int template_id { get; set; }
        public int processtype { get; set; }
        public EmailSettingsModeType modetype { get; set; }
        public string description { get; set; }
        public string templatefile { get; set; }
        public string sms_text { get; set; }
        public int status { get; set; }
        public int queue_id { get; set; }
        public string email_to { get; set; }
        public string email_cc { get; set; }
        public string email_subject { get; set; }
        public string email_body { get; set; }
        public int is_closed { get; set; }
        public string filename { get; set; }
        

        public int pkqueueid { get; set; }
        public string product_code { get; set; }
        public int process_type { get; set; }
        public string sitecode { get; set; }
        public int userid { get; set; }
        //public int Issue_Id { get; set; }
        //public int Function_Id { get; set; }
        public string emailto { get; set; }
        public string CustomerName { get; set; }
        public string MobileNo { get; set; }
        public int TicketType { get; set; }
        public int Category { get; set; }
        public int SubCategory { get; set; }
        public int Status { get; set; }
        public int EscalatingTeam { get; set; }
        public string EscalatingTeamName { get; set; }
        public string Description { get; set; }

        public int esclation_delay { get; set; }

        public string Issue_Code { get; set; }
        public string Issue_Name { get; set; }
        public string Issue_Desc { get; set; }
        public int issue_type_id { get; set; }

        public int issue_id { get; set; }
        public string function_code { get; set; }
        public string Function_Name { get; set; }
        public int issue_function_id { get; set; }

        public int function_Id { get; set; }
        public string Complaint_Code { get; set; }
        public string Complaint_Name { get; set; }
        public int Complaint_Id { get; set; }

        public int assigned_user_id { get; set; }

        public string compaint_category { get; set; }
    }

    public enum EmailSettingsModeType
    {
        GETRULEASSIGNING = 1,
        UPDATERULEASSIGNING = 2,
        GETSMSASSIGNING = 3,
        UPDATESMSASSIGNING = 4,
        GETEMAILASSIGNING = 5,
        UPDATEEMAILASSIGNING = 6,
        EMAILQUEUEASSIGNEDREPLY = 7,
        SENDMAIL = 8,
        GENEARATETICKET = 9,
        CHANGEEMAILQUEUESTATUS= 10,
        INSERTTICKETTYPE = 11,
        INSERTCATEGORY = 12,
        INSERTSUBCATEGORY = 13,
        //Reset MyAccount Password
        RESETMYACCOUNTPASSWORD = 14
    }

    public class EmailSettingsCommon
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
        public string sitecode { get; set; }
        public int issue_id { get; set; }
        public int Complaint_Id { get; set; }
        public int function_Id { get; set; }
    }

    public class RuleAssigningOutput : EmailSettingsCommon
    {

    }

    public class SMSAssigningOutput
    {
        public int template_id { get; set; }
        public string template_name { get; set; }
        public string sms_text { get; set; }
        public DateTime create_date { get; set; }
        public DateTime last_update { get; set; }
    }

    public class EmailAssigningOutput
    {
        public int template_id { get; set; }
        public string template_name { get; set; }
        public string filename { get; set; }
        public string filename_full { get { return System.IO.Path.Combine(System.Configuration.ConfigurationManager.AppSettings["QUEUEEMAILTEMPLATEPATH"], filename); } }
        public string email_subject { get; set; }
        public int status { get; set; }
        public DateTime create_date { get; set; }
        public DateTime last_update { get; set; }
        public string email_blob { get; set; }
    }
}
