﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;

namespace CRM_API.Models
{
    public class ReportingServicesReportViewModel
    {
        //#region Constructor
        public ReportingServicesReportViewModel(String reportPath, List<ReportParameter> Parameters)
        {
            ReportPath = reportPath;
            parameters = Parameters.ToArray();
        }
        public ReportingServicesReportViewModel()
        {
        }
        //#endregion Constructor

        //#region Public Properties
        public string ddlReportList { get; set; }
        public string ddlApplyLayout { get; set; }
        public string ddPlanType { get; set; }
        public string ddYear { get; set; }
        public string ddMonth { get; set; }
        public string EnterText { get; set; }
        public string ddreportType { get; set; }
        public ReportServerCredentials ServerCredentials { get { return new ReportServerCredentials(); } }
        public String ReportPath { get; set; }
        public Uri ReportServerURL { get { return new Uri(WebConfigurationManager.AppSettings["ReportServerUrl"]); } }
        public ReportParameter[] parameters { get; set; }
        private string UploadDirectory = HttpContext.Current.Server.MapPath("~/App_Data/UploadTemp/");
        private string TempDirectory = HttpContext.Current.Server.MapPath("~/tempFiles/");      
        public class FullName : CommonModels
        {
            public string firstName { get; set; }
            public string lastName { get; set; }
            public string UserName { get; set; }
        }
        public class Reports : CommonModels
        {
            public string ddlReportList { get; set; }
            public string ddlApplyLayout { get; set; }
            public string ddPlanType { get; set; }
            public string ddMonth { get; set; }
        }
    }
}
