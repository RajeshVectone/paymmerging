﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models.Issue
{
    public class IssueModels : IssueList
    {
        public SearchType SearchType { get; set; }
        public int SubscriberID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName
        {
            get
            {
                return string.IsNullOrEmpty(LastName) ? string.Format("{0} %", FirstName) : string.Format("{0}-{1}", FirstName, LastName);

            }
        }
        public string Sitecode { get; set; }
        public string mobileno { get; set; }
        public string Issue_Id { get; set; }
        public string Function_Id { get; set; }
        //public string Issue_Id { get; set; }
        public string Subscriber_ID { get; set; }
        public string Issue_Status { get; set; }
        public string Mobile_No { get; set; }

        //public string Sitecode { get; set; }

        //public string ProblemID { get; set; }
        public string Complaint { get; set; }
        public string Complaint_Id { get; set; }
        public string Issue_Desc { get; set; }
        public string Solution_Desc { get; set; }
        public string Created_By { get; set; }

        public string contact_type { get; set; }
        public string Comp_Status { get; set; }
        public string JIRA_Tickets { get; set; }
        public string intreval { get; set; }
        public int Dead_Call_Status { get; set; }
        public int mode { get; set; }
        public string SIM_Type { get; set; }
        public string Esclated_To { get; set; }
        public string agent_email_id { get; set; }

        //CRM Enhancement 2
        public string interval_comment { get; set; }
        public string dt_from { get; set; }
        public string dt_to { get; set; }

        public string Category { get; set; }
        public string Sub_category { get; set; }
        public string Status { get; set; }
        public string Interval { get; set; }

        public string Function { get; set; }

        public string AssignPriority { get; set; }
        public string Assigned_To { get; set; }
    }

    public enum SearchType
    {
        ProblemListAll = 1,
        ProblemDetail = 2,
        IssueCategory = 3,
        IssueCategoryDetail = 4,
        InsertIssue = 5,
        InsertIssue1 = 12,
        UpdateIssue = 6,
        IssueHistory = 7,
        IssueCategory1 = 8,
        IssueCategory2 = 9,
        IssueCategory3 = 11,
        DeadCall = 13,
        Contacttype = 14,
        compliantpe = 15,
        intervaltype = 16,
        AssignPriority = 17,
        AssignedTo = 18
        //IssueCategory4=12
    }

    public class IssueList : IssueCategory
    {
        public int ProblemID { get; set; }
        public string TypeName { get; set; }
        public string StatusName { get; set; }
        public int ProblemType { get; set; }
        public int ProblemStatus { get; set; }
        public string ProblemDesc { get; set; }
        public string SolutionDesc { get; set; }
        public string EditDate { get; set; }
        public string TypeDesc { get; set; }
        public int CategoryType { get; set; }
        public DateTime SubmitDate { get; set; }
        public string SubmitDate_String { get { return SubmitDate.ToString("dd-MM-yyyy hh:mm:ss tt"); } }
        public string SubmitBy { get; set; }
        public string Action { get; set; }
        public string esclated_to { get; set; }
    }

    public class IssueCategory : Common.ErrCodeMsg
    {
        public int problemcategorytype { get; set; }
        public string categorytypename { get; set; }
        public string categorytypedesc { get; set; }
    }
    public class IssueCategory1 : Common.ErrCodeMsg
    {
        public string Issue { get; set; }
        public string Issue_Id { get; set; }
    }
    public class IssueCategory2 : Common.ErrCodeMsg
    {
        public string Issue_Id { get; set; }
        public string Sitecode { get; set; }
        public string Function { get; set; }
        public string Function_Id { get; set; }
    }
    public class IssueCategoryDetail
    {
        public int problemcategorytype { get; set; }
        public int problemtype { get; set; }
        public string typename { get; set; }
        public string categorytypename { get; set; }
    }
    public class IssueCategory3 : Common.ErrCodeMsg
    {
        public string Sitecode { get; set; }
        public string Function_Id { get; set; }
        public string Complaint { get; set; }
        public string Complaint_Id { get; set; }
    }
    public class InsertIssue1 : Common.ErrCodeMsg
    {
        public string Issue_Id { get; set; }
        public string Subscriber_ID { get; set; }
        public string Issue_Status { get; set; }
        public string Mobile_No { get; set; }
        public string Sitecode { get; set; }

        public string Function_Id { get; set; }
        public string Complaint { get; set; }
        public string Complaint_Id { get; set; }
        public string Issue_Desc { get; set; }
        public string Solution_Desc { get; set; }
        public string Created_By { get; set; }
        public string ProblemID { get; set; }
    }
    public class IssueHistory
    {
        public int problemid { get; set; }
        public DateTime editdate { get; set; }
        public string editdate_string { get { return editdate.ToString("dd-MM-yyyy hh:mm:ss tt"); } }
        public string status { get; set; }
        public string solutiondesc { get; set; }
    }

    public class Contact_Type
    {
        public string contact_type { get; set; }
        public int status { get; set; }
    }
    public class Complaint_type
    {
        public string Complaint_Status { get; set; }
        public int status { get; set; }
    }
    public class timeinterval
    {
        public string time_interval { get; set; }
        public int status { get; set; }
    }
    public class AssignedTo
    {
        public string Email { get; set; }
        public int Id { get; set; }
        public string Username { get; set; }
        
    }
    public class Priority
    {
        public int fk_priority_id { get; set; }
        public string priority { get; set; }

    }
    public class IssueHistoryDetailInput
    {

        public int problemid { get; set; }
        public string Sitecode { get; set; }

    }
    public class IssueHistoryDetailOutput
    {

        public DateTime submitdate { get; set; }
        public string submitdate_string { get { return submitdate.ToString("dd-MM-yyyy hh:mm:ss tt"); } }
        public string submitby { get; set; }
        public string problemdesc { get; set; }
        public string typedesc { get; set; }
        public int problemid { get; set; }
        public int Dead_Call_Status { get; set; }
        public string esclated_to { get; set; }
        public string contact_type { get; set; }
        public string email { get; set; }
        public string Category { get; set; }
        public string Sub_category { get; set; }
        public string Status { get; set; }
        public int Interval { get; set; }
        public string priority { get; set; }

    } 

}
