﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models
{
    public class MyAccountLogin:Common.ErrCodeMsg
    {
        public string Company_Id { get; set; }
        public string Company_Name { get; set; }
        public string Id { get; set; }
        public string Full_Name { get; set; }
        public string Flag { get; set; }
        public string AppType { get; set; }
        public string Extension { get; set; }
    }
}
