﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models.SIM
{
    public class SIMInfo : Common.ErrCodeMsg
    {
        public string msisdn { get; set; }
        public string iccid { get; set; }
        public string activeimsi { get; set; }
        public string masterimsi { get; set; }
        public string sitecode { get; set; }
        public string status { get; set; }
        //anees 11Oct2018
        public string vendor { get; set; }
        public string simtype { get; set; }
        public DateTime oldsim_firstupd { get; set; }
        public string oldsim_firstupd_string { get { return oldsim_firstupd.ToString("dd-MM-yyyy hh:mm:ss tt"); } }
        public DateTime oldsim_lastupd { get; set; }
        public string oldsim_lastupd_lastupd { get { return oldsim_lastupd.ToString("dd-MM-yyyy hh:mm:ss tt"); } }

        public string strstatus { get; set; }
        public string odbout { get; set; }
        public string odbin { get; set; }
        public string mnrf { get; set; }
        public string mcef { get; set; }
        public DateTime firstauth { get; set; }
        public string firstauth_string { get { return firstauth.ToString("dd-MM-yyyy hh:mm:ss tt"); } }
        public DateTime firstupd { get; set; }
        public string firstupd_string { get { return firstupd.ToString("dd-MM-yyyy hh:mm:ss tt"); } }
        public DateTime lastupd { get; set; }
        public string lastupde_string { get { return lastupd.ToString("dd-MM-yyyy hh:mm:ss tt"); } }
        public string vlr { get; set; }
        public string last_loc { get; set; }
        public string current_loc { get; set; }
        public PinInfo pinInfo { get; set; }
        public Reasoninfo reasoninfo { get; set; }
        public SIMVersion SIMVersion { get; set; }
        public CallFowardInfo callFoward { get; set; }
        public string Roaming_comments { get; set; }
    }
    public class PinInfo
    {
        public int idx { get; set; }
        public string msisdn { get; set; }
        public string imsi { get; set; }
        public string iccid { get; set; }
        public string pin1 { get; set; }
        public string pin2 { get; set; }
        public string puk1 { get; set; }
        public string puk2 { get; set; }
    }
    public class Reasoninfo
    {
        public string reason { get; set; }
    }
    public class SIMVersion
    {
        public string sim_Version { get; set; }
    }
    public class CallFowardInfo : Common.ErrCodeMsg
    {
        public string Iccid { get; set; }
        public string MobileNo { get; set; }
        public string Sitecode { get; set; }
        public int CFWType { get; set; }
        public CallFowardType eCFWType { get; set; }
        public string CFWTypeName { get; set; }
        private string _forwardToNb;
        public string ForwardToNb
        {
            get {
                if (!string.IsNullOrEmpty(_forwardToNb) ? (_forwardToNb.Substring(0, 2) == CountryCode || _forwardToNb.Substring(0, 2) == "01") : false)
                    return _forwardToNb.Substring(_forwardToNb.IndexOf(CountryCode) + CountryCode.Length);
                else
                    return _forwardToNb;
            }
            set { _forwardToNb = value; }
        }
        //public string FowardToNb_sub
        //{
        //    get
        //    {
        //        if (!string.IsNullOrEmpty(FowardToNb_sub))
        //            return ForwardToNb.Substring(ForwardToNb.IndexOf(CountryCode) + CountryCode.Length);
        //        else
        //            return string.Empty;
        //    }
        //}
        public int NoReplyTimer { get; set; }
        public CallFowardStatus CFWStatus { get; set; }
        public string CFWStatusName { get; set; }
        public int SmsFlag { get; set; }
        public string CountryCode { get; set; }
    }
    public class SimInfo_Request : Common.ErrCodeMsg
    {
        public int Id { get; set; }
        public string Msisdn { get; set; }
        public string Iccid { get; set; }
        public string Sitecode { get; set; }
        public SimInfoType InfoType { get; set; }
    }

    public enum SimInfoType
    {
        SimInfo = 1,
        CallFoward = 2,
        PinInfo = 3,
        SetCallFoward = 5
    }
    public enum CallFowardType
    {
        CallForwardBusy = 41,
        CallForwardNoReply = 42,
        CallForwardUnreachable = 43,
        CallFowardUnconditional = 33
    }
    public enum CallFowardStatus
    {
        Disabled = 0,
        EnableVoiceMail = 1,
        EnabledDestNumber = 2
    }
}
