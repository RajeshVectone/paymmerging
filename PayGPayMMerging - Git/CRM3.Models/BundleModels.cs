﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models.Bundle
{
    public class SubscribedBundle : Common.ErrCodeMsg
    {
        public int bundleid { get; set; }
        public string name { get; set; }
        public string bundle_type { get; set; }
        public string price { get; set; }
        public string renewal_mode { get; set; }
        public string renewal_delay { get; set; }
        public string sendmsg_mode { get; set; }
        public string action { get { return string.Format("do_unsubscribe({0})", this.bundleid); } }
        public string action2 { get { return string.Format("do_adddata({0})", this.bundleid); } }
        public string group_type { get { return string.Format("group_{0}", this.bundleid); } }
        //Added by karthik Jira:CRMT-196
        public DateTime subscription_date { get; set; }
        public string subscriptiondate_string
        {
            get
            {
                return subscription_date.ToString("dd-MM-yyyy hh:mm:ss tt");
            }
        }
        public DateTime renewal_date { get; set; }
        //public string status { get; set; }

        public Boolean DisplayCancel { get { if (Status == "Active") return true; else return false; } }
        //for testing purpose changed
        public Boolean DisplayReactivate { get { if (Status == "Cancelled") return true; else return false; } }
        //public Boolean DisplayReactivate { get { if (status == "Active") return true; else return false; } }
        /*End added*/
        public string renewaldate_string
        {
            get
            {
                return renewal_date.ToString("dd-MM-yyyy hh:mm:ss tt");
            }
        }
        public string Status { get; set; }
        //29-Oct-2018 : Moorthy : Added two new fields
        public string assigned_allowance { get; set; }
        public string avaialable_allowance { get; set; }
        //public string assigned_allowance { get { return "1/2/3/4"; } }
        //public string avaialable_allowance { get { return "5/6/7/8"; } }

        public DateTime? auto_renewal_date { get; set; }
        public string auto_renewal_status
        {
            get
            {
                return auto_renewal_date == null ? "Active" : (!String.IsNullOrEmpty(auto_stop_by) ? "Via " + auto_stop_by + " cancelled on " + Convert.ToDateTime(auto_renewal_date).ToString("dd-MM-yyyy hh:mm:ss tt") : Convert.ToDateTime(auto_renewal_date).ToString("dd-MM-yyyy hh:mm:ss tt"));
            }
        }
        public string auto_stop_by { get; set; }

        public int is_limited_min_flag { get; set; }
        public int config_id { get; set; }
        public string actionlink { get { return ""; } }

        //27-Dec-2018 : Moorthy : Exchange Bundles
        public string action3 { get { return string.Format("do_exchangebundles({0})", this.bundleid); } }

        public string action4 { get { return string.Format("do_Adjustbundles({0})", this.bundleid); } }

        public string payment_reference { get; set; }
        public string purchased_by { get; set; }

        //CRM Enhancement 2
        public string international_tariff { get; set; }
    }

    public class AvailableBundle : Common.ErrCodeMsg
    {
        public BundleType Bundle_Type { get; set; }
        public float balance { get; set; }
        public string bundle_name { get; set; }
        public string tariffclass { get; set; }
        public int bundleid { get; set; }
        public string name { get; set; }
        public string price { get; set; }
        public string renewal_mode { get; set; }
        public string renewal_delay { get; set; }
        public string sendmsg_mode { get; set; }
        public string action { get { return string.Format("do_subscribe({0})", this.bundleid); } }
        public string action2 { get { return string.Format("do_adddata({0})", this.bundleid); } }
        public string group_type { get { return string.Format("group_{0}", this.bundleid); } }
        public string ussd_code { get; set; }
        public string ussd_balance_check { get; set; }

        //CRM Enhancement 2
        public string category_name { get; set; }

        public string new_customer_price { get; set; }
    }
    public class BundleSearch : Common.ErrCodeMsg
    {
        public int id { get; set; }
        public BundleType Bundle_Type { get; set; }
        public float balance { get; set; }
        public string bundle_name { get; set; }
        public string tariffclass { get; set; }
        public string msisdn { get; set; }
        public string iccid { get; set; }
        public string sitecode { get; set; }
        public string productcode { get; set; }
        public int bundleid { get; set; }
        public int usertype { get; set; }
        public int paymode { get; set; }
        public string processby { get; set; }
        public string pack_dest { get; set; }
        public UnsubscribeType UnsubscribeType { get; set; }

        public int config_id { get; set; }

        //27-Dec-2018 : Moorthy : Exchange Bundles
        public int current_bundleid { get; set; }
        public int new_bundleid { get; set; }
        public int process_flag { get; set; }
        public string crm_user { get; set; }

        //CRM Enhancement 2
        public string international_tariff { get; set; }
        public string subscription_date { get; set; }
    }
    //added by karthik CRMT-196 Internet profile 
    public class internetprofile : Common.ErrCodeMsg
    {
        public string IMSI { get; set; }
        public DateTime FirstAuthentication { get; set; }
        // public string APN { get { return string.Format("IMSI_{0}", this.IMSI); } }
        public string Firstautdate_string
        {
            get
            {
                return FirstAuthentication.ToString("dd-MM-yyyy hh:mm:ss tt");
            }
        }
        public DateTime FirstGPRSUpdate { get; set; }
        public string GPRSUpdate_string
        {
            get
            {
                return FirstGPRSUpdate.ToString("dd-MM-yyyy hh:mm:ss tt");
            }
        }
        public string ExistOCSI { get; set; }
        //Modif by karthik Jira:CRMT-196
        public string APN { get; set; }
        public string profile_value { get; set; }
        public string IMSI_Status { get; set; }
        // public string Action { get { return string.Format("Action_{0})", this.IMSI); } }
    }

    public enum BundleType
    {
        ViewAvailableBundle = 1,
        ViewSubscribedBundle = 2,
        SubscribeBundle = 3,
        UnsubscribeBundle = 4,
        //Added by karthik Jira:CRMT-196
        InternetProfile = 5,
        GPRSSetting = 6,
        InternetProfileSMS = 7,
        GetLimitedMinInfoByBundleId = 8,
        ExchangeBundle = 9
    }

    public enum UnsubscribeType
    {
        expirydate = 1,
        immediately = 2
    }

    //Bundle Name Click
    public class LimitedMinInfoByBundleIdRef
    {
        public string country { get; set; }
        public string country_code { get; set; }
        public int bundle_min_category { get; set; }
        public string category_name { get; set; }
        public string @fixed { get; set; }
        public string mobile { get; set; }
        public string bundle_call { get; set; }

        //CRM Enhancement 2
        public string fixed_used_min { get; set; }
        public string mobile_used_min { get; set; }
        public string country_type { get; set; }
        public string errmsg { get; set; }
    }

    //27-Dec-2018 : Moorthy : Exchange Bundles
    public class ExchangeBundleRef
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
    }
}
