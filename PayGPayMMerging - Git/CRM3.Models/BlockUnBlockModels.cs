﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models.BlockUnBlockModels
{
    public class BlockUnblock : Common.ErrCodeMsg
    {
        public BlockUnBlockmode Block_Type { get; set; }
        public string Process_By { get; set; }
        public string product { get; set; }
        public string sitecode { get; set; }
        public int type { get; set; }
        public int mode { get; set; }
        public string reason { get; set; }
        public string account_info { get; set; }

        public DateTime StartDate { get; set; }
        public string Date_From_date_string { get { return StartDate.ToString("yyyy-MM-dd"); } }
        public string Date_from_month_string { get { return StartDate.ToString("MM"); } }
        public string Date_from_day_string { get { return StartDate.ToString("dd"); } }
        public string Date_from_year_string { get { return StartDate.ToString("yyyy"); } }

        public DateTime EndDate { get; set; }

        public string Date_To_date_string { get { return EndDate.ToString("yyyy-MM-dd"); } }
        public string Date_to_month_string { get { return EndDate.ToString("MM"); } }
        public string Date_To_day_string { get { return EndDate.ToString("dd"); } }
        public string Date_To_year_string { get { return EndDate.ToString("yyyy"); } }

        public string formData { get; set; }

        public string vlr_number { get; set; }
        public string imsi_active { get; set; }
        public string url { get; set; }
    }

    public class BlockUnBlockReports
    {
        public string Batch_Id { get; set; }
        public string Mobile_No { get; set; }
        public string Iccid { get; set; }
        public string brand { get; set; }
        public string Process_By { get; set; }

        public DateTime Log_Date { get; set; }
        public string Log_Datestring { get { return Log_Date.ToString("dd-MM-yyyy hh:mm:ss tt"); } set { Log_Date = DateTime.ParseExact(value, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture); } }
        
        public string master_bal { get; set; }
        public string reason { get; set; }
        public int mode { get; set; }
        public string Currency { get; set; }
    }
    public class BlukBlock:Common.ErrCodeMsg
    {
        public string ICCID { get; set; }
        public string Mobile { get; set; }
    }
    public enum BlockUnBlockmode
    {
        type = 1,
        BlockUnblock = 2,
        Reports=3
        
    }
}
