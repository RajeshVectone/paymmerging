﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models
{
    public class EmailGenerateTicketModelsCommon
    {
        public int errcode { get; set; }
        public int errmsg { get; set; }
    }

    public class EmailGenerateTicketModels : CommonModels
    {
        public string product_code { get; set; }
        public int process_type { get; set; }
        public string sitecode { get; set; }
        public int userid { get; set; }
        public int Issue_Id { get; set; }
        public int Function_Id { get; set; }
        public string emailto { get; set; }
        public int pkqueueid { get; set; }

        public string CustomerName { get; set; }
        public string MobileNo { get; set; }
        public int TicketType { get; set; }
        public int Category { get; set; }
        public int SubCategory { get; set; }
        public int Status { get; set; }
        public int EscalatingTeam { get; set; }
        public string EscalatingTeamName { get; set; }
        public string Description { get; set; }

        public int role_id { get; set; }

        public GenerateTicketModeType ModeType { get; set; }
    }

    public class EscalationTeamInfo
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
    }

    public class TicketTypeInfo
    {
        public int Issue_Id { get; set; }
        public string Issue_Name { get; set; }
        public string Issue_Code { get; set; }
    }

    public class CategoryInfo
    {
        public int Function_Id { get; set; }
        public string Function_Name { get; set; }
        public string Function_Code { get; set; }
    }

    public class SubCategoryInfo
    {
        public int Complaint_Id { get; set; }
        public string Complaint_Name { get; set; }
        public string Complaint_Code { get; set; }
    }

    public class StatusInfo
    {
        public int status_id { get; set; }
        public string status { get; set; }
        public string Complaint_Code { get; set; }
    }

    public class GenerateTicketOutput
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
        public int userid { get; set; }
        public string ticket_id { get; set; }
        public string sms_text { get; set; }
        public string sms_url { get; set; }
        public string sms_org_address { get; set; }
        public string email_filename { get; set; }
        public string email_sub { get; set; }
    }

    public class ViewTicketOutput
    {
        public string Ticket_id { get; set; }
        public string product_code { get; set; }
        public string mobileno { get; set; }
        public string Issue_Name { get; set; }
        public string Function_Name { get; set; }
        public string Complaint_Name { get; set; }
        public DateTime issue_generate_date { get; set; }
        public string issue_generate_date_string { get { return issue_generate_date != null ? issue_generate_date.ToString("dd/MM/yyyy hh:mm:ss tt") : ""; } }
        public string descr { get; set; }
        public int issue_type { get; set; }
        public int Issue_Function { get; set; }
        public int Issue_Complaint { get; set; }
        public string customer_name { get; set; }
        public int queue_status { get; set; }
        public int esclating_user { get; set; }
        public string queue_status_txt { get; set; }
        public string esclating_user_txt { get; set; }
        public int queue_id { get; set; }
    }

    public enum GenerateTicketModeType
    {
        TICKETTYPE = 1,
        CATEGORY = 2,
        CONTACTTYPE = 3,
        SUBCATEGORY = 4,
        STATUS = 5,
        PRIORITY = 6,
        ESCALATIONTEAM = 7,
        NOTIFYTEAM = 8,
        //GENERATETICKET =9,
        VIEWTICKET = 10
    }
    public class PriorityOutput
    {
        public string priority_id { get; set; }
        public string priority { get; set; }

    }
    public class PriorityInput
    {
        public string sitecode { get; set; }
    }
}
