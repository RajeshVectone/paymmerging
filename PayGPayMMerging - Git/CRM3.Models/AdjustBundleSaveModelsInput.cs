﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace CRM_API.Models
{
    public class AdjustBundleSaveModelsInput : Common.ErrCodeMsg
    {
        public string sitecode { get; set; }
        public int bundleid { get; set; }
        public string mobileno { get; set; }
    }
    public class AdjustBundleSaveModelsOutput: Common.ErrCodeMsg
    {
        public int package_id { get; set; }
        public string pack_type { get; set; }
        public double? assigned_balance { get; set; }
        public double? remain_balance { get; set; }
        public string comment { get; set; }
        public string m_cc { get; set; }
        public int m_bc { get; set; }
        public int m_sc { get; set; }
        public string AdjustUsage { get; set; }
        public string Amount { get; set; }
        public string AdjustSave { get; set; }

    }
}