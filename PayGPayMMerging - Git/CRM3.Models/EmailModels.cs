﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models
{
    public class EmailModelCommon
    {
        public int errcode { get; set; }
        public int errmsg { get; set; }
    }

    public class EmailModel : CommonModels
    {
        public int roleid { get; set; }
        public string customer_email_id { get; set; }
        public string mailLocation { get; set; }
        public int request_id { get; set; }
        public int Id
        {
            get
            {
                return 1;
            }
        }
        public string email_from { get; set; }
        public string email_to { get; set; }
        public string email_cc { get; set; }
        public string status { get; set; }
        public string agent_name { get; set; }
        public string email_subject { get; set; }
        public string email_subject_trim
        {
            get
            {
                return email_subject != null && email_subject.Length > 50 ? email_subject.Substring(0, 46) + "..." : email_subject;
            }
        }
        public string email_body { get; set; }
        public string email_body_trim
        {
            get
            {
                return email_body != null && email_body.Length > 100 ? email_body.Substring(0, 96) + "..." : email_body;
            }
        }
        public int no_of_attachaments { get; set; }
        public string attachment_location { get; set; }
        public DateTime? create_date { get; set; }
        public string createDate_string { get { return create_date != null ? create_date.Value.ToString("dd/MM/yyyy hh:mm:ss") : ""; } }

        public string ActiveTab { get; set; }
        public ModeType modetype { get; set; }
        public int userid { get; set; }
        public int queue_status { get; set; }
        public int process_type { get; set; }
        public int queue_id { get; set; }
        public string request_id_selected { get; set; }
        public int assigned_user { get; set; }
        public string Assiged_agent { get; set; }
        public string Ticket_id { get; set; }
        public int? pk_queue_id { get; set; }
        public int is_closed { get; set; }
        public string filename { get; set; }
        public List<EmailAttachments> lstEmailAttachments { get; set; }

        public int role_id { get; set; }
        public int status_id { get; set; }
    }

    public class EmailAttachments
    {
        public string filename { get; set; }
        public string fileurl { get; set; }
        public string extension { get; set; }
    }

    //public class EmailEueueAssignToUserOutput
    //{
    //    public int errcode { get; set; }
    //    public string errmsg { get; set; }
    //    public int queue_id { get; set; }
    //    public string email_sub { get; set; }
    //    public string email_body { get; set; }
    //}

    public class EmailQueueSaveReplyTextOutput
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
    }

    public enum ModeType
    {
        GETEMAILQUEUEINFO = 1,
        //EMAILQUEUEASSIGNTOUSER = 2,
        GETAGENTINFO = 3,
        EMAILQUEUESAVEREPLYTEXT = 4,
        GETEMAILQUEUEASSIGNEDINFO = 5,
        EMAILQUEUECHANGESTATUS = 6,
        EMAILQUEUEASSIGNEDREPLY = 7,
        GETMYEMAILOPENMAILITEMS = 8,
        GETMYEMAILOPENTICKETITEMS = 9,
        GETEMAILVIEWTICKETSBYUSER = 10,
        REMOVEEMAILFROMLIST = 11
        //CHANGEEMAILQUEUESTATUS = 11
    }

    public class AgentInfoModels
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
    }

    public class GetEmailQueueAssignedInfoOutput
    {
        public int pk_queue_id { get; set; }
        public int request_id { get; set; }
        public string email_subject { get; set; }
        public string email_subject_trim
        {
            get
            {
                return email_subject != null && email_subject.Length > 50 ? email_subject.Substring(0, 46) + "..." : email_subject;
            }
        }
        public string assigned_user_email { get; set; }
        public string email_body { get; set; }
        public string email_body_trim
        {
            get
            {
                return email_body != null && email_body.Length > 100 ? email_body.Substring(0, 96) + "..." : email_body;
            }
        }
        public string assigned_user { get; set; }
        //public string assigned_by { get; set; }
        public string queue_status { get; set; }
        public DateTime assigned_date { get; set; }
        public DateTime complete_date { get; set; }
        public string assigned_date_string { get { return assigned_date.ToString("yyyy-MM-dd hh:mm:ss"); } }
        public string complete_date_string { get { return complete_date.ToString("yyyy-MM-dd hh:mm:ss"); } }
        public string reply_text { get; set; }
        public string customer_email { get; set; }
        public string action { get { return string.Format("do_generate({0})", this.pk_queue_id); } }
        public string action2 { get { return string.Format("do_open({0})", this.pk_queue_id); } }
        public string action3 { get; set; }
        public int queue_status_id { get; set; }
    }

    public class GetMyEmailOpenItemsOutput
    {
        public string email_date { get; set; }
        public string email_subject { get; set; }
        public string email_body { get; set; }
        public string email_body_html
        {
            get
            {
                return !String.IsNullOrEmpty(email_body) ? email_body.Replace("\r\n", "<br/>") : "";
            }
        }
        public int no_of_attachaments { get; set; }
        public List<EmailAttachments> lstEmailAttachments { get; set; }
    }

    public class GetEmailQueueCustomerHistoryOutput
    {
        public string Ticket_id { get; set; }
        public DateTime issue_generate_date { get; set; }
        public string issue_generate_date_string { get { return issue_generate_date.ToString("yyyy-MM-dd hh:mm:ss"); } }
        public string email_body { get; set; }
        public string email_body_trim
        {
            get
            {
                return email_body != null && email_body.Length > 100 ? email_body.Substring(0,96) + "..." : email_body;
            }
        }
        public int queue_id { get; set; }
    }

    //My Tickets
    public class GetEmailViewTicketsByUserOutput
    {
        public string Ticket_id { get; set; }
        public string product_code { get; set; }
        public string mobileno { get; set; }
        public string Issue_Name { get; set; }
        public string Function_Name { get; set; }
        public string Complaint_Name { get; set; }
        public DateTime issue_generate_date { get; set; }
        public string issue_generate_date_string { get { return issue_generate_date.ToString("yyyy-MM-dd hh:mm:ss"); } }
        public string descr { get; set; }
        public string descr_trim
        {
            get
            {
                return descr != null && descr.Length > 250 ? "<div style='height: 35px;overflow:hidden;'>" + descr + "</div>" : descr;
            }
        }
        public int issue_type { get; set; }
        public int Issue_Function { get; set; }
        public int Issue_Complaint { get; set; }
        public string customer_name { get; set; }
        public int queue_status { get; set; }
        public int esclating_user { get; set; }
        public string queue_status_txt { get; set; }
        public string esclating_user_txt { get; set; }
        public int queue_id { get; set; }
        public string agent_email { get; set; }
        public string cust_email { get; set; }
        public string assigned_from { get; set; }
        public string compaint_category { get; set; }
    }

    public class ChangeEmailQueueStatusOutput
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
        public string email_filename { get; set; }
        public string email_sub { get; set; }
        public string emailto { get; set; }
        public string email_from { get; set; }
    }
}
