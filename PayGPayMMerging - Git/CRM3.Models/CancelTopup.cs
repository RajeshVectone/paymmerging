﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models
{
    //CancelTopup
    public class CancelTopupInput
    {
        public string sitecode { get; set; }
        public string mobileno { get; set; }
        public string paymentRef { get; set; }
        public float Amount { get; set; }
        public string Reason { get; set; }
        public string requestedby { get; set; }

        public string date_from { get; set; }
        public string date_to { get; set; }

        public CancelTopupReportModeType CancelModeType { get; set; }

        public int process_type { get; set; }
    }

    public class CancelTopupOutput
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
    }

    public enum CancelTopupReportModeType
    {
        INSERTCANCELTOPUP = 1,
        GETCANCELTOPUPREPORT = 2,
        UPDATECANCELTOPUP = 3
    }

    public class CancelTopupReportOutput
    {
        public DateTime? REQUEST_DATE { get; set; }
        public string REQUEST_DATE_String { get { return REQUEST_DATE != null ? REQUEST_DATE.Value.ToString("dd/MM/yyyy hh:mm:ss") : ""; } }
        public DateTime? REFUND_DATE { get; set; }
        public string REFUND_DATE_String { get { return REFUND_DATE != null ? REFUND_DATE.Value.ToString("dd/MM/yyyy hh:mm:ss") : ""; } }
        public string MOBILENO { get; set; }
        public string PAYMENT_REF { get; set; }
        public double AMOUNT { get; set; }
        public double PREV_BALANCE { get; set; }
        public double AFTER_BALANCE { get; set; }
        public string REASON { get; set; }
        public string REFUND_BY { get; set; }
        public string ACTION { get { return ""; } }
        public string REQUEST_BY { get; set; }
        public string sitecode { get; set; }
        public int STATUS { get; set; }
    }
}
