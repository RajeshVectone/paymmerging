﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models.Package
{
    public class PackageList:Common.ErrCodeMsg
    {
        public PackageType package_type { get; set; }
        public string mobileno { get; set; }
        public string status { get; set; }
        public string custcode { get; set; }
        public int batchcode { get; set; }
        public int serialcode { get; set; }
        public int packageid { get; set; }
        public string packagename { get; set; }
        public float balance { get; set; }
        public DateTime startdate { get; set; }
        public string startdate_string { get { return startdate.ToString("dd-MM-yyyy hh:mm:ss tt"); } }
        //Added by karthik JIra CRMT-198
        public string startdate_str { get { return startdate.ToString("dd/MMM/yyyy"); } }

        public DateTime expdate { get; set; }
        //Added by karthik Jira CRMT-198
        public string expdate_str { get { return expdate.ToString("dd/MMM/yyyy"); } }
        //Added by karthik Jira CRMT-198
        public string tariffClass { get; set; }

        public string expdate_string { get { return expdate.ToString("dd-MM-yyyy hh:mm:ss tt"); } }
        public string statusname { get; set; }
        public string packageTypeID { get; set; }
        public string packageTypeName { get; set; }
        public string comment { get; set; }
        public string m_custcode { get; set; }
        public int m_batchcode { get; set; }
        public int m_serialcode { get; set; }
        public string action { get { return string.Format("action({0})", packageid); } }
    }
    public class PackageSearch:Common.ErrCodeMsg
    {
        public int id { get; set; }
        public string msisdn { get; set; }
        public string iccid { get; set;}
        public string status { get; set; }
        public string sitecode { get; set; }
        public PackageType package_type { get; set; }
    }
    public enum PackageType
    {
        GetPackageList = 1
    }
}
