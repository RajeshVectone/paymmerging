﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models.Porting.AT
{
    public class PortinEntry : Common.ErrCodeMsg
    {
        public string Title { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Houseno { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string Postcode { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string CountryCode { get; set; }
        public string Telephone { get; set; }
        public string BBMsisdn { get; set; }
        public string CompanyName { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public DateTime DOB { get; set; }
        public int SubscriberType { get; set; }
        public int CustomerType { get; set; }
        public string ReceipientOperator { get; set; }
        public string ReceipientOperatorCode { get; set; }
        public string PortedMsisdn { get; set; }
        public string PUK { get; set; }
        public string VoiceMail { get; set; }
        public string PortingCode { get; set; }
        public string Donor { get; set; }
        public string DonorCode { get; set; }
        public DateTime DesiredPortedDate { get; set; }
        public string CancelCode { get; set; }
        public PortingAction ActionType { get; set; } 
        public string Sitecode { get; set; }
        public string Msisdn { get; set; }
    }
    public class AdditionalReqInfo:Common.ErrCodeMsg
    {
        public AdditionInfo InfoType { get; set; }
        public string Sitecode { get; set; }
        public string PortingCode { get; set; }
        public string Msisdn { get; set; }
    }
    public enum PortingAction
    {
        Testing = -8,
        RequestPorting = 1,
        RequestCancel = 2,
        RequestNeuvInfoList = 3,
        SendNeuvInfo = 5,
        TransactionReport = 8
    }
    public enum AdditionInfo
    {
        ListTitle = 1,
        ListCountry = 2,
        ListSubscriberType =3,
        ListDonor = 5,
        ListCancel = 7,
        ListReceipientOp = 8
    }
    public class PortingList : CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Transactions
    {
        public string CancelCode { get; set; }
        public string DesiredPortDate { get; set; }
        public int GCode { get; set; }
        public string GDesc { get; set; }
        public string ICCID { get; set; }
        public int LCode { get; set; }
        public string LDesc { get; set; }
        public int MsgStatus { get; set; }
        public string Msisdn { get; set; }
        public string MsisdnEnd { get; set; }
        public string MsisdnStart { get; set; }
        public string PortingCode { get; set; }
        public string PortingDate { get; set; }
        public int PortingType { get; set; }
        public string PrevMsisdn { get; set; }
        public string RequestId { get; set; }
        public int RoutingCount { get; set; }
        public string VoiceMail { get; set; }
        public string ReceipientOperator {get;set;}
    }
}
