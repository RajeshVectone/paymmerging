﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace CRM_API.Models
{
    public class Customer2in1
    {
        public string MobileNo { get; set; }
        public IEnumerable<Customer2in1Data> DataList { get; set; }

        public Customer2in1(string mobileNo)
        {
            this.MobileNo = mobileNo;
        }
    }

    public class Customer2in1Data
    {
        public int OrderId { get; set; }
        public string ICCID { get; set; }
        public string IMSI { get; set; }
        public string Country { get; set; }
        public string Status { get; set; }
        public string MobileNo { get; set; }
        public DateTime ActivationDate { get; set; }
    }

    public class Customer2in1Order
    {
        public string OrderCountryCode { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string PostCode { get; set; }
        public string City { get; set; }
        public string CountryCode { get; set; }
        public string Email { get; set; }
        public string Contact { get; set; }
    }

    public enum Customer2in1OrderType
    {
        Default = 0,
        NewOrderByBalance = 1,
        NewOrderByCard = 2,
        Cancellation = 3,
        NewCountryList = 4,
        GetCountryCurrency = 5,
        NewOrder = 10,
        ActivateOTA = 11,
        ActivateOTAFree = 100,
        ActivateOTAVectoneLocal2in1 = 101,
        ActivateOTAVectone2in1 = 102,
        ActivateSWAP = 12,
        ChangePaymentToBalance = 13,
        ChangePaymentToCard = 14,
        ReActivateByBalance = 21,
        ReActivateByCard = 22,
        InsertSIMOrder = 23,
        ReactivationNoPayment = 24
    }

    public class Customer2in1Records : CRM_API.Models.Payment.CardDetails
    {
        public Customer2in1OrderType order_type { get; set; } 
        public int order_id { get; set; }
        public string mobileno { get; set; }
        public string imsi_country { get; set; }
        public string imsi_mobileno { get; set; }
        public string swapiccid { get; set; }
        public DateTime order_date { get; set; }
        public string order_date_string { get; set; }
        public string order_status { get; set; }
        public DateTime process_date { get; set; }
        public string process_date_string
        {
            get
            {
                if (process_date == DateTime.MinValue)
                    return "-";
                else
                    return process_date.ToString("dd-MM-yyyy");
            }
        }
        public DateTime expired_date { get; set; }
        public string expire_date_string
        {
            get
            {
                if (expired_date == DateTime.MinValue || order_status.ToLower() != "active")
                    return "-";
                else
                    return expired_date.ToString("dd-MM-yyyy");
            }
        }
        public string renewal_date_string
        {
            get
            {
                if (expired_date == DateTime.MinValue || order_status.ToLower() != "active")
                    return "-";
                else
                    return expired_date.AddDays(1).ToString("dd-MM-yyyy");
            }
        }
        public int renewal_method { get; set; }
        public string renewal_method_string { get; set; }
        public string currency { get; set; }
        public double monthly_charge { get; set; }
        public string monthly_charge_string { get; set; }
        public string imsi_location { get; set; }
        public string imsi { get; set; }
        public string title { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string address2 { get; set; }
        public string address1 { get; set; }
        public string town { get; set; }
        public string postcode { get; set; }
        public string country { get; set; }
        public int id_type { get; set; }
        public string id_number { get; set; }
        public string email { get; set; }
        public string contact { get; set; }
        public int can_be_activated { get; set; }
        public string new_iccid_to_swap { get; set; }
        public string crm_login { get; set; }
        public string payment_ref { get; set; }
        public DateTime date_of_birth { get; set; }
        public string product_code { get; set; }
        public string sitecode { get; set; }
        public string brand { get; set; }
        public string order_source { get; set; }
        public string order_url { get; set; }
    }

    public class OTAString : Common.ErrCodeMsg
    {
        public int ota_job_id { get; set; }
        public int seq_no { get; set; }
        public string UDL { get; set; }
        public string TpuD { get; set; }
    }

    public class SMSMsgReturn : Common.ErrCodeMsg
    {
        public string sms_msg { get; set; }
    }

    public class Customer2in1Swap : Common.ErrCodeMsg
    {
        public int order_id { get; set; }
        public string mobileno { get; set; }
        public string swapiccid { get; set; }
        public string crm_login { get; set; }
    }

    public class SimOrder : Common.ErrCodeMsg
    {
        public int order_id { get; set; }
    }
    public class ErrCodeMsg1
    {
        public int errcode { get; set; }
        public string err_message { get; set; }

    }
    public class Country2in1Currency
    {
        public string Currency { get; set; }
        public double amount { get; set; }
        public string amount_string
        {
            get
            {
                return string.Format("{0:N}", amount).Replace(",", ".");
            }
        }
    }
    public class Country2in1Command
    {
        public string SPI { get; set; }
        public string Iccid { get; set; }
        public string KIC { get; set; }
        public string KID { get; set; }
        public string TAR { get; set; }
        public string Counter { get; set; }
        public string Cmd1 { get; set; }
        public string Cmd2 { get; set; }
        public string Cmd3 { get; set; }
        public string Cmd4 { get; set; }
        public string kc_Value { get; set; }
        public string Cmd5 { get; set; }
        public string Cmd6 { get; set; }
        public string Cmd7 { get; set; }
        public string Cmd8 { get; set; }
        public int OTA_Customer_Id { get; set; }
        public int errcode { get; set; }
        public string errMsg { get; set; }
    }
}