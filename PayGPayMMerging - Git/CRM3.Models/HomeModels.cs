﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models
{
    public class HomeModels : CommonModels
    {
        public HomeModeType modetype { get; set; }
        public string fromdate { get; set; }
        public string todate { get; set; }
    }

    public class SearchAgentPerformanceOutput
    {
        public string Username { get; set; }
        public int Assigned { get; set; }
        public int Inprogress { get; set; }
        public int Closed { get; set; }
        public int agent_avg { get; set; }
    }

    public class SearchUnresolvedTaskOutput
    {
        public string Team { get; set; }
        public int Inprogress { get; set; }
        public int Unresolved_count { get; set; }
    }

    public class SearchGrapViewOutput
    {
        public string received_date { get; set; }
        public int received_count { get; set; }
    }

    public class SearchOverviewTaskOutput
    {
        public string status { get; set; }
        public int total_count { get; set; }
    }

    public enum HomeModeType
    {
        SEARCHAGENTPERFORMANCE = 1,
        SEARCHUNRESOLVEDTASK = 2,
        SEARCHOVERVIEWTASK = 3,
        SEARCHGRAPVIEW = 4
    }
}
