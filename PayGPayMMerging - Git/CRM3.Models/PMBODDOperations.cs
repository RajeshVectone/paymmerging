﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models
{
    //Grid View Class
    public class PMBODDOperations : Common.ErrCodeMsg
    {
        public string registerdate { get; set; }
        public string subscriberid { get; set; }
        public string lastupdate { get; set; }
        public string paymentref { get; set; }
        public string fullname { get; set; }
        public string sort_code { get; set; }
        public string account_number { get; set; }
        public string accountname { get; set; }
        public string orderselected { get; set; }
        public string ddstatus { get; set; }
        public string iccid { get; set; }
        public string email { get; set; }
        public string opsid { get; set; }
        public string dd_action { get; set; }
        public string mobileno { get; set; }
        public string freesimid { get; set; }
        public string outstanding_amount { get; set; }
        public string pp_customer_id { get; set; }
        public string bank_reference { get; set; }
    }

    //Search Class
    public class PMBOSearch : Common.ErrCodeMsg
    {
        public string CountTransctions { get; set; }
        public string DDOpreations { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string CCTransactionID { get; set; }
        public string SubscriberID { get; set; }
        public string SortCode { get; set; }
        public string AccountNumber { get; set; }
        public string SiteCode { get; set; }
    }

    //c3pm_dd_set_status
    public class PMBOSave : Common.ErrCodeMsg
    {
        public string subs_date { get; set; }
        public string subs_ID { get; set; }
        public string trans_ID { get; set; }
        public string last_name { get; set; }
        public string sort_code { get; set; }
        public string account_number { get; set; }
        public string account_name { get; set; }
        public string plan { get; set; }
        public string DD_status { get; set; }
        public string updateby { get; set; }
        public string ICCID { get; set; }
        public string email { get; set; }
        public string opsid { get; set; }
        public string suspendType { get; set; }
        public string freesimid { get; set; }
        public string reason { get; set; }
    }

    //Output
    public class PMBOOutput : Common.ErrCodeMsg
    {
        public string mobileNo { get; set; }
        public string email { get; set; }
        public string custcode { get; set; }
        public string bathcode { get; set; }
        public string serialcode { get; set; }
        public string usage_sofar { get; set; }
    }


    public class SetPaymentOutput : Common.ErrCodeMsg
    {
        public string updated_by { get; set; }
        public Int16 errcode { get; set; }
        public string errmsg { get; set; }
       
    }

    public class Subscribers : Common.ErrCodeMsg
    {
        public string SubscriberID { get; set; }
    }

    public class log : Common.ErrCodeMsg
    {
        public int operation { get; set; }
        public int status { get; set; }
        public int userid { get; set; }
        public string user_name { get; set; }
        public int operationid { get; set; }
        public string operation_name { get; set; }
        public int total_rows { get; set; }
        public string filename { get; set; }
        public string notes { get; set; }
        public int opsid { get; set; }
    }

    //Payway section.
    public class PayWay : Common.ErrCodeMsg
    {
        public string opsid { get; set; }
        public string createdate { get; set; }
        public string filename { get; set; }
        public string operation_name { get; set; }
        public string total_rows { get; set; }
        public string user_name { get; set; }
        public string status { get; set; }
        public string userid { get; set; }
    }

    //Manual Marking.
    public class ManualMarking : Common.ErrCodeMsg
    {
        public string ref1 { get; set; }
        public string transCode { get; set; }
        public string returnCode { get; set; }
        public string returnDescription { get; set; }
        public string originalProcessingDate { get; set; }
        public string valueOf { get; set; }
        public string currency { get; set; }
        public string number3 { get; set; }
        public string ref4 { get; set; }
        public string name5 { get; set; }
        public string sortCode6 { get; set; }
        public string bankName7 { get; set; }
        public string branchName8 { get; set; }
        public string numberOf { get; set; }
        public string valueOf9 { get; set; }
        public string currency10 { get; set; }
    }
    public class DDgoSubmit : Common.ErrCodeMsg
    {
        public string reference_id { get; set; }
        public DateTime charge_date { get; set; }
        public double amount { get; set; }
        public string desiscion { get; set; }
        public string currency { get; set; }
        public int amount_refunded { get; set; }
        public string cust_reference { get; set; }
        public string mandate_id { get; set; }
        public string creditor_id	{ get; set; }
        public string pp_customer_id { get; set; }
        public string status { get; set; }
        public string subscriberid { get; set; }
        public string cust_metadata_reference { get; set; }
        public string email { get; set; }
        public string Name { get; set; }
        public string AcHldrName { get; set; }
        public string BankName { get; set; }
        public string AccountStatus { get; set; }

    }

    //public class ManualMarkingPage : PMBOPageModel
    //{
    //    public string ref1 { get; set; }
    //    public int transCode { get; set; }
    //    public string returnCode { get; set; }
    //    public string returnDescription { get; set; }
    //    public DateTime originalProcessingDate { get; set; }
    //    public float valueOf { get; set; }
    //    public string currency { get; set; }
    //    public string number3 { get; set; }
    //    public string ref4 { get; set; }
    //    public string name5 { get; set; }
    //    public string sortCode6 { get; set; }
    //    public string bankName7 { get; set; }
    //    public string branchName8 { get; set; }
    //    public int numberOf { get; set; }
    //    public float valueOf9 { get; set; }
    //    public string currency10 { get; set; }
    //}

    public partial class ManualMarkingPage : PMBOPageModel
    {
        public List<ManualMarkingPage> lstPartialModel { get; set; }
       

    }

    public class ManualMarkingXLSUploadResult 
    {
        public int errcode { get; set; }
        public string errmsg{ get; set; }
        public string id{ get; set; } 
    }

    public class ManualMarkUploadCF
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
        public string SuccessMesg { get; set; }
        public string mobileno { get; set; }
    }
   
    public class DDCancellation : Common.ErrCodeMsg
    {
        public string report_generation_date { get; set; }
        public string effective_date { get; set; }
        public string reference { get; set; }
        public string payer_name { get; set; }
        public string payer_account_number { get; set; }
        public string payer_sort_code { get; set; }
        public string reason_code { get; set; }
    }

    public class DDViewNProcessFailure: Common.ErrCodeMsg
    {
        public string Customer_ID { get; set; }
        public string Subscription_Id { get; set; } 
        public string MobileNo { get; set; } 
        public string DD_Status { get; set; } 
        public string Amount { get; set; }
        public string pay_reference { get; set; } 
        public string PaymentStatus { get; set; } 
        public string CustomerName { get; set; } 
        public string Connection_status { get; set; } 
        public string Payment_Attempts { get; set; } 
        public string Processing_Date { get; set; } 
        public string Payment_Mode { get; set; } 
     }

    public class DDViewNProcessFailureParam
    {
        public string ddYear { get; set; }
        public string ddMonth { get; set; }
        public string MobileNo { get; set; }
        public string ddStatus { get; set; }
        public int ddCustID { get; set; }
        public string ddPaymtStatus { get; set; }
        public string ddMobileNo { get; set; }
        public string ddConnStatus { get; set; }
        public string ddPaymentRef { get; set; }
        public string ddPayMode { get; set; }
        public string sitecode { get; set; }
    }

    public class SMSMobileNos: Common.ErrCodeMsg
    {
        public string MobileNo { get; set; } 
    }
    public class ViewNProcessFailureMessage 
    {
        public string Totalcustomerschanged { get; set; }
        public string Batch_Id { get; set; }
    }
    public class SMSMobileNosEmailAccs : Common.ErrCodeMsg
    {
        public string Mobile_No { get; set; }
        public string Customer_Name { get; set; }
        public string EMail_Id { get; set; }
    }

    public class ProcessPaymentCC_MESG
    {
        //public string SuccesMesg { get; set; }
        public string TotalcustomersMarked { get; set; }
    }
    public class MoveToSetUpInProgressMesg
    {
        public string Totalcustomerschanged { get; set; }
    }

    public class DirectDebitActiveMesg
    {
        public string ActiveDD { get; set; }
    }

    //public class PaymentMode
    //{
    //    public string Payment_Mode { get; set; }
    //}

}
