﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace CRM_API.Models
{
    public class EnquiryOutput 
    {
        public int Enquiry_ID { get; set; }
        public string Enquirer_Name { get; set; }
        public string Mobileno { get; set; }
        public string Existing_Network { get; set; }
        public string Country { get; set; }
        public int  Fk_Enq_type { get; set; }
        public DateTime  Enquiry_Date { get; set; }
        //public string Enquiry_Date_string { get { return Enquiry_Date.ToString("yyyy-MM-dd hh:mm:ss"); } }
        public string Enquiry_Date_string { get { return Enquiry_Date.ToString("dd-MM-yyyy"); } }
        public string Query { get; set; }
        public string  Comments { get; set; }
        public string create_agent { get; set; }
        public string close_agent { get; set; }
        public DateTime close_date { get; set; }
        public int Fk_status_id { get; set; }
        public string  current_status { get; set; } 
        public int errcode { get; set; }
        public string errmsg { get; set; }
        public string Email { get; set; }
        public int id { get; set; }
        public string Enquiry_Name { get; set; } 
        public string h_comments { get; set; }
        public string agent { get; set; }
    }
    public class EnquiryComments
    {
        public string comments { get; set; }
        public string create_by { get; set; }
        public DateTime create_date { get; set; }
        //public string create_datee_string { get { return create_date.ToString("yyyy-MM-dd hh:mm:ss"); } }
        public string create_datee_string { get { return create_date.ToString("dd-MM-yyyy"); } }
    }

    public class EnquiryInput
    {
        public string Enquirer_Name { get; set; }
        public string Mobileno { get; set; }
        public string Existing_Network { get; set; }
        public string Email { get; set; }
        public string Country { get; set; }
        public int Enquiry_Type { get; set; }
        public DateTime Enquiry_Date { get; set; }

        public string Query { get; set; }
        public string Comments { get; set; }
        public int Enquiry_ID { get; set; }
        public int fk_status_id { get; set; }
        public int fk_agent_id { get; set; }
        public int fk_close_agent { get; set; }
        public DateTime date_from { get; set; }
        public DateTime date_to { get; set; }
        public int errcode { get; set; }
        public string errmsg { get; set; }
        public int mode{ get; set; }
        public string create_by { get; set; }

    }
    public class EnquiryCountry
    {
        public int phonecode { get; set; }
        public string name { get; set; }
    }
    public class EnquiryType
    {
        public int Enquiry_TYPE_ID { get; set; }
        public string Enquiry_Name { get; set; }
    }
    public class BindStatus
    {
        public int status_id { get; set; }
        public string status { get; set; }
    }


}