﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models
{
    public class History
    {
        public string AccountId
        {
            get;
            set;
        }

        public string Country
        {
            get;
            set;
        }

        public DateTime CreateDate
        {
            get;
            set;
        }

        public string Currency
        {
            get;
            set;
        }

        public string Email
        {
            get;
            set;
        }

        public string IpClient
        {
            get;
            set;
        }

        public string IpPaymentAgent
        {
            get;
            set;
        }

        public string MobileNo
        {
            get;
            set;
        }

        public string Note
        {
            get;
            set;
        }

        public string Operator
        {
            get;
            set;
        }

        public string PaymentMethod
        {
            get;
            set;
        }

        public string PaymentType
        {
            get;
            set;
        }

        public string ProductCode
        {
            get;
            set;
        }

        public float RecAmount
        {
            get;
            set;
        }

        public string RecipientNo
        {
            get;
            set;
        }

        public string ReferenceId
        {
            get;
            set;
        }

        public string Status
        {
            get;
            set;
        }

        public float TotalAmount
        {
            get;
            set;
        }

        //02-Dec-2018: Moorthy : added for Request for refund
        public float TotalAmountOrg
        {
            get { return TotalAmount; }
        }

        public float TrfAmount
        {
            get;
            set;
        }

        public string User
        {
            get;
            set;
        }

        public int ViewRefund { get; set; }

        public History()
        {
        }

        public string bundle_name { get; set; }

        public int is_refunded { get; set; }
    }
}
