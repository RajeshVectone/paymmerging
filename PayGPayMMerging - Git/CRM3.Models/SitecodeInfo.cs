﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models
{
    public class SitecodeInfo:Common.ErrCodeMsg
    {
        public string Sitecode { get; set; }
        public string CC { get; set; }
        public int PLMNID { get; set; }
        public string VMDepositAddress { get; set; }
        public string ProductName { get; set; }
        public bool IsActive { get; set; }
    }
}
