﻿using System;
using System.Collections.Generic;

namespace CRM_API.Models.Porting.Syniverse
{
    public enum BulkTransferSelector
    {
        Null, Consumer, Bulk, Both
    }

    public enum EqualityTest
    {
        Null, LessThan, LessThanOrEqualTo, Equals, GreaterThan, GreaterThanOrEqualTo
    }

    public enum MsisdnTypeSelector
    {
        Null, Primary, Secondary, Both
    }

    public enum StatusSelector
    {
        Null, Open, Closed, Locked, Cancelled, Expired, Archived, Active, Dormant, All
    }

    public enum TaggedSelector
    {
        Null, True, False, Both
    }

    public enum TransferTypeSelector
    {
        Null, Port, Migration, Both
    }

    public class Authenticator
    {
        #region Username
        private string _username;
        public string Username
        {
            get { return _username; }
            set { _username = value; }
        }
        #endregion

        #region Password
        private string _password;
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }
        #endregion
    }

    public class ContactInfo
    {
        #region ContactName

        private string _contactName;

        public string ContactName
        {
            get { return _contactName; }
            set { _contactName = value; }
        }

        #endregion ContactName

        #region ContactDetails

        private string _contactDetails;

        public string ContactDetails
        {
            get { return _contactDetails; }
            set { _contactDetails = value; }
        }

        #endregion ContactDetails
    }

    public class DnoSelector
    {
        #region NoCode
        private string _noCode;
        public string NoCode
        {
            get { return _noCode; }
            set { _noCode = value; }
        }
        #endregion

        #region All
        // Empty element All 
        #endregion
    }

    public class DspSelector
    {
        #region SpCode
        private string _spCode;
        public string SpCode
        {
            get { return _spCode; }
            set { _spCode = value; }
        }
        #endregion

        #region All
        // Empty element All 
        #endregion
    }

    public class EntryKey
    {
        #region Pac

        private string _pac;

        public string Pac
        {
            get { return _pac; }
            set { _pac = value; }
        }

        #endregion Pac

        #region Msisdn

        private string _msisdn;

        public string Msisdn
        {
            get { return _msisdn; }
            set { _msisdn = value; }
        }

        #endregion Msisdn

        // only needed in our side for Submit Request specifically

        #region BbMsisdn

        private string _bbMsisdn;

        public string BbMsisdn
        {
            get { return _bbMsisdn; }
            set { _bbMsisdn = value; }
        }

        #endregion BbMsisdn
    }

    public class EntryResult
    {
        #region Msisdn
        private string _msisdn;
        public string Msisdn
        {
            get { return _msisdn; }
            set { _msisdn = value; }
        }
        #endregion

        #region Confirmed
        // Empty element Confirmed
        #endregion

        #region Error
        private Error _error;
        public Error Error
        {
            get { return _error; }
            set { _error = value; }
        }

        #endregion
    }

    public class Error : CRM_API.Models.MNP.MnpResponse
    {
        public Error()
        {
            _errorCode = 0;
        }

        #region ErrorCode
        private int _errorCode;
        public int ErrorCode
        {
            get { return _errorCode; }
            set { _errorCode = value; }
        }
        #endregion

        #region Text
        private string _text;
        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }
        #endregion

        #region Detail
        private List<string> _detail;
        public List<string> Detail
        {
            get { return _detail; }
            set { _detail = value; }
        }
        #endregion
    }

    public class Logoff : CRM_API.Models.MNP.MnpRequest
    {
    }

    public class Logon
    {
        #region Authenticator
        private Authenticator _authenticator;
        public Authenticator Authenticator
        {
            get { return _authenticator; }
            set { _authenticator = value; }
        }
        #endregion
    }

    public class LogonResp
    {
        #region Cookie
        private string _cookie;
        public string Cookie
        {
            get { return _cookie; }
            set { _cookie = value; }
        }
        #endregion

        #region PasswordExpiryWarning
        private PasswordExpiryWarning _passwordExpiryWarning;
        public PasswordExpiryWarning PasswordExpiryWarning
        {
            get { return _passwordExpiryWarning; }
            set { _passwordExpiryWarning = value; }
        }
        #endregion
    }

    public class MsisdnQuantitySelector
    {
        #region EqualityTest
        private EqualityTest _equalityTest;
        public EqualityTest EqualityTest
        {
            get { return _equalityTest; }
            set { _equalityTest = value; }
        }
        #endregion

        #region EntryCount
        private int _entryCount;
        public int EntryCount
        {
            get { return _entryCount; }
            set { _entryCount = value; }
        }
        #endregion
    }

    public class OptionalData
    {
        #region PrivateData

        private PrivateData _privateData;

        public PrivateData PrivateData
        {
            get { return _privateData; }
            set { _privateData = value; }
        }

        #endregion PrivateData

        #region ContactInfo

        private ContactInfo _contactInfo;

        public ContactInfo ContactInfo
        {
            get { return _contactInfo; }
            set { _contactInfo = value; }
        }

        #endregion ContactInfo
    }

    public class PacAgeSelector
    {
        #region EqualityTest
        private EqualityTest _equalityTest;
        public EqualityTest EqualityTest
        {
            get { return _equalityTest; }
            set { _equalityTest = value; }
        }
        #endregion

        #region Days
        private int _days;
        public int Days
        {
            get { return _days; }
            set { _days = value; }
        }
        #endregion
    }

    public class PacExpirySelector
    {
        #region EqualityTest
        private EqualityTest _equalityTest;
        public EqualityTest EqualityTest
        {
            get { return _equalityTest; }
            set { _equalityTest = value; }
        }
        #endregion

        #region Days
        private int _days;
        public int Days
        {
            get { return _days; }
            set { _days = value; }
        }
        #endregion
    }

    public class PasswordExpiryWarning
    {
        #region ExpiryDate
        private string _expiryDate;
        public string ExpiryDate
        {
            get { return _expiryDate; }
            set { _expiryDate = value; }
        }
        #endregion
    }

    public class PortDueSelector
    {
        #region EqualityTest
        private EqualityTest _equalityTest;
        public EqualityTest EqualityTest
        {
            get { return _equalityTest; }
            set { _equalityTest = value; }
        }
        #endregion

        #region Days
        private int _days;
        public int Days
        {
            get { return _days; }
            set { _days = value; }
        }
        #endregion
    }

    public class PrimaryMsisdn
    {
        #region Msisdn
        private string _msisdn;
        public string Msisdn
        {
            get { return _msisdn; }
            set { _msisdn = value; }
        }
        #endregion
    }

    public class PrivateData
    {
        #region AccNo

        private string _accNo;

        public string AccNo
        {
            get { return _accNo; }
            set { _accNo = value; }
        }

        #endregion AccNo

        #region Ref

        private string _ref;

        public string Ref
        {
            get { return _ref; }
            set { _ref = value; }
        }

        #endregion Ref

        #region SpId

        private string _spId;

        public string SpId
        {
            get { return _spId; }
            set { _spId = value; }
        }

        #endregion SpId
    }
    public class Report : CRM_API.Models.MNP.MnpReportRequest
    {
        #region Constructor to set default value
        public Report()
        {
            _portDate = DateTime.MinValue;
        }
        #endregion

        #region Pac
        private string _pac;
        public string Pac
        {
            get { return _pac; }
            set { _pac = value; }
        }
        #endregion

        #region Msisdn
        private string _msisdn;
        public string Msisdn
        {
            get { return _msisdn; }
            set { _msisdn = value; }
        }
        #endregion

        #region PacExpirySelector
        private PacExpirySelector _pacExpirySelector;
        public PacExpirySelector PacExpirySelector
        {
            get { return _pacExpirySelector; }
            set { _pacExpirySelector = value; }
        }
        #endregion

        #region StatusSelector
        private StatusSelector _statusSelector;
        public StatusSelector StatusSelector
        {
            get { return _statusSelector; }
            set { _statusSelector = value; }
        }
        #endregion

        #region BulkTransferSelector
        private BulkTransferSelector _bulkTransferSelector;
        public BulkTransferSelector BulkTransferSelector
        {
            get { return _bulkTransferSelector; }
            set { _bulkTransferSelector = value; }
        }
        #endregion

        #region MsisdnQuantitySelector
        private MsisdnQuantitySelector _msisdnQuantitySelector;
        public MsisdnQuantitySelector MsisdnQuantitySelector
        {
            get { return _msisdnQuantitySelector; }
            set { _msisdnQuantitySelector = value; }
        }
        #endregion

        #region PrimaryMsisdn
        private PrimaryMsisdn _primaryMsisdn;
        public PrimaryMsisdn PrimaryMsisdn
        {
            get { return _primaryMsisdn; }
            set { _primaryMsisdn = value; }
        }
        #endregion

        #region MsisdnTypeSelector
        private MsisdnTypeSelector _msisdnTypeSelector;
        public MsisdnTypeSelector MsisdnTypeSelector
        {
            get { return _msisdnTypeSelector; }
            set { _msisdnTypeSelector = value; }
        }
        #endregion

        #region TransferTypeSelector
        private TransferTypeSelector _transferTypeSelector;
        public TransferTypeSelector TransferTypeSelector
        {
            get { return _transferTypeSelector; }
            set { _transferTypeSelector = value; }
        }
        #endregion

        #region PortDueSelector
        private PortDueSelector _portDueSelector;
        public PortDueSelector PortDueSelector
        {
            get { return _portDueSelector; }
            set { _portDueSelector = value; }
        }
        #endregion

        #region PacAgeSelector
        private PacAgeSelector _pacAgeSelector;
        public PacAgeSelector PacAgeSelector
        {
            get { return _pacAgeSelector; }
            set { _pacAgeSelector = value; }
        }
        #endregion

        #region TaggedSelector
        private TaggedSelector _taggedSelector;
        public TaggedSelector TaggedSelector
        {
            get { return _taggedSelector; }
            set { _taggedSelector = value; }
        }
        #endregion

        #region DspAccNo
        private string _dspAccNo;
        public string DspAccNo
        {
            get { return _dspAccNo; }
            set { _dspAccNo = value; }
        }
        #endregion

        #region DspRef
        private string _dspRef;
        public string DspRef
        {
            get { return _dspRef; }
            set { _dspRef = value; }
        }
        #endregion

        #region DspContactName
        private string _dspContactName;
        public string DspContactName
        {
            get { return _dspContactName; }
            set { _dspContactName = value; }
        }
        #endregion

        #region DspContactDetails
        private string _dspContactDetails;
        public string DspContactDetails
        {
            get { return _dspContactDetails; }
            set { _dspContactDetails = value; }
        }
        #endregion

        #region DspSelector
        private DspSelector _dspSelector;
        public DspSelector DspSelector
        {
            get { return _dspSelector; }
            set { _dspSelector = value; }
        }
        #endregion

        #region DnoSelector
        private DnoSelector _dnoSelector;
        public DnoSelector DnoSelector
        {
            get { return _dnoSelector; }
            set { _dnoSelector = value; }
        }
        #endregion

        #region RspAccNo
        private string _rspAccNo;
        public string RspAccNo
        {
            get { return _rspAccNo; }
            set { _rspAccNo = value; }
        }
        #endregion

        #region RspRef
        private string _rspRef;
        public string RspRef
        {
            get { return _rspRef; }
            set { _rspRef = value; }
        }
        #endregion

        #region RspContactName
        private string _rspContactName;
        public string RspContactName
        {
            get { return _rspContactName; }
            set { _rspContactName = value; }
        }
        #endregion

        #region RspContactDetails
        private string _rspContactDetails;
        public string RspContactDetails
        {
            get { return _rspContactDetails; }
            set { _rspContactDetails = value; }
        }
        #endregion

        #region RspSelector
        private RspSelector _rspSelector;
        public RspSelector RspSelector
        {
            get { return _rspSelector; }
            set { _rspSelector = value; }
        }
        #endregion

        #region RnoSelector
        private RnoSelector _rnoSelector;
        public RnoSelector RnoSelector
        {
            get { return _rnoSelector; }
            set { _rnoSelector = value; }
        }
        #endregion

        #region SpId
        private string _spId;
        public string SpId
        {
            get { return _spId; }
            set { _spId = value; }
        }
        #endregion

        #region PortDate
        private DateTime _portDate;
        public DateTime PortDate
        {
            get { return _portDate; }
            set { _portDate = value; }
        }
        #endregion
    }

    public class RnoSelector
    {
        #region NoCode
        private string _noCode;
        public string NoCode
        {
            get { return _noCode; }
            set { _noCode = value; }
        }
        #endregion

        #region All
        // Empty element All 
        #endregion
    }

    public class RspSelector
    {
        #region SpCode
        private string _spCode;
        public string SpCode
        {
            get { return _spCode; }
            set { _spCode = value; }
        }
        #endregion

        #region All
        // Empty element All 
        #endregion
    }

    public class SubmitRequest : MNP.MnpRequest
    {
        #region Constructor to set default value

        public SubmitRequest()
        {
            _useSuggestedDateOnConflict = false;
            _portDate = DateTime.MinValue;
        }

        #endregion Constructor to set default value

        #region EntryKey

        private EntryKey _entryKey;

        public EntryKey EntryKey
        {
            get { return _entryKey; }
            set { _entryKey = value; }
        }

        #endregion EntryKey

        #region NoCode

        private string _noCode;

        public string NoCode
        {
            get { return _noCode; }
            set { _noCode = value; }
        }

        #endregion NoCode

        #region PortDate

        private DateTime _portDate;

        public DateTime PortDate
        {
            get { return _portDate; }
            set { _portDate = value; }
        }

        #endregion PortDate

        #region SpOptionalData

        private OptionalData _spOptionalData;

        public OptionalData SpOptionalData
        {
            get { return _spOptionalData; }
            set { _spOptionalData = value; }
        }

        #endregion SpOptionalData

        #region UseSuggestedDateOnConflict

        private bool _useSuggestedDateOnConflict;

        public bool UseSuggestedDateOnConflict
        {
            get { return _useSuggestedDateOnConflict; }
            set { _useSuggestedDateOnConflict = value; }
        }

        #endregion UseSuggestedDateOnConflict
    }

    public class ReviseRequest : MNP.MnpRequest
    {
        #region Constructor to set default value

        public ReviseRequest()
        {
            _useSuggestedDateOnConflict = false;
            _portDate = DateTime.MinValue;
        }

        #endregion Constructor to set default value

        #region EntryKey

        private EntryKey _entryKey;

        public EntryKey EntryKey
        {
            get { return _entryKey; }
            set { _entryKey = value; }
        }

        #endregion EntryKey

        #region NoCode

        private string _noCode;

        public string NoCode
        {
            get { return _noCode; }
            set { _noCode = value; }
        }

        #endregion NoCode

        #region PortDate

        private DateTime _portDate;

        public DateTime PortDate
        {
            get { return _portDate; }
            set { _portDate = value; }
        }

        #endregion PortDate

        #region SpOptionalData

        private OptionalData _spOptionalData;

        public OptionalData SpOptionalData
        {
            get { return _spOptionalData; }
            set { _spOptionalData = value; }
        }

        #endregion SpOptionalData

        #region UseSuggestedDateOnConflict

        private bool _useSuggestedDateOnConflict;

        public bool UseSuggestedDateOnConflict
        {
            get { return _useSuggestedDateOnConflict; }
            set { _useSuggestedDateOnConflict = value; }
        }

        #endregion UseSuggestedDateOnConflict
    }

    public class CancelRequest : MNP.MnpRequest
    {
        #region Constructor to set default value
        public CancelRequest()
        {
            _cancelAllInPac = false;
        }
        #endregion

        #region EntryKey
        private EntryKey _entryKey;
        public EntryKey EntryKey
        {
            get { return _entryKey; }
            set { _entryKey = value; }
        }
        #endregion

        #region CancelAllInPac
        private bool _cancelAllInPac;
        public bool CancelAllInPac
        {
            get { return _cancelAllInPac; }
            set { _cancelAllInPac = value; }
        }
        #endregion
    }

    public class SubmitRequestResp : CRM_API.Models.MNP.MnpResponse
    {
        #region Constructor to set default values
        public SubmitRequestResp()
        {
            _portDateWarning = false;
            _portDate = DateTime.MinValue;
        }
        #endregion

        #region EntryResult
        private EntryResult _entryResult;
        public EntryResult EntryResult
        {
            get { return _entryResult; }
            set { _entryResult = value; }
        }
        #endregion

        #region PortDate
        private DateTime _portDate;
        public DateTime PortDate
        {
            get { return _portDate; }
            set { _portDate = value; }
        }
        #endregion

        #region PortDateWarning
        private bool _portDateWarning;
        public bool PortDateWarning
        {
            get { return _portDateWarning; }
            set { _portDateWarning = value; }
        }
        #endregion

        #region DnoCode
        private string _dnoCode;
        public string DnoCode
        {
            get { return _dnoCode; }
            set { _dnoCode = value; }
        }
        #endregion // required on our side for saving

    }

    public class ReviseRequestResp : CRM_API.Models.MNP.MnpResponse
    {
        #region Constructor to set default values
        public ReviseRequestResp()
        {
            _portDateWarning = false;
            _portDate = DateTime.MinValue;
        }
        #endregion

        #region EntryResult
        private EntryResult _entryResult;
        public EntryResult EntryResult
        {
            get { return _entryResult; }
            set { _entryResult = value; }
        }
        #endregion

        #region PortDate
        private DateTime _portDate;
        public DateTime PortDate
        {
            get { return _portDate; }
            set { _portDate = value; }
        }
        #endregion

        #region PortDateWarning
        private bool _portDateWarning;
        public bool PortDateWarning
        {
            get { return _portDateWarning; }
            set { _portDateWarning = value; }
        }
        #endregion

    }

    public class CancelRequestResp : CRM_API.Models.MNP.MnpResponse
    {
        #region Constructor to set default values
        public CancelRequestResp()
        {
            _cancelAllInPacRequested = false;
        }
        #endregion

        #region Pac
        private string _pac;
        public string Pac
        {
            get { return _pac; }
            set { _pac = value; }
        }
        #endregion

        #region MultipleEntryResult
        private MultipleEntryResult _multipleEntryResult;
        public MultipleEntryResult MultipleEntryResult
        {
            get { return _multipleEntryResult; }
            set { _multipleEntryResult = value; }
        }
        #endregion

        #region CancelAllInPacRequested
        private bool _cancelAllInPacRequested;
        public bool CancelAllInPacRequested
        {
            get { return _cancelAllInPacRequested; }
            set { _cancelAllInPacRequested = value; }
        }
        #endregion
    }

    public class MultipleEntryResult
    {
        #region ResultCount
        private ResultCount _resultCount;
        public ResultCount ResultCount
        {
            get { return _resultCount; }
            set { _resultCount = value; }
        }
        #endregion

        #region EntryResultList
        private EntryResultList _entryResultList;
        public EntryResultList EntryResultList
        {
            get { return _entryResultList; }
            set { _entryResultList = value; }
        }
        #endregion
    }

    public class ResultCount
    {
        #region GoodCount
        private int _goodCount;
        public int GoodCount
        {
            get { return _goodCount; }
            set { _goodCount = value; }
        }
        #endregion

        #region BadCount
        private int _badCount;
        public int BadCount
        {
            get { return _badCount; }
            set { _badCount = value; }
        }
        #endregion
    }

    public class EntryResultList
    {
        #region EntryResult
        private List<EntryResult> _entryResult;
        public List<EntryResult> EntryResult
        {
            get { return _entryResult; }
            set { _entryResult = value; }
        }
        #endregion
    }

    public class CancelEntryResp : CRM_API.Models.MNP.MnpResponse
    {
        #region Constructor to set default values
        public CancelEntryResp()
        {
            _cancelAllInPacRequested = false;
        }
        #endregion

        #region Pac
        private string _pac;
        public string Pac
        {
            get { return _pac; }
            set { _pac = value; }
        }
        #endregion

        #region MultipleEntryResult
        private MultipleEntryResult _multipleEntryResult;
        public MultipleEntryResult MultipleEntryResult
        {
            get { return _multipleEntryResult; }
            set { _multipleEntryResult = value; }
        }
        #endregion

        #region CancelAllInPacRequested
        private bool _cancelAllInPacRequested;
        public bool CancelAllInPacRequested
        {
            get { return _cancelAllInPacRequested; }
            set { _cancelAllInPacRequested = value; }
        }
        #endregion
    }

    public class ReportResp : CRM_API.Models.MNP.MnpReportResponse
    {
        private List<ReportRecord> _reportRecord;

        public List<ReportRecord> ReportRecord
        {
            get { return _reportRecord; }
            set { _reportRecord = value; }
        }
    }
    public class ReportRecord : CRM_API.Models.MNP.MnpReportResponse
    {
        #region Constructor to set default value
        public ReportRecord()
        {
            _portDate = DateTime.MinValue;
        }
        #endregion

        #region Pac
        private string _pac;
        public string Pac
        {
            get { return _pac; }
            set { _pac = value; }
        }
        #endregion

        #region Msisdn
        private string _msisdn;
        public string Msisdn
        {
            get { return _msisdn; }
            set { _msisdn = value; }
        }
        #endregion

        #region PacExpiryDays
        private int _pacExpiryDays;
        public int PacExpiryDays
        {
            get { return _pacExpiryDays; }
            set { _pacExpiryDays = value; }
        }
        #endregion

        #region Status
        private Status _status;
        public Status Status
        {
            get { return _status; }
            set { _status = value; }
        }
        #endregion

        #region IsBulk
        private IsBulk _isBulk;
        public IsBulk IsBulk
        {
            get { return _isBulk; }
            set { _isBulk = value; }
        }
        #endregion

        #region EntryCount
        private int _entryCount;
        public int EntryCount
        {
            get { return _entryCount; }
            set { _entryCount = value; }
        }
        #endregion

        #region PrimaryMsisdn
        private PrimaryMsisdn _primaryMsisdn;
        public PrimaryMsisdn PrimaryMsisdn
        {
            get { return _primaryMsisdn; }
            set { _primaryMsisdn = value; }
        }
        #endregion

        #region MsisdnType
        private MsisdnType _msisdnType;
        public MsisdnType MsisdnType
        {
            get { return _msisdnType; }
            set { _msisdnType = value; }
        }
        #endregion

        #region TransferType
        private TransferType _transferType;
        public TransferType TransferType
        {
            get { return _transferType; }
            set { _transferType = value; }
        }
        #endregion

        #region PortDays
        private string _portDays;
        public string PortDays
        {
            get { return _portDays; }
            set { _portDays = value; }
        }
        #endregion

        #region PortDate
        private DateTime _portDate;
        public DateTime PortDate
        {
            get { return _portDate; }
            set { _portDate = value; }
        }
        #endregion

        #region PacAge
        private int _pacAge;
        public int PacAge
        {
            get { return _pacAge; }
            set { _pacAge = value; }
        }
        #endregion

        #region IsTagged
        private TaggedSelector _isTagged;
        public TaggedSelector IsTagged
        {
            get { return _isTagged; }
            set { _isTagged = value; }
        }
        #endregion

        #region DspContactName
        private string _dspContactName;
        public string DspContactName
        {
            get { return _dspContactName; }
            set { _dspContactName = value; }
        }
        #endregion

        #region DspContactDetails
        private string _dspContactDetails;
        public string DspContactDetails
        {
            get { return _dspContactDetails; }
            set { _dspContactDetails = value; }
        }
        #endregion

        #region DspCode
        private string _dspCode;
        public string DspCode
        {
            get { return _dspCode; }
            set { _dspCode = value; }
        }
        #endregion

        #region DnoCode
        private string _dnoCode;
        public string DnoCode
        {
            get { return _dnoCode; }
            set { _dnoCode = value; }
        }
        #endregion

        #region DspAccNo
        private string _dspAccNo;
        public string DspAccNo
        {
            get { return _dspAccNo; }
            set { _dspAccNo = value; }
        }
        #endregion

        #region DspRef
        private string _dspRef;
        public string DspRef
        {
            get { return _dspRef; }
            set { _dspRef = value; }
        }
        #endregion

        #region DspName
        private string _dspName;
        public string DspName
        {
            get { return _dspName; }
            set { _dspName = value; }
        }
        #endregion

        #region DnoName
        private string _dnoName;
        public string DnoName
        {
            get { return _dnoName; }
            set { _dnoName = value; }
        }
        #endregion

        #region RspContactName
        private string _rspContactName;
        public string RspContactName
        {
            get { return _rspContactName; }
            set { _rspContactName = value; }
        }
        #endregion

        #region RspContactDetails
        private string _rspContactDetails;
        public string RspContactDetails
        {
            get { return _rspContactDetails; }
            set { _rspContactDetails = value; }
        }
        #endregion

        #region RspCode
        private string _rspCode;
        public string RspCode
        {
            get { return _rspCode; }
            set { _rspCode = value; }
        }
        #endregion

        #region RnoCode
        private string _rnoCode;
        public string RnoCode
        {
            get { return _rnoCode; }
            set { _rnoCode = value; }
        }
        #endregion

        #region SpId
        private string _spId;
        public string SpId
        {
            get { return _spId; }
            set { _spId = value; }
        }
        #endregion

        #region RspAccNo
        private string _rspAccNo;
        public string RspAccNo
        {
            get { return _rspAccNo; }
            set { _rspAccNo = value; }
        }
        #endregion

        #region RspRef
        private string _rspRef;
        public string RspRef
        {
            get { return _rspRef; }
            set { _rspRef = value; }
        }
        #endregion

        #region RspName
        private string _rspName;
        public string RspName
        {
            get { return _rspName; }
            set { _rspName = value; }
        }
        #endregion

        #region RnoName
        private string _rnoName;
        public string RnoName
        {
            get { return _rnoName; }
            set { _rnoName = value; }
        }
        #endregion
    }
    public enum IsBulk
    {
        Null, Bulk, Consumer
    }
    public enum MsisdnType
    {
        Null, Primary, Secondary
    }
    public enum TransferType
    {
        Null, Port, Migration, Undefined
    }
    public enum Status
    {
        Null, Open, Closed, Locked, Cancelled, Expired, Archived
    }

}