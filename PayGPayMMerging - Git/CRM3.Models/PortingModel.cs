﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models.PortingModel
{
    public class PortingModelInput
    {
        public string sitecode { get; set; }
        public string mobileno { get; set; }
    }

    public class PortingModelOutput
    {
        public DateTime potin_date { get; set; }
        public string portin_operator { get; set; }
        public string paccode { get; set; }
        public DateTime pack_code_expiry_date { get; set; }
        public int pack_validatity { get; set; }
        public DateTime portout_date { get; set; }
        public string portout_operator { get; set; }
        public double charging_fee { get; set; }
        public DateTime pack_code_request_date { get; set; }
        public double portin_bonus { get; set; }


        public string potin_date_str { get { return potin_date != null && potin_date != DateTime.MinValue ? potin_date.ToString("dd-MM-yyyy HH:mm:ss") : ""; } }
        public string pack_code_expiry_date_str { get { return pack_code_expiry_date != null && pack_code_expiry_date != DateTime.MinValue ? pack_code_expiry_date.ToString("dd-MM-yyyy HH:mm:ss") : ""; } }
        public string pack_code_request_date_str { get { return pack_code_request_date != null && pack_code_request_date != DateTime.MinValue ? pack_code_request_date.ToString("dd-MM-yyyy HH:mm:ss") : ""; } }
        public string portout_date_str { get { return portout_date != null && portout_date != DateTime.MinValue ? portout_date.ToString("dd-MM-yyyy HH:mm:ss") : ""; } }
    }
    public class PortingHistoryModelOutput
    {
        public DateTime Createdate { get; set; }
        public string Createdate_str { get { return Createdate != null && Createdate != DateTime.MinValue ? Createdate.ToString("dd-MM-yyyy HH:mm:ss") : ""; } }
        public int Promoid { get; set; }
        public string Promo_name { get; set; }
        public double Amount { get; set; }
        public string Mode { get; set; }
    }
}
