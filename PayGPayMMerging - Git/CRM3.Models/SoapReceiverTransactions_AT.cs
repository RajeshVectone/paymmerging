﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models.Porting.AT.SoapReceiver.Transactions
{
    public class Transactions
    {
        #region Private Field
        private string _requestId = string.Empty;
        public string RequestId
        {
            get { return _requestId; }
            set { _requestId = value; }
        }

        private string _sender = string.Empty;
        public string Sender
        {
            get { return _sender; }
            set { _sender = value; }
        }

        private string _receiver = string.Empty;
        public string Receiver
        {
            get { return _receiver; }
            set { _receiver = value; }
        }

        private string _mnpImp = string.Empty;
        public string MnpImp
        {
            get { return _mnpImp; }
            set { _mnpImp = value; }
        }

        private string _mspImp = string.Empty;
        public string MspImp
        {
            get { return _mspImp; }
            set { _mspImp = value; }
        }

        private string _mnpExp = string.Empty;
        public string MnpExp
        {
            get { return _mnpExp; }
            set { _mnpExp = value; }
        }

        private string _mspExp = string.Empty;
        public string MspExp
        {
            get { return _mspExp; }
            set { _mspExp = value; }
        }

        private int _envFlag = 0;
        public int EnvFlag
        {
            get { return _envFlag; }
            set { _envFlag = value; }
        }

        private string _timeStamp = string.Empty;
        public string TimeStamp
        {
            get { return _timeStamp; }
            set { _timeStamp = value; }
        }

        private string _version = string.Empty;
        public string Version
        {
            get { return _version; }
            set { _version = value; }
        }

        private int _bc = 0;
        public int Bc
        {
            get { return _bc; }
            set { _bc = value; }
        }

        private int _sentStatus = 0;

        public int SentStatus
        {
            get { return _sentStatus; }
            set { _sentStatus = value; }
        }
        private int _portingStatus = 0;

        public int PortingStatus
        {
            get { return _portingStatus; }
            set { _portingStatus = value; }
        }
        private string _portingState;

        public string PortingState
        {
            get { return _portingState; }
            set { _portingState = value; }
        }
        private string _statusSent;

        public string StatusSent
        {
            get { return _statusSent; }
            set { _statusSent = value; }
        }
        private DateTime _tmsf = DateTime.MinValue;

        public DateTime Tmsf
        {
            get { return _tmsf; }
            set { _tmsf = value; }
        }
        private DateTime _tmsl = DateTime.MinValue;

        public DateTime Tmsl
        {
            get { return _tmsl; }
            set { _tmsl = value; }
        }
        private DateTime _tar = DateTime.MinValue;

        public DateTime Tar
        {
            get { return _tar; }
            set { _tar = value; }
        }
        private DateTime _tmrf = DateTime.MinValue;

        public DateTime Tmrf
        {
            get { return _tmrf; }
            set { _tmrf = value; }
        }
        private DateTime _tmrl = DateTime.MinValue;

        public DateTime Tmrl
        {
            get { return _tmrl; }
            set { _tmrl = value; }
        }
        private DateTime _tas = DateTime.MinValue;

        public DateTime Tas
        {
            get { return _tas; }
            set { _tas = value; }
        }
        private int _rCount = 0;

        public int RCount
        {
            get { return _rCount; }
            set { _rCount = value; }
        }
        private string _msgType = string.Empty;

        public string MsgType
        {
            get { return _msgType; }
            set { _msgType = value; }
        }
        private int _isvoc = 0;

        public int IsVoc
        {
            get { return _isvoc; }
            set { _isvoc = value; }
        }

        private string _receivedRequestId = string.Empty;
        public string ReceivedRequestId
        {
            get { return _receivedRequestId; }
            set { _receivedRequestId = value; }
        }

        #endregion
    }
    public class Ping : Transactions
    {
        private string _Message;
        public string MnpId { get; set; }
        public string MnpCode { get; set; }
        public string Label { get; set; }
        public int IsHome { get; set; }
        public int Status { get; set; }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
    }

    public class Cancel : Transactions
    {
        public string CancelCode { get; set; }
        public int GCode { get; set; }
        public string GDesc { get; set; }
        public int LCode { get; set; }
        public string LDesc { get; set; }
        public int MsgStatus { get; set; }
        public string PortingCode { get; set; }
        public int PortingType { get; set; }
        public string RequestId { get; set; }
    }
    public class Porting : Transactions
    {
        #region Private Field
        private string _portingCode = string.Empty;

        public string PortingCode
        {
            get { return _portingCode; }
            set { _portingCode = value; }
        }
        private int _portingType = 0;


        public int PortingType
        {
            get { return _portingType; }
            set { _portingType = value; }
        }
        private string _requestId = string.Empty;


        public string RequestId
        {
            get { return _requestId; }
            set { _requestId = value; }
        }
        private int _msgStatus = 0;


        public int MsgStatus
        {
            get { return _msgStatus; }
            set { _msgStatus = value; }
        }
        private string _desiredPortDate = string.Empty;


        public string DesiredPortDate
        {
            get { return _desiredPortDate; }
            set { _desiredPortDate = value; }
        }
        private string _msisdn = string.Empty;

        public string Msisdn
        {
            get { return _msisdn; }
            set { _msisdn = value; }
        }
        private int _routingCount = 0;


        public int RoutingCount
        {
            get { return _routingCount; }
            set { _routingCount = value; }
        }
        private string _cancelCode = string.Empty;

        public string CancelCode
        {
            get { return _cancelCode; }
            set { _cancelCode = value; }
        }
        private string _portingDate = string.Empty;

        public string PortingDate
        {
            get { return _portingDate; }
            set { _portingDate = value; }
        }
        private int _gCode = int.MinValue;

        public int GCode
        {
            get { return _gCode; }
            set { _gCode = value; }
        }
        private string _gDesc = string.Empty;

        public string GDesc
        {
            get { return _gDesc; }
            set { _gDesc = value; }
        }
        private int _lCode;

        public int LCode
        {
            get { return _lCode; }
            set { _lCode = value; }
        }
        private string _lDesc = string.Empty;

        public string LDesc
        {
            get { return _lDesc; }
            set { _lDesc = value; }
        }
        private string _voicemail = string.Empty;
        public string VoiceMail
        {
            get { return _voicemail; }
            set { _voicemail = value; }
        }
        private string _msisdnStart = string.Empty;
        public string MsisdnStart
        {
            get { return _msisdnStart; }
            set { _msisdnStart = value; }
        }

        private string _msisdnEnd = string.Empty;
        public string MsisdnEnd
        {
            get { return _msisdnEnd; }
            set { _msisdnEnd = value; }
        }

        private string _prevMsisdn = string.Empty;

        public string PrevMsisdn
        {
            get { return _prevMsisdn; }
            set { _prevMsisdn = value; }
        }
        private string _ICCID = string.Empty;

        public string ICCID
        {
            get { return _ICCID; }
            set { _ICCID = value; }
        }
        #endregion
        #region additional properties
        public string DuplicateMessage { get; set; }
        public string PortStatus { get; set; }
        public string DuplicateOverwrite { get; set; }
        #endregion
    }
    public class NuvInfo : Transactions
    {
        #region Private Field
        private double _nuvInfoId = double.MinValue;

        public double NuvInfoId
        {
            get { return _nuvInfoId; }
            set { _nuvInfoId = value; }
        }
        private int _portingType = 0;

        public int PortingType
        {
            get { return _portingType; }
            set { _portingType = value; }
        }
        private string _requestId = string.Empty;

        public string RequestId
        {
            get { return _requestId; }
            set { _requestId = value; }
        }
        private int _msgStatus = 0;

        public int MsgStatus
        {
            get { return _msgStatus; }
            set { _msgStatus = value; }
        }

        private string _portingCode = string.Empty;

        public string PortingCode
        {
            get { return _portingCode; }
            set { _portingCode = value; }
        }
        private string _msisdn = string.Empty;

        public string Msisdn
        {
            get { return _msisdn; }
            set { _msisdn = value; }
        }
        private string _voicemail = string.Empty;

        public string VoiceMail
        {
            get { return _voicemail; }
            set { _voicemail = value; }
        }
        private string _puk = string.Empty;

        public string Puk
        {
            get { return _puk; }
            set { _puk = value; }
        }
        private int _queryFlag = 0;

        public int QueryFlag
        {
            get { return _queryFlag; }
            set { _queryFlag = value; }
        }
        private string _customerId;

        public string CustomerId
        {
            get { return _customerId; }
            set { _customerId = value; }
        }
        private string _firstName = string.Empty;

        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }
        private string _lastName = string.Empty;

        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }
        private string _birthDate = string.Empty;

        public string BirthDate
        {
            get { return _birthDate; }
            set { _birthDate = value; }
        }
        private string _companyName = string.Empty;

        public string CompanyName
        {
            get { return _companyName; }
            set { _companyName = value; }
        }
        private string _email = string.Empty;

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
        private string _pdfName = string.Empty;

        public string PdfName
        {
            get { return _pdfName; }
            set { _pdfName = value; }
        }
        private int _gCode = 0;

        public int GCode1
        {
            get { return _gCode; }
            set { _gCode = value; }
        }

        public int GCode
        {
            get { return _gCode; }
            set { _gCode = value; }
        }
        private string _gDesc = string.Empty;

        public string GDesc
        {
            get { return _gDesc; }
            set { _gDesc = value; }
        }
        private int _lCode = 0;

        public int LCode
        {
            get { return _lCode; }
            set { _lCode = value; }
        }
        private string _lDesc = string.Empty;

        public string LDesc
        {
            get { return _lDesc; }
            set { _lDesc = value; }
        }

        private string _pdfContent = string.Empty;

        public string PdfContent
        {
            get { return _pdfContent; }
            set { _pdfContent = value; }
        }

        private string _address = string.Empty;

        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }
        private string _postcode = string.Empty;

        public string Postcode
        {
            get { return _postcode; }
            set { _postcode = value; }
        }
        private string _city = string.Empty;

        public string City
        {
            get { return _city; }
            set { _city = value; }
        }
        private string _address2 = string.Empty;

        public string Address2
        {
            get { return _address2; }
            set { _address2 = value; }
        }




        private string _state = string.Empty;

        public string State
        {
            get { return _state; }
            set { _state = value; }
        }
        private string _country = string.Empty;

        public string Country
        {
            get { return _country; }
            set { _country = value; }
        }
        private string _fax = string.Empty;

        public string Fax
        {
            get { return _fax; }
            set { _fax = value; }
        }

        private string _telephone = string.Empty;

        public string Telephone
        {
            get { return _telephone; }
            set { _telephone = value; }
        }
        private string _secQ = string.Empty;

        public string SeqQ
        {
            get { return _secQ; }
            set { _secQ = value; }
        }
        private string _secA = string.Empty;

        public string SecA
        {
            get { return _secA; }
            set { _secA = value; }
        }
        private int _subType = 0;

        public int SubType
        {
            get { return _subType; }
            set { _subType = value; }
        }
        private string _prefix = string.Empty;

        public string Prefix
        {
            get { return _prefix; }
            set { _prefix = value; }
        }
        private string _houseNo = string.Empty;

        public string HouseNo
        {
            get { return _houseNo; }
            set { _houseNo = value; }
        }



        #endregion
    }
    public class Mobile
    {
        #region Mobile
        private string _ICCID = string.Empty;

        public string ICCID
        {
            get { return _ICCID; }
            set { _ICCID = value; }
        }
        private int _NuvInfoID = 0;

        public int NuvInfoID
        {
            get { return _NuvInfoID; }
            set { _NuvInfoID = value; }
        }
        private string _bmsisdn = string.Empty;

        public string BMSISDN
        {
            get { return _bmsisdn; }
            set { _bmsisdn = value; }
        }
        private string _Msisdn = string.Empty;

        public string Msisdn
        {
            get { return _Msisdn; }
            set { _Msisdn = value; }
        }
        private string _MsisdnStart = string.Empty;

        public string MsisdnStart
        {
            get { return _MsisdnStart; }
            set { _MsisdnStart = value; }
        }
        private string _MsisdnEnd = string.Empty;

        public string MsisdnEnd
        {
            get { return _MsisdnEnd; }
            set { _MsisdnEnd = value; }
        }
        private int _RoutingCount = 0;

        public int RoutingCount
        {
            get { return _RoutingCount; }
            set { _RoutingCount = value; }
        }
        private int _LCode = 0;

        public int LCode
        {
            get { return _LCode; }
            set { _LCode = value; }
        }
        private string _LDesc = string.Empty;

        public string LDesc
        {
            get { return _LDesc; }
            set { _LDesc = value; }
        }
        private int _IsVoiceMail = 0;

        public int IsVoiceMail
        {
            get { return _IsVoiceMail; }
            set { _IsVoiceMail = value; }
        }
        private string _PortingCode = string.Empty;

        public string PortingCode
        {
            get { return _PortingCode; }
            set { _PortingCode = value; }
        }
        private string _PrevMsisdn = string.Empty;

        public string PrevMsisdn
        {
            get { return _PrevMsisdn; }
            set { _PrevMsisdn = value; }
        }
        private int _RoutingID = 0;

        public int RoutingID
        {
            get { return _RoutingID; }
            set { _RoutingID = value; }
        }
        private int _RoutingAction = 0;

        public int RoutingAction
        {
            get { return _RoutingAction; }
            set { _RoutingAction = value; }
        }
        private int _IsSplit = 0;

        public int IsSplit
        {
            get { return _IsSplit; }
            set { _IsSplit = value; }
        }
        private string _MnpImp = string.Empty;

        public string MnpImp
        {
            get { return _MnpImp; }
            set { _MnpImp = value; }
        }

        private string _MnpExp = string.Empty;

        public string MnpExp
        {
            get { return _MnpExp; }
            set { _MnpExp = value; }
        }
        private string _MsisdnMstStart = string.Empty;

        public string MsisdnMstStart
        {
            get { return _MsisdnMstStart; }
            set { _MsisdnMstStart = value; }
        }
        private string _MsisdnMstEnd = string.Empty;

        public string MsisdnMstEnd
        {
            get { return _MsisdnMstEnd; }
            set { _MsisdnMstEnd = value; }
        }
        private int _IsVoc = 0;

        public int IsVoc
        {
            get { return _IsVoc; }
            set { _IsVoc = value; }
        }
        #endregion
    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.1015")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.mnp.org/mnp")]
    public partial class ReqPortingType
    {

        private HeaderType headerField;

        private string portingCodeField;

        private PortingRequestType[] portingRequestField;

        private System.DateTime desiredPortingDateField;

        /// <remarks/>
        public HeaderType Header
        {
            get
            {
                return this.headerField;
            }
            set
            {
                this.headerField = value;
            }
        }

        /// <remarks/>
        public string PortingCode
        {
            get
            {
                return this.portingCodeField;
            }
            set
            {
                this.portingCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PortingRequest")]
        public PortingRequestType[] PortingRequest
        {
            get
            {
                return this.portingRequestField;
            }
            set
            {
                this.portingRequestField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime DesiredPortingDate
        {
            get
            {
                return this.desiredPortingDateField;
            }
            set
            {
                this.desiredPortingDateField = value;
            }
        }
    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.1015")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.mnp.org/mnp")]
    public partial class PortingRequestType
    {

        private MsisdnSpecType msisdnSpecField;

        /// <remarks/>
        public MsisdnSpecType MsisdnSpec
        {
            get
            {
                return this.msisdnSpecField;
            }
            set
            {
                this.msisdnSpecField = value;
            }
        }
    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.1015")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.mnp.org/mnp")]
    public partial class MsisdnSpecType
    {

        private string msisdnField;

        private MsisdnRangeType msisdnRangeField;

        /// <remarks/>
        public string Msisdn
        {
            get
            {
                return this.msisdnField;
            }
            set
            {
                this.msisdnField = value;
            }
        }

        /// <remarks/>
        public MsisdnRangeType MsisdnRange
        {
            get
            {
                return this.msisdnRangeField;
            }
            set
            {
                this.msisdnRangeField = value;
            }
        }
    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.1015")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.mnp.org/mnp")]
    public partial class MsisdnRangeType
    {

        private string msisdnStartField;

        private string msisdnEndField;

        private int routingCountField;

        private bool routingCountFieldSpecified;

        /// <remarks/>
        public string MsisdnStart
        {
            get
            {
                return this.msisdnStartField;
            }
            set
            {
                this.msisdnStartField = value;
            }
        }

        /// <remarks/>
        public string MsisdnEnd
        {
            get
            {
                return this.msisdnEndField;
            }
            set
            {
                this.msisdnEndField = value;
            }
        }

        /// <remarks/>
        public int RoutingCount
        {
            get
            {
                return this.routingCountField;
            }
            set
            {
                this.routingCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool RoutingCountSpecified
        {
            get
            {
                return this.routingCountFieldSpecified;
            }
            set
            {
                this.routingCountFieldSpecified = value;
            }
        }
    }
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.1015")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.mnp.org/mnp")]
    public partial class HeaderType
    {

        private long requestIDField;

        private string senderField;

        private string receiverField;

        private string mNP_impField;

        private string mSP_impField;

        private string mNP_expField;

        private string mSP_expField;

        private System.DateTime timestampField;

        private int envFlagField;

        private string versionField;

        private int bcField;

        private bool bcFieldSpecified;

        /// <remarks/>
        public long RequestID
        {
            get
            {
                return this.requestIDField;
            }
            set
            {
                this.requestIDField = value;
            }
        }

        /// <remarks/>
        public string Sender
        {
            get
            {
                return this.senderField;
            }
            set
            {
                this.senderField = value;
            }
        }

        /// <remarks/>
        public string Receiver
        {
            get
            {
                return this.receiverField;
            }
            set
            {
                this.receiverField = value;
            }
        }

        /// <remarks/>
        public string MNP_imp
        {
            get
            {
                return this.mNP_impField;
            }
            set
            {
                this.mNP_impField = value;
            }
        }

        /// <remarks/>
        public string MSP_imp
        {
            get
            {
                return this.mSP_impField;
            }
            set
            {
                this.mSP_impField = value;
            }
        }

        /// <remarks/>
        public string MNP_exp
        {
            get
            {
                return this.mNP_expField;
            }
            set
            {
                this.mNP_expField = value;
            }
        }

        /// <remarks/>
        public string MSP_exp
        {
            get
            {
                return this.mSP_expField;
            }
            set
            {
                this.mSP_expField = value;
            }
        }

        /// <remarks/>
        public System.DateTime Timestamp
        {
            get
            {
                return this.timestampField;
            }
            set
            {
                this.timestampField = value;
            }
        }

        /// <remarks/>
        public int EnvFlag
        {
            get
            {
                return this.envFlagField;
            }
            set
            {
                this.envFlagField = value;
            }
        }

        /// <remarks/>
        public string Version
        {
            get
            {
                return this.versionField;
            }
            set
            {
                this.versionField = value;
            }
        }

        /// <remarks/>
        public int BC
        {
            get
            {
                return this.bcField;
            }
            set
            {
                this.bcField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool BCSpecified
        {
            get
            {
                return this.bcFieldSpecified;
            }
            set
            {
                this.bcFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.mnp.org/mnp")]
    public partial class ReqCancelType
    {

        private HeaderType headerField;

        private int cancellationCodeField;

        /// <remarks/>
        public HeaderType Header
        {
            get
            {
                return this.headerField;
            }
            set
            {
                this.headerField = value;
            }
        }

        /// <remarks/>
        public int CancellationCode
        {
            get
            {
                return this.cancellationCodeField;
            }
            set
            {
                this.cancellationCodeField = value;
            }
        }
    }
}
