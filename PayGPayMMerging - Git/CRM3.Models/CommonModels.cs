﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_API.Models
{
    public class CommonModels
    {
        public string ErrMsg { get; set; }
        public string errcode { get; set; }
    }

    public class HistoryLog
    {
        public DateTime LogTime { get; set; }
        public string Description { get; set; }
        public string CSR { get; set; }
    }
}