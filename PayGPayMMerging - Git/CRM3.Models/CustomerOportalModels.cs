﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models
{
    public class CustomerOportalList : CRM_API.Models.Common.ErrCodeMsg
    {
        public CustomerOportalOrderType order_type { get; set; }
        public string saver_operator { get; set; }
        public DateTime date_from { get; set; }
        public DateTime date_to { get; set; }
        public int account_id { get; set; }
        public DateTime reg_date { get; set; }
        public string regdate_string { get { return reg_date.ToString("dd-MM-yyyy H:mm"); } }
        public string mundio_cli { get; set; }
        public string dest_msisdn { get; set; }
    }

    public enum CustomerOportalOrderType
    {
       CustomerList = 1
    }
}

