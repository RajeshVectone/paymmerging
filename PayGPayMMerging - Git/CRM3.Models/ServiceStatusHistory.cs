﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models
{
    //class ServiceStatusHistory
    //{
    //}

    /// <summary>
    /// Get All Service Status History Details
    /// api/ServiceStatusHistory
    /// </summary>
    public class ServiceStatusHistory : Common.ErrCodeMsg
    {
        public string updatedate { get; set; }
        public string prev_service_status { get; set; }
        public string after_service_status { get; set; }
        public string remark { get; set; }
        public string updateby { get; set; }
        
    }


    /// <summary>
    /// Get All Service Status History Details
    /// api/ServiceStatusHistory
    /// </summary>
    /// Modified by BSK for Improvement : Jira CRMT-141
    public class SetupInProgress : Common.ErrCodeMsg
    {
        public string CustId_AccNo_SortCode { get; set; }
        

    }

    /// Modified by BSK for Improvement : Jira CRMT-141
    public class SuspendServiceNewModel : Common.ErrCodeMsg
    {
        public string UpdatedBy { get; set; }
        public string CustIDs { get; set; }
        public string sitecode { get; set; }

    }

    public class ActivateServiceNewModel : Common.ErrCodeMsg
    {
        public string UpdatedBy { get; set; }
        public string CustIDs { get; set; }

    }
}
