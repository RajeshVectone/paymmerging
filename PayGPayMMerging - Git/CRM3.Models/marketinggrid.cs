﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models
{
    public class marketinggrid
    {
        public string FreesimId { get; set; }   
        public string ICCID { get; set; }
        public string Email_Address { get; set; }
        public string First_Name { get; set; }
        public string Last_Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Mobile_Number { get; set; }
        public string Activate_Date { get; set; }
        public string Sent_Date { get; set; }
        public string Source_Address { get; set; }
        public string Ordersim_Url { get; set; }
        
    }
    public class marketinggrid_v2
    {
        public string FreesimId { get; set; }
        public string ICCID { get; set; }
        public string Email_Address { get; set; }
        public string First_Name { get; set; }
        public string Last_Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Mobile_Number { get; set; }
        public string Activate_Date { get; set; }
        public string Sent_Date { get; set; }
        public string Source_Address { get; set; }
        public string Ordersim_Url { get; set; }
        public string First_update { get; set; }
        public string Last_update { get; set; }
        public string Last_Topupdate { get; set; }
        public string SimType { get; set; }

    }
}
