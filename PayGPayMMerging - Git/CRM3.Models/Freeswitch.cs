﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models.Freeswitch
{
    public class Auth : ResponsMsg
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }        
        public string Token { get; set; }
        public Action ReqType { get; set; }
    }
    public class RequestWS:Auth
    {
        public string OrigExt { get; set; }
        public string DestNumber { get; set; }
    }
    public class ResponsMsg
    {
        public string ErrMsg { get; set; }
        public int ErrCode { get; set; }
        public int StatusCode { get; set; }
        public string Extension { get; set; }
        public string UserId { get; set; }
        public string TicketId { get; set; }
    }
    public enum Action
    {
        Login = 1,
        CallExt = 2,
        CallNumber = 3,
        Logout = 0
    }
}
