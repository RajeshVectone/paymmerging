﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models.FruadBlockUnBlockModels
{
    public class FruadBlockUnBlockInput : Common.ErrCodeMsg
    {
        public FruadBlockUnBlockMode Block_Type { get; set; }
        public string Product { get; set; }
        public string SiteCode { get; set; }
        public int USERTYPE { get; set; }
        public string USERINFO { get; set; }
        public int REQTYPE { get; set; }
        public int REASON_ID { get; set; }
        public int FK_REQ_USERID { get; set; }
        public int role_id { get; set; }
        public int REQID { get; set; }
        public string loginuser_name { get; set; }
    }

    public enum FruadBlockUnBlockMode
    {
        GetBlockReason = 1,
        DoBlockUnblockRequest = 2,
        GetBlockUnblockRequest = 3,
        DoBlockConfirmation = 4
    }

    public class BlockReason
    {
        public int REASON_ID { get; set; }
        public string REASON { get; set; }
        public int REASON_TYPE { get; set; }
    }

    public class BlockCommon
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
    }

    public class BlockUnblockRequestOutput
    {
        public int REASON_TYPE { get; set; }
        public int REQID { get; set; }
        public string USERINFO { get; set; }
        public string REQ_TYE { get; set; }
        public DateTime REQ_DATE { get; set; }
        public string REQ_DATE_string { get { return REQ_DATE.ToString("yyyy-MM-dd"); } }
        public string REASON { get; set; }
        public string REQ_STATUS { get; set; }
        public string REQ_BY { get; set; }
        public string APPROVED_BY { get; set; }
        public DateTime APPROVE_DATE { get; set; }
        public string APPROVE_DATE_string
        {
            get
            {
                if (!String.IsNullOrEmpty(APPROVED_BY))
                    return APPROVE_DATE.ToString("yyyy-MM-dd");
                else
                    return "";
            }
        }
        public string PRODUCT { get; set; }
    }
}
