﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models
{
    public class financeviewtrans
    {
        public string REFERENCE_ID { get; set; }
        public string SITECODE { get; set; }
        public string PAYMENT_AGENT { get; set; }
        public string SERVICE_TYPE { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string PAYMENT_MODE { get; set; }
        public string ACCOUNT_ID { get; set; }
        public string CC_NO { get; set; }
        public string TOTALAMOUNT { get; set; }
        public string CURRENCY { get; set; }
        public string CREATED_DATE { get; set; }
        public string SUBSCRIPTIONID { get; set; }
        public string CURRENT_STATUS { get; set; }
        public string CS_ERROR_CODE { get; set; }
        public string ReasonforReject { get; set; }
        public string ECI_Value { get; set; }
        public string Action { get; set; }
        public string ErrMsg { get; set; }

        public static object cardResult { get; set; }
    }



    public class financeviewtransdownload
    {
        public string REFERENCE_ID { get; set; }
        public string SITECODE { get; set; }
        public string PAYMENT_AGENT { get; set; }
        public string SERVICE_TYPE { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string PAYMENT_MODE { get; set; }
        public string ACCOUNT_ID { get; set; }
        public string CC_NO { get; set; }
        public string TOTALAMOUNT { get; set; }
        public string CURRENCY { get; set; }
        public string CREATED_DATE { get; set; }
        public string SUBSCRIPTIONID { get; set; }
        public string CURRENT_STATUS { get; set; }
        public string CS_ERROR_CODE { get; set; }
        public string ReasonforReject { get; set; }
        public string ECI_Value { get; set; }
        public string Action { get; set; }
        public string ErrMsg { get; set; }
        public string Days_from_1st_login { get; set; }
        public string Days_from_1st_topup { get; set; }
        public string days_from_Lasttopup { get; set; }
        public string total_spend { get; set; }
        public string total_spend_consum { get; set; }
        public string spend_last3months { get; set; }
        public string spend_curr_month { get; set; }
        public string Noof_cardtrans { get; set; }
        public string Noof_voucher_trans { get; set; }
        public string Noof_failed_cardtrans { get; set; }
        public string Noof_success_cardtrans_30Days { get; set; }
        public string Noof_Fail_cardtrans_30Days { get; set; }
        public string email { get; set; }
        public string Current_Balance { get; set; }
        public string IP_Address { get; set; }
        public static object cardResult { get; set; }
        public string requestid { get; set; }
        public string fraud_score { get; set; }
        //26-Aug-2019 : Moorthy added for retrieving SRD value
        public string srd { get; set; }
    }


    public class getafterpaymentdetails
    {
        public string REFERENCE_ID { get; set; }
        public string reason { get; set; }
        public string status { get; set; }
        public string update_date { get; set; }
        public string errcode { get; set; }
        

        public static object cardResult { get; set; }
    }


    public class getafterpaymentdetails_ctopup
    {
        public string REFERENCE_ID { get; set; }
        public string reason { get; set; }
        public string status { get; set; }
        public DateTime update_date { get; set; }
        public int errcode { get; set; }
        public string errmsg { get; set; }
        public string sms_sender { get; set; }
        public string sms_text { get; set; }
        public string freecredit { get; set; }
        public decimal prevbal { get; set; }
        public decimal afterbal { get; set; }
        public static object cardResult { get; set; }
    }




    public class getafterpaymentdetails_topup
    {
        public string REFERENCE_ID { get; set; }
        public string reason { get; set; }
        public string status { get; set; }
        public string update_date { get; set; }
        public int errcode { get; set; }
        public string errmsg { get; set; }
        public string sms_sender { get; set; }
        public string sms_text { get; set; }
        public string freecredit { get; set; }
        public string prevbal { get; set; }
        public string afterbal { get; set; }      
        public static object cardResult { get; set; }
    }

    public class Values
    {
        public string merchID { get; set; }
    }

    public class Sucess_failed
    {
        public string MerchID { get; set; }
        public string StatusCode { get; set; }
        public string Reason {get;set;}
        public string Create_revert { get; set; } // 0-- Create 1 -- revert
    }


    public class Sucess_failed_topup
    {
        public string MerchID { get; set; }
        public string StatusCode { get; set; }
        public string Reason { get; set; }
        public string ACCOUNT_ID { get; set; }
        public string TOTALAMOUNT { get; set; }

        
        public string Create_revert { get; set; } // 0-- Create 1 -- revert
    }

    public class cardResult
    {
        public string Decision { get; set; }
        public string MerchantReferenceCode { get; set; }
        public string ReasonCode { get; set; }
        public string RequestID { get; set; }
        public string RequestToken { get; set; }
    }

    //AddBalanceReport
    public class addbalancereport
    {
        public string mobileno { get; set; }
        public string referenceID { get; set; }
        public string username { get; set; }
        public double amount { get; set; }
        public double prevbalance { get; set; }
        public double afterbalance { get; set; }
        public string ErrMsg { get; set; }
    }

    //05-Feb-2019 : Moorthy : Added for auto Topup Report
    public class AutotopupReport
    {
        public int ref_code { get; set; }
        public string reg_date { get; set; }
        public string mobileno { get; set; }
        public string subscription_id { get; set; }
        public string currency { get; set; }
        public double? threshold_amount { get; set; }
        public double? amount { get; set; }
        public string decision { get; set; }
        public string request_id { get; set; }
        public string authorization_code { get; set; }
        public string request_time { get; set; }
        public int? payment_status { get; set; }
        public string payment_description { get; set; }
        public int? topup_status { get; set; }
        public string topup_description { get; set; }
        public string sorderid { get; set; }
        public string reconciliation_id { get; set; }
        //26-Aug-2019 : Moorthy added for retrieving SRD value
        public string srd { get; set; }
    }

    //12-Feb-2019 : Moorthy : Added for General Report
    public class GeneralReport
    {
        public string sitecode { get; set; }
        public string created_date { get; set; }
        public string account_id { get; set; }
        public string reference_id { get; set; }
        public double? totalamount { get; set; }
        public string payment_mode { get; set; }
        public string payment_agent { get; set; }
        public string product_code { get; set; }
        public string service_type { get; set; }
        public string currency { get; set; }
        public string subscriptionid { get; set; }
        public string cc_no { get; set; }
        public string ipclient { get; set; }
        public string email { get; set; }
        public string status { get; set; }
        public int? error_code { get; set; }
        public string error_msg { get; set; }
        public string sim_first_login { get; set; }
        public int? ECI_Value { get; set; }
        //26-Aug-2019 : Moorthy added for retrieving SRD value
        public string srd { get; set; }
    }
}
