﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models.HappyBundle
{

   public class countrylist : Common.ErrCodeMsg
   {
        public int id { get; set; }
        public string country_name { get; set; }
        public int country_Id { get; set; }
   }
    public class HappyBundleSearch:Common.ErrCodeMsg
    {      
        public HappyBundleType Bundle_Type { get; set; }
        public string msisdn { get; set; }
        public string iccid { get; set; }
        public string sitecode { get; set; }
        public string Dailnumber { get; set; }
    }

    public class HappyBundlelist : Common.ErrCodeMsg
    {
        public string msisdn { get; set; }
        public string Mapping_number { get; set; }
        public string destinationNB { get; set; }
        public DateTime startdate { get; set; }
        public string startdate_string
        {
            get
            {
                return startdate.ToString("dd-MM-yyyy");
            }
        }
        public int bundleid { get; set; }
        public string Mode { get; set; }
      

    }
   

    public enum HappyBundleType
    {
       
        HappyBundleList=1,
        HappyBundleListInsert= 2,
        Bundleid=3
    }
}
