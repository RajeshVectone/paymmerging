﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models.BreakageUsageModels
{
    public class BreakageUsageOutput
    {
        public string logdate { get; set; }
        public string mobileno { get; set; }
        public float GPRS_CHARGE { get; set; }
        public string package { get; set; }
        public int PACKAGE_ID { get; set; }
        public float INITIAL_BALANCE_MB { get; set; }
        public float ASSIGNED_BALANCE_MB { get; set; }
    }

    public class BreakageUsageInput
    {
        public string mobileno { get; set; }
        public string sitecode { get; set; }
        public string date_fr { get; set; }
        public string date_to { get; set; }
    }
}
