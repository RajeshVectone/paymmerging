﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models
{
    public class OrderManagement
    {

        public dynamic freesimid { get; set; }
        public string Fullname { get { return Convert.ToString(firstname) + " " + Convert.ToString(lastname); } }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string title { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address { get; set; }
        public string city { get; set; }
        public string postcode { get; set; }
        public string houseno { get; set; }
        public string telephone { get; set; }
        public string mobilephone { get; set; }
        public string email { get; set; }
        public DateTime? birthdate { get; set; }
        public int subscriberid { get; set; }
        public string msisdn_referrer { get; set; }
        public string iccid { get; set; }
        public string source_address { get; set; }
        public dynamic stat_id { get; set; }
        public string ordersim_url { get; set; }
        public string freesimstatus { get; set; }
        public dynamic registerdate { get; set; }
        public string registerdate_string { get { return registerdate != null ? registerdate.ToString("dd-MM-yyyy hh:mm:ss tt") : ""; } }
        public dynamic sentdate { get; set; }
        public dynamic activatedate { get; set; }
        public int Quantity { get; set; }
        public string CyberSourceId { get; set; }
        public string SourceReg { get; set; }
        public string CrmCs { get; set; }
        public string sitecode { get; set; }
        public string Subscriber_Type { get; set; }
        public string FreeSimType { get; set; }
        public int mimsi_order_id { get; set; }
        public string sim_brand { get; set; }
        public string homecountry_code { get; set; }
        public string royal_mail_reference { get; set; }
        public string order_status { get; set; }
    }
}
