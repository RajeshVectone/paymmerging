﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models.BundleService
{
    public class BundleServiceModels
    {
        public string mobileno { get; set; }
        public DateTime topup_date { get; set; }
        public string topup_date_string { get { return topup_date.ToString("dd-MM-yyyy hh:mm:ss tt"); } }
        public string paymentref { get; set; }
        public double amount { get; set; }
        public string ccdc_curr { get; set; }
        public string date_from { get; set; }
        public string date_to { get; set; }
    }
}
    