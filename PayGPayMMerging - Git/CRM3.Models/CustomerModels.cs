﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace CRM_API.Models
{
    public class CustomerSearchResult : CustomerMobileInfo
    {
        public string AccountNumber { get; set; }
        public int FreeSimId { get; set; }
        public string Login { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string SubscriberID { get; set; }
        public string MobileNo { get; set; }
        public string ICCID { get; set; }
        public int CustomerID { get; set; }
        public string ConnectionStatus { get; set; }
        public string Email { get; set; }
        public string SerialNumber { get; set; }
        public string Status { get; set; }
        public int mimsi_compatible { get; set; }
        public string Houseno { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string postcode { get; set; }
        public string State { get; set; }
        public string Telephone { get; set; }
        public string Mobilephone { get; set; }
        public string Fax { get; set; }
        public string country { get; set; }
        public string countrycode { get; set; }
        public string contact { get; set; }
        public string confirmcode { get; set; }
        //public string tariffclass { get; set; }
        public int SubscriberType { get; set; }
        public string TelcoCode { get; set; }
        public string SubscriberTypeName { get; set; }
        public static int IsMimsiCompatible = 1;
        public string sitecode { get; set; }
        public string DenomType { get; set; }
        public string SimType { get; set; }
        //07-Aug-2015 : Moorthy
        public string Sim_Type { get; set; }
        public string brand { get; set; }
        //13-Jan-2017 : Moorthy Added for ID File Download Func
        public string proof_file_name { get; set; }
        public string securityanswer { get; set; }

        //Added : 22-Dec-2018 : CRM Enhancement
        public decimal bonus_balance { get; set; }
        public int vectone_app { get; set; }
        public string passwd { get; set; }
        public string iccid { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }

        //Added : 11-Jan-2019 : SIM Activation details for AT
        public string act_flag { get; set; }
        public string act_by { get; set; }
        public DateTime act_on { get; set; }
        public string act_on_string
        {
            get
            {
                return act_on.ToString("dd-MM-yyyy hh:mm:ss tt");
            }
        }

        //30-May-2019 : Moorthy : Added for Notification Popup
        public int today_birthday { get; set; }
        public string last_interacted_on { get; set; }
        public string category { get; set; }
    }
    //Added by karthik Jira:CRMT-196
    public class CustomerTariffdetails : Common.ErrCodeMsg
    {
        public string Site_code { get; set; }
        public string Customer_code { get; set; }
        public string Batch_code { get; set; }
        public string Serial_code { get; set; }
        public string account_status { get; set; }
        public string tariff_class { get; set; }
        public decimal Balance_unit { get; set; }
        //Added by bsk for CRMT-252
        
        public decimal Balance_Credit_Bonus { get; set; }
        public decimal Total_Balance { get; set; }
        
        public DateTime First_usage { get; set; }
        public string Firstusage_string
        {
            get
            {
                return First_usage.ToString("dd-MM-yyyy hh:mm:ss tt");
            }
        }
        public DateTime start_date { get; set; }
        public string startdate_string
        {
            get
            {
                return start_date.ToString("dd-MM-yyyy hh:mm:ss tt");
            }
        }
        public DateTime End_date { get; set; }
        public string Enddate_String
        {
            get
            {
                return End_date.ToString("dd-MM-yyyy hh:mm:ss tt");
            }
        }

        public string smart_tariffclass { get; set; }
        public DateTime smart_startdate { get; set; }
        public string smart_startdate_String
        {
            get
            {
                return smart_startdate.ToString("dd-MM-yyyy hh:mm:ss tt");
            }
        }
        public DateTime smart_enddate { get; set; }
        public string smart_enddate_String
        {
            get
            {
                return smart_enddate.ToString("dd-MM-yyyy hh:mm:ss tt");
            }
        }
    }

    public class CustomerSearchPage : CommonModels
    {
        public CustomerSearchType Search_Type { get; set; }
        public string Product_Code { get; set; }
        public string SearchBy { get; set; }
        public string SearchText { get; set; }
        public string Sitecode { get; set; }
        public string SIM_Type { get; set; }
        public IEnumerable<CustomerSearchResult> Result { get; set; }
    }

    public class CustomerDetailPage : CommonModels
    {
        public string ActiveTab { get; set; }
        public string MobileNo { get; set; }
        public string Product_Code { get; set; }
        public string Sitecode { get; set; }
        public CustomerSearchResult Customer { get; set; }
        public Customer2in1 Detail2in1 { get; set; }
        public Customer2in1Order Order2in1 { get; set; }
        public Customer2in1Data Activate2in1 { get; set; }
        //Added by karthik Jira:CRMT-196
        public CustomerTariffdetails Customertariff { get; set; }
    }

    public enum CustomerSearchType
    {
        SearchCustomer = 1,
        SearchCustomerDetail = 2,
        GeneralInfo = 3,
        //Added by karthik Jira:CRMT-196
        TariffCustomerinfo = 4,

        //30-May-2019 : Moorthy : Added for Notification Popup
        LatestOpenComplaints = 5
    }

    public class CustomerMobileInfo : MvnoAccount
    {
        public string first_name { get; set; }
        public string last_name { get; set; }
        public int CustomerID { get; set; }
        public string ConnectionStatus { get; set; }
        public DateTime dob { get; set; }
        public string dob_string { get { return dob.ToString("dd-MM-yyyy"); } }
        public string address { get; set; }
        public DateTime? BirthDate { get; set; }
        public string BirthDate_String { get { return (this.BirthDate.HasValue) ? this.BirthDate.GetValueOrDefault().ToString("dd-MM-yyyy") : ""; } }
        public DateTime active_date { get; set; }
        public string active_date_string
        {
            get
            {
                return active_date.ToString("dd-MM-yyyy");
            }
        }
        public string product { get; set; }
        public string bill_type { get; set; }
        public string currency { get; set; }
        private decimal _balance;
        public decimal balance { get { return decimal.Parse(string.Format("{0:N}", _balance)); } set { _balance = value; } }
        public string balance_string { get { return string.Format("{0:N}", balance); } }
    }

    public class CustomerUpdatedata
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string sitecode { get; set; }
        public string Houseno { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string postcode { get; set; }
        public DateTime BirthDate { get; set; }
        public string country { get; set; }
        public int SubscriberID { get; set; }
        public string Email { get; set; }
        public string Title { get; set; }
        public string Sitecode { get; set; }
        public string Telephone { get; set; }
        public string SecurityAnswer { get; set; }
    }

    public class CancelLocation : CommonModels
    {
        public string mobileNo { get; set; }
        public string siteCode { get; set; }
        public string IMSIActive { get; set; }
        public string VLRNumber { get; set; }
        public string SGSNNumber { get; set; }
        public string GPRS { get; set; }
    }

    public class SimDetails : CommonModels
    {
        public string ICCID { get; set; }
        public string MSCategory { get; set; }
        public string IMSIActive { get; set; }
        public string VLRNumber { get; set; }
        public string MSCNumber { get; set; }
        public string SGSNNumber { get; set; }
        public string GGSNNumber { get; set; }
        public string SGSNAddress { get; set; }
        public string SubscriberStatus { get; set; }
    }
    public class smsmarketingchangeviewhistory
    {
        public string Action { get; set; }
        public string Date { get; set; }
        public string Mode { get; set; }
    }
    public class Email 
    {   
        public string msg { get; set; }
        public string title { get; set; }
        public string toAddress { get; set; }
    }
    public class Agent
    { 
    
    }

    //Customer Overview Page
    public class CustomerOverviewPageInput
    {
        public string MobileNo { get; set; }
        public string Product_Code { get; set; }
        public string Sitecode { get; set; }
        public CustomerOverviewPageType Type { get; set; }

        //CRM Enhancement 2
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public int? PackageID { get; set; }
    }

    //CRM Enhancement 2
    public enum CustomerOverviewPageType
    {
        GetVectoneXtraDetails = 1,
        GetVectoneXtraCallHistory = 2,
    }

    public class CustomerOverviewPageOutput
    {
        public string mobileno { get; set; }
        public double balance { get; set; }
        public double bonus_balance { get; set; }
        public string bundle_allowance { get; set; }
        public string bundle_remain_allowance { get; set; }
        public DateTime last_update { get; set; }
        public DateTime gprs_loc_update { get; set; }
        public DateTime last_topup { get; set; }
        public DateTime last_call_date { get; set; }
        public DateTime last_sms_date { get; set; }
        public DateTime last_gprs_date { get; set; }

        public string last_update_str { get { return last_update.ToString("dd-MM-yyyy HH:mm:ss"); } }
        public string gprs_loc_update_str { get { return gprs_loc_update.ToString("dd-MM-yyyy HH:mm:ss"); } }
        public string last_topup_str { get { return last_topup.ToString("dd-MM-yyyy HH:mm:ss"); } }
        public string last_call_date_str { get { return last_call_date.ToString("dd-MM-yyyy HH:mm:ss"); } }
        public string last_sms_date_str { get { return last_sms_date.ToString("dd-MM-yyyy HH:mm:ss"); } }
        public string last_gprs_date_str { get { return last_gprs_date.ToString("dd-MM-yyyy HH:mm:ss"); } }

        public string roaming_country { get; set; }
        public string roaming_outcall { get; set; }
        public string roaming_outsms { get; set; }
        public string roaming_outdata { get; set; }
        public string bundle_name { get; set; }
        public double bundle_price { get; set; }
        public int bundleid { get; set; }

        public string reseller_name { get; set; }
        public string portin_date { get; set; }
        public string portin_operator { get; set; }
    }

    public class UserRole
    {
        public int role_id { get; set; }
        public string name { get; set; }
    }

    //30-May-2019 : Moorthy : Added for Notification Popup
    public class LatestOpenComplaints
    {
        public string Ticket_id { get; set; }
        public string Issue_Name { get; set; }
        public string Function_Name { get; set; }
        public string status { get; set; }
        public int open_status { get; set; }
    }

}