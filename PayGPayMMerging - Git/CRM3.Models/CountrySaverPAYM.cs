﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models.CountrySaverPAYM
{
   
    public class CountrySaverList : Common.ErrCodeMsg
    {
        public string RegID { get; set; }
        public string PackageID { get; set; }
        public string Charge { get; set; }
        public string Currency { get; set; }
        public string DestinationNumber { get; set; }
        public string Description { get; set; }
        public Decimal MinRemaining { get; set; }
        public Decimal MinUsed { get; set; }
        public Decimal Amount { get; set; }
        public string Number { get; set; }
        public string Account_ID { get; set; }
        //Add by BSK for CRMT-221 reqd.
        public string AccountNo { get; set; }
        public string TotalAmount { get; set; }
        public DateTime SubscriptionDate { get; set; }
        public DateTime RenewalDate { get; set; }
        public int Status { get; set; }
        public string Status_Description { get; set; }
        public string Product_Type { get; set; }
        public CS_Paymode PayMode { get; set; }
        public string PaymentType_Description
        {
            get
            {
                return Enum.GetName(typeof(CS_Paymode), PayMode);
            }
        }

        public string BundleID { get; set; }
        public string Pack_Dest { get; set; }

        //added on 30 Jan 2015 - by Hari
        public string Main_Status { get; set; }
        public Decimal price { get; set; }

        public string AccountNumber { get; set; }
        public List<AdditionalCLI> AdditionalCLIs { get; set; } 
    }

    //8-Jan-2016 : Moorthy : Added for Country Saver
    public class CountrySaverList_New : Common.ErrCodeMsg
    {
        public string packageid { get; set; }
        public string description { get; set; }
        public int status { get; set; }
        public string mundio1 { get; set; }
        public string mundio2 { get; set; }
        public string mundio3 { get; set; }
        public string mundio4 { get; set; }
        public DateTime subscriptiondate { get; set; }
        public DateTime RenewalDate { get; set; }
        public string currency { get; set; }
        public string destinationCli1 { get; set; }
        public string destinationCli2 { get; set; }
        public string destinationCli3 { get; set; }
        public string destinationCli4 { get; set; }
        public string destinationCli5 { get; set; }
        public string Minremaining { get; set; }
        public string PayModeRerence { get; set; }
        public string Main_Status { get; set; }
        public string amount { get; set; }
        public int bundleid { get; set; }

        public string AccountNumber { get; set; }
        public string pack_dest { get; set; }

        public bool DisplayCancel
        {
            get
            {
                if (Main_Status == "Active")
                    return true;
                else
                    return false;
            }
        }
        public bool DisplayChangepaymentMode
        {
            get
            {
                if (Main_Status == "Active")
                    return true;
                else
                    return false;
            }
        }
        public bool DisplayReactivate
        {
            get
            {
                if (Main_Status == "Cancelled")
                    return true;
                else
                    return false;
            }
        }

        public bool DisplaySuspended
        {
            get
            {
                if (Main_Status == "Suspended")
                    return true;
                else
                    return false;
            }
        }

        public List<AdditionalCLI> AdditionalCLIs { get; set; } 
    }

    public class StatusList {
        public int Status_Id { get; set; }
        public string Status_Description { get; set; }
    
    }

    /* status code and definition 
        7 = active
     * 13 = available to renew
     * 18 = stop, available to reactivate
     */

    public class AdditionalReqInfo:Common.ErrCodeMsg
    {
        public AdditionInfo InfoType { get; set; }
        public string RegIDX { get; set; }
        public string Sitecode { get; set; }
        public string Mobileno { get; set; }


        public string Paymode { get; set; }
        public int bundleid { get; set; }
        public int accountid { get; set; }
        public string DestinationCli { get; set; }


        //Payment
        public string LoggedUser { get; set; }
        public string country { get; set; }
        public string paytype { get; set; }
        public int mode { get; set; }
    }


    public enum AdditionInfo
    {
        GETList = 0,        //because value is 0, this is not mandatory     
        RENEW = 1,          // renew country saver
        REACTIVATE = 2,     //reactivate country saver
        CANCEL = 3,         //cancel country saver
        GETSTATUS = 4,  
        GETListLLOM = 5,  //add country saver list for LLOM, by Hari - 19 Jan 2015
        GetListCLI = 6,
        Test = -100,
        ViewSubscriptionhistory = 9,
        REACTIVATEBYBALANCE = 10,
        REACTIVATEBYEXISTINGCARD = 11,
        REACTIVATEBYNEWCARD = 12,
        SavedCardDetails = 13
    }

   
    public enum CS_Paymode
    {
        [CRM_API.Models.Common.EnumStringValue("Balance")]
        Balance = 1,
        [CRM_API.Models.Common.EnumStringValue("Credit Card")]
        CreditCard = 2
    }

    /*Added on 23-02-2015 - for Additional CLI*/
    public class AdditionalCLI : AdditionalReqInfo
    {
        public int SubscriberId { get; set; }           // Subscriber Id
        public string CLI { get; set; }                 // Additional Vectone Number
        public DateTime SubscriptionDate { get; set; }  // Subscription Date (Date of addition of Additional number)
        public Decimal MinUsed { get; set; }            // Minutes Used (No of mins used this month )
        public string Product_Type { get; set; }        // Number Type (Landline/Vectone Mobile UK/ Vectone Mobile FR/ etc)
        public string Brand { get; set; }               // Brand 
        public string FandFNo { get; set; }             // F&F No
        public string AccountId { get; set; }           // Account Id
        public string Status { get; set; }              // Status column to display the status
        public string Action { get; set; }              // Action column to display the button

        //08-Jan-2015 : Moorthy : Added to show the Additional CLI Details
        public string addon_msisdn { get; set; }
        public string subscription_Date { get; set; }
        public string minutes_used { get; set; }
        public string clitype { get; set; }
    }

    public class CountrySaverPaymentHistory
    {
        public DateTime Date { get; set; }
        public string Action { get; set; }
        public string Bundle_Details { get; set; }
        public string Amount { get; set; }
        public string PaymentMode { get; set; }
        public string Payment_Status { get; set; }
    }
}