﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models.CDR
{
    public class CallHistorySearch
    {
        public CDRType HistoryType { get; set; }
        public string mobileno { get; set; }
        public DateTime Date_From { get; set; }

        public string Date_From_date_string { get { return Date_From.ToString("yyyy-MM-dd"); } }
        public string Date_from_month_string { get { return Date_From.ToString("MM"); } }
        public string Date_from_day_string { get { return Date_From.ToString("dd"); } }
        public string Date_from_year_string { get { return Date_From.ToString("yyyy"); } }
        public DateTime Date_To { get; set; }
        public string Date_To_date_string { get { return Date_To.ToString("yyyy-MM-dd"); } }
        public string Date_to_month_string { get { return Date_To.ToString("MM"); } }
        public string Date_To_day_string { get { return Date_To.ToString("dd"); } }
        public string Date_To_year_string { get { return Date_To.ToString("yyyy"); } }
        public string Sitecode { get; set; }
        public int PageperRow { get; set; }
        public int PageNumber { get; set; }
        //Modif by karthik CRMT-198
        public string Packageid { get; set; }
        public string ProductCode { get; set; }
    }

    public class CallHistory : Common.ErrCodeMsg
    {
        private DateTime _callDate;
        public string CallDate { get { return _callDate.ToString("dd-MM-yyyy hh:mm:ss tt"); } set { _callDate = DateTime.ParseExact(value, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture); } }
        public string CustCode { get; set; }
        public string BatchCode { get; set; }
        public string SerialCode { get; set; }
        public string Ani { get; set; }
        public string DialedNum { get; set; }
        public string DestCode { get; set; }
        private decimal _balance;
        public string Balance { get { return decimal.Round(_balance, 2, MidpointRounding.AwayFromZero).ToString(); } set { _balance = decimal.Parse(value); } }
        public string TalkTime { get; set; }
        public string Type { get; set; }
        public decimal TalkTime_Minutes { get { return decimal.Round(decimal.Parse(TalkTime) / 60, 2, MidpointRounding.AwayFromZero); } }
        public string TalkTime_Minutes_string { get { return String.Format("{0:0.00}", TalkTime_Minutes).Replace(",", "."); } }
        public string TalkCharge { get; set; }
        public string TotalCons { get; set; }
        public string CurrCode { get; set; }
        public string FreeBalance { get; set; }

    }
    public class SMSHistory : Common.ErrCodeMsg
    {
        public DateTime Cnxdate { get; set; }
        public string Cnxdate_String { get { return Cnxdate.ToString("dd-MM-yyyy HH:mm:ss"); } }
        public string Cli { get; set; }
        public string DialedNum { get; set; }
        public string DestCode { get; set; }
        public string Type { get; set; }
        public double TalkCharge { get; set; }
        public string TalkCharge_string { get { return String.Format("{0:0.00}", TalkCharge).Replace(",", "."); } }
        private decimal _balance;
        public decimal Balance { get { return decimal.Round(_balance, 2, MidpointRounding.AwayFromZero); } set { _balance = value; } }
        public int TalkTime { get; set; }
        public decimal TalkTime_Minutes { get { return decimal.Round((TalkTime / 60), 2, MidpointRounding.AwayFromZero); } }
        public string TalkTime_Minutes_string { get { return String.Format("{0:0.00}", TalkTime_Minutes).Replace(",", "."); } }
        public string Currency { get; set; }
        public string CurrCode { get; set; }
        public int Result { get; set; }
    }

    public class GPRSHistory
    {
        private DateTime _cnxdate;
        public string CnxDate { get { return _cnxdate.ToString("dd-MM-yyyy hh:mm:ss tt"); } set { _cnxdate = DateTime.ParseExact(value, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture); } }
        public string GprsAllocation { get; set; }
        public decimal GprsAllocation_MB { get { return decimal.Round(decimal.Parse(GprsAllocation) / 1024, 2, MidpointRounding.AwayFromZero); } }
        private decimal _gprscharge;
        public string GprsCharge { get { return String.Format("{0:N}", (_gprscharge)); } set { _gprscharge = decimal.Parse(value); } }
        //public string GprsCharge { get; set; }
        public string Type { get; set; }
        public string TariffClass { get; set; }
        public string RoamingZone { get; set; }
        public string PackageID { get; set; }
    }

    //5Jan2015 : Moorthy : Added Jira CRMT:198
    public class CallHistory_v1 : Common.ErrCodeMsg
    {

        public string History_Type { get; set; }
        //5Jan2015 : Moorthy : Added Jira CRMT:198
        public string Package_Name { get; set; }
        public DateTime Date_Time { get; set; }
        // public string dateTime_String { get { return Date_Time.ToString("dd-MM-yyyy hh:mm:ss tt"); } }
        public string dateTime_String { get { return Date_Time.ToString("dd-MM-yyyy hh:mm:ss tt"); } set { Date_Time = DateTime.ParseExact(value, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture); } }
        public string CLI { get; set; }
        public string Destination_Number { get; set; }
        public string Type { get; set; }
        public string Destination_Code { get; set; }
        public string Duration_Min { get; set; }
        public string DurationMin_string { get { return String.Format("{0:0.00}", Duration_Min).Replace(",", "."); } }
        private decimal Data_Usage;
        public string DataUsage_String { get { return String.Format("{0:N}", (Data_Usage)); } set { Data_Usage = decimal.Parse(value); } }
        public string Tariff_Class { get; set; }
        public string Roaming_Zone { get; set; }
        public string Net_Charges { get; set; }
        public decimal Balance;
        public string Balance_String { get { return decimal.Round(Balance, 2, MidpointRounding.AwayFromZero).ToString(); } set { Balance = decimal.Parse(value); } }
        public int Status { get; set; }
        //05-Mar-2019 : Moorthy : Added for 'Excess Credit Usage' Column
        public string ExcessCreditUsage { get { return ""; } }
    }

    public enum CDRType
    {
        //5Jan2015 : Moorthy : Added Jira CRMT:198
        AllHistory = 0,
        CallHistory = 1,
        SMSHistory = 2,
        GPRSHistory = 3,
        //5Jan2015 : Moorthy : Added Jira CRMT:198
        CallandSMS = 5,
        //25-Jan-2019 : Moorthy : Added for View Prefix
        ViewPrefix = 6,
        //25-Jan-2019 : Moorthy : Added for Excess Usage
        ExcessUsage = 7
    }

    //25-Jan-2019 : Moorthy : Added for View Prefix
    public class ViewHistoryOutput
    {
        public string name { get; set; }
        public string phonecode { get; set; }
    }
}
