﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_API.Models.Payment
{
    public class Subscription
    {
        public string account_id { get; set; }
        public string Last6digitsCC { get; set; }
        public string SubscriptionID { get; set; }
        public string expirydate { get; set; }
        public string purchasedate { get; set; }
        public string provider_code { get; set; }
        public int idx { get; set; }
        public string cvv_number { get; set; }
    }

    public class CardDetails : Common.ErrCodeMsg 
    {
        public string SubscriptionID { get; set; }
        public string card_number { get; set; }
        public string card_expiry_month { get; set; }
        public string card_expiry_year { get; set; }
        public string card_issue_number { get; set; }
        public string card_verf_code { get; set; }
        public string card_first_name { get; set; }
        public string card_last_name { get; set; }
        public string card_post_code { get; set; }
        public string card_house_number { get; set; }
        public string card_address { get; set; }
        public string card_city { get; set; }
        public string card_country { get; set; }
        public string card_type { get; set; }
    }
}