﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models
{
    public enum PAYGRecommendedBonusRequestType
    {
        RBOverView = 0,
        RBReferralList = 1,
        RBTransactionList = 2,
        RBReferralDetailsView = 3,
        RBReferralDetailsUpdate = 4,
        RBTransactionDetailsView = 5,
        RBTransactionDetailsUpdate = 6,
        RBBindReferralStatus = 7,
        RBBindReferralSimStatus = 8,
        RBBindRechargeMode = 9,
        RBBindTransactionType = 10,
        RBBindPaymentStatus = 11
    }

    public class PAYGRecommendedBonus : Common.ErrCodeMsg
    {
        public PAYGRecommendedBonusRequestType requestType { get; set; }
        public string User_Id { get; set; }
        public string Site_Code { get; set; }

        public string Referral_Status { get; set; }
        public string SIM_Status { get; set; }
        public string Recharge_Mode { get; set; }

        public string Transfer_Type { get; set; }
        public string Payment_Status { get; set; }

        public string Referral_Id { get; set; }
        public string Credit_Status_Desc { get; set; }
        public string SIM_Status_Desc { get; set; }
        public string User_Status_Desc { get; set; }

        public string Trans_Id { get; set; }
        public string Transfer_Status_Desc { get; set; }
        public string Payment_Status_Desc { get; set; }
    }

    public class PAYGRBOverview : Common.ErrCodeMsg
    {
        public string Total_Credit_Available { get; set; }
        public string SIM_Requests { get; set; }
        public string Active_SIMs { get; set; }
        public string Bonus_Credited { get; set; }
    }

    public class PAYGRBReferralHistory : Common.ErrCodeMsg
    {
        public string Referral_Id { get; set; }
        public DateTime SIM_Order_Date { get; set; }
        public string Invitee_Name { get; set; }
        public string Referral_Status { get; set; }
        public string SIM_Status { get; set; }
        public string Amount_Recharged { get; set; }
        public string Value_Credited { get; set; }
        public string Credit_Status { get; set; }
        public string Recharge_Mode { get; set; }
        public string Action { get { return string.Format("Action{0}", this.Referral_Id); } }
    }

    public class PAYGRBReferralDetailView : Common.ErrCodeMsg
    {
        public string Invitee_Name { get; set; }
        public string User_Id { get; set; }
        public string Referral_Id { get; set; }
        public string Credit_Status { get; set; }
        public string Credit_Status_Desc { get; set; }
        public string SIM_Status { get; set; }
        public string SIM_Status_Desc { get; set; }
        public string Credit_Validity_Status { get; set; }
        public string User_Status { get; set; }
        public string User_Status_Desc { get; set; }
    }

    public class PAYGRBTransactionDetailView : Common.ErrCodeMsg
    {
        public string Trans_id { get; set; }
        public string Transfer_Status { get; set; }
        public string Transfer_Status_Desc { get; set; }
        public string Bank_Name { get; set; }
        public string First_Name { get; set; }
        public string Surname { get; set; }
        public string Sort_Code { get; set; }
        public string AC_Number { get; set; }
        public string Credit_Available { get; set; }
        public string Payment_Status { get; set; }
        public string Payment_Status_Desc { get; set; }
    }

    public class PAYGRBReferral_ReferralStatus : Common.ErrCodeMsg
    {
        public string Referral_Status { get; set; }
    }

    public class PAYGRBReferral_ReferralSimStatus : Common.ErrCodeMsg
    {
        public string SIM_Status { get; set; }
    }

    public class PAYGRBReferral_RechargeMode : Common.ErrCodeMsg
    {
        public string Recharge_Mode { get; set; }
    }

    public class PAYGRBTransaction_Transfer_Type : Common.ErrCodeMsg
    {
        public string Transfer_Type { get; set; }
    }

    public class PAYGRBTransaction_Payment_Status : Common.ErrCodeMsg
    {
        public string Payment_Status { get; set; }
    }

    public class PAYGRBTransactionHistory : Common.ErrCodeMsg
    {
        public string Trans_Id { get; set; }
        public string Request_Date { get; set; }
        public string Transfer_Type { get; set; }
        public string Transfer_Date { get; set; }
        public string Payment_Status { get; set; }
        public string Bank_Account_Mapped { get; set; }
        public string Active_Bank_Account { get; set; }
        public string Amount_Transferred { get; set; }
        public string Action { get { return string.Format("Action{0}", this.Trans_Id); } }
    }

}
