﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models.CustomerProfile
{
    public class PaymentProfileCustomer
    {
        public bool IsDefault { get; set; }
        public string CardNumber { get; set; }
        public string Status { get; set; }
        public DateTime SetupDate { get; set; }
        public DateTime LastUsedDate { get; set; }
        public decimal LastPaymentAmount { get; set; }
        public string Currency { get; set; }
        public string MoreInfoAction { get; set; }

        //22-Feb-2019 : Moorthy : Added for Block Card
        public int card_status { get; set; }
        public string blocked_by { get; set; }
        public string block_date { get; set; }
    }
    public class CustomerPaymentRequest
    {
        public string Msisdn { get; set; }
        public string Sitecode { get; set; }
        public string LastCCNumber { get; set; }
        public Action RequestType { get; set; }
        public int AxTopupStatus { get; set; }
        public decimal AxTopupAmount { get; set; }
        public string Currency { get; set; }
        public int PaymentType { get; set; }
        //22-Feb-2019 : Moorthy : Added for Block Card
        public string CalledBy { get; set; }
        public int card_status { get; set; }
    }
    public enum Action
    {
        ListProfile = 1,
        DetailInfo = 2,
        SettingInfo = 3,
        SettingUpdate = 5,
        SetDefaultProfile = 8,
        //22-Feb-2019 : Moorthy : Added for Block Card 
        DoBlockCard = 9
    }
    public class CustomerCCInfo
    {
        public string NameCardholder { get; set; }
        public string CardType { get; set; }
        public string CardNumber { get; set; }
        public string ExpDate { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Postcode { get; set; }
    }

    //22-Feb-2019 : Moorthy : Added for Block Card 
    public class DoBlockResponse
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
    }
}
