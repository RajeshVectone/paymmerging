﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace CRM_API.Models.Permission
{
    public class PermissionRecord : Common.ErrCodeMsg
    {
        public PermissionRecord()
        {
            this.Users = new HashSet<User>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string SystemName { get; set; }
        public string Category { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
    public class User : Common.ErrCodeMsg
    {
        public User()
        {
            this.PermissionRecords = new HashSet<PermissionRecord>();
        }

        public int Id { get; set; }
        public System.Guid UserGuid { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }

        public string Email { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [DataType(DataType.Password)]
        public string RetypePassword { get; set; }
        public int PasswordFormatId { get; set; }
        public string PasswordSalt { get; set; }
        public string AdminComment { get; set; }
        public bool Active { get; set; }
        public bool Deleted { get; set; }
        public bool IsSystemAccount { get; set; }
        public string SystemName { get; set; }
        public string LastIpAddress { get; set; }
        public System.DateTime CreatedOnUtc { get; set; }
        public Nullable<System.DateTime> LastLoginDateUtc { get; set; }
        public System.DateTime LastActivityDateUtc { get; set; }


        public string FullName { get; set; }
        public string Status { get; set; }
        public string AssignEmails { get; set; }
        public Nullable<int> assign_email { get; set; }
        public string LastLoginDate_str { get; set; }
        public string PermissionList { get; set; }
        public int mode { get; set; }
        public string sitecode { get; set; }
        public virtual ICollection<PermissionRecord> PermissionRecords { get; set; }
        public int role_id { get; set; }
    }
    public class PermissionRecord_Role_Mapping
    {
        public int PermissionRecord_Id { get; set; }
        public int User_Id { get; set; }
    }
}
