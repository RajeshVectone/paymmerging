﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models
{
    public class ConnectionString
    {
        public string esp_connstring { get; set; }
        public string crm_connstring { get; set; }
        public string hlr_connstring { get; set; }
    }
}
