﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models.AutoRenewal
{
    public class AutoRenewalInfo : AutoRenewalInput
    {
        public AutoRenewalType ModelType { get; set; }
    }

    public enum AutoRenewalType
	{
        GetAutoRenwalInfo = 1,
        EnableDisableProcess = 2
	}

    public class AutoRenewalInput
    {
        public string mobileno { get; set; }
        public string sitecode { get; set; }
        public string iccid { get; set; }
        public string crm_user { get; set; }
        public int processflag { get; set; }
        public string firstname { get; set; }
        public string email { get; set; }
    }

    public class AutoRenewalOutput
    {
        public string msisdn { get; set; }
        public string auto_ren_status { get; set; }
        public string disbale_by { get; set; }
        public string enable_by { get; set; }
        public DateTime? enable_date { get; set; }
        public string enable_date_str { get { return enable_date != null ? Convert.ToDateTime(enable_date).ToString("yyyy-MM-dd") : "" ; } }
        public DateTime? disable_date { get; set; }
        public string disable_date_str { get { return disable_date != null ? Convert.ToDateTime(disable_date).ToString("yyyy-MM-dd") : ""; } }
    }

    public class Output
	{
		public int errcode { get; set; }
        public string errmsg { get; set; }
        public string cur_date { get; set; }
        public string cur_time { get; set; }
        public string exp_date { get; set; }
	}
}
