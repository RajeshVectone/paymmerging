﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models
{
    public class SMSCampaignModel : Common.ErrCodeMsg
    {
        public String Campaign_Name{get; set;}
        public DateTime Run_Date{get;set;}
        public String Goal {get;set;}
        public String Delivery_status {get; set;}
        public String Goal_Status { get; set; }
    }

    public enum CampaignModelType
    {
        GetCustomerWiseSMSNotification = 1,
        SendSMS = 2
    }

    public class SMSCampaignRequest : Common.ErrCodeMsg
    {
        public int Id { get; set; }
        public string MobileNo { get; set; }
        public CampaignModelType Action { get; set; }
        public string Sitecode { get; set; }
        public string LoggedUser { get; set; }
        public bool IsDownload { get; set; }
        //31-Jul-2015 : Moorthy Added
        public string ProductCode { get; set; }
        //26-Dec-2018 : Moorthy : Addded for SMS module
        public string SMSText { get; set; }
        public string smsType { get; set; }
        public string customer_name { get; set; }
        public string mode { get; set; }
    }

    public class SMSMarketingHistory : Common.ErrCodeMsg
    {
        public string Action { get; set; }
        public DateTime Date { get; set; }
        public string Mode { get; set; }
    }

    public class CustomerWiseSMSNotification
    {
        public int id { get; set; }
        public DateTime sms_sent_date { get; set; }
        public string sms_sent_date_string { get { return sms_sent_date.ToString("dd-MM-yyyy hh:mm:ss tt"); } }
        public string msisdn { get; set; }
        public string sms_text { get; set; }
        public string sms_module { get; set; }
        public string sms_status { get; set; }
        public string crm_user { get; set; }
    }

    public class InsertSMSNotifyTextOutput
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
    }
}
