﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models.Roaming
{
    public class RoamingInfo : Common.ErrCodeMsg
    {
        public string rateIncoming { get; set; }
        public string rate_incomming_sms { get; set; }
        public string rateoutging { get; set; }
        public string rate_outgoing_sms { get; set; }
        public string rate_data_charge { get; set; }
        public string roaming_country { get; set; }
    }

    public class RoamingInfo_Request : Common.ErrCodeMsg
    {
        public string ICCID { get; set; }
        public string MOBILENO { get; set; }
        public string SITECODE { get; set; }
    }
}
