﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models.AirTimeTransfer
{
   
    public class AirTimeTransferRes : Common.ErrCodeMsg
    {
        public string AirtimeID { get; set; }
        public string DonorID { get; set; }
        public string RecipientID { get; set; }
        public string RecipientName { get; set; }
        public string DonorCountry { get; set; }
        public string RecipientCountry { get; set; }
        public string Currency { get; set; }
        public string Method { get; set; }
        public string TransactionReference { get; set; }
        public string Status { get; set; }
        public string FailedReason { get; set; }
        public string ProviderName { get; set; }
        public Decimal ProviderAmount { get; set; }
        public Decimal TransferAmount { get; set; }
        public Decimal ReceivedAmount { get; set; }
        public DateTime AirTimeDate{ get; set; }
        
    }

    public class AdditionalReqInfo:Common.ErrCodeMsg
    {
        public AdditionInfo InfoType { get; set; }
        public string Sitecode { get; set; }
        public string Mobileno { get; set; }
        public string Datefrom { get; set; }
        public string Dateto { get; set; }
        public string Name { get; set; }
    }


    public enum AdditionInfo
    {
        GETList = 0,     //because value is 0, this is not mandatory     
        Detail = 1,     //details - UNDEFINED YET
        Test = -100
    }
}
