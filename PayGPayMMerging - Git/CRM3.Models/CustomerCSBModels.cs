﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_API.Models
{
    public class CustomerCSBRecords : Common.ErrCodeMsg
    {
        public int account_id { get; set; }
        public string mundio_cli { get; set; }
        public string dest_msisdn { get; set; }
        public string dest_country { get; set; }
        public DateTime subscription { get; set; }
        public string subscription_string { get { return subscription.ToString("dd-MM-yyyy"); } }
        public string renewal_method { get; set; }
        public DateTime expiry { get; set; }
        public string expiry_string { get { return expiry.ToString("dd-MM-yyyy"); } }
        public string status { get; set; }
        public string remaining { get; set; }
        public int order_id { get; set; }
        public string new_sim { get; set; }
    }

    public class CustomerCSBHistory : CRM_API.Models.Payment.CardDetails
    {
        public CustomerCSBOrderRecords order_records { get; set; }
        public int order_id { get; set; }
        public string account_id { get; set; }
        public string csb_id { get; set; }
        public string site_code { get; set; }
        public string mobileno { get; set; }
        public string destno { get; set; }
        public string logdate { get; set; }
        public string action { get; set; }
        public string description { get; set; }
        public string imsi_country { get; set; }
        public string imsi_mobileno { get; set; }
        public string swapiccid { get; set; }
        public DateTime order_date { get; set; }
        public string order_date_string { get; set; }
        public string order_status { get; set; }
        public DateTime process_date { get; set; }
        public string process_date_string
        {
            get
            {
                if (process_date == DateTime.MinValue)
                    return "-";
                else
                    return process_date.ToString("dd-MM-yyyy");
            }
        }
        public DateTime expired_date { get; set; }
        public string expire_date_string
        {
            get
            {
                if (expired_date == DateTime.MinValue || order_status.ToLower() != "active")
                    return "-";
                else
                    return expired_date.ToString("dd-MM-yyyy");
            }
        }
        public string renewal_date_string
        {
            get
            {
                if (expired_date == DateTime.MinValue || order_status.ToLower() != "active")
                    return "-";
                else
                    return expired_date.AddDays(1).ToString("dd-MM-yyyy");
            }
        }
        public int renewal_method { get; set; }
        public string renewal_method_string { get; set; }
        public double monthly_charge { get; set; }
        public string monthly_charge_string { get; set; }
        public string imsi_location { get; set; }
        public string imsi { get; set; }
        public string title { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string address2 { get; set; }
        public string address1 { get; set; }
        public string town { get; set; }
        public string postcode { get; set; }
        public string country { get; set; }
        public string email { get; set; }
        public string contact { get; set; }
        public int can_be_activated { get; set; }
        public string new_iccid_to_swap { get; set; }
        public string crm_login { get; set; }
        public string payref { get; set; }
        public string user_login{get;set;}
    }

    public enum CustomerCSBOrderRecords
    {
        History = 1,
        Cancelation = 2,
        ReactivatedByCard = 3,
        ChangePaymentToBalance = 4,
        ChangePaymentToCard = 5,
        ReactivatedByBalance = 6,
        GetCardList = 7,
        CheckCVV = 8,
        NotesList = 9,
        NotesAdd = 10,
        GetCountry = 11,
        SaveNewSubscription = 12,
        ReactivateCountrySaverByCard = 13

    }

    public class CustomerCSBNotes
    {
        public int No { get; set; }
        public string descript { get; set; }
        public DateTime create_date { get; set; }
        public string create_by { get; set; }
    }

}