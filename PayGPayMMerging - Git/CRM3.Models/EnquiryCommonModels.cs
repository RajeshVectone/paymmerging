﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace CRM_API.Models
{
    public class EnquiryCommonModels
    {
        public int Enquiry_ID { get; set; }
        public string Enquirer_Name { get; set; }
        public string Mobileno { get; set; }
        public string Existing_Network { get; set; }
        public string Country { get; set; }
        public int  Fk_Enq_type { get; set; }
        public DateTime  Enquiry_Date { get; set; }
        public string Query { get; set; }
        public string  Comments { get; set; }
        public string create_agent { get; set; }
        public string close_agent { get; set; }
        public DateTime close_date { get; set; }
        public int Fk_status_id { get; set; }
        public string  current_status { get; set; } 
        public int errcode { get; set; }
        public string errmsg { get; set; }
        public int Enquiry_Type { get; set; }
        public int fk_status_id { get; set; }
        public int fk_agent_id { get; set; }
        public int fk_close_agent { get; set; }
        public DateTime date_from { get; set; }
        public DateTime date_to { get; set; }

    }

   

}