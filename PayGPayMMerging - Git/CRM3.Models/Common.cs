﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models.Common
{
    public class ErrCodeMsg
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
        private string _errsubject = "General Error";
        public string errsubject
        {
            get
            {
                return (errcode == 0 || errcode == 1) ? "Result Information" : _errsubject;
            }
            set { _errsubject = value; }
        }
    }
    public class ErrCodeMsgExt:ErrCodeMsg
    {
        public int err_code { get; set; }
        public string err_message { get; set; }
        public string data_type { get; set; }
        public string Text { get; set; }
        public int Value { get; set; }
        public float balance_old { get; set; }
        public float afterbal { get; set; }
    }
    public class CancelLocationsNew
    {
        public string vlr_number { get; set; }
        public string imsi_active { get; set; }
        public string url { get; set; }
        public string type { get; set; }
        public string mode { get; set; }

    }
    public class Helper
    {
        public static ErrCodeMsg Coalese(IEnumerable<ErrCodeMsg> ecmArr)
        {
            var def = new Common.ErrCodeMsg()
            {
                errcode = -1,
                errmsg = "Unknown",
            };

            if (ecmArr == null)
                return def;

            if (ecmArr.Count() == 0)
                return def;

            return ecmArr.ElementAt(0);
        }
    }
    public class EnumStringValue : System.Attribute
    {
        private string _value;

        public EnumStringValue(string value)
        {
            _value = value;
        }

        public string Value
        {
            get { return _value; }
        }
    }
}
