﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models
{
    public class PAYMDetails : Common.ErrCodeMsg
    {
        public string BillPeriod { get; set; }
        public float Outstanding { get; set; }
        public float Payment { get; set; }
        public float PlanCharges { get; set; }
        public float ExcessUsage { get; set; }
        public float LLOM { get; set; }
        public float Deduction { get; set; }
        public float TotalBill { get; set; }
        public int Status { get; set; }
        public string PaymentStatus { get; set; }
        public string Fee { get; set; }
        public string Action { get; set; }
        //Added by Barani
        public string Pay_Reference { get; set; }
        public string Pay_Mode { get; set; }
        //Added by BSK
        public string Bundle { get; set; }
        public string Country_Saver { get; set; }
        //CRM Enhancement 2
        public DateTime? payment_date { get; set; }
        public string payment_date_str
        {
            get
            {
                return payment_date != null ? Convert.ToDateTime(payment_date).ToString("dd-MM-yyyy hh:mm:ss tt") : "";
            }
        }
    }

    public class PAYMMobile : Common.ErrCodeMsg
    {
        public string MobileNo { get; set; }
        // 31-Jul-2015 : Moorthy Added
        public string ProductCode { get; set; }
    }

    public class getDetailsByNo : Common.ErrCodeMsg
    {
        public string MobileNo { get; set; }
        public string Paymentmode { get; set; }
        public string CardNumber { get; set; }
        // 08-Aug-2015 : Moorthy Added
        public string ProductCode { get; set; }
    }

    public class PaymPaymentHistroy : Common.ErrCodeMsg
    {
        public string CardNumber { get; set; }
        public string PaymentProfile { get; set; }
        public string Status { get; set; }
        public string SetupDate { get; set; }
        public string LastDateofUse { get; set; }
        public string LastPaymentAmount { get; set; }
        public string Action { get; set; }
    }

    public class PAYMPaymentDeatils : Common.ErrCodeMsg
    {
        public string PaymentDate { get; set; }
        public string PaymentProfile { get; set; }
        public string PaymentAmount { get; set; }
        public string PaymentStatus { get; set; }
        //Added By Barani
        public string Pay_Reference { get; set; }
        public string Pay_Mode { get; set; }
    }

    public class PAYMCreditCard : Common.ErrCodeMsg
    {
        public string CardNumber { get; set; }
        public string ExpiryDate { get; set; }
        public string ProviderCode { get; set; }
        public string Last6DigitsCC { get; set; }
        public string expiydate { get; set; }
    }

    public class PAYMCreditCard_v1 : Common.ErrCodeMsg
    {
        //public string Last6DigitsCC { get; set; }
        public string Name { get; set; }
        public string Street { get; set; }
        public string city { get; set; }
        public string Postcode { get; set; }
        public string Status { get; set; }
        public string Expirydate { get; set; }
        public string CC_No { get; set; }
    }


    public class PAYMBankAccount : Common.ErrCodeMsg
    {
        public string BankReference { get; set; }
        public string SortCode { get; set; }
        public string AccountNumber { get; set; }
        public string AccountName { get; set; }
        public string DDStatus { get; set; }
        public string BankName { get; set; }

        //07-Feb-2017 : Moorthy : Added for VMFR (Branch Code) 
        public string BranchCode { get; set; }
        public string Country { get; set; }
    }

    public class getBankList : Common.ErrCodeMsg
    {
        public string bankcode { get; set; }
        public string bankname { get; set; }
        public string AccountLength { get; set; }
        public string Result { get; set; }
        public string TresCode { get; set; }
        public string Msg { get; set; }
    }

    public class validationBank : Common.ErrCodeMsg
    {
        public string mobileNo { get; set; }
        public string subscriberID { get; set; }
        public string bankName { get; set; }
        public string bankCode { get; set; }
        public string sortCode { get; set; }
        public string accoutNumber { get; set; }
        public string accountName { get; set; }
        //added by Barani
        public string updateby { get; set; }
        //30-Jan-2017 : Moorthy : Added for product code
        public string productCode { get; set; }

        //07-Feb-2017 : Moorthy : Added for VMFR (Branch Code) 
        public string branchCode { get; set; }
        public string country { get; set; }

        //03-Dec-2018 : Moorthy : Added for Mandate Creation in Add New Direct Debit Details screen
        public string sitecode { get; set; }
        public string pp_customer_id { get; set; }
    }

    public class AddDDSetUp : Common.ErrCodeMsg
    {
        public int subscriberid { get; set; }
        public int freesimid { get; set; }
        public string paymentref { get; set; }
        public string sort_code { get; set; }
        public string account_num { get; set; }
        public string account_name { get; set; }
        //public float amount { get; set; }
        public string amountPaidTag { get; set; }
        public string updateby { get; set; }
        public string bank_name { get; set; }

        //07-Feb-2017 : Moorthy : Added for VMFR (Branch Code) 
        public string branch_code { get; set; }
        public string country { get; set; }

        //03-Dec-2018 : Moorthy : Added for Mandate Creation in Add New Direct Debit Details screen
        public string mobileno { get; set; }
        public string sitecode { get; set; }
        public string pp_customer_id { get; set; }
    }

    //03-Dec-2018 : Moorthy : Added for Mandate Creation in Add New Direct Debit Details screen
    public class AddDDSetUpPersonalDetails
    {
        public string username { get; set; }
        public string password { get; set; }
        public string title { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string address3 { get; set; }
        public string city { get; set; }
        public string postcode { get; set; }
        public string houseno { get; set; }
        public string countryname { get; set; }
        public string email { get; set; }
        public string mobilephone { get; set; }
        public string landlinenumber { get; set; }
        public string language { get; set; }
        public string gender { get; set; }
        public DateTime createdate { get; set; }
        public DateTime birthdate { get; set; }
        public int bySMS { get; set; }
        public int byEmail { get; set; }
        public int byCall { get; set; }
        public string securityanswer { get; set; }
    }

    public class BasicDetail : Common.ErrCodeMsg
    {
        public int subscriberid { get; set; }
        public string cybersourceid { get; set; }
        public int freesimid { get; set; }
        public string branchsortcode { get; set; }
        public string accountnumber { get; set; }
        public string accountname { get; set; }
        public string amountpaidtag { get; set; }
        public string status { get; set; }
    }
}
