﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_API.Models
{
    public class LoginPage : CommonModels
    {
        public string LoginName { get; set; }
        public string Password { get; set; }
        public string Country { get; set; }
        public string ReturnUrl { get; set; }
    }

    public class UserByToken
    {
        public int user_id { get; set; }
    }

    public class RoleByUser
    {
        public string role { get; set; }
    }

    public class permission
    {
        public int permission_id { get; set; }
    }
    public class CountryByUser
    {
        public string code { get; set; }
        public string country { get; set; }
    }

    public class LoginMsg : Common.ErrCodeMsg
    {
        public string token { get; set; }
    }

    public class SessionInfo
    {
        public int userid { get; set; }
        public string user_login { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string site_code { get; set; }
        public string country { get; set; }
        //public int user_role { get; set; }
        public User_Role user_role { get; set; }
        public int roleid { get; set; }
        public string email_id { get; set; }
    }

    public enum User_Role
    {
        User = 0,
        Agent = 1,
        EsclatingTeam = 2,
        TeamLeader = 3,
        Manager = 4,
        Admin = 5
    }
}