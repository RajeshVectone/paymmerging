﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models.MNP
{
    public abstract class MnpRequest
    {
        #region ReqCode
        private string _reqCode;
        public string ReqCode
        {
            get { return _reqCode; }
            set { _reqCode = value; }
        }
        #endregion
    }
    public abstract class MnpResponse
    {
        #region RespCode
        private string _respCode;
        public string RespCode
        {
            get { return _respCode; }
            set { _respCode = value; }
        }
        #endregion
    }
    public abstract class MnpReportRequest
    {
        #region ReqCode
        private string _reqCode;
        public string ReqCode
        {
            get { return _reqCode; }
            set { _reqCode = value; }
        }
        #endregion

        #region Name
        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        #endregion

        #region Logon
        private Models.Porting.Syniverse.Logon _logon;
        public CRM_API.Models.Porting.Syniverse.Logon Logon
        {
            get { return _logon; }
            set { _logon = value; }
        }
        #endregion

        #region Cookie
        private string _cookie;
        public string Cookie
        {
            get { return _cookie; }
            set { _cookie = value; }
        }
        #endregion

        #region Logoff
        private CRM_API.Models.Porting.Syniverse.Logoff _logoff;
        public CRM_API.Models.Porting.Syniverse.Logoff LogOff
        {
            get { return _logoff; }
            set { _logoff = value; }
        }
        #endregion
    }
    public abstract class MnpReportResponse
    {
        #region RespCode
        private string _respCode;
        public string RespCode
        {
            get { return _respCode; }
            set { _respCode = value; }
        }
        #endregion
    }
}
