﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models
{
    public class TransferBalanceInput
    {
        public string sitecode { get; set; }
        public string donor_mobileno { get; set; }
        public string receiver_mobileno { get; set; }
        public string amount { get; set; }
        public string username { get; set; }
    }

    public class TransferBalanceOutput
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
    }

    public class TransferBalanceReportInput
    {
        public string sitecode { get; set; }
        public string calledby { get; set; }
        public string date_from { get; set; }
        public string date_to { get; set; }
    }

    public class TransferBalanceReportOutput
    {
        public string referenceid { get; set; }
        public DateTime? debit_date { get; set; }
        public string debit_date_string { get { return debit_date != null ? debit_date.Value.ToString("dd/MM/yyyy hh:mm:ss") : ""; } }
        public string donor_mobileno { get; set; }
        public double debit_amount { get; set; }
        public DateTime? credit_date { get; set; }
        public string credit_date_string { get { return credit_date != null ? credit_date.Value.ToString("dd/MM/yyyy hh:mm:ss") : ""; } }
        public string receiver_mobileno { get; set; }
        public double credit_amount { get; set; }
        public string process_by { get; set; }
    }
}
