﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models
{
    //AddBalance
    public class AddBalanceInput
    {
        public string sitecode { get; set; }
        public string mobileno { get; set; }
        public string amount { get; set; }
        public string username { get; set; }
        public string calledby { get; set; }
        public int balance_type { get; set; }
        public int bundleid { get; set; }
        public int daysvalid { get; set; }
    }
    public class AddBalanceLTPInput
    {
        public string sitecode { get; set; }
        public string mobileno { get; set; }
        public string paymentref { get; set; }
        public float amount { get; set; }
        public string calledby { get; set; }
        public string crm_user { get; set; }
        public string comments { get; set; }
    }
    public class AddBalancemanualInput
    {
        public string sitecode { get; set; }
        public string mobileno { get; set; }
        public string comments { get; set; }
        public string refund_by { get; set; }
        public float amount { get; set; }
        public string crm_user { get; set; } 
    }

    public class AddBalanceOutput
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
    }

    //AddBalanceReport
    public class AddBalanceReportInput
    {
        public string sitecode { get; set; }
        public string calledby { get; set; }
        public string date_from { get; set; }
        public string date_to { get; set; }
    }

    public class AddBalanceReportOutput
    {
        public string mobileno { get; set; }
        public string referenceID { get; set; }
        public string username { get; set; }
        public double amount { get; set; }
        public double prevbalance { get; set; }
        public double afterbalance { get; set; }
        public string credit_date { get; set; }
        public string ErrMsg { get; set; }
    }
}
