﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models.Complaint
{
    public class ComplaintModels : InsertComplaint
    {
        public SearchType SearchType { get; set; }
        public int Subscriberid { get; set; }
        public string Site_code { get; set; }
        public string SIM_Type { get; set; }
        public string Ticket_id { get; set; }
        public string mobileno { get; set; }
        public DateTime issue_generate_date { get; set; }
        public string issue_generate_date_string { get { return issue_generate_date.ToString("dd-MM-yyyy hh:mm:ss tt"); } }
        public string Issue_Name { get; set; }
        public string Function_Name { get; set; }
        public string agent_name { get; set; }
        public string eslated_user { get; set; }
        public string descr { get; set; }
        public string status { get; set; }
        public DateTime? issue_complete_date { get; set; }
        public string issue_complete_date_string { get { return (issue_complete_date.HasValue) ? issue_complete_date.GetValueOrDefault().ToString("dd-MM-yyyy") : ""; } }
        public string Action { get; set; }
        public string Action2 { get; set; }
        public int issue_type { get; set; }
        public int Issue_Function { get; set; }
        public int Issue_Complaint { get; set; }
        public int status_id { get; set; }
        public int esclating_user_id { get; set; }
        public int fk_priority_id { get; set; }
        public string sub_category { get; set; }
        public int complaint_id { get; set; }
        public string priority_name { get; set; }
        public string email { get; set; }
        public string date_from { get; set; }
        public string date_to { get; set; }
        public int cur_user { get; set; }
        public string aging { get; set; }
    }

    public class InsertComplaint
    {
        public string customer_name { get; set; }
        public string product_code { get; set; }
        public string mobileno { get; set; }
        public int issue_type { get; set; }
        public int Issue_Function { get; set; }
        public int Issue_Complaint { get; set; }
        public int issue_status { get; set; }
        public string descr { get; set; }
        public int esclating_user { get; set; }
        public int process_type { get; set; }
        public int agent_userid { get; set; }
        public string sitecode { get; set; }

        public string ticket_id { get; set; }
        public string sms_text { get; set; }
        public string sms_url { get; set; }
        public string sms_org_address { get; set; }

        public int errcode { get; set; }
        public string errmsg { get; set; }
    }

    public enum SearchType
    {
        ComplaintListAll = 1,
        InsertComplaint = 2
    }
}
