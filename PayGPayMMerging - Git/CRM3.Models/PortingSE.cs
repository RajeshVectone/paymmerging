﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models.Porting.SE
{
    public class PortinEntry : Common.ErrCodeMsg
    {
        public AdditionInfo InfoType { get; set; }
        public PortingAction ActionType { get; set; } 
        public string Sitecode { get; set; }
        public string MSISDN { get; set; }
        public string VectoneMSISDN { get; set; }
        public string PortingTime { get; set; }
        public string PortingHour { get; set; }
        public string DSO { get; set; }     //recipient network operator
        public string DSP { get; set; }     //recipient network provider
        public string DonorServiceProvider { get; set; }
        public string PersonalCorporateID { get; set; }
        public string SubsName { get; set; }
        public string SubsAddress { get; set; }
        public string SubsPostal { get; set; }
        public string SubsCity { get; set; }
        public string Department { get; set; }
        public string DeptPerson { get; set; }
        public string DeptPhone { get; set; }
        public string DeptFax { get; set; }
        public string DeptEmail { get; set; }
        public string PortingDateFrom { get; set; }
        public string PortingDateTo { get; set; }
        public string PortoutDateFrom { get; set; }
        public string PortoutDateTo { get; set; }
        public string RequestDateFrom { get; set; }
        public string RequestDateTo { get; set; }
        public PortingStatus PortingStatus { get; set; }
        public PortOutStatus PortoutStatus { get; set; }
    }

    public class PortingDetail : Common.ErrCodeMsg
    {
        public string PortStatus { get; set; }
        public string Description { get; set; }
        public string MSISDN { get; set; }
        public string VectoneMSISDN { get; set; }
        public string DNOName { get; set; }        
        public string DonorServiceProvider { get; set; }
        public string PersonalCorporateID { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Postal { get; set; }
        public string City { get; set; }
        public string Department { get; set; }
        public string ContactPerson { get; set; }
        public string ContactPhone{ get; set; }
        public string ContactFax { get; set; }
        public string ContactEmail { get; set; }
        public string OrderRejectCode { get; set; }
        public string OrderRejectDesc { get; set; }
        public DateTime PortingDate { get; set; }
        public DateTime DNOCutOffTime { get; set; }
        public DateTime LastUpdate { get; set; }
    }

    public class DonorSERes
    {
        public string DonorID { get; set; }
        public string DonorName { get; set; }
    }

    public class PortListResult
    {
        public string PortID { get; set; }
        public string MSISDN { get; set; }
        public string VectoneMSISDN { get; set; }
        public DateTime RequestedDate{ get; set; }
        public DateTime PortingDate { get; set; }
        public string SubscriberName { get; set; }
        public string ContactPhone { get; set; }
        public string Email { get; set; }
        public string PortinStatus { get; set; }
        public string Description { get; set; }
        public DateTime LastUpdate{ get; set; }
    }

    public class AdditionalReqInfo:Common.ErrCodeMsg
    {
        public AdditionInfo InfoType { get; set; }
        public string Sitecode { get; set; }
        public string PortID { get; set; }
    }

    public enum PortingAction
    {
        PortinRejected = -1,
        Testing = -8,
        RequestPorting = 1,
        RequestCancel = 2,
        PortinListByDate = 3,
        PortinListByMSISDN = 4,
        PortinListByRequestDate = 5,
        PortoutListByDate = 30,
        PortoutListByMSISDN = 40,

    }

    public enum PortingStatus
    { 
        All = 0,
        Open = 1,
        Completed = 2,
        AbortedRejectedCancelled = 3
    }

    public enum PortOutStatus
    {
        All = 0,
        Pending = 1,
        Open = 2,
        Completed = 3,
        AbortedRejectedCancelled = 4
    }

    public enum AdditionInfo
    {
        Portin = 0,     //because value is 0, this is not mandatory
        Portout = 1,
        ListDonor = 3,
        Detail = 4,     //details of port by port_id
        Test = 5,
        PortinSubmitRequest = 100
    }
}
