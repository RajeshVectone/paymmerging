﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace CRM_API.Models
{
    public enum GoCheapOrderType
    {
        Default = 0,
        ValidateMobileNo = 1,
        InsertNewOrder = 11,
        TopUp = 12,
        UpdateBalance = 13,
        InsertVerificationCode = 14,
        TopUpLog = 15,
        PersonalLogin = 21,
        GetPersonalInfo = 22,
        GetPersonalInfoForgotPassword = 23,
        UpdatePersonalInfo = 24,
        GetAccountInfo = 25,
        ResetPassword = 26
    }

    public class GoCheapOrder : Common.ErrCodeMsg
    {
        public GoCheapOrderType order_type { get; set; }
        public string mobileno { get; set; }
        public string telephone { get; set; }
        public string fax { get; set; }
        public int paymentid { get; set; }
        public string paymentref { get; set; }
        public int productid { get; set; }
        public float amount { get; set; }
        public string currency { get; set; }
        public string crm_login { get; set; }
        public string title { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string town { get; set; }
        public string state { get; set; }
        public string housenumber { get; set; }
        public string postcode { get; set; }
        public string countrycode { get; set; }
        public string country { get; set; }
        public string email { get; set; }
        public string contact { get; set; }
        public string order_url { get; set; } // "http://blablbaa"
        public int subscription_id { get; set; }
        public int id_type { get; set; }
        public string id_number { get; set; }
        public DateTime date_of_birth { get; set; }
        public string sitecode { get; set; } // GCM
        public string mundio_product { get; set; } // GCM
        public string order_source { get; set; } // "GCM WEB"
        public string source_reg { get; set; } // GCM UK
        public string verificationcode { get; set; }
        public int status { get; set; }
        public string password { get; set; }
        public string new_password { get; set; }
        public string languange { get; set; }
        public string gender { get; set; }
        public int verificationbysms { get; set; }
        public int verificationbyemail { get; set; }
        public string question { get; set; }
        public string answer { get; set; }
        public int subscribertype { get; set; }
        public int freesimstatus { get; set; }
        public int freesimtype { get; set; }
        public int method { get; set; }
        public DateTime startdate { get; set; }
        public DateTime enddate { get; set; }
    }

    public class GoCheapValidateResult : Common.ErrCodeMsg
    {
        public string currcode { get; set; }
    }

    public class GoCheapBalanceResult : Common.ErrCodeMsg
    {
        public string incentif_data { get; set; }
        public string total_notif { get; set; }
        public float prevbalance { get; set; }
        public float afterbalance { get; set; }
    }

    public class GoCheapLoginResult : Common.ErrCodeMsg
    {
        public string mobileno { get; set; }
        public int subscriberid { get; set; }
        public string fullname { get; set; }
        public string firstname { get; set; }
    }

    public class GoCheapPersonalInfo : Common.ErrCodeMsg
    {
        public string username { get; set; }
        public string password { get; set; }
        public string fullname { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string postcode { get; set; }
        public string houseno { get; set; }
        public string countryname { get; set; }
        public string email { get; set; }
        public string mobilephone { get; set; }
        public string landlinenumber { get; set; }
        public string language { get; set; }
        public string gender { get; set; }
        public string createdate { get; set; }
        public string birthdate { get; set; }
        public int bySMS { get; set; }
        public int byEmail { get; set; }
    }

    public class GoCheapAccountInfo : Common.ErrCodeMsg
    {
        public string mobileno { get; set; }
        public string iccid { get; set; }
        public string telcocode { get; set; }
        public string custcode { get; set; }
        public int batchcode { get; set; }
        public int serialcode { get; set; }
        public string trffclass { get; set; }
        public double balance { get; set; }
        public string currcode { get; set; }
    }

    public class GoCheapTopUpLogResult
    {
        public DateTime createdate { get; set; }
        public double amount { get; set; }
        public string paymentref { get; set; }
        public string Payment { get; set; }
    }
}
