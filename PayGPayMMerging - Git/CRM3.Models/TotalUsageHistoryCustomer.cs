﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models.TotalUsageHistory
{
    public class Customerpaymenthistory
    {
        public DateTime paydate { get; set; }
        public string paydate_String { get { return paydate.ToString("dd-MM-yyyy hh:mm:ss tt"); } set { paydate = DateTime.ParseExact(value, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture); } }
        public string cnxdate { get; set; }
        public string type { get; set; }
        public decimal totalusage { get; set; }
        public string totalusage_String { get { return decimal.Round(totalusage, 2, MidpointRounding.AwayFromZero).ToString(); } set { totalusage = decimal.Parse(value); } }
        public decimal beforebal { get; set; }
        public string beforebal_String { get { return decimal.Round(beforebal, 2, MidpointRounding.AwayFromZero).ToString(); } set { beforebal = decimal.Parse(value); } }
        public decimal afterbal { get; set; }
        public string afterbal_String { get { return decimal.Round(afterbal, 2, MidpointRounding.AwayFromZero).ToString(); } set { afterbal = decimal.Parse(value); } }
    }
    public class CustomerTotalUsageHistoryRequest
    {
        public string Msisdn { get; set; }
        public string Sitecode { get; set; }
        public Action RequestType { get; set; }
        public string date_from { get; set; }
        public string date_to { get; set; }

        public Action type { get; set; }
        public DateTime Date_From1 { get; set; }
        public string Date_From_date_string { get { return Date_From1.ToString("yyyy-MM-dd"); } }
        public string Date_from_month_string { get { return Date_From1.ToString("MM"); } }
        public string Date_from_day_string { get { return Date_From1.ToString("dd"); } }
        public string Date_from_year_string { get { return Date_From1.ToString("yyyy"); } }
        public DateTime Date_To1 { get; set; }
        public string Date_To_date_string { get { return Date_To1.ToString("yyyy-MM-dd"); } }
        public string Date_to_month_string { get { return Date_To1.ToString("MM"); } }
        public string Date_To_day_string { get { return Date_To1.ToString("dd"); } }
        public string Date_To_year_string { get { return Date_To1.ToString("yyyy"); } }

        //28-Dec-2018 : Moorthy : Added for Bundle breakdown
        public int search_type { get; set; }
        public string PackageID { get; set; }
        public string BundleName { get; set; }
    }

    public enum Action
    {
        History = 1,
        //28-Dec-2018 : Moorthy : Added for Bundle breakdown
        GetSubscribedBundlePackagesCDR = 2,
        GetCallHistorySearchCBS = 3
    }

    //28-Dec-2018 : Moorthy : Added for Bundle breakdown
    public class GetSubscribedBundlePackagesCDRRef
    {
        public int package_id { get; set; }
        public string name { get; set; }
        public string tariffClass { get; set; }
    }

    public class GetCallHistorySearchCBSRef : CRM_API.Models.Common.ErrCodeMsg
    {
        public string History_Type { get; set; }
        public DateTime? Date_Time { get; set; }
        public string CLI { get; set; }
        public string Destination_Number { get; set; }
        public string Type { get; set; }
        public string Destination_Code { get; set; }
        public string Duration_Min { get; set; }
        public decimal? Data_Usage { get; set; }
        public string Tariff_Class { get; set; }
        public int? Roaming_Zone { get; set; }
        public string Package_Name { get; set; }
        public string Net_Charges { get; set; }
        public decimal? Balance { get; set; }
        public string custcode { get; set; }
        public int? batchcode { get; set; }
        public int? serialcode { get; set; }
        public string roaming_status { get; set; }
    }
}
