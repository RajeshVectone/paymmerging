﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models
{
    public class ProductModel:Common.ErrCodeMsg
    {
        public string mundio_product { get; set; }
        public string sitecode { get; set; }
        public string product_name { get; set; }
        public int is_active { get; set; }
        public bool is_active_bool
        {
            get
            {
                if (is_active == 1)
                    return true;
                else
                    return false;
            }
            private set
            {
                is_active = value == true ? 1 : 0;            
            }
        }
    }
}
