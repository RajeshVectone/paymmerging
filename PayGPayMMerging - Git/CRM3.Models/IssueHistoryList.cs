﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models
{
    //Issue History Report
    public class IssueHistoryListInput
    {
        public string sitecode { get; set; }
        public string date_from { get; set; }
        public string date_to { get; set; }
    }
    public class SMSIssueHistoryListInput
    {
        public string sitecode { get; set; }
        public string date_from { get; set; }
        public string date_to { get; set; }
    }
    public class SMSIssueHistoryListOutput
    {
        public string msisdn { get; set; }
        public string customer_name { get; set; }
        public string agent_name { get; set; }
        public DateTime? sms_date { get; set; }
        //public string sms_date_String { get { return sms_date != null ? sms_date.Value.ToString("dd/MM/yyyy hh:mm:ss") : ""; } }
        public string sms_date_String { get { return sms_date != null ? sms_date.Value.ToString("dd/MM/yyyy") : ""; } }
        public string time { get; set; }
        public string sms_type { get; set; }
        public string sms_text { get; set; }
    }

    public class IssueHistoryListOutput
    {
        public DateTime? submitdate { get; set; }
        public string submitdate_String { get { return submitdate != null ? submitdate.Value.ToString("dd/MM/yyyy hh:mm:ss") : ""; } }
        public string contact_type { get; set; }
        public string problemdesc { get; set; }
        public string typedesc { get; set; }
        public string Submitby { get; set; }

        public string Category { get; set; }
        public string Sub_category { get; set; }
        public string Status { get; set; }
        public string Interval { get; set; }

        public string Customer_Name { get; set; }
        public string Subscriber_ID { get; set; }
        public string Mobile_number { get; set; }

        public string Call_Type { get; set; }
        public string esclated_to { get; set; }

        public string assigned_to { get; set; }
        public string resolved_by { get; set; }
        public DateTime? resolved_date { get; set; }
        public string resolved_date_String { get { return resolved_date != null ? resolved_date.Value.ToString("dd/MM/yyyy hh:mm:ss") : ""; } }
    }
}
