﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models.ChargebackSuspension
{
    public class ChargebackCountInput
    { 
    
    }

    public class ChargebackSuspensionInput
    {
        public ChargebackSuspensionMode Mode_Type { get; set; }

        public string product_code { get; set; }
        public string sitecode { get; set; }
        public string pay_reference { get; set; }
        public DateTime transaction_date { get; set; }
        public string mobileno { get; set; }
        public string reason { get; set; }
        public DateTime chargeback_claim_date { get; set; }
        public string entered_by { get; set; }
        public int proces_type { get; set; }
        public int req_type { get; set; }
        public double trans_amount { get; set; }
        public string customer_name { get; set; }
        public DateTime date_from { get; set; }
        public DateTime date_to { get; set; }
        public int search_flag { get; set; }

        public int userid { get; set; }
        public int role_id { get; set; }
        public int status { get; set; }

        public string approved_user { get; set; }

        //06-Mar-2019 : Moorthy : Added for Chargeback Fee
        public double chargeback_fee { get; set; }

        public string firstname { get; set; }
        public string email { get; set; }
    }

    public enum ChargebackSuspensionMode
    {
        DoInsertChargebackTransaction = 1,
        GetRequestedChargeback = 2,
        DoApproveRequestedChargeback = 3,
        DoUnblockRequest = 4,
        GetChargebackByAccountId = 5,
        GetChargebackCollectionPayment = 6,
        GetRequestedChargebackCount = 7,
        SendMail = 8
    }

    public class SuspensionCommon
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
    }

    public class RequestedChargebackOutput
    {
        public string sitecode { get; set; }
        public string product_code { get; set; }
        public string pay_reference { get; set; }
        public DateTime transaction_date { get; set; }
        public string transaction_date_string { get { return transaction_date.ToString("yyyy-MM-dd"); } }
        public DateTime chargeback_claim_date { get; set; }
        public string chargeback_claim_date_string { get { return chargeback_claim_date.ToString("yyyy-MM-dd"); } }
        public DateTime block_date { get; set; }
        public string block_date_string { get { return block_date.ToString("yyyy-MM-dd"); } }
        public string mobileno { get; set; }
        public string reason { get; set; }
        public int req_type { get; set; }
        public string req_type_name { get; set; }
        public double? trans_amount { get; set; }
        public string customer_name { get; set; }
        public string approved_by { get; set; }
        public DateTime approved_date { get; set; }
        public string approved_date_string
        {
            get
            {
                if (String.IsNullOrEmpty(approved_by))
                    return "";
                else
                    return approved_date.ToString("yyyy-MM-dd");
            }
        }
        public string entered_by { get; set; }
        public int status { get; set; }
        //06-Mar-2019 : Moorthy : Added for Chargeback Fee
        public double chargeback_fee { get; set; }
    }

    public class GetChargebackCollectionPaymentOutput
    {
        public string paid_status { get; set; }
        public double paid_amount { get; set; }
        public string paid_reference { get; set; }
        public DateTime paid_date { get; set; }
        public string paid_date_string
        {
            get
            { return paid_date.ToString("yyyy-MM-dd"); }
        }
    }

    public class ChargebackCountOutput
    {
        public int suspend_count { get; set; }
        public int activate_count { get; set; }
    }
}
