﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRM_API.Models.Common;

namespace CRM_API.Models.LOTG
{
    public enum LOTG_Action
    {
        List = 1,
        ViewDetail = 2,
        ChangePayment = 3,
        ManualRenew = 5,
        StopContract = 8,
        BundleDetail = 9, //Added on 17 Jan 2015 - to get Product -> BundleDetail
        LLOMDetail = 10, //Added on 19 Jan 2015 - to get Product -> LLOMDetail
        CountrySaverDetail = 11, //Added on 19 Jan 2015 - to get Product -> CountrySaverDetail
        BundleSubscriptionHistory = 12,//Added on 19 Jan 2015 - to get BundleDetail -> SubscriptionHistory
        LLOMSubscriptionHistory = 13,//Added on 19 Jan 2015 - to get LLOMDetail -> SubscriptionHistory
        CountrySaverSubscriptionHistory = 14,//Added on 19 Jan 2015 - to get CountrySaverDetail -> SubscriptionHistory
        BundleConfirmCancel = 15,//Added on 19 Jan 2015 - to get BundleDetail -> Cancel
        LLOMConfirmCancel = 16,//Added on 19 Jan 2015 - to get LLOMDetail -> Cancel
        CountrySaverConfirmCancel = 17//Added on 19 Jan 2015 - to get CountrySaver -> Cancel
    }
    public enum LOTG_Status
    {
        Active = 1,
        Stop = 2,
        Suspend = 3,
        Failed = 4,
        Expired = -1       
    }
    public enum LOTG_Paymode
    {
        [EnumStringValue("Balance")]
        Balance = 1,
        [EnumStringValue("Credit Card")]
        CreditCard = 2
    }
    public class LLOTG : ErrCodeMsg
    {

        public string Reg_Number { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public int svc_id { get; set; }
        public string PackageID { get; set; }
        public string Plan { get; set; }
        public string TotalAmount { get; set; }
        public string SubscriptionDate { get; set; }
        public string RenewalDate { get; set; }
        public string Status { get; set; }
        public LLOTG_Paymode Payment_Type { get; set; }
        public double Plan_amount { get; set; }
        public string PaymentType_Description
        {
            get
            {
                return Enum.GetName(typeof(LLOTG_Paymode), Payment_Type);
            }
        }

        public bool DisplayCancel
        {
            get
            {
                if (Status == "Active")
                    return true;
                else
                    return false;
            }
        }

        public bool DisplayChangepayment
        {
            get
            {
                if (Status == "Active")
                    return true;
                else
                    return false;
            }
        }

        public bool DisplayReactivate
        {
            get
            {
                if (Status == "Cancelled" || Status == "Suspended" || Status == "Rejected")
                    return true;
                else
                    return false;
            }
        }
        public bool Displaynotactive
        {
            get
            {
                if (Status == "Not Active")
                    return true;
                else
                    return false;
            }
        }
        public bool DisplaySuspended
        {
            get
            {
                if (Status == "Suspended")
                    return true;
                else
                    return false;
            }
        }
    }
    public enum LLOTG_Paymode
    {
        [EnumStringValue("Pay By Balance")]
        Balance = 1,
        [EnumStringValue("Pay By Credit")]
        CreditCard = 2
    }

    public class Collection : Common.ErrCodeMsg
    {
        public string Reg_Number { get; set; }
        public string Country { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime ExpiredDate { get; set; }
        /* 1= Active , 2=Stop ,3=Suspend, 4= Renew Failed*/
        private LOTG_Status _status;
        public LOTG_Status Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
                if (ExpiredDate < DateTime.Now && _status == LOTG_Status.Active )
                    _status = LOTG_Status.Expired;
            }
        }
        /* 1 = Balance, 2=CreditCard*/
        public string Status_Description
        {
            get
            {
                return Enum.GetName(typeof(LOTG_Status), this.Status);
            }
        }
        public LOTG_Paymode Payment_Type { get; set; }
        public string PaymentType_Description
        {
            get
            {
                return Enum.GetName(typeof(LOTG_Paymode), Payment_Type);
            }
        }
        public bool Renewable
        {
            get
            {
                if (Status == LOTG_Status.Failed || Status == LOTG_Status.Expired)
                    return true;
                else
                    return false;
            }
        }
        public bool Cancelable
        {
            get
            {
                if (Status == LOTG_Status.Stop || Status == LOTG_Status.Expired || Status == LOTG_Status.Failed)
                    return false;
                else
                    return true;
            }
        }
        public bool ChangePayment { 
            get
            {
                if (Status == LOTG_Status.Stop)
                    return false;
                else
                    return true;
            }
        }

        //add by mamin @24-01-2014

        private string Currency { get; set; }
        public string City { get; set; }
        private Decimal Amount { get; set; }
        public string TotalAmount 
        {
            get {
                return Currency + ' ' + Amount;
            }
        }
        public string Destination { get; set; }
        public string LastAction { get; set; }
        public string BundlePlan { get; set; }
        public string PackageID { get; set; }
        public DateTime LastRenewedDate { get; set; }
        public DateTime RenewalDate { get; set; }
        //end by mamin @24-01-2014
    }
    public class DetailLOTG : Common.ErrCodeMsg
    {
        public string Reg_Number { get; set; }
        public string Svc_Id { get; set; }
        public int Account_Type { get; set; }
        public string Account_Info { get; set; }
        public string Custcode { get; set; }
        public string Batchcode { get; set; }
        public string Serialcode { get; set; }
        public DateTime JoinDate { get; set; }
        public DateTime StopDate { get; set; }
        public DateTime LastUpdate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime LastPay { get; set; }
        public DateTime Due_Pay { get; set; }
        public string UpdateBy { get; set; }
        public string Country { get; set; }
        public DateTime ExpiredDate { get; set; }
        /* 1= Active , 2=Stop ,3=Suspend, 4= Renew Failed*/
        private LOTG_Status _status;
        public LOTG_Status Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
                if (ExpiredDate < DateTime.Now && _status == LOTG_Status.Active)
                    _status = LOTG_Status.Expired;
            }
        }
        /* 1 = Balance, 2=CreditCard */
        public string Status_Description
        {
            get
            {
                return Enum.GetName(typeof(LOTG_Status), this.Status);
            }
        }
        public LOTG_Paymode Paymode { get; set; }
        public string PaymentType_Description
        {
            get
            {
                return Enum.GetName(typeof(LOTG_Paymode), Paymode);
            }
        }
        public bool Renewable
        {
            get
            {
                if (Status == LOTG_Status.Failed || Status == LOTG_Status.Expired)
                    return true;
                else
                    return false;
            }
        }
        public bool Cancelable
        {
            get
            {
                if (Status == LOTG_Status.Stop || Status == LOTG_Status.Expired || Status == LOTG_Status.Failed)
                    return false;
                else
                    return true;
            }
        }
        public bool ChangePayment
        {
            get
            {
                if (Status == LOTG_Status.Stop)
                    return false;
                else
                    return true;
            }
        }
    }
    public class Request : Collection
    {
        public int Id { get; set; }
        public string MobileNo { get; set; }
        public LOTG_Action Action { get; set; }
        public string Sitecode { get; set; }
        public string LoggedUser { get; set; }
        //Add by BSK for CRMT-221 reqd.
        public string AccountNo { get; set; }
        public string Pack_Dest { get; set; }
        public string Reg_Number { get; set; }
    }

    public class ViewSubscriptionHistory
    {
        public string Action { get; set; }
        public DateTime Date { get; set; }
        public string Status { get; set; }
        public string Mode { get; set; }

        /* 
         public string Date { get; set; }
         public string Payment_Type { get; set; }
         public string Amount { get; set; }
         public string Payment_Method { get; set; }
         public string Payment_Status { get; set; }
         */
    }

    //added by Hari - 17-01-2015
    public class LLOMModel : Collection
    {        
        public string Status { get; set; }
        public DateTime SubscriptionDate { get; set; }
        public int Svc_Id { get; set; }

        public bool DisplayCancel
        {
            get
            {
                if (Status == "Active")
                    return true;
                else
                    return false;
            }
        }

        public bool DisplayReactivate
        {
            get
            {
                //changed for testing 
                if (Status == "Cancelled" || Status == "Suspended")
                //if (Status == "Cancelled" || Status == "Active" || Status == "Cancelling")
                    return true;
                else
                    return false;
            }
        }

        public string TotalAmount { get; set; }
    }
    //end added

    //added by Hari - 19-01-2015
    public class LLOMSubscriptionHistoryModel : Common.ErrCodeMsg
    {
        public int BundleId { get; set; }
        public string CLI { get; set; }
        public DateTime SubscriptionDate { get; set; }
        public DateTime RenewalDate { get; set; }
        public string RenewalStatus { get; set; }
        
        /*Added on 09-02-2015 - by Hari - for Subscription History LLOM Change*/
        public String Action { get; set; }
        public DateTime Date { get; set; }
        public String Status { get; set; }
        public String Mode { get; set; }
        /*End added*/
    }

    public class CountrySaverSubscriptionHistoryModel : Common.ErrCodeMsg
    {
        public DateTime Date { get; set; }
        public string Action { get; set; }
        public string BundleDetails { get; set; }
        public string Amount { get; set; }
        public string PaymentMode { get; set; }
        public string PaymentStatus { get; set; }
    }

    public class Response : Request { }
}
