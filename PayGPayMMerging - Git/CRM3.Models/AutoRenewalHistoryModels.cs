﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models.AutoRenewalHistory
{
    public class AutoRenewalHistoryModels : Output
    {
        public string date_from { get; set; }
        public string date_to { get; set; }
        public string msisdn { get; set; }
        public string sitecode { get; set; }
    }
    public class Output
    {
        public string mobileno { get; set; }
        public string iccid { get; set; }
        public int bundleid { get; set; }
        public string bundle_name { get; set; }
        public DateTime create_date { get; set; }
        public string create_date_string { get { return create_date.ToString("dd-MM-yyyy hh:mm:ss tt"); } }
        public string create_by { get; set; }
        public string comments { get; set; }
        public string process_name { get; set; }

        public int errcode { get; set; }
        public string errmsg { get; set; }
    }
}
