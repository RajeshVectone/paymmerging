﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_API.Models
{
    public class MvnoAccount:Common.ErrCodeMsg
    {
        public string mobileno { get; set; }
        public string custcode { get; set; }
        public int batchcode { get; set; }
        public int serialcode { get; set; }
        public string iccid { get; set; }
        public string imei { get; set; }
        public string tariffclass { get; set; }
        public int card_id { get; set; }
        public string cardname { get; set; }
        public DateTime? expdate { get; set; }
    }
    public class ESPAccount : Common.ErrCodeMsg
    {
        public string TelcoCode { get; set; }
        public string CustomerCode { get; set; }
        public int BatchCode { get; set; }
        public int SerialCode { get; set; }
        public int Status { get; set; }
        public string StatusName { get; set; }
        public string PinCode { get; set; }
        public string LangCode { get; set; }
        public int AccType { get; set; }
        public string PhoneNB { get; set; }
        public double Balance { get; set; }
        public double TotalCons { get; set; }
        public string CurrencyCode { get; set; }
        //public float FBalance { get { return _fBalance; } set { _fBalance = value; } }
        public double DBalance { get; set; }
        public string TariffClass { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string CurrCode { get; set; }
        public int MaxAccess { get; set; }
        //public int FirstUsgDD { get { return _ifirstusgdd; } set { _ifirstusgdd = value; } }
        public string DID { get; set; }
        public DateTime FirstUsg { get; set; }
        public int CnxCount { get; set; }
        public string ICCID { get; set; }
        public bool IsPorted { get; set; }
        private string _sbatchno;
        public string BatchNo
        {
            get
            {
                return String.Format("{0}{1}{2}",
                    CustomerCode.Substring(4),
                    BatchCode.ToString().PadLeft(4, '0'),
                    SerialCode.ToString().PadLeft(4, '0'));
            }
            set { _sbatchno = value; }
        }
        private double _dbalancemoney;
        public double BalanceMoney
        {
            get
            {
                if (Balance != 0)
                    return Balance / 100;
                else
                    return 0;
            }
            set { _dbalancemoney = value; }
        }

        public string SIMType { get; set; }
        public int SIMTypeID { get; set; }

        //Added : 22-Dec-2018 : CRM Enhancement
        public double bonus_balance { get; set; }
    }
}
