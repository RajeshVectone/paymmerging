﻿using CRM_API.Models.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models
{
    public class LLOTGCountryModel
    {
        public string country { get; set; }

        public string city { get; set; }

        public string searchby { get; set; }

        public string searchvalue { get; set; }
    }
    public class LLOTGProductModel
    {
        public string product { get; set; }

        public string searchby { get; set; }

        public string searchvalue { get; set; }
    }
    /// <summary>
    /// LLOTG entity
    /// </summary>
    public class LLOTG : ErrCodeMsg
    {

        public string Reg_Number { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public int svc_id { get; set; }
        public string PackageID { get; set; }
        public string Plan { get; set; }
        public string TotalAmount { get; set; }
        public string SubscriptionDate { get; set; }
        public string  RenewalDate { get; set; }
        public string Status { get; set; }
        public LLOTG_Paymode Payment_Type { get; set; }
        public double Plan_amount { get; set; }
        public string PaymentType_Description
        {
            get
            {
                return Enum.GetName(typeof(LLOTG_Paymode), Payment_Type);
            }
        }

        public bool DisplayCancel
        {
            get
            {
                if (Status == "Active")
                    return true;
                else
                    return false;
            }
        }
        public bool DisplayChangepayment
        {
            get
            {
                if (Status == "Active")
                    return true;
                else
                    return false;
            }
        }
        public bool DisplayReactivate
        {
            get
            { 
                if (Status == "Cancelled")
                    return true;
                else
                    return false;
            }
        }
        public bool Displaynotactive
        {
            get
            {
                if (Status == "Not Active")
                    return true;
                else
                    return false;
            }
        }
        public bool DisplaySuspended
        {
            get
            {
                if (Status == "Suspended")
                    return true;
                else
                    return false;
            }
        }

        public bool DisplayRejected
        {
            get
            {
                if (Status == "Rejected")
                    return true;
                else
                    return false;
            }
        }
    /****** Old code******/

        //public string Reg_Number { get; set; }
        //public string Country { get; set; }
        //public DateTime Subscription_Date { get; set; }
        //public DateTime Next_Renewal_Date { get; set; }
        //public DateTime ExpiredDate { get; set; }
        ///* 1= Active , 2=Stop ,3=Suspend, 4= Renew Failed*/
        //private LLOTG_Status _status;
        //public LLOTG_Status Status
        //{
        //    get;
        //    set;
        //    //get
        //    //{
        //    //    return _status;
        //    //}
        //    //set
        //    //{
        //    //    _status = value;
        //    //    if (ExpiredDate < DateTime.Now && _status == LLOTG_Status.Active)
        //    //        _status = LLOTG_Status.InActive;
        //    //}
        //}
        ///* 1 = Balance, 2=CreditCard*/
        //public string Status_Description
        //{
        //    get
        //    {
        //        return Enum.GetName(typeof(LLOTG_Status), this.Status);
        //    }
        //}
        //public string Payment_Type { get; set; }
        ////public string PaymentType_Description
        ////{
        ////    get
        ////    {
        ////        return Enum.GetName(typeof(LLOTG_Paymode), Payment_Type);
        ////    }
        ////}
        //public bool Renewable
        //{
        //    get;
        //    set;
        //}
        //public bool Cancelable
        //{
        //    get;
        //    set;
        //}
        //public bool ChangePayment
        //{
        //    get
        //    {
        //        if (Status == LLOTG_Status.InActive)
        //            return false;
        //        else
        //            return true;
        //    }
        //}

        ////add by mamin @24-01-2014

        //private string Currency { get; set; }
        //public string City { get; set; }
        //public string Plan { get; set; }
        //private Decimal Amount { get; set; }

        //public string TotalAmount
        //{
        //    get;
        //    set;

        //}
        //public string Destination { get; set; }
        //public string LastAction { get; set; }
        //public string BundlePlan { get; set; }
        //public string PackageID { get; set; }
        //public DateTime LastRenewedDate { get; set; }
        //public DateTime RenewalDate { get; set; }
        ////end by mamin @24-01-2014
        
        /****** End*********/
    }
    public class PlanPackages
    {
        public string Price { get; set; }
        public string Plan_Desc { get; set; }
        public string Plan_Descsymbol { get; set; }
        public string svc_id { get; set; }
        public string currency { get; set; }

    }
    public class ViewSubscriptionHistory
    {
        public string Action { get; set; }
        public DateTime Date { get; set; }
        public string Status { get; set; }
        public string Mode { get; set; }
        
       /* 
        public string Date { get; set; }
        public string Payment_Type { get; set; }
        public string Amount { get; set; }
        public string Payment_Method { get; set; }
        public string Payment_Status { get; set; }
        */
    }
    /// <summary>
    /// LLOTG request
    /// </summary>
    public class Request : LLOTG
    {
        public int Id { get; set; }
        public string MobileNo { get; set; }
        public string Reg_Number { get; set; }
        public LLOTG_Action Action { get; set; }
        public string Sitecode { get; set; }
        public string LoggedUser { get; set; }
        public string country { get; set; }
        public string paytype { get; set; }
        public int mode { get; set; }

    }
    public class GetBalanceAndFreeMins
    {
        public string Balance { get; set; }
        public string Freemins { get; set; }
        public string Currency { get; set; }
    }
    public class Response : Request { }
    public class NewSubscription
    {
        private string _CELL_NO_LIST_MAP_NO;
        private string _CELL_NO_LIST_NO;
        private string _MAIL_SERVICE_PROVIDER;
        private string _MAP_NO_HLR;
        private string _MAP_NO_OPERATOR_CODE;
        private string _OPERATOR_DESC;
        private string _OPERATOR_NAME;
        private string _REG_SWITCH_NO;

        public string CELL_NO_LIST_MAP_NO
        {
            get { return _CELL_NO_LIST_MAP_NO; }
            set { _CELL_NO_LIST_MAP_NO = value; }
        }

        public string CELL_NO_LIST_NO
        {
            get { return _CELL_NO_LIST_NO; }
            set { _CELL_NO_LIST_NO = value; }
        }

        public string MAIL_SERVICE_PROVIDER
        {
            get { return _MAIL_SERVICE_PROVIDER; }
            set { _MAIL_SERVICE_PROVIDER = value; }
        }

        public string MAP_NO_HLR
        {
            get { return _MAP_NO_HLR; }
            set { _MAP_NO_HLR = value; }
        }

        public string MAP_NO_OPERATOR_CODE
        {
            get { return _MAP_NO_OPERATOR_CODE; }
            set { _MAP_NO_OPERATOR_CODE = value; }
        }

        public string OPERATOR_DESC
        {
            get { return _OPERATOR_DESC; }
            set { _OPERATOR_DESC = value; }
        }

        public string OPERATOR_NAME
        {
            get { return _OPERATOR_NAME; }
            set { _OPERATOR_NAME = value; }
        }

        public string REG_SWITCH_NO
        {
            get { return _REG_SWITCH_NO; }
            set { _REG_SWITCH_NO = value; }
        }
    }
    /// <summary>
    /// LLOTG Action
    /// </summary>
    public enum LLOTG_Action
    {
        List = 1,
        ViewSubscriptionHistory = 2,
        CancelSubscription = 3,
        getSavedCC = 4,
        PayByBalance = 5,
        GetUserBalance = 6,
        Reactivate = 7,
        PayByExisting = 8,

        PayByNewCard = 9,
        ChangePayMode = 10,
        SuspendedtoCancelled=11,

        CountrySaverReactiveExsitCard=12

    }
    public enum LLOTG_Status
    {
        Active = 1,
        Cancelled = 2,
        Suspended = 3,
        Cancelling = 4,
        InActive = 5
    }
    public enum LLOTG_Paymode
    {
        [EnumStringValue("Pay By Balance")]
        Balance = 1,
        [EnumStringValue("Pay By Credit")]
        CreditCard = 2
    }
    public class FamilyNumber
    {
        public string Country { get; set; }

        public string FirstName { get; set; }

        public int Index { get; set; }
        public string LastName { get; set; }

        public string Name { get; set; }

        public string Number { get; set; }
        public string PostCode { get; set; }
        public string Street { get; set; }
        public string Town { get; set; }
    }
    public class BundleModel
    {
        public string name { get; set; }
        public string minutes { get; set; }
        public string mobileno { get; set; }
        public string balance { get; set; }
        public string price { get; set; }
        public string currency { get; set; }
    }
    public class SubscriberPersonalDetail
    {
        public String Account_No { get; set; }

        public int SubscriberID { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string EMail { get; set; }

        public DateTime DOB { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public string PostCode { get; set; }

        public string Fax { get; set; }
    }
    public class ErrMsg
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public int ErrorCode { get; set; }
        public string Message { get; set; }
        public string ErrorMessage { get; set; }
    }
    public class SubscriberPersonalDetailViewModel
    {
        [Display(Name = "Account Number")]
        public String Account_No { get; set; }

        [Display(Name = "Subscriber ID")]
        public int SubscriberID { get; set; }

        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "Email")]
        public string EMail { get; set; }

        [Display(Name = "DOB")]
        public DateTime DOB { get; set; }

        [Display(Name = "Address")]
        public string Address { get; set; }

        [Display(Name = "City")]
        public string City { get; set; }

        [Display(Name = "Country")]
        public string Country { get; set; }

        [Display(Name = "Post Code")]
        public string PostCode { get; set; }

        [Display(Name = "Fax")]
        public string Fax { get; set; }
    }
    public class CliNumber
    {
        public string Number { get; set; }
        public int Type { get; set; }

        public bool Equals(CliNumber other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(other.Number, Number) && other.Type == Type;
        }
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(CliNumber)) return false;
            return Equals((CliNumber)obj);
        }
        public override int GetHashCode()
        {
            unchecked
            {
                return ((Number != null ? Number.GetHashCode() : 0) * 397) ^ Type;
            }
        }
    }
    public class FamilySaverRegistration
    {
        public double Amount { get; set; }

        public FamilyNumber FamilyNumber { get; set; }

        public int HistoryId { get; set; }

        public string Login { get; set; }
        public CliNumber Main { get; set; }
        public CliNumber Number1 { get; set; }
        public CliNumber Number2 { get; set; }
        public CliNumber Number3 { get; set; }
        public int PayMode { get; set; }

        public string PayRef { get; set; }

        public string ProductId { get; set; }

        public string SourceReg { get; set; }

        public int Status { get; set; }
    }
    public class CTPHomeSaverPostModel : ErrCodeMsg
    {
        public int Id { get; set; }
        public string SiteCode { get; set; } // IT1- UK , US1 - America
        public string ApplicationCode { get; set; } // CTP-UK

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Prefix { get; set; }
        public string LocalNumber { get; set; }
        public string Password { get; set; }
        public bool IsAgree { get; set; }
        public string DestinationNumber { get; set; }
        public string MainNumber { get; set; }
        public string AdditionalNumber1 { get; set; }
        public string AdditionalNumber2 { get; set; }
        public string AdditionalNumber3 { get; set; }
        public string DestinationFirstName { get; set; }
        public string DestinationLastName { get; set; }
        public string DestinationPostCode { get; set; }
        public string DestinationStreet { get; set; }
        public string DestinationTown { get; set; }
        public string PromoCode { get; set; }
        public string CardFirstName { get; set; }
        public string CardLastName { get; set; }
        public string CardNumber { get; set; }
        public string CardType { get; set; }
        public string CardExpireMonth { get; set; }
        public string CardExpireYear { get; set; }
        public string CardCVV { get; set; }
        public string CardPhone { get; set; }
        public string CardCountry { get; set; }
        public string CardBillAddress { get; set; }
        public string CardCity { get; set; }
        public string CardPostCode { get; set; }
        public double CostAdditionalNumber { get; set; }
        public double CostTotal { get; set; }
        public double CostDiscount { get; set; }
        public string AccountId { get; set; }
        public string Username { get; set; }
        public string Currency { get; set; }
        public int HistoryId { get; set; }
        public string ProductId { get; set; }
        public string SourceReg { get; set; }
        public string referenceCode { get; set; }
        public string PaRes { get; set; }
        public bool Bypass3Ds { get; set; }
        public string state { get; set; }
        public string IpAdd { get; set; }
        public string CLI { get; set; }
        public string SubscriberId { get; set; }
    }
    public class CTPHomeSaverResultViewModel 
    {
      
        public int Id { get; set; }
        public string Country { get; set; }
        public string DestinationNumber { get; set; }
        public string MainNumber { get; set; }
        public string AdditionalNumber1 { get; set; }
        public string AdditionalNumber2 { get; set; }
        public string AdditionalNumber3 { get; set; }
        public int RemainingMinutes { get; set; }
        public double AmountCredit { get; set; }
        public double CurrentBalance { get; set; }
        public double CostHomeSaver { get; set; }
        public double CostAdditionalNumber { get; set; }
        public double CostDiscount { get; set; }
        public double CostTotal { get; set; }
        public string PayReference { get; set; }
        public string MerchantRefCode { get; set; }
        public string ReasonCode { get; set; }
        public string Description { get; set; }
        public string Decision { get; set; }
        public string AcsURL { get; set; }
        public string PaReq { get; set; }
        public string SubscriptionId { get; set; }
        public DateTime ActivationDate { get; set; }
        public DateTime EndDate { get; set; }
        public double PreviousBalance { get; set; }
        public int errcode { get; set; }
        public string errmsg { get; set; }
        private string _errsubject = "General Error";
        public string errsubject
        {
            get
            {
                return (errcode == 0 || errcode == 1) ? "Result Information" : _errsubject;
            }
            set { _errsubject = value; }
        }
    }

    //public class CTPHomeSaverResultViewModelCountrySaver
    //{

    //    public int Id { get; set; }
    //    public string Country { get; set; }
    //    public string DestinationNumber { get; set; }
    //    public string MainNumber { get; set; }
    //    public string AdditionalNumber1 { get; set; }
    //    public string AdditionalNumber2 { get; set; }
    //    public string AdditionalNumber3 { get; set; }
    //    public int RemainingMinutes { get; set; }
    //    public double AmountCredit { get; set; }
    //    public double CurrentBalance { get; set; }
    //    public double CostHomeSaver { get; set; }
    //    public double CostAdditionalNumber { get; set; }
    //    public double CostDiscount { get; set; }
    //    public double CostTotal { get; set; }
    //    public string PayReference { get; set; }
    //    public string MerchantRefCode { get; set; }
    //    public string ReasonCode { get; set; }
    //    public string Description { get; set; }
    //    public string Decision { get; set; }
    //    public string AcsURL { get; set; }
    //    public string PaReq { get; set; }
    //    public string SubscriptionId { get; set; }
    //    public DateTime ActivationDate { get; set; }
    //    public DateTime EndDate { get; set; }
    //    public double PreviousBalance { get; set; }
    //    //public int errcode { get; set; }
    //    //public string errmsg { get; set; }
    //    //private string _errsubject = "General Error";
    //    //public string errsubject
    //    //{
    //    //    get
    //    //    {
    //    //        return (errcode == 0 || errcode == 1) ? "Result Information" : _errsubject;
    //    //    }
    //    //    set { _errsubject = value; }
    //    //}
    //}

    public class errmessage
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
        private string _errsubject = "General Error";    
        public string errsubject
        {
            get
            {
                return (errcode == 0 || errcode == 1) ? "Result Information" : _errsubject;
            }
            set { _errsubject = value; }
        }
    }
    public enum PaymentAgent
    {
        [EnumStringValue("CTP")]
        CTP = 0,
        [EnumStringValue("CTPFR")]
        CTPFR = 6,
        [EnumStringValue("CTPUS")]
        CTPUS = 7,
        [EnumStringValue("DMSE")]
        DMSE = 1,
        [EnumStringValue("DMUK")]
        DMUK = 5,
        [EnumStringValue("IVR")]
        IVR = 2,
        [EnumStringValue("VHUK")]
        VHUK = 3,
        [EnumStringValue("VMUK")]
        VMUK = 4,
        [EnumStringValue("VMNL")]
        VMNL = 8
    }
    public class GetProductAndCCVValues
    {
        private string _accountNumberGlobalTraffic;

        public string AccountNumberGlobalTraffic
        {
            get { return _accountNumberGlobalTraffic; }
            set { _accountNumberGlobalTraffic = value; }
        }
        private string _idx;

        public string Idx
        {
            get { return _idx; }
            set { _idx = value; }
        }
        private string _ccNo;

        public string CcNo
        {
            get { return _ccNo; }
            set { _ccNo = value; }
        }
        private string _mainMsisdn;

        public string MainMsisdn
        {
            get { return _mainMsisdn; }
            set { _mainMsisdn = value; }
        }
        private string _accountId;

        public string AccountId
        {
            get { return _accountId; }
            set { _accountId = value; }
        }
        private string _sitecode;

        public string Sitecode
        {
            get { return _sitecode; }
            set { _sitecode = value; }
        }
        private string _productType;

        public string ProductType
        {
            get { return _productType; }
            set { _productType = value; }
        }
        private string _amount;

        public string Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }
        private string _serviceType;

        public string ServiceType
        {
            get { return _serviceType; }
            set { _serviceType = value; }
        }
        private string _currency;

        public string Currency
        {
            get { return _currency; }
            set { _currency = value; }
        }
        private string _cvvNumber;

        public string CvvNumber
        {
            get { return _cvvNumber; }
            set { _cvvNumber = value; }
        }


        private string _errcode;

        public string errcode
        {
            get { return _errcode; }
            set { _errcode = value; }
        }

        private string _errmsg;

        public string errmsg
        {
            get { return _errmsg; }
            set { _errmsg = value; }
        }

    }
    public class PaymentProfileCustomer
    {
        public bool IsDefault { get; set; }
        public string CardNumber { get; set; }
        public string Status { get; set; }
        public DateTime SetupDate { get; set; }
        public DateTime LastUsedDate { get; set; }
        public decimal LastPaymentAmount { get; set; }
        public string Currency { get; set; }
        public string MoreInfoAction { get; set; }
    }
    public class CardDetails : ErrCodeMsg
    {
        public string CardFirstName { get; set; }
        public string CardLastName { get; set; }
        public string CardNumber { get; set; }
        public string CardExpireYear { get; set; }
        public string CardExpireMonth { get; set; }
        public string CardCVV { get; set; }
        public string CardPhone { get; set; }
        public string CardCity { get; set; }
        public string CardAddress { get; set; }
        public string CardCountry { get; set; }
        public string CardPostCode { get; set; }
        public string CardType{ get; set; }
    }
    public enum PaymentMode
    {
        [EnumStringValue("DEF")]
        DEF = 0,
        [EnumStringValue("SUB")]
        SUB = 1
    }
    public enum ServiceType
    {
        [EnumStringValue("CARD")]
        CARD = 0,
        [EnumStringValue("MVNO")]
        MVNO = 1,
        [EnumStringValue("RES")]
        RES = 2
    }
}
