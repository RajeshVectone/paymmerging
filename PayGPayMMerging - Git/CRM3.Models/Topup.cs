﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models
{
    public class Topup : Common.ErrCodeMsg
    {
        public int topup_id { get; set; }
        public string mobileNo { get; set; }
        public DateTime? createDate { get; set; }
        public string createDate_string { get { return createDate.Value.ToString("dd/MM/yyyy hh:mm:ss tt"); } }
        public int topupTypeID { get; set; }
        public string topupTypeName { get; set; }
        public int transStatusID { get; set; }
        public string transStatusName { get; set; }
        public decimal amount { get; set; }
        public decimal prevbal { get; set; }
        public decimal afterbal { get; set; }
        //private decimal _prevbal;
        //public string prevbal { get { return string.Format("{0:N}", _prevbal); } set { _prevbal = decimal.Parse(value); } }
        //private decimal _afterbal;
        //public string afterbal { get { return string.Format("{0:N}", _afterbal); } set { _afterbal = decimal.Parse(value); } }
        public int voucher_id { get; set; }
        public string voucher_no { get; set; }
        public string reseller_name { get; set; }
        public decimal VMplus_Bonus_balance { get; set; }
        public string currency { get; set; }
        public decimal conversion_rate { get; set; }
        public decimal effective_amount { get; set; }
        public string MerchantRefCode { get; set; }
    }
    public class TopupSearch : Common.ErrCodeMsg
    {
        public string account_id { get; set; }
        public string submit_by { get; set; }
        public string Msisdn { get; set; }
        public string Sitecode { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public InfoType SearchType { get; set; }
        //06-Aug-2015 : Moorthy Added
        public string Product_Code { get; set; }
    }
    public enum InfoType
    {
        TopUpLog = 1,
        TopupStatus =2,
        CancelAutoTopup =3,
        //25-Feb-2019 : Moorthy : CRMIMP-18 - Daily usage history
        DailyUsageHistory = 4
    }

    public class TopupStatus : Common.ErrCodeMsg
    {
        public int status { get; set; }
        public string message { get; set; }
        public double threshold { get; set; }
        public string _Last6digitsCC;
        public string Last6digitsCC { get { return string.Format("********{0}", _Last6digitsCC); } set { _Last6digitsCC = value; } }
    }

    //20-Feb-2019 : Moorthy : Added for New Cancel Topup
    public class TopupStatusNew : Common.ErrCodeMsg
    {
        public string autotopup_status { get; set; }
        public string cardnumber { get; set; }
        public string recent_autotopup_date { get; set; }
        public string aut_cancel_date { get; set; }
        public string aut_cancel_by { get; set; }
    }

    //25-Feb-2019 : Moorthy : CRMIMP-18 - Daily usage history
    public class DailyUsageHistoryOutput
    {
        public string MOBILENO { get; set; }
        public string BREAKAGE_TYPE { get; set; }
        public double? CALL { get; set; }
        public double? SMS { get; set; }
        public double? DATA { get; set; }
    }
}
