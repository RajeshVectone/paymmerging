﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models.Subscriber
{
    public class SubsciberInfo : Common.ErrCodeMsg
    {
        private string _title;
        public string Title
        {
            get
            {
                return string.IsNullOrEmpty(_title) ? "" : _title;
            }
            set
            {
                _title = value;
            }
        }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Address1 { get; set; }
        private string _Address2;
        public string Address2
        {
            get
            {
                return string.IsNullOrEmpty(_Address2) ? "" : _Address2;
            }
            set
            {
                _Address2 = value;
            }
        }
        public string City { get; set; }
        public string Postcode { get; set; }
        private string _Houseno;
        public string HouseNo
        {
            get
            {
                return string.IsNullOrEmpty(_Houseno) ? "" : _Houseno;
            }
            set
            {
                _Houseno = value;
            }
        }
        public string Country { get; set; }
        private string _CountryCode;
        public string CountryCode
        {
            get
            {
                return string.IsNullOrEmpty(_CountryCode) ? "" : _CountryCode;
            }
            set
            {
                _CountryCode = value;
            }
        }
        private string _State;
        public string State
        {
            get
            {
                return string.IsNullOrEmpty(_State) ? "" : _State;
            }
            set
            {
                _State = value;
            }
        }
        private string _Telephone;
        public string Telephone
        {
            get
            {
                return string.IsNullOrEmpty(_Telephone) ? "" : _Telephone;
            }
            set
            {
                _Telephone = value;
            }
        }
        private string _mobilephone;
        public string MobilePhone
        {
            get
            {
                return string.IsNullOrEmpty(_mobilephone) ? "" : _mobilephone;
            }
            set
            {
                _mobilephone = value;
            }
        }
        private string _Fax;
        public string Fax
        {
            get
            {
                return string.IsNullOrEmpty(_Fax) ? "" : _Fax;
            }
            set
            {
                _Fax = value;
            }
        }
        private string _Email;
        public string Email
        {
            get
            {
                return string.IsNullOrEmpty(_Email) ? "" : _Email;
            }
            set
            {
                _Email = value;
            }
        }
        //public string BirthDate_String { get { return BirthDate. ToString("dd-MM-yyyy"); } }
        private DateTime? _BirthDate { get; set; }
        public DateTime? BirthDate { get 
        {
            return _BirthDate.HasValue ? _BirthDate.Value : DateTime.Now.AddYears(-17);
        } set { _BirthDate = value; } }
        private string _PersonNumber;
        public string PersonNumber
        {
            get
            {
                return string.IsNullOrEmpty(_PersonNumber) ? "" : _PersonNumber;
            }
            set
            {
                _PersonNumber = value;
            }
        }
        public string _Question;
        public string SecurityQuestion
        {
            get
            {
                return string.IsNullOrEmpty(_Question) ? "" : _Question;
            }
            set
            {
                _Question = value;
            }
        }
        public string _answer;
        public string SecurityAnswer
        {
            get
            {
                return string.IsNullOrEmpty(_answer) ? "" : _answer;
            }
            set
            {
                _answer = value;
            }
        }
        public int SubscriberType { get; set; }
        public string _ssn;
        public string Ssn
        {
            get
            {
                return string.IsNullOrEmpty(_ssn) ? "" : _ssn;
            }
            set
            {
                _ssn = value;
            }
        }
        public int PersonalIDType { get; set; }
        public string reference { get; set; }
        public string Comment { get; set; }
        public string SourceReg { get; set; }
    }

    public class CountryCode
    {
        public string countrycode { get; set; }
        public string countryname { get; set; }
    }
}
