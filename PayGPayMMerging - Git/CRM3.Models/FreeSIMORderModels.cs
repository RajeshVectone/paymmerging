﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace CRM_API.Models
{
    public class FreeSIMOrderInfo
    {
        public string sitecode { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string houseno { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string postcode { get; set; }
        public string state { get; set; }
        public string mobilephone { get; set; }
        public string email { get; set; }
        public string subscriberchannel { get; set; }
        public string sourceaddress { get; set; }
        public string ordersimurl { get; set; }
        public string ip_address { get; set; }
        public int nb_of_sim { get; set; }

        public FreeSIMOrderType Type { get; set; }

        public string username { get; set; }
    }

    public class FreeSIMOrderResponse
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
        public int subsId { get; set; }
        public int freesimid { get; set; }
        public string freesimid_out { get; set; }
    }

    public enum FreeSIMOrderType
    {
        PlaceOrder = 1,
        ReferAFriend = 2
    }
}