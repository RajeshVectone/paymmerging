﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace CRM_API.Models
{
    public enum RefMisc
    {
        Status = 1,
        FreesimId = 2,
        SubOrder = 3
    }

    public enum SIMOrderActionType
    {
        SearchByNew = 1,
        SearchByNameUnprocessed = 11,
        SearchBySIMIdUnprocessed = 12,
        SearchByNameNew = 13,
        SearchBySIMIdNew = 14,
        SearchByNameProcessed = 15,
        SearchBySIMIdProcessed = 16,
        SearchByOrderId = 17,
        List = 21
    }

    public class RefInfo
    {
        public string Description { get; set; }

        public int Id { get; set; }
        public string Name { get; set; }

        public string Value { get; set; }
    }

    public class SIMOrderItem
    {
        public SIMOrderActionType action_type { get; set; }
        public string activate_date_string { get { return "-"; } }

        public string address1 { get; set; }

        public string address2 { get; set; }

        public string contact { get; set; }

        public string country { get; set; }

        public string create_by { get; set; }

        public string cybersource_id { get { return payment_ref; } }

        public DateTime dispatch_date { get; set; }

        public string email { get; set; }

        public string first_name { get; set; }

        public dynamic freesim_order_id { get; set; }

        public string fromdate { get; set; }

        public string fullname { get { return first_name + " " + last_name; } }

        public string iccid { get; set; }

        public string last_name { get; set; }

        public int max_result { get; set; }
        public int mimsi_order_id { get; set; }

        public string mobileno { get; set; }

        public DateTime order_date { get; set; }

        public string order_date_string { get { return order_date.ToString("dd-MM-yyyy"); } }

        public DateTime sentdate { get; set; }

        //public string sent_date_string { get { return sentdate.ToString("dd-MM-yyyy"); } }

        public DateTime activatedate { get; set; }

        //public string activate_date_string { get { return activatedate.ToString("dd-MM-yyyy"); } }

        public string order_source { get; set; }

        public string order_status { get; set; }

        public string ordersim_url { get; set; }

        public string payment_ref { get; set; }

        public string postcode { get; set; }

        public int quantity { get { return 1; } }

        public string search_text { get; set; }
        public string ipaddress { get; set; }

        public string sent_date_string
        {
            get
            {
                if (dispatch_date == DateTime.MinValue)
                    return "-";
                else
                    return dispatch_date.ToString("dd-MM-yyyy");
            }
        }

        public string Sitecode { get; set; }

        public string source_reg { get; set; }

        public int stat_id { get { return 0; } }

        public string subsciber_type { get; set; }

        public int subscriber_id { get { return mimsi_order_id; } }

        public string title { get; set; }

        public string todate { get; set; }
        public string town { get; set; }
        public string logged_user { get; set; }
        public dynamic Birthdate { get; set; }
        public dynamic UpdateDate { get; set; }
        public string royal_mail_reference { get; set; }

        //Added : 24-Dec-2018 - Send a SIM Card
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string Postcode { get; set; }
        public bool IsCancel { get; set; }

    }
    public class CancelLocations
    {
        public string vlr_number { get; set; }
        public string imsi_active { get; set; }
        public string url { get; set; }
        public string type { get; set; }
        public string mode { get; set; }

    }
    public class FreesimOrderItem
    {
        public dynamic freesimid { get; set; }
        public string Fullname { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string title { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Address { get; set; }
        public string city { get; set; }
        public string postcode { get; set; }
        public string houseno { get; set; }
        public string telephone { get; set; }
        public string mobilephone { get; set; }
        public string email { get; set; }
        public DateTime? birthdate { get; set; }
        public int subscriberid { get; set; }
        public string msisdn_referrer { get; set; }
        public string iccid { get; set; }
        public string source_address { get; set; }
        public dynamic stat_id { get; set; }
        public string ordersim_url { get; set; }
        public string freesimstatus { get; set; }
        public dynamic registerdate { get; set; }
        public dynamic sentdate { get; set; }
        public dynamic activatedate { get; set; }
        public int Quantity { get; set; }
        public string CyberSourceId { get; set; }
        public string SourceReg { get; set; }
        public string CrmCs { get; set; }
        public string sitecode { get; set; }
        public string Subscriber_Type { get; set; }
        public string FreeSimType { get; set; }
        public int mimsi_order_id { get; set; }
        public string sim_brand { get; set; }
        public string homecountry_code { get; set; }
        public string royal_mail_reference { get; set; }
        public string fav_call_country { get; set; }
        public string ipaddress { get; set; }
        //21-Jul-2017 : Moorthy : Added for landing & visit page url's
        public string landing_page_url { get; set; }
        public string last_visit_page_url { get; set; }

        //11-Dec-2017 : Added to display Replacement Date
        public dynamic replacement_date { get; set; }
    }

    public class SIMOrderLabelItem : SIMOrderItem
    {
        public int dispatch_id { get; set; }

        public string item_status { get; set; }

        public string sim_brand { get; set; }
    }

    public class SIMOrderLabelRequest
    {
        public string crm_login { get; set; }

        public string iccid { get; set; }

        public int orderid { get; set; }
        public string freesimid { get; set; }
        public string sitecode { get; set; }
    }

    public class SIMOrderRequest
    {
        public string contact { get; set; }

        public string email { get; set; }

        public string fromdate { get; set; }

        public string full_name { get; set; }

        public int max_result { get; set; }
        public string mobileno { get; set; }

        public int order_id { get; set; }

        public string postcode { get; set; }

        public string sim_id { get; set; }
        public string iccid { get; set; }
        public string cybersourceid { get; set; }
        public string sitecode { get; set; }
        public string product_code { get; set; }
        public string status { get; set; }
        public string todate { get; set; }
        public string searchby { get; set; }
        public string searchvalue { get; set; }
        public bool isDownload { get; set; }
    }
    public class SIMOrderStatus
    {
        public static string Dispatched = "DISPATCHED";
        public static string NewOrder = "NEW ORDER";
    }

    public class CancelFreeSimOrderOutput
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
        private string _errsubject = "General Error";
        public string errsubject
        {
            get
            {
                return (errcode == 0 || errcode == 1) ? "Result Information" : _errsubject;
            }
            set { _errsubject = value; }
        }
        public string email { get; set; }
        public string firstname { get; set; }
    }
}