﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.Models.Docs
{
    public class DocModelCommon
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
    }

    public class DocModel : DocModelCommon
    {
        public int category_id { get; set; }
        public string category_name { get; set; }
        public string filename { get; set; }
        public string filepath { get; set; }
        public string upload_by { get; set; }
        public int doc_id { get; set; }
        public int processtype { get; set; }
        public ModeType modetype { get; set; }
        public int process_type { get; set; }
        public int is_archive_flag { get; set; }
        public string product_name { get; set; }
    }

    public enum ModeType
    {
        GETUPLOADEDDOCINFO = 1,
        GETDOCCATEGORYINFO = 2,
        CREATEDOCCATEGORY = 3,
        INSERTDOCUPLOADINFO = 4,
        UPDATEDOCUPLOADINFO = 5
    }

    public class UploadedDocInfo
    {
        public int pk_doc_id { get; set; }
        public int fk_category_id { get; set; }
        public string filename { get; set; }
        public string filepath { get; set; }
        public DateTime upload_date { get; set; }
        public string upload_date_str { get { return upload_date != null ? Convert.ToDateTime(upload_date).ToString("yyyy-MM-dd") : ""; } }
        public string upload_by { get; set; }
        public DateTime last_update { get; set; }
        public string last_update_str { get { return last_update != null ? Convert.ToDateTime(last_update).ToString("yyyy-MM-dd") : ""; } }
        public string category_name { get; set; }
        public string fullpath { get { return ConfigurationManager.AppSettings["UPLOADURLPATH"] + filepath; } }
        public string Action { get { return ""; } }
        public string Action2 { get { return ""; } }
    }

    public class DocCategory
    {
        public int pk_category_id { get; set; }
        public string category_name { get; set; }
    }
}
