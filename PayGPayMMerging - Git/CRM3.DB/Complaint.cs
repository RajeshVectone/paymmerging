﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;
using CRM_API.Models.Issue;
using CRM_API.Models.Complaint;
using CRM_API.DB.Common;

namespace CRM_API.DB.Complaint
{
    public class SProc
    {
        public static IEnumerable<Models.Complaint.ComplaintModels> GetComplaintList(string mobileno)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<Models.Complaint.ComplaintModels>(
                            "crm_get_tickets_by_mobileno", new
                            {
                                @mobileno = mobileno,
                                @userid = 0    // '0' get all records 
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<Models.Complaint.ComplaintModels>();
                }
            }
            catch (Exception ex)
            {
                List<Models.Complaint.ComplaintModels> resValue = new List<Models.Complaint.ComplaintModels>() { new Models.Complaint.ComplaintModels { errcode = -1, errmsg = ex.Message } };
                return resValue;
            }
        }

        public static IEnumerable<Models.Complaint.ComplaintModels> InsertComplaint(string customer_name, string product_code, string mobileno, int issue_type, int Issue_Function, int Issue_Complaint, int issue_status, string descr, int esclating_user, int process_type, int agent_userid, string ticket_id, string fk_priority_id)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<Models.Complaint.ComplaintModels>(
                            "crm_generate_complaint_ticket", new
                            {
                                @ticket_id = ticket_id,
                                @customer_name = customer_name,
                                @product_code = product_code,
                                @mobileno = mobileno,
                                @issue_type = issue_type,
                                @Issue_Function = Issue_Function,
                                @Issue_Complaint = Issue_Complaint,
                                @issue_status = issue_status,
                                @descr = descr,
                                @esclating_user = esclating_user,
                                @process_type = process_type,
                                @agent_userid = agent_userid,
                                @fk_priority_id = fk_priority_id
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<Models.Complaint.ComplaintModels>();
                }
            }
            catch (Exception ex)
            {
                List<Models.Complaint.ComplaintModels> resValue = new List<Models.Complaint.ComplaintModels>() { new Models.Complaint.ComplaintModels { errcode = -1, errmsg = ex.Message } };
                return resValue;
            }
        }
    }
}
