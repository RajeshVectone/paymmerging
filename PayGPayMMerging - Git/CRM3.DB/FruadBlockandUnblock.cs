﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;
using CRM_API.Models.FruadBlockUnBlockModels;

namespace CRM_API.DB.FruadBlockUnBlock
{
    public class SProc
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public static IEnumerable<BlockReason> GetBlockReason()
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<BlockReason>(
                            "crm_get_block_reason", new
                            {
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<BlockReason>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetBlockReason: " + ex.Message);
                return new List<BlockReason>();
            }
        }

        public static IEnumerable<BlockCommon> DoBlockUnblockRequest(int USERTYPE, string USERINFO, int REQ_TYPE, int REASON_ID, int FK_REQ_USERID, string Product, string SiteCode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<BlockCommon>(
                            "crm_do_block_unblock_request", new
                            {
                                @USERTYPE = USERTYPE,
                                @USERINFO = USERINFO,
                                @REQ_TYPE = REQ_TYPE,
                                @REASON_ID = REASON_ID,
                                @FK_REQ_USERID = FK_REQ_USERID,
                                @PRODUCT_CODE = Product,
                                @SITECODE = SiteCode,
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<BlockCommon>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("DoBlockUnblockRequest: " + ex.Message);
                return new List<BlockCommon>() { new BlockCommon() { errcode = -1, errmsg = ex.Message } };
            }
        }

        public static IEnumerable<BlockUnblockRequestOutput> GetBlockUnblockRequest(int user_id, int role_id)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<BlockUnblockRequestOutput>(
                            "crm_get_block_unblock_request", new
                            {
                                @user_id = user_id,
                                @role_id = role_id
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<BlockUnblockRequestOutput>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetBlockUnblockRequest: " + ex.Message);
                return new List<BlockUnblockRequestOutput>() { };
            }
        }

        public static IEnumerable<BlockCommon> DoBlockConfirmation(int loginuser_id, int role_id, int REQID, string loginuser_name)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<BlockCommon>(
                            "crm_do_block_confirmation", new
                            {
                                @loginuser_id = loginuser_id,
                                @role_id = role_id,
                                @REQID = REQID,
                                @loginuser_name = loginuser_name
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<BlockCommon>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("DoBlockConfirmation: " + ex.Message);
                return new List<BlockCommon>() { new BlockCommon() { errcode = -1, errmsg = ex.Message } };
            }
        }
    }
}
