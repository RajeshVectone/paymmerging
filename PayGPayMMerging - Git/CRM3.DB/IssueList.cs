﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;

namespace CRM_API.DB
{
    //CRM Enhancement 2
    public class IssueList
    {
        public static IEnumerable<Models.Issue.IssueModels> GetIssueListBySubscriberid(int subscriber, string sitecode, string mobileno, string dt_from, string dt_to)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<Models.Issue.IssueModels>(
                            "crm3_crm_issue_list_by_subscriberid", new
                            {
                                @subscriberid = subscriber,
                                @sitecode = sitecode,
                                @mobileno = mobileno,
                                @dt_from = dt_from,
                                @dt_to = dt_to
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<Models.Issue.IssueModels>();
                }
            }
            catch (Exception ex)
            {
                return new List<Models.Issue.IssueModels>();
            }
        }
        public static IEnumerable<CRM_API.Models.Issue.IssueHistoryDetailOutput> GetIssueHistoryDetailList(CRM_API.Models.Issue.IssueHistoryDetailInput req)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.Issue.IssueHistoryDetailOutput>(
                            "crm3_crm_get_issue_history_info", new
                            {
                                @sitecode = req.Sitecode,
                                problem_id = req.problemid 
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.Issue.IssueHistoryDetailOutput>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL GetIssueHistoryDetailList: " + ex.Message);
            }
        }
    }
}
