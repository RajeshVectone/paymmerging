﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;

namespace CRM_API.DB
{
    public class FreeDataBundleDB
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public static List<GetBundleGroupOutput> GetBundleGroup(FreeDataBundleInput req)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<GetBundleGroupOutput>(
                            "crm_get_bundle_group", new
                            {
                                @sitecode = req.sitecode,
                                @product_code = req.product_code,
                            }, commandType: CommandType.StoredProcedure);

                    return result.ToList() ?? new List<CRM_API.Models.GetBundleGroupOutput>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetBundleGroup : " + ex.Message);
                return new List<CRM_API.Models.GetBundleGroupOutput>();
            }
        }

        public static List<GetBundleListInfoOutput> GetBundleListInfo(FreeDataBundleInput req)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<GetBundleListInfoOutput>(
                            "crm_get_bundle_list_info", new
                            {
                                @sitecode = req.sitecode,
                                @product_code = req.product_code,
                                @category_id = req.category_id
                            }, commandType: CommandType.StoredProcedure);

                    return result.ToList() ?? new List<CRM_API.Models.GetBundleListInfoOutput>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetBundleListInfo : " + ex.Message);
                return new List<CRM_API.Models.GetBundleListInfoOutput>();
            }
        }

        public static List<BundleSubscribeFreeOutput> BundleSubscribeFree(FreeDataBundleInput req)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<BundleSubscribeFreeOutput>(
                            "crm3_bundle_subscribe_free", new
                            {
                                @sitecode = req.sitecode,
                                @usertype = 2,
                                @userinfo = req.userinfo,
                                @bundleid = req.bundleid,
                                @paymode = 3,
                                @processby = "CRM",
                                @pack_dest = req.pack_dest,
                                @subscribed_user = req.subscribed_user,
                                @bundle_price = req.bundle_price,
                                @bundle_category = req.bundle_category,
                                @bundle_name = req.bundle_name
                            }, commandType: CommandType.StoredProcedure);

                    return result.ToList() ?? new List<CRM_API.Models.BundleSubscribeFreeOutput>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("BundleSubscribeFree : " + ex.Message);
                return new List<CRM_API.Models.BundleSubscribeFreeOutput>();
            }
        }

        public static List<GetFreelySubscribedBundleReportOutput> GetFreelySubscribedBundleReport(FreeDataBundleInput req)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<GetFreelySubscribedBundleReportOutput>(
                            "crm_get_freely_subscribed_bundle_report", new
                            {
                                @sitecode = req.sitecode,
                                @from_date = req.from_date,
                                @to_date = req.to_date
                            }, commandType: CommandType.StoredProcedure);

                    return result.ToList() ?? new List<CRM_API.Models.GetFreelySubscribedBundleReportOutput>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetFreelySubscribedBundleReport : " + ex.Message);
                return new List<CRM_API.Models.GetFreelySubscribedBundleReportOutput>();
            }
        }
    }
}
