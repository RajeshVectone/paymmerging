﻿using CRM_API.Models;
using CRM_API.Models.CTP;
using Dapper;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CRM_API.DB.CTP
{
    public class SProc
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public static IEnumerable<SearchOption> GetSearchOption()
        {
            var result = new List<SearchOption>();
            result.Add(new SearchOption() { Id = 1, Label = "CLI/Registered Number" });
            result.Add(new SearchOption() { Id = 2, Label = "Email" });
            result.Add(new SearchOption() { Id = 3, Label = "Account Number" });
            result.Add(new SearchOption() { Id = 4, Label = "Global eTalk (GT)" });
            result.Add(new SearchOption() { Id = 5, Label = "Name" });
            return result;
        }
        public static IEnumerable<CustomerSearchResult> SearchCTP(string product_code, string searchBy, string searchValue)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<CustomerSearchResult>(
                            "crm3_search_customer", new
                            {
                                @mundio_product = product_code,
                                @searchby = searchBy,
                                @searchvalue = searchValue
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CustomerSearchResult>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.CTP SearchCTP: {0}", ex.Message);
                throw new Exception("CRM_API.DB.CTP SearchCTP: " + ex.Message);
            }
        }
    }
}
