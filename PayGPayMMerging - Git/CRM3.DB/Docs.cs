﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using NLog;
using Newtonsoft.Json;
using System.IO;
using System.Net.Mail;
using System.Configuration;
using CRM_API.Models.Docs;

namespace CRM_API.DB
{
    public class Docs
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        #region GetUploadedDocInfo
        public static IEnumerable<UploadedDocInfo> GetUploadedDocInfo(int category_id, int @is_archive_flag, string product_name)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<UploadedDocInfo>(
                            "crm_get_uploaded_doc_info", new
                            {
                                @category_id = category_id,
                                @is_archive_flag = is_archive_flag,
                                @product_name = product_name
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<UploadedDocInfo>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetUploadedDocInfo() : " + ex.Message);
                throw new Exception("GetUploadedDocInfo() : " + ex.Message);
            }
        }
        #endregion

        #region GetDocCategoryInfo
        public static IEnumerable<DocCategory> GetDocCategoryInfo()
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<DocCategory>(
                            "crm_get_doc_category_info", new
                            {

                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<DocCategory>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetDocCategoryInfo() : " + ex.Message);
                throw new Exception("GetDocCategoryInfo() : " + ex.Message);
            }
        }
        #endregion

        #region CreateDocCategory
        public static IEnumerable<DocModelCommon> CreateDocCategory(int category_id, string category_name, int process_type)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<DocModelCommon>(
                            "crm_create_doc_category", new
                            {
                                @category_id = category_id,
                                @category_name = category_name,
                                @process_type = process_type
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<DocModelCommon>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CreateDocCategory() : " + ex.Message);
                throw new Exception("CreateDocCategory() : " + ex.Message);
            }
        }
        #endregion

        #region InsertDocUploadInfo
        public static IEnumerable<DocModelCommon> InsertDocUploadInfo(int category_id, string filename, string filepath, string upload_by, int doc_id, int processtype, string product_name)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<DocModelCommon>(
                            "crm_insert_doc_upload_info", new
                            {
                                @category_id = category_id,
                                @filename = filename,
                                @filepath = filepath,
                                @upload_by= upload_by,
                                @doc_id = doc_id,
                                @processtype = processtype,
                                @product_name = product_name
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<DocModelCommon>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("InsertDocUploadInfo() : " + ex.Message);
                throw new Exception("InsertDocUploadInfo() : " + ex.Message);
            }
        }
        #endregion

        #region UpdateDocUploadInfo
        public static IEnumerable<DocModelCommon> UpdateDocUploadInfo(int doc_id, int processtype)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<DocModelCommon>(
                            "crm_insert_doc_upload_info", new
                            {
                                @category_id=0,
                                @filename="",
                                @filepath="",
                                @upload_by="",
                                @doc_id = doc_id,
                                @processtype = processtype
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<DocModelCommon>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("UpdateDocUploadInfo() : " + ex.Message);
                throw new Exception("UpdateDocUploadInfo() : " + ex.Message);
            }
        }
        #endregion
    }
}
