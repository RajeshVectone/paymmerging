﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;

namespace CRM_API.DB.Customer
{
    public class VectoneXtraApp
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public static VectoneXtraAppOutput GetVectoneXtraDetails(string mobileno)
        {            
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<VectoneXtraAppOutput>(
                            "crm_mvno_vectoneapp_details", new
                            {
                                @mobileno = mobileno
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    return result ?? new VectoneXtraAppOutput();
                }
            }
            catch (Exception ex)
            {
                Log.Error("BL GetVectoneXtraDetails: {0}", ex.Message);
                throw new Exception("BL GetVectoneXtraDetails: " + ex.Message);
            }
        }

        //CRM Enhancement 2
        public static IEnumerable<VectoneXtraCallHistoryOutput> GetVectoneXtraCallHistory(string mobileno, string DateFrom, string DateTo, string SiteCode, int? PackageID)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<VectoneXtraCallHistoryOutput>(
                            "CRM_VectoneX_App_Call_History_Search_SP", new
                            {
                                @Msisdn = mobileno,
                                @DateFrom = DateFrom,
                                @DateTo = DateTo,
                                @Type=0,
                                @SiteCode = SiteCode,
                                @PackageID = PackageID == 0 ? null : PackageID
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<VectoneXtraCallHistoryOutput>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("BL GetVectoneXtraCallHistory: {0}", ex.Message);
                throw new Exception("BL GetVectoneXtraCallHistory: " + ex.Message);
            }
        }
    }
}