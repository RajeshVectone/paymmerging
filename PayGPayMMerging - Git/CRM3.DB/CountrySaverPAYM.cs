﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Dapper;
using System.Threading.Tasks;
using CRM_API.Models.CountrySaverPAYM;


namespace CRM_API.DB.CountrySaverPAYM
{
    public class SProc
    {
        private static string SP_GETLIST_CS = "crm3_countrysaver_getlist";
        private static string SP_GETLIST_STATUS = "crm3_countrysaver_getstatus";
        private static string SP_SUBMIT_RENEW = "crm3_countrysaver_renew";
        private static string SP_SUBMIT_REACTIVATE = "crm3_countrysaver_reactivate";
        private static string SP_SUBMIT_CANCELPAYM = "crm3_Countrysaver_cancel_paym"; //added on 21-01-2015 for PAYM - by Hari
        //08-Jan-2015 : Moorthy : Added to show the Additional CLI Details
        private static string SP_GETADDITIONALCLI_CS = "crm3_crm_get_Addon_list";
        private static string SP_GETLISTCLI_CS = "REGISTRATIONS_SELECT_VIEWDETAILS_V1";
        private static string SP_GET_REGID = "COMMON_SELECT";
        private static string SP_GETLIST_CS_Subscription_LIST = "Subscription_History_CountrySaver_PayM";
        private static string SP_GETLIST_CS_ActivateCancelling_LIST = "Crm3_Cancle_By_Account_Id";

        public static IEnumerable<CountrySaverList> GetListCSaver(AdditionalReqInfo Model)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<CountrySaverList>(SP_GETLIST_CS, new
                    {
                        @mobileno = Model.Mobileno,
                        @sitecode = Model.Sitecode
                    }, commandType: CommandType.StoredProcedure
                    );

                    return result ?? new List<CountrySaverList>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("CountrySaverList: " + ex.Message);
            }
        }

        /*added on 19 Jan 2015 - by Hari*/
        //8-Jan-2016 : Moorthy : Modified for Country Saver
        public static async Task<IEnumerable<CountrySaverList_New>> GetBundleCountrySaverDetail(string mobileno, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result =
                        Task.Run(() =>
                        {
                            //return conn.Query<CountrySaverList>(
                            //    "crm3_countrysaver_getlist", new
                            //    {
                            //        @mobileno = mobileno,
                            //        @sitecode = sitecode
                            //    }, commandType: CommandType.StoredProcedure);

                            //changed on 12 Feb 2015 - Country Saver
                            //return conn.Query<CountrySaverList>(
                            //    "crm3_countrysaver_getlist_paym", new
                            //    {
                            //        @mobileno = mobileno,
                            //        @sitecode = sitecode
                            //    }, commandType: CommandType.StoredProcedure);

                            //return conn.Query<CountrySaverList>(
                            //   "crm3_countrysaver_getlist_paym_v2", new
                            //   {
                            //       @mobileno = mobileno,
                            //       @sitecode = sitecode
                            //   }, commandType: CommandType.StoredProcedure,commandTimeout: 300);
                            //end changed

                            //
                            return conn.Query<CountrySaverList_New>(
                               "crm3_countrysaver_getlist_new_V1", new
                               {
                                   @mobileno = mobileno,
                                   @sitecode = sitecode
                               }, commandType: CommandType.StoredProcedure,commandTimeout: 300);
                        }
                    );
                    return await result ?? new List<CountrySaverList_New>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("CountrySaverList: " + ex.Message);
            }
        }
        /*end hari*/

        public static IEnumerable<StatusList> GetListStatus(AdditionalReqInfo Model)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<StatusList>(SP_GETLIST_STATUS, new
                    {
                        @sitecode = Model.Sitecode
                    }, commandType: CommandType.StoredProcedure
                    );

                    return result ?? new List<StatusList>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("CountrySaverList: " + ex.Message);
            }
        }

        public static Models.Common.ErrCodeMsg SubmitRenew(AdditionalReqInfo Model)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var p = new DynamicParameters();
                    p.Add("@reg_idx", Convert.ToInt32(Model.RegIDX));
                    p.Add("@sitecode", Model.Sitecode);
                    p.Add("@errcode", dbType: DbType.Int32, size: 3, direction: ParameterDirection.Output);
                    p.Add("@errmessage", dbType: DbType.String, size: 255, direction: ParameterDirection.Output);
                    conn.Execute(SP_SUBMIT_RENEW, p, null, null, CommandType.StoredProcedure);

                    Model.errcode = p.Get<Int32>("errcode");     
                    Model.errmsg = p.Get<string>("errmessage");

                    return new Models.Common.ErrCodeMsg()
                    {
                        errcode = Model.errcode,
                        errmsg = Model.errmsg
                    };
                }
            }
            catch (Exception ex)
            {
                return new Models.Common.ErrCodeMsg()
                {
                    errcode = -1,
                    errmsg = ex.Message
                };
            }
        }

        public static Models.Common.ErrCodeMsg SubmitReactivate(AdditionalReqInfo Model)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var p = new DynamicParameters();
                    p.Add("@reg_idx", Convert.ToInt32(Model.RegIDX));
                    p.Add("@sitecode", Model.Sitecode);
                    p.Add("@errcode", dbType: DbType.Int32, size: 3, direction: ParameterDirection.Output);
                    p.Add("@errmessage", dbType: DbType.String, size: 255, direction: ParameterDirection.Output);
                    p.Add("@paymode", Convert.ToInt32(Model.Paymode));
                    conn.Execute(SP_SUBMIT_REACTIVATE, p, null, null, CommandType.StoredProcedure);

                    Model.errcode = p.Get<Int32>("errcode");
                    //Model.errcode = Convert.ToInt32(p.Get<string>("errcode"));      // param in DB varchar
                    Model.errmsg = p.Get<string>("errmessage");

                    return new Models.Common.ErrCodeMsg()
                    {
                        errcode = Model.errcode,
                        errmsg = Model.errmsg
                    };
                }

            }
            catch (Exception ex)
            {
                return new Models.Common.ErrCodeMsg()
                {
                    errcode = -1,
                    errmsg = ex.Message
                };
            }
        }

        public static IEnumerable<Models.Common.ErrCodeMsg> ChangeCancelmode(AdditionalReqInfo Model)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Models.Common.ErrCodeMsg>(SP_GETLIST_CS_ActivateCancelling_LIST, new
                    {
                        @MAIN_MSISDN = Model.accountid,
                        @FNF_NO = Model.DestinationCli
                    }, commandType: CommandType.StoredProcedure, commandTimeout: 300
                    );

                    return result ?? new List<Models.Common.ErrCodeMsg>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("CountrySaverList: " + ex.Message);
            }
        }

        public static Models.Common.ErrCodeMsg SubmitCancel(AdditionalReqInfo Model)
        {
            try
            {             
                return new Models.Common.ErrCodeMsg()
                {
                    errcode = 0,
                    errmsg = "Cancel Country Saver Couldn't be done by CRM"
                };

            }
            catch (Exception ex)
            {
                return new Models.Common.ErrCodeMsg()
                {
                    errcode = -1,
                    errmsg = ex.Message
                };
            }
        }

        /*added on 21 Jan 2015 - by Hari*/
        public static Models.Common.ErrCodeMsg CancelBundleCountrySaverDetail(string sitecode, int usertype, string mobileno, DateTime renewal_date, int bundleid, int packageid, string processby, string AccountNo, string Pack_Dest)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.CountrySaverConnection))
                {
                    var result = conn.Query<Models.Common.ErrCodeMsg>(
                                //"crm3_Countrysaver_cancel_paym", new
                                "FSB_CANCEL_BY_GT_ID_v1_on_11_06_2015", new
                                {
                                    @MAIN_MSISDN = AccountNo,
                                    @FNF_NO = Pack_Dest,
                                    //@renewal_date = renewal_date,
                                    //@bundleid = bundleid,
                                    //@package_id = packageid,
                                    //@processby = processby
                                }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    if (result.errcode == 0)
                    { SMSDeliveryContent.SMS_BundleCancelService(mobileno, 2, result.errmsg); }
                    return result;
                }
            }
            catch (Exception ex)
            {
                //throw new Exception("CancelBundleCountrySaverDetail: " + ex.Message);
                return new Models.Common.ErrCodeMsg()
                {
                    errcode = -1,
                    errmsg = ex.Message
                };
            }
        }
        /*end hari*/

        //08-Jan-2015 : Moorthy : Added to show the Additional CLI Details

        public static IEnumerable<AdditionalCLI> GetListCLI(CRM_API.Models.CountrySaver.CountrySaverList countrySaver)
        {
            try
            {
                int regid = GetRegIDForCountrySaver(countrySaver);
                String paramString = GetFandFNoCountrySaver(countrySaver.DestinationNumber);
                //Find Account ID
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.PortalConnection))
                {

                    var resultcli = conn.Query<AdditionalCLI>(SP_GETLISTCLI_CS, new
                    {
                        @ParamID = regid,
                        @ParamString = paramString,
                        @ActionType = 2,
                        @CLI_Number = countrySaver.Account_ID
                    }, commandType: CommandType.StoredProcedure
                    );

                    return resultcli ?? new List<AdditionalCLI>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("GetListCLI: " + ex.Message);
            }
        }

        public static IEnumerable<AdditionalCLI> GetAdditionalCLI(AdditionalReqInfo Model)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {

                    var resultcli = conn.Query<AdditionalCLI>(SP_GETADDITIONALCLI_CS, new
                    {
                        @mobileno = Model.Mobileno,
                        @sitecode = Model.Sitecode
                    }, commandType: CommandType.StoredProcedure
                    );
                    return resultcli ?? new List<AdditionalCLI>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("GetAdditionalCLI: " + ex.Message);
            }
        }

        private static int GetRegIDForCountrySaver(CRM_API.Models.CountrySaver.CountrySaverList countrySaver)
        {
            using (var conn = new SqlConnection(CRM_API.Helpers.Config.PortalConnection))
            {
                conn.Open();
                String paramString = "";
                if (countrySaver.DestinationNumber.IndexOf("Srilanka") >= 0)
                {
                    paramString = "SRI-LANKA" + "|" + countrySaver.AccountNumber;
                }
                else if (countrySaver.DestinationNumber.IndexOf("Philippines") >= 0)
                {
                    paramString = "PHILIPPINES" + "|" + countrySaver.AccountNumber;
                }

                //Get RegID                    
                var result = conn.Query<dynamic>(SP_GET_REGID, new
                {
                    @ParamID = 0,
                    @ParamString = paramString,
                    @ActionType = 8
                }, commandType: CommandType.StoredProcedure
                ).FirstOrDefault<dynamic>();

                //Get RegID


                int regid = int.Parse(result.REG_ID.ToString());
                return regid;
            }
        }

        public static String GetFandFNoCountrySaver(String destinationNumber)
        {
            String paramString = "";
            paramString = destinationNumber.Replace("Srilanka ", "").Replace("Philippines ", "");

            if (paramString.Substring(0, 2) != "00")
            {
                paramString = "00" + paramString;
            }
            return paramString;
        }

        public static IEnumerable<CountrySaverPaymentHistory> GetViewSubscriptionlist(AdditionalReqInfo Model)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<CountrySaverPaymentHistory>(SP_GETLIST_CS_Subscription_LIST, new
                    {
                        @mobileno = Model.Mobileno,
                        @sitecode = Model.Sitecode,
                        @BundleId = Model.bundleid
                    }, commandType: CommandType.StoredProcedure, commandTimeout: 300
                    );

                    return result ?? new List<CountrySaverPaymentHistory>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("CountrySaverList: " + ex.Message);
            }
        }
    }
}
