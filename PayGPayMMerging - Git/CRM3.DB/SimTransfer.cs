﻿using CRM_API.Models;
using Dapper;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.DB.SimTransfer
{
    public class SProc
    {
        public static IEnumerable<CRM_API.Models.SimTransfer.SimTransferLog> GetSimTransferLogData(string msisdn, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.SimTransfer.SimTransferLog>(
                                // Add by BSK for 2 in 1 OTA Status display new sp
                                    //"crm3_get_simswap_log_by_mobileno", new   // old SP
                                    "crm3_simtransfer_log_getby_msisdn", new
                                    {
                                        @msisdn = msisdn,
                                        @sitecode = sitecode
                                    }, commandType: CommandType.StoredProcedure);


                    return result ?? new List<CRM_API.Models.SimTransfer.SimTransferLog>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL GetSimTransferLogData: " + ex.Message);
            }
        }
    }
}
