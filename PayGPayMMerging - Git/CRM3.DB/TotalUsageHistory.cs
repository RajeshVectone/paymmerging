﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;

namespace CRM_API.DB.TotalUsageHistory
{
    public class SProc
    {
        //28-Dec-2018 : Moorthy : Added for Bundle breakdown
        public static IEnumerable<CRM_API.Models.TotalUsageHistory.GetSubscribedBundlePackagesCDRRef> GetSubscribedBundlePackagesCDR(string mobileno, string sitecode, int search_type)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.TotalUsageHistory.GetSubscribedBundlePackagesCDRRef>(
                            "crm_subscribed_bundle_packages_cdr_drp", new
                            {
                                @sitecode = sitecode,
                                @mobileno = mobileno,
                                @search_type = search_type
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.TotalUsageHistory.GetSubscribedBundlePackagesCDRRef>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("CRM_API.DB.TotalUsageHistory.GetSubscribedBundlePackagesCDR: " + ex.Message);
            }
        }

        //28-Dec-2018 : Moorthy : Added for Bundle breakdown
        public static IEnumerable<CRM_API.Models.TotalUsageHistory.GetCallHistorySearchCBSRef> GetCallHistorySearchCBS(string Msisdn, DateTime DateFrom, DateTime DateTo, int Type, string SiteCode, string PackageID, string BundleName)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.TotalUsageHistory.GetCallHistorySearchCBSRef>(
                            "CRM_Call_History_Search_SP_CBS", new
                            {
                                @Msisdn = Msisdn,
                                @DateFrom = DateFrom.ToString("dd/MMM/yyyy"),
                                @DateTo = DateTo.ToString("dd/MMM/yyyy"),
                                @Type = Type,
                                @SiteCode = SiteCode,
                                @PackageID = PackageID
                            }, commandType: CommandType.StoredProcedure);

                    if (result != null && result.Count() > 0)
                    {
                        for (int i = 0; i < result.Count(); i++)
                        {
                            result.ElementAt(i).Package_Name = BundleName;
                        }
                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("CRM_API.DB.TotalUsageHistory.GetCallHistorySearchCBS: " + ex.Message);
            }
            return new List<CRM_API.Models.TotalUsageHistory.GetCallHistorySearchCBSRef>();
        }

        public static IEnumerable<CRM_API.Models.TotalUsageHistory.Customerpaymenthistory> GettotalusagehistoryCall(string mobileno, string sitecode, DateTime date_from, DateTime date_to)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.TotalUsageHistory.Customerpaymenthistory>(
                            "crm3_get_total_usage_Call_history_by_mobileno", new
                            {
                                @mobileno = mobileno,
                                @sitecode = sitecode,
                                @datefrom = date_from,
                                @dateto = date_to
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.TotalUsageHistory.Customerpaymenthistory>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("CRM_API.DB.TotalUsageHistory.GettotalusagehistoryCall: " + ex.Message);
            }
        }

        public static IEnumerable<CRM_API.Models.TotalUsageHistory.Customerpaymenthistory> GettotalusagehistorySMS(string mobileno, string sitecode, DateTime date_from, DateTime date_to)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {

                    var result = conn.Query<CRM_API.Models.TotalUsageHistory.Customerpaymenthistory>(
                            "crm3_get_total_usage_SMS_history_by_mobileno", new
                            {
                                @mobileno = mobileno,
                                @sitecode = sitecode,
                                @datefrom = date_from,
                                @dateto = date_to
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.TotalUsageHistory.Customerpaymenthistory>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("CRM_API.DB.TotalUsageHistory.GettotalusagehistorySMS: " + ex.Message);
            }
        }

        public static IEnumerable<CRM_API.Models.TotalUsageHistory.Customerpaymenthistory> GettotalusagehistoryData(string mobileno, string sitecode, DateTime date_from, DateTime date_to)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {

                    var result = conn.Query<CRM_API.Models.TotalUsageHistory.Customerpaymenthistory>(
                            "crm3_get_total_usage_Data_history_by_mobileno", new
                            {
                                @mobileno = mobileno,
                                @sitecode = sitecode,
                                @datefrom = date_from,

                                @dateto = date_to
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.TotalUsageHistory.Customerpaymenthistory>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("CRM_API.DB.TotalUsageHistory.GettotalusagehistoryData: " + ex.Message);
            }
        }

        public static IEnumerable<CRM_API.Models.TotalUsageHistory.Customerpaymenthistory> GettotalusagehistoryLLOM(string mobileno, string sitecode, DateTime date_from, DateTime date_to)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {

                    var result = conn.Query<CRM_API.Models.TotalUsageHistory.Customerpaymenthistory>(
                            "crm3_get_total_usage_LLOM_history_by_mobileno", new
                            {
                                @mobileno = mobileno,
                                @sitecode = sitecode,
                                @datefrom = date_from,

                                @dateto = date_to
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.TotalUsageHistory.Customerpaymenthistory>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("CRM_API.DB.TotalUsageHistory.GettotalusagehistoryLLOM: " + ex.Message);
            }
        }
    }
}
