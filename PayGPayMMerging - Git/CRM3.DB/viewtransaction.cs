﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Dapper;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Xml;
using NLog;

namespace CRM_API.DB
{
    public class viewtransaction
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public static IEnumerable<CRM_API.Models.financeviewtrans> getpaymentdetails()
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.PaymentConnection))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.financeviewtrans>("get_latest_details_from_tpayment ", new { }, commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.financeviewtrans>();
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("c3pm_get_operation_files   :  " + ex.Message);
            }
        }

        public static IEnumerable<CRM_API.Models.financeviewtransdownload> getpaymentdetailsdownload()
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.PaymentConnection))
                {
                    conn.Open();

                    // old SP:get_latest_details_from_tpayment_v1
                    // New SP:get_latest_details_from_tpayment_with_AUT  /** Auto top up purpose **/
                    //var result = conn.Query<CRM_API.Models.financeviewtransdownload>("get_latest_details_from_tpayment_v1", new { }, commandTimeout: (5 * 60), commandType: CommandType.StoredProcedure);
                    var result = conn.Query<CRM_API.Models.financeviewtransdownload>("get_latest_details_from_tpayment_with_AUT", new { }, commandTimeout: (5 * 60), commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.financeviewtransdownload>();
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("c3pm_get_operation_files   :  " + ex.Message);
            }
        }


        //Auto top Search Transcation 

        public static IEnumerable<CRM_API.Models.financeviewtransdownload> getpaymentdetailsautotop()
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.PaymentConnection))
                {
                    conn.Open();

                    // old SP:get_latest_details_from_tpayment_v1
                    // New SP:get_latest_details_from_tpayment_with_AUT  /** Auto top up purpose **/
                    var result = conn.Query<CRM_API.Models.financeviewtransdownload>("get_latest_details_from_tpayment_with_AUT_Topup", new { }, commandTimeout: (5 * 60), commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.financeviewtransdownload>();
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("c3pm_get_operation_files   :  " + ex.Message);
            }
        }

        public static IEnumerable<CRM_API.Models.financeviewtransdownload> getpaymentdetailsdownloads(string date)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.PaymentConnection))
                {
                    conn.Open();                    
                    var result = conn.Query<CRM_API.Models.financeviewtransdownload>("get_latest_details_from_tpayment_v2", new {
                        @date = date

                    }, commandTimeout: (5 * 60), commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.financeviewtransdownload>();
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("c3pm_get_operation_files   :  " + ex.Message);
            }
        }

        public static IEnumerable<CRM_API.Models.financeviewtransdownload> getpaymentdetailsdownloads_New(string Transation_Status, string Rejection_Status, string Product_Code, string Customer_Name, string payment_Ref, string mobile_Number, string EmailAddress, string ICCID, string startdate, string enddate)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.PaymentConnection))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.financeviewtransdownload>("SP_Get_TPayment_Dtls_By_Errcode_v1", new
                    {
                        @Product_Code = Product_Code,
                        @V_Trans_Status = Transation_Status,
                        @V_Rejected_Status = Rejection_Status,
                        @CustName = Customer_Name,
                        @Mobile_No = mobile_Number,
                        @Payment_Ref = payment_Ref,
                        @Email = EmailAddress,
                        @Iccid = ICCID,
                        @Fromdate = startdate,
                        @Todate = enddate

                    }, commandTimeout: (5 * 60), commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.financeviewtransdownload>();
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("SP_Get_TPayment_Dtls_By_Errcode_v1   :  " + ex.Message);
            }
        }

        //public static IEnumerable<CRM_API.Models.financeviewtransdownload> getpaymentdetailsdownloads_new(string Transation_Status, string Rejection_Status, string Product_Code, string Customer_Name, string payment_Ref, string mobile_Number, string EmailAddress, string ICCID, string startdate, string enddate)
        //{
        //    try
        //    {
        //        using (var conn = new SqlConnection(CRM_API.Helpers.Config.PaymentConnection))
        //        {
        //            conn.Open();
        //            var result = conn.Query<CRM_API.Models.financeviewtransdownload>("SP_Get_TPayment_Dtls_By_Errcode_v1", new
        //            {

        //                @Product_Code = Product_Code,
        //                @V_Trans_Status = Transation_Status,
        //                @V_Rejected_Status = Rejection_Status,
        //                @CustName = Customer_Name,
        //                @Mobile_No = mobile_Number,
        //                @Payment_Ref = payment_Ref,
        //                @Email = EmailAddress,
        //                @Iccid =ICCID,
        //                @Fromdate = startdate,
        //                @Todate =enddate

        //            }, commandTimeout: (5 * 60), commandType: CommandType.StoredProcedure);
        //            return result ?? new List<CRM_API.Models.financeviewtransdownload>();
        //        }
        //    }
        //    catch (SqlException ex)
        //    {
        //        throw new Exception("SP_Get_TPayment_Dtls_By_Errcode_v1   :  " + ex.Message);
        //    }
        //}
        public static IEnumerable<CRM_API.Models.Lotgsearch> getlotgsearch()
        {
            
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultMconnection))
                {
                    
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.Lotgsearch>("crm3_Number_Pool_Default_Country_view", new { }, commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.Lotgsearch>();
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("c3pm_get_operation_files   : " +  ex.Message);
            }
        }



        public static IEnumerable<CRM_API.Models.Lotgwisesearch> Lotgpsearch(string id, string value)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultMconnection))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.Lotgwisesearch>("Crm3_Number_Pool_Action_ViewByProduct_v2", new
                    
                    {
                        @type =id,
                        @value = value 
                    
                    
                    }, commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.Lotgwisesearch>();
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("c3pm_get_operation_files   :  " + ex.Message);
            }
        }



        public static IEnumerable<CRM_API.Models.Lotgproductview> Lotgproductview()
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultMconnection))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.Lotgproductview>("product_based_llom_v2", new { }, commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.Lotgproductview>();
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("c3pm_get_operation_files   :  " + ex.Message);
            }
        }


        public static IEnumerable<CRM_API.Models.lotgcityview> lottmcityview()
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultMconnection))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.lotgcityview>("city_based_llom", new { }, commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.lotgcityview>();
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("c3pm_get_operation_files   :  " + ex.Message);
            }
        }


        public static IEnumerable<CRM_API.Models.lotgcityview_v2> lottmcityview_v2(string product)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultMconnection))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.lotgcityview_v2>("city_based_llom1", new { 
                    
                    @product=product
                    }, commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.lotgcityview_v2>();
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("c3pm_get_operation_files   :  " + ex.Message);
            }
        }


        public static IEnumerable<CRM_API.Models.lotgproductview_v2> lottmproductview_v2(string product)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultMconnection))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.lotgproductview_v2>("product_based_LLOM1", new
                    {

                        @product = product
                    }, commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.lotgproductview_v2>();
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("c3pm_get_operation_files   :  " + ex.Message);
            }
        }



        public static IEnumerable<CRM_API.Models.lotgcountrytoproduct> lotgcountproduct(string country)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultMconnection))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.lotgcountrytoproduct>("country_based_llom", new
                    {
                        @country_name = country
                    }, commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.lotgcountrytoproduct>();
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("c3pm_get_operation_files   :  " + ex.Message);
            }
        }


        public static IEnumerable<CRM_API.Models.lotgcountrytocity> lotgcountcity(string country)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultMconnection))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.lotgcountrytocity>("country_based_city_list_for_all_product", new
                    {
                        @country_name = country
                    }, commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.lotgcountrytocity>();
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("c3pm_get_operation_files   :  " + ex.Message);
            }
        }


        public static IEnumerable<CRM_API.Models.lotgproductcityview> lottmproductcityview(string product, string city)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultMconnection))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.lotgproductcityview>("Crm3_Number_Pool_by_area_name_and_product_v1", new
                    {

                        @product = product,
                        @area_name=city
                    }, commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.lotgproductcityview>();
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("c3pm_get_operation_files   :  " + ex.Message);
            }
        }




        public static IEnumerable<CRM_API.Models.lotgproductcityview_v5> lottmproductcityview_v5(string product, string city)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultMconnection))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.lotgproductcityview_v5>("LLOM_number_by_area_name_and_product", new
                    {

                        @product = product,
                        @area_name = city
                    }, commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.lotgproductcityview_v5>();
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("c3pm_get_operation_files   :  " + ex.Message);
            }
        }



        public static IEnumerable<CRM_API.Models.bugetvalues> BUsearch(string mobileno)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.MCMESPConnection))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.bugetvalues>("sim_block_unblock_all_log_display", new
                    {

                        @mobileno = mobileno                       
                    }, commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.bugetvalues>();
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("c3pm_get_operation_files   :  " + ex.Message);
            }
        }


        public static IEnumerable<CRM_API.Models.bubgetvalues> BUBsearch(string mobileno,string user,string status,string reason)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.MCMESPConnection))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.bubgetvalues>("sim_block_unblock_all_log", new
                    {

                        @mobileno = mobileno,
                        @user=user,
                        @reason=reason,
                        @block_flag=status
                    }, commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.bubgetvalues>();
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("c3pm_get_operation_files   :  " + ex.Message);
            }
        }



        public static IEnumerable<CRM_API.Models.lotgareanameid> lottmproductareaid(string area, string id)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultMconnection))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.lotgareanameid>("Crm3_Number_Pool_by_area_name_ID", new
                    {

                        @area_name = area,
                        @area_code = id
                    }, commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.lotgareanameid>();
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("c3pm_get_operation_files   :  " + ex.Message);
            }
        }


        public static List<CRM_API.Models.mobvalidation> lottmmobvalidation(string mob)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultMconnection))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.mobvalidation>("LLOTG_number_validation", new
                    {

                        @vectone_number = mob                      
                    }, commandType: CommandType.StoredProcedure);
                    return result.ToList() ?? new List<CRM_API.Models.mobvalidation>();
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("c3pm_get_operation_files   :  " + ex.Message);
            }
        }


        public static IEnumerable<CRM_API.Models.lotgproductcityview_v2> lottmproductcityview_v2(string product, string city)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultMconnection))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.lotgproductcityview_v2>("Crm3_Number_Pool_by_area_name_and_product", new
                    {

                        @product = product,
                        @area_name = city
                    }, commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.lotgproductcityview_v2>();
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("c3pm_get_operation_files   :  " + ex.Message);
            }
        }


        public static IEnumerable<CRM_API.Models.lotgproductcityview_v2> lottmproductcityview_v4(string city)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultMconnection))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.lotgproductcityview_v2>("Crm3_Number_Pool_by_area_name_v1", new
                    {                       
                        @area_name = city
                    }, commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.lotgproductcityview_v2>();
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("c3pm_get_operation_files   :  " + ex.Message);
            }
        }



        public static IEnumerable<CRM_API.Models.lotgproductcityview_v2> lottmproductcityview_v3(string product)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultMconnection))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.lotgproductcityview_v2>("Crm3_Number_Pool_by_product", new
                    {

                        @product = product
                      
                    }, commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.lotgproductcityview_v2>();
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("c3pm_get_operation_files   :  " + ex.Message);
            }
        }





        public static IEnumerable<CRM_API.Models.lotgmapping> lottmmaping(string llomno, string vectoneno)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultMconnection))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.lotgmapping>("reserve_vectone_number_to_llom_number", new
                    {

                        @LLOM_number = llomno,
                        @vectone_number = vectoneno
                    }, commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.lotgmapping>();
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("c3pm_get_operation_files   :  " + ex.Message);
            }
        }


        public static IEnumerable<CRM_API.Models.lotgmapping> lottunmaping(string llomno, string vectoneno)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultMconnection))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.lotgmapping>("unmap_vectone_number", new
                    {

                        @LLOM_number = llomno,
                        @vectone_number = vectoneno
                    }, commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.lotgmapping>();
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("c3pm_get_operation_files   :  " + ex.Message);
            }
        }

        public static IEnumerable<CRM_API.Models.financeviewtrans> geterrrCode(string spName, string paymentCode)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.PaymentConnection))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.financeviewtrans>(spName, new
                    {
                        @err_code = paymentCode
                    }, commandType: CommandType.StoredProcedure
                    );

                    return result ?? new List<CRM_API.Models.financeviewtrans>();
                }
            }
            catch (SqlException ex)
            {
                throw new Exception(spName + "   :  " + ex.Message);
            }
        }


        public static IEnumerable<CRM_API.Models.financeviewtransdownload> geterrrCodedownload(string spName, string paymentCode)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.PaymentConnection))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.financeviewtransdownload>(spName, new
                    {
                        @err_code = paymentCode
                    }, commandTimeout: (5 * 60), commandType: CommandType.StoredProcedure
                    );

                    return result ?? new List<CRM_API.Models.financeviewtransdownload>();
                }
            }
            catch (SqlException ex)
            {
                throw new Exception(spName + "   :  " + ex.Message);
            }
        }


        public static IEnumerable<CRM_API.Models.financeviewtransdownload> getserrrCodedownload(string spName, string paymentCode,string date)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.PaymentConnection))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.financeviewtransdownload>(spName, new
                    {
                        @err_code = paymentCode,
                        @date=date
                    }, commandTimeout: (5 * 60), commandType: CommandType.StoredProcedure
                    );

                    return result ?? new List<CRM_API.Models.financeviewtransdownload>();
                }
            }
            catch (SqlException ex)
            {
                throw new Exception(spName + "   :  " + ex.Message);
            }
        }

        public static List<CRM_API.Models.financeviewtrans> getMerchentIDs(string MerchID)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.PaymentConnection))
                {
                    conn.Open();
                    var results = conn.Query<CRM_API.Models.financeviewtrans>(
                            "get_latest_details_from_tpayment_by_referenceid",
                            new
                            {
                                @REFERENCE_ID = MerchID

                            }, commandType: CommandType.StoredProcedure);
                    return results.ToList() ?? new List<CRM_API.Models.financeviewtrans>();
                }
            }
            catch (Exception ex)
            {
                return new List<CRM_API.Models.financeviewtrans>();
            }
        }



        public static IEnumerable<CRM_API.Models.getafterpaymentdetails> afterpayment(string MerchID, string reason, string statusref)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.PaymentConnection))
                {
                    conn.Open();
                    var results = conn.Query<CRM_API.Models.getafterpaymentdetails>(
                            "tcs_payment_insert",
                            new
                            {
                                @reference_id = MerchID,
                                @Reason = reason,
                                @statusref = statusref

                            }, commandType: CommandType.StoredProcedure);
                    return results.ToList() ?? new List<CRM_API.Models.getafterpaymentdetails>();
                }
            }
            catch (Exception ex)
            {
                return new List<CRM_API.Models.getafterpaymentdetails>();
            }
        }

        public static IEnumerable<CRM_API.Models.getafterpaymentdetails_ctopup> afterpayment_cancel(string MerchID,string accountid, string reason, string statusref)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var results = conn.Query<CRM_API.Models.getafterpaymentdetails_ctopup>(
                            "manual_cancel_topup",
                            new
                            {
                                @paymentRef = MerchID,
                                @mobileno=accountid,
                                @Reason = reason,
                                @statusref = statusref

                            }, commandType: CommandType.StoredProcedure);
                    return results.ToList() ?? new List<CRM_API.Models.getafterpaymentdetails_ctopup>();
                }
            }
            catch (Exception ex)
            {
                return new List<CRM_API.Models.getafterpaymentdetails_ctopup>();
            }
        }


        public static IEnumerable<CRM_API.Models.getafterpaymentdetails_topup> afterpayment_topup(string accountid, string MerchID, string amount, string reason, string statusref)
        {
            try
            {
                //Old SP Name:manual_topup_v4
                //Reason Top up issue so checking new sp:manual_topup_v6
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var results = conn.Query<CRM_API.Models.getafterpaymentdetails_topup>(
                            "manual_topup_v6",
                            new
                            {
                                @paymentRef = MerchID,
                                @mobileno = accountid,
                                @Reason = reason,
                                @statusref = statusref,
                                @amount = amount

                            }, commandType: CommandType.StoredProcedure);
                    return results.ToList() ?? new List<CRM_API.Models.getafterpaymentdetails_topup>();
                }
            }
            catch (Exception ex)
            {
                return new List<CRM_API.Models.getafterpaymentdetails_topup>();
            }
        }



        //public static IEnumerable<CRM_API.Models.getafterpaymentdetails_topup> afterpayment_topup(string accountid, string MerchID, string amount, string reason, string statusref)
        //{
        //    try
        //    {
        //        float famount = float.Parse(amount);
        //        using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
        //        {
        //            conn.Open();
        //            var p = new DynamicParameters();
        //            p.Add("@mobileno", accountid);
        //            p.Add("@paymentref", MerchID);
        //            p.Add("@Reason", reason);
        //            p.Add("@statusref", statusref);
        //            p.Add("@amount", famount);
        //            p.Add("@errcode", dbType: DbType.Int32, direction: ParameterDirection.Output);
        //            p.Add("@errmsg", dbType: DbType.String,size: 200, direction: ParameterDirection.Output);
        //            p.Add("@incentif_data", dbType: DbType.String, size: 200, direction: ParameterDirection.Output);
        //            p.Add("@total_notif", dbType: DbType.String, size: 200, direction: ParameterDirection.Output);
        //            p.Add("@freecredit", dbType: DbType.String,size: 200, direction: ParameterDirection.Output);
        //            p.Add("@sms_sender", dbType: DbType.String,size: 200, direction: ParameterDirection.Output);
        //            p.Add("@sms_text", dbType: DbType.String,size: 500, direction: ParameterDirection.Output);
        //            p.Add("@afterbal1", dbType: DbType.Decimal,direction: ParameterDirection.Output);
        //            p.Add("@update_date", dbType: DbType.DateTime,direction: ParameterDirection.Output);
        //            p.Add("@prevbal1", dbType: DbType.Decimal, direction: ParameterDirection.Output);
        //            p.Add("@reference_id", dbType: DbType.String, size: 200, direction: ParameterDirection.Output);
        //            p.Add("@reason1", dbType: DbType.String, size: 200, direction: ParameterDirection.Output);
        //            p.Add("@status", dbType: DbType.String, size: 200, direction: ParameterDirection.Output);
        //            conn.Execute("manual_topup_v1",
        //                   p,
        //                   commandType: CommandType.StoredProcedure);

        //            List<CRM_API.Models.getafterpaymentdetails_topup> result = new List<CRM_API.Models.getafterpaymentdetails_topup>();
        //            result[0].errcode = p.Get<int>("@errcode");
        //            result[0].sms_sender = p.Get<string>("@sms_sender");
        //            result[0].sms_text = p.Get<string>("@sms_text");
        //            result[0].afterbal = p.Get<decimal>("@afterbal1");
        //            result[0].prevbal = p.Get<decimal>("@prevbal1");
        //            result[0].REFERENCE_ID = p.Get<string>("@reference_id");
        //            result[0].reason = p.Get<string>("@reason1");
        //            result[0].status = p.Get<string>("@status");
        //            result[0].update_date = p.Get<DateTime>("@update_date");



        //            return result ?? new List<CRM_API.Models.getafterpaymentdetails_topup>();
        //        }
        //    }
        //    catch (Exception ex)
        //    {               
        //        throw new Exception("BL MyAccountLogin: " + ex.Message);
        //    }
        //}



        public static bool getTransaction(string url)
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
            req.UserAgent = "Mozilla";
            //req.Headers["user"] = "tester";
            req.Method = "GET";
            req.Timeout = 180000;
            try
            {
                HttpWebResponse res = (HttpWebResponse)req.GetResponse();
                Stream resStream = res.GetResponseStream();
                StreamReader reader = new StreamReader(resStream);
                string result = reader.ReadToEnd();
                res.Close();
                string[] splitresult = result.Split('=');
                if (splitresult[3].ToString().Contains("100"))
                    return true;
                else
                    return false;
                //XmlDocument doc = new XmlDocument();
                //doc.LoadXml(result);
                //DataSet ds = new DataSet();
                //XmlNodeReader xnodereader = new XmlNodeReader(doc);
                //ds.ReadXml(xnodereader);
                //if (ds != null)
                //{
                //    if (ds.Tables.Count > 0)
                //        return ds.Tables[0].AsEnumerable().Select(row => new CRM_API.Models.cardResult
                //         {
                //             Decision = row.Field<string>("Decision"),
                //             MerchantReferenceCode = row.Field<string>("MerchantReferenceCode"),
                //             ReasonCode = row.Field<string>("ReasonCode"),
                //             RequestID = row.Field<string>("RequestID"),
                //             RequestToken = row.Field<string>("RequestToken")
                //         }).ToList();
                //}
                
            }
            catch (Exception ex)
            {

            }
            return false;
            //return new List<CRM_API.Models.cardResult>();
        }

        //05-Feb-2019 : Moorthy : Added for auto Topup Report
        public static IEnumerable<CRM_API.Models.AutotopupReport> GetAutoTopupReport(string date)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.PaymentConnection))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.AutotopupReport>("sp_autotupup_report", new
                    {
                        @date = date

                    }, commandTimeout: (5 * 60), commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.AutotopupReport>();
                }
            }
            catch (SqlException ex)
            {
                Log.Error("GetAutoTopupReport   :  " + ex.Message);
                return new List<CRM_API.Models.AutotopupReport>();
            }
        }

        //12-Feb-2019 : Moorthy : Added for General Report
        public static IEnumerable<CRM_API.Models.GeneralReport> GetGeneralReport(string date)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.ESPMCMConnection))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.GeneralReport>("crm3_get_payment_transaction_detils", new
                    {
                        @from_date = date,
                        @todate = date
                    }, commandTimeout: (5 * 60), commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.GeneralReport>();
                }
            }
            catch (SqlException ex)
            {
                Log.Error("GetGeneralReport   :  " + ex.Message);
                return new List<CRM_API.Models.GeneralReport>();
            }
        }
    }
}
