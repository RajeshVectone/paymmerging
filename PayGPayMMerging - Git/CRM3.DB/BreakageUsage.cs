﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;
using CRM_API.Models.BreakageUsageModels;

namespace CRM_API.DB.BreakageUsage
{
    public class BreakageUsage
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public static IEnumerable<BreakageUsageOutput> GetBreakageUsageInfo(BreakageUsageInput req)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<BreakageUsageOutput>(
                            "crm_get_gprs_breakege_usage", new
                            {
                                @mobileno = req.mobileno,
                                @sitecode = req.sitecode,
                                @date_fr = req.date_fr,
                                @date_to = req.date_to
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<BreakageUsageOutput>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL GetBreakageUsageInfo: " + ex.Message);
            }
        }
    }
}
