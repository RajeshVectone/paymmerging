﻿using CRM_API.Models.CountrySaver;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;

namespace CRM_API.DB.CLI
{

    public class SProc
    {

        //private static string SP_GETLISTCLI_CS = "crm3_countrysaver_getlist"; 
        //private static string SP_GETLISTCLI_CS = "REGISTRATIONS_SELECT_VIEWDETAILS";
        //02-Jan-2015 : Moorthy : Added to show the Additional CLI Details
        private static string SP_GETADDITIONALCLI_CS = "crm3_crm_get_Addon_list";
        private static string SP_GETLISTCLI_CS = "REGISTRATIONS_SELECT_VIEWDETAILS_V1";
        private static string SP_GET_REGID = "COMMON_SELECT";
        private static string SP_SUBMIT_REACTIVATE = "crm3_countrysaver_reactivate";


        private static int GetRegIDForCountrySaver(CRM_API.Models.CountrySaver.CountrySaverList countrySaver)
        {
            using (var conn = new SqlConnection(CRM_API.Helpers.Config.PortalConnection))
            {
                conn.Open();
                String paramString = "";
                if (countrySaver.DestinationNumber.IndexOf("Srilanka") >= 0)
                {
                    paramString = "SRI-LANKA" + "|" + countrySaver.AccountNumber;
                }
                else if (countrySaver.DestinationNumber.IndexOf("Philippines") >= 0)
                {
                    paramString = "PHILIPPINES" + "|" + countrySaver.AccountNumber;
                }

                //Get RegID                    
                var result = conn.Query<dynamic>(SP_GET_REGID, new
                    {
                        @ParamID = 0,
                        @ParamString =  paramString,
                        @ActionType = 8                            
                    }, commandType: CommandType.StoredProcedure
                ).FirstOrDefault<dynamic>();              
                    
                //Get RegID
                                        
                
                int regid = int.Parse(result.REG_ID.ToString());
                return regid;
            }
        }

        public static String GetFandFNoCountrySaver(String destinationNumber)
        {
            String paramString = "";
            paramString = destinationNumber.Replace("Srilanka ", "").Replace("Philippines ", "");

            if (paramString.Substring(0, 2) != "00")
            {
                paramString = "00" + paramString;
            }
            return paramString;
        }

        //02-Jan-2015 : Moorthy : Added to show the Additional CLI Details
        public static IEnumerable<AdditionalCLI> GetAdditionalCLI(AdditionalReqInfo Model)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {

                    var resultcli = conn.Query<AdditionalCLI>(SP_GETADDITIONALCLI_CS, new
                    {
                        @mobileno = Model.Mobileno,
                        @sitecode = Model.Sitecode
                    }, commandType: CommandType.StoredProcedure
                    );
                    return resultcli ?? new List<AdditionalCLI>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("GetAdditionalCLI: " + ex.Message);
            }
        }

        public static IEnumerable<AdditionalCLI> GetListCLINew(AdditionalReqInfo Modal)
        {
            try
            {
                //int regid = GetRegIDForCountrySaver(countrySaver);
                //String paramString = GetFandFNoCountrySaver(countrySaver.DestinationNumber);
                    using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {

                    var resultcli = conn.Query<AdditionalCLI>("crm3_get_fsb_addition_cli_list", new
                    {
                        @mobileno = Modal.Mobileno,
                        @sitecode = Modal.Sitecode
                    }, commandType: CommandType.StoredProcedure
                    );
                    return resultcli ?? new List<AdditionalCLI>();
                }

                //Find Account ID
                /*using (var conn = new SqlConnection(CRM_API.Helpers.Config.PortalConnection))
                {
                
                    var resultcli = conn.Query<AdditionalCLI>(SP_GETLISTCLI_CS, new
                    {
                        @ParamID = regid,
                        @ParamString = paramString,
                        @ActionType=2,
                        @CLI_Number = countrySaver.Account_ID                      
                    }, commandType: CommandType.StoredProcedure
                    );

                    return resultcli ?? new List<AdditionalCLI>();
                }*/
            }
            catch (Exception ex)
            {
                throw new Exception("GetListCLI: " + ex.Message);
            }
        }

        //public static IEnumerable<AdditionalCLI> GetListCLI_New(CRM_API.Models.CountrySaver.CountrySaverList countrySaver)
        //{
        //    try
        //    {
        //        int regid = GetRegIDForCountrySaver(countrySaver);
        //        String paramString = GetFandFNoCountrySaver(countrySaver.DestinationNumber);
        //        //Find Account ID
        //        using (var conn = new SqlConnection(CRM_API.Helpers.Config.PortalConnection))
        //        {

        //            var resultcli = conn.Query<AdditionalCLI>(SP_GETLISTCLI_CS, new
        //            {
        //                @ParamID = regid,
        //                @ParamString = paramString,
        //                @ActionType = 2,
        //                @CLI_Number = countrySaver.Account_ID
        //            }, commandType: CommandType.StoredProcedure
        //            );

        //            return resultcli ?? new List<AdditionalCLI>();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception("GetListCLI: " + ex.Message);
        //    }
        //}


        public static IEnumerable<AdditionalCLI> GetListCLI(AdditionalReqInfo Model)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<AdditionalCLI>(SP_GETLISTCLI_CS, new
                    {
                        @mobileno = Model.Mobileno,
                        @sitecode = Model.Sitecode
                    }, commandType: CommandType.StoredProcedure
                    );


                    return result ?? new List<AdditionalCLI>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("GetListCLI: " + ex.Message);
            }
        }

        public static Models.Common.ErrCodeMsg AddCLI(AdditionalReqInfo Model)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var p = new DynamicParameters();
                    p.Add("@reg_idx", Convert.ToInt32(Model.RegIDX));
                    p.Add("@sitecode", Model.Sitecode);
                    p.Add("@errcode", dbType: DbType.Int32, size: 3, direction: ParameterDirection.Output);
                    p.Add("@errmessage", dbType: DbType.String, size: 255, direction: ParameterDirection.Output);
                    p.Add("@paymode", Convert.ToInt32(Model.Paymode));
                    conn.Execute(SP_SUBMIT_REACTIVATE, p, null, null, CommandType.StoredProcedure);

                    Model.errcode = p.Get<Int32>("errcode");
                    //Model.errcode = Convert.ToInt32(p.Get<string>("errcode"));      // param in DB varchar
                    Model.errmsg = p.Get<string>("errmessage");

                    return new Models.Common.ErrCodeMsg()
                    {
                        errcode = Model.errcode,
                        errmsg = Model.errmsg
                    };
                }

            }
            catch (Exception ex)
            {
                return new Models.Common.ErrCodeMsg()
                {
                    errcode = -1,
                    errmsg = ex.Message
                };
            }
        }

        public static Boolean ValidateCLI(String CLINumber)
        {
            //Log.Info("Start CSB_HSB.ValidateCLI: {0}", CLINumber);
            //using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
            using (var conn = new SqlConnection(Helpers.Config.PortalConnection))
            {
                conn.Open();
                string sql = @"CHECK_CLI_EXISTS";
                var result = conn.Query<ErrCodeMsgCLIExist>(sql, new
                {
                    @CLI = CLINumber

                }, commandType: CommandType.StoredProcedure).ToList().FirstOrDefault();


                if (result.ErrCode == 0)
                {
                    //if (String.Compare(result.cli_country, "OTHER MOBILE") == 0)
                    //{
                    //    return false;
                    //}
                    //else
                    //{ 
                    return true;
                    //}
                }
                else
                { return false; }
            }
            //Log.Info("End CSB_HSB.ValidateCLI: {0}", CLINumber);
        }

        public static bool ValidateCLIPrefix(string FFNumber, string CLI, string Brand, string CLINumber)
        {
            if (!(String.IsNullOrEmpty(Brand)) && String.Compare(Brand.Substring(0, 3), "CTP") == 0 && CLI.Length == CLINumber.Length)
            {
                return true;
            }
            if (!(String.IsNullOrEmpty(CLI)) && !(String.IsNullOrEmpty(CLINumber)) && String.Compare(CLI.Substring(0, 2), CLINumber.Substring(0, 2)) == 0 && CLI.Length == CLINumber.Length)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public static AdditionalCLI GetSubscriberVectoneCLI(String subscriberId, String FFNumber, String CLI, int Action)
        {
            using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
            {
                conn.Open();
                string sql = @"REGISTRATIONS_SELECT_VIEWDETAILS";
                var result = conn.Query<AdditionalCLI>(sql, new
                {
                    @ParamID = subscriberId,
                    @ParamString = FFNumber,
                    @ActionType = Action,
                    @CLI_Number = CLI

                }, commandType: CommandType.StoredProcedure).FirstOrDefault<AdditionalCLI>();
                return result;
            }
        }


        public static List<AdditionalCLI> GetSubscriberVectoneCLIDetails(String subscriberId, String FFNumber, String CLI, int Action)
        {
            using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
            {
                conn.Open();
                string sql = @"REGISTRATIONS_SELECT_VIEWDETAILS";
                var result = conn.Query<AdditionalCLI>(sql, new
                {
                    @ParamID = subscriberId,
                    @ParamString = FFNumber,
                    @ActionType = Action,
                    @CLI_Number = CLI

                }, commandType: CommandType.StoredProcedure).ToList();
                return result;
            }
        }
        // creates a Subscriber object based on DataReader

        public static Models.Common.ErrCodeMsg InsertSubscriberVectoneCLIDetail(AdditionalCLI subscriberVectoneCLI)
        {
            String subscriberId;
            String FFNumber;
            String CLI;
            String CLI_PIN;
            String CLI_Type;
            String sql;
            DateTime create_date;
            //int Action;
            CLI = subscriberVectoneCLI.CLI;
            CLI_PIN = subscriberVectoneCLI.Sitecode;
            create_date = subscriberVectoneCLI.SubscriptionDate;
            //CLI_Type = "Mobile";
            CLI_Type = subscriberVectoneCLI.Product_Type;
            sql = @"REGISTRATIONS_INSERT_CLI";
            object[] parms = { "@CLI", CLI, "@CLI_PIN", CLI_PIN, "@TYPE", CLI_Type };
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var p = new DynamicParameters();
                    p.Add("@CLI", CLI);
                    p.Add("@CLI_PIN", CLI_PIN);
                    //p.Add("@errcode", dbType: DbType.Int32, size: 3, direction: ParameterDirection.Output);
                    //p.Add("@errmessage", dbType: DbType.String, size: 255, direction: ParameterDirection.Output);
                    p.Add("@TYPE", CLI_Type);
                    conn.Execute(sql, p, null, null, CommandType.StoredProcedure);

                    subscriberVectoneCLI.errcode = p.Get<Int32>("errcode");
                    //Model.errcode = Convert.ToInt32(p.Get<string>("errcode"));      // param in DB varchar
                    subscriberVectoneCLI.errmsg = p.Get<string>("errmessage");

                    return new Models.Common.ErrCodeMsg()
                    {
                        errcode = subscriberVectoneCLI.errcode,
                        errmsg = subscriberVectoneCLI.errmsg
                    };
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static Models.Common.ErrCodeMsg DeleteSubscriberVectoneCLIDetail(AdditionalCLI subscriberVectoneCLI)
        {
            String CLI;
            String CLI_PIN;
            String CLI_Type = "";
            String sql;
            DateTime create_date;
            //CLI_PIN = CLI;
            //int Action;            
            CLI = subscriberVectoneCLI.CLI;
            CLI_PIN = subscriberVectoneCLI.Sitecode;
            create_date = subscriberVectoneCLI.SubscriptionDate;
            //CLI_Type = "Mobile";
            CLI_Type = subscriberVectoneCLI.Product_Type;
            sql = @"REGISTRATIONS_DELETE_CLI";
            //object[] parms = { "@CLI", CLI, "@CLI_PIN ", CLI_PIN, "@TYPE", CLI_Type };
            //db.Delete(sql, parms);
            //object[] parms = { "@CLI", CLI, "@CLI_PIN", CLI_PIN, "@TYPE", CLI_Type };
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var p = new DynamicParameters();
                    p.Add("@CLI", CLI);
                    p.Add("@CLI_PIN", CLI_PIN);
                    //p.Add("@errcode", dbType: DbType.Int32, size: 3, direction: ParameterDirection.Output);
                    //p.Add("@errmessage", dbType: DbType.String, size: 255, direction: ParameterDirection.Output);
                    p.Add("@TYPE", CLI_Type);
                    conn.Execute(sql, p, null, null, CommandType.StoredProcedure);

                    subscriberVectoneCLI.errcode = p.Get<Int32>("errcode");
                    //Model.errcode = Convert.ToInt32(p.Get<string>("errcode"));      // param in DB varchar
                    subscriberVectoneCLI.errmsg = p.Get<string>("errmessage");

                    return new Models.Common.ErrCodeMsg()
                    {
                        errcode = subscriberVectoneCLI.errcode,
                        errmsg = subscriberVectoneCLI.errmsg
                    };
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
