﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;

namespace CRM_API.DB.SMSCdr
{
    public class SProc
    {
        public static IEnumerable<CRM_API.Models.CDR.SMSHistory> GetSMSCDR(string mobileno, DateTime dateFrom, DateTime dateUntil, string sitecode, int pagenumber = 1, int rowperpage = 10)
        {
            var subsInfo = CRM_API.DB.Customer.SProc.SubscriberInfo(mobileno, sitecode);
            int rCount = GetCountSMSCDR(mobileno, dateFrom, dateUntil, sitecode);
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.CDR.SMSHistory>(
                            "crm3_rp1_getsmsrecordbydaterange", new
                            {
                                @telcocode = "ICS",
                                @custcode = subsInfo.custcode,
                                @batchcode = subsInfo.batchcode,
                                @serialcode = subsInfo.serialcode,
                                @startdate = dateFrom.ToString("dd MMM yyyy"),
                                @enddate = dateUntil.ToString("dd MMM yyyy"),
                                @rowsperpage = rCount,
                                @pagenumber = 1,
                                @sitecode = sitecode
                            },null,true,(5*60*60), commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.CDR.SMSHistory>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL GetSMSCDR: " + ex.Message);
            }
        }
        public static int GetCountSMSCDR(string mobileno, DateTime dateFrom, DateTime dateUntil, string sitecode)
        {
            int result = 0;
            var subsInfo = CRM_API.DB.Customer.SProc.SubscriberInfo(mobileno, sitecode);
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    result = conn.Query<CRM_API.Models.CDR.SMSHistory>(
                            "crm3_rp1_countsmsrecordbydaterange", new
                            {
                                @telcocode = "ICS",
                                @custcode = subsInfo.custcode,
                                @batchcode = subsInfo.batchcode,
                                @serialcode = subsInfo.serialcode,
                                @startdate = dateFrom.ToString("dd MMM yyyy"),
                                @enddate = dateUntil.ToString("dd MMM yyyy"),
                                @sitecode = sitecode
                            }, null, true, (5 * 60 * 60), commandType: CommandType.StoredProcedure).Select(s => s.Result).FirstOrDefault();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL SubscriberInfo: " + ex.Message);
            }
        }
    }
}
