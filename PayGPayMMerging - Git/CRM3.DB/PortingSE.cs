﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Dapper;
using System.Threading.Tasks;
using CRM_API.Models.Porting.SE;

    public class DonorSE
    {
        public string donorid { get; set; }
        public string donorname { get; set; }
        public string opr { get; set; }
    }

    public class PortingList
    {
        public string port_id { get; set; }
        public string msisdn { get; set; }
        public string bb_msisdn { get; set; }
        public DateTime port_datetime  { get; set; }
        public string subs_name { get; set; }
        public string contact_phone { get; set; }
        public string contact_email { get; set; }
        public string port_status_desc { get; set; }
        public DateTime last_update{ get; set; }
        public DateTime created_date { get; set; }
    }

    public class PortingView
    {
        public string port_status { get; set; }
        public string port_status_desc { get; set; }
        public string msisdn { get; set; }
        public string bb_msisdn { get; set; }
        public string dno_name { get; set; }
        public string dsp_name { get; set; }
        public string subs_pincin{ get; set; }
        public string subs_name { get; set; }
        public string subs_addr { get; set; }
        public string subs_postal { get; set; }
        public string subs_city { get; set; }
        public string contact_dept { get; set; }
        public string contact_person { get; set; }
        public string contact_phone { get; set; }
        public string contact_fax { get; set; }
        public string contact_email { get; set; }
        public string order_reject_cause_code { get; set; }
        public string order_reject_cause_expl { get; set; }
        public DateTime port_datetime { get; set; }
        public DateTime dno_cutoff_time { get; set; }
        public DateTime last_update { get; set; }
    }

namespace CRM_API.DB.Porting.SE
{
    public class SProc
    {
        private static string SP_GETLIST_PORTIN_BYPORTINDATE = "crm3_get_portinlist";
        private static string SP_GETLIST_PORTIN_BYREQUESTDATE = "crm3_get_portinlist_byreqdate";
        private static string SP_GETLIST_PORTIN_BYMSISDN = "crm3_get_portinlist_bymsisdn";

        private static string SP_GETLIST_PORTOUT_BYPORTINGDATE = "crm3_get_portoutlist_bydate";
        private static string SP_GETLIST_PORTOUT_BYMSISDN = "crm3_get_portoutlist_bymsisdn";
        private static string SP_SUBMIT_PORTIN_REQUEST = "crm3_submit_portin_request";
        private static string SP_GETLIST_DONOR = "crm3_get_mnpref";
        private static string SP_GETPORTING_DETAIL = "crm3_get_porting_detail";

        private static string SITECODE = "BSE";

        public static IEnumerable<DonorSE> GetDonor()
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<DonorSE>(SP_GETLIST_DONOR, new
                    {
                        @sitecode = SITECODE
                    }, commandType: CommandType.StoredProcedure
                    );

                    return result ?? new List<DonorSE>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("SE.GetDonorOperator: " + ex.Message);
            }
        }

        public static PortingView GetPortingDetail(string portingid)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<PortingView>(SP_GETPORTING_DETAIL, new
                    {
                        @sitecode = SITECODE,
                        @port_id = portingid
                    }, commandType: CommandType.StoredProcedure
                    );

                    if (result.Count() > 0)
                        return result.ElementAt(0);
                    else
                        return new PortingView();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("SE.GetPORTING DETAIL: " + ex.Message);
            }
        }

        public static IEnumerable<PortingList> GetPortInByPortinDate(PortinEntry Model)
        {
            try
            {
                //get the date part only

                DateTime datefrom = DateTime.ParseExact(Model.PortingDateFrom, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                DateTime dateend = DateTime.ParseExact(Model.PortingDateTo, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var result = conn.Query<PortingList>(SP_GETLIST_PORTIN_BYPORTINDATE, new
                    {
                        @sitecode = SITECODE,
                        @search_status = Model.PortingStatus,
                        @date_from = datefrom,
                        @date_end = dateend
                    }, commandType: CommandType.StoredProcedure
                    );

                    return result ?? new List<PortingList>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("SE.Get Port in By Porting Date: " + ex.Message);
            }
        }

        public static IEnumerable<PortingList> GetPortInByRequestDate(PortinEntry Model)
        {
            try
            {
                //get the date part only
                DateTime datefrom = DateTime.ParseExact(Model.RequestDateFrom, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                DateTime dateend = DateTime.ParseExact(Model.RequestDateTo, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var result = conn.Query<PortingList>(SP_GETLIST_PORTIN_BYREQUESTDATE, new
                    {
                        @sitecode = SITECODE,
                        @search_status = Model.PortingStatus,
                        @date_from = datefrom,
                        @date_end = dateend
                    }, commandType: CommandType.StoredProcedure
                    );

                    return result ?? new List<PortingList>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("SE.Get Port in By Request Date: " + ex.Message);
            }
        }

        public static IEnumerable<PortingList> GetPortInByMSISDN(PortinEntry Model)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var result = conn.Query<PortingList>(SP_GETLIST_PORTIN_BYMSISDN, new
                    {
                        @sitecode = SITECODE,
                        @msisdn = Model.MSISDN
                    }, commandType: CommandType.StoredProcedure
                    );

                    return result ?? new List<PortingList>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("SE.Get Port in By MSISDN : " + ex.Message);
            }
        }

        /*---------------------------------------PORTING OUT-------------------------------------*/
        public static IEnumerable<PortingList> GetPortOutByPortingDate(PortinEntry Model)
        {
            try
            {
                //get the date part only

                DateTime datefrom = DateTime.ParseExact(Model.PortingDateFrom, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                DateTime dateend = DateTime.ParseExact(Model.PortingDateTo, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var result = conn.Query<PortingList>(SP_GETLIST_PORTOUT_BYPORTINGDATE, new
                    {
                        @sitecode = SITECODE,
                        @search_status = Model.PortoutStatus,
                        @date_from = datefrom,
                        @date_end = dateend
                    }, commandType: CommandType.StoredProcedure
                    );

                    return result ?? new List<PortingList>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("SE.Get Port OUT By Porting Date: " + ex.Message);
            }
        }

        public static IEnumerable<PortingList> GetPortOutByMSISDN(PortinEntry Model)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var result = conn.Query<PortingList>(SP_GETLIST_PORTOUT_BYMSISDN, new
                    {
                        @sitecode = SITECODE,
                        @msisdn = Model.MSISDN
                    }, commandType: CommandType.StoredProcedure
                    );

                    return result ?? new List<PortingList>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("SE.Get Port OUT By MSISDN : " + ex.Message);
            }
        }

        public static Models.Common.ErrCodeMsg SubmitPortin(PortinEntry Model)
        {
            try
            {
                DateTime PortingDate = DateTime.ParseExact(Model.PortingTime, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var p = new DynamicParameters();
                    p.Add("@sitecode", SITECODE);
                    p.Add("@msisdn", Model.MSISDN);
                    p.Add("@bb_msisdn", Model.VectoneMSISDN);
                    p.Add("@porting_time", PortingDate);
                    p.Add("@porting_hour", Model.PortingHour);
                    p.Add("@dsp", Model.DSP);
                    p.Add("@pic", Model.PersonalCorporateID);
                    p.Add("@subs_name", Model.SubsName);
                    p.Add("@subs_addr", Model.SubsAddress);
                    p.Add("@subs_postal", Model.SubsPostal);
                    p.Add("@subs_city", Model.SubsCity);
                    p.Add("@contact_dept", Model.Department);
                    p.Add("@contact_person", Model.DeptPerson);
                    p.Add("@contact_phone", Model.DeptPhone);
                    p.Add("@contact_fax", Model.DeptFax);
                    p.Add("@contact_email", Model.DeptEmail);
                    p.Add("@crm_agent","");
                    p.Add("@port_id", dbType: DbType.String, size: 32, direction: ParameterDirection.Output);
                    p.Add("@err_code", dbType: DbType.String, size: 3, direction: ParameterDirection.Output);
                    p.Add("@err_msg", dbType: DbType.String, size: 255, direction: ParameterDirection.Output);
                    conn.Execute(SP_SUBMIT_PORTIN_REQUEST, p, null, null, CommandType.StoredProcedure);
                    
                    Model.errcode = Convert.ToInt32(p.Get<string>("err_code"));
                    Model.errmsg = p.Get<string>("err_msg");
                        
                    return new Models.Common.ErrCodeMsg()
                    {
                        errcode = Model.errcode,
                        errmsg = Model.errmsg
                    };
                }

            }
            catch (Exception ex)
            {
                return new Models.Common.ErrCodeMsg()
                {
                    errcode= -1,
                    errmsg = ex.Message
                };                
            }
        }
    }
}
