﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;

namespace CRM_API.DB.PAYGRecommendedBonus
{
    //Added by Elango for Jira CRMT-315 & 316 Start
    public class SProc
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public static IEnumerable<PAYGRBOverview> GetRBOverView(string User_Id, string Site_Code)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<PAYGRBOverview>(
                            "RB_Recommented_Referral_Bonus_Overview_SP", new
                            {
                                @User_Id = User_Id,
                                @Site_Code = Site_Code
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<PAYGRBOverview>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.PAYGRecommendedBonus.GetRBOverView: {0}", ex.Message);
                throw new Exception("CRM_API.DB.PAYGRecommendedBonus.GetRBOverView: " + ex.Message);
            }
        }

        public static IEnumerable<PAYGRBReferralHistory> GetRBReferralHistory(string User_Id, string Site_Code, string Referral_Status, string SIM_Status, string Recharge_Mode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<PAYGRBReferralHistory>(
                            "RB_Referral_History_Referral_List_SP", new
                            {
                                @User_Id = User_Id,
                                @Site_Code = Site_Code,
                                @Referral_Status = Referral_Status,
                                @SIM_Status = SIM_Status,
                                @Recharge_Mode = Recharge_Mode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<PAYGRBReferralHistory>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.PAYGRecommendedBonus.GetRBReferralHistory: {0}", ex.Message);
                throw new Exception("CRM_API.DB.PAYGRecommendedBonus.GetRBReferralHistory: " + ex.Message);
            }
        }

        public static IEnumerable<PAYGRBReferralDetailView> GetRBReferralDetailsView(string User_Id, string Site_Code, string Referral_Id)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<PAYGRBReferralDetailView>(
                            "RB_Referral_List_Detail_Fetch_SP", new
                            {
                                @Referral_Id = Referral_Id,
                                @User_Id = User_Id,
                                @Site_Code = Site_Code
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<PAYGRBReferralDetailView>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.PAYGRecommendedBonus.GetRBReferralDetailsView: {0}", ex.Message);
                throw new Exception("CRM_API.DB.PAYGRecommendedBonus.GetRBReferralDetailsView: " + ex.Message);
            }
        }

        public static IEnumerable<PAYGRBReferralDetailView> GetRBReferralDetailsUpdate(string User_Id, string Site_Code, string Referral_Id, string Credit_Status_Desc, string SIM_Status_Desc, string User_Status_Desc)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<PAYGRBReferralDetailView>(
                            "RB_Referral_List_Detail_Update_SP", new
                            {
                                @User_Id = User_Id,
                                @Site_Code = Site_Code,
                                @Referral_Id = Referral_Id,
                                @Credit_Status_Desc = Credit_Status_Desc,
                                @SIM_Status_Desc = SIM_Status_Desc,
                                @User_Status_Desc = User_Status_Desc
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<PAYGRBReferralDetailView>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.PAYGRecommendedBonus.GetRBReferralDetailsUpdate: {0}", ex.Message);
                throw new Exception("CRM_API.DB.PAYGRecommendedBonus.GetRBReferralDetailsUpdate: " + ex.Message);
            }
        }

        public static IEnumerable<PAYGRBReferral_ReferralStatus> BindReferralStatus(string User_Id, string Site_Code)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<PAYGRBReferral_ReferralStatus>(
                            "RTM_Load_Referral_Status_Dropdown_SP", new
                            {
                                @User_Id = User_Id,
                                @Site_Code = Site_Code
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<PAYGRBReferral_ReferralStatus>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.PAYGRecommendedBonus.BindReferralStatus: {0}", ex.Message);
                throw new Exception("CRM_API.DB.PAYGRecommendedBonus.BindReferralStatus: " + ex.Message);
            }
        }

        public static IEnumerable<PAYGRBReferral_ReferralSimStatus> BindReferralSimStatus(string User_Id, string Site_Code)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<PAYGRBReferral_ReferralSimStatus>(
                            "RTM_Load_SIM_Status_Dropdown_SP", new
                            {
                                @User_Id = User_Id,
                                @Site_Code = Site_Code
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<PAYGRBReferral_ReferralSimStatus>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.PAYGRecommendedBonus.BindReferralSimStatus: {0}", ex.Message);
                throw new Exception("CRM_API.DB.PAYGRecommendedBonus.BindReferralSimStatus: " + ex.Message);
            }
        }

        public static IEnumerable<PAYGRBReferral_RechargeMode> BindRechargeMode(string User_Id, string Site_Code)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<PAYGRBReferral_RechargeMode>(
                            "RTM_Load_Recharge_Mode_Dropdown_SP", new
                            {
                                @User_Id = User_Id,
                                @Site_Code = Site_Code
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<PAYGRBReferral_RechargeMode>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.PAYGRecommendedBonus.BindRechargeMode: {0}", ex.Message);
                throw new Exception("CRM_API.DB.PAYGRecommendedBonus.BindRechargeMode: " + ex.Message);
            }
        }

        public static IEnumerable<PAYGRBTransaction_Transfer_Type> BindTransaction_Transfer_Type(string Site_Code)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<PAYGRBTransaction_Transfer_Type>(
                            "RTM_Load_Transfer_Type_Dropdown_SP", new
                            {
                                @Site_Code = Site_Code
                            },
                            commandType: CommandType.StoredProcedure);

                    return result ?? new List<PAYGRBTransaction_Transfer_Type>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.PAYGRecommendedBonus.BindTransaction_Transfer_Type: {0}", ex.Message);
                throw new Exception("CRM_API.DB.PAYGRecommendedBonus.BindTransaction_Transfer_Type: " + ex.Message);
            }
        }

        public static IEnumerable<PAYGRBTransaction_Payment_Status> BindTransaction_Payment_Status(string Site_Code)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<PAYGRBTransaction_Payment_Status>(
                            "RTM_Load_Payment_Status_Dropdown_SP", new
                            {
                                @Site_Code = Site_Code
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<PAYGRBTransaction_Payment_Status>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.PAYGRecommendedBonus.BindTransaction_Payment_Status: {0}", ex.Message);
                throw new Exception("CRM_API.DB.PAYGRecommendedBonus.BindTransaction_Payment_Status: " + ex.Message);
            }
        }

        public static IEnumerable<PAYGRBTransactionHistory> GetRBTransactionHistory(string User_Id, string Site_Code, string Transfer_Type, string Payment_Status)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<PAYGRBTransactionHistory>(
                            "RB_Transaction_History_List_SP", new
                            {
                                @User_Id = User_Id,
                                @Site_Code = Site_Code,
                                @Transfer_Type = Transfer_Type == "" ? null : Transfer_Type,
                                @Payment_Status = Payment_Status == "" ? null : Payment_Status
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<PAYGRBTransactionHistory>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.PAYGRecommendedBonus.GetRBReferralHistory: {0}", ex.Message);
                throw new Exception("CRM_API.DB.PAYGRecommendedBonus.GetRBReferralHistory: " + ex.Message);
            }
        }

        public static IEnumerable<PAYGRBTransactionDetailView> GetRBTransactionDetailsView(string User_Id, string Site_Code, string Trans_Id)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<PAYGRBTransactionDetailView>(
                            "RB_Transaction_List_Detail_Fetch_SP", new
                            {
                                @User_Id = User_Id,
                                @Site_Code = Site_Code,
                                @Trans_Id = Trans_Id
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<PAYGRBTransactionDetailView>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.PAYGRecommendedBonus.GetRBTransactionDetailsView: {0}", ex.Message);
                throw new Exception("CRM_API.DB.PAYGRecommendedBonus.GetRBTransactionDetailsView: " + ex.Message);
            }
        }

        public static IEnumerable<PAYGRBTransactionDetailView> GetRBTransactionDetailsUpdate(string User_Id, string Site_Code, string Trans_Id, string Transfer_Status_Desc, string Payment_Status_Desc, string Payment_Status)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<PAYGRBTransactionDetailView>(
                            "RB_Transaction_Detail_Update_SP", new
                            {
                                @Trans_Id = Convert.ToInt32(Trans_Id),
                                @User_Id = User_Id,
                                @Site_Code = Site_Code,
                                @Transfer_Status_Desc = Transfer_Status_Desc,
                                @Payment_Status = Payment_Status,
                                @Payment_Status_Desc = Payment_Status_Desc
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<PAYGRBTransactionDetailView>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.PAYGRecommendedBonus.GetRBTransactionDetailsUpdate: {0}", ex.Message);
                throw new Exception("CRM_API.DB.PAYGRecommendedBonus.GetRBTransactionDetailsUpdate: " + ex.Message);
            }
        }
    }
}
