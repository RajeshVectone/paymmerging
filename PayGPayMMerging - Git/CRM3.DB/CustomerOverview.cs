﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;

namespace CRM_API.DB.Customer
{
    public class CustomerOverview
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public static CustomerOverviewPageOutput GetCustomerOverview(string mobileno, string sitecode)
        {            
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<CustomerOverviewPageOutput>(
                            "crm3_customer_overview_detail", new
                            {
                                @mobileno = mobileno,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    return result ?? new CustomerOverviewPageOutput();
                }
            }
            catch (Exception ex)
            {
                Log.Error("BL GetCustomerOverview: {0}", ex.Message);
                throw new Exception("BL GetCustomerOverview: " + ex.Message);
            }
        }
    }
}