﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;

namespace CRM_API.DB.PaymentProfile
{
    public class SProc
    {
        public static IEnumerable<CRM_API.Models.CustomerProfile.PaymentProfileCustomer> GetListProfile(string mobileno, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.CustomerProfile.PaymentProfileCustomer>(
                            "crm3_get_customer_payment_profile", new
                            {
                                @msisdn = mobileno,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.CustomerProfile.PaymentProfileCustomer>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("CRM_API.DB.PaymentProfile.GetListProfile: " + ex.Message);
            }
        }
        public static CRM_API.Models.CustomerProfile.CustomerCCInfo DetailProfile(string last6CCNum , string mobileno, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.CustomerProfile.CustomerCCInfo>(
                            "crm3_get_detail_payment_profile", new
                            {
                                @msisdn = mobileno,
                                @ccnum = last6CCNum,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    return result ?? new CRM_API.Models.CustomerProfile.CustomerCCInfo();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("CRM_API.DB.PaymentProfile.DetailProfile: " + ex.Message);
            }
        }
        public static CRM_API.Models.Common.ErrCodeMsg SetDefaultProfile(string last6CCNum, string mobileno, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.Common.ErrCodeMsg>(
                            "crm3_set_default_payment_profile", new
                            {
                                @msisdn = mobileno,
                                @lastccnum = last6CCNum,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    return result ?? new CRM_API.Models.Common.ErrCodeMsg();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("CRM_API.DB.PaymentProfile.SetDefaultProfile: " + ex.Message);
            }
        }
        public static CRM_API.Models.Common.ErrCodeMsg SetAutopupProfile(string last6CCNum, string mobileno, decimal amount, string currency, int axstatus, string sitecode, int paymenttype)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.Common.ErrCodeMsg>(
                            "crm3_set_autopup_profile", new
                            {
                                @msisdn = mobileno,
                                @lastccnum = last6CCNum,
                                @amount = amount,
                                @axstatus = axstatus,
                                @currency = currency,
                                @sitecode = sitecode,
                                @topup_type = paymenttype
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    return result ?? new CRM_API.Models.Common.ErrCodeMsg();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("CRM_API.DB.PaymentProfile.SetAutopupProfile: " + ex.Message);
            }
        }
        
        //22-Feb-2019 : Moorthy : Added for Block Card 
        public static CRM_API.Models.CustomerProfile.DoBlockResponse DoBlockCard(string mobileno, string cc_no, int status, string calledby)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.CustomerProfile.DoBlockResponse>(
                            "crm_block_ccdc_card_detail", new
                            {
                                @mobileno = mobileno,
                                @cc_no = cc_no,
                                @status = status,
                                @calledby = calledby
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    return result ?? new CRM_API.Models.CustomerProfile.DoBlockResponse();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("CRM_API.DB.PaymentProfile.DoBlockCard: " + ex.Message);
            }
        }
    }
}
