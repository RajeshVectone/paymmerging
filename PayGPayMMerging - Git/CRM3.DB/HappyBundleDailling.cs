﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;

namespace CRM_API.DB.HappyBundle
{
    public class SProc
    {
        public static IEnumerable<CRM_API.Models.HappyBundle.countrylist> SubscribedBundle()
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.HappyBundle.countrylist>(
                            "crm_HappyBundDaillingnum_countrylist", new
                            {
                                //@mobileno = mobileno,
                                //@sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.HappyBundle.countrylist>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL HappyBundDaillingnum_countrylist: " + ex.Message);
            }
        }
        //added by karthik CRMT-231 Happy Bundle Dailling number
        public static IEnumerable<CRM_API.Models.HappyBundle.HappyBundlelist> HappyBundleDaillingNumberList(string mobileno, string sitecode)
        {
            try
            {
                //Test SP Fixed data :crm3_get_bundleid_happy_dialing_number
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.HappyBundle.HappyBundlelist>(
                            "crm3_get_happy_dialing_numbers_list", new
                            {
                                @mobileno = mobileno,
                                @sitecode = sitecode
                               

                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.HappyBundle.HappyBundlelist>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL Happy Bundle Dailling number list: " + ex.Message);
            }
        }

        //added by karthik CRMT-231 Happy Bundle Dailling number
        public static IEnumerable<CRM_API.Models.HappyBundle.HappyBundlelist> InsertHappyBundleDaillingnumber(string mobileno, string sitecode, string Dailnumber)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    
                    var result = conn.Query<CRM_API.Models.HappyBundle.HappyBundlelist>(
                            "Crm3_create_happy_dialing_number", new
                            {
                                @mobileno = mobileno,
                                @sitecode = sitecode,
                                @dest_number = Dailnumber

                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.HappyBundle.HappyBundlelist>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL Happy bundleid : " + ex.Message);
            }
        }
        //added by karthik CRMT-231 Happy Bundle Dailling number
        public static IEnumerable<CRM_API.Models.HappyBundle.HappyBundlelist> GetBundleidlist(string mobileno, string sitecode)
        {
            try
            {
                //Test SP Fixed data :crm3_get_bundleid_happy_dialing_number
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.HappyBundle.HappyBundlelist>(
                            "crm3_get_bundleid_happy_dialing_number_v1", new
                            {
                                @mobileno = mobileno,
                                @sitecode = sitecode


                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.HappyBundle.HappyBundlelist>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL Happy Bundle Dailling number list: " + ex.Message);
            }
        }

    }
}
