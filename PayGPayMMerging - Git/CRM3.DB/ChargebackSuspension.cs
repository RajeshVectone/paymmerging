﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;
using CRM_API.Models.ChargebackSuspension;

namespace CRM_API.DB.ChargebackSuspension
{
    public class SProc
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public static IEnumerable<SuspensionCommon> DoInsertChargebackTransaction(string product_code, string sitecode, string pay_reference, DateTime transaction_date, string mobileno, string reason, DateTime chargeback_claim_date, string entered_by, int proces_type, int req_type, double trans_amount, string customer_name, double chargeback_fee)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<SuspensionCommon>(
                            "crm_do_insert_chargeback_transaction", new
                            {
                                product_code = product_code,
                                sitecode = sitecode,
                                pay_reference = pay_reference,
                                transaction_date = transaction_date,
                                mobileno = mobileno,
                                reason = reason,
                                chargeback_claim_date = chargeback_claim_date,
                                entered_by = entered_by,
                                proces_type = proces_type,
                                req_type = req_type,
                                trans_amount = trans_amount,
                                customer_name = customer_name,
                                chargeback_fee = chargeback_fee
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<SuspensionCommon>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("DoInsertChargebackTransaction: " + ex.Message);
                return new List<SuspensionCommon>() { new SuspensionCommon() { errcode = -1, errmsg = ex.Message } };
            }
        }

        public static IEnumerable<RequestedChargebackOutput> GetRequestedChargeback(int userid, int role_id, int status, DateTime date_from, DateTime date_to, int search_flag)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<RequestedChargebackOutput>(
                            "crm_get_requested_chargeback", new
                            {
                                userid = userid,
                                role_id = role_id,
                                status  = status,
                                date_from = date_from,
                                date_to = date_to,
                                search_flag = search_flag
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<RequestedChargebackOutput>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetRequestedChargeback: " + ex.Message);
                return new List<RequestedChargebackOutput>() { };
            }
        }

        public static IEnumerable<SuspensionCommon> DoApproveRequestedChargeback(int userid, int role_id, string sitecode, string pay_reference, string mobileno, string approved_user, int req_type)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<SuspensionCommon>(
                            "crm_do_approve_requested_chargeback", new
                            {
                                userid = userid,
                                role_id = role_id,
                                sitecode = sitecode,
                                pay_reference = pay_reference,
                                mobileno = mobileno,
                                approved_user = approved_user,
                                req_type = req_type
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<SuspensionCommon>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("DoApproveRequestedChargeback: " + ex.Message);
                return new List<SuspensionCommon>() { new SuspensionCommon() { errcode = -1, errmsg = ex.Message } };
            }
        }

        public static IEnumerable<SuspensionCommon> DoUnblockRequest(string pay_reference, string mobileno)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<SuspensionCommon>(
                            "crm_do_unblock_request", new
                            {
                                pay_reference = pay_reference,
                                mobileno = mobileno,
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<SuspensionCommon>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("DoUnblockRequest: " + ex.Message);
                return new List<SuspensionCommon>() { new SuspensionCommon() { errcode = -1, errmsg = ex.Message } };
            }
        }

        public static IEnumerable<RequestedChargebackOutput> GetChargebackByAccountId(string mobileno)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<RequestedChargebackOutput>(
                            "crm_get_chargeback_by_accountid", new
                            {
                                @mobileno = mobileno
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<RequestedChargebackOutput>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetChargebackByAccountId: " + ex.Message);
                return new List<RequestedChargebackOutput>() { };
            }
        }

        public static IEnumerable<GetChargebackCollectionPaymentOutput> GetChargebackCollectionPayment(string mobileno)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<GetChargebackCollectionPaymentOutput>(
                            "crm_get_chargeback_collection_payment", new
                            {
                                @mobileno = mobileno
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<GetChargebackCollectionPaymentOutput>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetChargebackCollectionPayment: " + ex.Message);
                return new List<GetChargebackCollectionPaymentOutput>() { };
            }
        }

        public static IEnumerable<ChargebackCountOutput> GetRequestedChargebackCount(DateTime date_from, DateTime date_to)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<ChargebackCountOutput>(
                            "crm_chargeback_summary_info", new
                            {
                                @date_from = date_from,
                                @date_to = date_to
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<ChargebackCountOutput>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetRequestedChargebackCount: " + ex.Message);
                return new List<ChargebackCountOutput>() { };
            }
        }
    }
}
