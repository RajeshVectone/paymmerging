﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;

namespace CRM_API.DB
{
    public class IssueHistoryList
    {
        ////Issue History Report
        public static IEnumerable<IssueHistoryListOutput> GetIssueHistoryList(IssueHistoryListInput req)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<IssueHistoryListOutput>(
                            "crm_issue_list_history", new
                            {
                                @sitecode = req.sitecode,
                                @fromdate = req.date_from,
                                @todate = req.date_to
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<IssueHistoryListOutput>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL GetIssueHistoryList: " + ex.Message);
            }
        }
        ////Issue History Report
        public static IEnumerable<SMSIssueHistoryListOutput> GetSMSIssueHistoryList(SMSIssueHistoryListInput req)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<SMSIssueHistoryListOutput>(
                            "crm_get_sms_content_report", new
                            {
                                @date_from = req.date_from,
                                @date_to = req.date_to
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<SMSIssueHistoryListOutput>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL GetIssueHistoryList: " + ex.Message);
            }
        } 
    }
}
