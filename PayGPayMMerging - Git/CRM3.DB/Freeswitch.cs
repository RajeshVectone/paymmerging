﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using Dapper;

namespace CRM_API.DB.SProc
{
    public class Freeswitch
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public static CRM_API.Models.MyAccountLogin MyAccountLogin(string Username, string Password)
        {
            string sql = "CRM_VB.smepbx.vt_myaccount_login";
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<CRM_API.Models.MyAccountLogin>(
                            sql, new
                            {
                                @username = Username,
                                @password = Password
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    return result ?? new CRM_API.Models.MyAccountLogin();
                }
            }
            catch (Exception ex)
            {
                Log.Error("MyAccountLogin BL: {0}", ex.Message);
                throw new Exception("BL MyAccountLogin: " + ex.Message);
            }
        }

    }
}
