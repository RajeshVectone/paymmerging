﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;
using CRM_API.Models.AutoRenewalHistory;

namespace CRM_API.DB
{
    public class AutoRenewalHistory
    {
        ////Issue History Report
        public static IEnumerable<AutoRenewalHistoryModels> GetAutoRenewalHistoryList(string sitecode, string msisdn, string date_from, string date_to)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<Models.AutoRenewalHistory.AutoRenewalHistoryModels>(
                            "crm_get_auto_renewal_history_by_mobileno", new
                            {
                                @sitecode = sitecode,
                                @msisdn = msisdn,
                                @from_date = date_from,
                                @to_date = date_to
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<Models.AutoRenewalHistory.AutoRenewalHistoryModels>();
                }
            }
            catch (Exception ex)
            {
                List<Models.AutoRenewalHistory.AutoRenewalHistoryModels> resValue = new List<Models.AutoRenewalHistory.AutoRenewalHistoryModels>();
                return resValue;
            }

        }
    }
}
