﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;
using System.IO;
using System.Net.Mail;
using System.Configuration;

namespace CRM_API.DB
{
    public class Emails
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public static IEnumerable<CRM_API.Models.EmailModel> GetEmailQueueInfo(int request_id)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.EmailModel>(
                            "email_queue_get_info", new
                            {
                                @request_id = request_id
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.EmailModel>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetEmailQueueInfo() : " + ex.Message);
                throw new Exception("GetEmailQueueInfo() : " + ex.Message);
            }
        }

        public static IEnumerable<CRM_API.Models.AgentInfoModels> GetAgentInfo()
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.AgentInfoModels>(
                            "email_queue_get_agent_info", new
                            {
                                @role_id = 1
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.AgentInfoModels>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetAgentInfo() : " + ex.Message);
                throw new Exception("GetAgentInfo() : " + ex.Message);
            }
        }

        //public static IEnumerable<CRM_API.Models.EmailEueueAssignToUserOutput> EmailQueueAssignToUser(EmailModel input)
        //{
        //    try
        //    {
        //        string[] requestIds = input.request_id_selected.Split('|');
        //        foreach (var requestId in requestIds)
        //        {
        //            if (!String.IsNullOrEmpty(requestId))
        //            {
        //                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
        //                {
        //                    var result = conn.Query<CRM_API.Models.EmailEueueAssignToUserOutput>(
        //                            "email_queue_assign_to_user", new
        //                            {
        //                                @requestid = requestId,
        //                                @assigned_user = input.assigned_user,
        //                                @assigned_by = input.userid,
        //                                @queue_status = input.queue_status,
        //                                @process_type = input.process_type,
        //                                @queue_id = input.queue_id
        //                            }, commandType: CommandType.StoredProcedure);

        //                    if (result.ElementAt(0).errcode == 0)
        //                    {
        //                        SendAssignedMail(input.mailLocation, input.email_to, input.agent_name, Convert.ToString(result.ElementAt(0).queue_id), Convert.ToString(result.ElementAt(0).email_sub), Convert.ToString(result.ElementAt(0).email_body));
        //                    }
        //                }
        //                return new List<CRM_API.Models.EmailEueueAssignToUserOutput> { new EmailEueueAssignToUserOutput() { errcode = 0, errmsg = "Success" } };
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return new List<CRM_API.Models.EmailEueueAssignToUserOutput> { new EmailEueueAssignToUserOutput() { errcode = -1, errmsg = ex.Message } };
        //    }
        //    return new List<CRM_API.Models.EmailEueueAssignToUserOutput> { new EmailEueueAssignToUserOutput() { errcode = -1, errmsg = "Failure" } };
        //}

        //private static void SendAssignedMail(string mailLocation, string email, string fullname, string queue_id, string subject, string body)
        //{
        //    try
        //    {
        //        StreamReader mailContent_file = new StreamReader(mailLocation);
        //        string mailContent = mailContent_file.ReadToEnd();
        //        mailContent_file.Close();
        //        mailContent = mailContent
        //        .Replace("*|FNAME|*", fullname)
        //        .Replace("*|QUEUEID|*", queue_id)
        //        .Replace("*|SUBJECT|*", subject)
        //        .Replace("*|BODY|*", body);
        //        // Send the email
        //        MailAddress mailFrom = new MailAddress("noreply@vectonemobile.co.uk", "Vectone Mobile");
        //        MailAddressCollection mailTo = new MailAddressCollection();
        //        mailTo.Add(new MailAddress(email, string.Format("{0}", fullname)));
        //        CRM_API.Helpers.EmailSending.Send(true, mailFrom, mailTo, null, null, subject, mailContent);
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //}

        private static void SendReplyMail(string mailLocation, string email, string fullname, string subject, string body)
        {
            try
            {
                StreamReader mailContent_file = new StreamReader(mailLocation);
                string mailContent = mailContent_file.ReadToEnd();
                mailContent_file.Close();
                mailContent = mailContent
                .Replace("*|FNAME|*", fullname)
                .Replace("*|SUBJECT|*", subject)
                .Replace("*|BODY|*", body);

                // Send the email
                MailAddress mailFrom = new MailAddress(ConfigurationManager.AppSettings["QUEUEEMAILMAILFROM"], ConfigurationManager.AppSettings["QUEUEEMAILMAILFROMDISPLAYNAME"]);
                MailAddressCollection mailTo = new MailAddressCollection();
                mailTo.Add(new MailAddress(email, string.Format("{0}", fullname)));
                CRM_API.Helpers.EmailSending.Send(true, mailFrom, mailTo, null, null, subject, mailContent);
            }
            catch (Exception ex)
            {
                Log.Error("SendReplyMail() : " + ex.Message);
                Log.Error("SendReplyMail() : " + ex.Message);
            }
        }

        public static IEnumerable<CRM_API.Models.GetEmailQueueAssignedInfoOutput> GetEmailQueueAssignedInfo(int user_id, int queue_id, int role_id, int status_id)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.GetEmailQueueAssignedInfoOutput>(
                            "email_queue_get_assigned_info", new
                            {
                                @user_id = user_id,
                                @queue_id = queue_id,
                                @role_id = role_id,
                                @status_id = status_id,
                                @user_check_flag = (queue_id == 0 ? 0 : 1)
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.GetEmailQueueAssignedInfoOutput>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetEmailQueueAssignedInfo() : " + ex.Message);
                throw new Exception("GetEmailQueueAssignedInfo() : " + ex.Message);
            }
        }

        public static IEnumerable<CRM_API.Models.EmailQueueSaveReplyTextOutput> EmailQueueSaveReplyText(int queue_id, string email_to, string email_cc, string email_subject, string email_body, int is_closed, string filename)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.EmailQueueSaveReplyTextOutput>(
                            "email_queue_save_reply_text", new
                            {
                                @queue_id = queue_id,
                                @email_subject = email_subject,
                                @reply_text = email_body,
                                @is_closed = is_closed
                            }, commandType: CommandType.StoredProcedure);

                    //if (result != null && result.ElementAt(0).errcode == 0)
                    //{
                    //    //SendMail("noreply@vectonemobile.co.uk", email_to, email_cc, email_subject, email_body);
                    //    SendReplyMail(Path.Combine(ConfigurationManager.AppSettings["QUEUEEMAILTEMPLATEPATH"], filename), email_to, "", email_subject, email_body);
                    //}
                    return result ?? new List<CRM_API.Models.EmailQueueSaveReplyTextOutput>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("Reply() : " + ex.Message);
                throw new Exception("Reply() : " + ex.Message);
            }
        }

        public static IEnumerable<CRM_API.Models.GetMyEmailOpenItemsOutput> GetMyEmailOpenItems(int queue_id)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.GetMyEmailOpenItemsOutput>(
                            "email_queue_get_email_body_text_info ", new
                            {
                                @queueid = queue_id
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.GetMyEmailOpenItemsOutput>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetMyEmailOpenItems() : " + ex.Message);
                throw new Exception("GetMyEmailOpenItems() : " + ex.Message);
            }
        }

        public static IEnumerable<CRM_API.Models.GetEmailQueueCustomerHistoryOutput> GetEmailQueueCustomerHistory(string customer_email_id)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.GetEmailQueueCustomerHistoryOutput>(
                            "email_queue_get_customer_history", new
                            {
                                @customer_email_id = customer_email_id
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.GetEmailQueueCustomerHistoryOutput>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetMyEmailOpenItems() : " + ex.Message);
                throw new Exception("GetMyEmailOpenItems() : " + ex.Message);
            }
        }

        public static IEnumerable<CRM_API.Models.GetMyEmailOpenItemsOutput> GetEmailQueueCustomerHistory(int queue_id)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.GetMyEmailOpenItemsOutput>(
                            "email_queue_get_customer_history ", new
                            {
                                @queue_id = queue_id
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.GetMyEmailOpenItemsOutput>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetMyEmailOpenItems() : " + ex.Message);
                throw new Exception("GetMyEmailOpenItems() : " + ex.Message);
            }
        }

        //My Tickets
        public static IEnumerable<CRM_API.Models.GetEmailViewTicketsByUserOutput> GetEmailViewTicketsByUser(int user_id, int role_id, int status_id)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.GetEmailViewTicketsByUserOutput>(
                            "email_view_tickets_by_user", new
                            {
                                @user_id = user_id,
                                @role_id = role_id,
                                @ticket_status = status_id
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.GetEmailViewTicketsByUserOutput>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetEmailViewTicketsByUser() : " + ex.Message);
                throw new Exception("GetEmailViewTicketsByUser() : " + ex.Message);
            }
        }


        public static IEnumerable<CRM_API.Models.ChangeEmailQueueStatusOutput> ChangeEmailQueueStatus(int queue_id, int status, int assigned_user_id, string descr, string compaint_category)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.ChangeEmailQueueStatusOutput>(
                            "email_queue_change_status", new
                            {
                                @queue_id = queue_id,
                                @status = status,
                                @assigned_user = assigned_user_id,
                                @descr = descr,
                                @compaint_category = compaint_category
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.ChangeEmailQueueStatusOutput>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("ChangeEmailQueueStatus() : " + ex.Message);
                throw new Exception("ChangeEmailQueueStatus() : " + ex.Message);
            }
        }

        //Common Email Send Logic
        public static void SendMail(string emailFrom, string emailTo, string emailCc, string mailSubject, string mailContent)
        {
            try
            {
                MailAddress mailFrom = new MailAddress(emailFrom);
                MailAddressCollection mailTo = new MailAddressCollection();
                var MailTo = emailTo.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var item in MailTo)
                {
                    mailTo.Add(new MailAddress(item.ToString(), ""));
                }
                MailAddressCollection mailCC = new MailAddressCollection();
                var MailCc = emailCc.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries); ;
                foreach (var item in MailCc)
                {
                    mailCC.Add(new MailAddress(item.ToString(), ""));
                }
                CRM_API.Helpers.EmailSending.Send(true, mailFrom, mailTo, mailCC, null, mailSubject, mailContent);
            }
            catch (Exception ex)
            {
                Log.Error("SendMail() : " + ex.Message);
                throw new Exception("SendMail() : " + ex.Message);
            }
        }
        //Common Email Send Logic
        public static IEnumerable<CRM_API.Models.ChangeEmailQueueStatusOutput> RemoveEmailFromList(int request_id, string CurrUser)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.ChangeEmailQueueStatusOutput>(
                            "email_qeueue_remove_request", new
                            {
                                @reqid = request_id,
                                @process_user = CurrUser
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.ChangeEmailQueueStatusOutput>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("RemoveEmails : " + ex.Message);
                throw new Exception("RemoveEmails : " + ex.Message);
            }
        }
    }
}
