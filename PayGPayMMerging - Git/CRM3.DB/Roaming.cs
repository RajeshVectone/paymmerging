﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;

namespace CRM_API.DB.Roaming
{
    public class Roaming
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public static IEnumerable<CRM_API.Models.Roaming.RoamingInfo> RoamingInfo(string ICCID, string mobileno, string sitecode)
        {
            try
            {
                /** Raoming tab  **/

                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.Roaming.RoamingInfo>(
                            "CRM3_GET_ROAMING_COUNTRY_RATES", new
                            {
                                @ICCID = ICCID,
                                @MOBILENO = mobileno,
                                @SITECODE = sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.Roaming.RoamingInfo>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL RoamingInfo: " + ex.Message);
            }
        }
    }
}
