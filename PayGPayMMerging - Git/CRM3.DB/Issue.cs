﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;
using CRM_API.Models.Issue;
using CRM_API.DB.Common;

namespace CRM_API.DB.Issue
{
    public class SProc
    {
        public static IEnumerable<Models.Issue.IssueModels> GetIssueList(int subscriber, string sitecode, string mobileno)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<Models.Issue.IssueModels>(
                            "crm3_crm_issue_list_by_subscriberid", new
                            {
                                @subscriberid = subscriber,
                                @sitecode = sitecode,
                                @mobileno = mobileno
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<Models.Issue.IssueModels>();
                }
            }
            catch (Exception ex)
            {
                List<Models.Issue.IssueModels> resValue = new List<Models.Issue.IssueModels>();
                return resValue;
            }
        }

        public static IEnumerable<Models.Issue.IssueModels> GetIssueListPAYM(string mobileNo, int subscriber, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    //if (subscriber != 0)
                    //{
                    //crm3_prob_search_by_subscriberid
                    var result = conn.Query<Models.Issue.IssueModels>(
                            "crm3_crm_issue_list_by_subscriberid", new
                            {
                                @subscriberid = subscriber,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure);
                    return result ?? new List<Models.Issue.IssueModels>();
                    //}
                    //else
                    //{
                    //    var result = conn.Query<Models.Issue.IssueModels>(
                    //            "crm3_prob_search_by_mobileno", new
                    //            {
                    //                @mobileno = mobileNo,
                    //                @sitecode = sitecode
                    //            }, commandType: CommandType.StoredProcedure);
                    //    return result ?? new List<Models.Issue.IssueModels>();
                    //}
                }
            }
            catch (Exception ex)
            {
                List<Models.Issue.IssueModels> resValue = new List<Models.Issue.IssueModels>();
                return resValue;
            }
        }

        public static IEnumerable<Models.Issue.IssueModels> GetIssueListByID(string problemid, string sitecode)
        {
            try
            {

                //old SP:crm3_prob_search_byID_v2
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<Models.Issue.IssueModels>(
                            "crm3_prob_search_byID_v3", new
                            {
                                @problemid = problemid,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<Models.Issue.IssueModels>();
                }
            }
            catch (Exception ex)
            {
                List<Models.Issue.IssueModels> resValue = new List<Models.Issue.IssueModels>();
                return resValue;
            }
        }

        public static IEnumerable<Models.Issue.IssueCategory> GetProblemCategory(string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<Models.Issue.IssueCategory>(
                            "crm3_CRMGetProblemCategory", new
                            {
                                @problemcategorytype = 0,
                                @categorytypename = string.Empty,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<Models.Issue.IssueCategory>();
                }
            }
            catch (Exception ex)
            {
                List<Models.Issue.IssueCategory> resValue = new List<Models.Issue.IssueCategory>();
                return resValue;
            }
        }
        public static IEnumerable<Models.Issue.IssueCategory1> GetProblemCategory1(string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<Models.Issue.IssueCategory1>(
                            "crm3_CRM_Get_Issue_Type", new
                            {
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<Models.Issue.IssueCategory1>();
                }
            }
            catch (Exception ex)
            {
                List<Models.Issue.IssueCategory1> resValue = new List<Models.Issue.IssueCategory1>();
                return resValue;
            }
        }
        public static IEnumerable<Models.Issue.IssueCategoryDetail> GetProblemCategoryDetail(string sitecode, int problemcategory, string categorytype)
        {
            try
            {
                //Old SP :  crm3_CRMGetProblemDetail
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<Models.Issue.IssueCategoryDetail>(
                            "crm3_CRMGetProblemDetail", new
                            {
                                @categorytypename = categorytype,
                                @problemcategorytype = problemcategory,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<Models.Issue.IssueCategoryDetail>();
                }
            }
            catch (Exception ex)
            {
                List<Models.Issue.IssueCategoryDetail> resValue = new List<Models.Issue.IssueCategoryDetail>();
                return resValue;
            }
        }
        public static IEnumerable<Models.Issue.IssueCategory2> GetProblemCategoryDetail1(string Issue_Id, string sitecode)
        {
            try
            {
                //Old SP:crm3_CRM_Get_Issue_Function
                //reason:new excel upload
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<Models.Issue.IssueCategory2>(
                            "crm3_CRM_Get_Issue_Function_V2", new
                            {
                                @Issue_Type = Issue_Id,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<Models.Issue.IssueCategory2>();
                }
            }
            catch (Exception ex)
            {
                List<Models.Issue.IssueCategory2> resValue = new List<Models.Issue.IssueCategory2>();
                return resValue;
            }
        }
        public static IEnumerable<Models.Issue.IssueCategory3> GetProblemCategoryDetaip(string Issue_Id, string sitecode)
        {
            try
            {
                //Old SP:crm3_CRM_Get_Issue_Complaint
                //Reason :new excel sheet upload
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<Models.Issue.IssueCategory3>(
                            "crm3_CRM_Get_Issue_Complaint_V2", new
                            {
                                @Function_Id = Issue_Id,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<Models.Issue.IssueCategory3>();
                }
            }
            catch (Exception ex)
            {
                List<Models.Issue.IssueCategory3> resValue = new List<Models.Issue.IssueCategory3>();
                return resValue;
            }
        }
        //karthik
        public static IEnumerable<Models.Issue.Contact_Type> Getcontacttype(int problemid, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<Models.Issue.Contact_Type>(
                            "crm3_crm_issue_get_contact_type_by_issueid", new
                            {
                                @Problem_Id = problemid,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<Models.Issue.Contact_Type>();
                }
            }
            catch (Exception ex)
            {
                List<Models.Issue.Contact_Type> resValue = new List<Models.Issue.Contact_Type>();
                return resValue;
            }
        }
        public static IEnumerable<Models.Issue.Complaint_type> Getcompliantpe(int problemid, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<Models.Issue.Complaint_type>(
                            "crm3_crm_issue_get_Complaint_Status_by_issueid", new
                            {
                                @Problem_Id = problemid,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<Models.Issue.Complaint_type>();
                }
            }
            catch (Exception ex)
            {
                List<Models.Issue.Complaint_type> resValue = new List<Models.Issue.Complaint_type>();
                return resValue;
            }
        }
        public static IEnumerable<Models.Issue.timeinterval> Getintervaltype(int problemid, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<Models.Issue.timeinterval>(
                            "crm3_crm_issue_get_time_interval_by_issueid", new
                            {
                                @Problem_Id = problemid,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<Models.Issue.timeinterval>();
                }
            }
            catch (Exception ex)
            {
                List<Models.Issue.timeinterval> resValue = new List<Models.Issue.timeinterval>();
                return resValue;
            }
        }
        //GetAssignedTo
        public static IEnumerable<Models.Issue.AssignedTo> GetAssignedTo(int problemid, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<Models.Issue.AssignedTo>(
                            "crm3_crm_issue_get_assigned_to_by_issueid", new
                            {
                                @Problem_Id = problemid,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<Models.Issue.AssignedTo>();
                }
            }
            catch (Exception ex)
            {
                List<Models.Issue.AssignedTo> resValue = new List<Models.Issue.AssignedTo>();
                return resValue;
            }
        }
        //GetAssignPriority
        public static IEnumerable<Models.Issue.Priority> GetAssignPriority(int problemid, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<Models.Issue.Priority>(
                            "crm3_crm_issue_get_priority_by_issueid", new
                            {
                                @Problem_Id = problemid,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<Models.Issue.Priority>();
                }
            }
            catch (Exception ex)
            {
                List<Models.Issue.Priority> resValue = new List<Models.Issue.Priority>();
                return resValue;
            }
        }
        public static Common.ErrCodeMsg InsertIssue(Models.Issue.IssueModels In)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<Common.ErrCodeMsg>(
                           "crm3_prob_create_v2", new
                           {
                               @problemtype = In.ProblemType,
                               @subscriberid = In.SubscriberID,
                               @problemdesc = In.ProblemDesc,
                               @submitby = In.SubmitBy,
                               @mobileno = In.mobileno,
                               @problemstatus = In.ProblemStatus,
                               @solutiondesc = In.SolutionDesc,
                               @categorytype = In.problemcategorytype,
                               @sitecode = In.Sitecode
                           }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    return result;

                }
            }
            catch (Exception ex)
            {
                return new Common.ErrCodeMsg() { errcode = -1, errmsg = ex.Message };
            }
        }
        public static Common.ErrCodeMsg InsertIssu1(Models.Issue.IssueModels In)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<Common.ErrCodeMsg>(
                           "crm3_CRM_Create_Issue_v1", new
                           {
                               @Subscriber_ID = In.Subscriber_ID,
                               @Mobile_No = In.Mobile_No,
                               @Issue_Type = In.Issue_Id,
                               @Issue_Function = In.Function_Id,
                               @Issue_Complaint = In.Complaint_Id,
                               @Issue_Status = In.Issue_Status,
                               @Issue_Desc = In.Issue_Desc,
                               @Solution_Desc = In.Solution_Desc,
                               @Created_By = In.Created_By,
                               @sitecode = In.Sitecode,
                               @time_interval = In.intreval,
                               @contact_type = In.contact_type,
                               @Complaint_Status = In.Comp_Status,
                               @Jira_ticket = In.JIRA_Tickets,
                               @esclated_to = In.Esclated_To,
                               @interval_comment = In.interval_comment,
                               @priority_id = In.AssignPriority
                           }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    return result;

                }
            }
            catch (Exception ex)
            {
                return new Common.ErrCodeMsg() { errcode = -1, errmsg = ex.Message };
            }
        }
        public static Common.ErrCodeMsg InsertIssudeadcall(Models.Issue.IssueModels In)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<Common.ErrCodeMsg>(
                           "crm3_CRM_dead_Call_insert", new
                           {
                               @subscriber_id = In.Subscriber_ID,
                               @mobileno = In.Mobile_No,
                               @submitted_by = In.Created_By,
                               @sitecode = In.Sitecode
                           }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    return result;

                }
            }
            catch (Exception ex)
            {
                return new Common.ErrCodeMsg() { errcode = -1, errmsg = ex.Message };
            }
        }
        public static Common.ErrCodeMsg UpdateIssue(Models.Issue.IssueModels In)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    if (In.mode == 2)
                    {
                        var result = conn.Query<Common.ErrCodeMsg>(
                               "crm3_CRM_Update_Issue_V1", new
                               {
                                   @Problem_Id = In.ProblemID,
                                   @Subscriber_ID = In.Subscriber_ID,
                                   @Issue_Type = In.Issue_Id,
                                   @Issue_Function = In.Function_Id,
                                   @Issue_Complaint = In.Complaint_Id,
                                   @Issue_Status = In.Issue_Status,
                                   @Issue_Desc = In.Issue_Desc,
                                   @Solution_Desc = In.Solution_Desc,
                                   @Created_By = In.Created_By,
                                   @sitecode = In.Sitecode,
                                   @time_interval = In.intreval,
                                   @contact_type = In.contact_type,
                                   @Complaint_Status = In.Comp_Status,
                                   @Jira_ticket = In.JIRA_Tickets,
                                   @esclated_to = In.Esclated_To,
                                   @interval_comment = In.interval_comment,
                                   @assigned_to = In.Assigned_To
                               }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                        return result;
                    }
                    else
                    {
                        var result = conn.Query<Common.ErrCodeMsg>(
                               "crm3_CRM_Old_Entry_Update_Issue", new
                               {
                                   @Problem_Id = In.ProblemID,
                                   @Subscriber_ID = In.Subscriber_ID,
                                   @Issue_Type = In.Issue_Id,
                                   @Issue_Function = In.Function_Id,
                                   @Issue_Complaint = In.Complaint_Id,
                                   @Issue_Status = In.Issue_Status,
                                   @Issue_Desc = In.Issue_Desc,
                                   @Solution_Desc = In.Solution_Desc,
                                   @Created_By = In.Created_By,
                                   @sitecode = In.Sitecode,
                                   @time_interval = In.intreval,
                                   @contact_type = In.contact_type,
                                   @Complaint_Status = In.Comp_Status,
                                   @Jira_ticket = In.JIRA_Tickets
                               }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                return new Common.ErrCodeMsg() { errcode = -1, errmsg = ex.Message };
            }
        }
        public static Common.ErrCodeMsg UpdateIssuePAYM(Models.Issue.IssueModels In)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<Common.ErrCodeMsg>(
                           "crm3_prob_update_v2", new
                           {
                               @problemid = In.ProblemID,
                               @problemtype = In.ProblemType,
                               @problemdesc = In.ProblemDesc,
                               @problemstatus = In.ProblemStatus,
                               @solutiondesc = In.SolutionDesc,
                               @categorytype = In.problemcategorytype,
                               @sitecode = In.Sitecode
                           }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    return result;
                }
            }
            catch (Exception ex)
            {
                return new Common.ErrCodeMsg() { errcode = -1, errmsg = ex.Message };
            }
        }
        public static IEnumerable<Models.Issue.IssueHistory> GetIssueHistory(string sitecode, int problemid)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<Models.Issue.IssueHistory>(
                            "crm3_list_prob_history", new
                            {
                                @problemid = problemid,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<Models.Issue.IssueHistory>();
                }
            }
            catch (Exception ex)
            {
                List<Models.Issue.IssueHistory> resValue = new List<Models.Issue.IssueHistory>();
                return resValue;
            }
        }
    }
}
