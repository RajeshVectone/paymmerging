﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Dapper;
using System.Threading.Tasks;
using NLog;

namespace CRM_API.DB
{
    public class PMBODDOperations
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public static IEnumerable<CRM_API.Models.PMBODDOperations> getPMBODDNewInstructions(CRM_API.Models.PMBOSearch objSearch)
        {
            string fdate, tdate = "";
            if (objSearch.FromDate != null && objSearch.ToDate != null)
            {
                fdate = objSearch.FromDate.Substring(6, 4) + "-" + objSearch.FromDate.ToString().Substring(3, 2) + "-" + objSearch.FromDate.ToString().Substring(0, 2);
                tdate = objSearch.ToDate.ToString().Substring(6, 4) + "-" + objSearch.ToDate.ToString().Substring(3, 2) + "-" + objSearch.ToDate.ToString().Substring(0, 2);
            }
            else
            {
                fdate = null;
                tdate = null;
            }
            try
            {
                string ConnStr = "";
                if (objSearch.SiteCode.ToString().ToUpper() == "VMUK")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnection.ToString();
                }
                if (objSearch.SiteCode.ToString().ToUpper() == "VMNL")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnectionVMNL.ToString();
                }
                if (objSearch.SiteCode.ToString().ToUpper() == "VMFR")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnectionVMFR.ToString();
                }
                if (objSearch.SiteCode.ToString().ToUpper() == "VMAT")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnectionVMAT.ToString();
                }
                using (var conn = new SqlConnection(ConnStr))
                {
                    conn.Open();
                    //var result = conn.Query<CRM_API.Models.PMBODDOperations>("c3pm_dd_new_instruction", new
                    var result = conn.Query<CRM_API.Models.PMBODDOperations>("c3pm_dd_new_instruction_v2", new
                    //var result = conn.Query<CRM_API.Models.PMBODDOperations>("c3pm_dd_new_instruction_v1", new
                    {
                        @registerdate1 = fdate,
                        @registerdate2 = tdate,
                        @paymentref = (objSearch.CCTransactionID == string.Empty || objSearch.CCTransactionID == "" || objSearch.CCTransactionID == null) ? null : objSearch.CCTransactionID,
                        @subscribeid = (objSearch.SubscriberID == string.Empty || objSearch.SubscriberID == "" || objSearch.SubscriberID == null) ? null : objSearch.SubscriberID,
                        @branchsortcode = (objSearch.SortCode == string.Empty || objSearch.SortCode == "" || objSearch.SortCode == null) ? null : objSearch.SortCode,
                        @accountnumber = (objSearch.AccountNumber == string.Empty || objSearch.AccountNumber == "" || objSearch.AccountNumber == null) ? null : objSearch.AccountNumber
                    }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.PMBODDOperations>();
                }
            }
            catch (SqlException ex)
            {
                Log.Error("c3pm_dd_new_instruction_v2 : " + ex.Message);
                throw new Exception("c3pm_dd_new_instruction_v2  :  " + ex.Message);
            }
        }

        public static IEnumerable<CRM_API.Models.PMBODDOperations> getPMBODDCollectionInstructions(CRM_API.Models.PMBOSearch objSearch)
        {
            try
            {
                string ConnStr = "";
                if (objSearch.SiteCode.ToString().ToUpper() == "VMUK")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnection.ToString();
                }
                if (objSearch.SiteCode.ToString().ToUpper() == "VMNL")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnectionVMNL.ToString();
                }
                if (objSearch.SiteCode.ToString().ToUpper() == "VMFR")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnectionVMFR.ToString();
                }
                if (objSearch.SiteCode.ToString().ToUpper() == "VMAT")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnectionVMAT.ToString();
                }
                using (var conn = new SqlConnection(ConnStr))
                {
                    conn.Open();
    
                    //var result = conn.Query<CRM_API.Models.PMBODDOperations>("c3pm_dd_collection", new
                    //var result = conn.Query<CRM_API.Models.PMBODDOperations>("c3pm_dd_collection_TEST", new
                    //var result = conn.Query<CRM_API.Models.PMBODDOperations>("c3pm_dd_collection_v1", new
                    var result = conn.Query<CRM_API.Models.PMBODDOperations>("get_collection_file_in_txt", new
                    {
                        @paymentref = objSearch.CCTransactionID,
                        @subscribeid = objSearch.SubscriberID,
                        @branchsortcode = objSearch.SortCode,
                        @accountnumber = objSearch.AccountNumber
                    }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.PMBODDOperations>();
                }
            }
            catch (SqlException ex)
            {
                Log.Error("get_collection_file_in_txt : " + ex.Message);
                throw new Exception("get_collection_file_in_txt  :  " + ex.Message);
            }
        }

        public static List<CRM_API.Models.ManualMarking> getmanualmarkinggrid(string subId,string Sitecode)
        {
                try
                {
                    string ConnStr = "";
                    if (Sitecode.ToUpper() == "VMUK")
                    {
                        ConnStr = CRM_API.Helpers.Config.MCMCRMConnection.ToString();
                    }
                    if (Sitecode.ToString().ToUpper() == "VMNL")
                    {
                        ConnStr = CRM_API.Helpers.Config.MCMCRMConnectionVMNL.ToString();
                    }
                    if (Sitecode.ToString().ToUpper() == "VMFR")
                    {
                        ConnStr = CRM_API.Helpers.Config.MCMCRMConnectionVMFR.ToString();
                    }
                    if (Sitecode.ToString().ToUpper() == "VMAT")
                    {
                        ConnStr = CRM_API.Helpers.Config.MCMCRMConnectionVMAT.ToString();
                    }
                    //using (var conn = new SqlConnection(CRM_API.Helpers.Config.MCMCRMConnection))
                    using (var conn = new SqlConnection(ConnStr))
                    {
                        conn.Open();
                        //var result = conn.Query<CRM_API.Models.PMBODDOperations>("c3pm_dd_collection", new
                        var result = conn.Query<CRM_API.Models.ManualMarking>(
                                               "get_latest_upload_info_for_grid",
                                               new
                                               {
                                                   @id = subId
                                               }, commandType: CommandType.StoredProcedure);

                        return result.ToList() ?? new List<CRM_API.Models.ManualMarking>();
                    }
                }
                catch (SqlException ex)
                {
                    Log.Error("get_latest_upload_info_for_grid : " + ex.Message);
                    throw new Exception("get_latest_upload_info_for_grid  :  " + ex.Message);
                }
        }
        public static List<CRM_API.Models.DDCancellation> getmanualmarkingDDgrid(string subId,string SiteCode)
        {
                try
                {
                    string ConnStr = "";
                    if (SiteCode.ToUpper() == "VMUK")
                    {
                        ConnStr = CRM_API.Helpers.Config.MCMCRMConnection.ToString();
                    }
                    if (SiteCode.ToString().ToUpper() == "VMNL")
                    {
                        ConnStr = CRM_API.Helpers.Config.MCMCRMConnectionVMNL.ToString();
                    }
                    if (SiteCode.ToString().ToUpper() == "VMFR")
                    {
                        ConnStr = CRM_API.Helpers.Config.MCMCRMConnectionVMFR.ToString();
                    }
                    if (SiteCode.ToString().ToUpper() == "VMAT")
                    {
                        ConnStr = CRM_API.Helpers.Config.MCMCRMConnectionVMAT.ToString();
                    }
                    using (var conn = new SqlConnection(ConnStr))
                    {
                        conn.Open();
                        var result = conn.Query<CRM_API.Models.DDCancellation>(
                                               "get_latest_DD_Cancellation_upload_info_for_grid ",
                                               new
                                               {
                                                   @id = subId
                                               }, commandType: CommandType.StoredProcedure);

                        return result.ToList() ?? new List<CRM_API.Models.DDCancellation>();
                    }
                }
                catch (SqlException ex)
                {
                    Log.Error("get_latest_DD_Cancellation_upload_info_for_grid : " + ex.Message);
                    throw new Exception("get_latest_DD_Cancellation_upload_info_for_grid   :  " + ex.Message);
                }
        }

        public static IEnumerable<CRM_API.Models.PMBOOutput> savePMBOSaveCollections(string subId, int DD_status,string username)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.MCMCRMConnection))
                {
                    conn.Open();
                    CRM_API.Models.PMBOSearch objSearch = new Models.PMBOSearch();
                    objSearch.FromDate = null;
                    objSearch.ToDate = null;
                    objSearch.CCTransactionID = null;
                    objSearch.SubscriberID = subId;
                    objSearch.SortCode = null;
                    objSearch.AccountNumber = null;
                    var newValues = CRM_API.DB.PMBODDOperations.getPMBODDNewInstructions(objSearch).ToArray();
                   // var result1 = conn.Query<CRM_API.Models.PMBOOutput>("insert_new_DD_stage", new { },commandType: CommandType.StoredProcedure);//Added By Barani.
                    var result = conn.Query<CRM_API.Models.PMBOOutput>("c3pm_dd_set_status", new
                    {
                        @subs_date = (newValues[0].lastupdate == string.Empty || newValues[0].lastupdate == "" || newValues[0].lastupdate == null) ? DateTime.MinValue : DateTime.Parse(newValues[0].lastupdate),
                        @subs_ID = (newValues[0].subscriberid == string.Empty || newValues[0].subscriberid == "" || newValues[0].subscriberid == null) ? 0 : Convert.ToInt32(newValues[0].subscriberid),
                        @trans_ID = (newValues[0].paymentref == string.Empty || newValues[0].paymentref == "" || newValues[0].paymentref == null) ? null : newValues[0].paymentref,
                        @last_name = (newValues[0].fullname == string.Empty || newValues[0].paymentref == "" || newValues[0].paymentref == null) ? null : newValues[0].paymentref,
                        @sort_code = (newValues[0].sort_code == string.Empty || newValues[0].sort_code == "" || newValues[0].sort_code == null) ? null : newValues[0].sort_code,
                        @account_number = (newValues[0].account_number == string.Empty || newValues[0].account_number == "" || newValues[0].account_number == null) ? null : newValues[0].account_number,
                        @account_name = (newValues[0].accountname == string.Empty || newValues[0].accountname == "" || newValues[0].accountname == null) ? null : newValues[0].accountname,
                        @plan = (newValues[0].orderselected == string.Empty || newValues[0].orderselected == "" || newValues[0].orderselected == null) ? null : newValues[0].orderselected,
                        @DD_status = DD_status,
                        @updateby = "Mundio",
                        @ICCID = (newValues[0].iccid == string.Empty || newValues[0].iccid == "" || newValues[0].iccid == null) ? null : newValues[0].iccid,
                        @email = (newValues[0].email == string.Empty || newValues[0].email == "" || newValues[0].email == null) ? null : newValues[0].email,
                        @opsid = (newValues[0].opsid == string.Empty || newValues[0].opsid == "" || newValues[0].opsid == null) ? 0 : Convert.ToInt32(newValues[0].opsid),
                        @suspendType = 0,
                        @freesimid = (newValues[0].freesimid == string.Empty || newValues[0].freesimid == "" || newValues[0].freesimid == null) ? 0 : Convert.ToInt32(newValues[0].freesimid),
                        @reason = ""
                    }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.PMBOOutput>();
                }
            }
            catch (SqlException ex)
            {
                Log.Error("c3pm_dd_set_status : " + ex.Message);
                throw new Exception("c3pm_dd_set_status   :  " + ex.Message); 
            }
        }

        public static IEnumerable<CRM_API.Models.PMBOOutput> savePMBOCollections(string subId, int DD_status, string username)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.MCMCRMConnection))
                {
                    conn.Open();
                    CRM_API.Models.PMBOSearch objSearch = new Models.PMBOSearch();
                    objSearch.FromDate = null;
                    objSearch.ToDate = null;
                    objSearch.CCTransactionID = null;
                    objSearch.SubscriberID = subId;
                    objSearch.SortCode = null;
                    objSearch.AccountNumber = null;
                    var newValues = CRM_API.DB.PMBODDOperations.getPMBODDCollectionInstructions(objSearch).ToArray();
                    var result = conn.Query<CRM_API.Models.PMBOOutput>("c3pm_dd_set_status", new
                    {
                        @subs_date = (newValues[0].lastupdate == string.Empty || newValues[0].lastupdate == "" || newValues[0].lastupdate == null) ? DateTime.MinValue : DateTime.Parse(newValues[0].lastupdate),
                        @subs_ID = (newValues[0].subscriberid == string.Empty || newValues[0].subscriberid == "" || newValues[0].subscriberid == null) ? 0 : Convert.ToInt32(newValues[0].subscriberid),
                        @trans_ID = (newValues[0].paymentref == string.Empty || newValues[0].paymentref == "" || newValues[0].paymentref == null) ? null : newValues[0].paymentref,
                        @last_name = (newValues[0].fullname == string.Empty || newValues[0].paymentref == "" || newValues[0].paymentref == null) ? null : newValues[0].paymentref,
                        @sort_code = (newValues[0].sort_code == string.Empty || newValues[0].sort_code == "" || newValues[0].sort_code == null) ? null : newValues[0].sort_code,
                        @account_number = (newValues[0].account_number == string.Empty || newValues[0].account_number == "" || newValues[0].account_number == null) ? null : newValues[0].account_number,
                        @account_name = (newValues[0].accountname == string.Empty || newValues[0].accountname == "" || newValues[0].accountname == null) ? null : newValues[0].accountname,
                        @plan = (newValues[0].orderselected == string.Empty || newValues[0].orderselected == "" || newValues[0].orderselected == null) ? null : newValues[0].orderselected,
                        @DD_status = DD_status,
                        @updateby = "Mundio",
                        @ICCID = (newValues[0].iccid == string.Empty || newValues[0].iccid == "" || newValues[0].iccid == null) ? null : newValues[0].iccid,
                        @email = (newValues[0].email == string.Empty || newValues[0].email == "" || newValues[0].email == null) ? null : newValues[0].email,
                        @opsid = (newValues[0].opsid == string.Empty || newValues[0].opsid == "" || newValues[0].opsid == null) ? 0 : Convert.ToInt32(newValues[0].opsid),
                        @suspendType = 0,
                        @freesimid = (newValues[0].freesimid == string.Empty || newValues[0].freesimid == "" || newValues[0].freesimid == null) ? 0 : Convert.ToInt32(newValues[0].freesimid),
                        @reason = ""
                    }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.PMBOOutput>();
                }
            }
            catch (SqlException ex)
            {
                Log.Error("c3pm_dd_set_status : " + ex.Message);
                throw new Exception("c3pm_dd_set_status   :  " + ex.Message); ;
            }
        }

        public static IEnumerable<CRM_API.Models.PMBOOutput> SavePMBOLog(CRM_API.Models.log objLogMng)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.MCMCRMConnection))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.PMBOOutput>("c3pm_dd_operation_set", new
                    {
                        @operation = objLogMng.operation,
                        @status = objLogMng.status,
                        @userid = objLogMng.userid,
                        @user_name = objLogMng.user_name,
                        @operationid = objLogMng.operationid,
                        @operation_name = objLogMng.operation_name,
                        @total_rows = objLogMng.total_rows,
                        @filename = objLogMng.filename,
                        @notes = objLogMng.notes,
                        @opsid = objLogMng.opsid
                    }, commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.PMBOOutput>();
                }
            }
            catch (SqlException ex)
            {
                Log.Error("c3pm_dd_operation_set : " + ex.Message);
                throw new Exception("c3pm_dd_operation_set :  " + ex.Message); ;
            }
        }


        public static IEnumerable<CRM_API.Models.SetPaymentOutput> SetPaymentStatus(CRM_API.Models.log objLogMng)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.MCMCRMConnection))
                {
                   
                    conn.Open();
                    
                    var result = conn.Query<CRM_API.Models.SetPaymentOutput>("update_payment_status_for_DD_collection_v1", new
                    {
                        @updated_by = objLogMng.user_name

                    }, commandType: CommandType.StoredProcedure, commandTimeout: 5600);
                    return result ?? new List<CRM_API.Models.SetPaymentOutput>();
                }
            }
            catch (SqlException ex)
            {
                Log.Error("update_payment_status_for_DD_collection_v1 : " + ex.Message);
                throw new Exception("update_payment_status_for_DD_collection_v1 :  " + ex.Message);
            }
        }


        public static IEnumerable<CRM_API.Models.ManualMarkUploadCF> getManualMarkUploadCollectionFailure(int UpldID, string UpldBy)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.MCMCRMConnection))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.ManualMarkUploadCF>("Mark_Collection_Failure_Payment_DD_Status", new
                    {
                        @upload_id = UpldID,
                        @updated_by = UpldBy
                    }, commandType: CommandType.StoredProcedure, commandTimeout:5600);

                    return result ?? new List<CRM_API.Models.ManualMarkUploadCF>();
                }
            }
            catch (SqlException ex)
            {
                Log.Error("Mark_Collection_Failure_Payment_DD_Status : " + ex.Message);
                throw new Exception("Mark_Collection_Failure_Payment_DD_Status :  " + ex.Message);
            }
        }
        public static IEnumerable<CRM_API.Models.ManualMarkUploadCF> getManualMarkUploadDDCancellation(int UpldID, string UpldBy)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.MCMCRMConnection))
                {
                    conn.Open();
                   // var result = conn.Query<CRM_API.Models.ManualMarkUploadCF>("Mark_DD_Cancellation_DD_Status", new
                    var result = conn.Query<CRM_API.Models.ManualMarkUploadCF>("Mark_DD_Cancellation_DD_Status_v2", new
                    {
                        @Upload_Id = UpldID,
                        @UploadBy = UpldBy
                    }, commandType: CommandType.StoredProcedure, commandTimeout:5600);

                    return result ?? new List<CRM_API.Models.ManualMarkUploadCF>();
                }
            }
            catch (SqlException ex)
            {
                Log.Error("Mark_DD_Cancellation_DD_Status : " + ex.Message);
                throw new Exception("Mark_DD_Cancellation_DD_Status :  " + ex.Message);
            }
        }
        public static List<CRM_API.Models.DDViewNProcessFailure> GetDataForViewNProcessFailure(string ddYear, string ddMonth, string ddStatus, int? ddCustID, string ddPaymtStatus, string ddMobileNo, string ddConnStatus, string ddPaymentRef, string ddPayMode, string sitecode)
        {
            try
            {

                string ConnStr = "";
                if (sitecode.ToString().ToUpper() == "VMUK")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnection.ToString();
                }
                else if (sitecode.ToString().ToUpper() == "VMNL")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnectionVMNL.ToString();
                }
                else if (sitecode.ToString().ToUpper() == "VMFR")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnectionVMFR.ToString();
                }
                else if (sitecode.ToString().ToUpper() == "VMAT")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnectionVMAT.ToString();
                }
                else
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnection.ToString();
                }

                using (var conn = new SqlConnection(ConnStr))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.DDViewNProcessFailure>(
                                           //"view_and_process_failure_fetch_data",
                                           //"view_and_process_failure_fetch_data_v1",
                                           "view_and_process_failure_fetch_data_v2",
                                           new
                                           {
                                             @ddYear = (ddYear == null ? "": ddYear),
                                             @ddMonth = (ddMonth == null ? "" : ddMonth),
                                             @ddMobileNo = (ddMobileNo == null ? "" : ddMobileNo),
                                             @ddPaymentRef = (ddPaymentRef == null ? "" : ddPaymentRef),
                                             @ddStatus = (ddStatus == null ? "" : ddStatus),
                                             @ddConnStatus = (ddConnStatus == null ? "" : ddConnStatus),
                                             @ddPaymtStatus = (ddPaymtStatus == null ? "" : ddPaymtStatus),
                                             @ddCustID = (ddCustID == null? 0: ddCustID),
                                             @ddPayMode = (ddPayMode == null ? "" : ddPayMode)
                                           }, commandType: CommandType.StoredProcedure, commandTimeout:0);

                    return result.ToList() ?? new List<CRM_API.Models.DDViewNProcessFailure>();
                }
            }
            catch (SqlException ex)
            {
                Log.Error("view_and_process_failure_fetch_data_v2 : " + ex.Message);
                throw new Exception("view_and_process_failure_fetch_data_v2  :  " + ex.Message);
            }
        }


        public static List<CRM_API.Models.DDViewNProcessFailure> GetDataForViewNProcessFailureForDownload(string ddYear, string ddMonth, string ddStatus, string ddCustID, string ddPaymtStatus, string ddMobileNo, string ddConnStatus, string ddPaymentRef, string ddPayMode)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.MCMCRMConnection))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.DDViewNProcessFailure>(
                        //"view_and_process_failure_fetch_data",
                        //"view_and_process_failure_fetch_data_v1",
                                           "view_and_process_failure_fetch_data_download",
                                           new
                                           {
                                               //@ddYear = (ddYear == null ? "" : ddYear),
                                               //@ddMonth = (ddMonth == null ? "" : ddMonth),
                                               //@ddMobileNo = (ddMobileNo == null ? "" : ddMobileNo),
                                               @ddPaymentRef = ddPaymentRef //(ddPaymentRef == null ? "" : ddPaymentRef),
                                               //@ddStatus = (ddStatus == null ? "" : ddStatus),
                                               //@ddConnStatus = (ddConnStatus == null ? "" : ddConnStatus),
                                               //@ddPaymtStatus = (ddPaymtStatus == null ? "" : ddPaymtStatus),
                                               //@ddCustID = (ddCustID == null ? "" : ddCustID),
                                               //@ddPayMode = (ddPayMode == null ? "" : ddPayMode)
                                           }, commandType: CommandType.StoredProcedure, commandTimeout: 0);

                    return result.ToList() ?? new List<CRM_API.Models.DDViewNProcessFailure>();
                }
            }
            catch (SqlException ex)
            {
                Log.Error("view_and_process_failure_fetch_data_download : " + ex.Message);
                throw new Exception("view_and_process_failure_fetch_data_download :  " + ex.Message);
            }
        }

        //Modified by BSK for Improvement : Jira CRMT-141
        public static List<CRM_API.Models.ViewNProcessFailureMessage> SuspendService(string updatedBy, string ddCustIDs,string sitecode)
        {
            try
            {
                string ConnStr = "";
                if (sitecode.ToString().ToUpper() == "VMUK")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnection.ToString();
                }
                if (sitecode.ToString().ToUpper() == "VMNL")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnectionVMNL.ToString();
                }
                if (sitecode.ToString().ToUpper() == "VMFR")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnectionVMFR.ToString();
                }
                if (sitecode.ToString().ToUpper() == "VMAT")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnectionVMAT.ToString();
                }

                using (var conn = new SqlConnection(ConnStr))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.ViewNProcessFailureMessage>(
                                           "Suspend_Active_DD",
                                           new
                                           {
                                            @Process_By = updatedBy,
                                            @Customer_Ids = ddCustIDs
                                           }, commandType: CommandType.StoredProcedure);

                    return result.ToList() ?? new List<CRM_API.Models.ViewNProcessFailureMessage>();
                }
            }
            catch (SqlException ex)
            {
                Log.Error("Suspend_Active_DD : " + ex.Message);
                throw new Exception("Suspend_Active_DD  :  " + ex.Message);
            }
        }

        public static List<CRM_API.Models.ViewNProcessFailureMessage> ActivateService(string updatedBy, string ddCustIDs,string sitecode)
        {
            try
            {

                string ConnStr = "";
                if (sitecode.ToString().ToUpper() == "VMUK")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnection.ToString();
                }
                else if (sitecode.ToString().ToUpper() == "VMNL")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnectionVMNL.ToString();
                }
                else if (sitecode.ToString().ToUpper() == "VMFR")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnectionVMFR.ToString();
                }
                else if (sitecode.ToString().ToUpper() == "VMAT")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnectionVMAT.ToString();
                }
                else
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnectionVMAT.ToString();
                }

                using (var conn = new SqlConnection(ConnStr))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.ViewNProcessFailureMessage>(
                                           "Activate_Suspended_DD",
                                           new
                                           {
                                               @Process_By = updatedBy,
                                               @Customer_Ids = ddCustIDs
                                           }, commandType: CommandType.StoredProcedure);

                    return result.ToList() ?? new List<CRM_API.Models.ViewNProcessFailureMessage>();
                }
            }
            catch (SqlException ex)
            {
                Log.Error("Activate_Suspended_DD : " + ex.Message);
                throw new Exception("Activate_Suspended_DD  :  " + ex.Message);
            }
        }

        public static IEnumerable<CRM_API.Models.SMSMobileNos> GetCollectionFailureMobileNumbers(int UpldID)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.MCMCRMConnection))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.SMSMobileNos>("Collection_Failure_SMS_Mobile_Nos", new
                    {
                        @Upload_ID = UpldID,
                    }, commandType: CommandType.StoredProcedure, commandTimeout: 5600);

                    return result ?? new List<CRM_API.Models.SMSMobileNos>();
                }
            }
            catch (SqlException ex)
            {
                Log.Error("Collection_Failure_SMS_Mobile_Nos : " + ex.Message);
                throw new Exception("Collection_Failure_SMS_Mobile_Nos :  " + ex.Message);
            }
        }


        public static IEnumerable<CRM_API.Models.SMSMobileNosEmailAccs> GetMobileNumbersAndEmail(string Batch_Id, string sitecode)
        {
            try
            {

                string ConnStr = "";
                if (sitecode.ToString().ToUpper() == "VMUK")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnection.ToString();
                }
                else if (sitecode.ToString().ToUpper() == "VMNL")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnectionVMNL.ToString();
                }
                else if (sitecode.ToString().ToUpper() == "VMFR")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnectionVMFR.ToString();
                }
                else if (sitecode.ToString().ToUpper() == "VMAT")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnectionVMAT.ToString();
                }
                else
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnection.ToString();
                }


                using (var conn = new SqlConnection(ConnStr))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.SMSMobileNosEmailAccs>("Get_SMS_Mobile_Nos", new
                    {
                        @Batch_Id = Batch_Id,
                    }, commandType: CommandType.StoredProcedure, commandTimeout: 5600);

                    return result ?? new List<CRM_API.Models.SMSMobileNosEmailAccs>();
                }
            }
            catch (SqlException ex)
            {
                Log.Error("Get_SMS_Mobile_Nos : " + ex.Message);
                throw new Exception("Get_SMS_Mobile_Nos :  " + ex.Message);
            }
        }

        public static string ProcessPaymentCC(string ddCustIDs,string sitecode)
        {
            try
            {
                string ConnStr = "";
                if (sitecode.ToString().ToUpper() == "VMUK")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnection.ToString();
                }
                if (sitecode.ToString().ToUpper() == "VMNL")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnectionVMNL.ToString();
                }
                if (sitecode.ToString().ToUpper() == "VMFR")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnectionVMFR.ToString();
                }
                if (sitecode.ToString().ToUpper() == "VMAT")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnectionVMAT.ToString();
                }

                using (var conn = new SqlConnection(ConnStr))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.ProcessPaymentCC_MESG>(
                                           "Process_Payment_CC",
                                           new
                                           {
                                               @Customers = ddCustIDs
                                           }, commandType: CommandType.StoredProcedure);
                    List<CRM_API.Models.ProcessPaymentCC_MESG> res = (List<CRM_API.Models.ProcessPaymentCC_MESG>)result;
                    return res[0].TotalcustomersMarked;

                }
            }
            catch (SqlException ex)
            {
                Log.Error("Process_Payment_CC : " + ex.Message);
                throw new Exception("Process_Payment_CC  :  " + ex.Message);
            }
        }

        public static string MoveNewCustomersToSetupInProgress(string CustId_AccNo_SortCode)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.MCMCRMConnection))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.MoveToSetUpInProgressMesg>(
                                           "New_Customer_SetUp_InProgress",
                                           new
                                           {
                                               @CustId_AccNo_SortCd = CustId_AccNo_SortCode
                                           }, commandType: CommandType.StoredProcedure);
                    List<CRM_API.Models.MoveToSetUpInProgressMesg> res = (List<CRM_API.Models.MoveToSetUpInProgressMesg>)result;

                    return res[0].Totalcustomerschanged;
                }
            }
            catch (SqlException ex)
            {
                Log.Error("New_Customer_SetUp_InProgress : " + ex.Message);
                throw new Exception("New_Customer_SetUp_InProgress  :  " + ex.Message);
            }
        }

        //Modified by BSK for Improvement : Jira CRMT-141
        public static IEnumerable<CRM_API.Models.MoveToSetUpInProgressMesg> UpdateSetupInProgress(CRM_API.Models.SetupInProgress LOV)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.MCMCRMConnection))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.MoveToSetUpInProgressMesg>(
                                           "New_Customer_SetUp_InProgress",
                                           new
                                           {
                                               @CustId_AccNo_SortCd = LOV.CustId_AccNo_SortCode
                                           }, commandType: CommandType.StoredProcedure);
                    List<CRM_API.Models.MoveToSetUpInProgressMesg> res = (List<CRM_API.Models.MoveToSetUpInProgressMesg>)result;
                    return result ?? new List<CRM_API.Models.MoveToSetUpInProgressMesg>();

                }
            }
            catch (SqlException ex)
            {
                Log.Error("New_Customer_SetUp_InProgress : " + ex.Message);
                throw new Exception("New_Customer_SetUp_InProgress  :  " + ex.Message);
            }
        }



        public static string IsDirectDebitActive(string MobileNo)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.MCMCRMConnection))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.DirectDebitActiveMesg>(
                                           "IsDirectDebitActive",
                                           new
                                           {
                                               @MobileNo = MobileNo
                                           }, commandType: CommandType.StoredProcedure);
                    List<CRM_API.Models.DirectDebitActiveMesg> res = (List<CRM_API.Models.DirectDebitActiveMesg>)result;

                    return res[0].ActiveDD;
                }
            }
            catch (SqlException ex)
            {
                Log.Error("IsDirectDebitActive : " + ex.Message);
                throw new Exception("IsDirectDebitActive  :  " + ex.Message);
            }
        }
        public static List<CRM_API.Models.PaymentMode> GetPaymentMode()
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.MCMCRMConnection))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.PaymentMode>(
                                           "Get_Payment_Mode",
                                           new
                                           {
                                           }, commandType: CommandType.StoredProcedure, commandTimeout: 5600);

                    return result.ToList() ?? new List<CRM_API.Models.PaymentMode>();
                }
            }
            catch (SqlException ex)
            {
                Log.Error("Get_Payment_Mode : " + ex.Message);
                throw new Exception("Get_Payment_Mode  :  " + ex.Message);
            }
        }

        public static string GetCustomerID(string MobileNo)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.MCMCRMConnection))
                {
                    conn.Open();
                    var result =  conn.Query<string>(
                                           "GetCustomerId",
                                           new
                                           {
                                              @MobileNo = MobileNo
                                           }, commandType: CommandType.StoredProcedure, commandTimeout: 5600);

                    return result.ElementAt(0).ToString();
                }
            }
            catch (SqlException ex)
            {
                Log.Error("GetCustomerId : " + ex.Message);
                throw new Exception("GetCustomerId  :  " + ex.Message);
            }
        }


    }
}
