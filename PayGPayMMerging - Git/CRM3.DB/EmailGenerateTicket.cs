﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;
using System.IO;
using System.Net.Mail;
using System.Configuration;

namespace CRM_API.DB
{
    public class EmailGenerateTicket
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public static IEnumerable<CRM_API.Models.GenerateTicketOutput> GenerateTicket(EmailSettingsModel Model)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.GenerateTicketOutput>(
                            "email_queue_generate_ticket", new
                            {
                                @customer_name = Model.CustomerName,
                                @product_code = Model.product_code,
                                @fk_queue_id = Model.pkqueueid,
                                @mobileno = Model.MobileNo,
                                @issue_type = Model.TicketType,
                                @Issue_Function = Model.Category,
                                @Issue_Complaint = Model.SubCategory,
                                @issue_status = Model.Status,
                                @descr = Model.Description,
                                @esclating_user = Model.EscalatingTeam,
                                @process_type = Model.process_type
                            }, commandType: CommandType.StoredProcedure);

                    //if (result != null && result.ElementAt(0).errcode == 0)
                    //{

                    //    SendGenerateTicketMail(Path.Combine(ConfigurationManager.AppSettings["QUEUEEMAILTEMPLATEPATH"], result.ElementAt(0).email_filename), Model.emailto, Model.EscalatingTeamName, result.ElementAt(0).email_sub);
                    //}
                    return result ?? new List<CRM_API.Models.GenerateTicketOutput>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("GenerateTicket() : " + ex.Message);
                throw new Exception("GenerateTicket() : " + ex.Message);
            }
        }

        public static IEnumerable<CRM_API.Models.ViewTicketOutput> ViewTicket(int queue_id)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.ViewTicketOutput>(
                            "email_view_generated_tickets", new
                            {
                                @queue_id = queue_id
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.ViewTicketOutput>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("ViewTicket() : " + ex.Message);
                throw new Exception("ViewTicket() : " + ex.Message);
            }
        }

        public static IEnumerable<CRM_API.Models.EscalationTeamInfo> GetEscalationTeamInfo(int role_id)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.EscalationTeamInfo>(
                            "email_queue_get_agent_info", new
                            {
                                @role_id = role_id == 0 ? 2 : role_id
                                //@role_id = -1
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.EscalationTeamInfo>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetEscalationTeamInfo() : " + ex.Message);
                throw new Exception("GetEscalationTeamInfo() : " + ex.Message);
            }
        }

        public static IEnumerable<CRM_API.Models.TicketTypeInfo> GetTicketTypeInfo(string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.TicketTypeInfo>(
                            "email_get_ticket_issue_type", new
                            {
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.TicketTypeInfo>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetTicketTypeInfo() : " + ex.Message);
                throw new Exception("GetTicketTypeInfo() : " + ex.Message);
            }
        }

        public static IEnumerable<CRM_API.Models.CategoryInfo> GetCategoryInfo(string sitecode, int Issue_Id)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.CategoryInfo>(
                            "email_get_ticket_Issue_Function", new
                            {
                                @sitecode = sitecode,
                                @Issue_Id = Issue_Id
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.CategoryInfo>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetCategoryInfo() : " + ex.Message);
                throw new Exception("GetCategoryInfo() : " + ex.Message);
            }
        }

        public static IEnumerable<CRM_API.Models.SubCategoryInfo> GetSubCategoryInfo(string sitecode, int Function_Id)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.SubCategoryInfo>(
                            "email_get_ticket_Issue_complaints", new
                            {
                                @sitecode = sitecode,
                                @Function_Id = Function_Id
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.SubCategoryInfo>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetSubCategoryInfo() : " + ex.Message);
                throw new Exception("GetSubCategoryInfo() : " + ex.Message);
            }
        }

        public static IEnumerable<CRM_API.Models.StatusInfo> GetStatusInfo(int userid)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.StatusInfo>(
                            "email_get_queue_status", new
                            {
                                @userid = userid
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.StatusInfo>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetStatusInfo() : " + ex.Message);
                throw new Exception("GetStatusInfo() : " + ex.Message);
            }
        }

        private static void SendGenerateTicketMail(string mailLocation, string email, string fullname, string subject)
        {
            try
            {
                StreamReader mailContent_file = new StreamReader(mailLocation);
                string mailContent = mailContent_file.ReadToEnd();
                mailContent_file.Close();
                mailContent = mailContent
                .Replace("*|FNAME|*", fullname)
                .Replace("*|SUBJECT|*", subject);
                //.Replace("*|BODY|*", body);

                // Send the email
                MailAddress mailFrom = new MailAddress(ConfigurationManager.AppSettings["QUEUEEMAILMAILFROM"], ConfigurationManager.AppSettings["QUEUEEMAILMAILFROMDISPLAYNAME"]);
                MailAddressCollection mailTo = new MailAddressCollection();
                mailTo.Add(new MailAddress(email, string.Format("{0}", fullname)));
                CRM_API.Helpers.EmailSending.Send(true, mailFrom, mailTo, null, null, subject, mailContent);
            }
            catch (Exception ex)
            {
                Log.Error("SendGenerateTicketMail() : " + ex.Message);
                throw new Exception("SendGenerateTicketMail() : " + ex.Message);
            }
        }
        public static IEnumerable<CRM_API.Models.PriorityOutput> Priority(string queue_id)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.PriorityOutput>(
                            "crm_get_issue_priority", new
                            {
                                @queue_id = queue_id
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.PriorityOutput>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("ViewTicket() : " + ex.Message);
                throw new Exception("ViewTicket() : " + ex.Message);
            }
        }
    }
}
