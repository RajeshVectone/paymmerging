﻿using Dapper;
using System.Linq;
using System.Web;
using NLog;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace CRM_API.DB.Porting.AT
{
    public class SProc
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public static bool DateIsWorkingDay(DateTime date)
        {
            string sql = "CRM3_date_is_working_day";
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var p = new DynamicParameters();
                    p.Add("@date", date, DbType.DateTime, ParameterDirection.Input);
                    p.Add("@is_working_day", 0, DbType.Int32, ParameterDirection.Output, int.MaxValue);
                    var count = conn.Execute(sql, p);

                    return p.Get<int>("@is_working_day") == 0 ? false : true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL CRM_API.DB.Porting.AU.DateIsWorkingDay: " + ex.Message);
            }
        }

        public static DateTime GetDatePlusXWorkingDay(DateTime date, int xWorkingDays, string sitecode)
        {
            string sql = "crm3_get_date_plus_x_working_day";
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var p = new DynamicParameters();
                    p.Add("@date", date.ToString("yyyy-MM-dd"), DbType.DateTime, ParameterDirection.Input);
                    p.Add("@x", xWorkingDays, DbType.Int32, ParameterDirection.Input, int.MaxValue);
                    p.Add("@date_plus_x", 0, DbType.DateTime, ParameterDirection.Output);
                    p.Add("@sitecode", sitecode);
                    var count = conn.Execute(sql, p);
                    return p.Get<DateTime>("@date_plus_x");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL CRM_API.DB.Porting.AU.GetDatePlusXWorkingDay: " + ex.Message);
            }
        }

        public static bool CheckMsisdnAtMvnoAccount(String msisdn)
        {
            string sql = "CRM3_usp_check_msisdn_at_mvno_account";
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var count = conn.Execute(sql, new { @BarabluMSISDN = msisdn });
                    if (count > 0)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            { throw new Exception("BL CRM_API.DB.Porting.AU.CheckMsisdnAtMvnoAccount: " + ex.Message); }
        }

        public static CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Porting CheckPortingCode(string portingCode, string sitecode, ref bool IsValid)
        {
            string sql = "crm3_usp_check_portingcode";
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var p = new DynamicParameters();
                    p.Add("@portingCode", portingCode, DbType.String, ParameterDirection.Input, 17);
                    p.Add("@desiredPortingDate", string.Empty, DbType.String, ParameterDirection.Output, 10);
                    p.Add("@PortingDate", string.Empty, DbType.String, ParameterDirection.Output, 12);
                    p.Add("@timestamp", string.Empty, DbType.String, ParameterDirection.Output, 25);
                    p.Add("@portingStatus", 0, DbType.Int32, ParameterDirection.Output, int.MaxValue);
                    p.Add("@sentStatus", 0, DbType.Int32, ParameterDirection.Output, int.MaxValue);
                    p.Add("@sitecode", sitecode, DbType.String, ParameterDirection.Input, 17);
                    var count = conn.Execute(sql, p);
                    if (count > 0)
                    {
                        var tempPorting = new Models.Porting.AT.SoapReceiver.Transactions.Porting()
                        {
                            PortingCode = portingCode,
                            DesiredPortDate = p.Get<string>("@desiredPortingDate"),
                            PortingDate = p.Get<string>("@PortingDate"),
                            TimeStamp = p.Get<string>("@timestamp"),
                            PortingStatus = p.Get<int>("@portingStatus"),
                            SentStatus = p.Get<int>("@sentStatus")
                        };
                        if (tempPorting.DesiredPortDate != null && tempPorting.DesiredPortDate != "") // contains duplicate data
                        {
                            tempPorting.MnpExp = tempPorting.PortingCode.Substring(0, 3);
                            if (tempPorting.SentStatus == 0)
                            {
                                tempPorting.DuplicateMessage = "Porting code already used, but request porting failed to sent.";
                                tempPorting.PortStatus = "Porting requested, not received by " + tempPorting.MnpExp;
                                tempPorting.DuplicateOverwrite = "Select Overwrite to resend request porting with new input data or select Close to cancel request porting and edit input data.";
                            }
                            else if (tempPorting.PortingStatus == 4)
                            {
                                tempPorting.DuplicateMessage = "Porting code already used and the request porting already sent.";
                                tempPorting.PortStatus = "Porting requested, not responded yet by " + tempPorting.MnpExp;
                                tempPorting.DuplicateOverwrite = "Transaction cannot be resend, waiting for response from " + tempPorting.MnpExp + ".";
                            }
                            else if (tempPorting.PortingStatus == 5)
                            {
                                tempPorting.DuplicateMessage = "Porting code already used and the request porting responded OK.";
                                tempPorting.PortStatus = "Porting requested, responded OK by " + tempPorting.MnpExp;
                                tempPorting.DuplicateOverwrite = tempPorting.MnpExp + " replied with OK response. "
                                    + "The transaction cannot be resend, but can be cancelled at Request Cancel menu if needed.";
                            }
                            else if (tempPorting.PortingStatus == 6)
                            {
                                tempPorting.DuplicateMessage = "Porting code already used and the request porting responded not OK.";
                                tempPorting.PortStatus = "Porting requested, responded not OK by " + tempPorting.MnpExp;
                                tempPorting.DuplicateOverwrite = "Select Overwrite to resend request porting with new input data or select Close to cancel request porting and edit input data.";
                            }
                            IsValid = false;
                        }
                        else // no duplicate, safe to save
                        {
                            IsValid = true;
                        }
                        return tempPorting;
                    }
                    else
                        return null;
                }
            }
            catch (Exception ex)
            { throw new Exception("BL CRM_API.DB.Porting.AU.CheckPortingCode: " + ex.Message); }
        }

        public static Common.ErrCodeMsg InsertPortingIn(CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Transactions oTrans,
            CRM_API.Models.Porting.AT.SoapReceiver.Transactions.NuvInfo oNuv, CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Porting oPort, string sitecode)
        {
            string sql = "CRM3_usp_insert_porting";
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var count = conn.Execute(sql, new
                    {
                        @RequestID = oTrans.RequestId,
                        @title = oNuv.Prefix,
                        @FirstName = oNuv.FirstName,
                        @LastName = oNuv.LastName,
                        @Email = oNuv.Email,
                        @CustomerID = int.Parse(oNuv.CustomerId),
                        @BirthDate = oNuv.BirthDate,
                        @CompanyName = oNuv.CompanyName,
                        @HouseNo = oNuv.HouseNo,
                        @Address1 = oNuv.Address,
                        @Address2 = oNuv.Address2,
                        @city = oNuv.City,
                        @postcode = oNuv.Postcode,
                        @state = oNuv.State,
                        @countrycode = oNuv.Country,
                        @telephone = oNuv.Telephone,
                        @BarabluMSISDN = oNuv.Msisdn,
                        @fax = oNuv.Fax,
                        @subscribertype = oNuv.SubType,
                        @PortingType = oPort.PortingType,
                        @MessStatus = oPort.MsgStatus,
                        @PortingCode = oPort.PortingCode,
                        @Gcode = oPort.GCode,
                        @Gdesc = oPort.GDesc,
                        @Lcode = oPort.LCode,
                        @LDesc = oPort.LDesc,
                        @OriMsisdn = oPort.Msisdn,
                        @voicemail = oPort.VoiceMail,
                        @desiredportdate = oPort.DesiredPortDate,
                        @PortingState = oPort.PortingState,
                        @Timestamp = oTrans.TimeStamp,
                        @Sender = oTrans.Sender,
                        @Receiver = oTrans.Receiver,
                        @MnpImp = oTrans.MnpImp,
                        @MnpExp = oTrans.MnpExp,
                        @EnvFlag = oTrans.EnvFlag,
                        @Version = oTrans.Version,
                        @PortingStatus = oTrans.PortingStatus,
                        @PdfName = oNuv.PdfName,
                        @Sitecode = sitecode
                    }, null, null, CommandType.StoredProcedure);
                    if (count > 0)
                        return new Common.ErrCodeMsg() { errcode = 0, errmsg = "Porting Saved" };
                    else
                        return new Common.ErrCodeMsg() { errcode = -1, errmsg = "Porting Insert Failed" };
                }
            }
            catch (Exception e)
            {
                var msg = "SProc InsertPortingIn: " + e.Message;
                Log.Debug(msg);
                throw new Exception(msg);
            }
        }

        public static void GetAndCheckReqPorting(CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Porting oPort,
            ref CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Transactions oTrans,
            ref CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Mobile arlMbl,
            string sitecode)
        {
            string sql = "crm3_sp_check_porting";
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var p = new DynamicParameters();
                    p.Add("@Msisdn", string.Empty, DbType.String, ParameterDirection.Input, 20);
                    p.Add("@PortingCode", string.Empty, DbType.String, ParameterDirection.Input, 17);
                    p.Add("@DesiredPortDate", string.Empty, DbType.String, ParameterDirection.Input, 10);
                    p.Add("@LCode", string.Empty, DbType.Int32, ParameterDirection.Output, int.MaxValue);
                    p.Add("@GCode", string.Empty, DbType.Int32, ParameterDirection.Output, int.MaxValue);
                    p.Add("@isvoc", string.Empty, DbType.Int32, ParameterDirection.Output, int.MaxValue);
                    p.Add("@pairNumber", string.Empty, DbType.String, ParameterDirection.Output, 20);
                    p.Add("@sitecode", string.Empty, DbType.String, ParameterDirection.Input);

                    var resx = conn.Execute(sql, p, null, null, CommandType.StoredProcedure);
                    if (resx != 0)
                    {
                        int isVoc = p.Get<int>("@isvoc");
                        int LCode = p.Get<int>("@LCode");
                        int GCode = p.Get<int>("@GCode");
                        string pairNumber = p.Get<string>("@pairNumber");

                        if (isVoc == -1)
                        {
                            arlMbl.LCode = LCode;
                            oPort.GCode = GCode;
                        }
                        else
                        {
                            int exists = 0;
                            if (arlMbl.Msisdn == pairNumber)
                            {
                                exists = 1;
                            }
                            if (exists == 0) // pairing number doesn't exist
                            {
                                arlMbl.LCode = (isVoc == 0 ? 117 : 116); //116:missing main calling number ; 117:missing voice mail number
                                oPort.GCode = 115; //15:porting refused (at least 1 error ocurred)
                            }
                            else
                            {
                                arlMbl.LCode = LCode;
                                oPort.GCode = GCode;
                                arlMbl.PortingCode = oPort.PortingCode;
                            }
                        }
                    }
                    arlMbl.LDesc = CRM_API.Helpers.Porting.AT.EnumErr.ToText(arlMbl.LCode);
                    oPort.GDesc = CRM_API.Helpers.Porting.AT.EnumErr.ToText(oPort.GCode);
                }
                oPort.PortingType = 2;
                oPort.CancelCode = GetCancellationCode(oPort, sitecode);
                oPort.PortingStatus = 104;
            }
            catch (Exception ex)
            { throw new Exception("BL CRM_API.DB.Porting.AU.GetAndCheckReqPorting: " + ex.Message); }
        }

        public static string GetCancellationCode(CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Porting objPorting, string sitecode)
        {
            try
            {
                string sql = "get_cancellation_code";
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var p = new DynamicParameters();
                    p.Add("@PortingCode", objPorting.PortingCode, DbType.String, ParameterDirection.Input, 17);
                    p.Add("@sitecode", sitecode, DbType.String, ParameterDirection.Input, 3);
                    p.Add("@CancelCode", string.Empty, DbType.String, ParameterDirection.Output, 50);
                    var resx = conn.Execute(sql, p, null, null, CommandType.StoredProcedure);
                    if (resx != 0)
                    {
                        objPorting.CancelCode = p.Get<string>("@CancelCode");
                    }
                }
                return objPorting.CancelCode;
            }
            catch (Exception e)
            {
                { throw new Exception("BL CRM_API.DB.Porting.AU.GetCancellationCode: " + e.Message); }
            }
        }

        // Summary:
        //     Austria Porting UpdateTransactions
        // Parameters:
        //   template:
        //     SentStatus : 1=Recieved, 2=Sent
        public static bool UpdateTransactions(CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Transactions oTrx,
            string PortingCode, int SentStatus, string sitecode)
        {
            try
            {
                string sql = "crm3_sp_update_transactions";
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var p = new DynamicParameters();
                    p.Add("@RequestId", oTrx.RequestId, DbType.String, ParameterDirection.Input, 15);
                    p.Add("@SentStatus", SentStatus, DbType.Int32, ParameterDirection.Input, int.MaxValue);
                    p.Add("@portingstatus", oTrx.PortingStatus, DbType.Int32, ParameterDirection.Input, int.MaxValue);
                    p.Add("@portingcode", PortingCode, DbType.String, ParameterDirection.Input, 17);
                    p.Add("@sitecode", sitecode, DbType.String, ParameterDirection.Input, 3);
                    var resx = conn.Execute(sql, p, null, null, CommandType.StoredProcedure);
                    if (resx == 0)
                        throw new Exception("Update Failed");
                }
                return true;
            }
            catch (Exception e)
            {
                { throw new Exception("BL CRM_API.DB.Porting.AU.UpdateTransactions: " + e.Message); }
            }
        }

        public static IEnumerable<Models.Porting.AT.PortingList> GetCancellationList(string PortingCode, string Msisdn,string Sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Models.Porting.AT.PortingList>(
                            "crm3_usp_get_port_tocancel", new
                            {
                                @portingcode = PortingCode,
                                @msisdn = Msisdn,
                                @sitecode = Sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<Models.Porting.AT.PortingList>();
                }
            }
            catch (Exception ex)
            {
                { throw new Exception("BL CRM_API.DB.Porting.AU.GetCancellationList: " + ex.Message); }
            }
        }

        public static Models.Common.ErrCodeMsg RequestCancel(CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Transactions oTrx,
            CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Cancel oCan)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Models.Common.ErrCodeMsg>(
                            "crm3_usp_insert_port_cancel", new
                            {
                                @requestid = oTrx.RequestId,
                                @sender = oTrx.Sender,
                                @receiver = oTrx.Receiver,
                                @portingstate = oTrx.PortingState,
                                @portingstatus = oTrx.PortingStatus,
                                @portingcode = oCan.PortingCode,
                                @cancelcode = oCan.CancelCode,
                                @PortingType = oCan.PortingType,
                                @MsgStatus = oCan.MsgStatus,
                                @gcode = oCan.GCode,
                                @gdesc = oCan.GDesc,
                                @envflag = oTrx.EnvFlag,
                                @version = oCan.Version,
                                @timestamp = oCan.TimeStamp
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    return result ;
                }
            }
            catch (Exception ex)
            {
                { throw new Exception("BL CRM_API.DB.Porting.AU.RequestCancel: " + ex.Message); }
            }
        }

        public static CRM_API.Models.Porting.AT.SoapReceiver.Transactions.ReqCancelType SendReqCancel(CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Transactions oTrx,
            CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Cancel oCan)
        {
            CRM_API.Models.Porting.AT.SoapReceiver.Transactions.ReqCancelType oCancelType = new CRM_API.Models.Porting.AT.SoapReceiver.Transactions.ReqCancelType();
            oCancelType.Header = SetObjectHeader(oTrx);
            oCancelType.CancellationCode = Convert.ToInt32(oCan.CancelCode);
            return oCancelType;
        }

        public static Models.Common.ErrCodeMsg UpdateTransactionsCancel(string ReqID, string portingCode, int PortingStatus, int SentStatus)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Models.Common.ErrCodeMsg>(
                            "crm3_sp_update_transactions", new
                            {
                                @RequestId = ReqID,
                                @SentStatus = SentStatus,
                                @portingstatus = PortingStatus,
                                @portingcode = portingCode
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    return result;
                }
            }
            catch (Exception ex)
            {
                { throw new Exception("BL CRM_API.DB.Porting.AU.RequestCancel: " + ex.Message); }
            }
        }

        private static CRM_API.Models.Porting.AT.SoapReceiver.Transactions.HeaderType SetObjectHeader(CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Transactions oTrans)
        {
            CRM_API.Models.Porting.AT.SoapReceiver.Transactions.HeaderType oHeader = new CRM_API.Models.Porting.AT.SoapReceiver.Transactions.HeaderType();
            oHeader.RequestID = Convert.ToInt64(oTrans.RequestId);
            oHeader.Receiver = oTrans.Receiver;
            oHeader.Sender = oTrans.Sender;
            oHeader.Version = oTrans.Version;
            oHeader.EnvFlag = oTrans.EnvFlag;
            oHeader.Timestamp = Convert.ToDateTime(oTrans.TimeStamp);
            return oHeader;
        }

        public static IEnumerable<Models.Porting.AT.PortingList> GetReceipientListNuevo(string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Models.Porting.AT.PortingList>(
                            "crm3_get_mnpref", new
                            {
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure).Where(x => x.ReceipientOperator != "BMA");

                    return result ?? new List<Models.Porting.AT.PortingList>();
                }
            }
            catch (Exception ex)
            {
                { throw new Exception("BL CRM_API.DB.Porting.AU.GetReceipientListt: " + ex.Message); }
            }
        }

        public static Models.Common.ErrCodeMsg insertTransaction(CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Transactions Trans, string Msgtype, int Status, string portingCode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Models.Common.ErrCodeMsg>(
                            "crm3_usp_insert_transactions", new
                            {
                                @RequestId = Trans.RequestId,
                                @Sender = Trans.Sender,
                                @Receiver = Trans.Receiver,
                                @MnpImp = Trans.MnpImp,
                                @MnpExp = Trans.MnpExp,
                                @EnvFlag = Trans.EnvFlag,
                                @TimeStamp = Trans.TimeStamp,
                                @Version = Trans.Version,
                                @PortingStatus = Trans.PortingStatus,
                                @MsgType = Msgtype,
                                @Status = Status,
                                @PortingCode = portingCode,
                                @ReceivedRequestId = Trans.ReceivedRequestId
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    if (Status == 2)
                        result = SentTransaction(Trans, Msgtype);

                    return result;
                }
            }
            catch (Exception ex)
            {
                { throw new Exception("BL CRM_API.DB.Porting.AU.InsertTransaction: " + ex.Message); }
            }
        }

        public static Models.Common.ErrCodeMsg SentTransaction(CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Transactions Trans, string Msgtype)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Models.Common.ErrCodeMsg>(
                            "crm3_set_sent_status", new
                            {
                                @RequestId = Trans.RequestId,
                                @MsgType = Trans.Sender,

                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    return result;
                }
            }
            catch (Exception ex)
            {
                { throw new Exception("BL CRM_API.DB.Porting.AU.SentTransaction: " + ex.Message); }
            }
        }

        public static Models.Common.ErrCodeMsg InsertNuvInfo(CRM_API.Models.Porting.AT.SoapReceiver.Transactions.NuvInfo oNuv,List<CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Mobile> arlMbl)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Models.Common.ErrCodeMsg>(
                            "crm3_SP_INSERTNUV", new
                            {
                                @RequestId = oNuv.RequestId,
                                @FirstName = oNuv.FirstName,
                                @LastName = oNuv.LastName,
                                @Email = oNuv.Email,
                                @CustomerID = oNuv.CustomerId,
                                @BirthDate = oNuv.BirthDate,
                                @CompanyName = oNuv.CompanyName,
                                @PortingType = oNuv.PortingType,
                                @MessStatus = oNuv.MsgStatus,
                                @PDFName = oNuv.PdfName,
                                @PortingCode = oNuv.PortingCode,
                                @PUK = oNuv.Puk,
                                @GCode = oNuv.GCode,
                                @GDesc = oNuv.LastName,
                                @isvoc = oNuv.IsVoc
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    return result;
                }
            }
            catch (Exception ex)
            {
                { throw new Exception("BL CRM_API.DB.Porting.AU.InsertTransaction: " + ex.Message); }
            }
        }
    }
}