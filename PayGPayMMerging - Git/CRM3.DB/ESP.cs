﻿using CRM_API.Models;
using Dapper;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.DB.ESP
{
    public class SProc
    {
        public static IEnumerable<CRM_API.Models.ESPAccount> GetESPAccountInfo(string mobileno, string sitecode)
        {
            //crm3_crm_sim_swap
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.ESPAccount>(
                            "crm3_account_search_by_mobileno", new
                            {
                                @mobileno = mobileno,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.ESPAccount>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL GetESPAccountInfo: " + ex.Message);
            }

        }
    }
}
