﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;

namespace CRM_API.DB.PAYMPlanBundles
{
    public class SProc
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public static IEnumerable<PAYMPlanAccountInfo> GetAccountInfo(string mobileNo, string calledby)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.PMBOConnection))
                {
                    conn.Open();

                    var result = conn.Query<PAYMPlanAccountInfo>(
                            "c3pm_getplanaccountinfo ", new
                            {
                                @mobileno = mobileNo,
                                @calledby = calledby
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<PAYMPlanAccountInfo>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.PAYMPlanBundles.GetAccountInfo: {0}", ex.Message);
                throw new Exception("CRM_API.DB.PAYMPlanBundles.GetAccountInfo: " + ex.Message);
            }
        }

        //Added by Elango for Jira CRMT-216 Start
        public static IEnumerable<PAYMPlan> RequestForChangeCreditLimit(string mobileNo, float RequestCreditAmount, string calledby)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.PMBOConnection))
                {
                    conn.Open();

                    var result = conn.Query<PAYMPlan>(
                            "c3pm_customer_credit_limit_sp", new
                            {
                                @mobileno = mobileNo,
                                @credit_limit = RequestCreditAmount,
                                @calledby = calledby,
                                @task = "Request"
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<PAYMPlan>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.PAYMPlanBundles.RequestForChangeCreditLimit: {0}", ex.Message);
                throw new Exception("CRM_API.DB.PAYMPlanBundles.RequestForChangeCreditLimit: " + ex.Message);
            }
        }
        public static IEnumerable<PAYMPlan> GetCreditLimit(string mobileNo)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.PMBOConnection))
                {
                    conn.Open();

                    var result = conn.Query<PAYMPlan>(
                            "c3pm_customer_credit_limit_sp", new
                            {
                                @mobileno = mobileNo,
                                @calledby = "",
                                @credit_limit = "",
                                @task = "Get"
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<PAYMPlan>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.PAYMPlanBundles.GetCreditLimit: {0}", ex.Message);
                throw new Exception("CRM_API.DB.PAYMPlanBundles.GetCreditLimit: " + ex.Message);
            }
        }
        public static IEnumerable<PAYMPlan> ApproveForChangeCreditLimit(string mobileNo, float RequestCreditAmount, string calledby)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.PMBOConnection))
                {
                    conn.Open();

                    var result = conn.Query<PAYMPlan>(
                            "c3pm_customer_credit_limit_sp", new
                            {
                                @mobileno = mobileNo,
                                @credit_limit = RequestCreditAmount,
                                @calledby = calledby,
                                @task = "Approve"
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<PAYMPlan>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.PAYMPlanBundles.ApproveForChangeCreditLimit: {0}", ex.Message);
                throw new Exception("CRM_API.DB.PAYMPlanBundles.ApproveForChangeCreditLimit: " + ex.Message);
            }
        }
        public static IEnumerable<PAYMPlan> RejectForChangeCreditLimit(string mobileNo, string calledby)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.PMBOConnection))
                {
                    conn.Open();

                    var result = conn.Query<PAYMPlan>(
                            "c3pm_customer_credit_limit_sp", new
                            {
                                @mobileno = mobileNo,
                                @calledby = calledby,
                                @credit_limit = "",
                                @task = "Reject"
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<PAYMPlan>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.PAYMPlanBundles.RejectForChangeCreditLimit: {0}", ex.Message);
                throw new Exception("CRM_API.DB.PAYMPlanBundles.RejectForChangeCreditLimit: " + ex.Message);
            }
        }
        //Added by Elango for Jira CRMT-216 End

        public static IEnumerable<PAYMPlan> ListPlans(string mobileNo, string calledby, string product, string sitecode)
        {
            try
            {
                //CRM Enhancement 2
                //using (var conn = new SqlConnection(Helpers.Config.PMBOConnection))
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<PAYMPlan>(
                            //CRM Enhancement 2
                            //"c3pm_planlist", new
                            "crm3_paym_get_plan_dtl", new
                            {
                                @mobileno = mobileNo,
                                @calledby = calledby,
                                @product = product,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<PAYMPlan>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.PAYMPlanBundles.ListPlans: {0}", ex.Message);
                throw new Exception("CRM_API.DB.PAYMPlanBundles.ListPlans: " + ex.Message);
            }
        }

        public static IEnumerable<PAYMPlanDetail> ListPlanDetails(string mobileNo, string calledby)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.PMBOConnection))
                {
                    conn.Open();

                    var result = conn.Query<PAYMPlanDetail>(
                            "c3pm_plandetailslist ", new
                            {
                                @mobileno = mobileNo,
                                @calledby = calledby
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<PAYMPlanDetail>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.PAYMPlanBundles.ListPlanDetails: {0}", ex.Message);
                throw new Exception("CRM_API.DB.PAYMPlanBundles.ListPlanDetails: " + ex.Message);
            }
        }

        public static IEnumerable<PAYMPlanHistory> ListPlanHistory(string mobileNo, string calledby)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.PMBOConnection))
                {
                    conn.Open();

                    var result = conn.Query<PAYMPlanHistory>(
                            "c3pm_planhistorylist ", new
                            {
                                @mobileno = mobileNo,
                                @calledby = calledby
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<PAYMPlanHistory>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.PAYMPlanBundles.ListPlanHistory: {0}", ex.Message);
                throw new Exception("CRM_API.DB.PAYMPlanBundles.ListPlanHistory: " + ex.Message);
            }
        }

        public static IEnumerable<PAYMPlanRef> ListPlanRef()
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.PMBOConnection))
                {
                    conn.Open();

                    var result = conn.Query<PAYMPlanRef>(
                            "c3pm_refplanlist ", null, commandType: CommandType.StoredProcedure);

                    return result ?? new List<PAYMPlanRef>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.PAYMPlanBundles.ListPlanRef: {0}", ex.Message);
                throw new Exception("CRM_API.DB.PAYMPlanBundles.ListPlanRef: " + ex.Message);
            }
        }

        //Added : 25-Dec-2018 - Change Plan
        public static IEnumerable<BundleCategorryRef> ListBundleCategory(string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<BundleCategorryRef>(
                            "crm_get_bundle_category_list ", new
                            {
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<BundleCategorryRef>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.PAYMPlanBundles.ListBundleCategory: {0}", ex.Message);
                throw new Exception("CRM_API.DB.PAYMPlanBundles.ListBundleCategory: " + ex.Message);
            }
        }

        //Added : 25-Dec-2018 - Change Plan 
        public static IEnumerable<BundleListByCategoryIdRef> GetBundleListByCategoryId(string sitecode, int category_id)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<BundleListByCategoryIdRef>(
                            "crm_get_bundle_list_by_category_id ", new
                            {
                                @sitecode = sitecode,
                                @category_id = category_id
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<BundleListByCategoryIdRef>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.PAYMPlanBundles.GetBundleListByCategoryId: {0}", ex.Message);
                throw new Exception("CRM_API.DB.PAYMPlanBundles.GetBundleListByCategoryId: " + ex.Message);
            }
        }

        public static IEnumerable<PlanDetail> GetPlanDetail(int bundleId)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.PMBOConnection))
                {
                    conn.Open();

                    var result = conn.Query<PlanDetail>(
                            "c3pm_getplandetail ", new
                            {
                                @bundleid = bundleId
                            }, commandType: CommandType.StoredProcedure);

                    return result != null && result.Count() > 0 ? result : new List<PlanDetail>() { new PlanDetail() { errcode = -1, errmsg = "Plan details not found for Bundle ID :  " + bundleId } };
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.PAYMPlanBundles.GetPlanDetail: {0}", ex.Message);
                //throw new Exception("CRM_API.DB.PAYMPlanBundles.GetPlanDetail: " + ex.Message);
                return new List<PlanDetail>() { new PlanDetail() { errcode = -1, errmsg = ex.Message } };
            }
        }

        //06-Feb-2017 : Moorthy : Added ProductCode 
        public static IEnumerable<PlanDetail> GetBundleDetail(string planName, string productCode, int bundleid)
        {
            try
            {
                if (productCode == "" || productCode == "VMUK")
                    planName = planName.Replace('€', '£');
                using (var conn = new SqlConnection(Helpers.Config.PMBOConnection))
                {
                    conn.Open();

                    var result = conn.Query<PlanDetail>(
                            "c3pm_getbundledetailbyplanname ", new
                            {
                                @planname = planName,
                                @bundleid = bundleid
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<PlanDetail>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.PAYMPlanBundles.GetBundleDetail: {0}", ex.Message);
                throw new Exception("CRM_API.DB.PAYMPlanBundles.GetBundleDetail: " + ex.Message);
            }
        }

        public static IEnumerable<ChangePlanOutput> ChangePlan(string mobileno, DateTime prevplan_end_date, DateTime newplan_start_date, double newplan, int bundleId, string calledby,
            string pay_reference = "n/a", string pay_method = "n/a", float pay_amount = 0, int pay_status = 0)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.PMBOConnection))
                {
                    conn.Open();

                    var result = conn.Query<ChangePlanOutput>(
                            "c3pm_change_plan ", new
                            {
                                @bundleid = bundleId,
                                @mobileno = mobileno,
                                @prevplan_end_date = prevplan_end_date,
                                @newplan_start_date = newplan_start_date,
                                @newplan = newplan,
                                @calledby = calledby,
                                @pay_reference = pay_reference,
                                @pay_method = pay_method,
                                @pay_amount = pay_amount,
                                @pay_status = pay_status
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<ChangePlanOutput>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.PAYMPlanBundles.ChangePlan: {0}", ex.Message);
                List<ChangePlanOutput> errValues = new List<ChangePlanOutput>();
                do
                {
                    errValues.Add(new ChangePlanOutput() { errcode = -1, errmsg = ex.Message });
                    ex = ex.InnerException;
                } while (ex != null);
                return errValues;
            }
        }

        public static IEnumerable<Common.ErrCodeMsg> CancelPlan(string mobileno, bool isImmediate, DateTime plan_end_date, string calledby,
            float fee = 0, string pay_reference = "n/a", string pay_method = "n/a", float pay_amount = 0, int pay_status = 0)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.PMBOConnection))
                {
                    conn.Open();

                    var result = conn.Query<Common.ErrCodeMsg>(
                            "c3pm_cancel_account ", new
                            {
                                @mobileno = mobileno,
                                @fee = fee,
                                @disconectdate = plan_end_date,
                                @dd_status = isImmediate ? 8 : 9,
                                @updateby = calledby,
                                @pay_reference = pay_reference,
                                @pay_method = pay_method,
                                @pay_amount = pay_amount,
                                @pay_status = pay_status
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<Common.ErrCodeMsg>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.PAYMPlanBundles.CancelPlan: {0}", ex.Message);
                List<Common.ErrCodeMsg> errValues = new List<Common.ErrCodeMsg>();
                do
                {
                    errValues.Add(new Common.ErrCodeMsg() { errcode = -1, errmsg = ex.Message });
                    ex = ex.InnerException;
                } while (ex != null);
                return errValues;
            }
        }

        public static IEnumerable<AccountBalance> GetAccountBalance(string mobileNo)
        {
            try
            {
                //using (var conn = new SqlConnection(Helpers.Config.MCMESPConnection))
                using (var conn = new SqlConnection(Helpers.Config.PMBOConnection))
                {
                    conn.Open();

                    var result = conn.Query<AccountBalance>(
                            "pmbo_get_mvno_account_balance_v1", new
                            {
                                @mobileno = mobileNo
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<AccountBalance>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.PAYMPlanBundles.GetAccountBalance: {0}", ex.Message);
                throw new Exception("CRM_API.DB.PAYMPlanBundles.GetAccountBalance: " + ex.Message);
            }
        }

        public static IEnumerable<Common.ErrCodeMsg> BundleReactivate(string mobileno, int bundleId, string sitecode, string destno, string calledby)
        {
            try
            {
                /*
                using (var conn = new SqlConnection(Helpers.Config.PMBOConnection))
                {
                    conn.Open();

                    var result = conn.Query<Common.ErrCodeMsg>(
                            "c3pm_change_plan ", new
                            {
                                @bundleid = bundleId,
                                @mobileno = mobileno,
                                @prevplan_end_date = prevplan_end_date,
                                @newplan_start_date = newplan_start_date,
                                @newplan = newplan,
                                @calledby = calledby,
                                @pay_reference = pay_reference,
                                @pay_method = pay_method,
                                @pay_amount = pay_amount,
                                @pay_status = pay_status
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<Common.ErrCodeMsg>();
                }
                */

                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var p = new DynamicParameters();
                    p.Add("@sitecode", sitecode);
                    p.Add("@usertype", 2);
                    p.Add("@userinfo", mobileno);
                    p.Add("@bundleid", bundleId);
                    p.Add("@paymode", 2);
                    p.Add("@processby", calledby);
                    p.Add("@errcode", 0, DbType.Int32, ParameterDirection.Output, int.MaxValue);
                    p.Add("@errmsg", "", DbType.String, ParameterDirection.Output, 100);
                    p.Add("@pack_dest", destno);
                    conn.Execute("crm3_bundle_reactivate_paym", p, null, null, CommandType.StoredProcedure);
                    if (p.Get<int>("@errcode") != 0)
                        throw new Exception(p.Get<string>("@errmsg"));

                    var result = new Common.ErrCodeMsg()
                    {
                        errcode = p.Get<int>("@errcode"),
                        errmsg = p.Get<string>("@errmsg")
                    };

                    var resultlist = new List<Common.ErrCodeMsg>();
                    resultlist.Add(result);

                    return resultlist ?? new List<Common.ErrCodeMsg>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.PAYMPlanBundles.BundleReactivate: {0}", ex.Message);
                List<Common.ErrCodeMsg> errValues = new List<Common.ErrCodeMsg>();
                do
                {
                    errValues.Add(new Common.ErrCodeMsg() { errcode = -1, errmsg = ex.Message });
                    ex = ex.InnerException;
                } while (ex != null);
                return errValues;
            }
        }

        public static IEnumerable<Common.ErrCodeMsg> LLOMReactivate(string mobileno, string sitecode, string reg_number, int svc_Id, int paymode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Common.ErrCodeMsg>(
                            "crm3_LLOTG_Multinb_reactivate_paym ", new
                            {
                                @mobileno = mobileno,
                                @sitecode = sitecode,
                                @reg_number = reg_number,
                                @svc_Id = svc_Id,
                                @paymode = paymode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<Common.ErrCodeMsg>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.PAYMPlanBundles.LLOMReactivate: {0}", ex.Message);
                List<Common.ErrCodeMsg> errValues = new List<Common.ErrCodeMsg>();
                do
                {
                    errValues.Add(new Common.ErrCodeMsg() { errcode = -1, errmsg = ex.Message });
                    ex = ex.InnerException;
                } while (ex != null);
                return errValues;
            }
        }

        // public static IEnumerable<Common.ErrCodeMsg> CountrySaverReactivate(string sitecode, int usertype , string mobileNo, int bundleid, int paymode, string pack_dest, string processby="SMS")
        public static IEnumerable<Common.ErrCodeMsg> CountrySaverReactivate(string reg_idx, string merchant_ref_code, int payment_status, int use_for_renewal, string brandtype)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Common.ErrCodeMsg>(
                        //"crm3_Countrysaver_reactivate_paym ", new
                        //{
                        //   @sitecode = sitecode,    
                        //   @usertype = usertype,
                        //   @userinfo = mobileNo,
                        //   @bundleid = bundleid,
                        //   @paymode = paymode,
                        //   @processby = processby,     
                        //   @errcode = 0,    
                        //   @errmsg = "",
                        //   @pack_dest = pack_dest                                
                        //}, commandType: CommandType.StoredProcedure);

                            "crm3_countrysaver_reactivate_v1_bycc  ", new
                            {
                                @reg_idx = Convert.ToInt32(reg_idx),
                                @merchant_ref_code = merchant_ref_code,
                                @payment_status = payment_status,
                                @use_for_renewal = use_for_renewal,
                                @brandtype = brandtype,
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<Common.ErrCodeMsg>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.PAYMPlanBundles.CountrySaverReactivate: {0}", ex.Message);
                List<Common.ErrCodeMsg> errValues = new List<Common.ErrCodeMsg>();
                do
                {
                    errValues.Add(new Common.ErrCodeMsg() { errcode = -1, errmsg = ex.Message });
                    ex = ex.InnerException;
                } while (ex != null);
                return errValues;
            }
        }

        public static string GetCurrency(string country)
        {
            //if (country.ToLower() == "uk" || country.ToLower() == "united kingdom")
            //    return "GBP";
            //else
            //    return "EUR";

            if (country.ToUpper() == "BAU")
            {
                return "EUR";
            }
            else if (country.ToUpper() == "BDK")
            {
                return "DKK";
            }
            else if (country.ToUpper() == "MFR")
            {
                return "EUR";
            }
            else if (country.ToUpper() == "BNL")
            {
                return "EUR";
            }
            else if (country.ToUpper() == "MPT")
            {
                return "EUR";
            }
            else if (country.ToUpper() == "BSE")
            {
                return "SEK";
            }
            else if (country.ToUpper() == "MCM")
            {
                return "GBP";
            }
            else if (country.ToUpper() == "MBE")
            {
                return "EUR";
            }
            else if (country.ToUpper() == "LO2")
            {
                return "GBP";
            }
            else if (country.ToUpper() == "US1")
            {
                return "USD";
            }

            else if (country.ToUpper() == "FR1")
            {
                return "EUR";
            }
            else if (country.ToUpper() == "MCZ")
            {
                return "CZK";
            }
            else if (country.ToUpper() == "MCY")
            {
                return "EUR";
            }
            else if (country.ToUpper() == "MSR")
            {
                return "RSD";
            }
            else
            {
                return "GBP";
            }


        }

        //Restrict Excess Usage
        public static IEnumerable<GetRestrictExcessUsageOutput> GetRestrictExcessUsage(string mobileno, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<GetRestrictExcessUsageOutput>(
                            "crm_paym_get_restrict_excess_usage_mobileno", new
                            {
                                @mobileno = mobileno,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<GetRestrictExcessUsageOutput>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.PAYMPlanBundles.GetRestrictExcessUsage: {0}", ex.Message);
                throw new Exception("CRM_API.DB.PAYMPlanBundles.GetRestrictExcessUsage: " + ex.Message);
            }
        }

        public static IEnumerable<CRM_API.Models.Common.ErrCodeMsg> InsertRestrictExcessUsage(string mobileno, string sitecode, int status, string updated_by)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<CRM_API.Models.Common.ErrCodeMsg>(
                            "crm_paym_restrict_excess_usage_by_mobileno", new
                            {
                                @mobileno = mobileno,
                                @sitecode = sitecode,
                                @status = status,
                                @updated_by = updated_by
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.Common.ErrCodeMsg>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.PAYMPlanBundles.InsertRestrictExcessUsage: {0}", ex.Message);
                throw new Exception("CRM_API.DB.PAYMPlanBundles.InsertRestrictExcessUsage: " + ex.Message);
            }
        }

        public static IEnumerable<CRM_API.Models.Common.ErrCodeMsg> ExchangeBundle(PAYMPlanBundleRequest requestData)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<CRM_API.Models.Common.ErrCodeMsg>(
                            "crm_paym_restrict_excess_usage_by_mobileno", new
                            {
                                //@mobileno = mobileno,
                                //@sitecode = sitecode,
                                //@status = status,
                                //@updated_by = updated_by
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.Common.ErrCodeMsg>() { new CRM_API.Models.Common.ErrCodeMsg { errcode = -1, errmsg = "Failure" } };
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.PAYMPlanBundles.ExchangeBundle: {0}", ex.Message);
                throw new Exception("CRM_API.DB.PAYMPlanBundles.ExchangeBundle: " + ex.Message);
            }
        }
    }
}
