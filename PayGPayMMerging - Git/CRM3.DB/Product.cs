﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;

namespace CRM_API.DB.Product
{
    public class ProductSProc
    {
        public static IEnumerable<Models.ProductModel> GetAllProducts()
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Models.ProductModel>(
                            "crm3_mundio_products_list",
                            commandType: CommandType.StoredProcedure);
                    return result ?? new List<Models.ProductModel>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("ProductSProc.GetAllProducts: " + ex.Message);
            }
        }
        public static Models.ProductModel GetProductByMobile(string MobileNo)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    // Old SP:crm3_saver_get_producttype
                    //port out SP:crm3_saver_get_producttype
                    conn.Open();
                    var result = conn.Query<Models.ProductModel>(
                            "crm3_saver_get_producttype_PO", new
                            {
                                @mundio_cli = MobileNo
                            },
                            commandType: CommandType.StoredProcedure);

                    CRM_API.Helpers.Session.GetProductByMobile = result.FirstOrDefault() ?? new Models.ProductModel();
                    return result.FirstOrDefault() ?? new Models.ProductModel();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("ProductSProc.GetProductByMobile: " + ex.Message);
            }
        }
        public static Models.ConnectionString GetConnection(string siteCode)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.UserMgtConnection))
                {
                    conn.Open();

                    var result = conn.Query<Models.ConnectionString>(
                            "crm3_get_db_info", new
                            {
                                @site_code = siteCode
                            }, commandType: CommandType.StoredProcedure);

                    return result.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("ProductSProc.GetConnection: " + ex.Message);
            }
        }

        public static IEnumerable<Models.ProductModel> GetAllProductsForChargeback()
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Models.ProductModel>(
                            "crm3_mundio_products_list", new {
                                @user_id= 0,
                                @specify_product= 1
                            }, commandType: CommandType.StoredProcedure);
                    return result ?? new List<Models.ProductModel>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("ProductSProc.GetAllProducts: " + ex.Message);
            }
        }
    }
}
