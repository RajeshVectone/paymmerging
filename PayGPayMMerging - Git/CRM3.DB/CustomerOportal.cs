﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;

namespace CRM_API.DB.CustomerOportal
{
    public class SProc
    {
        public static IEnumerable<CRM_API.Models.CustomerOportalList> Search(string saver_operator, DateTime date_from, DateTime date_to)
        {
            try
            {

                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<CRM_API.Models.CustomerOportalList>(
                            "saver_oportal_customer_list", new
                            {
                                @saver_operator = saver_operator,
                                @date_from = date_from,
                                @date_to = date_to
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.CustomerOportalList>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL Search: " + ex.Message);
            }
        }
    }
}
