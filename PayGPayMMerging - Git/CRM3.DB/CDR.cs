﻿using Dapper;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CRM_API.DB.CDR
{
    public class SProc
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public static IEnumerable<CRM_API.Models.CDR.CallHistory> GocheapCallCDR(string msisdn, DateTime dateFrom ,DateTime dateUntil, string sitecode, int type= 1)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<CRM_API.Models.CDR.CallHistory>(
                            "crm3_PayG_All_V2", new
                            {
                                @msisdn = msisdn,
                                @datefrom = dateFrom,
                                @dateto = dateUntil,
                                @tipe = type,
                                @sitecode = sitecode
                            }, commandTimeout: (5 * 60), commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.CDR.CallHistory>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.CDR.SProc GocheapCallCDR: {0}", ex.Message);
                throw new Exception("CRM_API.DB.CDR.SProc GocheapCallCDR: " + ex.Message);
            }
        }
        public static IEnumerable<CRM_API.Models.CDR.SMSHistory> GocheapSMSCDR(string msisdn, DateTime dateFrom, DateTime dateUntil, string sitecode, int type = 2)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<CRM_API.Models.CDR.SMSHistory>(
                            "crm3_PayG_All_V2", new
                            {
                                @msisdn = msisdn,
                                @datefrom = dateFrom,
                                @dateto = dateUntil,
                                @tipe = type,
                                @sitecode = sitecode
                            }, commandTimeout: (5 * 60), commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.CDR.SMSHistory>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.CDR.SProc GocheapSMSCDR: {0}", ex.Message);
                throw new Exception("CRM_API.DB.CDR.SProc GocheapSMSCDR: " + ex.Message);
            }
        }
        public static IEnumerable<CRM_API.Models.CDR.GPRSHistory> GocheapGPRSCDR(string msisdn, DateTime dateFrom, DateTime dateUntil, string sitecode, int type = 3)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<CRM_API.Models.CDR.GPRSHistory>(
                            "crm3_PayG_All_V2", new
                            {
                                @msisdn = msisdn,
                                @datefrom = dateFrom,
                                @dateto = dateUntil,
                                @tipe = type,
                                @sitecode = sitecode
                            }, commandTimeout: (5*60), commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.CDR.GPRSHistory>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.CDR.SProc GocheapCDR: {0}", ex.Message);
                throw new Exception("CRM_API.DB.CDR.SProc GocheapCDR: " + ex.Message);
            }
        }
    }
}

