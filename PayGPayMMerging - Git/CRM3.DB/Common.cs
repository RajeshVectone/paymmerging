﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.DB.Common
{
    public class ErrCodeMsg
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
        private string _errsubject = "General Error";
        public string errsubject { 
            get { 
                return (errcode == 0 || errcode == 1) ? "Result Information":_errsubject ; 
            } 
            set { _errsubject = value; } }
    }

    public class GetSmsIwmscUrlOutput
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
        public string SmsServer { get; set; }
        public string sms_iwmsc_url { get; set; }
    }

    public class Helper
    {
        public static ErrCodeMsg Coalese(IEnumerable<ErrCodeMsg> ecmArr)
        {
            var def = new Common.ErrCodeMsg()
            {
                errcode = -1,
                errmsg = "Unknown",
            };

            if (ecmArr == null)
                return def;

            if (ecmArr.Count() == 0)
                return def;

            return ecmArr.ElementAt(0);
        }
    }
}
