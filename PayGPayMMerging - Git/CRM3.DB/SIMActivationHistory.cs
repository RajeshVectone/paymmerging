﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;
using CRM_API.Models.SIMActivation;

namespace CRM_API.DB
{
    public class SIMActivationHistory
    {
        ////Issue History Report
        public static IEnumerable<SIMActivationModels> GetSIMActivationHistoryList(string date_from, string date_to)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<Models.SIMActivation.SIMActivationModels>(
                            "crm_get_simactivation_details", new
                            {
                                @date_from = date_from,
                                @date_to = date_to
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<Models.SIMActivation.SIMActivationModels>();
                }
            }
            catch (Exception ex)
            {
                List<Models.SIMActivation.SIMActivationModels> resValue = new List<Models.SIMActivation.SIMActivationModels>();
                return resValue;
            }

        }
    }
}
