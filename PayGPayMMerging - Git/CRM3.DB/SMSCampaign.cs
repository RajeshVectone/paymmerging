﻿using CRM_API.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
//using System.Net.NetworkInformation;

namespace CRM_API.DB.SMSCampaign
{
    public class SProc
    {           
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public static IEnumerable<SMSCampaignModel> GetSMSCampaignList(string mobileNo, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.SMSCampaignConnection))
                {
                    conn.Open();

                    var result = conn.Query<SMSCampaignModel>(
                            "CRM_SMS_View_Campaign_list ", new
                            {
                                @mobile_no = mobileNo,
                                @site_code = sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<SMSCampaignModel>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.SMSCampaign.GetSMSCampaignList: {0}", ex.Message);
                throw new Exception("CRM_API.DB.SMSCampaign.GetSMSCampaignList: " + ex.Message);
            }
        }

        public static IEnumerable<SMSMarketingHistory> GetSMSMarketingHistory(string mobileNo, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.SMSCampaignConnection))
                {
                    conn.Open();

                    var result = conn.Query<SMSMarketingHistory>(
                            "CRM_SMS_Get_UserSetting_history ", new
                            {
                                @mobile_no = mobileNo,
                                @site_code = sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<SMSMarketingHistory>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.SMSCampaign.GetSMSMarketingHistory: {0}", ex.Message);
                throw new Exception("CRM_API.DB.SMSCampaign.GetSMSMarketingHistory: " + ex.Message);
            }
        }

        //26-Dec-2018 : Moorthy : Addded for SMS module
        public static IEnumerable<CustomerWiseSMSNotification> GetCustomerWiseSMSNotification(string mobileNo, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<CustomerWiseSMSNotification>(
                            "crm_get_customer_wise_sms_notification", new
                            {
                                @mobileno = mobileNo,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CustomerWiseSMSNotification>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.SMSCampaign.GetCustomerWiseSMSNotification: {0}", ex.Message);
                throw new Exception("CRM_API.DB.SMSCampaign.GetCustomerWiseSMSNotification: " + ex.Message);
            }
        }

        public static IEnumerable<InsertSMSNotifyTextOutput> InsertSMSNotifyText(string mobileNo, string sms_text, string crm_user, int sms_status,
            string sms_type, string customer_name, string scenerio)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<InsertSMSNotifyTextOutput>(
                            "crm_insert_sms_notify_text", new
                            {
                                @mobileno = mobileNo,
                                @sms_text = sms_text,
                                @mode = "CRM",
                                @crm_user = crm_user,
                                @status = sms_status,
                                @sms_type = sms_type,
                                @customer_name = customer_name,
                                @scenerio = scenerio
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<InsertSMSNotifyTextOutput>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.SMSCampaign.InsertSMSNotifyText: {0}", ex.Message);
                throw new Exception("CRM_API.DB.SMSCampaign.InsertSMSNotifyText: " + ex.Message);
            }
        }

        #region GetSMSGatewayInfo
        //Used to get the SMS gateway info from the DB
        public static string GetSMSGatewayInfo(string sitecode, string msisdn)
        {
            Log.Info("GetSMSGatewayInfo");
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<SMSGatewayInfo>(
                            "website_central_get_sms_iwmsc_url", new
                            {
                                @sitecode = sitecode,
                                @data_activation = 1,
                                @msisdn = msisdn
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        return result.ElementAt(0).sms_iwmsc_url;
                        //foreach (var item in result)
                        //{
                        //    try
                        //    {
                        //        string ip = item.sms_url.Split('/')[2];
                        //        Ping pingSender = new Ping();
                        //        PingReply reply = pingSender.Send(ip);
                        //        if (reply.Status == IPStatus.Success)
                        //        {
                        //            Log.Info("GetSMSGatewayInfo : Success URL : " + item.sms_url);
                        //            return item.sms_url;
                        //        }
                        //    }
                        //    catch { }
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetSMSGatewayInfo : " + ex.Message);
            }
            return "";
        }
        #endregion
    }

    public class SMSGatewayInfo
    {
        public string sms_iwmsc_url { get; set; }
        public int errcode { get; set; }
        public string errmsg { get; set; }
    }
}
