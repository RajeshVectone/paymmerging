﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;
using System.Globalization;
using System.Threading.Tasks;

namespace CRM_API.DB.SIMOrder
{
    public class SProc
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public static Common.ErrCodeMsg DoLabelling(SIMOrderLabelRequest requestList)
        {
            Common.ErrCodeMsg resValue = new Common.ErrCodeMsg();
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<Common.ErrCodeMsg>(
                            "crm3_freesim_dispatch", new
                            {
                                @freesim_order_id = requestList.orderid,
                                @iccid = requestList.iccid,
                                @crm_login = requestList.crm_login
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    return result ?? new Common.ErrCodeMsg() { errcode = -1, errmsg = "Empty result" };
                }
            }
            catch (Exception ex)
            {
                Log.Error("DoLabelling: {0}", ex.Message);
                return new Common.ErrCodeMsg() { errcode = -1, errmsg = ex.Message };
            }
        }

        public static Common.ErrCodeMsg DoLabelling2in1Order(SIMOrderLabelRequest requestList)
        {
            Common.ErrCodeMsg resValue = new Common.ErrCodeMsg();
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<Common.ErrCodeMsg>(
                            "crm3_freesim_dispatch", new
                            {
                                @freesim_order_id = int.Parse(requestList.freesimid.Substring(3)),
                                @iccid = requestList.iccid,
                                @crm_login = requestList.crm_login
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    return result ?? new Common.ErrCodeMsg() { errcode = -1, errmsg = "Empty result" };
                }
            }
            catch (Exception ex)
            {
                Log.Error("DoLabelling: {0}", ex.Message);
                return new Common.ErrCodeMsg() { errcode = -1, errmsg = ex.Message };
            }
        }
        public static IEnumerable<FreesimOrderItem> Get2in1AllOrder(string key, string dateFrom, string dateUntil, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    //old SP:crm3_freesim_order2in1_search
                    var a = conn.Query<FreesimOrderItem>(
                            "crm3_freesim_order2in1_search", new
                            {
                                //@max_result = requestSearch.max_result,
                                //@order_status = requestSearch.status,
                                @order_date_from = dateFrom,
                                @order_date_to = dateUntil
                                //@order_id = requestSearch.order_id,
                                //@mobileno = requestSearch.mobileno,
                                //@full_name = requestSearch.full_name,
                                //@postcode = requestSearch.postcode,
                                //@email = requestSearch.email,
                                //@contact = requestSearch.contact,
                                //@sim_id = requestSearch.sim_id
                            }, commandType: CommandType.StoredProcedure);
                    var b = CRM_API.DB.Product.ProductSProc.GetAllProducts().ToList().Where(w => w.sitecode.Contains(sitecode));
                    var result = a.Where(w => b.Any(t => t.mundio_product.Contains(w.sim_brand)));

                    if (key == "registerdate")
                    {
                        var resa = result.Where(w => w.freesimstatus.Contains("NEW ORDER"));
                        //var resb = result.Where(w => w.freesimstatus.Contains("Incomplete"));
                        //var resc = result.Where(w => w.freesimstatus.Contains("Replica"));
                        //var resd = result.Where(w => w.freesimstatus.Contains("Incapable"));
                        //var rese = result.Where(w => w.freesimstatus.Contains("Returned"));
                        //var iresult = resa.Union(resb.Union(resc.Union(resd.Union(rese))));
                        return resa;
                    }
                    if (key == "registerdateunprocessed")
                    {
                        //var resa = result.Where(w => w.freesimstatus.Contains("NEW ORDER"));
                        var resb = result.Where(w => w.freesimstatus.Contains("Incomplete"));
                        var resc = result.Where(w => w.freesimstatus.Contains("Replica"));
                        var resd = result.Where(w => w.freesimstatus.Contains("Incapable"));
                        var rese = result.Where(w => w.freesimstatus.Contains("Returned"));
                        var iresult = resb.Union(resc.Union(resd.Union(rese)));
                        return iresult;
                    }
                    if (key == "sentdate")
                    {
                        var resx = result.Where(w => w.freesimstatus.Contains("DISPATCHED"));
                        var resy = result.Where(w => w.freesimstatus.Contains("Fixed"));
                        var iresult = resx.Union(resy);
                        return iresult;
                    }
                    return result ?? new List<FreesimOrderItem>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("Get2in1AllOrder : {0}", ex.Message);
                List<FreesimOrderItem> resValue = new List<FreesimOrderItem>();
                throw new Exception("Get2in1AllOrder : " + ex.Message);
            }
        }

        public static IEnumerable<FreesimOrderItem> GetCombinedFreesimOrder(string key, string dateFrom, string dateUntil, string sitecode)
        {
            var resx = GetFreesimOrderGroup(key, dateFrom, dateUntil, sitecode);
            //if (sitecode == "MCM")
            //{
            var resy = Get2in1AllOrder(key, dateFrom, dateUntil, sitecode);
            return resx.Union(resy);
            //}
            //else
            return resx;
        }

        public static IEnumerable<FreesimOrderItem> GetCombinedtUnprocessedFreesimOrder(string dateFrom, string dateUntil, string sitecode)
        {
            var resx = GetUnprocessedOrder(dateFrom, dateUntil, sitecode);
            //if (sitecode == "MCM")
            //{
            var resy = Get2in1AllOrder("registerdateunprocessed", dateFrom, dateUntil, sitecode);
            return resx.Union(resy);
            //}
            //else
            return resx;
        }

        public static IEnumerable<FreesimOrderItem> GetFreeSimByFreesimidGroupBySubscriberid(string freesimId, string sitecode)
        {
            string sql = "crm3_getfreesim_get_by_freesimid_groupsubscriberid";
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<FreesimOrderItem>(sql, new
                    {
                        @freesimid = freesimId,
                        @sitecode = sitecode
                    }, null, true, null, CommandType.StoredProcedure);
                    return result;
                }
            }
            catch (Exception e)
            {
                Log.Error("CRM_API.DB.SIMOrder.SProc.GetFreeSimByFreesimidGroupBySubscriberid: " + e.Message);
                throw new Exception("CRM_API.DB.SIMOrder.SProc.GetFreeSimByFreesimidGroupBySubscriberid: " + e.Message);
            }
        }

        public static IEnumerable<FreesimOrderItem> GetFreesimByMasterOrder(string fieldname, string fieldvalue, string sitecode)
        {
            string sql = "crm3_get_freesim_subsciber";
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<FreesimOrderItem>(sql, new
                    {
                        @fieldname = fieldname,
                        @fieldvalue = fieldvalue,
                        @sitecode = sitecode
                    }, null, true, null, CommandType.StoredProcedure);
                    return result;
                }
            }
            catch (Exception e)
            {
                Log.Error("CRM_API.DB.SIMOrder.SProc.GetFreesimByMasterOrder: " + e.Message);
                throw new Exception("CRM_API.DB.SIMOrder.SProc.GetFreesimByMasterOrder: " + e.Message);
            }
        }

        public static IEnumerable<FreesimOrderItem> GetFreeSimByFullnameGroupBySubscriberid(string fullname, string sitecode)
        {
            string sql = "crm3_getfreesim_get_by_fullname_groupsubscriberid";
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<FreesimOrderItem>(sql, new
                    {
                        @fullname = fullname,
                        @sitecode = sitecode
                    }, null, true, (60 * 1000), CommandType.StoredProcedure);
                    return result;
                }
            }
            catch (Exception e)
            {
                Log.Error("CRM_API.DB.SIMOrder.SProc.GetUnprocessedOrder: " + e.Message);
                throw new Exception("CRM_API.DB.SIMOrder.SProc.GetUnprocessedOrder: " + e.Message);
            }
        }

        public static IEnumerable<FreesimOrderItem> GetFreeSimForDownload(string dateFrom, string dateUntil, string fieldName, string sitecode)
        {
            //old SP:crm3_getfreesimfordownload
            string sql = "crm3_getfreesimfordownload_v3";
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<FreesimOrderItem>(sql, new
                    {
                        @FieldName = fieldName,
                        @DateFrom = dateFrom,
                        @DateTo = dateUntil,
                        @sitecode = sitecode
                    }, null, true, 600, CommandType.StoredProcedure);
                    return result;
                }
            }
            catch (Exception e)
            {
                Log.Error("CRM_API.DB.SIMOrder.SProc.GetFreeSimForDownload: " + e.Message);
                throw new Exception("CRM_API.DB.SIMOrder.SProc.GetFreeSimForDownload: " + e.Message);
            }
        }

        public static async Task<IEnumerable<FreesimOrderItem>> GetFreeSimForDownloadAsync(string dateFrom, string dateUntil, string fieldName, string sitecode)
        {
            string sql = "crm3_getfreesimfordownload";
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = Task<IEnumerable<FreesimOrderItem>>.Run(() =>
                    {
                        var resx = conn.Query<FreesimOrderItem>(sql, new
                            {
                                @FieldName = fieldName,
                                @DateFrom = dateFrom,
                                @DateTo = dateUntil,
                                @sitecode = sitecode
                            }, null, true, 600, CommandType.StoredProcedure);
                        return resx;
                    });
                    return await result;
                }
            }
            catch (Exception e)
            {
                Log.Error("CRM_API.DB.SIMOrder.SProc.GetFreeSimForDownload: " + e.Message);
                throw new Exception("CRM_API.DB.SIMOrder.SProc.GetFreeSimForDownload: " + e.Message);
            }
        }
        public static IEnumerable<CancelLocations> GetFreeSimByFullnameGroupBycancel(string ICCID, string Sitecode, string type)
        {
            string sql = "crm_get_hlr_loc_cancel_imsi";
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CancelLocations>(sql, new
                    {
                        @Iccid = ICCID,
                        @sitecode = Sitecode,
                        @cancel_type = type
                    }, null, true, (60 * 1000), CommandType.StoredProcedure);
                    return result;
                }
            }
            catch (Exception e)
            {
                Log.Error("Cancel Location: " + e.Message);
                throw new Exception("Cancel Location: " + e.Message);
            }
        }
        public static IEnumerable<FreesimOrderItem> GetFreeSimForDownloadByFreesimid(string freesimid, string sitecode)
        {
            //old Sp:crm3_getfreesimfordownload_byfreesimid
            string sql = "crm3_getfreesimfordownload_byfreesimid_v1";
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<FreesimOrderItem>(sql, new
                    {
                        @freesimid = freesimid,
                        @sitecode = sitecode
                    }, null, true, null, CommandType.StoredProcedure);
                    return result;
                }
            }
            catch (Exception e)
            {
                Log.Error("CRM_API.DB.SIMOrder.SProc.GetFreeSimForDownloadByFreesimid: " + e.Message);
                throw new Exception("CRM_API.DB.SIMOrder.SProc.GetFreeSimForDownloadByFreesimid: " + e.Message);
            }
        }

        public static IEnumerable<FreesimOrderItem> GetFreeSimForDownloadByFullname(string fullname, string sitecode)
        {
            string sql = "crm3_getfreesimfordownload_byfullname";
            try
            {
                //old SP:crm3_getfreesimfordownload_byfullname
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<FreesimOrderItem>(sql, new
                    {
                        @fullname = fullname,
                        @sitecode = sitecode
                    }, null, true, null, CommandType.StoredProcedure);
                    return result;
                }
            }
            catch (Exception e)
            {
                Log.Error("CRM_API.DB.SIMOrder.SProc.GetFreeSimForDownloadByFullname: " + e.Message);
                throw new Exception("CRM_API.DB.SIMOrder.SProc.GetFreeSimForDownloadByFullname: " + e.Message);
            }
        }

        public static IEnumerable<FreesimOrderItem> GetFreesimOrderGroup(string key, string dateFrom, string dateUntil, string sitecode)
        {
            //Old SP:crm3_getfreesim_get_groupsubscriberid_v2
            string sql = "crm3_getfreesim_get_groupsubscriberid_v3";
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<FreesimOrderItem>(sql, new
                    {
                        @FieldName = key,
                        @DateFrom = dateFrom,
                        @DateTo = dateUntil,
                        @sitecode = sitecode
                    }, null, true, 300, CommandType.StoredProcedure);
                    return result;
                }
            }
            catch (Exception e)
            {
                Log.Error("CRM_API.DB.SIMOrder.SProc.GetFreesimOrderGroup: " + e.Message);
                throw new Exception("CRM_API.DB.SIMOrder.SProc.GetFreesimOrderGroup: " + e.Message);
            }
        }

        public static IEnumerable<FreesimOrderItem> GetUnprocessedOrder(string dateFrom, string dateUntil, string sitecode)
        {
            string sql = "crm3_usp_getfreesim_registered_unprocessed";
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<FreesimOrderItem>(sql, new
                    {
                        @DateFrom = dateFrom,
                        @DateTo = dateUntil,
                        @sitecode = sitecode
                    }, null, true, null, CommandType.StoredProcedure);
                    return result;
                }
            }
            catch (Exception e)
            {
                Log.Error("CRM_API.DB.SIMOrder.SProc.GetUnprocessedOrder: " + e.Message);
                throw new Exception("CRM_API.DB.SIMOrder.SProc.GetUnprocessedOrder: " + e.Message);
            }
        }

        public static IEnumerable<SIMOrderItem> List(SIMOrderRequest requestList)
        {
            //string sql = "crm3_freesim_order_list"; // Old SP
            string sql = "crm3_freesim_order_list_v2"; // For 2IN1 order
            //string sql = "";
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    CultureInfo culture = CultureInfo.CreateSpecificCulture("en-GB");
                    DateTimeStyles styles = DateTimeStyles.None;

                    DateTime dateTo;
                    DateTime.TryParse(requestList.todate, culture, styles, out dateTo);

                    DateTime dateFrom;
                    DateTime.TryParse(requestList.fromdate, culture, styles, out dateFrom);

                    var result = conn.Query<SIMOrderItem>(
                            sql, new
                            {
                                @max_result = requestList.max_result,
                                @order_status = requestList.status,
                                @order_date_from = dateFrom.ToString("yyyy-MM-dd HH:mm:ss"),
                                @order_date_to = dateTo.ToString("yyyy-MM-dd HH:mm:ss"),
                                @order_id = requestList.order_id,
                                @mobileno = requestList.mobileno,
                                @full_name = requestList.full_name,
                                @postcode = requestList.postcode,
                                @email = requestList.email,
                                @contact = requestList.contact,
                                @sim_id = requestList.sim_id,
                                @sitecode = requestList.sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<SIMOrderItem>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("List: {0}", ex.Message);
                List<SIMOrderItem> resValue = new List<SIMOrderItem>();
#if DEBUG
                resValue.Add(new SIMOrderLabelItem() { first_name = "test", freesim_order_id = 10, order_date = DateTime.Now, order_source = "Auto Generated", order_status = "New REquest" });
#endif
                return resValue;
            }
        }

        public static IEnumerable<SIMOrderLabelItem> ListLastDispatch(int maxResult)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<SIMOrderLabelItem>(
                            "crm3_freesim_dispatch_list ", new
                            {
                                @max_result = maxResult
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<SIMOrderLabelItem>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("ListLastDispatch: {0}", ex.Message);
                List<SIMOrderLabelItem> resValue = new List<SIMOrderLabelItem>();
#if DEBUG
                resValue.Add(new SIMOrderLabelItem() { first_name = "test", freesim_order_id = 10, order_date = DateTime.Now, order_source = "Auto Generated", item_status = "Labeled", iccid = "123456789" });
#endif
                return resValue;
            }

        }

        public static IEnumerable<FreesimOrderItem> ListLastDispatch2in1(int maxResult)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<FreesimOrderItem>(
                            "crm3_freesim2in1_dispatch_list", new
                            {
                                @max_result = maxResult
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<FreesimOrderItem>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("ListLastDispatch2in1: {0}", ex.Message);
                throw new Exception("ListLastDispatch2in1: " + ex.Message);
            }

        }

        public static IEnumerable<RefInfo> ListOfStatus()
        {
            string sql = "crm3_master_freesim_order_status";
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<RefInfo>(sql, null, null, true, null, CommandType.StoredProcedure);
                    return result;
                }
            }
            catch (Exception e)
            {
                Log.Error("CRM_API.DB.SIMOrder.SProc.ListOfStatus: " + e.Message);
                throw new Exception("CRM_API.DB.SIMOrder.SProc.ListOfStatus: " + e.Message);
            }
        }

        public static IEnumerable<SIMOrderItem> Search(SIMOrderRequest requestSearch)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    CultureInfo culture = CultureInfo.CreateSpecificCulture("en-GB");
                    DateTimeStyles styles = DateTimeStyles.None;

                    DateTime dateTo;
                    DateTime.TryParse(requestSearch.todate, culture, styles, out dateTo);

                    DateTime dateFrom;
                    DateTime.TryParse(requestSearch.fromdate, culture, styles, out dateFrom);

                    var result = conn.Query<SIMOrderItem>(
                            "crm3_freesim_order_search", new
                            {
                                @max_result = requestSearch.max_result,
                                @order_status = requestSearch.status,
                                @order_date_from = dateFrom.ToString("yyyy-MM-dd HH:mm:ss"),
                                @order_date_to = dateTo.ToString("yyyy-MM-dd HH:mm:ss"),
                                @order_id = requestSearch.order_id,
                                @mobileno = requestSearch.mobileno,
                                @full_name = requestSearch.full_name,
                                @postcode = requestSearch.postcode,
                                @email = requestSearch.email,
                                @contact = requestSearch.contact,
                                @sim_id = requestSearch.sim_id
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<SIMOrderItem>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("Search: {0}", ex.Message);
                List<SIMOrderItem> resValue = new List<SIMOrderItem>();
#if DEBUG
                resValue.Add(new SIMOrderLabelItem() { first_name = "test", freesim_order_id = 10, order_date = DateTime.Now, order_source = "Auto Generated", order_status = "New REquest" });
#endif
                return resValue;
            }
        }

        public static IEnumerable<FreesimOrderItem> Search2in1Order(SIMOrderRequest requestSearch)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    CultureInfo culture = CultureInfo.CreateSpecificCulture("en-GB");
                    DateTimeStyles styles = DateTimeStyles.None;

                    DateTime dateTo;
                    DateTime.TryParse(requestSearch.todate, culture, styles, out dateTo);

                    DateTime dateFrom;
                    DateTime.TryParse(requestSearch.fromdate, culture, styles, out dateFrom);
                    //old SP:crm3_freesim_order2in1_search
                    var result = conn.Query<FreesimOrderItem>(
                            "crm3_freesim_order2in1_search", new
                            {
                                //@max_result = requestSearch.max_result,
                                //@order_status = requestSearch.status,
                                //@order_date_from = dateFrom.ToString("yyyy-MM-dd HH:mm:ss"),
                                //@order_date_to = dateTo.ToString("yyyy-MM-dd HH:mm:ss"),
                                @order_id = requestSearch.order_id,
                                //@mobileno = requestSearch.mobileno,
                                @full_name = requestSearch.full_name,
                                //@postcode = requestSearch.postcode,
                                //@email = requestSearch.email,
                                //@contact = requestSearch.contact,
                                //@sim_id = requestSearch.sim_id
                                @product_code = requestSearch.product_code
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<FreesimOrderItem>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("Search: {0}", ex.Message);
                throw new Exception("Search: " + ex.Message);
            }
        }

        public static SIMOrderItem SearchByOrderId(SIMOrderRequest requestSearch)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    CultureInfo culture = CultureInfo.CreateSpecificCulture("en-GB");
                    DateTimeStyles styles = DateTimeStyles.None;

                    DateTime dateTo;
                    DateTime.TryParse(requestSearch.todate, culture, styles, out dateTo);

                    DateTime dateFrom;
                    DateTime.TryParse(requestSearch.fromdate, culture, styles, out dateFrom);

                    var result = conn.Query<SIMOrderItem>(
                            "crm3_freesim_order_search_by_orderid", new
                            {
                                @orderid = requestSearch.order_id
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    return result ?? new SIMOrderItem();
                }
            }
            catch (Exception ex)
            {
                Log.Error("Search: {0}", ex.Message);
                SIMOrderItem resValue = new SIMOrderItem();
#if DEBUG
                resValue = new SIMOrderLabelItem() { first_name = "test", freesim_order_id = 10, order_date = DateTime.Now, order_source = "Auto Generated", order_status = "New REquest" };
#endif
                return resValue;
            }
        }

        public static FreesimOrderItem SearchByOrderId(string id, string keyname, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    //old SP:crm3_getfreesimById
                    var result = conn.Query<FreesimOrderItem>(
                            "crm3_getfreesimById_v2", new
                            {
                                @FieldName = keyname.ToLower(),
                                @Fieldvalue = id,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    return result ?? new FreesimOrderItem();
                }
            }
            catch (Exception ex)
            {
                Log.Error("Search: {0}", ex.Message);
                FreesimOrderItem resValue = new FreesimOrderItem();
#if DEBUG
                resValue = new FreesimOrderItem() { firstname = "test", freesimid = 10, registerdate = DateTime.Now, ordersim_url = "Auto Generated", freesimstatus = "New Request" };
#endif
                return resValue;
            }
        }

        //04-Dec-2017 : Moorthy : Replace Free Sim With New ICCID
        public static Common.ErrCodeMsg UpdateFreeSimWithReplacement(string sitecode, string freesimid, string iccid_old, string iccid_new, string processby, int address_update = 0, string address1 = "", string address2 = "", string address3 = "", string postcode = "", string city = "")
        {
            string sql = "crm3_replace_freesim_with_new_iccid";
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var p = new DynamicParameters();
                    p.Add("@sitecode", sitecode, DbType.String, ParameterDirection.Input, 5);
                    p.Add("@freesimid", freesimid, DbType.Int32, ParameterDirection.Input);
                    p.Add("@iccid_old", iccid_old, DbType.String, ParameterDirection.Input, 20);
                    p.Add("@iccid_new", iccid_new, DbType.String, ParameterDirection.Input, 22);
                    p.Add("@processby", processby, DbType.String, ParameterDirection.Input, 50);
                    if (address_update == 1)
                    {
                        p.Add("@address_update", address_update, DbType.Int32, ParameterDirection.Input);
                        p.Add("@address1", address1, DbType.String, ParameterDirection.Input, 150);
                        p.Add("@address2", address2, DbType.String, ParameterDirection.Input, 150);
                        p.Add("@address3", address3, DbType.String, ParameterDirection.Input, 150);
                        p.Add("@postcode", postcode, DbType.String, ParameterDirection.Input, 150);
                        p.Add("@city", city, DbType.String, ParameterDirection.Input, 150);
                    }

                    var result = conn.Query<Common.ErrCodeMsg>(sql, p, null, true, null, CommandType.StoredProcedure).FirstOrDefault();
                    return result;
                }
            }
            catch (Exception e)
            {
                Log.Error("CRM_API.DB.SIMOrder.SProc.UpdateFreeSimWithReplacement: " + e.Message);
                throw new Exception("CRM_API.DB.SIMOrder.SProc.UpdateFreeSimWithReplacement: " + e.Message);
            }
        }

        public static bool UpdateFreeSimOrder(FreesimOrderItem oItem, int newSimStatus, string loggeduser, string royal_mail_ref = "")
        {
            string sql = "crm3_usp_updatefreesim_bysubscriberid_registerdate_3";
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var p = new DynamicParameters();
                    p.Add("@subscriberid", oItem.subscriberid, DbType.Int32, ParameterDirection.Input, int.MaxValue);
                    p.Add("@iccid", oItem.iccid, DbType.String, ParameterDirection.Input, 20);
                    if (oItem.sitecode == "GCM")
                        p.Add("@registerdate", oItem.registerdate.ToString("yyyy/MM/dd"), DbType.String, ParameterDirection.Input, 20);
                    else
                        p.Add("@registerdate", oItem.registerdate.ToString("yyyy/MM/dd"), DbType.String, ParameterDirection.Input, 20);
                    p.Add("@freesimstatus", newSimStatus, DbType.Int32, ParameterDirection.Input, int.MaxValue);
                    p.Add("@crmuser", loggeduser, DbType.String, ParameterDirection.Input, 16);
                    p.Add("@sitecode", oItem.sitecode, DbType.String, ParameterDirection.Input, 3);
                    p.Add("@ReturnValue", null, DbType.Int32, ParameterDirection.ReturnValue, 4);
                    p.Add("@royal_mail_ref", royal_mail_ref, DbType.String, ParameterDirection.Input, 100);

                    var resx = conn.Execute(sql, p, null, null, CommandType.StoredProcedure);
                    if (resx >= 1)
                        return true;
                    else
                        return false;
                    //return result;
                }
            }
            catch (Exception e)
            {
                Log.Error("CRM_API.DB.SIMOrder.SProc.UpdateFreeSimOrder: " + e.Message);
                throw new Exception("CRM_API.DB.SIMOrder.SProc.UpdateFreeSimOrder: " + e.Message);
            }
        }

        public static Common.ErrCodeMsg UpdateFreeSimOrder2in1(int mimsi_orderid, int freesim_id, string status, string sitecode, string crmuser, string iccid = null, string iccid_swap = null, string royal_mail_ref = "")
        {
            string sql = "crm3_freesim_update_status";
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var p = new DynamicParameters();
                    p.Add("@mimsi_order_id ", mimsi_orderid, DbType.Int32, ParameterDirection.Input, int.MaxValue);
                    p.Add("@freesim_id", freesim_id, DbType.Int32, ParameterDirection.Input, 20);
                    p.Add("@status", status, DbType.String, ParameterDirection.Input, 15);
                    p.Add("@sitecode", sitecode, DbType.String, ParameterDirection.Input, 3);
                    p.Add("@crmuser", crmuser, DbType.String, ParameterDirection.Input, 16);
                    p.Add("@iccid", iccid, DbType.String, ParameterDirection.Input, 20);
                    p.Add("@iccidswap", iccid_swap, DbType.String, ParameterDirection.Input, 20);
                    p.Add("@royal_mail_ref", royal_mail_ref, DbType.String, ParameterDirection.Input, 100);

                    var result = conn.Query<Common.ErrCodeMsg>(sql, p, null, true, null, CommandType.StoredProcedure).FirstOrDefault();
                    return result;
                }
            }
            catch (Exception e)
            {
                Log.Error("CRM_API.DB.SIMOrder.SProc.UpdateFreeSimOrder2in1: " + e.Message);
                throw new Exception("CRM_API.DB.SIMOrder.SProc.UpdateFreeSimOrder2in1: " + e.Message);
            }
        }

        public static bool ValidateIccidInput(string iccid, string orderid, string product_code, string sitecode)
        {
            string sql = "sim_dispatch_get_SIM_category";
            try
            {
                // brandtype 1 = vectone , 2 Delight, 3=Unitedfone, 4=GoCheap, 
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var p = new DynamicParameters();
                    p.Add("@iccid", iccid, DbType.String, ParameterDirection.Input, int.MaxValue);
                    p.Add("@is_2in1", 0, DbType.Int32, ParameterDirection.Output, int.MaxValue);
                    p.Add("@brandtype", 0, DbType.Int32, ParameterDirection.Output, int.MaxValue);
                    p.Add("@sitecode", "", DbType.String, ParameterDirection.Output, 3);
                    p.Add("@country", "", DbType.String, ParameterDirection.Output, 20);
                    p.Add("@errcode", 0, DbType.Int32, ParameterDirection.Output, int.MaxValue);
                    p.Add("@errmsg", "", DbType.String, ParameterDirection.Output, 100);

                    conn.Execute(sql, p, null, null, CommandType.StoredProcedure);
                    if (p.Get<int>("@errcode") != 0)
                        throw new Exception(p.Get<string>("@errmsg"));

                    var result = new Common.ErrCodeMsg()
                    {
                        errcode = p.Get<int>("@errcode"),
                        errmsg = p.Get<string>("@errmsg")
                    };

                    if (sitecode != p.Get<string>("@sitecode"))
                        throw new Exception("Iccid product/country not match.");
                    if (product_code.ToLower().Contains("vm") && p.Get<int>("@brandtype") != 1)
                        throw new Exception("Iccid product is not Vectone product.");
                    if (product_code.ToLower().Contains("dm") && p.Get<int>("@brandtype") != 2)
                        throw new Exception("Iccid product is not Delight product.");
                    if (orderid.ToLower().Contains("m") && p.Get<int>("@is_2in1") != 1)
                        throw new Exception("Iccid is not compatible for 2in1.");


                    return true;
                }
            }
            catch (Exception e)
            {
                Log.Error("CRM_API.DB.SIMOrder.SProc.GetIccidInfo: " + e.Message);
                throw new Exception("CRM_API.DB.SIMOrder.SProc.GetIccidInfo: " + e.Message);
            }
        }
        public static IEnumerable<FreesimOrderItem> GetFreeSimByFullnameGroupBySubscriberidemail(string email, string sitecode)
        {
            string sql = "crm3_getfreesim_get_by_email_groupsubscriberid";
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<FreesimOrderItem>(sql, new
                    {
                        @email = email,
                        @sitecode = sitecode
                    }, null, true, (60 * 1000), CommandType.StoredProcedure);
                    return result;
                }
            }
            catch (Exception e)
            {
                Log.Error("CRM_API.DB.SIMOrder.SProc.GetUnprocessedOrder: " + e.Message);
                throw new Exception("CRM_API.DB.SIMOrder.SProc.GetUnprocessedOrder: " + e.Message);
            }
        }
        public static IEnumerable<FreesimOrderItem> GetFreeSimByFullnameGroupBySubscriberidiccid(string email, string sitecode)
        {
            string sql = "crm3_getfreesim_get_by_iccid_groupsubscriberid";
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<FreesimOrderItem>(sql, new
                    {
                        @iccid = email,
                        @sitecode = sitecode
                    }, null, true, (60 * 1000), CommandType.StoredProcedure);
                    return result;
                }
            }
            catch (Exception e)
            {
                Log.Error("CRM_API.DB.SIMOrder.SProc.GetUnprocessedOrder: " + e.Message);
                throw new Exception("CRM_API.DB.SIMOrder.SProc.GetUnprocessedOrder: " + e.Message);
            }
        }
        public static IEnumerable<FreesimOrderItem> GetFreeSimByFullnameGroupBySubscriberidcybersourceid(string email, string sitecode)
        {
            string sql = "crm3_getfreesim_get_by_payrefernce_groupsubscriberid";
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<FreesimOrderItem>(sql, new
                    {
                        @pay_refernce = email,
                        @sitecode = sitecode
                    }, null, true, (60 * 1000), CommandType.StoredProcedure);
                    return result;
                }
            }
            catch (Exception e)
            {
                Log.Error("CRM_API.DB.SIMOrder.SProc.GetUnprocessedOrder: " + e.Message);
                throw new Exception("CRM_API.DB.SIMOrder.SProc.GetUnprocessedOrder: " + e.Message);
            }
        }
        public static IEnumerable<FreesimOrderItem> GetFreeSimByFullnameGroupBySubscriberidcybermobileno(string email, string sitecode)
        {
            string sql = "crm3_getfreesim_get_by_mobileno_groupsubscriberid";
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<FreesimOrderItem>(sql, new
                    {
                        @mobileno = email,
                        @sitecode = sitecode
                    }, null, true, (60 * 1000), CommandType.StoredProcedure);
                    return result;
                }
            }
            catch (Exception e)
            {
                Log.Error("CRM_API.DB.SIMOrder.SProc.GetUnprocessedOrder: " + e.Message);
                throw new Exception("CRM_API.DB.SIMOrder.SProc.GetUnprocessedOrder: " + e.Message);
            }
        }

        //25-Dec-2018 : Added for Cancel Sim Order
        public static IEnumerable<CancelFreeSimOrderOutput> CancelFreeSimOrder(int freesimid, string mobileno, string sitecode, string logged_user)
        {
            string sql = "crm3_cancel_freesim_order";
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CancelFreeSimOrderOutput>(sql, new
                    {
                        @freesimid = freesimid,
                        @mobileno = mobileno,
                        @sitecode = sitecode,
                        @crm_user = logged_user
                    }, null, true, (60 * 1000), CommandType.StoredProcedure);
                    return result;
                }
            }
            catch (Exception e)
            {
                Log.Error("CRM_API.DB.SIMOrder.SProc.CancelFreeSimOrder: " + e.Message);
                throw new Exception("CRM_API.DB.SIMOrder.SProc.CancelFreeSimOrder: " + e.Message);
            }
        }
    }
}