﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Dapper;
using System.Threading.Tasks;

namespace CRM_API.DB.Ref.Donor.AT
{
    public class SProc
    {
        public static IEnumerable<Models.Porting.AT.SoapReceiver.Transactions.Ping> GetDonorOperator(string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Models.Porting.AT.SoapReceiver.Transactions.Ping>(
                            "crm3_get_mnpref",new {
                                @sitecode = sitecode
                            },
                            commandType: CommandType.StoredProcedure);

                    return result ?? new List<Models.Porting.AT.SoapReceiver.Transactions.Ping>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL CRM_API.DB.Ref.Donor.AT.GetDonorOperator: " + ex.Message);
            }
        }
    }
}
