﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;

namespace CRM_API.DB.Country
{
    public class CountrySProc
    {
        public static IEnumerable<Models.CountryInfo> GetAllCountries()
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Models.CountryInfo>(
                            "crm3_countries_list_all", 
                            commandType: CommandType.StoredProcedure);

                    return result ?? new List<Models.CountryInfo>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL GetAllCountries: " + ex.Message);
            }
        }

        public static Models.ConnectionString GetConnection(string siteCode)
        {
            try
            {
                //using (var conn = new SqlConnection(CRM_API.Helpers.Config.UserMgtConnection))
                //Old SP:crm3_get_db_info

                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Models.ConnectionString>(
                            "crm3_get_db_info_by_sitecode", new
                            {
                                @Sitecode = siteCode
                            }, commandType: CommandType.StoredProcedure);

                    return result.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL GetConnection: " + ex.Message);
            }
        }

        public static IEnumerable<Models.CountryInfo> Get2in1CountriesByProductCode(string product_code)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Models.CountryInfo>(
                            "crm3_2in1_newcountries_list", new
                            {
                                @product_code = product_code
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<Models.CountryInfo>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL Get2in1CountriesByProductCode: " + ex.Message);
            }
        }

        public static string GetCountryByMSIDN(string mobileNo)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<string>(
                            string.Format("select dbo.crm3_country_msisdn('{0}')", mobileNo),
                            null
                            , commandType: CommandType.Text);

                    return result.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL GetCountryByMSIDN: " + ex.Message);
            }
            
        }
    }
}
