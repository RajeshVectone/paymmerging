﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Dapper;
using System.Threading.Tasks;

namespace CRM_API.DB
{
    public class PAYMPaymentProfile
    {
        //Get Credit Card Details
        public static IEnumerable<CRM_API.Models.PAYMCreditCard_v1> getCreditCard(string MobileNo, string cc_no)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.MCMCRMConnection))
                {
                    conn.Open();
                    //var result = conn.Query<CRM_API.Models.PAYMCreditCard>("c3pm_getCCAccount_byMobileno", new
                    var result = conn.Query<CRM_API.Models.PAYMCreditCard_v1>("cc_details_by_mobileno", new
                    {
                        @lastdigit_no = cc_no,
                        @mobileno = MobileNo

                        //@mobileno = MobileNo
                    }, commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.PAYMCreditCard_v1>();
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("cc_details_by_mobileno  :  " + ex.Message);
            }
        }

        //Get Bank A/c Detail
        public static IEnumerable<CRM_API.Models.PAYMBankAccount> getBankAccount(string MobileNo, string cc_no)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.MCMCRMConnection))
                {
                    conn.Open();
                    //var result = conn.Query<CRM_API.Models.PAYMBankAccount>("c3pm_getDDAccount_byMobileno", new
                    var result = conn.Query<CRM_API.Models.PAYMBankAccount>("dd_details_by_mobileno", new
                    {
                        @lastdigit_no = cc_no,
                        @mobileno = MobileNo
                    }, commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.PAYMBankAccount>();
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("dd_details_by_mobileno  :  " + ex.Message);
            }
        }

        //Get List Out A/C - Credit Card
        public static IEnumerable<CRM_API.Models.PaymPaymentHistroy> getAllPaymentProfile(string MobileNo)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.MCMCRMConnection))
                {
                    conn.Open();
                    //var result = conn.Query<CRM_API.Models.PaymPaymentHistroy>("c3pm_dd_payment_profile_byMSISDN", new
                    var result = conn.Query<CRM_API.Models.PaymPaymentHistroy>("get_PaymentProfile_by_mobileno", new
                    {
                        //@msisdn = MobileNo
                        @mobileno = MobileNo 
                    }, commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.PaymPaymentHistroy>();
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("get_PaymentProfile_by_mobileno  :  " + ex.Message);
            }
        }
    }
}
