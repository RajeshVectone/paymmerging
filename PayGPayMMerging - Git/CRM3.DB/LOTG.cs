﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;
namespace CRM_API.DB.LOTG
{
    public class SProc 
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public static async Task<IEnumerable<CRM_API.Models.LOTG.LLOTG>> GetCollectionAsync(string mobileno, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result =
                        Task.Run(() =>
                        {
                            return conn.Query<CRM_API.Models.LOTG.LLOTG>(
                                "Crm3_Get_LLOTG_Details_by_Mobileno", new
                                {
                                    @mobileno = mobileno,
                                    @sitecode = sitecode
                                }, commandType: CommandType.StoredProcedure);
                        }
                    );
                    return await result ?? new List<CRM_API.Models.LOTG.LLOTG>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.LOTG.SProc.GetCollectionAsync: " + ex.Message);
                throw new Exception("CRM_API.DB.LOTG.SProc.GetCollectionAsync: " + ex.Message);
            }
        }

        public static async Task<IEnumerable<CRM_API.Models.LOTG.Collection>> GetCollectionAsync_Old(string mobileno, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result =
                        Task.Run(() =>
                        {
                            return  conn.Query<CRM_API.Models.LOTG.Collection>(
                                "crm3_lotg_list", new
                                {
                                    @mobileno = mobileno,
                                    @sitecode = sitecode
                                }, commandType: CommandType.StoredProcedure);
                        }
                    );
                    return await result ?? new List<CRM_API.Models.LOTG.Collection>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.LOTG.SProc.GetCollectionAsync: " + ex.Message);
                throw new Exception("CRM_API.DB.LOTG.SProc.GetCollectionAsync: " + ex.Message);
            }
        }

        /*added on 17 Jan 2015 - by Hari*/
        public static async Task<IEnumerable<CRM_API.Models.LOTG.LLOTG>> GetCollectionLLOMAsync(string mobileno, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result =
                        Task.Run(() =>
                        {
                            return conn.Query<CRM_API.Models.LOTG.LLOTG>(
                                "Crm3_Get_LLOTG_Details_by_Mobileno", new
                                {
                                    @mobileno = mobileno,
                                    @sitecode = sitecode
                                }, commandType: CommandType.StoredProcedure);
                        }
                    );
                    return await result ?? new List<CRM_API.Models.LOTG.LLOTG>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.LOTG.SProc.GetCollectionLLOMAsync: " + ex.Message);
                throw new Exception("CRM_API.DB.LOTG.SProc.GetCollectionLLOMAsync: " + ex.Message);
            }
        }
        /*end hari*/

        /*added on 19 Jan 2015 - by Hari*/
        public static async Task<IEnumerable<CRM_API.Models.LOTG.LLOMSubscriptionHistoryModel>> GetLLOMSubscriptionHistory(string mobileno, string sitecode, int reg_no)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result =
                        Task.Run(() =>
                        {
                            String[] paramVal = mobileno.Split('|');
                            return conn.Query<CRM_API.Models.LOTG.LLOMSubscriptionHistoryModel>(
                                "Subscription_History_LLOM", new
                                {
                                    @MobileNo = paramVal[0],
                                    @Reg_Number = paramVal[1],                                     
                                    @SiteCode = sitecode,                                    
                                }, commandType: CommandType.StoredProcedure);
                        }
                    );
                    return await result ?? new List<CRM_API.Models.LOTG.LLOMSubscriptionHistoryModel>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.LOTG.SProc.GetCollectionLLOMAsync: " + ex.Message);
                throw new Exception("CRM_API.DB.LOTG.SProc.GetCollectionLLOMAsync: " + ex.Message);
            }
        }
        /*end hari*/

        /*added on 09 Feb 2015 - by Hari*/
        public static async Task<IEnumerable<CRM_API.Models.LOTG.LLOMSubscriptionHistoryModel>> GetLLOMSubscriptionHistory_v1(string mobileno, string sitecode, int reg_no)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result =
                        Task.Run(() =>
                        {
                            String[] paramVal = mobileno.Split('|');
                            return conn.Query<CRM_API.Models.LOTG.LLOMSubscriptionHistoryModel>(
                                "Subscription_History_LLOM_V1", new
                                {
                                    @MobileNo = paramVal[0],
                                    @Reg_Number = paramVal[1],
                                    @SiteCode = sitecode,
                                }, commandType: CommandType.StoredProcedure);
                        }
                    );
                    return await result ?? new List<CRM_API.Models.LOTG.LLOMSubscriptionHistoryModel>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.LOTG.SProc.GetCollectionLLOMAsync: " + ex.Message);
                throw new Exception("CRM_API.DB.LOTG.SProc.GetCollectionLLOMAsync: " + ex.Message);
            }
        }
        /*end hari*/

        /*added on 19 Jan 2015 - by Hari*/
        public static async Task<IEnumerable<CRM_API.Models.Bundle.SubscribedBundle>> GetBundleDetail(string mobileno, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result =
                        Task.Run(() =>
                        {
                            return conn.Query<CRM_API.Models.Bundle.SubscribedBundle>(
                                "crm3_get_list_subscribed_bundle_paym", new
                                {
                                    @mobileno = mobileno,
                                    @sitecode = sitecode
                                }, commandType: CommandType.StoredProcedure);
                        }
                    );
                    return await result ?? new List<CRM_API.Models.Bundle.SubscribedBundle>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.LOTG.SProc.GetCollectionLLOMAsync: " + ex.Message);
                throw new Exception("CRM_API.DB.LOTG.SProc.GetCollectionLLOMAsync: " + ex.Message);
            }
        }
        /*end hari*/
             
        /*added on 19 Jan 2015 - by Hari*/
        public static async Task<IEnumerable<CRM_API.Models.LOTG.CountrySaverSubscriptionHistoryModel>> GetBundleSubscriptionHistory(string mobileno, string sitecode, int bundleid)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    /*
                    var result =
                        Task.Run(() =>
                        {
                            
                            return conn.Query<CRM_API.Models.LOTG.CountrySaverSubscriptionHistoryModel>(
                                "Subscription_History_Bundle ", new
                                {
                                    @mobileno = mobileno,
                                    @sitecode = sitecode,
                                    @bundleId = bundleid
                                }, commandType: CommandType.StoredProcedure);                            
                        }
                    );
                    */

                  
                    var resultnew =
                        Task.Run(() =>
                        {
                            var result = conn.Query<dynamic>(
                              "Subscription_History_Bundle", new
                              {
                                  @mobileno = mobileno,
                                  @sitecode = sitecode,
                                  @bundleId = bundleid
                              }, commandType: CommandType.StoredProcedure);
                            List<CRM_API.Models.LOTG.CountrySaverSubscriptionHistoryModel> lstCountrySubscriptionHistory = new List<CRM_API.Models.LOTG.CountrySaverSubscriptionHistoryModel>();

                            for (int i = 0; i < result.Count(); i++)
                            {
                                CRM_API.Models.LOTG.CountrySaverSubscriptionHistoryModel model = new CRM_API.Models.LOTG.CountrySaverSubscriptionHistoryModel();
                                var row = result.ElementAt(i);
                                foreach (var kv in row)
                                {
                                    if (kv.Key == "Bundle Details")
                                    {
                                        model.BundleDetails = kv.Value;
                                    }
                                    else if (kv.Key == "Payment Status")
                                    {
                                        model.PaymentStatus = kv.Value;
                                    }
                                }
                                model.Action = row.Action;
                                model.Amount = row.Amount.ToString();
                                model.PaymentMode = row.PaymentMode;
                                model.Date = row.Date;
                                lstCountrySubscriptionHistory.Add(model);

                            }
                            return lstCountrySubscriptionHistory;
                        }
                );
                   return await resultnew ?? new List<CRM_API.Models.LOTG.CountrySaverSubscriptionHistoryModel>();
                    
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.LOTG.SProc.GetBundleSubscriptionHistory: " + ex.Message);
                throw new Exception("CRM_API.DB.LOTG.SProc.GetBundleSubscriptionHistory: " + ex.Message);
            }
        }
        /*end hari*/

        /*added on 19 Jan 2015 - by Hari*/
        public static async Task<IEnumerable<CRM_API.Models.LOTG.CountrySaverSubscriptionHistoryModel>> GetCountrySaverSubscriptionHistory(string mobileno, string sitecode, int bundleid)
        {
            try
            {
                //using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                //{
                //    var result =
                //        Task.Run(() =>
                //        {
                //            return conn.Query<CRM_API.Models.LOTG.CountrySaverSubscriptionHistoryModel>(
                //                "Subscription_History_CountrySaver", new
                //                {
                //                    @mobileno = mobileno,
                //                    @sitecode = sitecode,
                //                    @bundleId = bundleid
                //                }, commandType: CommandType.StoredProcedure);
                //        }
                //    );
                //    return await result ?? new List<CRM_API.Models.LOTG.CountrySaverSubscriptionHistoryModel>();
                //}
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var resultnew =
                            Task.Run(() =>
                            {
                                var result = conn.Query<dynamic>(
                                  "Subscription_History_CountrySaver", new
                                  {
                                      @mobileno = mobileno,
                                      @sitecode = sitecode,
                                      @bundleId = bundleid
                                  }, commandType: CommandType.StoredProcedure);
                                List<CRM_API.Models.LOTG.CountrySaverSubscriptionHistoryModel> lstCountrySubscriptionHistory = new List<CRM_API.Models.LOTG.CountrySaverSubscriptionHistoryModel>();

                                for (int i = 0; i < result.Count(); i++)
                                {
                                    CRM_API.Models.LOTG.CountrySaverSubscriptionHistoryModel model = new CRM_API.Models.LOTG.CountrySaverSubscriptionHistoryModel();
                                    var row = result.ElementAt(i);
                                    foreach (var kv in row)
                                    {
                                        if (kv.Key == "Bundle Details")
                                        {
                                            model.BundleDetails = kv.Value;
                                        }
                                        else if (kv.Key == "Payment Status")
                                        {
                                            model.PaymentStatus = kv.Value;
                                        }
                                    }
                                    model.Action = row.Action;
                                    model.Amount = row.Amount.ToString();
                                    model.PaymentMode = row.PaymentMode;
                                    model.Date = row.Date;
                                    lstCountrySubscriptionHistory.Add(model);

                                }
                                return lstCountrySubscriptionHistory;
                            }
                    );
                    return await resultnew ?? new List<CRM_API.Models.LOTG.CountrySaverSubscriptionHistoryModel>();
                }
                    
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.LOTG.SProc.GetCountrySaverSubscriptionHistory: " + ex.Message);
                throw new Exception("CRM_API.DB.LOTG.SProc.GetCountrySaverSubscriptionHistory: " + ex.Message);
            }
        }
        /*end hari*/


        public static async Task<CRM_API.Models.LOTG.DetailLOTG> GetDetailAsync(string reg_number, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result =
                        Task.Run(() =>
                        {
                            return conn.Query<CRM_API.Models.LOTG.DetailLOTG>(
                                "crm3_lotg_list_detail", new
                                {
                                    @reg_number = reg_number,
                                    @sitecode = sitecode
                                }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                        }
                    );
                    return await result ?? new CRM_API.Models.LOTG.DetailLOTG();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.LOTG.SProc.GetDetailAsync: " + ex.Message);
                throw new Exception("CRM_API.DB.LOTG.SProc.GetDetailAsync: " + ex.Message);
            }
        }
        public static async Task<CRM_API.Models.LOTG.Response> RenewContract(string mobileno, string reg_number,string sitecode, string loggedUser)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result =
                        Task.Run(() =>
                        {
                            return conn.Query<CRM_API.Models.LOTG.Response>(
                                "crm3_lotg_renew", new
                                {
                                    @mobileno = mobileno,
                                    @sitecode = sitecode,
                                    @crm_login = loggedUser,
                                    @reg_number = reg_number
                                }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                        }
                    );
                    return await result ?? new CRM_API.Models.LOTG.Response();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.LOTG.SProc.RenewContract: " + ex.Message);
                throw new Exception("CRM_API.DB.LOTG.SProc.RenewContract: " + ex.Message);
            }
        }
        public static async Task<CRM_API.Models.LOTG.Response> UnsubscribeContract(string mobileno, string reg_number, string sitecode, string loggedUser)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result =
                        Task.Run(() =>
                        {
                            return conn.Query<CRM_API.Models.LOTG.Response>(
                                "crm3_lotg_unsubcribe", new
                                {
                                    @mobileno = mobileno,
                                    @sitecode = sitecode,
                                    @crm_login = loggedUser,
                                    @reg_number = reg_number
                                }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                        }
                    );
                    return await result ?? new CRM_API.Models.LOTG.Response();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.LOTG.SProc.UnsubscribeContract: " + ex.Message);
                throw new Exception("CRM_API.DB.LOTG.SProc.UnsubscribeContract: " + ex.Message);
            }
        }
        public static async Task<CRM_API.Models.LOTG.Response> ChangePaymentMode(string mobileno, string reg_number, string sitecode, int
            paymode, string loggedUser)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result =
                        Task.Run(() =>
                        {
                            return conn.Query<CRM_API.Models.LOTG.Response>(
                                "crm3_lotg_change_payment", new
                                {
                                    @mobileno = mobileno,
                                    @sitecode = sitecode,
                                    @paymode = paymode,
                                    @crm_login = loggedUser,
                                    @reg_number = reg_number
                                }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                        }
                    );
                    return await result ?? new CRM_API.Models.LOTG.Response();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.LOTG.SProc.ChangePaymentMode: " + ex.Message);
                throw new Exception("CRM_API.DB.LOTG.SProc.ChangePaymentMode: " + ex.Message);
            }
        }

        public static Models.Common.ErrCodeMsg BundleConfirmCancel(string mobileno, string sitecode, int bundleid, string loggedUser)
        {
            try
            {

                //Int32 errcode;
                //string errmsg = ""; 
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var p = new DynamicParameters();
                    p.Add("@sitecode", sitecode);
                    p.Add("@usertype", 2);
                    p.Add("@userinfo", mobileno);
                    p.Add("@bundleid", bundleid);
                    p.Add("@processby", loggedUser);
                    //p.Add("@errcode", dbType: DbType.Int32, size: 3, direction: ParameterDirection.Output);
                    p.Add("@errcode", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    p.Add("@errmsg", dbType: DbType.String, size: 200, direction: ParameterDirection.Output);
                    //conn.Execute("crm3_bundle_cancel_paym", p, null, null, CommandType.StoredProcedure);
                    var err = conn.Query<CRM_API.Models.Common.ErrCodeMsg>("crm3_bundle_cancel_paym", p, commandType: CommandType.StoredProcedure).FirstOrDefault();
                     //errcode = p.Get<Int32>("errcode");
                     //errmsg = p.Get<string>("errmsg");

                    if (err.errcode == 0)
                    { SMSDeliveryContent.SMS_BundleCancelService(mobileno, 2, bundleid.ToString()); }

                    return new Models.Common.ErrCodeMsg()
                    {
                        errcode = err.errcode,
                        errmsg = err.errmsg
                    };
                }
                
                

            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.LOTG.SProc.BundleConfirmCancel: " + ex.Message);
                throw new Exception("CRM_API.DB.LOTG.SProc.BundleConfirmCancel: " + ex.Message);
            }
        }

        public static async Task<CRM_API.Models.LOTG.Response> LLOMConfirmCancel(string mobileno, string sitecode, string reg_number, int svc_id)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result =
                        Task.Run(() =>
                        {
                            var err = conn.Query<CRM_API.Models.LOTG.Response>(
                                "crm3_LLOTG_Multinb_cancel_paym", new
                                {
                                    @mobileno = mobileno,
                                    @sitecode = sitecode,
                                    @reg_number = reg_number,
                                    @svc_id = svc_id
                                }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                            
                            if (err.errcode == 0)
                            { SMSDeliveryContent.SMS_BundleCancelService(mobileno, 1, reg_number); }

                            return err;
                        }
                    );

                    

                    return await result ?? new CRM_API.Models.LOTG.Response();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.LOTG.SProc.LLOMConfirmCancel: " + ex.Message);
                throw new Exception("CRM_API.DB.LOTG.SProc.LLOMConfirmCancel: " + ex.Message);
            }
        }
        
        
    
    
    }
}
