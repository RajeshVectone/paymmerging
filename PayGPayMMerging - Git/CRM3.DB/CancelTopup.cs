﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;

namespace CRM_API.DB
{
    public class CancelTopup
    {
        //Update CancelTopup
        public static CancelTopupOutput UpdateCancelTopup(CancelTopupInput input)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CancelTopupOutput>(
                            "crm_cancel_topup_by_mobileno", new
                            {
                                @sitecode = input.sitecode,
                                @paymentRef = input.paymentRef,
                                @mobileno = input.mobileno,
                                @Reason = input.Reason,
                                @refund_by = input.requestedby,
                                @amount = input.Amount,
                                @process_type = input.process_type
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    return result ?? new CancelTopupOutput();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL UpdateBalance: " + ex.Message);
            }
        }

        //Insert CancelTopup
        public static CancelTopupOutput InsertCancelTopup(CancelTopupInput input)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CancelTopupOutput>(
                            "crm_docancel_topup_request", new
                            {
                                @mobileno = input.mobileno,
                                @paymentRef = input.paymentRef,
                                @amount = input.Amount,
                                @requestedby = input.requestedby,
                                @sitecode = input.sitecode
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    return result ?? new CancelTopupOutput();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL UpdateBalance: " + ex.Message);
            }
        }

        //CancelTopupReport
        public static IEnumerable<CancelTopupReportOutput> GetCancelTopupReport(CancelTopupInput req)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CancelTopupReportOutput>(
                            "crm_cancel_topup_report", new
                            {
                                //sitecode = req.sitecode,
                                date_from = req.date_from,
                                date_to = req.date_to
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CancelTopupReportOutput>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL GetCancelTopupReport: " + ex.Message);
            }
        }
    }
}
