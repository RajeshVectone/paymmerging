﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;

namespace CRM_API.DB.Package
{
    public class SProc
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public static IEnumerable<CRM_API.Models.Package.PackageList> PackageList(string mobileno, string status, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.Package.PackageList>(
                            "crm3_get_list_packageinfo_bymobileno", new
                            {
                                @mobileno = mobileno,
                                @status = string.IsNullOrEmpty(status) ? "" : status,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.Package.PackageList>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("BL PackageList: " + ex.Message);
                throw new Exception("BL PackageList: " + ex.Message);
            }
        }
        public static string InfoPackage(string cc, int bc, int sc, string sitecode,int packid)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    Common.ErrCodeMsg resvalue = new Common.ErrCodeMsg();

                    var p = new DynamicParameters();
                    p.Add("@cc", cc);
                    p.Add("@bc", bc);
                    p.Add("@sc", sc);
                    p.Add("@packid", packid);
                    p.Add("@sitecode", sitecode);
                    p.Add("@infotext", dbType: DbType.String,size:500, direction: ParameterDirection.Output);
                    p.Add("@ussdinfotext", dbType: DbType.String, size: 500, direction: ParameterDirection.Output);

                    conn.Execute("crm3_getInfoAccountPackage", p, commandType: CommandType.StoredProcedure);

                    return p.Get<string>("@ussdinfotext");
                }
            }
            catch (Exception ex)
            {
                Log.Error("BL InfoPackage: " + ex.Message);
                return "BL InfoPackage: " + ex.Message;
            }
        }
    }
}
