﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;

namespace CRM_API.DB.BlockUnBlockModels
{
    public class SProc
    {

       
        public static IEnumerable<CRM_API.Models.BlockUnBlockModels.BlockUnblock> Getverifybothblockandunblock(string Process_By, string product, string sitecode, int type, int mode, string reason, string account_info)
        {
            try
            {
                //Test SP Fixed data :crm3_get_bundleid_happy_dialing_number
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.BlockUnBlockModels.BlockUnblock>(
                            "Crm3_block_unblock_verify", new
                            {
                                @Process_By =Process_By,    
                                @product =product,    
                                @sitecode =sitecode,   
                                @type =type,       
                                @mode =mode,    
                                @reason =reason,   
                                @account_info=account_info  


                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.BlockUnBlockModels.BlockUnblock>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL Happy Bundle Dailling number list: " + ex.Message);
            }
        }

        public static IEnumerable<CRM_API.Models.BlockUnBlockModels.BlockUnblock> GetbothblockandunblockSP(string Process_By, string product, string sitecode, int type, int mode, string reason, string account_info)
        {
            try
            {
                //Test SP Fixed data :crm3_get_bundleid_happy_dialing_number
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.BlockUnBlockModels.BlockUnblock>(
                            "Crm3_Block_Unblock", new
                            {
                                @Process_By = Process_By,
                                @product = product,
                                @sitecode = sitecode,
                                @type = type,
                                @mode = mode,
                                @reason = reason,
                                @account_info = account_info

                            }, commandType: CommandType.StoredProcedure);

                    
                    return result ?? new List<CRM_API.Models.BlockUnBlockModels.BlockUnblock>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL Happy Bundle Dailling number list: " + ex.Message);
            }
        }

        public static IEnumerable<CRM_API.Models.BlockUnBlockModels.BlockUnBlockReports> Getbothblockandunblockreports(string product, int mode, DateTime startdate, DateTime enddate)
        {
            try
            {
                //Test SP Fixed data :Crm3_Get_Block_Unblock_Rpt

                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.BlockUnBlockModels.BlockUnBlockReports>(
                            "Crm3_Get_Block_Unblock_Rpt", new
                            {                               
                                @product = product,                                                         
                                @mode = mode,
                                @from_date = startdate,
                                @to_date = enddate

                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.BlockUnBlockModels.BlockUnBlockReports>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL Happy Bundle Dailling number list: " + ex.Message);
            }
        }


    }
}
