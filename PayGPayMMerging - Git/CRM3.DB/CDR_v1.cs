﻿using Dapper;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CRM_API.DB.CDR
{
    //06-Jan-2015 : Moorthy : Modified the 'CRM_Call_History_Search_SP_CBS' to 'CRM_Call_History_Search_SP'
    public class SProc_v1
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        //Added by Karthik Jira:CRMT-8 ALL 
        //Go Cheap All History data
        public static IEnumerable<CRM_API.Models.CDR.CallHistory_v1> GocheapALLCDR(string msisdn, DateTime dateFrom ,DateTime dateUntil, string sitecode, int type= 0)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    //Old SP: CRM_Call_History_Search_SP /**resion new sp balance going via bundle or balance**/
                    var result = conn.Query<CRM_API.Models.CDR.CallHistory_v1>(
                           "CRM_Call_History_Search_SP_CBS", new
                           {
                               @Msisdn = msisdn,
                               @DateFrom = dateFrom,
                               @DateTo = dateUntil,
                               @Type = type,
                               @SiteCode = sitecode
                           }, commandTimeout: (5 * 60), commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.CDR.CallHistory_v1>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.CDR.SProc GocheapCallCDR: {0}", ex.Message);
                throw new Exception("CRM_API.DB.CDR.SProc GocheapCallCDR: " + ex.Message);
            }
        }
        //Added by Karthik Jira:CRMT-8 Call 
        //Go Cheap Call History data
        public static IEnumerable<CRM_API.Models.CDR.CallHistory_v1> GocheapcallCDR(string msisdn, DateTime dateFrom, DateTime dateUntil, string sitecode, int type = 1)
        {
            try
            {
                //Old SP: CRM_Call_History_Search_SP /**resion new sp balance going via bundle or balance**/
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<CRM_API.Models.CDR.CallHistory_v1>(
                            "CRM_Call_History_Search_SP_CBS", new
                            {
                                @Msisdn = msisdn,
                                @DateFrom = dateFrom,
                                @DateTo = dateUntil,
                                @Type = type,
                                @SiteCode = sitecode
                            }, commandTimeout: (5 * 60), commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.CDR.CallHistory_v1>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.CDR.SProc GocheapSMSCDR: {0}", ex.Message);
                throw new Exception("CRM_API.DB.CDR.SProc GocheapSMSCDR: " + ex.Message);
            }
        }
        //Added by karthik Jira:CRMT-8
        //Go cheap For SMS data 
        public static IEnumerable<CRM_API.Models.CDR.CallHistory_v1> GocheapSMSCDR(string msisdn, DateTime dateFrom, DateTime dateUntil, string sitecode, int type = 2)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    //Old SP: CRM_Call_History_Search_SP /**resion new sp balance going via bundle or balance**/
                    conn.Open();

                    var result = conn.Query<CRM_API.Models.CDR.CallHistory_v1>(
                           "CRM_Call_History_Search_SP_CBS", new
                           {
                               @Msisdn = msisdn,
                               @DateFrom = dateFrom,
                               @DateTo = dateUntil,
                               @Type = type,
                               @SiteCode = sitecode
                           }, commandTimeout: (5 * 60), commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.CDR.CallHistory_v1>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.CDR.SProc GocheapSMSCDR: {0}", ex.Message);
                throw new Exception("CRM_API.DB.CDR.SProc GocheapSMSCDR: " + ex.Message);
            }
        }
        //Added by karthik Jira:CRMT-8
        //GO Cheap GPRS data
        public static IEnumerable<CRM_API.Models.CDR.CallHistory_v1> GocheapGPRSCDR(string msisdn, DateTime dateFrom, DateTime dateUntil, string sitecode, int type = 3)
        {
            try
            {
            //Old SP: CRM_Call_History_Search_SP /**resion new sp balance going via bundle or balance**/
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<CRM_API.Models.CDR.CallHistory_v1>(
                           "CRM_Call_History_Search_SP_CBS", new
                           {
                               @Msisdn = msisdn,
                               @DateFrom = dateFrom,
                               @DateTo = dateUntil,
                               @Type = type,
                               @SiteCode = sitecode
                           }, commandTimeout: (5 * 60), commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.CDR.CallHistory_v1>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.CDR.SProc GocheapCDR: {0}", ex.Message);
                throw new Exception("CRM_API.DB.CDR.SProc GocheapCDR: " + ex.Message);
            }
        }





        //Added by karthik Jira:CRMT-198
        //For All History data
        public static IEnumerable<CRM_API.Models.CDR.CallHistory_v1> AllHistory(string msisdn, DateTime dateFrom, DateTime dateto, string sitecode, string Packageid, int type = 0)
        {
            try
            {
                //Old SP: CRM_Call_History_Search_SP /**resion new sp balance going via bundle or balance**/
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<CRM_API.Models.CDR.CallHistory_v1>(
                            "CRM_Call_History_Search_SP_CBS", new
                            {
                                @msisdn = msisdn,
                                @datefrom = dateFrom,
                                @dateto = dateto,
                                @type = type,
                                @sitecode = sitecode,
                                @PackageID = Packageid ?? ""      
                            }, commandTimeout: (5 * 60), commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.CDR.CallHistory_v1>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.CDR_v1.SProc_v1 AllHistory: {0}", ex.Message);
                throw new Exception("CRM_API.DB.CDR.SProc_v1 AllHistory: " + ex.Message);
            }
        }

        //Added by karthik Jira:CRMT-198
        //For All History data
        public static IEnumerable<CRM_API.Models.CDR.CallHistory_v1> CallandSMSHistory(string msisdn, DateTime dateFrom, DateTime dateto, string sitecode, string Packageid, int type = 5)
        {
            //Old SP: CRM_Call_History_Search_SP /**resion new sp balance going via bundle or balance**/
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<CRM_API.Models.CDR.CallHistory_v1>(
                            "CRM_Call_History_Search_SP_CBS", new
                            {
                                @msisdn = msisdn,
                                @datefrom = dateFrom,
                                @dateto = dateto,
                                @type = type,
                                @sitecode = sitecode,
                                @PackageID = Packageid      
                            }, commandTimeout: (5 * 60), commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.CDR.CallHistory_v1>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.CDR_v1.SProc_v1 AllHistory: {0}", ex.Message);
                throw new Exception("CRM_API.DB.CDR.SProc_v1 AllHistory: " + ex.Message);
            }
        }

        //Added by karthik Jira:CRMT-198
        //for all Call history data
        public static IEnumerable<CRM_API.Models.CDR.CallHistory_v1> GetCallHistory(string msisdn, DateTime dateFrom, DateTime dateto, string sitecode,string Packageid,int  type = 1)
        {
            //Old SP: CRM_Call_History_Search_SP /**resion new sp balance going via bundle or balance**/
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<CRM_API.Models.CDR.CallHistory_v1>(
                            "CRM_Call_History_Search_SP_CBS", new
                            {
                                @msisdn = msisdn,
                                @datefrom = dateFrom,
                                @dateto = dateto,
                                @type = type,
                                @sitecode = sitecode,
                                @PackageID = Packageid   
                            }, commandTimeout: (5 * 60), commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.CDR.CallHistory_v1>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.CDR_v1.SProc_v1 GetCallHistory: {0}", ex.Message);
                throw new Exception("CRM_API.DB.CDR_v1.SProc_v1 GetCallHistory: " + ex.Message);
            }
        }
        //Added by karthik Jira:CRMT-198
        public static IEnumerable<CRM_API.Models.CDR.CallHistory_v1> GetSMSHistory(string msisdn, DateTime dateFrom, DateTime dateto, string sitecode, string Packageid, int type = 2)
        {
            //Old SP: CRM_Call_History_Search_SP /**resion new sp balance going via bundle or balance**/
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<CRM_API.Models.CDR.CallHistory_v1>(
                            "CRM_Call_History_Search_SP_CBS", new
                            {
                                @msisdn = msisdn,
                                @datefrom = dateFrom,
                                @dateto = dateto,
                                @type = type,
                                @sitecode = sitecode,
                                @PackageID = Packageid   
                            }, commandTimeout: (5 * 60), commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.CDR.CallHistory_v1>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.CDR_v1.SProc_v1 GetSMSHistory: {0}", ex.Message);
                throw new Exception("CRM_API.DB.CDR_v1.SProc_v1 GetSMSHistory: " + ex.Message);
            }
        }
        //Added by karthik Jira:CRMT-198
        public static IEnumerable<CRM_API.Models.CDR.CallHistory_v1> GetGPRSHistory(string msisdn, DateTime dateFrom, DateTime dateto, string sitecode, string Packageid, int type = 3)
        {
            //Old SP: CRM_Call_History_Search_SP /**resion new sp balance going via bundle or balance**/
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<CRM_API.Models.CDR.CallHistory_v1>(
                            "CRM_Call_History_Search_SP_CBS", new
                            {
                                @msisdn = msisdn,
                                @datefrom = dateFrom,
                                @dateto = dateto,
                                @type = type,
                                @sitecode = sitecode,
                                @PackageID = Packageid   
                            }, commandTimeout: (5 * 60), commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.CDR.CallHistory_v1>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.CDR_v1.SProc_v1 GetGPRSHistory: {0}", ex.Message);
                throw new Exception("CRM_API.DB.CDR_v1.SProc_v1 GetGPRSHistory: " + ex.Message);
            }
        }

        //25-Jan-2019 : Moorthy : Added for View Prefix
        public static IEnumerable<CRM_API.Models.CDR.ViewHistoryOutput> ViewPrefix()
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<CRM_API.Models.CDR.ViewHistoryOutput>(
                            "crm3_get_all_country_list", new
                            {
                            }, commandTimeout: (5 * 60), commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.CDR.ViewHistoryOutput>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.CDR_v1.SProc_v1 ViewPrefix: {0}", ex.Message);
                throw new Exception("CRM_API.DB.CDR_v1.SProc_v1 ViewPrefix: " + ex.Message);
            }
        }
    }
}

