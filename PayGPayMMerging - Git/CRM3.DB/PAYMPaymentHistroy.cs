﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Dapper;
using System.Threading.Tasks;

namespace CRM_API.DB
{
    public class PAYMPaymentHistroy
    {
        //CRM Enhancement 2
        public static IEnumerable<CRM_API.Models.PAYMPaymentDeatils> getPaymentDetails(string MobileNo, string SiteCode)
        {
            try
            {
                //using (var conn = new SqlConnection(CRM_API.Helpers.Config.MCMCRMConnection))
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    //var result = conn.Query<CRM_API.Models.PAYMPaymentDeatils>("c3pm_dd_payment_history_byMSISDN", new
                    var result = conn.Query<CRM_API.Models.PAYMPaymentDeatils>("crm3_paym_payment_history_by_msisdn", new
                    {
                       @mobileno = MobileNo,
                       @sitecode = SiteCode
                    }, commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.PAYMPaymentDeatils>();
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("c3pm_dd_payment_history_byMSISDN  :  " + ex.Message);
            }
        }
    }
}
