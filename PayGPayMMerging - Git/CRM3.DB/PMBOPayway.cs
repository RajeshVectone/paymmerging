﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Dapper;
using System.Threading.Tasks;

namespace CRM_API.DB
{
    public class PMBOPayway
    {
        public static IEnumerable<CRM_API.Models.PayWay> getPayWayFiles(string sitecode)
        {
            try
            {
                string ConnStr = "";
                if (sitecode.ToString().ToUpper() == "VMUK")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnection.ToString();
                }
                if (sitecode.ToString().ToUpper() == "VMNL")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnectionVMNL.ToString();
                }
                if (sitecode.ToString().ToUpper() == "VMFR")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnectionVMFR.ToString();
                }
                if (sitecode.ToString().ToUpper() == "VMAT")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnectionVMAT.ToString();
                }


                using (var conn = new SqlConnection(ConnStr))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.PayWay>("c3pm_get_operation_files ", new { }, commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.PayWay>();
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("c3pm_get_operation_files   :  " + ex.Message);
            }
        }

        public static IEnumerable<CRM_API.Models.PayWay> getFileNames(string FileID)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.MCMCRMConnection))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.PayWay>("select opsid, createdate, userid, user_name, operation_name, total_rows, filename from pmbo_operation_files (nolock)  where opsid='" + FileID + "'");
                    return result ?? new List<CRM_API.Models.PayWay>();
                }
            }
            catch (SqlException ex)
            {
                throw;
            }
        }

        public static IEnumerable<CRM_API.Models.DDgoSubmit> goSubmit(string sitecode)
        {
            string status = "pending_submission";
            //string status = "Failed";
            try
            {
                string ConnStr = "";
                if (sitecode.ToString().ToUpper() == "VMUK")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnection.ToString();
                }
                if (sitecode.ToString().ToUpper() == "VMNL")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnectionVMNL.ToString();
                }
                if (sitecode.ToString().ToUpper() == "VMFR")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnectionVMFR.ToString();
                }
                if (sitecode.ToString().ToUpper() == "VMAT")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnectionVMAT.ToString();
                }
                using (var conn = new SqlConnection(ConnStr))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.DDgoSubmit>("gocard_dd_cust_transaction_details", new { @status = status, @period = "" },
                        commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.DDgoSubmit>();

                }
            }
            catch (SqlException ex)
            {
                throw new Exception("gocard_dd_cust_transaction_details   :  " + ex.Message);
            }
        }

        public static IEnumerable<CRM_API.Models.DDgoSubmit> goReject(string sitecode)
        {
            //string status = "pending_submission";
            string status = "Failed";
            try
            {
                string ConnStr = "";
                if (sitecode.ToString().ToUpper() == "VMUK")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnection.ToString();
                }
                if (sitecode.ToString().ToUpper() == "VMNL")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnectionVMNL.ToString();
                }
                if (sitecode.ToString().ToUpper() == "VMFR")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnectionVMFR.ToString();
                }
                if (sitecode.ToString().ToUpper() == "VMAT")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnectionVMAT.ToString();
                }
                using (var conn = new SqlConnection(ConnStr))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.DDgoSubmit>("gocard_dd_cust_transaction_details", new { @status = status, @period = "" },
                        commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.DDgoSubmit>();

                }
            }
            catch (SqlException ex)
            {
                throw new Exception("gocard_dd_cust_transaction_details   :  " + ex.Message);
            }
        }
        public static IEnumerable<CRM_API.Models.DDgoSubmit> goSuccess(string sitecode)
        {
            //string status = "pending_submission";
            string status = "Paid_out";
            try
            {
                string ConnStr = "";
                if (sitecode.ToString().ToUpper() == "VMUK")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnection.ToString();
                }
                if (sitecode.ToString().ToUpper() == "VMNL")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnectionVMNL.ToString();
                }
                if (sitecode.ToString().ToUpper() == "VMFR")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnectionVMFR.ToString();
                }
                if (sitecode.ToString().ToUpper() == "VMAT")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnectionVMAT.ToString();
                }
                using (var conn = new SqlConnection(ConnStr))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.DDgoSubmit>("gocard_dd_cust_transaction_details", new { @status = status, @period = "" },
                        commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.DDgoSubmit>();

                }
            }
            catch (SqlException ex)
            {
                throw new Exception("gocard_dd_cust_transaction_details   :  " + ex.Message);
            }
        }
        public static int TransactionPayment(string sitecode, string productcode, string paymentagent, string servicetype,
           string paymentmode, int paymentstep, int paymentstatus, string accountid,
           string last6digitccno, double totalamount, string currency,
           string subscriptionid, string merchantid, string providercode, string csreasoncode, string generalerrorcode)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.PaymentConnection))
                {
                    conn.Open();

                    var result = conn.Query<int>(
                            "csPaymentInsert", new
                            {
                                @sitecode = sitecode,
                                @productcode = productcode,
                                @paymentagent = paymentagent,
                                @servicetype = servicetype,
                                @paymentmode = paymentagent,
                                @paymentstep = paymentstep,
                                @paymentstatus = paymentstatus,
                                @accountid = accountid,
                                @last6digitccno = last6digitccno,
                                @totalamount = totalamount,
                                @currency = currency,
                                @subscriptionid = subscriptionid,
                                @merchantid = merchantid,
                                @providercode = providercode,
                                @csreasoncode = csreasoncode,
                                @generalerrorcode = generalerrorcode
                            }, commandType: CommandType.StoredProcedure);

                    return result.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        //Update the Status for Download 
        public static void getUpdateStatus(string FileID)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.MCMCRMConnection))
                {
                    conn.Open();
                    conn.Execute("update pmbo_operation_files set Status=1 where opsid='" + FileID + "'");
                }
            }
            catch (SqlException ex)
            {
                throw;
            }
        }
    }
}
