﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using AutoMapper;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using CRM_API.DB.Common;

namespace CRM_API.DB.LLOTG
{

    public class Payment
    {
        private string paymentUrl = System.Configuration.ConfigurationManager.AppSettings["PaymentUrl"];
        //private static readonly string crm_connStr = ConfigurationManager.AppSettings["CRM"];
        //private static readonly string crm3_connStr = ConfigurationManager.AppSettings["CRM3"];
        //private static readonly string action = ConfigurationManager.AppSettings["Action"];
        public string MerchantReferenceCode { get; private set; }
        public string ReasonCode { get; private set; }
        public string Description { get; private set; }
        public string Decision { get; private set; }
        public string AcsURL { get; private set; }
        public string PaReq { get; private set; }
        public string PaRes { get; set; }
        public string SubscriptionId { get; private set; }
        static Payment()
        {
            Mapper.CreateMap<SubscriberPersonalDetailViewModel, SubscriberPersonalDetail>();
            Mapper.CreateMap<SubscriberPersonalDetail, SubscriberPersonalDetailViewModel>();
        }

        private static readonly Logger Log = LogManager.GetCurrentClassLogger();


        public int GetIdxUser(string account_id, string destination_number, string brand_type)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();


                    var result = conn.Query<dynamic>("fsb_get_idx_by_account", new
                    {
                        @account_id = account_id,
                        @destination_number = destination_number,
                        @brandtype = brand_type
                    }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    return result == null ? -1 : result.reg_idx;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.ToString());
            }
        }

        public void SubcriptionCharge(string siteCode, string applicationCode,
            string accountId, string Last6DigitCC, string currency, double price, string cvv)
        {
            string productCode;
            if (siteCode == "MCM")
            {
                siteCode = "MCM";
            }
            else if (siteCode == "MFR")
            {
                siteCode = "FR2";
            }
            else if (siteCode == "BNL")
            {
                siteCode = "NL1";
            }
            else if (siteCode == "BAU")
            {
                siteCode = "AT1";
            }
            else if (siteCode == "BSE")
            {
                siteCode = "BSE";
            }
            else if (siteCode == "BDK")
            {
                siteCode = "VMDK";
            }
            else if (siteCode == "MFI")
            {
                siteCode = "VMFI";
            }
            else if (siteCode == "MPT")
            {
                siteCode = "PT1";
            }
            else
            {
                siteCode = "MCM";
            }
            productCode = "VRN";
            var soapRequest = new StringBuilder(string.Format("?site-code={0}&", siteCode));
            soapRequest.AppendFormat("application-code={0}&", applicationCode);
            soapRequest.AppendFormat("product-code={0}&", productCode);
            soapRequest.AppendFormat("payment-agent={0}&", applicationCode);
            soapRequest.AppendFormat("service-type={0}&", ServiceType.RES);
            soapRequest.AppendFormat("payment-mode={0}&", PaymentMode.SUB);
            soapRequest.AppendFormat("account-id={0}&", accountId);
            soapRequest.AppendFormat("cc-no={0}&", Last6DigitCC);
            soapRequest.AppendFormat("cc-cvv={0}&", cvv);
            soapRequest.AppendFormat("amount={0:0.00}&", price);
            soapRequest.AppendFormat("currency={0}&", currency);
            soapRequest.Append("capture=1");

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(paymentUrl);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/plain"));
                //var content = new StringContent(soapRequest.ToString());
                //content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                try
                {


                    var response = client.GetAsync("/transaction.svc/subscription/authorize" + Uri.EscapeUriString(soapRequest.ToString())).Result;
                    string responseBody = response.Content.ReadAsStringAsync().Result;
                    //NameValueCollection infoCollection = new NameValueCollection();
                    if (!string.IsNullOrEmpty(responseBody))
                    {
                        string[] data = responseBody.Split(":".ToCharArray());
                        for (int i = 0; i <= data.Length; i++)
                        {
                            if (i == 0)
                            {
                                this.MerchantReferenceCode = data[0].ToString();
                            }
                            if (i == 1)
                            {
                                this.ReasonCode = data[1].ToString();
                            }
                            if (i == 2)
                            {
                                this.Decision = data[2].ToString();
                            }
                        }
                    }



                }
                catch (Exception ex)
                {

                }
            }

        }

        //Country Saver Subscription charge
        public void CountrySaverSubcriptionCharge(string siteCode, string applicationCode,
           string accountId, string Last6DigitCC, string currency, double price, string cvv)
        {
            string productCode;
            if (siteCode == "MCM")
            {
                siteCode = "MCM";
            }
            else if (siteCode == "MFR")
            {
                siteCode = "FR2";
            }
            else if (siteCode == "BNL")
            {
                siteCode = "NL1";
            }
            else if (siteCode == "BAU")
            {
                siteCode = "AT1";
            }
            else if (siteCode == "BSE")
            {
                siteCode = "BSE";
            }
            else if (siteCode == "BDK")
            {
                siteCode = "VMDK";
            }
            else if (siteCode == "MFI")
            {
                siteCode = "VMFI";
            }
            else if (siteCode == "MPT")
            {
                siteCode = "PT1";
            }
            else
            {
                siteCode = "MCM";
            }
            productCode = "RCSB";
            var soapRequest = new StringBuilder(string.Format("?site-code={0}&", siteCode));
            soapRequest.AppendFormat("application-code={0}&", applicationCode);
            soapRequest.AppendFormat("product-code={0}&", productCode);
            soapRequest.AppendFormat("payment-agent={0}&", applicationCode);
            soapRequest.AppendFormat("service-type={0}&", ServiceType.RES);
            soapRequest.AppendFormat("payment-mode={0}&", PaymentMode.SUB);
            soapRequest.AppendFormat("account-id={0}&", accountId);
            soapRequest.AppendFormat("cc-no={0}&", Last6DigitCC);
            soapRequest.AppendFormat("cc-cvv={0}&", cvv);
            soapRequest.AppendFormat("amount={0:0.00}&", price);
            soapRequest.AppendFormat("currency={0}&", currency);
            soapRequest.Append("capture=1");

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(paymentUrl);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/plain"));
                //var content = new StringContent(soapRequest.ToString());
                //content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                try
                {


                    var response = client.GetAsync("/transaction.svc/subscription/authorize" + Uri.EscapeUriString(soapRequest.ToString())).Result;
                    string responseBody = response.Content.ReadAsStringAsync().Result;
                    //NameValueCollection infoCollection = new NameValueCollection();
                    if (!string.IsNullOrEmpty(responseBody))
                    {
                        string[] data = responseBody.Split(":".ToCharArray());
                        for (int i = 0; i <= data.Length; i++)
                        {
                            if (i == 0)
                            {
                                this.MerchantReferenceCode = data[0].ToString();
                            }
                            if (i == 1)
                            {
                                this.ReasonCode = data[1].ToString();
                            }
                            if (i == 2)
                            {
                                this.Decision = data[2].ToString();
                            }
                        }
                    }

                    //Testing Fixed Value

                    //this.MerchantReferenceCode = "VMUK-RCSB-830250";
                    //this.ReasonCode = "0";
                    //this.Decision = "Transcation Sucessed";


                }
                catch (Exception ex)
                {

                }
            }

        }

        public ErrCodeMsg ReactivateByCard(int reg_idx, string MerchantRef, int PaymentStatus, int UseForRenewal, string brand)
        {

            Log.Info("Start SP Reactivate CSB_HSB Saver By Card . Param : reg_idx = {0} | MerchantRef = {1} | PayStatus = {2}", reg_idx.ToString(), MerchantRef, PaymentStatus.ToString());
            try
            {
                ErrCodeMsg result = new ErrCodeMsg();
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    result = conn.Query<ErrCodeMsg>("crm_fsb_bundle_reactivation_bycc", new
                    {
                        @reg_idx = reg_idx,
                        @merchant_ref_code = MerchantRef,
                        @payment_status = PaymentStatus,
                        @use_for_renewal = UseForRenewal,
                        @brandtype = brand
                    }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    Log.Info("End SP Reactivate  CSB_HSB Saver By Card");
                    return result;
                }
            }
            catch (Exception e)
            {
                string str_err = string.Format("CSB_HSB.ReactivateByCard: {0}", e.Message);
                Log.Info(str_err);
                return new ErrCodeMsg() { errcode = -1, errmsg = e.ToString() };
            }
        }
        public GetProductAndCCVValues GetCardValues(string accountid)
        {
            GetProductAndCCVValues result = null;

            try
            {

                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    result = conn.Query<GetProductAndCCVValues>("Get_CVV_Detail_By_Account_Id", new
                   {
                       @account_id = accountid,

                   }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    return result;
                }
            }
            catch (Exception e)
            {
                string str_err = string.Format("CSB_HSB.GetCardValues: {0}", e.Message);
                Log.Info(str_err);
                return result;
            }
        }
        public IEnumerable<PaymentProfileCustomer> GetCardList(string mobileno, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<PaymentProfileCustomer>(
                            "crm3_get_mvno_customer_payment_profile", new
                            {
                                @msisdn = mobileno,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<PaymentProfileCustomer>();
                }
            }
            catch (Exception ex)
            {
                string str_err = string.Format("CSB_HSB.GetListProfile: {0}", ex.Message);
                Log.Info(str_err);
                return null;
            }
        }
        public ProductModel GetSitecodeByMobile(string brand)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<ProductModel>(
                            "crm_fsb_get_sitecode_by_brand", new
                            {
                                @brandtype = brand
                            },
                            commandType: CommandType.StoredProcedure);

                    return result.FirstOrDefault() ?? new ProductModel();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("CSB_HSB.GetProductByMobile: " + ex.Message);
            }
        }

        //Old Source for Payment - Before RealEx implementation
        //public void Authorize(CardDetails cardInfo, string siteCode, string applicationCode,
        //        string accountId, string emailAddress, string currency, double price, bool Bypass3Ds, string state, string IpAddr)
        //{

        //    string productCode;


        //    productCode = "VRN";

        //    if (siteCode == "MCM")
        //    {
        //        siteCode = "MCM";
        //    }
        //    else if (siteCode == "MFR")
        //    {
        //        siteCode = "FR2";
        //    }
        //    else if (siteCode == "BNL")
        //    {
        //        siteCode = "NL1";
        //    }
        //    else if (siteCode == "BAU")
        //    {
        //        siteCode = "AT1";
        //    }
        //    else if (siteCode == "BSE")
        //    {
        //        siteCode = "BSE";
        //    }
        //    else if (siteCode == "BDK")
        //    {
        //        siteCode = "VMDK";
        //    }
        //    else if (siteCode == "MFI")
        //    {
        //        siteCode = "VMFI";
        //    }
        //    else if (siteCode == "MPT")
        //    {
        //        siteCode = "PT1";
        //    }
        //    else
        //    {
        //        siteCode = "MCM";
        //    }



        //    var soapRequest = new StringBuilder(string.Format("site-code={0}", siteCode)).AppendLine();
        //    soapRequest.AppendFormat("application-code={0}", applicationCode).AppendLine();
        //    soapRequest.AppendFormat("product-code={0}", productCode).AppendLine();
        //    soapRequest.AppendFormat("payment-agent={0}", applicationCode).AppendLine();
        //    soapRequest.AppendFormat("service-type={0}", ServiceType.RES).AppendLine();
        //    soapRequest.AppendFormat("payment-mode={0}", PaymentMode.DEF).AppendLine();
        //    soapRequest.AppendFormat("account-id={0}", accountId).AppendLine();
        //    soapRequest.AppendFormat("cc-no={0}", cardInfo.CardNumber).AppendLine();
        //    soapRequest.AppendFormat("cc-cvv={0}", cardInfo.CardCVV).AppendLine();
        //    soapRequest.AppendFormat("exp-date={0}{1}", cardInfo.CardExpireYear, cardInfo.CardExpireMonth).AppendLine();
        //    soapRequest.AppendFormat("first-name={0}", cardInfo.CardFirstName).AppendLine();
        //    soapRequest.AppendFormat("last-name={0}", cardInfo.CardLastName).AppendLine();
        //    soapRequest.AppendFormat("street-name={0}", cardInfo.CardAddress).AppendLine();
        //    soapRequest.AppendFormat("city={0}", cardInfo.CardCity).AppendLine();
        //    soapRequest.AppendFormat("post-code={0}", cardInfo.CardPostCode).AppendLine();
        //    soapRequest.AppendFormat("country={0}", cardInfo.CardCountry).AppendLine();
        //    soapRequest.AppendFormat("email={0}", emailAddress).AppendLine();
        //    soapRequest.AppendFormat("amount={0:0.00}", price).AppendLine();
        //    soapRequest.AppendFormat("client-ip={0}", IpAddr).AppendLine();
        //    soapRequest.AppendFormat("currency={0}", currency).AppendLine();
        //    soapRequest.AppendFormat("check-enroll={0}", Bypass3Ds == false ? "1" : "0").AppendLine();
        //    soapRequest.Append("capture=1").AppendLine();
        //    if (siteCode == "US1" || siteCode == "CA1" || siteCode == "ZZ1" /*rest of the world*/)
        //    {
        //        soapRequest.AppendFormat("state={0}", state).AppendLine();
        //    }

        //    using (HttpClient client = new HttpClient())
        //    {
        //        client.BaseAddress = new Uri(paymentUrl);

        //        var content = new StringContent(soapRequest.ToString());
        //        content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");

        //        try
        //        {
        //            Log.Debug(string.Format("Payment.Authorize: Request={0}", soapRequest));

        //            var response = client.PostAsync("/transaction.svc/enrollment/authorize", content).Result;
        //            string responseBody = response.Content.ReadAsStringAsync().Result;
        //            NameValueCollection infoCollection = new NameValueCollection();
        //            if (!string.IsNullOrEmpty(responseBody))
        //            {
        //                string[] data = responseBody.Split("\r\n".ToCharArray());
        //                for (int i = 0; i < data.Length; i++)
        //                {
        //                    string[] chunk;
        //                    if (i == 0)
        //                    {
        //                        chunk = data[i].Split(":".ToCharArray(), 3);
        //                        infoCollection.Add("Description", chunk.Length == 3 ? chunk[2] : "Invalid data");
        //                    }
        //                    else
        //                    {
        //                        chunk = data[i].Split("=:".ToCharArray(), 2);
        //                        if (chunk.Length == 2)
        //                            infoCollection.Add(chunk[0], chunk[1]);
        //                    }
        //                }
        //            }

        //            this.MerchantReferenceCode = infoCollection["MerchantReferenceCode"] ?? string.Empty;
        //            this.ReasonCode = infoCollection["ReasonCode"] ?? "-1";
        //            this.Description = infoCollection["Description"] ?? "Internal Error";
        //            this.Decision = (infoCollection["Decision"] ?? "ERROR").ToUpper();
        //            this.AcsURL = infoCollection["AcsURL"] ?? string.Empty;
        //            this.PaReq = infoCollection["PaReq"] ?? string.Empty;
        //            this.SubscriptionId = infoCollection["SubscriptionId"] ?? string.Empty;

        //            Log.Debug(string.Format("Payment.Authorize: Response={0}", responseBody));
        //        }
        //        catch (Exception ex)
        //        {
        //            Log.Error(string.Format("Payment.Authorize:{0}", ex.Message));
        //        }
        //    }

        //}

        public void Authorize(CardDetails cardInfo, string siteCode, string applicationCode,
               string accountId, string emailAddress, string currency, double price, bool Bypass3Ds, string state, string IpAddr)
        {

            string productCode;


            //productCode = "VRN";
            //productCode = string.Format("VRN{0:00}", price);

            if (state == "countrysaver")
            {
                productCode = string.Format("CSB{0:00}", price);
            }
            else
            {
                productCode = string.Format("VRN{0:00}", price);
            }
            


            if (siteCode == "MCM")
            {
                siteCode = "MCM";
            }
            else if (siteCode == "MFR")
            {
                siteCode = "FR2";
            }
            else if (siteCode == "BNL")
            {
                siteCode = "NL1";
            }
            else if (siteCode == "BAU")
            {
                siteCode = "AT1";
            }
            else if (siteCode == "BSE")
            {
                siteCode = "BSE";
            }
            else if (siteCode == "BDK")
            {
                siteCode = "VMDK";
            }
            else if (siteCode == "MFI")
            {
                siteCode = "VMFI";
            }
            else if (siteCode == "MPT")
            {
                siteCode = "PT1";
            }
            else
            {
                siteCode = "MCM";
            }


            if (cardInfo.CardCountry == "United Kingdom")
            {
                cardInfo.CardCountry = "UK";
                currency = "GBP";
            }
            else if (cardInfo.CardCountry == "Austria")
            {
                cardInfo.CardCountry = "AT";
                currency = "EUR";
            }
            else if (cardInfo.CardCountry == "Belgium")
            {
                cardInfo.CardCountry = "BE";
                currency = "EUR";
            }
            else if (cardInfo.CardCountry == "Denmark")
            {
                cardInfo.CardCountry = "DK";
                currency = "DKK";
            }
            else if (cardInfo.CardCountry == "Finland")
            {
                cardInfo.CardCountry = "FI";
                currency = "EUR";
            }
            else if (cardInfo.CardCountry == "France")
            {
                cardInfo.CardCountry = "FR";
                currency = "EUR";
            }
            else if (cardInfo.CardCountry == "Netherlands")
            {
                cardInfo.CardCountry = "NL";
                currency = "EUR";
            }
            else if (cardInfo.CardCountry == "Poland")
            {
                cardInfo.CardCountry = "PL";
                currency = "PLN";
            }
            else if (cardInfo.CardCountry == "Portugal")
            {
                cardInfo.CardCountry = "PT";
                currency = "EUR";
            }
            else if (cardInfo.CardCountry == "Sweden")
            {
                cardInfo.CardCountry = "SE";
                currency = "SEK";
            }

            var soapRequest = new StringBuilder(string.Format("site-code={0}", siteCode)).AppendLine();
            soapRequest.AppendFormat("application-code={0}", applicationCode).AppendLine();
            soapRequest.AppendFormat("product-code={0}", productCode).AppendLine();
            soapRequest.Append("payment-agent=CRM3").AppendLine();
            soapRequest.Append("service-type=MVNO").AppendLine();
            soapRequest.Append("payment-mode=DEF").AppendLine();
            //soapRequest.AppendFormat("payment-agent={0}", applicationCode).AppendLine();
            //soapRequest.AppendFormat("service-type={0}", ServiceType.RES).AppendLine();
            //soapRequest.AppendFormat("payment-mode={0}", PaymentMode.DEF).AppendLine();
            soapRequest.AppendFormat("account-id={0}", accountId).AppendLine();
            soapRequest.AppendFormat("cc-no={0}", cardInfo.CardNumber).AppendLine();
            soapRequest.AppendFormat("cc-cvv={0}", cardInfo.CardCVV).AppendLine();
            soapRequest.AppendFormat("exp-date={0}{1}", cardInfo.CardExpireYear, cardInfo.CardExpireMonth).AppendLine();
            soapRequest.AppendFormat("first-name={0}", cardInfo.CardFirstName).AppendLine();
            soapRequest.AppendFormat("last-name={0}", cardInfo.CardLastName).AppendLine();
            soapRequest.AppendFormat("street-name={0}", cardInfo.CardAddress).AppendLine();
            soapRequest.AppendFormat("city={0}", cardInfo.CardCity).AppendLine();
            soapRequest.AppendFormat("post-code={0}", cardInfo.CardPostCode).AppendLine();
            soapRequest.AppendFormat("country={0}", cardInfo.CardCountry).AppendLine();
            soapRequest.AppendFormat("email={0}", emailAddress).AppendLine();
            soapRequest.AppendFormat("amount={0:0.00}", price).AppendLine();
            soapRequest.AppendFormat("client-ip={0}", IpAddr).AppendLine();
            soapRequest.AppendFormat("currency={0}", currency).AppendLine();
            soapRequest.AppendFormat("check-enroll={0}", Bypass3Ds == false ? "1" : "0").AppendLine();
            soapRequest.Append("capture=1").AppendLine();
            soapRequest.AppendFormat("cc-type={0}", cardInfo.CardType).AppendLine();

            if (siteCode == "US1" || siteCode == "CA1" || siteCode == "ZZ1" /*rest of the world*/)
            {
                soapRequest.AppendFormat("state={0}", state).AppendLine();
            }

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(paymentUrl);

                var content = new StringContent(soapRequest.ToString());
                content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");

                try
                {
                    Log.Debug(string.Format("Payment.Authorize: Request={0}", soapRequest));

                    var response = client.PostAsync("/transaction.svc/enrollment/authorize", content).Result;
                    string responseBody = response.Content.ReadAsStringAsync().Result;
                    NameValueCollection infoCollection = new NameValueCollection();
                    if (!string.IsNullOrEmpty(responseBody))
                    {
                        string[] data = responseBody.Split("\r\n".ToCharArray());
                        for (int i = 0; i < data.Length; i++)
                        {
                            string[] chunk;
                            if (i == 0)
                            {
                                chunk = data[i].Split(":".ToCharArray(), 3);
                                infoCollection.Add("Description", chunk.Length == 3 ? chunk[2] : "Invalid data");
                            }
                            else
                            {
                                chunk = data[i].Split("=:".ToCharArray(), 2);
                                if (chunk.Length == 2)
                                    infoCollection.Add(chunk[0], chunk[1]);
                            }
                        }
                    }

                    this.MerchantReferenceCode = infoCollection["MerchantReferenceCode"] ?? string.Empty;
                    this.ReasonCode = infoCollection["ReasonCode"] ?? "-1";
                    this.Description = infoCollection["Description"] ?? "Internal Error";
                    this.Decision = (infoCollection["Decision"] ?? "ERROR").ToUpper();
                    this.AcsURL = infoCollection["AcsURL"] ?? string.Empty;
                    this.PaReq = infoCollection["PaReq"] ?? string.Empty;
                    this.SubscriptionId = infoCollection["SubscriptionId"] ?? string.Empty;

                    int pay_status = 1;
                    if (Decision.ToUpper().Contains("SUCEEDED"))
                    {
                        pay_status = 0;
                    }
                    else
                    {
                        pay_status = 1;
                    }
                    Log.Debug(string.Format("Payment.Authorize: Response={0}", responseBody));

                    //To Insert Payment Status in Payment DB. If success, pay_status = 0 otherwise 1
                    using (var conn = new SqlConnection(Helpers.Config.PaymentConnection))
                    {
                        conn.Open();

                        var p = new DynamicParameters();
                        p.Add("@mobileno", accountId, DbType.String, ParameterDirection.Input, 20);
                        p.Add("@reference_id", this.MerchantReferenceCode ?? string.Empty, DbType.String, ParameterDirection.Input, 50);
                        p.Add("@pay_status", pay_status, DbType.Int32, ParameterDirection.Input, 100);
                        conn.Query<ErrMsg>("crm_llom_payment_trans_insert",p, null, true, null, CommandType.StoredProcedure).FirstOrDefault();
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(string.Format("Payment.Authorize:{0}", ex.Message));
                }
            }

        }


        public bool RegisterWithPayment(FamilySaverRegistration data, out ErrMsg errMessage)
        {
            Log.Info("Enter SP Register With Payment");
            //Log.Info(new JavaScriptSerializer().Serialize(data)); Log.Info("Start PayWithCreditAfter3Ds:");
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var p = new DynamicParameters();
                    p.Add("@login", data.Login, DbType.String, ParameterDirection.Input, 20);
                    p.Add("@main", data.Main.Number ?? string.Empty, DbType.String, ParameterDirection.Input, 20);
                    p.Add("@number1", data.Number1.Number ?? string.Empty, DbType.String, ParameterDirection.Input, 20);
                    p.Add("@number2", data.Number2.Number ?? string.Empty, DbType.String, ParameterDirection.Input, 20);
                    p.Add("@number3", data.Number3.Number ?? string.Empty, DbType.String, ParameterDirection.Input, 20);
                    p.Add("@clitypemain", data.Main.Type, DbType.Int32, ParameterDirection.Input, 100);
                    p.Add("@clitype1", data.Number1.Type, DbType.Int32, ParameterDirection.Input);
                    p.Add("@clitype2", data.Number2.Type, DbType.Int32, ParameterDirection.Input, 50);
                    p.Add("@clitype3", data.Number3.Type, DbType.Int32, ParameterDirection.Input, 50);
                    p.Add("@dest_number", data.FamilyNumber.Number, DbType.String, ParameterDirection.Input, 32);
                    p.Add("@sourcereg", data.SourceReg, DbType.String, ParameterDirection.Input, 30);
                    p.Add("@firstname", data.FamilyNumber.FirstName, DbType.String, ParameterDirection.Input, 30);
                    p.Add("@lastname", data.FamilyNumber.LastName, DbType.String, ParameterDirection.Input, 30);
                    p.Add("@name", data.FamilyNumber.Name, DbType.String, ParameterDirection.Input, 30);
                    p.Add("@postcode", data.FamilyNumber.PostCode, DbType.String, ParameterDirection.Input, 50);
                    p.Add("@street", data.FamilyNumber.Street, DbType.String, ParameterDirection.Input, 50);
                    p.Add("@town", data.FamilyNumber.Town, DbType.String, ParameterDirection.Input, 50);
                    p.Add("@country", data.FamilyNumber.Country, DbType.String, ParameterDirection.Input, 50);
                    p.Add("@amount", data.Amount, DbType.Double, ParameterDirection.Input);
                    p.Add("@paymode", data.PayMode, DbType.Int32, ParameterDirection.Input);
                    p.Add("@payref", data.PayRef, DbType.String, ParameterDirection.Input, 50);
                    p.Add("@historyId", data.HistoryId, DbType.Int32, ParameterDirection.Input);
                    p.Add("@productId", data.ProductId, DbType.String, ParameterDirection.Input, 100);
                    errMessage = conn.Query<ErrMsg>("fsb_ctp_registration_withpayment", p, null, true, null, CommandType.StoredProcedure).FirstOrDefault();
                    //Log.Info(new JavaScriptSerializer().Serialize(errMessage));
                    if (errMessage.ErrorCode <= 0)
                        return false;
                    else
                        return true;
                }
            }
            catch (Exception e)
            {
                string str_err = string.Format("vm.vogo.Areas.CTP.DB.HomeSaver.RegisterWithPayment: {0}", e.Message);
                Log.Debug(str_err);
                throw new Exception(str_err);
            }
        }
        public int GetCliType(string msisdn, string sitecode)
        {
            if (msisdn == "0" || string.IsNullOrEmpty(msisdn))
            {
                msisdn = "000";
            }
            //msisdn = string.IsNullOrEmpty(msisdn) ? "000" : msisdn;

            if (sitecode == "LO2")
            {
                if (msisdn.Length >= 1 && msisdn.Substring(0, 3).Equals("447")) return 2;
                if (msisdn.Length >= 1 && msisdn.Substring(0, 3).Equals("441")) return 1;
                if (msisdn.Length >= 1 && msisdn.Substring(0, 3).Equals("442")) return 1;
            }

            else
            {
                return 2;
            }

            return 0;
        }

        public ErrCodeMsg PayByBalnce(string regid, string brandtype)
        {
            ErrCodeMsg result = new ErrCodeMsg();
            //using (var conn = new SqlConnection(crm_connStr))
            using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
            {
                conn.Open();
                var p = new DynamicParameters();
                p.Add("@regidx", regid, DbType.Int16, direction: ParameterDirection.Input);
                p.Add("@brandtype", brandtype, DbType.String, direction: ParameterDirection.Input, size: 10);
                p.Add("@errorcode", dbType: DbType.Int32, direction: ParameterDirection.Output);
                p.Add("@errormsg", dbType: DbType.String, direction: ParameterDirection.Output, size: 250);
                try
                {
                    /*
                    result = conn.Query<ErrCodeMsg>("crm_fsb_bundle_reactivation", new
                         {
                             @regidx = regid,
                             @brandtype = brandtype,

                         }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                     */


                    conn.Query("crm_fsb_bundle_reactivation", p, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    int errorCode = p.Get<int>("@errorcode");
                    String errorMsg = p.Get<String>("@errormsg");
                    result.errcode = errorCode;
                    result.errmsg = errorMsg;
                    Log.Info("End SP Reactivate  CSB_HSB Saver By Balance");
                    return result;
                }

                catch (Exception e)
                {
                    string str_err = string.Format("CSB_HSB.ReactivateByCard: {0}", e.Message);
                    Log.Info(str_err);
                    return new ErrCodeMsg() { errcode = -1, errmsg = e.ToString() };
                }
            }
        }


        public ErrCodeMsg PayByBalnceNewCLI(string sitecode, string mobileno, double amount)
        {
            ErrCodeMsg result = new ErrCodeMsg();
            //using (var conn = new SqlConnection(crm_connStr))
            using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
            {
                conn.Open();
                var p = new DynamicParameters();
                p.Add("@sitecode", sitecode, DbType.Int16, direction: ParameterDirection.Input);
                p.Add("@mobileno", mobileno, DbType.String, direction: ParameterDirection.Input, size: 10);
                p.Add("@amount", amount, DbType.Double, direction: ParameterDirection.Input);
                try
                {
                    /*
                    result = conn.Query<ErrCodeMsg>("crm_fsb_bundle_reactivation", new
                         {
                             @regidx = regid,
                             @brandtype = brandtype,

                         }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                     */


                    conn.Query("add_cli_debit_balance", p, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    int errorCode = p.Get<int>("@errorcode");
                    String errorMsg = p.Get<String>("@errormsg");
                    result.errcode = errorCode;
                    result.errmsg = errorMsg;
                    Log.Info("End SP PayByBalnceNewCLI  CSB_HSB Saver By Balance");
                    return result;
                }

                catch (Exception e)
                {
                    string str_err = string.Format("CSB_HSB.PayByBalnceNewCLI: {0}", e.Message);
                    Log.Info(str_err);
                    return new ErrCodeMsg() { errcode = -1, errmsg = e.ToString() };
                }
            }
        }

        public BundleModel GetBundleDescription(string regid, string brandtype)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var result = conn.Query<BundleModel>("crm_fsb_bundle_description", new
                    {
                        @regidx = regid,
                        @brandtype = brandtype

                    }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    return result;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.ToString());
            }
        }

        public IEnumerable<Models.CountryInfo> Get2in1CountriesByProductCode(string product_code)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Models.CountryInfo>(
                            "crm3_2in1_newcountries_list", new
                            {
                                @product_code = product_code
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<Models.CountryInfo>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL Get2in1CountriesByProductCode: " + ex.Message);
            }
        }


        public List<NewSubscription> GetOperatorDetails(string __SubscriptionId, string __FFNumber)
        {
            List<NewSubscription> result = new List<NewSubscription>();

            using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
            {
                conn.Open();
                string sql = @"COMMON_SELECT_S1";
                result = conn.Query<NewSubscription>(sql, new
                {
                    @ParamID = 0,
                    @ParamString = __SubscriptionId,
                    @ActionType = 4,
                    @FandF_Number = __FFNumber

                }, commandType: CommandType.StoredProcedure).ToList();

                return result;
            }
        }

        /*Added on 25 Feb 2015 - by Hari - to get subscription details using subscription id*/
        public List<NewSubscription> GetOperatorDetails(string __SubscriptionId)
        {
            List<NewSubscription> result = new List<NewSubscription>();

            using (var conn = new SqlConnection(Helpers.Config.PortalConnection))
            {
                conn.Open();
                string sql = @"GetAcountDetails_from_REG_ID";
                result = conn.Query<NewSubscription>(sql, new
                {
                    @RegID = __SubscriptionId

                }, commandType: CommandType.StoredProcedure).ToList();

                return result;
            }
        }
        /*end added*/

        public string GetWebClient(String destUri, String xmlFilePath, List<String> paramValues)
        {
            WebClient client = new WebClient();
            client.Headers.Add("content-type", "text/xml");
            System.IO.StreamReader reader = new System.IO.StreamReader(xmlFilePath);
            object[] args = paramValues.Cast<object>().ToArray();
            string data = String.Format(reader.ReadToEnd(), args);
            reader.Close();
            string response = client.UploadString(destUri, data);
            return response;
        }

        //Add CLI
        //public Mvc.Payments.ErrCodeMsg AddCLI(SubscriberVectoneCLIViewModel cliViewModel, String accountId, String soapFormatFilePath)
        //{
        //    try
        //    {
        //        Log.Info("Start CSB_HSB.AddCLI {0}", cliViewModel.CLI);
        //        Mvc.Payments.ErrCodeMsg errCodeMsg = new Mvc.Payments.ErrCodeMsg();
        //        String status = "";
        //        using (WebClient client = new WebClient())
        //        {
        //            /*
        //            client.Headers.Add("Cache-Control", "no-cache");
        //            client.Headers.Add("User-Agent", "FREEDOM Service 1.0");
        //            NameValueCollection fields = new NameValueCollection();
        //            fields.Add("ACCOUNT_NUMBER", accountId);
        //            fields.Add("CLI_NO", cliViewModel.CLI);
        //            fields.Add("CLI_TYPE", cliViewModel.Type);                    
        //            */
        //            String newCLIAddUrl = null;
        //            String newCLIAddxmlns = null;
        //            /*
        //            if (cliViewModel.CLI.Substring(0,4).CompareTo("0063")==0 || cliViewModel.CLI.Substring(0,2).CompareTo("63")==0)
        //            {
        //                newCLIAddUrl = ConfigurationSettings.AppSettings["NewCLIUnlimitedAddUrl"].ToString();                        
        //            }
        //            else if (cliViewModel.CLI.Substring(0, 4).CompareTo("0094") == 0 || cliViewModel.CLI.Substring(0, 2).CompareTo("94") == 0)
        //            {
        //                newCLIAddUrl = ConfigurationSettings.AppSettings["NewCLIFreedomAddUrl"].ToString();
        //            }
        //            */

        //            if (accountId.Substring(0, 3).CompareTo("200") == 0)
        //            {
        //                newCLIAddUrl = ConfigurationSettings.AppSettings["NewCLIFreedomAddUrl"].ToString();
        //                newCLIAddxmlns = ConfigurationSettings.AppSettings["NewCLIFreedomAddxmlns"].ToString();
        //            }
        //            else if (accountId.Substring(0, 2).CompareTo("55") == 0)
        //            {
        //                newCLIAddUrl = ConfigurationSettings.AppSettings["NewCLIUnlimitedAddUrl"].ToString();
        //                newCLIAddxmlns = ConfigurationSettings.AppSettings["NewCLIUnlimitedAddxmlns"].ToString();
        //            }

        //            List<String> fieldvalues = new List<String>();
        //            fieldvalues.Add(newCLIAddxmlns);
        //            fieldvalues.Add(accountId);
        //            fieldvalues.Add(cliViewModel.CLI);
        //            fieldvalues.Add(cliViewModel.Type);
        //            if (!(String.IsNullOrEmpty(newCLIAddUrl)))
        //            {
        //                String resp = GetWebClient(newCLIAddUrl, soapFormatFilePath, fieldvalues);
        //                String XmlString = resp.Replace("&lt;", "<");
        //                XmlString = XmlString.Replace("&gt;", ">");
        //                System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
        //                xmlDoc.LoadXml(XmlString);
        //                System.Xml.XmlNodeList elemList = xmlDoc.GetElementsByTagName("AddCLIResult");
        //                //System.Xml.XmlNode xmlNode = xmlDoc.SelectSingleNode("string");
        //                System.Xml.XmlNode xmlNode = elemList.Item(0);


        //                if (xmlNode != null)
        //                    status = xmlNode.InnerText;
        //            }
        //            else
        //            {
        //                status = "Please enter a valid number!!";
        //            }
        //            /*
        //            FREEDOM.FREEDOMSoapClient serv;
        //            serv = new FREEDOM.FREEDOMSoapClient();
        //            serv.AddCLI(accountId,cliViewModel.CLI,cliViewModel.Type);
        //            */
        //            /*if (FREEDOM.FREEDOMSoa)
        //             */

        //            /*
        //            if (!(String.IsNullOrEmpty(newCLIAddUrl)))
        //            {
        //                byte[] respBytes = client.UploadValues(newCLIAddUrl, fields);
        //                string resp = client.Encoding.GetString(respBytes);
        //                String XmlString = resp.Replace("&lt;", "<");
        //                XmlString = XmlString.Replace("&gt;", ">");
        //                System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
        //                xmlDoc.LoadXml(XmlString);
        //                System.Xml.XmlNode xmlNode = xmlDoc.SelectSingleNode("string");

        //                if (xmlNode != null)
        //                    status = xmlNode.InnerText;
        //            }
        //            else
        //            {
        //                status = "Please enter a valid number!!";
        //            }
        //            */
        //        }
        //        if (String.Compare(status, "Success", true) == 0)
        //        {
        //            errCodeMsg.ErrCode = 0;
        //            errCodeMsg.ErrMsg = "Success";
        //        }
        //        else
        //        {
        //            errCodeMsg.ErrCode = -1;
        //            errCodeMsg.ErrMsg = status;
        //        }
        //        return errCodeMsg;
        //    }
        //    catch (Exception e)
        //    {
        //        string str_err = string.Format("CSB_HSB.AddCLI: {0}", e.Message);
        //        Log.Info(str_err);
        //        return new ErrCodeMsg() { ErrCode = -1, ErrMsg = e.ToString() };
        //    }
        //}

        //Delete CLI
        //public Mvc.Payments.ErrCodeMsg DeleteCLI(SubscriberVectoneCLIViewModel cliViewModel, String accountId, String soapFormatFilePath)
        //{
        //    try
        //    {
        //        Log.Info("Start CSB_HSB.DeleteCLI {0} {1}", cliViewModel.CLI, cliViewModel.Type);
        //        Mvc.Payments.ErrCodeMsg errCodeMsg = new Mvc.Payments.ErrCodeMsg();
        //        String status = "";
        //        using (WebClient client = new WebClient())
        //        {
        //            //NameValueCollection fields = new NameValueCollection();
        //            //fields.Add("ACCOUNT_NUMBER", accountId);
        //            //fields.Add("CLI_NO", cliViewModel.CLI);
        //            String newCLIDeleteUrl = null;
        //            String newCLIDeletexmlns = null;
        //            /*
        //            if (cliViewModel.CLI.Substring(0, 4).CompareTo("0063") == 0 || cliViewModel.CLI.Substring(0, 2).CompareTo("63") == 0)
        //            {
        //                newCLIDeleteUrl = ConfigurationSettings.AppSettings["NewCLIFreedomDeleteUrl"].ToString();
        //                newCLIDeletexmlns = ConfigurationSettings.AppSettings["NewCLIFreedomAddxmlns"].ToString();
        //            }
        //            else if (cliViewModel.CLI.Substring(0, 4).CompareTo("0094") == 0 || cliViewModel.CLI.Substring(0, 2).CompareTo("94") == 0)
        //            {
        //                newCLIDeleteUrl = ConfigurationSettings.AppSettings["NewCLIUnlimitedDeleteUrl"].ToString();
        //                newCLIDeletexmlns = ConfigurationSettings.AppSettings["NewCLIUnlimitedAddxmlns"].ToString();
        //            }
        //             */
        //            if (accountId.Substring(0, 3).CompareTo("200") == 0)
        //            {
        //                newCLIDeleteUrl = ConfigurationSettings.AppSettings["NewCLIFreedomDeleteUrl"].ToString();
        //                newCLIDeletexmlns = ConfigurationSettings.AppSettings["NewCLIFreedomDeletexmlns"].ToString();
        //            }
        //            else if (accountId.Substring(0, 2).CompareTo("55") == 0)
        //            {
        //                newCLIDeleteUrl = ConfigurationSettings.AppSettings["NewCLIUnlimitedDeleteUrl"].ToString();
        //                newCLIDeletexmlns = ConfigurationSettings.AppSettings["NewCLIUnlimitedDeletexmlns"].ToString();
        //            }

        //            if (!(String.IsNullOrEmpty(newCLIDeleteUrl)))
        //            {
        //                //byte[] respBytes = client.UploadValues(newCLIDeleteUrl, fields);
        //                //string resp = client.Encoding.GetString(respBytes);
        //                List<String> fieldvalues = new List<String>();
        //                fieldvalues.Add(newCLIDeletexmlns);
        //                fieldvalues.Add(accountId);
        //                fieldvalues.Add(cliViewModel.CLI);
        //                String resp = GetWebClient(newCLIDeleteUrl, soapFormatFilePath, fieldvalues);
        //                String XmlString = resp.Replace("&lt;", "<");
        //                XmlString = XmlString.Replace("&gt;", ">");
        //                System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
        //                xmlDoc.LoadXml(XmlString);
        //                System.Xml.XmlNodeList elemList = xmlDoc.GetElementsByTagName("DeleteCLIResult");
        //                //System.Xml.XmlNode xmlNode = xmlDoc.SelectSingleNode("string");
        //                System.Xml.XmlNode xmlNode = elemList.Item(0);
        //                if (xmlNode != null)
        //                    status = xmlNode.InnerText;
        //            }
        //            else
        //            {
        //                status = "Please enter a valid number!!";
        //            }


        //        }
        //        if (String.Compare(status, "Success", true) == 0)
        //        {
        //            errCodeMsg.ErrCode = 0;
        //            errCodeMsg.ErrMsg = "Success";
        //        }
        //        else
        //        {
        //            errCodeMsg.ErrCode = -1;
        //            errCodeMsg.ErrMsg = status;
        //        }
        //        return errCodeMsg;
        //    }
        //    catch (Exception e)
        //    {
        //        string str_err = string.Format("CSB_HSB.DeleteCLI: {0}", e.Message);
        //        Log.Info(str_err);
        //        return new ErrCodeMsg() { ErrCode = -1, ErrMsg = e.ToString() };
        //    }

        //}


        //public Boolean ValidateCLI(string SubscriberId, string FFNumber, string CLI, String CLINumber)
        //{
        //    Log.Info("Start CSB_HSB.ValidateCLI: {0}", CLINumber);
        //    using (var conn = new SqlConnection(action))
        //    {
        //        conn.Open();
        //        string sql = @"CHECK_CLI_EXISTS";
        //        var result = conn.Query<ErrCodeMsgCLIExist>(sql, new
        //        {
        //            @CLI = CLINumber

        //        }, commandType: CommandType.StoredProcedure).ToList().FirstOrDefault();


        //        if (result.ErrCode == 0)
        //        {
        //            //if (String.Compare(result.cli_country, "OTHER MOBILE") == 0)
        //            //{
        //            //    return false;
        //            //}
        //            //else
        //            //{ 
        //            return true;
        //            //}
        //        }
        //        else
        //        { return false; }
        //    }
        //    Log.Info("End CSB_HSB.ValidateCLI: {0}", CLINumber);
        //}

        //public NewCLIPaymentModel GetNewCLIAmount(string brand)
        //{
        //    try
        //    {
        //        using (var conn = new SqlConnection(action))
        //        {
        //            conn.Open();

        //            var result = conn.Query<NewCLIPaymentModel>(
        //                    "crm_fsb_get_sitecode_byaddcli", new
        //                    {
        //                        @brandtype = brand
        //                    },
        //                    commandType: CommandType.StoredProcedure);

        //            return result.FirstOrDefault() ?? new NewCLIPaymentModel();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception("CSB_HSB.GetNewCLIAmount: " + ex.Message);
        //    }
        //}

        //Validate CLI
        /*
        public Boolean ValidateCLI(string SubscriberId, string FFNumber, string CLI)
        {
            return true;
        }
        */

        //List CLIs
        //use the existing procedure

        public bool ValidateCLIPrefix(string SubscriberId, string FFNumber, string CLI, string Brand, string CLINumber)
        {
            if (String.Compare(Brand.Substring(0, 3), "CTP") == 0 && CLI.Length == CLINumber.Length)
            {
                return true;
            }
            if (String.Compare(CLI.Substring(0, 2), CLINumber.Substring(0, 2)) == 0 && CLI.Length == CLINumber.Length)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //validation  before pay by new and exit card
        public CRM_API.Models.Common.ErrCodeMsg validation(string mobileno, string sitecode, string p3, int mode)
        {
            try
            {
                string[] cancelValues = p3.Split(',');
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.Common.ErrCodeMsg>(
                        "crm3_LLOTG_check_assigned_no", new
                        {
                            @mobileno = mobileno,
                            @reg_number = cancelValues[0],
                        }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    return result ?? new CRM_API.Models.Common.ErrCodeMsg();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.LLOTG.SProc.Reactivate: " + ex.Message);
                throw new Exception("CRM_API.DB.LLOTG.SProc.Reactivate: " + ex.Message);
            }
        }

    }


    public class SProc
    {

        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        /// <summary>
        /// GetCollectionAsync
        /// </summary>
        /// <param name="mobileno"></param>
        /// <param name="sitecode"></param>
        /// <returns></returns>
        public static async Task<IEnumerable<CRM_API.Models.LLOTG>> GetCollectionAsync(string mobileno, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result =
                        Task.Run(() =>
                        {
                            return conn.Query<CRM_API.Models.LLOTG>(
                                "Crm3_Get_LLOTG_Details_by_Mobileno", new
                                {
                                    @mobileno = mobileno,
                                    @sitecode = sitecode
                                }, commandType: CommandType.StoredProcedure);
                        }
                    );
                    return await result ?? new List<CRM_API.Models.LLOTG>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.LLOTG.SProc.GetCollectionAsync: " + ex.Message);
                throw new Exception("CRM_API.DB.LLOTG.SProc.GetCollectionAsync: " + ex.Message);
            }
        }
        public static async Task<CRM_API.Models.LLOTG> GetDetailAsync(string reg_number, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result =
                        Task.Run(() =>
                        {
                            return conn.Query<CRM_API.Models.LLOTG>(
                                "crm3_LLOTG_list_detail", new
                                {
                                    @reg_number = reg_number,
                                    @sitecode = sitecode
                                }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                        }
                    );
                    return await result ?? new CRM_API.Models.LLOTG();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.LLOTG.SProc.GetDetailAsync: " + ex.Message);
                throw new Exception("CRM_API.DB.LLOTG.SProc.GetDetailAsync: " + ex.Message);
            }
        }
        public static async Task<IEnumerable<CRM_API.Models.PlanPackages>> getLLOTGPlans(string country, string paytype, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result =
                        Task.Run(() =>
                        {
                            return conn.Query<CRM_API.Models.PlanPackages>(
                                "Get_Plan_List_LLOTG", new
                                {
                                    @Country = country,
                                    @pay_mode = paytype,
                                    @sitecode = sitecode
                                }, commandType: CommandType.StoredProcedure);
                        }
                    );
                    return await result ?? new List<CRM_API.Models.PlanPackages>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.LLOTG.SProc.getLLOTGPlans: " + ex.Message);
                throw new Exception("CRM_API.DB.LLOTG.SProc.getLLOTGPlans: " + ex.Message);
            }
        }
        public static SubscriberPersonalDetail GetSubscriberPersonalDetail(String subscriberId, String FFNumber, String CLI, int Action)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {

                    return conn.Query<CRM_API.Models.SubscriberPersonalDetail>(
                        "REGISTRATIONS_SELECT_VIEWDETAILS", new
                        {
                            @ParamID = subscriberId,
                            @ParamString = FFNumber,
                            @ActionType = Action,
                            @CLI_Number = CLI
                        }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.LLOTG.SProc.GetSubscriberPersonalDetail: " + ex.Message);
                throw new Exception("CRM_API.DB.LLOTG.SProc.GetSubscriberPersonalDetail: " + ex.Message);
            }

        }


        /// <summary>
        /// GetViewSubscriptionHistory
        /// </summary>
        /// <param name="mobileno"></param>
        /// <param name="sitecode"></param>
        /// <returns></returns>
        public static async Task<IEnumerable<CRM_API.Models.ViewSubscriptionHistory>> GetViewSubscriptionHistory(string mobileno, string Reg_Number, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result =
                        Task.Run(() =>
                        {
                            return conn.Query<CRM_API.Models.ViewSubscriptionHistory>(
                                "Subscription_History_LLOM_v1", new
                                {
                                    @mobileno = mobileno,
                                    @reg_number = Reg_Number,
                                    @sitecode = sitecode
                                }, commandType: CommandType.StoredProcedure);
                        }
                    );
                    return await result ?? new List<CRM_API.Models.ViewSubscriptionHistory>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.LLOTG.SProc.GetViewSubscriptionHistory: " + ex.Message);
                throw new Exception("CRM_API.DB.LLOTG.SProc.GetViewSubscriptionHistory: " + ex.Message);
            }
        }
        /// <summary>
        /// GetCardList
        /// </summary>
        /// <param name="mobileno"></param>
        /// <param name="sitecode"></param>
        /// <returns></returns>
        public static async Task<IEnumerable<CRM_API.Models.PaymentProfileCustomer>> GetCardList(string mobileno, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result =
                          Task.Run(() =>
                          {
                              return conn.Query<CRM_API.Models.PaymentProfileCustomer>(
                                      "crm3_get_customer_payment_profile", new
                                      {
                                          @msisdn = mobileno,
                                          @sitecode = sitecode
                                      }, commandType: CommandType.StoredProcedure);
                          });
                    return await result ?? new List<CRM_API.Models.PaymentProfileCustomer>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.LLOTG.SProc.GetCardList: " + ex.Message);
                throw new Exception("CRM_API.DB.LLOTG.SProc.GetCardList: " + ex.Message);
            }
        }
        /// <summary>
        /// DoCancelSubscription
        /// </summary>
        /// <param name="mobileno"></param>
        /// <param name="sitecode"></param>
        /// <param name="p3"></param>
        /// <returns></returns>
        public static int DoCancelSubscription(string mobileno, string sitecode, string p3)
        {
            try
            {
                string[] cancelValues = p3.Split(',');
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {


                    return conn.Query<int>(
                                //"crm3_LLOTG_Multinb_cancel_payg", new
                                "crm3_LLOTG_Multinb_cancel_paym", new                                
                                {
                                    @mobileno = mobileno,
                                    @sitecode = sitecode,
                                    @reg_number = cancelValues[0],
                                    @svc_id = Convert.ToInt32(cancelValues[1]),
                                    //@country = cancelValues[1],
                                    //@paymode = Convert.ToInt32(cancelValues[2]),
                                    //@amount = Convert.ToInt32(cancelValues[3])
                                }, commandType: CommandType.StoredProcedure).First();

                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.LLOTG.SProc.DoCancelSubscription: " + ex.Message);
                throw new Exception("CRM_API.DB.LLOTG.SProc.DoCancelSubscription: " + ex.Message);
            }
        }
        /// <summary>
        /// PayByBalance
        /// </summary>
        /// <param name="mobileno"></param>
        /// <param name="amount"></param>
        /// <param name="sitecode"></param>
        /// <returns></returns>
        public static int PayByBalance(string mobileno, string amount, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {


                    return conn.Query<int>(
                        "crm3_LLOTG_pay_by_balance", new
                        {
                            @mobileno = mobileno,
                            @amount = Convert.ToInt32(amount),
                            @sitecode = sitecode,
                        }, commandType: CommandType.StoredProcedure).First();
                }

            }

            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.LLOTG.SProc.PayByBalance: " + ex.Message);
                throw new Exception("CRM_API.DB.LLOTG.SProc.PayByBalance: " + ex.Message);
            }
        }
        /// <summary>
        /// GetUserBalance
        /// </summary>
        /// <param name="mobileno"></param>
        /// <param name="sitecode"></param>
        /// <param name="p3"></param>
        /// <returns></returns>
        public static async Task<IEnumerable<CRM_API.Models.GetBalanceAndFreeMins>> GetUserBalance(string mobileno, string sitecode, string p3)
        {
            try
            {
                string[] inputValues = p3.Split(',');
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result =
                        Task.Run(() =>
                        {
                            return conn.Query<CRM_API.Models.GetBalanceAndFreeMins>(
                                "Crm3_LLOTG_Get_user_balance", new
                                {
                                    @mobileno = mobileno,
                                    @country = inputValues[0],
                                    @sitecode = sitecode,
                                    @paymode = Convert.ToInt32(inputValues[1]),
                                    @amount = Convert.ToInt32(inputValues[2])
                                }, commandType: CommandType.StoredProcedure);
                        }
                    );
                    return await result ?? new List<CRM_API.Models.GetBalanceAndFreeMins>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.LLOTG.SProc.GetUserBalance: " + ex.Message);
                throw new Exception("CRM_API.DB.LLOTG.SProc.GetUserBalance: " + ex.Message);
            }
        }
        /// <summary>
        /// Reactivate
        /// </summary>
        /// <param name="mobileno"></param>
        /// <param name="sitecode"></param>
        /// <param name="p3"></param>
        /// <returns></returns>
        //public static int Reactivate(string mobileno, string sitecode,string p3,int mode)
        //{
        public static ErrCodeMsg Reactivate(string mobileno, string sitecode, string p3, int mode)
        {
            //try
            //{
            //    string[] cancelValues = p3.Split(',');
            //    using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
            //    {
            //        return conn.Query<int>(
            //                   "crm3_LLOTG_Multinb_reactivate_payg", new
            //                   {
            //                       @mobileno = mobileno,
            //                       @sitecode = sitecode,
            //                       @reg_number = cancelValues[0],
            //                       @svc_id = Convert.ToInt32(cancelValues[1]),
            //                       @paymode = Convert.ToInt32(cancelValues[2]),
            //                       @mode=mode

            //                       //@country = cancelValues[1],
            //                       //@paymode = Convert.ToInt32(cancelValues[2]),
            //                       //@amount = Convert.ToInt32(cancelValues[3])
            //                   }, commandType: CommandType.StoredProcedure).First();


            //    }
            //}

            try
            {
                string[] cancelValues = p3.Split(',');
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {

                    //Task.Run(() =>
                    //{
                    var result = conn.Query<ErrCodeMsg>(
                        //"crm3_LLOTG_Multinb_reactivate_payg", new
                                "crm3_LLOTG_Multinb_reactivate_paym", new
                                {
                                    @mobileno = mobileno,
                                    @sitecode = sitecode,
                                    @reg_number = cancelValues[0],
                                    @svc_id = Convert.ToInt32(cancelValues[1]),
                                    @paymode = Convert.ToInt32(cancelValues[2]),
                                    @mode = mode

                                    //@country = cancelValues[1],
                                    //@paymode = Convert.ToInt32(cancelValues[2]),
                                    //@amount = Convert.ToInt32(cancelValues[3])
                                }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    //    }
                    //);
                    return result ?? new ErrCodeMsg();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.LLOTG.SProc.Reactivate: " + ex.Message);
                throw new Exception("CRM_API.DB.LLOTG.SProc.Reactivate: " + ex.Message);
            }
        }

        /// <summary>
        /// ChangePayMode
        /// </summary>
        /// <param name="mobileno"></param>
        /// <param name="sitecode"></param>
        /// <param name="p3"></param>
        /// <returns></returns>
        public static int ChangePayMode(string mobileno, string sitecode, string p3)
        {
            try
            {
                string[] cancelValues = p3.Split(',');
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    return conn.Query<int>("crm3_LLOTG_Update_pay_mode", new
                               {
                                   @mobileno = mobileno,
                                   @reg_number = cancelValues[0],
                                   @paymode = Convert.ToInt32(cancelValues[1]),
                                   @sitecode = sitecode
                               }, commandType: CommandType.StoredProcedure).First();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.LLOTG.SProc.ChangePayMode: " + ex.Message);
                throw new Exception("CRM_API.DB.LLOTG.SProc.ChangePayMode: " + ex.Message);
            }
        }

        /// <summary>
        /// Suspendedtocancel
        /// </summary>
        /// <param name="mobileno"></param>
        /// <param name="sitecode"></param>
        /// <param name="p3"></param>
        /// <returns></returns>
        public static int Suspendedtocancel(string mobileno, string sitecode, string p3)
        {
            try
            {
                string[] cancelValues = p3.Split(',');
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    return conn.Query<int>("crm3_LLOM_suspend_to_cancel", new
                    {
                        @mobileno = mobileno,
                        @sitecode = sitecode,
                        @svc_id = cancelValues[0]
                    }, commandType: CommandType.StoredProcedure).First();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.LLOTG.SProc.crm3_LLOM_suspend_to_cancel: " + ex.Message);
                throw new Exception("CRM_API.DB.LLOTG.SProc.crm3_LLOM_suspend_to_cancel: " + ex.Message);
            }
        }
    }
}
