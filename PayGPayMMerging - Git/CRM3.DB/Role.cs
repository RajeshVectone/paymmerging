﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;

namespace CRM_API.DB.Role
{
    public class Role
    {
        public int id { get; set; }
        public string role_name { get; set; }
    }

    public class RoleSProc
    {
        public static IEnumerable<Role> GetAllRoles(string connStr)
        {
            try
            {
                using (var conn = new SqlConnection(connStr))
                {
                    conn.Open();

                    var result = conn.Query<Role>(
                            "roles_list_all", 
                            commandType: CommandType.StoredProcedure);

                    return result ?? new List<Role>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL GetAllRoles: " + ex.Message);
            }
        }
    }
}
