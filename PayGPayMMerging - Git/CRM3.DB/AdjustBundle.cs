﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;

namespace CRM_API.DB
{
    public class AdjustBundle
    {
        //AddBalance
        public static IEnumerable<AdjustBundleModelsOutput> GetPackInfoByBundleId(AdjustBundleModelsInput input)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<AdjustBundleModelsOutput>(
                            "crm_get_pack_info_by_bundleid", new
                            {
                                @sitecode = input.sitecode,
                                @bundleid = input.bundleid,
                                @mobileno = input.mobileno 
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<AdjustBundleModelsOutput>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL GetPackInfoByBundleId: " + ex.Message);
            }
        }
    }
}
