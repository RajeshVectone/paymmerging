﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CRM_API.DB.PortingNL
{
    public class SProc
    {
        public static IEnumerable<CRM_API.Models.Porting.NLServiceProviderList> GetServiceProvider(string OptType, string ProviderCode, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Models.Porting.NLServiceProviderList>(
                            "crm3_SP_GET_OPERATOR", new
                            {
                                @OptType = OptType,
                                @ProviderCode = ProviderCode,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<Models.Porting.NLServiceProviderList>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("NL GetServiceProvider: " + ex.Message);
            }
        }

        public static Models.Porting.NLSubmitEntry SubmitEntry(Models.Porting.NLSubmitEntry In)
        {
            try
            {
                string sXMLDir = string.Empty;
                string Msisdn = In.Msisdn;
                string StatusAck = string.Empty;
                if (!string.IsNullOrEmpty(In.Iccid))
                {
                    In.NoteOfMsg = "PP" + In.Iccid;
                }
                
                string msgIdentifier,msgidentifierold;
                if (In.IsResent == "true")
                {
                    msgIdentifier = In.MsgIdentifier;
                    msgidentifierold = In.MsgIdentifier;
                }
                else
                {
                    msgIdentifier = String.Format("{0}{1}{2}{3}{4}", In.Rno, "-", In.Dno, "-", DateTime.Now.ToString("yyyyMMddHHmmss"));
                    msgidentifierold = "";
                }
                
                Models.Porting.NLSubmitEntry result = In;

                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var p = new DynamicParameters();
                    p.Add("@MsgIdentifier", msgIdentifier);
                    p.Add("@recipientserviceprovider", In.Rsp);
                    p.Add("@recipientnetworkoperator", In.Rno);
                    p.Add("@donorserviceprovider", In.Dsp);
                    p.Add("@donornetworkoperator", In.Dno);
                    p.Add("@requestdate", In.RequestDate_Decimal);
                    p.Add("@initials", In.Initials);
                    p.Add("@prefix", In.Prefix);
                    p.Add("@lastname", In.ContactName);
                    p.Add("@companyname", In.CompanyName);
                    p.Add("@street", In.Street);
                    p.Add("@housenumber", In.HouseNo);
                    p.Add("@housenumberext", In.HouseNoExt);
                    p.Add("@zipcode", In.ZipCode);
                    p.Add("@city", In.City);
                    p.Add("@mobileno", In.TelpNo);
                    p.Add("@icc", string.IsNullOrEmpty(In.Iccid) ? string.Empty : In.Iccid);
                    p.Add("@sender", "BMNL");
                    p.Add("@receiver", "PTXS");
                    p.Add("@vectoneno", In.Msisdn);
                    p.Add("@MsgIdentifierOld", msgidentifierold);
                    p.Add("@Email", In.Email);
                    p.Add("@customerid", In.CustID);
                    p.Add("@ErrorNo", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    p.Add("@CustIdOut", dbType: DbType.String, size: 20, direction: ParameterDirection.Output);
                    p.Add("@sitecode", In.Sitecode);
                    conn.Execute("crm3_SP_INSERT_PORTING_REQUEST", p, null, null, CommandType.StoredProcedure);

                    result.CustIdOut = p.Get<string>("CustIdOut");
                    result.errcode = p.Get<Int32>("ErrorNo");
                }

                    if (result != null)
                    {
                        if (result.errcode != 0)
                        {
                            result.errmsg = String.Format("{0}{1}{2}", "Barablu ICCID for Vectone Number: ", Msisdn, " is not exist!");
                        }
                        else if (result.CustIdOut != null)
                        {
                            string PortinState = "submit";
                            StatusAck = "0";
                            StatusAck = CRM_API.Helpers.Porting.XmlAdapter.ServiceNLPorting(In, msgIdentifier, sXMLDir, PortinState);

                            UpdateStatusAck(msgIdentifier, StatusAck, In.Sitecode);

                            if (StatusAck == "1")
                            {
                                result.errmsg = "Successfully Inserted Porting Request. Please Insert Another Request.";

                            }
                            else
                            {
                                result.errcode = -1;
                                result.errmsg = "Request Not Acknowledged!";
                            }
                        }
                        else 
                        {
                            result.errcode = -1;
                            result.errmsg = "Customer ID Null Value";
                        }
                    }
                    else
                    {
                        result.errcode = -1;
                        result.errmsg = "Null Value";
                    }

                    return result;
                
            }
            catch (Exception ex)
            {
                Models.Porting.NLSubmitEntry result = new Models.Porting.NLSubmitEntry();
                result.errcode = -1;
                result.errmsg = ex.Message;
                return result;
            }
        }

        public static void UpdateStatusAck(string msgidentifier, string StatusAck, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var p = new DynamicParameters();
                    p.Add("@MsgIdentifier", msgidentifier);
                    p.Add("@StatusAck", StatusAck);
                    p.Add("@sitecode", sitecode);
                    conn.Execute("crm3_SP_UPDATE_STATUS_ACK", p, null, null, CommandType.StoredProcedure);

                }
            }
            catch (Exception ex)
            {
                throw new Exception("NL UpdateStatusAck: " + ex.Message);
            }
        }

        public static IEnumerable<Models.Porting.StateList> GetState(string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Models.Porting.StateList>(
                            "crm3_SP_GET_OBJECT_LIST", new
                            {
                                @Object = "porting",
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<Models.Porting.StateList>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("NL GetState: " + ex.Message);
            }
        }

        public static IEnumerable<Models.Porting.NLPortingList> PortingList(Models.Porting.NLSubmitEntry In)        
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Models.Porting.NLPortingList>(
                            "crm3_SP_GET_TRX_LIST_V2", new
                            {
                                @StateId = In.StateID == string.Empty ? "%":In.StateID,
                                @TrxType = "IN",
                                @Msisdn = In.Msisdn == string.Empty ? "%" : In.Msisdn,
                                @MsgIdentifier = In.MsgIdentifier == string.Empty ? "%" : In.MsgIdentifier,
                                @PortingDateFrom = In.PortingDateFrom_Decimal == 0 ? 0:In.PortingDateFrom_Decimal,
                                @PortingDateTo = In.PortingDateTo_Decimal == 0 ? 0 : In.PortingDateTo_Decimal,
                                @sitecode = In.Sitecode
                            },null,true,100000,commandType: CommandType.StoredProcedure);

                    return result ?? new List<Models.Porting.NLPortingList>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("NL GetTrxList: " + ex.Message);
            }
        }

        public static Models.Porting.NLPortingList ViewDetailPortingList(string MsgIdentifier,string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Models.Porting.NLPortingList>(
                            "crm3_SP_VIEW_PORTING", new
                            {
                                @MsgIdentifier = MsgIdentifier,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("NL GetTrxList: " + ex.Message);
            }
        }


        #region perform porting

        public static Common.ErrCodeMsg PortingPerformed(Models.Porting.NLSubmitEntry In)
        {
            try
            {
                string sXMLDir = string.Empty;
                string Msisdn = In.Msisdn;
                string StatusAck = string.Empty;
                string ResponseFile = string.Empty;
                string XmlString = string.Empty;
                string PP = "porting_performed";
                string PortinState = "perform";

                Common.ErrCodeMsg result = new Common.ErrCodeMsg(); ;

                Common.ErrCodeMsg Validation = ValidationPorting(In.MsgIdentifier, In.Sitecode);

                if (Validation.errcode == 0)
                {
                    StatusAck = "0";
                    StatusAck = CRM_API.Helpers.Porting.XmlAdapter.ServiceNLPorting(In,In.MsgIdentifier,sXMLDir,PortinState);

                    if (StatusAck == "1")
                    {
                        Common.ErrCodeMsg UpdateAct = UpdateAction(In.MsgIdentifier, PP, In.Sitecode);
                        if (UpdateAct.errcode != null || UpdateAct.errcode != -1)
                        {
                            UpdateStatusAck(In.MsgIdentifier, StatusAck, In.Sitecode);
                            result.errcode = 0;
                            result.errmsg = "Porting Successfully Performed";
                        }
                        else 
                        {
                            result.errcode = -1;
                            result.errmsg = UpdateAct.errmsg;
                        }

                    }
                    else
                    {
                        result.errcode = -1;
                        result.errmsg = "Request Not Acknowledged!";
                    }

                }
                else
                {
                    result.errcode = -1;
                    result.errmsg = Validation.errmsg;
                }

                return result;
            }
            catch (Exception ex)
            {
                Common.ErrCodeMsg result = new Common.ErrCodeMsg();
                result.errcode = -1;
                result.errmsg = ex.Message;
                return result;
            }
        }

        public static Common.ErrCodeMsg ValidationPorting(string MsgIdentifier, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    Common.ErrCodeMsg result = new Common.ErrCodeMsg();
                    conn.Open();
                    var p = new DynamicParameters();
                    p.Add("@MsgIdentifier",MsgIdentifier);
                    p.Add("@IsPP", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    p.Add("@ErrMsg", dbType: DbType.String, size: 200, direction: ParameterDirection.Output);
                    p.Add("@sitecode", sitecode);
                    conn.Execute("crm3_sp_pp_validation", p, null, null, CommandType.StoredProcedure);

                    result.errcode = p.Get<Int32>("IsPP");
                    result.errmsg = p.Get<String>("ErrMsg");
                    return result;
                }
                
            }
            catch (Exception ex)
            {
                Common.ErrCodeMsg result = new Common.ErrCodeMsg
                {
                    errcode = -1,
                    errmsg = ex.Message
                };
                throw new Exception("NL ValidationPorting: " + ex.Message);
            }
        }

        public static Common.ErrCodeMsg UpdateAction(string MsgIdentifier, string PP,string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var p = new DynamicParameters();
                    p.Add("@MsgIdentifier", MsgIdentifier);
                    p.Add("@MsgType", PP);
                    p.Add("@sitecode", sitecode);
                    conn.Execute("crm3_SP_UPDATE_ACTION", p, null, null, CommandType.StoredProcedure); 

                }

                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    Common.ErrCodeMsg result = new Common.ErrCodeMsg();
                    conn.Open();
                    var p = new DynamicParameters();
                    p.Add("@MsgIdentifier", MsgIdentifier);
                    p.Add("@errcode", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    p.Add("@errmsg", dbType: DbType.String, size: 200, direction: ParameterDirection.Output);
                    p.Add("@sitecode", sitecode);
                    conn.Execute("crm3_SP_SET_MSISDN_IN", p, null, null, CommandType.StoredProcedure);

                    result.errcode = p.Get<Int32>("errcode");
                    result.errmsg = p.Get<String>("errmsg");
                    return result;
                }

            }
            catch (Exception ex)
            {
                Common.ErrCodeMsg result = new Common.ErrCodeMsg
                {
                    errcode = -1,
                    errmsg = ex.Message
                };
                throw new Exception("NL UpdateAction: " + ex.Message);
            }
        }

        #endregion


        #region Cancel porting

        public static Common.ErrCodeMsg CancelPorting(Models.Porting.NLSubmitEntry In)
        {
            try
            {
                string sXMLDir = string.Empty;
                string Msisdn = In.Msisdn;
                string StatusAck = string.Empty;
                string ResponseFile = string.Empty;
                string XmlString = string.Empty;
                string PortinState = "cancel";
                Common.ErrCodeMsg result = new Common.ErrCodeMsg();

                    StatusAck = "0";
                    StatusAck = CRM_API.Helpers.Porting.XmlAdapter.ServiceNLPorting(In, In.MsgIdentifier, sXMLDir, PortinState);

                    if (StatusAck == "1")
                    {
                        bool Update = UpdateActionCancel(In.MsgIdentifier, In.Sitecode, In.NoteOfMsg);
                        if (Update == true)
                        {
                            UpdateStatusAck(In.MsgIdentifier, StatusAck, In.Sitecode);
                            result.errcode = 0;
                            result.errmsg = "Request has been Cancelled";
                        }
                        else
                        {
                            result.errcode = -1;
                            result.errmsg = "Can't Cancel Porting request";
                        }

                    }
                    else
                    {
                        result.errcode = -1;
                        result.errmsg = "Request Not Acknowledged!";

                    }

                return result;
            }
            catch (Exception ex)
            {
                Common.ErrCodeMsg result = new Common.ErrCodeMsg();
                result.errcode = -1;
                result.errmsg = ex.Message;
                return result;
            }
        }

        public static bool UpdateActionCancel(string MsgIdentifier, string sitecode,string NoteOfMsg)
        {
            try
            {

                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    Common.ErrCodeMsg result = new Common.ErrCodeMsg();
                    conn.Open();
                    var p = new DynamicParameters();
                    p.Add("@MsgIdentifier", MsgIdentifier);
                    p.Add("@NoteOfMsg ", NoteOfMsg);
                    p.Add("@sitecode", sitecode);
                    conn.Execute("crm3_SP_UPDATE_CCL", p, null, null, CommandType.StoredProcedure);

                    return true;
                }

            }
            catch (Exception ex)
            {
                return false;
                throw new Exception("NL UpdateAction: " + ex.Message);
                
            }
        }

        #endregion


        #region revise porting

        public static Common.ErrCodeMsg EditPorting(Models.Porting.NLSubmitEntry In)
        {
            try
            {
                string sXMLDir = string.Empty;
                string Msisdn = In.Msisdn;
                string StatusAck = string.Empty;
                string ResponseFile = string.Empty;
                string XmlString = string.Empty;
                string PortinState = "edit";
                Common.ErrCodeMsg result = new Common.ErrCodeMsg();

                StatusAck = "0";
                StatusAck = CRM_API.Helpers.Porting.XmlAdapter.ServiceNLPorting(In, In.MsgIdentifier, sXMLDir, PortinState);

                if (StatusAck == "1")
                {
                    bool Update = UpdateActionEdit(In.MsgIdentifier, In.Sitecode, In.NoteOfMsg,In.RequestDate_Decimal);
                    if (Update == true)
                    {
                        UpdateStatusAck(In.MsgIdentifier, StatusAck, In.Sitecode);
                        result.errcode = 0;
                        result.errmsg = "Request has been Changed";
                    }
                    else
                    {
                        result.errcode = -1;
                        result.errmsg = "Change Request is not Updated";
                    }

                }
                else
                {
                    result.errcode = -1;
                    result.errmsg = "Change Request is not Acknowledged";
                }

                return result;
            }
            catch (Exception ex)
            {
                Common.ErrCodeMsg result = new Common.ErrCodeMsg();
                result.errcode = -1;
                result.errmsg = ex.Message;
                return result;
            }
        }

        public static bool UpdateActionEdit(string MsgIdentifier, string sitecode, string NoteOfMsg,decimal changedate)
        {
            try
            {

                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    Common.ErrCodeMsg result = new Common.ErrCodeMsg();
                    conn.Open();
                    var p = new DynamicParameters();
                    p.Add("@MsgIdentifier", MsgIdentifier);
                    p.Add("@ChangeDate", changedate);
                    p.Add("@NoteOfMsg ", NoteOfMsg);
                    p.Add("@sitecode", sitecode);
                    conn.Execute("crm3_SP_UPDATE_CCL", p, null, null, CommandType.StoredProcedure);

                    return true;
                }

            }
            catch (Exception ex)
            {
                return false;
                throw new Exception("NL UpdateAction: " + ex.Message);
                
            }
        }

        #endregion
    }
}