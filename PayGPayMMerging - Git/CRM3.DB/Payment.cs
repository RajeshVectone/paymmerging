﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using NLog;
using Newtonsoft.Json;
using CRM_API.Models.Payment;

namespace CRM_API.DB
{
    public class Payment
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        //26-Dec-2018 : Moorthy : Added for Payment Pending List
        public static IEnumerable<CustomerWisePaymentBreakupOutput> GetCustomerWisePaymentBreakup(string sitecode, string mobileno, string date_from, string date_to)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var result = conn.Query<CustomerWisePaymentBreakupOutput>(
                            "crm_get_customer_wise_payment_breakup", new
                            {
                                @sitecode = sitecode,
                                @mobileno = mobileno,
                                @date_from = date_from,
                                @date_to = date_to
                            }, null, true, (5 * 60 * 60), commandType: CommandType.StoredProcedure);

                    return result ?? new List<CustomerWisePaymentBreakupOutput>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetCustomerWisePaymentBreakup: {0}", ex.Message);
                return new List<CustomerWisePaymentBreakupOutput>();
            }
        }

        public static IEnumerable<CustomerWisePaymentDetailsOutput> GetCustomerWisePaymentDetails(string sitecode, string searchby, string mobileno, string date_from, string date_to)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var result = conn.Query<CustomerWisePaymentDetailsOutput>(
                            "crm_get_customer_wise_payment_details", new
                            {
                                @sitecode = sitecode,
                                @mobileno = mobileno,
                                @date_from = date_from,
                                @date_to = date_to,
                                @search_type = searchby
                            }, null, true, (5 * 60 * 60), commandType: CommandType.StoredProcedure);

                    return result ?? new List<CustomerWisePaymentDetailsOutput>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetCustomerWisePaymentBreakup: {0}", ex.Message);
                return new List<CustomerWisePaymentDetailsOutput>();
            }
        }

        public static IEnumerable<CRM_API.Models.Payment.Subscription> GetSubscribtionList(string accountId)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.PaymentConnection))
                {
                    conn.Open();

                    var result = conn.Query<CRM_API.Models.Payment.Subscription>(
                            "Rxpaym_cc_payment_getsubscriptionlist_crm", new
                            {
                                @account_id = accountId
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.Payment.Subscription>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.Payment.GetSubscribtionList: {0}", ex.Message);
                return new List<CRM_API.Models.Payment.Subscription>();
            }
        }
        public static IEnumerable<CRM_API.Models.Topup> GetTopupLog(string Msisdn, string Sitecode, DateTime Startdate, DateTime Enddate)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.Topup>(
                            "crm3_topup_get_by_mobileno", new
                            {
                                @mobileno = Msisdn,
                                @sitecode = Sitecode,
                                //@startdate = String.IsNullOrEmpty(Convert.ToString(Startdate)) ? Convert.ToString(Startdate) : Startdate.ToString("dd MMM yyyy"),
                                @startdate = Startdate.ToString("dd MMM yyyy"),
                                @enddate = Enddate.ToString("dd MMM yyyy")
                            },null ,true,(5*60*60),commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.Topup>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetTopupLog: {0}", ex.Message);
                return new List<CRM_API.Models.Topup>();
            }
        }

        public static CRM_API.Models.TopupStatus GetTopupStatus(string Msisdn, string Sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    CRM_API.Models.TopupStatus values = new CRM_API.Models.TopupStatus();
                    var p = new DynamicParameters();
                    p.Add("@msisdn", Msisdn);
                    p.Add("@sitecode", Sitecode);
                    p.Add("@status", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    p.Add("@threshold", dbType: DbType.Double, direction: ParameterDirection.Output);
                    p.Add("@message", dbType: DbType.String, size: 100, direction: ParameterDirection.Output);

                    conn.Execute("crm3_crm_get_autotopup_status", p, commandType: CommandType.StoredProcedure);

                    values.status = p.Get<int>("@status");
                    values.threshold = p.Get<double>("@threshold");
                    values.message = p.Get<string>("@message");
                    
                    
                    return values;
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetTopupStatus: {0}", ex.Message);
                return new Models.TopupStatus() { errcode = -1, errmsg = ex.Message };
            }
        }

        //20-Feb-2019 : Moorthy : Added for New Cancel Topup
        public static CRM_API.Models.TopupStatusNew GetTopupStatusNew(string Msisdn, string Sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.TopupStatusNew>(
                            "crm_get_autotopup_mobileno_info", new
                            {
                                @mobileno = Msisdn,
                                @sitecode = Sitecode
                            }, null, true, (5 * 60 * 60), commandType: CommandType.StoredProcedure).FirstOrDefault();

                    if (result != null)
                    {
                        result.errcode = 0;
                    }
                    return result ?? new CRM_API.Models.TopupStatusNew();
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetTopupStatus: {0}", ex.Message);
                return new Models.TopupStatusNew() { errcode = -1, errmsg = ex.Message };
            }
        }

        //public static int InsertLog(string sitecode, string productcode, string paymentagent, string servicetype,
        //    string paymentmode, int paymentstep, int paymentstatus, string accountid,
        //    string last6digitccno, double totalamount, string currency,
        //    string subscriptionid, string merchantid, string providercode, string csreasoncode, string generalerrorcode)
        //{
        //    try
        //    {
        //        using (var conn = new SqlConnection(Helpers.Config.PaymentConnection))
        //        {
        //            conn.Open();

        //            var result = conn.Query<int>(
        //                    "csPaymentInsert", new
        //                    {
        //                        @sitecode = sitecode,
        //                        @productcode = productcode,
        //                        @paymentagent = paymentagent,
        //                        @servicetype = servicetype,
        //                        @paymentmode = paymentagent,
        //                        @paymentstep = paymentstep,
        //                        @paymentstatus = paymentstatus,
        //                        @accountid = accountid,
        //                        @last6digitccno = last6digitccno,
        //                        @totalamount = totalamount,
        //                        @currency = currency,
        //                        @subscriptionid = subscriptionid,
        //                        @merchantid = merchantid,
        //                        @providercode = providercode,
        //                        @csreasoncode = csreasoncode,
        //                        @generalerrorcode = generalerrorcode
        //                    }, commandType: CommandType.StoredProcedure);

        //            return result.FirstOrDefault();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.Error("CRM_API.DB.Payment.InsertLog: {0}", ex.Message);
        //        return 0;
        //    }
        //}

        public static Common.ErrCodeMsg CancelTopup(string msisdn, string submitby, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    Common.ErrCodeMsg resvalue = new Common.ErrCodeMsg();

                    conn.Open();

                    var p = new DynamicParameters();
                    p.Add("@msisdn", msisdn);
                    p.Add("@submit_by", submitby);
                    p.Add("@sitecode", sitecode);
                    p.Add("@errcode", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    p.Add("@errmsg", dbType: DbType.String, size: 100, direction: ParameterDirection.Output);

                    conn.Execute("crm3_crm_cancel_tAuxAutoTopup", p, commandType: CommandType.StoredProcedure);

                    resvalue.errcode = p.Get<int>("@errcode");
                    resvalue.errmsg = p.Get<string>("@errmsg");

                    Log.Debug("Cancel Auto Topup: {0} {1}", resvalue.errcode, resvalue.errmsg);
                    return resvalue;
                }
            }
            catch (Exception ex)
            {
                return new Common.ErrCodeMsg() { errcode = -1, errmsg = ex.Message };
            }
        }

        //25-Feb-2019 : Moorthy : CRMIMP-18 - Daily usage history
        public static IEnumerable<CRM_API.Models.DailyUsageHistoryOutput> DailyUsageHistory(string Msisdn, string Sitecode, DateTime Startdate, DateTime Enddate)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.DailyUsageHistoryOutput>(
                            "crm_usage_breakdown_detail", new
                            {
                                @mobileno = Msisdn,
                                @sitecode = Sitecode,
                                @date_from = Startdate.ToString("dd MMM yyyy"),
                                @date_to = Enddate.ToString("dd MMM yyyy")
                            }, null, true, (5 * 60 * 60), commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.DailyUsageHistoryOutput>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("DailyUsageHistory: {0}", ex.Message);
                return new List<CRM_API.Models.DailyUsageHistoryOutput>();
            }
        }
    }
}