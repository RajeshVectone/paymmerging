﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Dapper;
using System.Threading.Tasks;
using CRM_API.Models.AirTimeTransfer;


namespace CRM_API.DB.AirTimeTransfer
{
    public class SProc
    {
        private static string SP_GETLIST_AIRTIME = "crm3_airtimetransfer_getlist";

        public static IEnumerable<AirTimeTransferRes> GetListAirTimeTransfer(AdditionalReqInfo Model)
        {
            DateTime dtFrom = DateTime.ParseExact(Model.Datefrom, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            DateTime dtTo = DateTime.ParseExact(Model.Dateto, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<AirTimeTransferRes>(SP_GETLIST_AIRTIME, new
                    {
                        @mobileno = Model.Mobileno,
                        @datefrom = dtFrom,
                        @dateto = dtTo,
                        @recipientname = Model.Name,
                        @sitecode = Model.Sitecode
                    }, commandType: CommandType.StoredProcedure
                    );

                    return result ?? new List<AirTimeTransferRes>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("SP_GETLIST_AIRTIME: " + ex.Message);
            }
        }       
    }
}
