﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Dapper;
using System.Threading.Tasks;

namespace CRM_API.DB
{
    public class PAYMBillGeneration
    {

        /// <summary>
        /// Add by BSK for CRMT-111 Jira
        /// Get All Service Status History Details
        /// api/ServiceStatusHistory
        /// </summary>
        public static IEnumerable<CRM_API.Models.ServiceStatusHistory> getServiceStatusHistory(string MobileNo)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.MCMCRMConnection))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.ServiceStatusHistory>("PMBO_Get_ServiceStatusHistory", new
                    {
                        @mobileno = MobileNo,
                        @customerid = "",
                        @subscriberid = "",
                        @freesimid = "",
                    }, commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.ServiceStatusHistory>();
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("PMBO_Get_ServiceStatusHistory  :  " + ex.Message);
            }
        }

        //CRM Enhancement 2
        public static IEnumerable<CRM_API.Models.PAYMDetails> getBillGeneraion(string MobileNo, string sitecode)
        {
            try
            {
                //CRM Enhancement 2
                //using (var conn = new SqlConnection(CRM_API.Helpers.Config.MCMCRMConnection))
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    //var result = conn.Query<CRM_API.Models.PAYMDetails>("c3pm_GetBillingSummary_v2", new
                    //{
                    //    @Mobileno = MobileNo,
                    //    @Customerid = "",
                    //    @Subscriberid = "",
                    //    @FreesimID = "",
                    //   }, commandType: CommandType.StoredProcedure);
                    var result = conn.Query<CRM_API.Models.PAYMDetails>("crm3_paym_get_billing_summary_by_msisdn", new
                    {
                        @mobileno = MobileNo,
                        @sitecode = sitecode
                       }, commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.PAYMDetails>();
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("c3pm_GetBillingSummary_v2  :  " + ex.Message);               
            }
        }

        public static IEnumerable<CRM_API.Models.PAYMDetails> getOutStandingAmount(CRM_API.Models.PAYMMobile objPAYMMobile)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.MCMCRMConnection))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.PAYMDetails>("c3pm_GetBillingSummary_v2", new
                    //var result = conn.Query<CRM_API.Models.PAYMDetails>("c3pm_GetBillingSummary_outstanding_amount", new     // new logig by logu 
                    {
                        @Mobileno = objPAYMMobile.MobileNo,
                        @Customerid = "",
                        @Subscriberid = "",
                        @FreesimID = "",
                    }, commandType: CommandType.StoredProcedure).OrderByDescending(r => r.BillPeriod).Take(1);
                    return result ?? new List<CRM_API.Models.PAYMDetails>();
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("c3pm_GetBillingSummary  :  " + ex.Message);
            }
        }
    }
}
