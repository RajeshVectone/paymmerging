﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;

namespace CRM_API.DB.Voucher
{
    public class SProc
    {
        public static CRM_API.Models.VoucherInfo VoucherCheckbyCBS(string cbs, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<CRM_API.Models.VoucherInfo>(
                            "crm3_crm_voucher_check_by_cbs", new
                            {
                                @serialnumber = cbs,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    return result ?? new CRM_API.Models.VoucherInfo();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL VoucherCheckbyCBS:" + ex.Message);
            }
        }
        public static CRM_API.Models.VoucherInfo VoucherCheckbyPin(string pin, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<CRM_API.Models.VoucherInfo>(
                            "crm3_crm_voucher_check_by_pin", new
                            {
                                @pin = pin,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    return result ?? new CRM_API.Models.VoucherInfo();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL VoucherCheckbyPin:" + ex.Message);
            }
        }
    }
}
