﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;

namespace CRM_API.DB.PAYMPayments
{
    public class SProc
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public static IEnumerable<Common.ErrCodeMsg> PaymentInsert(DateTime pay_date, string mobileno,
            string pay_reference, string pay_method, float pay_amount, int pay_status, string updatedby)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.PMBOConnection))
                {
                    conn.Open();

                    var result = conn.Query<Common.ErrCodeMsg>(
                            "c3pm_paymentinsert ", new
                            {
                                @pay_reference = pay_reference,
                                @mobileno = mobileno,
                                @pay_method = pay_method,
                                @pay_amount = pay_amount,
                                @pay_date = pay_date,
                                @pay_status = pay_status,
                                @updatedby = updatedby
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<Common.ErrCodeMsg>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.PAYMPayments.PaymentInsert: {0}", ex.Message);
                List<Common.ErrCodeMsg> errValues = new List<Common.ErrCodeMsg>();
                do
                {
                    errValues.Add(new Common.ErrCodeMsg() { errcode = -1, errmsg = ex.Message });
                    ex = ex.InnerException;
                } while (ex != null);
                return errValues;
            }
        }

        public static IEnumerable<Common.ErrCodeMsg> PaymentInsert_ForPaymentQueued(DateTime pay_date, string mobileno,
    string pay_reference, string pay_method, float pay_amount, int pay_status, string updatedby)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.PMBOConnection))
                {
                    conn.Open();

                    var result = conn.Query<Common.ErrCodeMsg>(
                            "c3pm_paymentinsert_queue", new
                            {
                                @pay_reference = pay_reference,
                                @mobileno = mobileno,
                                @pay_method = pay_method,
                                @pay_amount = pay_amount,
                                @pay_date = pay_date,
                                @pay_status = pay_status,
                                @updatedby = updatedby
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<Common.ErrCodeMsg>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.PAYMPayments.PaymentInsert_Queue : {0}", ex.Message);
                List<Common.ErrCodeMsg> errValues = new List<Common.ErrCodeMsg>();
                do
                {
                    errValues.Add(new Common.ErrCodeMsg() { errcode = -1, errmsg = ex.Message });
                    ex = ex.InnerException;
                } while (ex != null);
                return errValues;
            }
        }

        public static CRM_API.Models.PAYMPaymentModels.PaymentAutoQueue PaymentAutoQueueList()
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.PMBOConnection))
                {
                    conn.Open();

                    var result = conn.Query<CRM_API.Models.PAYMPaymentModels.PaymentAutoQueue>(
                            "c3pm_paymentauto_queue ", null, commandType: CommandType.StoredProcedure);

                    if (result == null || result.Count() < 0)
                        return null;
                    else
                        return result.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.PAYMPayments.PaymentAutoQueue: {0}", ex.Message);
                return null;
            }
        }

        public static IEnumerable<Common.ErrCodeMsg> PaymentAutoQueueUpdate(int pay_idx,
            string pay_reference, int pay_status, string updatedby)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.PMBOConnection))
                {
                    conn.Open();

                    var result = conn.Query<Common.ErrCodeMsg>(
                            "c3pm_paymentauto_update ", new
                            {
                                @payidx = pay_idx,
                                @pay_reference = pay_reference,
                                @updatedby = updatedby,
                                @pay_status = pay_status
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<Common.ErrCodeMsg>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.PAYMPayments.PaymentAutoQueueUpdate: {0}", ex.Message);
                List<Common.ErrCodeMsg> errValues = new List<Common.ErrCodeMsg>();
                do
                {
                    errValues.Add(new Common.ErrCodeMsg() { errcode = -1, errmsg = ex.Message });
                    ex = ex.InnerException;
                } while (ex != null);
                return errValues;
            }
        }

        public static IEnumerable<Common.ErrCodeMsg> ResetQueuedPayments(
            string pay_reference, int pp_customer_id)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.PMBOConnection))
                {
                    conn.Open();

                    var result = conn.Query<Common.ErrCodeMsg>(
                            "Process_Payment_CC_Status_reset", new
                            {
                                @customer_id = pp_customer_id,
                                @pay_reference = pay_reference
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<Common.ErrCodeMsg>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.PAYMPayments.ResetQueuedPayments: {0}", ex.Message);
                List<Common.ErrCodeMsg> errValues = new List<Common.ErrCodeMsg>();
                do
                {
                    errValues.Add(new Common.ErrCodeMsg() { errcode = -1, errmsg = ex.Message });
                    ex = ex.InnerException;
                } while (ex != null);
                return errValues;                
            }
        }

    }
}
