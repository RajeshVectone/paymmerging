﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;
using CRM_API.Models.Complaint;

namespace CRM_API.DB
{
    public class ComplaintsHistory
    {
        ////Issue History Report
        public static IEnumerable<ComplaintModels> GetComplaintsHistoryList(string date_from, string date_to, int cur_user)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<Models.Complaint.ComplaintModels>(
                            "crm_get_tickets_by_mobileno", new
                            {
                                @mobileno = "all",
                                @date_from = date_from,
                                @date_to = date_to,
                                @userid = cur_user
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<Models.Complaint.ComplaintModels>();
                }
            }
            catch (Exception ex)
            {
                List<Models.Complaint.ComplaintModels> resValue = new List<Models.Complaint.ComplaintModels>();
                return resValue;
            }

        }
    }
}
