﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;

namespace CRM_API.DB.Customer2in1
{
    public class SProc
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public static IEnumerable<Customer2in1Records> List2in1Records(string mobileNo)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Customer2in1Records>(
                            "crm3_mimsi_list_records ", new
                            {
                                @mobileno = mobileNo
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<Customer2in1Records>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("List2in1Records BL: {0}", ex.Message);
                throw new Exception("BL List2in1Records: " + ex.Message);
            }
        }
        public static Common.ErrCodeMsg Insert2in1RecordsByCounty(string mobileno, string imsi_country)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Common.ErrCodeMsg>(
                            "crm3_set_roaming_country", new
                            {
                                @mobileno = mobileno,
                                @country = imsi_country
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("Insert2in1RecordsBycountry: {0}", ex.Message);
                return new Common.ErrCodeMsg() { errcode = -1, errmsg = ex.Message };
            }
        }
        public static IEnumerable<Customer2in1Records> List2in1Records1(string mobileNo)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Customer2in1Records>(
                            "crm3_Get_2_in_1_List_By_Mobileno", new
                            {
                                @mobileno = mobileNo
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<Customer2in1Records>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("List2in1Records BL: {0}", ex.Message);
                throw new Exception("BL List2in1Records: " + ex.Message);
            }
        }

        public static IEnumerable<Customer2in1Records> List2in1Expired()
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Customer2in1Records>(
                            "crm3_mimsi_expired_list ", commandType: CommandType.StoredProcedure);

                    return result ?? new List<Customer2in1Records>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("List2in1Expired BL: {0}", ex.Message);
                return new List<Customer2in1Records>();
            }
        }

        public static Common.ErrCodeMsg Insert2in1Records(Customer2in1Records orderAddress, double renewal_charge, string orderUrl)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Common.ErrCodeMsg>(
                            "crm3_mimsi_insert_record ", new
                            {
                                @mobileno = orderAddress.mobileno,
                                @country = orderAddress.imsi_country,
                                @crm_login = orderAddress.crm_login,
                                @title = orderAddress.title,
                                @first_name = orderAddress.first_name,
                                @last_name = orderAddress.last_name,
                                @address1 = orderAddress.address1,
                                @address2 = orderAddress.address2,
                                @town = orderAddress.town,
                                @postcode = orderAddress.postcode,
                                @country_order = orderAddress.country,
                                @email = orderAddress.email,
                                @contact = orderAddress.contact,
                                @payment_ref = orderAddress.payment_ref,
                                @subscription_id = orderAddress.SubscriptionID,
                                @charge = renewal_charge,
                                @ordersim_url = orderUrl,
                                @id_type = orderAddress.id_type,
                                @id_number = orderAddress.id_number,
                                @sitecode = orderAddress.sitecode
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    Log.Debug("Insert2in1Records: {0}", result.errmsg);
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("Insert2in1Records: {0}", ex.Message);
                return new Common.ErrCodeMsg() { errcode = -1, errmsg = ex.Message };
            }
        }

        public static Common.ErrCodeMsg Insert2in1RecordsByCredit(Customer2in1Records orderAddress, double renewal_charge, string orderUrl)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Common.ErrCodeMsg>(
                            "crm3_mimsi_insert_record_bycredit ", new
                            {
                                @mobileno = orderAddress.mobileno,
                                @country = orderAddress.imsi_country,
                                @crm_login = orderAddress.crm_login,
                                @title = orderAddress.title,
                                @first_name = orderAddress.first_name,
                                @last_name = orderAddress.last_name,
                                @address1 = orderAddress.address1,
                                @address2 = orderAddress.address2,
                                @town = orderAddress.town,
                                @postcode = orderAddress.postcode,
                                @country_order = orderAddress.country,
                                @email = orderAddress.email,
                                @contact = orderAddress.contact,
                                @charge = renewal_charge,
                                @ordersim_url = orderUrl,
                                @id_type = orderAddress.id_type,
                                @id_number = orderAddress.id_number,
                                @sitecode = orderAddress.sitecode
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("Insert2in1RecordsByCredit: {0}", ex.Message);
                return new Common.ErrCodeMsg() { errcode = -1, errmsg = ex.Message };
            }
        }

        public static Common.ErrCodeMsg Renew2in1Record(int orderId, string crm_login)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Common.ErrCodeMsg>(
                            "crm3_mimsi_renew ", new
                            {
                                @order_id = orderId,
                                @crm_login = crm_login
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    Log.Debug("Renew2in1Record: {0} {1}", result.errcode, result.errmsg);
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("Renew2in1Record: {0}", ex.Message);
                return new Common.ErrCodeMsg() { errcode = -1, errmsg = ex.Message };
            }
        }

        public static Common.ErrCodeMsg Renew2in1RecordByCredit(int orderId, string crm_login, double renewal_charge)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Common.ErrCodeMsg>(
                            "crm3_mimsi_renew_bycredit ", new
                            {
                                @order_id = orderId,
                                @crm_login = crm_login,
                                @charge = renewal_charge
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    Log.Debug("Renew2in1RecordsByCredit: {0} {1}", result.errcode, result.errmsg);
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("Renew2in1RecordsByCredit: {0}", ex.Message);
                return new Common.ErrCodeMsg() { errcode = -1, errmsg = ex.Message };
            }
        }

        /// <summary>
        /// Return the JobId as Key
        /// </summary>
        /// <param name="orderId">jobId</param>
        /// <param name="crm_login"></param>
        /// <returns></returns>
        public static KeyValuePair<int, IEnumerable<OTAString>> List2in1OTAString(int orderId, string crm_login)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    int jobId = 0;
                    var p = new DynamicParameters();
                    p.Add("@order_id", orderId);
                    p.Add("@login_name", crm_login);

                    var result = conn.Query<OTAString>(
                            "crm3_get_ota_strings",
                            p,
                            commandType: CommandType.StoredProcedure);
                    if (result == null)
                        return new KeyValuePair<int, IEnumerable<OTAString>>(jobId, new List<OTAString>());
                    else
                    {
                        if (result.FirstOrDefault() != null)
                            jobId = result.FirstOrDefault().ota_job_id;
                        return new KeyValuePair<int, IEnumerable<OTAString>>(jobId, result);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("List2in1OTAString BL: {0}", ex.Message);
                return new KeyValuePair<int, IEnumerable<OTAString>>();
            }
        }

        public static Common.ErrCodeMsg OTAUpdate(int jobId, int seqNo, int status)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Common.ErrCodeMsg>(
                            "crm3_ota_sent_update", new
                            {
                                @job_id = jobId,
                                @seq_no = seqNo,
                                @status = status
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("OTAUpdate BL: {0}", ex.Message);
                return new Common.ErrCodeMsg() { errcode = -1, errmsg = ex.Message };
            }
        }

        /// <summary>
        /// Calls to CRM_API Connection to make a sim swap
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="newICCID"></param>
        /// <param name="crm_login"></param>
        /// <returns></returns>
        public static SMSMsgReturn SimSwap(int orderId, string newICCID, string crm_login)
        {
            SMSMsgReturn resvalue = new SMSMsgReturn()
            {
                errcode = -1,
                errmsg = "No result",
                sms_msg = string.Empty
            };

            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var p = new DynamicParameters();
                    p.Add("@order_id", orderId);
                    p.Add("@iccid_new", newICCID);
                    p.Add("@crm_login", crm_login);
                    //p.Add("@orderid", crm_login);
                    //p.Add("@jira_ticket", orderId);
                    //p.Add("@charge", charge);
                    //p.Add("@balance_old", dbType: DbType.Double, direction: ParameterDirection.Output);
                    //p.Add("@afterbal", dbType: DbType.Double, direction: ParameterDirection.Output);
                    //p.Add("@errcode", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    //p.Add("@errmsg", dbType: DbType.String, direction: ParameterDirection.Output);

                    //conn.Execute("crm3_2in1_sim_swap ", p, commandType: CommandType.StoredProcedure);

                    //resvalue.errcode = p.Get<int>("@errcode");
                    //resvalue.errmsg = p.Get<string>("@errmsg");

                    resvalue = conn.Query<SMSMsgReturn>(
                            "crm3_2in1_sim_swap", p, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }

            }
            catch (Exception ex)
            {
                resvalue.errcode = -1;
                resvalue.errmsg = ex.Message;
                Log.Error("SimSwap BL: {0}", ex.Message);
            }

            return resvalue;
        }

        /// <summary>
        /// Change the OTA status
        /// </summary>
        /// <param name="jobId"></param>
        /// <param name="status"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public static SMSMsgReturn OTAStatusUpdate(int jobId, string status, string description)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<SMSMsgReturn>(
                            "crm3_ota_status_set ", new
                            {
                                @job_id = jobId,
                                @status = status,
                                @descript = description
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("OTAStatusUpdate BL: {0}", ex.Message);
                return new SMSMsgReturn() { errcode = -1, errmsg = ex.Message };
            }
        }

        /// <summary>
        /// Change the Order status
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="status"></param>
        /// <param name="crm_login"></param>
        /// <returns></returns>
        public static Common.ErrCodeMsg OrderStatusUpdate(int orderId, string status, string crm_login)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Common.ErrCodeMsg>(
                            "crm3_2in1_order_update ", new
                            {
                                @order_id = orderId,
                                @order_status_id = status,
                                @crm_login = crm_login
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    Log.Debug("OrderStatusUpdate: {0} {1}", result.errcode, result.errmsg);
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("OrderStatusUpdate BL: {0}", ex.Message);
                return new Common.ErrCodeMsg() { errcode = -1, errmsg = ex.Message };
            }
        }

        public static Common.ErrCodeMsg Cancel(int orderId, string crm_login)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Common.ErrCodeMsg>(
                            "crm3_2in1_cancel ", new
                            {
                                @order_id = orderId,
                                @crm_login = crm_login
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    Log.Debug("Cancel: {0} {1}", result.errcode, result.errmsg);
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("Cancel BL: {0}", ex.Message);
                return new Common.ErrCodeMsg() { errcode = -1, errmsg = ex.Message };
            }
        }

        public static Models.Customer2in1Records GetStatus(int orderId,string Subscription,string mobileno,string crm_login)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Models.Customer2in1Records>(
                            "crm3_mimsi_status_check ", new
                            {
                                @order_id = orderId
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    result.order_id = orderId;
                    result.SubscriptionID = Subscription;
                    result.mobileno = mobileno;
                    result.crm_login = crm_login;
                    Log.Debug("GetStatus: {0} {1}", orderId, result.order_status);
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetStatus BL: {0}", ex.Message);
                return new Models.Customer2in1Records() { order_id = orderId };
            }
        }

        public static Common.ErrCodeMsg DeActivate(int orderId, string crm_login, out int needCancelLoc)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    Common.ErrCodeMsg resvalue = new Common.ErrCodeMsg();

                    conn.Open();

                    var p = new DynamicParameters();
                    p.Add("@order_id", orderId);
                    p.Add("@crm_login", crm_login);
                    p.Add("@errcode", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    p.Add("@errmsg", dbType: DbType.String, size: 100, direction: ParameterDirection.Output);
                    p.Add("@need_cancel_loc", dbType: DbType.Int32, direction: ParameterDirection.Output);

                    conn.Execute("crm3_mimsi_deactivate ", p, commandType: CommandType.StoredProcedure);

                    resvalue.errcode = p.Get<int>("@errcode");
                    resvalue.errmsg = p.Get<string>("@errmsg");
                    needCancelLoc = p.Get<int>("@need_cancel_loc");

                    Log.Debug("DeActivate: {0} {1}", resvalue.errcode, resvalue.errmsg);
                    return resvalue;
                }
            }
            catch (Exception ex)
            {
                Log.Error("DeActivate BL: {0}", ex.Message);
                needCancelLoc = 0;
                return new Common.ErrCodeMsg() { errcode = -1, errmsg = ex.Message };
            }
        }

        public static Common.ErrCodeMsg ReActivate(int orderId, string crm_login)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    Common.ErrCodeMsg resvalue = new Common.ErrCodeMsg();
                    conn.Open();
                    var p = new DynamicParameters();
                    p.Add("@order_id", orderId);
                    p.Add("@crm_login", crm_login);
                    p.Add("@errcode", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    p.Add("@errmsg", dbType: DbType.String, size: 100, direction: ParameterDirection.Output);

                    conn.Execute("crm3_mimsi_reactivate", p, commandType: CommandType.StoredProcedure);

                    resvalue.errcode = p.Get<int>("@errcode");
                    resvalue.errmsg = p.Get<string>("@errmsg");

                    Log.Debug("Activate: {0} {1}", resvalue.errcode, resvalue.errmsg);
                    return resvalue;
                }
            }
            catch (Exception ex)
            {
                Log.Error("ReActivate BL: {0}", ex.Message);
                return new Common.ErrCodeMsg() { errcode = -1, errmsg = ex.Message };
            }
        }

        public static Common.ErrCodeMsg ReActivateByCredit(int orderId, string crm_login, double price)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Common.ErrCodeMsg>(
                            "crm3_mimsi_reactivate_bycredit ", new
                            {
                                @order_id = orderId,
                                @crm_login = crm_login,
                                @charge = price
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    Log.Debug("ReActivateByCredit: {0} {1}", result.errcode, result.errmsg);
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("ReActivateByCredit BL: {0}", ex.Message);
                return new Common.ErrCodeMsg() { errcode = -1, errmsg = ex.Message };
            }
        }

        public static Common.ErrCodeMsg ChangeRenewalMethod(int orderId, string crm_login, int renewalMethod, string subscriptionId)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    Common.ErrCodeMsg resvalue = new Common.ErrCodeMsg();

                    conn.Open();

                    // TODO Change to Card missing subscriptionId

                    var p = new DynamicParameters();
                    p.Add("@order_id", orderId);
                    p.Add("@renewal_method", renewalMethod);
                    p.Add("@crm_login", crm_login);
                    p.Add("@errcode", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    p.Add("@errmsg", dbType: DbType.String, size: 100, direction: ParameterDirection.Output);

                    conn.Execute("crm3_mimsi_change_renewalmethod ", p, commandType: CommandType.StoredProcedure);

                    resvalue.errcode = p.Get<int>("@errcode");
                    resvalue.errmsg = p.Get<string>("@errmsg");

                    Log.Debug("ChangeRenewalMethod: {0} {1}", resvalue.errcode, resvalue.errmsg);
                    return resvalue;
                }
            }
            catch (Exception ex)
            {
                Log.Error("ChangeRenewalMethod BL: {0}", ex.Message);
                return new Common.ErrCodeMsg() { errcode = -1, errmsg = ex.Message };
            }
        }

        public static SimOrder InsertSimOrderRecords(Customer2in1Records orderAddress,double renewal_charge)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<SimOrder>(
                            "crm3_mimsi_insert_freesim", new
                            {
                                @country = orderAddress.imsi_country,
                                @crm_login = orderAddress.crm_login,
                                @title = orderAddress.title,
                                @first_name = orderAddress.first_name,
                                @last_name = orderAddress.last_name,
                                @address1 = orderAddress.address1,
                                @address2 = orderAddress.address2,
                                @town = orderAddress.town,
                                @postcode = orderAddress.postcode, 
                                @country_order = orderAddress.country,
                                @email = orderAddress.email,
                                @contact = orderAddress.contact,
                                @payment_ref = orderAddress.payment_ref,
                                @subscription_id = orderAddress.SubscriptionID,
                                @charge = renewal_charge,
                                @ordersim_url = orderAddress.order_url,
                                @id_type = orderAddress.id_type,
                                @id_number = orderAddress.id_number,
                                @dob = orderAddress.date_of_birth,
                                @sitecode = orderAddress.sitecode,
                                @mundio_product = orderAddress.brand,
                                @order_source = orderAddress.order_source,
                                @mobileno = orderAddress.mobileno
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    Log.Debug("InsertSIMOrderRecords: {0}", result.errmsg);
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("Insert2in1Records: {0}", ex.Message);
                return new SimOrder() { errcode = -1, errmsg = ex.Message };
            }
        }

        public static Models.Country2in1Currency Get2in1Currency(string product_code, string country)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Models.Country2in1Currency>(
                            "crm3_2in1_newcountry_charge", new
                            {
                                @product_code = product_code,
                                @country = country
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    Log.Debug("GetCurrency: {0} {1}", result.Currency, result.amount);
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetStatus BL: {0}", ex.Message);
                return new Models.Country2in1Currency() { Currency = "" };
            }
        }
        public static Models.ErrCodeMsg1 Get2in1Currency2(string product_code, string country)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Models.ErrCodeMsg1>(
                            "OTA_Check_Mobileno_Already_Exists_Or_Not", new
                            {
                                @mobileno = product_code

                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    Log.Debug("GetCurrency: {0} {1}");
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetStatus BL: {0}", ex.Message);

                return new Models.ErrCodeMsg1() { };
            }
        }
        public static Models.Country2in1Command Get2in1Currency1(string product_code, string country)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Models.Country2in1Command>(
                            "Generate_OTA_Command", new
                            {
                                @mobileno = product_code,
                                @country = country
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    Log.Debug("GetCurrency: {0} {1}");
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetStatus BL: {0}", ex.Message);
                return new Models.Country2in1Command() { };
            }
        }
        public static Models.Country2in1Command Get2in1CurrencySMS(string product_code, string country)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Models.Country2in1Command>(
                            "Generate_OTA_Command_By_SMS", new
                            {
                                @mobileno = product_code,
                                @country = country
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    Log.Debug("GetCurrency: {0} {1}");
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetStatus BL: {0}", ex.Message);
                return new Models.Country2in1Command() { };
            }
        }
        public static Models.Country2in1Command Get2in1CurrencyvECTONESMS(string product_code, string country)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Models.Country2in1Command>(
                            "Generate_OTA_Command_UF", new
                            {
                                @mobileno = product_code,
                                @country = country
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    Log.Debug("GetCurrency: {0} {1}");
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetStatus BL: {0}", ex.Message);
                return new Models.Country2in1Command() { };
            }
        }

        public static Models.Country2in1Command Get2in1CurrencySMS1(string product_code, string country)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.MCMHLRConnection))
                {
                    conn.Open();

                    var result = conn.Query<Models.Country2in1Command>(
                            "OTA_update_msg_sent_status", new
                            {
                                @mobileno = product_code,
                                @country = country

                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    Log.Debug("GetCurrency: {0} {1}");
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetStatus BL: {0}", ex.Message);
                return new Models.Country2in1Command() { };
            }
        }

        public static Models.Country2in1Command Get2in1Currency3(string product_code, int country)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Models.Country2in1Command>(
                            "HLR_Migration_2in1", new
                            {
                                @Mobileno = product_code,
                                @Customer_id = country
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    Log.Debug("GetCurrency: {0} {1}");
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetStatus BL: {0}", ex.Message);
                return new Models.Country2in1Command() { };
            }
        }
        public static Models.Country2in1Command Get2in1Currency4(string product_code, int country)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Models.Country2in1Command>(
                            "SMS_OTA_HLR_Migration", new
                            {
                                @Mobileno = product_code,
                                @Customer_id = country
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    Log.Debug("GetCurrency: {0} {1}");
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetStatus BL: {0}", ex.Message);
                return new Models.Country2in1Command() { };
            }
        }
        public static Models.Country2in1Command Get2in1CurrencyvECTONE4(string product_code, int country)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Models.Country2in1Command>(
                            "HLR_Migration_UF", new
                            {
                                @Mobileno = product_code,
                                @Customer_id = country
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    Log.Debug("GetCurrency: {0} {1}");
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetStatus BL: {0}", ex.Message);
                return new Models.Country2in1Command() { };
            }
        }



    }
}