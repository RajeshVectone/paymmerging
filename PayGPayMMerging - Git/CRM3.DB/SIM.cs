﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;

namespace CRM_API.DB.SIMInfo
{
    public class SProc
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public static IEnumerable<CRM_API.Models.SIM.SIMInfo> SIMInfo(string mobileno, string sitecode)
        {
            try
            {
                //Old SP:crm3_get_subscriber_detail
                //New SP:crm3_get_subscriber_detail_PO
                /** SIM tab sim satus port out **/

                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.SIM.SIMInfo>(
                            "crm3_get_subscriber_detail_PO", new 
                            {
                                @mobileno = mobileno,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.SIM.SIMInfo>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL SIMInfo: " + ex.Message);
            }
        }
        public static IEnumerable<CRM_API.Models.SIM.PinInfo> SIMPinInfo(string iccid, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.SIM.PinInfo>(
                            "sim_detail_view_by_iccid", new
                            {
                                @iccid = iccid,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.SIM.PinInfo>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL SIMPinInfo: " + ex.Message);
            }
        }
        public static IEnumerable<CRM_API.Models.SIM.Reasoninfo> SIMReasoninfo(string mobile, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.SIM.Reasoninfo>(
                            "CRM3_Get_block_reason_by_mobileno", new
                            {
                                @mobileno = mobile,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.SIM.Reasoninfo>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL SIMPinInfo: " + ex.Message);
            }
        }
        public static IEnumerable<CRM_API.Models.SIM.SIMVersion> SIMVersion(string ICCID, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.SIM.SIMVersion>(
                            "Crm3_Get_Sim_Version_byIccid", new
                            {
                                @iccid = ICCID,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.SIM.SIMVersion>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL SIMPinInfo: " + ex.Message);
            }
        }
        public static IEnumerable<CRM_API.Models.SIM.CallFowardInfo> CallFowardInfo(string iccid, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.SIM.CallFowardInfo>(
                            "crm3_callforward_check", new
                            {
                                @iccid = iccid,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.SIM.CallFowardInfo>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL CallFowardInfo: " + ex.Message);
            }
        }
        public static CRM_API.Models.Common.ErrCodeMsgExt CallFowardUpdate(string iccid, int cfbType, string cfbTo,int cfnryType, string cfnryTo, int cfnrcType, string cfnrcTo, int? cfnrTime, ref string errMsg, ref int dataType, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.Common.ErrCodeMsgExt>(
                            "crm3_crm_callforward_update", new
                            {
                                @iccid = iccid ,
                                @cfwtype1 = cfbType,
                                @forwardToNb1 = cfbTo,
                                @cfwtype2 = cfnryType,
                                @forwardToNb2 = cfnryTo,
                                @cfwtype3 = cfnrcType,
                                @forwardToNb3 = cfnrcTo,
                                @NoReplyTimer = cfnrTime,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    return result ?? new CRM_API.Models.Common.ErrCodeMsgExt();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL CallFowardUpdate: " + ex.Message);
            }
        }
        public static string CallDisabled(string sitecode, string iccid, string ind)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.Common.ErrCodeMsgExt>(
                            "crm3_voicemail_update_mailbox", new
                            {
                                @sitecode = sitecode,
                                @mobileno = iccid,
                                @voicemail_status = ind,
                                
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    return result == null ? "Error" : "Success";
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL CallFowardUpdate: " + ex.Message);
            }
        }
        public static CRM_API.Models.Common.ErrCodeMsgExt ActivateNotReachableSMS(string iccid, string cfwTo, int cfwType, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var p = new DynamicParameters();
                    p.Add("@iccid", iccid);
                    p.Add("@ftn_value", cfwTo);
                    p.Add("@ftn_value", cfwTo);
                    p.Add("@cfwtype",cfwType);
                    p.Add("@sitecode", sitecode);
                    conn.Execute("crm3_crm_smsfw_activate", p, commandType: CommandType.StoredProcedure);

                    return new Models.Common.ErrCodeMsgExt() { err_code = 0, err_message = "Success" };
                }
            }
            catch (Exception ex)
            {
                throw new Exception("CRM_API.DB.SIMInfo.ActivateNotReachableSMS: " + ex.Message);
            }
        }
        public static CRM_API.Models.Common.ErrCodeMsgExt ActivateUnconditionalCallFoward(string iccid, string cfuToNb, string sitecode, int smsFlag=1)
        {
            try
            {
                //old SP:crm3_crm_uncon_cf_sms_activate

                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.Common.ErrCodeMsgExt>(
                            "crm3_crm_uncon_cf_sms_activate_V1", new 
                            {
                                @iccid = iccid,
                                @cfu_forwardToNb = cfuToNb,
                                @sms_fwd_flag = smsFlag,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    return result ?? new CRM_API.Models.Common.ErrCodeMsgExt();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL ActivateUnconditionalCallFoward: " + ex.Message);
            }
        }
        public static CRM_API.Models.Common.ErrCodeMsgExt DeActivateNotReachableSMS(string iccid, int cfwType, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var p = new DynamicParameters();
                    p.Add("@iccid", iccid);
                    p.Add("@smsfwtype", cfwType);
                    p.Add("@sitecode", sitecode);
                    conn.Execute("crm_smsfw_erase", p, commandType: CommandType.StoredProcedure);
                    return new CRM_API.Models.Common.ErrCodeMsgExt() { err_code = 0, err_message = "Success" };
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL DeActivateNotReachableSMS: " + ex.Message);
            }
        }
        //CRMT-262 mobile number validate or not 
        public static CRM_API.Models.Common.ErrCodeMsg Get_Validatemobile( string sitecode,string Mobileno)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.Common.ErrCodeMsg>(
                            "crm3_checksubscriber", new
                            {
                                @sitecode = sitecode,
                                @mobileno = Mobileno
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    return result ?? new CRM_API.Models.Common.ErrCodeMsg();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL ActivateUnconditionalCallFoward: " + ex.Message);
            }
        }
        public static CRM_API.Models.Common.ErrCodeMsgExt DeactivateForwarder(string sICCID, int iCFWType, ref string resultMessage, ref string iDataType,string sitecode)
        {
            var result = new CRM_API.Models.Common.ErrCodeMsgExt();
            string sql = "crm3_crm_callforward_erase";
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    result = conn.Query<CRM_API.Models.Common.ErrCodeMsgExt>(
                            sql, new
                            {
                                @iccid = sICCID,
                                @cfwtype = iCFWType,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    resultMessage = result.err_message;
                    iDataType = result.data_type;
                    result.errcode = result.err_code;
                    return result ?? new CRM_API.Models.Common.ErrCodeMsgExt();
                }
            }
            catch (Exception e)
            {
                string msg = string.Format("CRM_API.DB.SIMInfo.DeactivateForwarder: {0}", e.Message);
                Log.Debug(msg);
                throw new Exception(msg);
            }
        }
        public static CRM_API.Models.Common.ErrCodeMsgExt ActivateForwarded(string sICCID, int iCFWType, string sForwardToNb, ref string resultMessage,string sitecode)
        {
            var result = new CRM_API.Models.Common.ErrCodeMsgExt();
            string sql = "crm3_crm_callforward_activate";
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    result = conn.Query<CRM_API.Models.Common.ErrCodeMsgExt>(
                            sql, new
                            {
                                @iccid = sICCID,
                                @cfwtype = iCFWType,
                                @forwardToNb =sForwardToNb , 
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    resultMessage = result.err_message;
                    result.errcode = result.err_code;
                    return result ?? new CRM_API.Models.Common.ErrCodeMsgExt();
                }
            }
            catch (Exception e)
            {
                string msg = string.Format("CRM_API.DB.SIMInfo.ActivateForwarded: {0}",e.Message);
                Log.Debug(msg);
                throw new Exception(msg);
            }
        }


        public static CRM_API.Models.Common.CancelLocationsNew GetFreeSimByFullnameGroupBycancel(string _iccid, string _sitecode, int _dataType)
        {
            var result = new CRM_API.Models.Common.CancelLocationsNew();
            string sql = "crm_get_hlr_loc_cancel_imsi";
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                     result = conn.Query<CRM_API.Models.Common.CancelLocationsNew>(sql, new
                    {
                        @Iccid = _iccid,
                        @sitecode = _sitecode,
                        @cancel_type = _dataType
                    }, null, true, (60 * 1000), CommandType.StoredProcedure).FirstOrDefault();

                   /* url= result.url;
                    vlr_number = result.vlr_number;
                    imsi_active = result.imsi_active;
                    */
                    return result ?? new  CRM_API.Models.Common.CancelLocationsNew();
                }
            }
            catch (Exception e)
            {
                Log.Error("Cancel Location: " + e.Message);
                throw new Exception("Cancel Location: " + e.Message);
            }
        }
    }
}
