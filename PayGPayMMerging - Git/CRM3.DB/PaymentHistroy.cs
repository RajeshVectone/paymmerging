﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Dapper;
using System.Threading.Tasks;
using NLog;

namespace CRM_API.DB
{
    public class PaymentHistroy
    {
        private readonly static Logger Log;
        static PaymentHistroy()
		{
			PaymentHistroy.Log = LogManager.GetCurrentClassLogger();
		}

        public static IEnumerable<CRM_API.Models.History> getPaymentDetails(string MobileNo, string sitecode)
        {
            
            try
            {
                
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.History>("PaymentGetFilteredPastPayment", new
                    {
                        @mobileno = MobileNo,
                        @sitecode = sitecode
                    }, commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.History>();
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("c3pm_dd_payment_history_byMSISDN  :  " + ex.Message);
            }
           
        }
        //4th stored procedure
        public static IEnumerable<CRM_API.Models.History> GetFilteredPastBundleSub(string mnum, string decision, string paymethod,string fromdate,string todate)
        {
            IEnumerable<CRM_API.Models.History> histories;
            try
            {
                //using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["MVNO"].ConnectionString))
                //{
                using (var sqlConnection = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    //Old SP:PaymentGetFilteredPastPaymentBundle
                    string str = "PaymentGetFilteredPastPaymentBundle_PayG";
                    sqlConnection.Open();
                    int? nullable = null;
                    IEnumerable<CRM_API.Models.History> histories1 = sqlConnection.Query<CRM_API.Models.History>(str, new { mobileno = mnum, payment_decision = decision, payment_method = paymethod, From_Date = fromdate, To_Date = todate }, null, true, nullable, new CommandType?(CommandType.StoredProcedure));
                    object obj = histories1;
                    if (obj == null)
                    {
                        obj = new List<CRM_API.Models.History>();
                    }
                    histories = (IEnumerable<CRM_API.Models.History>)obj;
                }
            }
            catch (Exception exception1)
            {
                Exception exception = exception1;
                PaymentHistroy.Log.Error(string.Concat("DB Error: ", exception.Message));
                throw new Exception(exception.Message);
            }
            return histories;
        }
        //3rd Stored procdure
        public static IEnumerable<CRM_API.Models.History> GetFilteredPastCreditTransfer(string mnum,string fromdate,string todate)
        {
            IEnumerable<CRM_API.Models.History> histories;
            try
            {
                //using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["Payment"].ConnectionString))
                //{
                using (var sqlConnection = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    //old SP:PaymentGetFilteredPastCreditTransfer

                    string str = "PaymentGetFilteredPastCreditTransfer_PayG";
                    sqlConnection.Open();
                    int? nullable = null;
                    IEnumerable<CRM_API.Models.History> histories1 = sqlConnection.Query<CRM_API.Models.History>(str, new { mobileno = mnum, From_Date = fromdate, To_Date = todate }, null, true, nullable, new CommandType?(CommandType.StoredProcedure));
                    object obj = histories1;
                    if (obj == null)
                    {
                        obj = new List<CRM_API.Models.History>();
                    }
                    histories = (IEnumerable<CRM_API.Models.History>)obj;
                }
            }
            catch (Exception exception1)
            {
                Exception exception = exception1;
                PaymentHistroy.Log.Error(string.Concat("DB Error: ", exception.Message));
                throw new Exception(exception.Message);
            }
            return histories;
        }
        //2nd Stored procedure
        public static IEnumerable<CRM_API.Models.History> GetFilteredPastPurchased(string mobile, string fromdate, string todate)
        {
            IEnumerable<CRM_API.Models.History> histories;
            try
            {
                //using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["CRM2"].ConnectionString))
                //{
                using (var sqlConnection = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    //old SP:crm_get_customers_filters_v2
                    string str = "CRM_Get_Customers_Filters_v2_PayG";
                    sqlConnection.Open();
                    int? nullable = null;
                    IEnumerable<CRM_API.Models.History> histories1 = sqlConnection.Query<CRM_API.Models.History>(str, new { mobileno = mobile, From_Date = fromdate, To_Date = todate }, null, true, nullable, new CommandType?(CommandType.StoredProcedure));
                    object obj = histories1;
                    if (obj == null)
                    {
                        obj = new List<CRM_API.Models.History>();
                    }
                    histories = (IEnumerable<CRM_API.Models.History>)obj;
                }
            }
            catch (Exception exception1)
            {
                Exception exception = exception1;
                PaymentHistroy.Log.Error(string.Concat("DB Error: ", exception.Message));
                throw new Exception(exception.Message);
            }
            return histories;
        }
        // 1st Stored procedure
        public static IEnumerable<CRM_API.Models.History> GetFilteredPastTopUp(string mnum, string decision, string paymethod, string paytype,string startdate,string enddate)
        {
            IEnumerable<CRM_API.Models.History> histories;
            try
            {
                //using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["MVNO"].ConnectionString))
                //{
                using (var sqlConnection = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    //old SP:PaymentGetFilteredPastPayment
                    string str = "PaymentGetFilteredPastPayment_PayG ";
                    sqlConnection.Open();
                    int? nullable = null;
                    IEnumerable<CRM_API.Models.History> histories1 = sqlConnection.Query<CRM_API.Models.History>(str, new { mobileno = mnum, payment_decision = decision, payment_method = paymethod, payment_type = paytype,From_Date = startdate,To_Date = enddate }, null, true, nullable, new CommandType?(CommandType.StoredProcedure));
                    object obj = histories1;
                    if (obj == null)
                    {
                        obj = new List<CRM_API.Models.History>();
                    }
                    histories = (IEnumerable<CRM_API.Models.History>)obj;
                }
            }
            catch (Exception exception1)
            {
                Exception exception = exception1;
                PaymentHistroy.Log.Error(string.Concat("DB Error: ", exception.Message));
                throw new Exception(exception.Message);
            }
            return histories;
        }
  
    }
}
