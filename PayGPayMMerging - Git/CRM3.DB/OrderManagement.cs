﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Dapper;
using NLog;

namespace CRM_API.DB
{
    public class OrderManagement
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        //Get the Details
        public static IEnumerable<CRM_API.Models.OrderManagement> SimOrderbyMobile(string MobileNo, string siteCode)
        {
            try
            {
                //Old SP : crm3_simswap_log_getby_mobileno
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.OrderManagement>(
                            "crm3_get_sim_order_log_by_mobileno", new
                            {
                                @mobileno = MobileNo,
                                @sitecode = siteCode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.OrderManagement>();
                }
            }
            catch (SqlException ex)
            {
                Log.Error("CRM_API.DB.OrderManagement.SProc.SimOrderbyMobile: " + ex.Message);
                throw new Exception("CRM_API.DB.OrderManagement.SProc.SimOrderbyMobile: " + ex.Message);
            }
        }

        //

    }
}
