﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;

namespace CRM_API.DB.Sitecode
{
    public class SProc
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public static CRM_API.Models.SitecodeInfo GetSiteInfo(string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.SitecodeInfo>(
                            "crm3_get_plmn", new
                            {
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    return result ?? new CRM_API.Models.SitecodeInfo();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.Sitecode.GetSiteInfo: {0}", ex.Message);
                return new CRM_API.Models.SitecodeInfo(); 
            }
        }

        /// <summary>
        /// Get the SiteCode based on the Product Code / Brand supplied
        /// </summary>
        /// <param name="productCode"></param>
        /// <returns></returns>
        public static string GetSiteCodeByBrand(string productCode)
        {
            string resValue = string.Empty;

            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.SitecodeInfo>(
                            "crm3_get_sitecode", new
                            {
                                @productcode = productCode
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    if (result != null)
                        resValue = result.Sitecode;
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.Sitecode.GetSiteCodeByBrand: {0}", ex.Message);
            }

            return resValue;
        }

        /// <summary>
        /// Get the SiteCode based on the mobileno
        /// </summary>
        /// <param name="mobileNo"></param>
        /// <returns></returns>
        public static string GetSiteCodeByMobileNo(string mobileNo)
        {
            string resValue = string.Empty;

            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.SitecodeInfo>(
                            "crm3_get_sitecode_byprefix", new
                            {
                                @mobile = mobileNo
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    if (result != null)
                        resValue = result.Sitecode;

                    if (string.IsNullOrEmpty(resValue))
                    {
                        if (mobileNo.StartsWith("33"))
                            resValue = "MFR";
                        else if (mobileNo.StartsWith("351"))
                            resValue = "MPT";
                        else if (mobileNo.StartsWith("358"))
                            resValue = "MFI";
                        else
                            resValue = "MCM";
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.Sitecode.GetSiteCodeByMobileNo: {0}", ex.Message);
            }

            return resValue;
        }
        public static string GetPrefixMobilenoBySitecode (string sitecode)
        {
            string sql = "crm3_get_prefix_bysitecode";
            string result = string.Empty;
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection)){
                    conn.Open();
                    var p = new DynamicParameters();
                    p.Add("@sitecode",sitecode,DbType.String,ParameterDirection.Input,3);
                    p.Add("@prefix",string.Empty,DbType.String, ParameterDirection.Output,5);
                    var count = conn.Execute(sql, p, null, 300, CommandType.StoredProcedure);

                    result= p.Get<string>("@prefix");
                }
                return result;
            }
            catch (Exception e)
            {
                Log.Error("CRM_API.DB.Sitecode.GetPrefixMobilenoBySitecode: {0}", e.Message);
                throw new Exception("CRM_API.DB.Sitecode.GetPrefixMobilenoBySitecode: " + e.Message);
            }
        }
    }
}
