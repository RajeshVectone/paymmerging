﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;

namespace CRM_API.DB.Auth
{
    public class SProc
    {
        public static LoginMsg Login(string login, string password, string countryCode)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.UserMgtConnection))
                {
                    conn.Open();


                    var result = conn.Query<LoginMsg>("crm3_auth_login", new

                            {
                                @user_login = login,
                                @user_password = password
                                //@site_code = countryCode
                            }, commandType: CommandType.StoredProcedure);

                    return result.ElementAt(0);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL Login: " + ex.Message);
            }
        }
        public static LoginMsg Login(string login, string password)
        {
            /*old SP =
            crm3_auth_login_without_sitecode
             */
            try
            {
                //Moorthy : Modified for PAYM & PAYG Merging
                //using (var conn = new SqlConnection(CRM_API.Helpers.Config.UserMgtConnection))
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    //var result = conn.Query<LoginMsg>(
                    //        "crm3_auth_login_without_sitecode_v2", new
                    //        {
                    //            @user_login = login,
                    //            @user_password = password
                    //        }, commandType: CommandType.StoredProcedure);

                    var result = conn.Query<LoginMsg>("crm3_user_login", new
                            {
                                @user_name = login,
                                @password = password
                            }, commandType: CommandType.StoredProcedure);

                    return result.ElementAt(0);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL Login: " + ex.Message);
            }
        }

        public static Common.ErrCodeMsg Logoff(string token)
        {
            try
            {
                //using (var conn = new SqlConnection(CRM_API.Helpers.Config.UserMgtConnection))
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Common.ErrCodeMsg>(
                            "crm3_auth_logoff", new
                            {
                                @token = token
                            }, commandType: CommandType.StoredProcedure);

                    return Common.Helper.Coalese(result);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL Logoff: " + ex.Message);
            }
        }

        public static UserByToken GetUserIDByToken(string token)
        {
            try
            {
                //Moorthy : Modified for PAYM & PAYG Merging
                //using (var conn = new SqlConnection(CRM_API.Helpers.Config.UserMgtConnection))
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    //var result = conn.Query<UserByToken>(
                    //        "crm3_auth_get_user_by_token", new
                    //        {
                    //            @token = token
                    //        }, commandType: CommandType.StoredProcedure);

                    var result = conn.Query<UserByToken>(
                            "crm3_get_user_by_token", new
                            {
                                @token = token
                            }, commandType: CommandType.StoredProcedure);

                    if (result == null)
                        throw new Exception("Failed authenticating user!");

                    if (result.Count() != 1)
                        throw new Exception("Failed authenticating user!");

                    return result.First();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL GetUserIDByToken: " + ex.Message);
            }
        }

        public static IEnumerable<RoleByUser> GetRolesByUserID(int userID)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.UserMgtConnection))
                {
                    conn.Open();

                    var result = conn.Query<RoleByUser>(
                            "crm3_auth_get_roles_by_user_id", new
                            {
                                @user_id = userID
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<RoleByUser>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL GetRolesByUserID: " + ex.Message);
            }
        }
        public static IEnumerable<permission> GetPermission(int userID)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<permission>(
                            "Crm3_get_permission_by_userid", new
                            {
                                @user_id = userID
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<permission>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL GetRolesByUserID: " + ex.Message);
            }
        }
        public static IEnumerable<CountryByUser> GetCountriesByUserID(int userID)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.UserMgtConnection))
                {
                    conn.Open();

                    var result = conn.Query<CountryByUser>(
                            "crm3_auth_get_user_site_codes", new
                            {
                                @user_id = userID
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CountryByUser>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL GetCountriesByUserID: " + ex.Message);
            }
        }

        public static Common.ErrCodeMsg SetTokenCountry(string token, string countryCode)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.UserMgtConnection))
                {
                    conn.Open();

                    var result = conn.Query<Common.ErrCodeMsg>(
                            "crm3_auth_set_token_site_code", new
                            {
                                @token = token,
                                @country_code = countryCode
                            }, commandType: CommandType.StoredProcedure);

                    return Common.Helper.Coalese(result);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL SetTokenCountry: " + ex.Message);
            }
        }

        public static SessionInfo GetSessionInfo(string token)
        {
            // old sp :crm3_auth_get_session_info
            try
            {
                //Moorthy : Modified for PAYM & PAYG Merging
                //using (var conn = new SqlConnection(CRM_API.Helpers.Config.UserMgtConnection))
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    //var result = conn.Query<SessionInfo>(
                    //        "crm3_auth_get_session_info_v2", new
                    //        {
                    //            @token = token
                    //        }, commandType: CommandType.StoredProcedure);

                    var result = conn.Query<SessionInfo>(
                           "crm3_get_session_info_by_token", new
                           {
                               @token = token
                           }, commandType: CommandType.StoredProcedure);

                    if (result == null)
                        throw new Exception("Failed authenticating user!");

                    if (result.Count() != 1)
                        throw new Exception("Failed authenticating user!");

                    return result.First();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL GetSessionInfo: " + ex.Message);
            }
        }
    }

}
