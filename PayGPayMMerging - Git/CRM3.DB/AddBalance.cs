﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;

namespace CRM_API.DB
{
    public class AddBalance
    {
        //AddBalance
        public static AddBalanceOutput UpdateBalance(AddBalanceInput input)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<AddBalanceOutput>(
                            "crm_update_customer_balance", new
                            {
                                @sitecode = input.sitecode,
                                @mobileno = input.mobileno,
                                @username = input.username,
                                @amount = input.amount,
                                @calledby = input.calledby,
                                @balance_type = input.balance_type,
                                @bundleid = input.bundleid,
                                @daysvalid = input.daysvalid
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    return result ?? new AddBalanceOutput();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL UpdateBalance: " + ex.Message);
            }
        }

        //AddBalanceReport
        public static IEnumerable<AddBalanceReportOutput> GetAddBalanceReport(AddBalanceReportInput req)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<AddBalanceReportOutput>(
                            "crm_get_updated_balance_report", new
                            {
                                sitecode = req.sitecode,
                                calledby = req.calledby,
                                date_from = req.date_from,
                                date_to = req.date_to
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<AddBalanceReportOutput>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL GetAddBalanceReport: " + ex.Message);
            }
        }
        public static AddBalanceOutput DoTopUpbypayment(AddBalanceLTPInput req)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<AddBalanceOutput>(
                            "crm_do_topup_by_payment_ref", new
                            {
                                sitecode = req.sitecode,
                                mobileno = req.mobileno,
                                paymentref = req.paymentref,
                                amount = req.amount,
                                calledby = req.calledby,
                                crm_user = req.crm_user,
                                comments = req.comments
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    return result ?? new AddBalanceOutput();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL DoTopUpbypayment: " + ex.Message);
            }
        }

        public static AddBalanceOutput AddbalanceManual(AddBalancemanualInput req)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<AddBalanceOutput>(
                            "crm_add_credit_by_manual", new
                            {
                                sitecode = req.sitecode,
                                mobileno = req.mobileno,
                                comments = req.comments,
                                refund_by = req.refund_by,
                                amount = req.amount,
                                crm_user = req.crm_user
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    return result ?? new AddBalanceOutput();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL AddbalanceManual: " + ex.Message);
            }
        }
    }
}
