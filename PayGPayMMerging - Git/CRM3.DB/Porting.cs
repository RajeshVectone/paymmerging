﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.DB.Porting
{
    public class SProc
    {
        public static string AddCountryCode(string msisdn, string sitecode)
        {
            // example input from crm : 07892170022
            // example input for hlrdb: 447892170022

            string prefix = "44"; // country code for UK

            if (msisdn.StartsWith("0"))
                msisdn = prefix + msisdn.Substring(1, msisdn.Length - 1);
            else if (!msisdn.StartsWith(prefix))
                msisdn = prefix + msisdn;

            return msisdn;
        }
        public static bool BrandIsUk1(string msisdn, string sitecode, ref int? isUK1)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var p = new DynamicParameters();
                    p.Add("@Msisdn", msisdn);
                    p.Add("@sitecode", sitecode);
                    p.Add("@is_uk1", 0, DbType.Int32, ParameterDirection.Output, int.MaxValue);
                    p.Add("@err_code", 0, DbType.Int32, ParameterDirection.Output, int.MaxValue);
                    p.Add("@err_msg", "", DbType.String, ParameterDirection.Output, 100);
                    conn.Execute("crm3_brand_is_uk1", p, commandType: CommandType.StoredProcedure);
                    var result = p.Get<int?>("@is_uk1");
                    if (result == null)
                    {
                        isUK1 = -1;
                        return false;
                    }
                    if (result == 1)
                    {
                        isUK1 = result;
                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL SetBrandUK1: " + ex.Message);
            }
        }

        public static Models.Porting.ExistingRequest CheckExistingRequest(string pac, string msisdn, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Models.Porting.ExistingRequest>(
                            "crm3_SP_CHECK_REQUEST", new
                            {
                                @pac = pac,
                                @msisdn = msisdn,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    return result;
                }
            }
            catch (Exception ex)
            {
                Models.Porting.ExistingRequest result = new Models.Porting.ExistingRequest();
                result.errcode = -1;
                result.errmsg = ex.Message;
                return result;
            }

        }

        public static void CheckRequestExist(string pac, string msisdn)
        {

        }
        public static int? GetBrand(string msisdn, string sitecode, ref string iccid)
        {
            try
            {
                msisdn = AddCountryCode(msisdn, sitecode);
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var p = new DynamicParameters();
                    p.Add("@msisdn", msisdn);
                    p.Add("@sitecode", sitecode);
                    p.Add("@iccid", "", DbType.String, ParameterDirection.Output, 20);
                    p.Add("@brand_type", "", DbType.Int32, ParameterDirection.Output, int.MaxValue);
                    p.Add("@errcode", "", DbType.Int32, ParameterDirection.Output, int.MaxValue);
                    p.Add("@errmsg", "", DbType.String, ParameterDirection.Output, 100);
                    conn.Execute("crm3_brand_differentiation", p, commandType: CommandType.StoredProcedure);
                    iccid = p.Get<string>("@iccid");
                    var brandtype = p.Get<int?>("@brand_type");
                    return (brandtype == null ? -1 : brandtype);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL Error during BrandDifferentiation: " + ex.Message);
            }
        }
        public static Common.ErrCodeMsg GetMsisdnValidation(string msisdn, string sitecode)
        {
            try
            {
                var result = new Common.ErrCodeMsg() { errcode = 0, errmsg = "Valid" };
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var p = new DynamicParameters();
                    p.Add("@bb_msisdn ", msisdn);
                    p.Add("@sitecode ", sitecode);
                    p.Add("@errcode", "", DbType.Int32, ParameterDirection.Output, int.MaxValue);
                    p.Add("@errmsg", "", DbType.String, ParameterDirection.Output, 100);
                    conn.Execute("crm3_sp_check_msisdn_active", p, commandType: CommandType.StoredProcedure);
                    result = new Common.ErrCodeMsg()
                    {
                        errcode = p.Get<int>("@errcode"),
                        errmsg = p.Get<string>("@errmsg")
                    };

                }
                if (result.errcode == 0)
                    result.errmsg = "Msisdn Valid and Active Data Can be processed for port in.";
                else
                    result.errmsg += " And Request cannot be processed.";
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception("BL MsisdnValidate: " + ex.Message);
            }
        }

        public static CRM_API.Models.Porting.PortinSMS GetPortInSmsById(int smsId, string sitecode)
        {
            try
            {
                var result = new Models.Porting.PortinSMS();
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    result = conn.Query<Models.Porting.PortinSMS>(
                            "crm3_SP_GET_PORTIN_SMS_BY_ID", new
                            {
                                @sms_id = smsId,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    //return result ?? new List<Models.Porting.PortinSMS>().FirstOrDefault();
                }
                if (result != null)
                {
                    var resx = GetMsisdnValidation(result.BB_Msisdn, sitecode);
                    result.Validate_Message = resx.errmsg;
                    result.Validate_Code = resx.errcode;
                }
                else
                {
                    result.Err_Message = "Detail Not Available";
                    result.Validate_Code = -1;
                }

                return result ?? new List<Models.Porting.PortinSMS>().FirstOrDefault(); ;
            }
            catch (Exception ex)
            {
                throw new Exception("BL GetPortInSmsById: " + ex.Message);
            }
        }

        public static CRM_API.Models.Porting.PortinSMS GetPortInSmsByMsisdn(string msisdn, string pac, string sitecode)
        {
            try
            {
                var result = new Models.Porting.PortinSMS();
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    result = conn.Query<Models.Porting.PortinSMS>(
                            "crm3_SP_GET_PORTIN_SMS_BY_MSISDN", new
                            {
                                @msisdn = msisdn,
                                @pac = pac,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    //return result ?? new List<Models.Porting.PortinSMS>().FirstOrDefault();
                }
                if (result != null)
                {
                    var resx = GetMsisdnValidation(result.BB_Msisdn, sitecode);
                    result.Validate_Message = resx.errmsg;
                    result.Validate_Code = resx.errcode;
                }
                else
                {
                    result.Err_Message = "Detail Not Available";
                    result.Validate_Code = -1;
                }

                return result ?? new List<Models.Porting.PortinSMS>().FirstOrDefault(); ;
            }
            catch (Exception ex)
            {
                throw new Exception("BL GetPortInSmsByMsisdn: " + ex.Message);
            }
        }

        public static string GetRequestCode(string Sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var p = new DynamicParameters();
                    p.Add("@sitecode", Sitecode);
                    p.Add("@ref_code", "", DbType.String, ParameterDirection.Output, 10);
                    conn.Execute("crm3_SP_GET_REF_CODE", p, commandType: CommandType.StoredProcedure);
                    return p.Get<string>("@ref_code");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL GetRequestCode: " + ex.Message);
            }
        }
        public static string GetSMSContent(string contentKey, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var p = new DynamicParameters();
                    p.Add("@content_key", contentKey);
                    p.Add("@sitecode", sitecode);
                    p.Add("@content", "", DbType.String, ParameterDirection.Output, 500);
                    conn.Execute("crm3_sp_get_sms_content", p, commandType: CommandType.StoredProcedure);
                    return p.Get<string>("@content");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL GetSMSContent: " + ex.Message);
            }
        }
        public static Common.ErrCodeMsg MsisdnValidate(string msisdn, string lastdigit_iccid, string sitecode)
        {
            try
            {
                var result = new Common.ErrCodeMsg() { errcode = 0, errmsg = "Valid" };
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var p = new DynamicParameters();
                    p.Add("@bb_msisdn ", msisdn);
                    p.Add("@sitecode ", sitecode);
                    p.Add("@errcode", "", DbType.Int32, ParameterDirection.Output, int.MaxValue);
                    p.Add("@errmsg", "", DbType.String, ParameterDirection.Output, 100);
                    conn.Execute("crm3_sp_check_msisdn_active", p, commandType: CommandType.StoredProcedure);
                    result = new Common.ErrCodeMsg()
                    {
                        errcode = p.Get<int>("@errcode"),
                        errmsg = p.Get<string>("@errmsg")
                    };

                }
                if (result.errcode != -1)
                {
                    var _resAccount = CRM_API.DB.ESP.SProc.GetESPAccountInfo(msisdn, sitecode).FirstOrDefault();
                    if (lastdigit_iccid != _resAccount.ICCID.Substring(_resAccount.ICCID.Trim().Length - 4).Trim())
                    {
                        result.errmsg = "Iccid Not Match";
                        result.errcode = -1;
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception("BL MsisdnValidate: " + ex.Message);
            }
        }
        public static IEnumerable<CRM_API.Models.Porting.PortinSMS> ReportPortInSms(int filter_option, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Models.Porting.PortinSMS>(
                            "crm3_SP_GET_PORTIN_SMS", new
                            {
                                @getUnprocessedOnly = filter_option,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<Models.Porting.PortinSMS>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL ReportPortInSms: " + ex.Message);
            }
        }

        public static async Task<IEnumerable<CRM_API.Models.Porting.PortinSMS>> ReportPortInSms_Async(int filter_option, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var result = Task.Run<IEnumerable<CRM_API.Models.Porting.PortinSMS>>(() =>
                    {
                        var resx = conn.Query<Models.Porting.PortinSMS>(
                            "crm3_SP_GET_PORTIN_SMS", new
                            {
                                @getUnprocessedOnly = filter_option,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure);
                        return resx;
                    });
                    return await result ?? new List<Models.Porting.PortinSMS>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL ReportPortInSms: " + ex.Message);
            }
        }

        public static void SaveCancelEntryResp(string pac, string msisdn, int is_all, int goodcount, int badcount, string errmsg, string sitecode)
        {
            string sql = "crm3_SP_CANCEL_ENTRY";
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    //var result = 
                    var p = new DynamicParameters();
                    p.Add("@Pac", pac);
                    p.Add("@Msisdn", msisdn);
                    p.Add("@is_all", is_all);
                    p.Add("@good_count", goodcount);
                    p.Add("@bad_count", badcount);
                    p.Add("@err_msg", errmsg);
                    p.Add("@sitecode", sitecode);
                    conn.Execute(sql, p, null, null, CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL SaveCancelEntryResp: " + ex.Message);
            }
        }

        public static Models.Porting.CancelRequest SaveCancelRequestResp(Models.Porting.Syniverse.CancelRequest reqObj, CRM_API.Models.Porting.Syniverse.CancelRequestResp respObj, string sitecode)
        {
            try
            {
                Models.Porting.CancelRequest result = new Models.Porting.CancelRequest();

                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var p = new DynamicParameters();
                    p.Add("@pac", reqObj.EntryKey.Pac);
                    p.Add("@msisdn", reqObj.EntryKey.Msisdn);
                    p.Add("@is_all", reqObj.CancelAllInPac ? 1 : 0);
                    p.Add("@good_count", respObj.MultipleEntryResult.ResultCount.GoodCount);
                    p.Add("@bad_count", respObj.MultipleEntryResult.ResultCount.BadCount);
                    p.Add("@cancel_req_id", "", DbType.Int32, ParameterDirection.Output, 0);
                    p.Add("@err_msg", "", DbType.String, ParameterDirection.Output, 128);
                    p.Add("@sitecode", sitecode);

                    conn.Execute("crm3_SP_CANCEL_REQUEST", p, commandType: CommandType.StoredProcedure);

                    result.cancel_req_id = p.Get<int>("@cancel_req_id");
                    result.err_msg = p.Get<String>("@err_msg");
                }

                foreach (Models.Porting.Syniverse.EntryResult erObj in respObj.MultipleEntryResult.EntryResultList.EntryResult)
                {
                    using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                    {
                        var p = new DynamicParameters();
                        p.Add("@cancel_req_id", result.cancel_req_id);
                        p.Add("@primary_msisdn", reqObj.EntryKey.Msisdn); // the primary is the entry key input
                        p.Add("@secondary_msisdn", erObj.Msisdn); // the secondary is all the cancelled pac (cancelAll is true)
                        p.Add("@error_code", 0);
                        p.Add("@sitecode", sitecode);

                        conn.Execute("crm3_SP_INSERT_CANCELED_MSISDN", p, commandType: CommandType.StoredProcedure);
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                throw new Exception("BL CancelRequest: " + ex.Message);
            }
        }

        public static Common.ErrCodeMsg SaveReviseRequestResp(Models.Porting.Syniverse.ReviseRequest reqObj, CRM_API.Models.Porting.Syniverse.ReviseRequestResp respObj, string sitecode)
        {
            try
            {
                var result = new Common.ErrCodeMsg() { errcode = 0, errmsg = "Valid" };
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var p = new DynamicParameters();
                    p.Add("@pac", reqObj.EntryKey.Pac);
                    p.Add("@msisdn", reqObj.EntryKey.Msisdn);
                    p.Add("@is_suggested_date", reqObj.UseSuggestedDateOnConflict ? 1 : 0);
                    p.Add("@is_warning", respObj.PortDateWarning ? 1 : 0);
                    p.Add("@port_date", respObj.PortDate);
                    p.Add("@error_code", 0);

                    // optional parameter
                    string accNo, refString, spId, contactName, contactDetails;
                    accNo = refString = spId = contactName = contactDetails = string.Empty;

                    p.Add("@acc_no", accNo == string.Empty ? null : accNo);
                    p.Add("@ref", refString == string.Empty ? null : refString);
                    p.Add("@sp_id", spId == string.Empty ? null : spId);
                    p.Add("@contact_name", contactName == string.Empty ? null : contactName);
                    p.Add("@contact_details", contactDetails == string.Empty ? null : contactDetails);
                    p.Add("@err_msg", "", DbType.String, ParameterDirection.Output, 128);
                    p.Add("@sitecode", sitecode);
                    conn.Execute("crm3_SP_REVISE_REQUEST", p, commandType: CommandType.StoredProcedure);
                    result = new Common.ErrCodeMsg()
                    {
                        errcode = 0,
                        errmsg = p.Get<string>("@errmsg")
                    };

                }
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception("BL ReviseRequest: " + ex.Message);
            }
        }

        public static void SaveSubmitRequestResp
            (string pac, string msisdn, string bb_msisdn, int is_suggested_date, int is_warning,
            DateTime port_date, string dno, int errcode, string acc_no, string referer, string sp_id,
            string contact_name, string contact_detail, int request_source, string errmsg, string sitecode)
        {
            string sql = "CRM3_SP_SUBMIT_REQUEST";
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var p = new DynamicParameters();
                    p.Add("@Pac", pac);
                    p.Add("@Msisdn", msisdn);
                    p.Add("@bb_msisdn", bb_msisdn);
                    p.Add("@is_suggested_date", is_suggested_date);
                    p.Add("@is_warning", is_warning);
                    p.Add("@port_date", port_date);
                    p.Add("@dno", dno);
                    p.Add("@error_code", errcode);
                    p.Add("@acc_no", acc_no);
                    p.Add("@ref", referer);
                    p.Add("@sp_id", sp_id);
                    p.Add("@contact_name", contact_name);
                    p.Add("@contact_details", contact_detail);
                    p.Add("@request_source", request_source);
                    p.Add("@sitecode", sitecode);
                    p.Add("@err_msg", errmsg, DbType.String, ParameterDirection.Output, 128);
                    conn.Execute(sql, p, null, null, CommandType.StoredProcedure);
                    var result = p.Get<string>("@err_msg");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL SaveSubmitRequestResp: " + ex.Message);
            }
        }
        public static void SaveTransactionLog(string pac, string msisdn, string trxname, int errcode,
            string dno, string rno, string ono, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    //var result = 
                    var p = new DynamicParameters();
                    p.Add("@Pac", pac, DbType.String, ParameterDirection.Input, 32);
                    p.Add("@Msisdn", msisdn, DbType.String, ParameterDirection.Input, 24);
                    p.Add("@TransactionName", trxname, DbType.String, ParameterDirection.Input, 30);
                    p.Add("@ErrorCode", errcode, DbType.Int32, ParameterDirection.Input, int.MaxValue);
                    p.Add("@Dno", dno, DbType.String, ParameterDirection.Input, 2);
                    p.Add("@Rno", rno, DbType.String, ParameterDirection.Input, 2);
                    p.Add("@Ono", ono, DbType.String, ParameterDirection.Input, 2);
                    p.Add("@sitecode", sitecode, DbType.String, ParameterDirection.Input, 3);
                    conn.Execute("crm3_SP_INSERT_TRANSACTION_LOG", p, null, null, CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL SaveTransactionLog: " + ex.Message);
            }
        }
        public static Common.ErrCodeMsg UpdatePortInSmsStatus(int smsId, int status, string sitecode)
        {
            string sql = "crm3_SP_UPDATE_PORTIN_SMS";
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var result = new Common.ErrCodeMsg() { errcode = -1, errmsg = "Default" };
                    var p = new DynamicParameters();
                    p.Add("@sms_id", smsId);
                    p.Add("@status", status);
                    p.Add("@sitecode", sitecode);

                    var resx = conn.Execute(sql, p, null, null, CommandType.StoredProcedure);
                    if (resx == 0)
                    {
                        result.errcode = -1;
                        result.errmsg = "Update Failed";
                    }
                    else
                    {
                        result.errcode = 0;
                        result.errmsg = "Update Success";
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL GetPortInSmsById: " + ex.Message);
            }
        }
    }
}
