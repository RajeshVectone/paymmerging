﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;

namespace CRM_API.DB
{
    public class TransferBalance
    {
        public static TransferBalanceOutput UpdateBalance(TransferBalanceInput input)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<TransferBalanceOutput>(
                            "crm_balance_transfer_process", new
                            {
                                @sitecode = input.sitecode,
                                @donor_mobileno = input.donor_mobileno,
                                @receiver_mobileno = input.receiver_mobileno,
                                @amount = input.amount,
                                @username = input.username
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    return result ?? new TransferBalanceOutput();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BLTransfer UpdateBalance: " + ex.Message);
            }
        }

        //TransferBalanceReport
        public static IEnumerable<TransferBalanceReportOutput> GetTransferBalanceReport(TransferBalanceReportInput req)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<TransferBalanceReportOutput>(
                            "crm_balance_transfer_report", new
                            {
                                sitecode = req.sitecode,
                                date_from = req.date_from,
                                date_to = req.date_to
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<TransferBalanceReportOutput>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL GetTransferBalanceReport: " + ex.Message);
            }
        }
    }
}
