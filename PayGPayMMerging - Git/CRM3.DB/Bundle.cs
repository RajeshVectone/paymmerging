﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;

namespace CRM_API.DB.Bundle
{
    public class SProc
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public static IEnumerable<CRM_API.Models.Bundle.SubscribedBundle> SubscribedBundle(string mobileno, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.Bundle.SubscribedBundle>(
                            "crm3_get_list_subscribed_bundle_v2", new
                            {
                                @mobileno = mobileno,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.Bundle.SubscribedBundle>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL SubscribedBundle: " + ex.Message);
            }
        }
        //added by karthik CRMT-196 Internet Profile
        public static IEnumerable<CRM_API.Models.Bundle.internetprofile> internetprofile(string mobileno, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.Bundle.internetprofile>(
                            "crm3_InternetProfile_cust_info", new
                            {
                                @sitecode = sitecode,
                                @mobileno = mobileno

                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.Bundle.internetprofile>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL InternetProfile: " + ex.Message);
            }
        }
        public static IEnumerable<CRM_API.Models.Bundle.internetprofile> GetGPRSSettingsMsg(string sitecode, string mobileno)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.Bundle.internetprofile>(
                            "CRM3_GPRS_Settings", new
                            {
                                @sitecode = sitecode,
                                @mobileno = mobileno

                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.Bundle.internetprofile>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL InternetProfile: " + ex.Message);
            }
        }
        public static IEnumerable<CRM_API.Models.Bundle.AvailableBundle> AvailableBundle(float balance, string tariffclass, string bundle_name, string sitecode)
        {
            try
            {
                /**Resion :Payg relevant avialble data display. **/
                //Old SP: crm3_get_list_available_bundle

                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.Bundle.AvailableBundle>(
                            "crm3_get_list_available_bundle_payg", new
                            {
                                @balance = balance,
                                @tariffclass = tariffclass,
                                @bundle_name = string.IsNullOrEmpty(bundle_name) ? "" : bundle_name,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.Bundle.AvailableBundle>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL AvailableBundle: " + ex.Message);
            }
        }

        public static Common.ErrCodeMsg SubscribeBundle(string sitecode, int usertype, string mobileno, int bundleid, int paymode, string processby, string pack_dest)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    Common.ErrCodeMsg resvalue = new Common.ErrCodeMsg();

                    var p = new DynamicParameters();
                    p.Add("@sitecode", sitecode);
                    p.Add("@usertype", usertype);
                    p.Add("@userinfo", mobileno);
                    p.Add("@bundleid", bundleid);
                    p.Add("@paymode", paymode);
                    p.Add("@processby", processby);
                    p.Add("@pack_dest", pack_dest);
                    p.Add("@errcode", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    p.Add("@errmsg", dbType: DbType.String, size: 200, direction: ParameterDirection.Output);
                    conn.Execute("crm3_bundle_subscribe", p, commandType: CommandType.StoredProcedure);
                    resvalue.errcode = p.Get<int>("@errcode");
                    resvalue.errmsg = p.Get<string>("@errmsg");
                    return resvalue;
                }
            }
            catch (Exception ex)
            {
                return new Common.ErrCodeMsg() { errcode = -1, errmsg = "BL SubscribeBundle:" + ex.Message };
            }
        }

        public static Common.ErrCodeMsg UnsubscribeBundleExpiry(string sitecode, int usertype, string mobileno, int bundleid, string processby)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    Common.ErrCodeMsg resvalue = new Common.ErrCodeMsg();

                    var p = new DynamicParameters();
                    p.Add("@sitecode", sitecode);
                    p.Add("@usertype", usertype);
                    p.Add("@userinfo", mobileno);
                    p.Add("@bundleid", bundleid);
                    p.Add("@processby", processby);
                    p.Add("@errcode", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    p.Add("@errmsg", dbType: DbType.String, size: 100, direction: ParameterDirection.Output);

                    conn.Execute("crm3_bundle_unsubscribe", p, commandType: CommandType.StoredProcedure);

                    resvalue.errcode = p.Get<int>("@errcode");
                    resvalue.errmsg = p.Get<string>("@errmsg");
                    return resvalue;
                }
            }
            catch (Exception ex)
            {
                return new Common.ErrCodeMsg() { errcode = -1, errmsg = "BL UnsubscribeBundleExpiry:" + ex.Message };
            }
        }

        public static Common.ErrCodeMsg UnsubscribeBundleImmediately(string sitecode, int usertype, string mobileno, int bundleid, string processby)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    Common.ErrCodeMsg resvalue = new Common.ErrCodeMsg();

                    var p = new DynamicParameters();
                    p.Add("@sitecode", sitecode);
                    p.Add("@usertype", usertype);
                    p.Add("@userinfo", mobileno);
                    p.Add("@bundleid", bundleid);
                    p.Add("@processby", processby);
                    p.Add("@errcode", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    p.Add("@errmsg", dbType: DbType.String, size: 100, direction: ParameterDirection.Output);

                    conn.Execute("crm3_bundle_cancel", p, commandType: CommandType.StoredProcedure);

                    resvalue.errcode = p.Get<int>("@errcode");
                    resvalue.errmsg = p.Get<string>("@errmsg");
                    return resvalue;
                }
            }
            catch (Exception ex)
            {
                return new Common.ErrCodeMsg() { errcode = -1, errmsg = "BL UnsubscribeBundleImmediately:" + ex.Message };
            }
        }

        public static Common.GetSmsIwmscUrlOutput GetSMSUrl(string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.DB.Common.GetSmsIwmscUrlOutput>(
                            "website_central_get_sms_iwmsc_url", new
                            {
                                @sitecode = sitecode,
                                @data_activation = 1
                            }, commandType: CommandType.StoredProcedure).SingleOrDefault();

                    return result ?? new CRM_API.DB.Common.GetSmsIwmscUrlOutput();
                }
            }
            catch (Exception ex)
            {
                return new Common.GetSmsIwmscUrlOutput() { errcode = -1, errmsg = "BL GetSMSUrl:" + ex.Message };
            }
        }

        //Bundle Name click
        //CRM Enhancement 2
        public static IEnumerable<CRM_API.Models.Bundle.LimitedMinInfoByBundleIdRef> GetLimitedMinInfoByBundleId(string sitecode, int config_id, string mobileno, string tariffclass, string startdate)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.Bundle.LimitedMinInfoByBundleIdRef>(
                            "crm3_limited_min_info_by_bundleid_v2 ", new
                            {
                                @sitecode = sitecode,
                                @config_id = config_id,
                                @mobileno = mobileno,
                                @tariffclass = tariffclass,
                                @startdate = startdate
                            //"crm3_limited_min_info_by_bundleid", new
                            //{
                            //    @sitecode = sitecode,
                            //    @config_id = config_id
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.Bundle.LimitedMinInfoByBundleIdRef>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("BL GetLimitedMinInfoByBundleId: " + ex.Message);
                return new List<CRM_API.Models.Bundle.LimitedMinInfoByBundleIdRef>() { new CRM_API.Models.Bundle.LimitedMinInfoByBundleIdRef() { errmsg = ex.Message } };
                //throw new Exception("BL GetLimitedMinInfoByBundleId: " + ex.Message);
            }
        }

        //27-Dec-2018 : Moorthy : Exchange Bundles
        public static IEnumerable<CRM_API.Models.Bundle.ExchangeBundleRef> ExchangeBundle(Models.Bundle.BundleSearch resBundle)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.Bundle.ExchangeBundleRef>(
                            "crm_bundle_exchange_process", new
                            {
                                @sitecode = resBundle.sitecode,
                                @mobileno = resBundle.msisdn,
                                @current_bundleid = resBundle.current_bundleid,
                                @new_bundleid = resBundle.new_bundleid,
                                @paymode = resBundle.paymode,
                                @process_flag = resBundle.process_flag,
                                @processby = resBundle.processby,
                                @crm_user = resBundle.crm_user
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.Bundle.ExchangeBundleRef>() { new CRM_API.Models.Bundle.ExchangeBundleRef { errcode = 0, errmsg = "Failure" } };
                }
            }
            catch (Exception ex)
            {
                Log.Error("BL ExchangeBundle: " + ex.Message);
                throw new Exception("BL ExchangeBundle: " + ex.Message);
            }
        }
    }
}
