﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;

namespace CRM_API.DB.FreeSIMOrder
{
    public class SProc
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        #region PlaceOrder
        public static IEnumerable<FreeSIMOrderResponse> PlaceOrder(FreeSIMOrderInfo Info)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<FreeSIMOrderResponse>(
                    "crm_create_freesim_order", new
                    {
                        @sitecode = Info.sitecode,
                        @firstname = Info.firstname,
                        @lastname = Info.lastname,
                        @houseno = Info.houseno,
                        @address1 = Info.address1,
                        @address2 = Info.address2,
                        @city = Info.city,
                        @postcode = Info.postcode,
                        @state = Info.state,
                        @mobilephone = Info.mobilephone,
                        @email = Info.email,
                        @subscriberchannel = Info.subscriberchannel,
                        @sourceaddress = Info.sourceaddress,
                        @ordersimurl = Info.ordersimurl,
                        @ip_address = Info.ip_address,
                        @nb_of_sim = Info.nb_of_sim
                    }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<FreeSIMOrderResponse>() { new FreeSIMOrderResponse { errcode = -1, errmsg = "Failed" } };
                }
            }
            catch (Exception ex)
            {
                Log.Info("BL PlaceOrder: " + ex.Message);
                return new List<FreeSIMOrderResponse>() { new FreeSIMOrderResponse { errcode = -1, errmsg = ex.Message } };
            }
        }
        #endregion
    }
}