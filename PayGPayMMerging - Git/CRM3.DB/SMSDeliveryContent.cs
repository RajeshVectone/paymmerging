﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.DB
{
    public class SMSDeliveryContent
    {
        public static bool SMS_BundleCancelService(string mobileNo, int Type, string Bundle)
        {
            try
            {
                string SMSContent = "";
                DateTime today = DateTime.Today;
                DateTime endOfMonth = new DateTime(today.Year, today.Month, DateTime.DaysInMonth(today.Year, today.Month));
                string PAYMTestMobileNo = System.Configuration.ConfigurationManager.AppSettings["PAYMTestMobileNo"];
                if (!string.IsNullOrEmpty(PAYMTestMobileNo))
                    mobileNo = PAYMTestMobileNo;
                if (Type == 0) // Activation of the Bundle
                { SMSContent = "Dear Customer, The Pakistan Pocket Saver Bundle is activated on your number for 30 days. You can enjoy 1000 free mins to any Pakistan Zong numbers."; }
                else if (Type == 1) // Cancellation of the LLOM 
                { SMSContent = "The LLOM Subscription has been marked for cancellation and will be cancelled by end of the Subscription Period - " + Convert.ToString(endOfMonth).Substring(0, 10) + "."; }
                else if (Type == 2) // Cancellation of the Bundle
                {
                    //SMSContent = "The " +Bundle+" Country Saver Bundle Subscription has been marked for cancellation and will be cancelled by end of the Subscription Period - " + Convert.ToString(endOfMonth).Substring(0, 10) + ".";
                    SMSContent =  Bundle + " - " + Convert.ToString(endOfMonth).Substring(0, 10) + ".";
                }
                //string smsText = string.Format("Your Service have been Activated! Please Contact Vectone Customer Service Center immediately for any Queries!");
                string[] res = new Helpers.SendSMS().Send(false, mobileNo, "111", SMSContent, 0);
                if (res != null && res.Count() > 0)
                {
                    return !(res[0] == "-1");
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                //Log.Error("PAYMPlanBundlesController.SMSChangePlan", ex.Message);
                return false;
            }
        }
    }
}
