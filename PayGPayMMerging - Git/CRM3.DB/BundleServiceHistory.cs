﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;
using CRM_API.Models.BundleService;

namespace CRM_API.DB
{
    public class BundleServiceHistory
    {
        ////Issue History Report
        public static IEnumerable<BundleServiceModels> GetBundleServiceHistoryList(string date_from, string date_to)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<Models.BundleService.BundleServiceModels>(
                            "crm_finance_reconcilation_exe_report", new
                            {
                                //@mobileno = "all",
                                @date_from = date_from,
                                @date_to = date_to
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<Models.BundleService.BundleServiceModels>();
                }
            }
            catch (Exception ex)
            {
                List<Models.BundleService.BundleServiceModels> resValue = new List<Models.BundleService.BundleServiceModels>();
                return resValue;
            }
            
        }        
    }
}
