﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;

namespace CRM_API.DB
{
    public class UpdateAdjustBundle
    {
        public static AdjustBundleSaveModelsOutput UpdateBundle(AdjustBundleSaveModelsInput input)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    AdjustBundleSaveModelsOutput objResult = new AdjustBundleSaveModelsOutput();
                    objResult.errcode = 0;
                    objResult.errmsg = "Succcess";
                    //var result = conn.Query<AdjustBundleSaveModelsOutput>(
                    //        "test", new
                    //        {
                    //            //@sitecode = input.sitecode,
                    //            //@mobileno = input.mobileno,
                    //            //@username = input.username,
                    //            //@amount = input.amount,
                    //            //@calledby = input.calledby,
                    //            //@balance_type = input.balance_type,
                    //            //@bundleid = input.bundleid,
                    //            //@daysvalid = input.daysvalid
                    //        }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    return objResult ?? new AdjustBundleSaveModelsOutput();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL UpdateAdjustBundle: " + ex.Message);
            }
        }
    }
}
