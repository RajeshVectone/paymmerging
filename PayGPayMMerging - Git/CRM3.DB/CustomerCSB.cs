﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;

namespace CRM_API.DB.CustomerCSB
{
    public class SProc
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public static IEnumerable<CustomerCSBRecords> List(string mobileno)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CustomerCSBRecords>(
                            "crm3_saver_cnx_list ", new
                            {
                                @cli = mobileno
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CustomerCSBRecords>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CustomerCSB.List: {0}", ex.Message);
                List<CustomerCSBRecords> resValue = new List<CustomerCSBRecords>();
#if DEBUG
                resValue.Add(new CustomerCSBRecords() { });
#endif
                return resValue;
            }
        }

        public static IEnumerable<CustomerCSBHistory> ListHistory(string mobileno,string destno)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CustomerCSBHistory>(
                            "crm3_saver_history_list", new
                            {
                                @mobileno = mobileno,
                                @destination=destno,
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CustomerCSBHistory>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CustomerCSBHistory.List: {0}", ex.Message);
                List<CustomerCSBHistory> resValue = new List<CustomerCSBHistory>();
#if DEBUG
                resValue.Add(new CustomerCSBHistory() { });
#endif
                return resValue;
            }
        }

        
        public static Common.ErrCodeMsg Cancel(string mobileno, string destno,int action=0)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    Common.ErrCodeMsg resvalue = new Common.ErrCodeMsg();
   
                    var p = new DynamicParameters();
                    p.Add("@mundio_cli", mobileno);
                    p.Add("@destination", destno);
                    p.Add("@action", action);
                    p.Add("@errcode", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    p.Add("@errmsg", dbType: DbType.String, size: 100, direction: ParameterDirection.Output);

                    conn.Execute("crm3_saver_action", p, commandType: CommandType.StoredProcedure);

                    resvalue.errcode = p.Get<int>("@errcode");
                    resvalue.errmsg = p.Get<string>("@errmsg");
                    return resvalue;
                }
            }
            catch (Exception ex)
            {
                Log.Error("Cancel BL: {0}", ex.Message);
                return new Common.ErrCodeMsg() { errcode = -1, errmsg = ex.Message };
            }
        }
        public static Common.ErrCodeMsg Renew(string mobileno, string destno, int action=1)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    Common.ErrCodeMsg resvalue = new Common.ErrCodeMsg();

                    var p = new DynamicParameters();
                    p.Add("@mundio_cli", mobileno);
                    p.Add("@destination", destno);
                    p.Add("@action", action);
                    p.Add("@errcode", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    p.Add("@errmsg", dbType: DbType.String, size: 100, direction: ParameterDirection.Output);

                    conn.Execute("crm3_saver_action", p, commandType: CommandType.StoredProcedure);

                    resvalue.errcode = p.Get<int>("@errcode");
                    resvalue.errmsg = p.Get<string>("@errmsg");
                    return resvalue;
                }
            }
            catch (Exception ex)
            {
                Log.Error("Cancel BL: {0}", ex.Message);
                return new Common.ErrCodeMsg() { errcode = -1, errmsg = ex.Message };
            }
        }

        public static Common.ErrCodeMsg ChangeRenewalMethod(string mobileno,string destno,string NewRenewal)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    
                    Common.ErrCodeMsg resvalue = new Common.ErrCodeMsg();
                    var p = new DynamicParameters();
                    p.Add("@mobile_cli", mobileno);
                    p.Add("@destination", destno);
                    p.Add("@newPaymentmethod", NewRenewal);
                    p.Add("@errcode", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    p.Add("@errmsg", dbType: DbType.String, size: 100, direction: ParameterDirection.Output);

                    conn.Execute("crm3_saver_change_paymentmethod", p, commandType: CommandType.StoredProcedure);

                    resvalue.errcode = p.Get<int>("@errcode");
                    resvalue.errmsg = p.Get<string>("@errmsg");
                    return resvalue;
                }
            }
            catch (Exception ex)
            {
                Log.Error("Change Payment BL: {0}", ex.Message);
                return new Common.ErrCodeMsg() { errcode = -1, errmsg = ex.Message };
            }
        }

        public static IEnumerable<CustomerCSBHistory> BalanceCharge(int order_id)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CustomerCSBHistory>(
                            "saver_balance_pay", new
                            {
                                @order_id = order_id 
                            }, commandType: CommandType.StoredProcedure);


                    return result ?? new List<CustomerCSBHistory>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CustomerCSBHistory.List: {0}", ex.Message);
                List<CustomerCSBHistory> resValue = new List<CustomerCSBHistory>();
#if DEBUG
                resValue.Add(new CustomerCSBHistory() { });
#endif
                return resValue;
            }
        }

        public static IEnumerable<CustomerCSBRecords> ListCSBExpired()
        {
            
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<CustomerCSBRecords>(
                            "crm3_saver_expired_list ", commandType: CommandType.StoredProcedure);

                    return result ?? new List<CustomerCSBRecords>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("ListCSBExpired BL: {0}", ex.Message);
                return new List<CustomerCSBRecords>();
            }
        }
        public static Common.ErrCodeMsg DeActivate(int orderId, string crm_login, out int needCancelLoc)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    Common.ErrCodeMsg resvalue = new Common.ErrCodeMsg();

                    conn.Open();

                    var p = new DynamicParameters();
                    p.Add("@order_id", orderId);
                    p.Add("@crm_login", crm_login);
                    p.Add("@errcode", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    p.Add("@errmsg", dbType: DbType.String, size: 100, direction: ParameterDirection.Output);
                    p.Add("@need_cancel_loc", dbType: DbType.Int32, direction: ParameterDirection.Output);

                    conn.Execute("crm3_mimsi_deactivate ", p, commandType: CommandType.StoredProcedure);

                    resvalue.errcode = p.Get<int>("@errcode");
                    resvalue.errmsg = p.Get<string>("@errmsg");
                    needCancelLoc = p.Get<int>("@need_cancel_loc");

                    Log.Debug("DeActivate: {0} {1}", resvalue.errcode, resvalue.errmsg);
                    return resvalue;
                }
            }
            catch (Exception ex)
            {
                Log.Error("DeActivate BL: {0}", ex.Message);
                needCancelLoc = 0;
                return new Common.ErrCodeMsg() { errcode = -1, errmsg = ex.Message };
            }
        }

        public static Common.ErrCodeMsg ChangeSubscriptionId(CRM_API.Models.CustomerCSBHistory changeRecord)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    Common.ErrCodeMsg resvalue = new Common.ErrCodeMsg();                    
                    var p = new DynamicParameters();

                    p.Add("@order_id", changeRecord.order_id);
                    p.Add("@paymode", changeRecord.renewal_method_string);
                    p.Add("@crm_login", changeRecord.crm_login);
                    p.Add("@payref", changeRecord.payref);
                    p.Add("@paid_amount", changeRecord.monthly_charge_string);
                    p.Add("@currency", "GBP");
                    p.Add("@cybersource_subscription", changeRecord.SubscriptionID);                    

                    Log.Debug("DeActivate: {0} {1}", resvalue.errcode, resvalue.errmsg);
                    return resvalue;
                }
            }
            catch (Exception ex)
            {
                Log.Error("Subscription Change: {0}", ex.Message);

                return new Common.ErrCodeMsg() { errcode = -1, errmsg = ex.Message };
            }
        }

        public static IEnumerable<CustomerCSBNotes> NotesList(string account_id)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CustomerCSBNotes>(
                            "saver_account_notes_list", new
                            {
                                @account_id = account_id
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CustomerCSBNotes>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CustomerCSBHistory.List: {0}", ex.Message);
                List<CustomerCSBNotes> resValue = new List<CustomerCSBNotes>();
                return resValue;
            }
        }

        public static Common.ErrCodeMsg AddNotes(string account_id, string description, string user_login)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Common.ErrCodeMsg>(
                            "saver_account_notes_add", new
                            {
                                @account_id = account_id,
                                @descript = description,
                                @user_login = user_login
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    Log.Debug("AddNotesCustomerCSB: {0}", result.errmsg);
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("AddNotesCustomerCSB: {0}", ex.Message);
                return new Common.ErrCodeMsg() { errcode = -1, errmsg = ex.Message };
            }
        }

        public static IEnumerable<CustomerCSBRecords> CountryList(string mobileno, string site_code)
        {

            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CustomerCSBRecords>(
                            "saver_account_notes_list", new
                            {
                                @mobileno = mobileno,
                                @site_code = site_code
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CustomerCSBRecords>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CustomerCSBHistory.CountryList: {0}", ex.Message);
                List<CustomerCSBRecords> resValue = new List<CustomerCSBRecords>();
                return resValue;
            }
        }
    }
}