﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;

namespace CRM_API.DB.Ref.Country
{
    public class Country
    {
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public static IEnumerable<Country> ListAllCountry(string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Country>(
                             "crm3_country_list", new
                             {
                                 @sitecode = sitecode
                             },
                            commandType: CommandType.StoredProcedure);

                    return result ?? new List<Country>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL ListAllCountry: " + ex.Message);
            }
        }
        public static async Task<IEnumerable<Country>> ListAllCountry_Async(string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = Task.Run<IEnumerable<Country>>(()=> {
                        return conn.QueryAsync<Country>(
                            "crm3_country_list", new {
                                @sitecode = sitecode
                            } ,
                            commandType: CommandType.StoredProcedure);
                    });
                    return await result ?? new List<Country>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL ListAllCountry_Async: " + ex.Message);
            }
        }
        public static async Task<IEnumerable<Country>> CountryById_Async(string id,string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = Task.Run<IEnumerable<Country>>(() =>
                    {
                        return conn.QueryAsync<Country>(
                            "crm3_country_get_by_id", new
                            {
                                @countrycode=id,
                                @sitecode = sitecode
                            },
                            commandType: CommandType.StoredProcedure);
                    });
                    return await result ?? new List<Country>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL CountryById_Async: " + ex.Message);
            }
        }
    }
}
