﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Dapper;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Xml;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using NLog;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace CRM_API.DB
{
    public class PAYMDDStetup
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        //get the bank details
        public static IEnumerable<CRM_API.Models.getBankList> GetAllBank()
        {
            try
            {
                string Url = "http://192.168.41.23:8071/VANServices.svc/VMUK/getbanklistV201210";
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Url);
                req.UserAgent = "Mozilla";
                req.Method = "GET";
                req.Timeout = 180000;
                try
                {
                    HttpWebResponse res = (HttpWebResponse)req.GetResponse();
                    Stream resStream = res.GetResponseStream();
                    StreamReader reader = new StreamReader(resStream);
                    string result = reader.ReadToEnd();
                    res.Close();
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(result);
                    DataSet ds = new DataSet();
                    XmlNodeReader xnodereader = new XmlNodeReader(doc);
                    ds.ReadXml(xnodereader);
                    if (ds != null)
                    {
                        if (ds.Tables.Count > 0)
                            return ds.Tables[0].AsEnumerable().Select(row => new CRM_API.Models.getBankList
                            {
                                bankcode = row.Field<string>("BankCode"),
                                bankname = row.Field<string>("BankName"),
                                AccountLength = row.Field<string>("AccountLength"),
                                Result = row.Field<string>("Result"),
                                TresCode = row.Field<string>("TresCode"),
                                Msg = row.Field<string>("Msg")
                            }).ToList();
                    }
                }
                catch (Exception ex) { throw new Exception(ex.Message); }
                return null;
            }
            catch (Exception ex)
            {
                throw new Exception("BL VANServices.svc : " + ex.Message);
            }
        }

        //validating the Account No, Sort code, bank Code
        public static IEnumerable<CRM_API.Models.getBankList> CheckAccount(CRM_API.Models.validationBank objvalidationBank)
        {
            try
            {
                ServicePointManager.ServerCertificateValidationCallback +=
                delegate(
                           Object sender1,
                           X509Certificate certificate,
                           X509Chain chain,
                           SslPolicyErrors sslPolicyErrors
                         )
                {
                    return true;
                };

                //var Url = "http://www.bankaccountchecker.com/listener.php?key=40d4b09e8ff222e7bc3308d565ff2c47&password=Murali!@34&output=xml&type=uk&sortcode=" + objvalidationBank.sortCode + "&bankaccount=" + objvalidationBank.accoutNumber + "";
                var Url = "http://www.bankaccountchecker.com/listener.php";
                string postData = "key=d04e34097b8913afff2f39b06a5373c0&password=Mundio#123&output=xml&type=uk&sortcode=" + objvalidationBank.sortCode + "&bankaccount=" + objvalidationBank.accoutNumber;
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Url);
                req.UserAgent = "Mozilla";
                //req.Method = "GET";
                req.Method = "POST";
                req.Timeout = 180000;
                req.ContentType = "application/x-www-form-urlencoded";
                ASCIIEncoding enc = new ASCIIEncoding();
                byte[] data = enc.GetBytes(postData);
                req.ContentLength = data.Length;
                Stream newStream = req.GetRequestStream();
                newStream.Write(data, 0, data.Length);
                newStream.Close();
                HttpWebResponse res = (HttpWebResponse)req.GetResponse();
                Stream resStream = res.GetResponseStream();
                StreamReader reader = new StreamReader(resStream);
                string ValidAccount = reader.ReadToEnd();
                res.Close();
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(ValidAccount);
                DataSet ds = new DataSet();
                XmlNodeReader xnodereader = new XmlNodeReader(doc);
                ds.ReadXml(xnodereader);
                if (ds != null)
                {
                    if (ds.Tables.Count == 2)
                    {
                        if (ds.Tables["bankaccountchecker"].Rows[0]["resultCode"].ToString() == "01" && ds.Tables["accountProperties"].Rows[0]["bacs_direct_debit"].ToString().ToLower() == "true")
                        {
                            return ds.Tables[0].AsEnumerable().Select(row => new CRM_API.Models.getBankList
                            {
                                bankcode = objvalidationBank.accoutNumber,
                                bankname = ds.Tables["accountProperties"].Rows[0]["institution"].ToString(),
                                AccountLength = objvalidationBank.accoutNumber.Length.ToString(),
                                Result = "0",
                                TresCode = objvalidationBank.sortCode.ToString(),
                                Msg = ds.Tables["bankaccountchecker"].Rows[0]["resultDescription"].ToString()
                            }).ToList();

                        }
                        else
                        {
                            return ds.Tables[0].AsEnumerable().Select(row => new CRM_API.Models.getBankList
                            {
                                bankcode = objvalidationBank.accoutNumber,
                                bankname = ds.Tables["accountProperties"].Rows[0]["institution"].ToString(),
                                AccountLength = objvalidationBank.accoutNumber.Length.ToString(),
                                Result = "-1",
                                TresCode = objvalidationBank.sortCode.ToString(),
                                Msg = ds.Tables["bankaccountchecker"].Rows[0]["resultDescription"].ToString()
                            }).ToList();

                        }
                    }
                    else
                    {
                        return ds.Tables[0].AsEnumerable().Select(row => new CRM_API.Models.getBankList
                        {
                            bankcode = objvalidationBank.accoutNumber,
                            bankname = objvalidationBank.bankName,
                            AccountLength = objvalidationBank.accoutNumber.Length.ToString(),
                            Result = "-1",
                            TresCode = objvalidationBank.sortCode.ToString(),
                            Msg = ds.Tables["bankaccountchecker"].Rows[0]["resultDescription"].ToString()
                        }).ToList();

                    }
                }
                else
                {
                    return ds.Tables[0].AsEnumerable().Select(row => new CRM_API.Models.getBankList
                    {
                        bankcode = objvalidationBank.accoutNumber,
                        bankname = objvalidationBank.bankName,
                        AccountLength = objvalidationBank.accoutNumber.Length.ToString(),
                        Result = "-1",
                        TresCode = objvalidationBank.sortCode.ToString(),
                        Msg = "Service not connected"
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL VANServices.svc, Checking A/C : " + ex.Message);
            }

            #region Oldc1
            //string Url = "http://192.168.41.23:8071/VANServices.svc/VMUK/" + objvalidationBank.bankCode + "/" + objvalidationBank.sortCode + "/" + objvalidationBank.accoutNumber + "/ValidatingProcessV201210";
            //string Url = "http://192.168.41.23:8962/VANServices.svc/VMUK/" + objvalidationBank.bankCode + "/" + objvalidationBank.sortCode + "/" + objvalidationBank.accoutNumber + "/ValidatingProcessV201603";
            /*
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Url);
            req.UserAgent = "Mozilla";
            req.Method = "GET";
            req.Timeout = 180000;
            try
            {
                //HttpWebResponse res = (HttpWebResponse)req.GetResponse();
                //Stream resStream = res.GetResponseStream();
                //StreamReader reader = new StreamReader(resStream);
                //string result = reader.ReadToEnd();
                res.Close();
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(result);
                DataSet ds = new DataSet();
                XmlNodeReader xnodereader = new XmlNodeReader(doc);
                ds.ReadXml(xnodereader);
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                        return ds.Tables[0].AsEnumerable().Select(row => new CRM_API.Models.getBankList
                        {
                            //bankcode = row.Field<string>("BankCode"),
                            //bankname = row.Field<string>("BankName"),
                            //AccountLength = row.Field<string>("AccountLength"),
                            //Result = row.Field<string>("Result"),
                            //TresCode = row.Field<string>("TresCode"),
                            //Msg = row.Field<string>("Msg")

                            bankcode = row.Field<string>("CorrectedAccountNumber"),
                            bankname = row.Field<string>("BANK"),
                            AccountLength = row.Field<string>("CorrectedSortCode"),
                            Result = row.Field<string>("IsCorrect"),
                            TresCode = row.Field<string>("IsDirectDebitCapable"),
                            Msg = row.Field<string>("StatusInformation")

                        }).ToList();
                }
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
            return null;*/
            #endregion

        }

        //save the DD setup
        public static IEnumerable<CRM_API.Models.Common.ErrCodeMsg> saveDirectDebit(CRM_API.Models.AddDDSetUp objAddDDSetUp, string productCode)
        {
            try
            {
                Log.Info("saveDirectDebit : Input : " + JsonConvert.SerializeObject(objAddDDSetUp));
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.MCMCRMConnection))
                {
                    conn.Open();
                    //var result = conn.Query<CRM_API.Models.Common.ErrCodeMsg>("c3pm_ppManualInsertDDAccount", new

                    if (productCode == "VMFR")
                    {
                        var result = conn.Query<CRM_API.Models.Common.ErrCodeMsg>("vmfr_ppInsertDirectDebitAccount", new
                        {
                            @subscriberid = objAddDDSetUp.subscriberid,
                            @freesimid = objAddDDSetUp.freesimid,
                            @paymentref = objAddDDSetUp.paymentref,
                            @sort_code = objAddDDSetUp.sort_code,
                            @account_num = objAddDDSetUp.account_num,
                            @account_name = objAddDDSetUp.account_name,
                            @amount = objAddDDSetUp.amountPaidTag,
                            @updateby = objAddDDSetUp.updateby,
                            //@buildingsociety = objAddDDSetUp.bank_name,
                            //@branch_code = objAddDDSetUp.branch_code,
                            @country = objAddDDSetUp.country
                        }, commandType: CommandType.StoredProcedure);

                        //08-Jan-2019 : Moorthy : Added for Mandate Creation in Add New Direct Debit Details screen for AT
                        Log.Info("VMFR : result : " + JsonConvert.SerializeObject(result));

                        if (result != null && result.ElementAt(0).errcode == 0)
                        {
                            using (var conn1 = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                            {
                                try
                                {
                                    Log.Info("CRM_API.Helpers.Config.DefaultConnection : " + CRM_API.Helpers.Config.DefaultConnection);
                                    conn1.Open();
                                    var result2 = conn1.Query<CRM_API.Models.AddDDSetUpPersonalDetails>("web_get_personal_details_myaccount", new
                                    {
                                        @mobileno = objAddDDSetUp.mobileno,
                                        @subscriberid = objAddDDSetUp.subscriberid,
                                        @sitecode = objAddDDSetUp.sitecode,
                                        @brand = 1
                                    }, commandType: CommandType.StoredProcedure);

                                    if (result2 != null && result2.Count() > 0)
                                    {
                                        Log.Info("VMFR : saveDirectDebit : result2 : " + JsonConvert.SerializeObject(result2));

                                        string gocardcustomer = goCardnewcustomerAT(result2.ElementAt(0).firstname.ToString(), result2.ElementAt(0).email, result2.ElementAt(0).lastname.ToString(), result2.ElementAt(0).address1.ToString(), result2.ElementAt(0).address2.ToString(), result2.ElementAt(0).city, result2.ElementAt(0).postcode.ToString(), "Fr", "VMFR" + objAddDDSetUp.mobileno);
                                        if (gocardcustomer == "Error")
                                        {

                                        }
                                        else
                                        {
                                            Log.Info("VMFR : gocardcustomer : " + gocardcustomer);
                                            //create customer bankid
                                            string[] retVal = gocardcustomer.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                                            string CustID = "";
                                            CustID = retVal[0].Split(':')[2].ToString(); ;
                                            CustID = CustID.Replace("\"", "");

                                            string gocardbankaccount = goCardnewbankAccountOtherCountries(result2.ElementAt(0).firstname.ToString(), objAddDDSetUp.account_num.ToString(), objAddDDSetUp.sort_code, "FR", CustID);
                                            //string gocardbankaccount = goCardnewbankAccountAT(result2.ElementAt(0).firstname.ToString(), objAddDDSetUp.account_num.ToString(), objAddDDSetUp.sort_code, "AT", CustID);
                                            Log.Info("VMFR : gocardbankaccount : " + gocardbankaccount);
                                            if (gocardbankaccount == "Error")
                                            {

                                            }
                                            else
                                            {
                                                retVal = gocardbankaccount.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                                string CustBankID = "";
                                                CustBankID = retVal[0].Split(':')[2].ToString(); ;
                                                CustBankID = CustBankID.Replace("\"", "");
                                                string createmandate = goCardNewMandateAT(objAddDDSetUp.mobileno.ToString(), CustBankID, "VMFR" + objAddDDSetUp.mobileno);
                                                Log.Info("VMFR : createmandate : " + createmandate);
                                                if (createmandate == "Error")
                                                {

                                                }
                                                else
                                                {
                                                    string mandateid = "";
                                                    retVal = createmandate.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                                    mandateid = retVal[0].Split(':')[2].ToString(); ;
                                                    mandateid = mandateid.Replace("\"", "");
                                                    string mandatestatus = retVal[3].Split(':')[1].ToString();
                                                    mandatestatus = mandatestatus.Replace("\"", "");

                                                    //output.errcode = 0;
                                                    //output.isvalid = 0;
                                                    //output.errmsg = "Success";
                                                    //output.mandateid = mandateid;
                                                    //output.mandatestatus = mandatestatus;
                                                    //output.gocard_customerid = CustID;
                                                    //if (!String.IsNullOrEmpty(result2.ElementAt(0).subscribrid))
                                                    //    output.pp_customer_id = result2.ElementAt(0).Cust_Contract;

                                                    string paymentGocardresult = "";
                                                    paymentGocardresult = MandateInsert(objAddDDSetUp.pp_customer_id, objAddDDSetUp.mobileno, mandateid, 1, CustID, mandatestatus);
                                                    Log.Info("VMFR : paymentGocardresult : " + paymentGocardresult);
                                                }
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Log.Error("VMFR : Create Mandate : " + ex.Message);
                                }
                            }
                        }
                        return result ?? new List<CRM_API.Models.Common.ErrCodeMsg>();
                    }
                    else if (productCode == "VMBE")
                    {
                        var result = conn.Query<CRM_API.Models.Common.ErrCodeMsg>("vmbe_ppInsertDirectDebitAccount", new
                        {
                            @subscriberid = objAddDDSetUp.subscriberid,
                            @freesimid = objAddDDSetUp.freesimid,
                            @paymentref = objAddDDSetUp.paymentref,
                            @sort_code = objAddDDSetUp.sort_code,
                            @account_num = objAddDDSetUp.account_num,
                            @account_name = objAddDDSetUp.account_name,
                            @amount = objAddDDSetUp.amountPaidTag,
                            @updateby = objAddDDSetUp.updateby,
                            //@buildingsociety = objAddDDSetUp.bank_name,
                            //@branch_code = objAddDDSetUp.branch_code,
                            @country = objAddDDSetUp.country
                        }, commandType: CommandType.StoredProcedure);

                        //08-Jan-2019 : Moorthy : Added for Mandate Creation in Add New Direct Debit Details screen for AT
                        Log.Info("VMBE : result : " + JsonConvert.SerializeObject(result));

                        if (result != null && result.ElementAt(0).errcode == 0)
                        {
                            using (var conn1 = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                            {
                                try
                                {
                                    Log.Info("CRM_API.Helpers.Config.DefaultConnection : " + CRM_API.Helpers.Config.DefaultConnection);
                                    conn1.Open();
                                    var result2 = conn1.Query<CRM_API.Models.AddDDSetUpPersonalDetails>("web_get_personal_details_myaccount", new
                                    {
                                        @mobileno = objAddDDSetUp.mobileno,
                                        @subscriberid = objAddDDSetUp.subscriberid,
                                        @sitecode = objAddDDSetUp.sitecode,
                                        @brand = 1
                                    }, commandType: CommandType.StoredProcedure);

                                    if (result2 != null && result2.Count() > 0)
                                    {
                                        Log.Info("VMBE : saveDirectDebit : result2 : " + JsonConvert.SerializeObject(result2));

                                        string gocardcustomer = goCardnewcustomerAT(result2.ElementAt(0).firstname.ToString(), result2.ElementAt(0).email, result2.ElementAt(0).lastname.ToString(), result2.ElementAt(0).address1.ToString(), result2.ElementAt(0).address2.ToString(), result2.ElementAt(0).city, result2.ElementAt(0).postcode.ToString(), "BE", "VMBE" + objAddDDSetUp.mobileno);
                                        if (gocardcustomer == "Error")
                                        {

                                        }
                                        else
                                        {
                                            Log.Info("VMBE : gocardcustomer : " + gocardcustomer);
                                            //create customer bankid
                                            string[] retVal = gocardcustomer.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                                            string CustID = "";
                                            CustID = retVal[0].Split(':')[2].ToString(); ;
                                            CustID = CustID.Replace("\"", "");

                                            string gocardbankaccount = goCardnewbankAccountOtherCountries(result2.ElementAt(0).firstname.ToString(), objAddDDSetUp.account_num.ToString(), objAddDDSetUp.sort_code, "BE", CustID);
                                            //string gocardbankaccount = goCardnewbankAccountAT(result2.ElementAt(0).firstname.ToString(), objAddDDSetUp.account_num.ToString(), objAddDDSetUp.sort_code, "AT", CustID);
                                            Log.Info("VMBE : gocardbankaccount : " + gocardbankaccount);
                                            if (gocardbankaccount == "Error")
                                            {

                                            }
                                            else
                                            {
                                                retVal = gocardbankaccount.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                                string CustBankID = "";
                                                CustBankID = retVal[0].Split(':')[2].ToString(); ;
                                                CustBankID = CustBankID.Replace("\"", "");
                                                string createmandate = goCardNewMandateAT(objAddDDSetUp.mobileno.ToString(), CustBankID, "VMBE" + objAddDDSetUp.mobileno);
                                                Log.Info("VMBE : createmandate : " + createmandate);
                                                if (createmandate == "Error")
                                                {

                                                }
                                                else
                                                {
                                                    string mandateid = "";
                                                    retVal = createmandate.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                                    mandateid = retVal[0].Split(':')[2].ToString(); ;
                                                    mandateid = mandateid.Replace("\"", "");
                                                    string mandatestatus = retVal[3].Split(':')[1].ToString();
                                                    mandatestatus = mandatestatus.Replace("\"", "");

                                                    //output.errcode = 0;
                                                    //output.isvalid = 0;
                                                    //output.errmsg = "Success";
                                                    //output.mandateid = mandateid;
                                                    //output.mandatestatus = mandatestatus;
                                                    //output.gocard_customerid = CustID;
                                                    //if (!String.IsNullOrEmpty(result2.ElementAt(0).subscribrid))
                                                    //    output.pp_customer_id = result2.ElementAt(0).Cust_Contract;

                                                    string paymentGocardresult = "";
                                                    paymentGocardresult = MandateInsert(objAddDDSetUp.pp_customer_id, objAddDDSetUp.mobileno, mandateid, 1, CustID, mandatestatus);
                                                    Log.Info("VMBE : paymentGocardresult : " + paymentGocardresult);
                                                }
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Log.Error("VMBE : Create Mandate : " + ex.Message);
                                }
                            }
                        }
                        return result ?? new List<CRM_API.Models.Common.ErrCodeMsg>();
                    }
                    else if (productCode == "VMAT")
                    {
                        var result = conn.Query<CRM_API.Models.Common.ErrCodeMsg>("vmat_ppInsertDirectDebitAccount", new
                        {
                            @subscriberid = objAddDDSetUp.subscriberid,
                            @freesimid = objAddDDSetUp.freesimid,
                            @paymentref = objAddDDSetUp.paymentref,
                            @sort_code = objAddDDSetUp.sort_code,
                            @account_num = objAddDDSetUp.account_num,
                            @account_name = objAddDDSetUp.account_name,
                            @amount = objAddDDSetUp.amountPaidTag,
                            @updateby = objAddDDSetUp.updateby,
                            //@buildingsociety = objAddDDSetUp.bank_name,
                            //@branch_code = objAddDDSetUp.branch_code,
                            @country = objAddDDSetUp.country
                        }, commandType: CommandType.StoredProcedure);
                        //return result ?? new List<CRM_API.Models.Common.ErrCodeMsg>();

                        //08-Jan-2019 : Moorthy : Added for Mandate Creation in Add New Direct Debit Details screen for AT
                        Log.Info("VMAT : result : " + JsonConvert.SerializeObject(result));

                        if (result != null && result.ElementAt(0).errcode == 0)
                        {
                            using (var conn1 = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                            {
                                try
                                {
                                    Log.Info("CRM_API.Helpers.Config.DefaultConnection : " + CRM_API.Helpers.Config.DefaultConnection);
                                    conn1.Open();
                                    var result2 = conn1.Query<CRM_API.Models.AddDDSetUpPersonalDetails>("web_get_personal_details_myaccount", new
                                    {
                                        @mobileno = objAddDDSetUp.mobileno,
                                        @subscriberid = objAddDDSetUp.subscriberid,
                                        @sitecode = objAddDDSetUp.sitecode,
                                        @brand = 1
                                    }, commandType: CommandType.StoredProcedure);

                                    if (result2 != null && result2.Count() > 0)
                                    {
                                        Log.Info("VMAT : saveDirectDebit : result2 : " + JsonConvert.SerializeObject(result2));

                                        string gocardcustomer = goCardnewcustomerAT(result2.ElementAt(0).firstname.ToString(), result2.ElementAt(0).email, result2.ElementAt(0).lastname.ToString(), result2.ElementAt(0).address1.ToString(), result2.ElementAt(0).address2.ToString(), result2.ElementAt(0).city, result2.ElementAt(0).postcode.ToString(), "AT", "VMAT" + objAddDDSetUp.mobileno);
                                        if (gocardcustomer == "Error")
                                        {

                                        }
                                        else
                                        {
                                            Log.Info("VMAT : gocardcustomer : " + gocardcustomer);
                                            //create customer bankid
                                            string[] retVal = gocardcustomer.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                                            string CustID = "";
                                            CustID = retVal[0].Split(':')[2].ToString(); ;
                                            CustID = CustID.Replace("\"", "");

                                            string gocardbankaccount = goCardnewbankAccountOtherCountries(result2.ElementAt(0).firstname.ToString(), objAddDDSetUp.account_num.ToString(), objAddDDSetUp.sort_code, "AT", CustID);
                                            //string gocardbankaccount = goCardnewbankAccountAT(result2.ElementAt(0).firstname.ToString(), objAddDDSetUp.account_num.ToString(), objAddDDSetUp.sort_code, "AT", CustID);
                                            Log.Info("VMAT : gocardbankaccount : " + gocardbankaccount);
                                            if (gocardbankaccount == "Error")
                                            {

                                            }
                                            else
                                            {
                                                retVal = gocardbankaccount.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                                string CustBankID = "";
                                                CustBankID = retVal[0].Split(':')[2].ToString(); ;
                                                CustBankID = CustBankID.Replace("\"", "");
                                                string createmandate = goCardNewMandateAT(objAddDDSetUp.mobileno.ToString(), CustBankID, "VMAT" + objAddDDSetUp.mobileno);
                                                Log.Info("VMAT : createmandate : " + createmandate);
                                                if (createmandate == "Error")
                                                {

                                                }
                                                else
                                                {
                                                    string mandateid = "";
                                                    retVal = createmandate.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                                    mandateid = retVal[0].Split(':')[2].ToString(); ;
                                                    mandateid = mandateid.Replace("\"", "");
                                                    string mandatestatus = retVal[3].Split(':')[1].ToString();
                                                    mandatestatus = mandatestatus.Replace("\"", "");

                                                    //output.errcode = 0;
                                                    //output.isvalid = 0;
                                                    //output.errmsg = "Success";
                                                    //output.mandateid = mandateid;
                                                    //output.mandatestatus = mandatestatus;
                                                    //output.gocard_customerid = CustID;
                                                    //if (!String.IsNullOrEmpty(result2.ElementAt(0).subscribrid))
                                                    //    output.pp_customer_id = result2.ElementAt(0).Cust_Contract;

                                                    string paymentGocardresult = "";
                                                    paymentGocardresult = MandateInsert(objAddDDSetUp.pp_customer_id, objAddDDSetUp.mobileno, mandateid, 1, CustID, mandatestatus);
                                                    Log.Info("VMAT : paymentGocardresult : " + paymentGocardresult);
                                                }
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Log.Error("VMAT : Create Mandate : " + ex.Message);
                                }
                            }
                        }
                        return result ?? new List<CRM_API.Models.Common.ErrCodeMsg>();
                    }
                    else
                    {
                        var result = conn.Query<CRM_API.Models.Common.ErrCodeMsg>("c3pm_ppManualInsertDDAccount_v1", new
                        {
                            @subscriberid = objAddDDSetUp.subscriberid,
                            @freesimid = objAddDDSetUp.freesimid,
                            @paymentref = objAddDDSetUp.paymentref,
                            @sort_code = objAddDDSetUp.sort_code,
                            @account_num = objAddDDSetUp.account_num,
                            @account_name = objAddDDSetUp.account_name,
                            @amount = objAddDDSetUp.amountPaidTag,
                            @updateby = objAddDDSetUp.updateby,
                            @buildingsociety = objAddDDSetUp.bank_name
                            //,@country = objAddDDSetUp.country
                        }, commandType: CommandType.StoredProcedure);

                        Log.Info("result : " + JsonConvert.SerializeObject(result));

                        //03-Dec-2018 : Moorthy : Added for Mandate Creation in Add New Direct Debit Details screen
                        if (result != null && result.ElementAt(0).errcode == 0)
                        {
                            using (var conn1 = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                            {
                                conn1.Open();
                                var result2 = conn1.Query<CRM_API.Models.AddDDSetUpPersonalDetails>("web_get_personal_details_myaccount", new
                                {
                                    @mobileno = objAddDDSetUp.mobileno,
                                    @subscriberid = objAddDDSetUp.subscriberid,
                                    @sitecode = objAddDDSetUp.sitecode,
                                    @brand = 1
                                }, commandType: CommandType.StoredProcedure);

                                try
                                {
                                    if (result2 != null && result2.Count() > 0)
                                    {
                                        Log.Info("saveDirectDebit : result2 : " + JsonConvert.SerializeObject(result2));

                                        string gocardcustomer = goCardnewcustomer(result2.ElementAt(0).firstname.ToString(), result2.ElementAt(0).email, result2.ElementAt(0).lastname.ToString(), result2.ElementAt(0).address1.ToString(), result2.ElementAt(0).address2.ToString(), result2.ElementAt(0).city, result2.ElementAt(0).postcode.ToString(), "GB", "VMUK" + objAddDDSetUp.mobileno);
                                        if (gocardcustomer == "Error")
                                        {

                                        }
                                        else
                                        {
                                            Log.Info("gocardcustomer : " + gocardcustomer);
                                            //create customer bankid
                                            string[] retVal = gocardcustomer.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                                            string CustID = "";
                                            CustID = retVal[0].Split(':')[2].ToString(); ;
                                            CustID = CustID.Replace("\"", "");

                                            string gocardbankaccount = goCardnewbankAccount(result2.ElementAt(0).firstname.ToString(), objAddDDSetUp.account_num.ToString(), objAddDDSetUp.sort_code, "GB", CustID);
                                            Log.Info("gocardbankaccount : " + gocardbankaccount);
                                            if (gocardbankaccount == "Error")
                                            {

                                            }
                                            else
                                            {
                                                retVal = gocardbankaccount.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                                string CustBankID = "";
                                                CustBankID = retVal[0].Split(':')[2].ToString(); ;
                                                CustBankID = CustBankID.Replace("\"", "");
                                                string createmandate = goCardNewMandate(objAddDDSetUp.mobileno.ToString(), CustBankID, "VMUK" + objAddDDSetUp.mobileno);
                                                Log.Info("createmandate : " + createmandate);
                                                if (createmandate == "Error")
                                                {

                                                }
                                                else
                                                {
                                                    string mandateid = "";
                                                    retVal = createmandate.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                                    mandateid = retVal[0].Split(':')[2].ToString(); ;
                                                    mandateid = mandateid.Replace("\"", "");
                                                    string mandatestatus = retVal[3].Split(':')[1].ToString();
                                                    mandatestatus = mandatestatus.Replace("\"", "");

                                                    //output.errcode = 0;
                                                    //output.isvalid = 0;
                                                    //output.errmsg = "Success";
                                                    //output.mandateid = mandateid;
                                                    //output.mandatestatus = mandatestatus;
                                                    //output.gocard_customerid = CustID;
                                                    //if (!String.IsNullOrEmpty(result2.ElementAt(0).subscribrid))
                                                    //    output.pp_customer_id = result2.ElementAt(0).Cust_Contract;

                                                    string paymentGocardresult = "";
                                                    paymentGocardresult = MandateInsert(objAddDDSetUp.pp_customer_id, objAddDDSetUp.mobileno, mandateid, 1, CustID, mandatestatus);
                                                    Log.Info("paymentGocardresult : " + paymentGocardresult);
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Log.Debug("Create Mandate : Personal details not found!");
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Log.Error("Create Mandate : " + ex.Message);
                                    //output.errcode = -1;
                                    //output.errmsg = ex.Message;
                                }
                            }
                        }
                        return result ?? new List<CRM_API.Models.Common.ErrCodeMsg>();
                    }

                }
            }
            catch (SqlException ex)
            {
                Log.Error("BL c3pm_ppManualInsertDDAccount : " + ex.Message);
                throw new Exception("BL c3pm_ppManualInsertDDAccount : " + ex.Message);
            }
        }

        private static string goCardNewMandate(string mobileno, string CustBankID, string Contract)
        {
            string paymentGocardresult = "Error";
            MandatesInput GMI = new MandatesInput();
            GMI.scheme = "bacs";
            MandateLinks GML = new MandateLinks();
            GML.customer_bank_account = CustBankID; // "BA0001WPDC9NXP";  //  Praveen Bank Account
            GML.creditor = "CR00003Q1EN87D";
            MandateIn GMIn = new MandateIn();
            GMIn.contract = Contract; // "VMUK447465047756";
            GMI.metadata = GMIn;
            GMI.links = GML;
            MandateCreate GMCobj = new MandateCreate();
            GMCobj.mandates = GMI;
            try
            {
                //  GoCardless Bank Account Creation
                var httpGocardRequest = (HttpWebRequest)WebRequest.Create("https://api.gocardless.com/mandates");
                httpGocardRequest.UserAgent = "Fiddler";
                httpGocardRequest.Headers.Add("GoCardless-Version", "2015-07-06");
                httpGocardRequest.Headers.Add("Authorization", "Bearer 1U2AJpqX_03pS73on0nD5lANO8gIw1RWWpNL50cp");
                httpGocardRequest.Accept = "application/json";
                httpGocardRequest.ContentType = "application/json";
                httpGocardRequest.Timeout = 180000;
                httpGocardRequest.Method = "POST";
                using (var streamGocardWriters = new StreamWriter(httpGocardRequest.GetRequestStream()))
                {
                    string jsonGocard = new JavaScriptSerializer().Serialize(GMCobj);
                    streamGocardWriters.Write(jsonGocard);
                    streamGocardWriters.Flush();
                    streamGocardWriters.Dispose();
                    HttpWebResponse httpGocardResponce = (HttpWebResponse)httpGocardRequest.GetResponse();
                    Stream paymentGocardStream = httpGocardResponce.GetResponseStream();
                    StreamReader paymentGocardreader = new StreamReader(paymentGocardStream);
                    paymentGocardresult = paymentGocardreader.ReadToEnd();
                    paymentGocardStream.Close();
                    paymentGocardreader.Close();
                }
            }
            catch (WebException ex)
            {
                Log.Error("BL goCardNewMandate : " + ex.Message);
                Console.Write(ex);
            }
            return paymentGocardresult;

        }

        private static string goCardnewcustomer(string name, string email, string familyname, string address1, string address2, string city, string postcode, string countrycode, string salesid)
        {
            if (email == "")
            {
                email = "test@test.com";
            }
            if (address1 == "")
            {
                address1 = name;
            }
            if (address2 == "")
            {
                address2 = address1;
            }
            if (city == "")
            {
                city = "London";
            }
            if (postcode == "")
            {
                postcode = city;
            }
            if (countrycode == "")
            {
                postcode = "GB";
            }
            string paymentGocardresult = "Error";
            GoCardCustomers cs = new GoCardCustomers();
            cs.email = email;
            cs.given_name = name;
            cs.family_name = familyname;
            cs.address_line1 = address1;
            cs.address_line2 = address2;
            cs.city = city;
            cs.postal_code = postcode;
            cs.country_code = countrycode;
            MetadataType md = new MetadataType();
            md.salesforce_id = salesid;
            EnterGoCard myobject = new EnterGoCard();
            cs.metadata = md;
            myobject.customers = cs;

            try
            {
                //Go-cardless payment
                var httpGocardRequest = (HttpWebRequest)WebRequest.Create("https://api.gocardless.com/customers");
                httpGocardRequest.UserAgent = "Fiddler";
                httpGocardRequest.Headers.Add("GoCardless-Version", "2015-07-06");
                httpGocardRequest.Headers.Add("Authorization", "Bearer 1U2AJpqX_03pS73on0nD5lANO8gIw1RWWpNL50cp");
                httpGocardRequest.Accept = "application/json";
                httpGocardRequest.ContentType = "application/json";
                httpGocardRequest.Timeout = 180000;
                httpGocardRequest.Method = "POST";
                using (var streamGocardWriters = new StreamWriter(httpGocardRequest.GetRequestStream()))
                {
                    string jsonGocard = new JavaScriptSerializer().Serialize(myobject);
                    streamGocardWriters.Write(jsonGocard);
                    streamGocardWriters.Flush();
                    streamGocardWriters.Dispose();
                    HttpWebResponse httpGocardResponce = (HttpWebResponse)httpGocardRequest.GetResponse();
                    Stream paymentGocardStream = httpGocardResponce.GetResponseStream();
                    StreamReader paymentGocardreader = new StreamReader(paymentGocardStream);
                    paymentGocardresult = paymentGocardreader.ReadToEnd();
                    paymentGocardStream.Close();
                    paymentGocardreader.Close();
                }
            }
            catch (WebException ex)
            {
                Log.Error("goCardnewcustomer : " + ex.Message);
            }
            return paymentGocardresult;
        }

        private static string goCardnewbankAccount(string ACholdername, string ACnumber, string ACbranch, string countrycode, string customerid)
        {
            string paymentGocardresult = "Error";
            CustomerBankAccounts GCBA = new CustomerBankAccounts();
            GCBA.account_holder_name = ACholdername;
            GCBA.account_number = ACnumber;
            GCBA.branch_code = ACbranch; // Sortcode
            GCBA.country_code = countrycode; // Twodigit
            CustBankLinks GCBL = new CustBankLinks();
            GCBL.customer = customerid;// "CU000252WCM7CZ"
            GCBA.links = GCBL;
            CustBankMainInput myBankAcc = new CustBankMainInput();
            myBankAcc.customer_bank_accounts = GCBA;
            Log.Info("myBankAcc : " + JsonConvert.SerializeObject(myBankAcc));
            try
            {
                //  GoCardless Bank Account Creation
                var httpGocardRequest = (HttpWebRequest)WebRequest.Create("https://api.gocardless.com/customer_bank_accounts");
                httpGocardRequest.UserAgent = "Fiddler";
                httpGocardRequest.Headers.Add("GoCardless-Version", "2015-07-06");
                httpGocardRequest.Headers.Add("Authorization", "Bearer 1U2AJpqX_03pS73on0nD5lANO8gIw1RWWpNL50cp");
                httpGocardRequest.Accept = "application/json";
                httpGocardRequest.ContentType = "application/json";
                httpGocardRequest.Timeout = 180000;
                httpGocardRequest.Method = "POST";
                using (var streamGocardWriters = new StreamWriter(httpGocardRequest.GetRequestStream()))
                {
                    string jsonGocard = new JavaScriptSerializer().Serialize(myBankAcc);
                    streamGocardWriters.Write(jsonGocard);
                    streamGocardWriters.Flush();
                    streamGocardWriters.Dispose();
                    HttpWebResponse httpGocardResponce = (HttpWebResponse)httpGocardRequest.GetResponse();
                    Stream paymentGocardStream = httpGocardResponce.GetResponseStream();
                    StreamReader paymentGocardreader = new StreamReader(paymentGocardStream);
                    paymentGocardresult = paymentGocardreader.ReadToEnd();
                    paymentGocardStream.Close();
                    paymentGocardreader.Close();
                }
            }
            catch (WebException ex)
            {
                Log.Error("goCardnewbankAccount : " + ex.Message);
            }
            return paymentGocardresult;
        }

        private static string goCardNewMandateAT(string mobileno, string CustBankID, string Contract)
        {
            string paymentGocardresult = "Error";
            MandatesInput GMI = new MandatesInput();
            GMI.scheme = "sepa_core";
            MandateLinks GML = new MandateLinks();
            GML.customer_bank_account = CustBankID; // "BA0001WPDC9NXP";  //  Praveen Bank Account
            GML.creditor = "CR00005J8BPB2Y";
            MandateIn GMIn = new MandateIn();
            GMIn.contract = Contract; // "VMUK447465047756";
            GMI.metadata = GMIn;
            GMI.links = GML;
            MandateCreate GMCobj = new MandateCreate();
            GMCobj.mandates = GMI;
            try
            {
                //  GoCardless Bank Account Creation
                var httpGocardRequest = (HttpWebRequest)WebRequest.Create("https://api.gocardless.com/mandates");
                httpGocardRequest.UserAgent = "Fiddler";
                httpGocardRequest.Headers.Add("GoCardless-Version", "2015-07-06");
                httpGocardRequest.Headers.Add("Authorization", "Bearer live_Fp5pA--EXjJNkBZZVXv869vIt9eOzLLIj1RtTEMs");
                httpGocardRequest.Accept = "application/json";
                httpGocardRequest.ContentType = "application/json";
                httpGocardRequest.Timeout = 180000;
                httpGocardRequest.Method = "POST";
                using (var streamGocardWriters = new StreamWriter(httpGocardRequest.GetRequestStream()))
                {
                    string jsonGocard = new JavaScriptSerializer().Serialize(GMCobj);
                    streamGocardWriters.Write(jsonGocard);
                    streamGocardWriters.Flush();
                    streamGocardWriters.Dispose();
                    HttpWebResponse httpGocardResponce = (HttpWebResponse)httpGocardRequest.GetResponse();
                    Stream paymentGocardStream = httpGocardResponce.GetResponseStream();
                    StreamReader paymentGocardreader = new StreamReader(paymentGocardStream);
                    paymentGocardresult = paymentGocardreader.ReadToEnd();
                    paymentGocardStream.Close();
                    paymentGocardreader.Close();
                }
            }
            catch (WebException ex)
            {
                Log.Error("BL goCardNewMandateAT : " + ex.Message);
                Console.Write(ex);
            }
            return paymentGocardresult;

        }

        private static string goCardnewcustomerAT(string name, string email, string familyname, string address1, string address2, string city, string postcode, string countrycode, string salesid)
        {
            if (email == "")
            {
                email = "test@test.com";
            }
            if (address1 == "")
            {
                address1 = name;
            }
            if (address2 == "")
            {
                address2 = address1;
            }
            if (city == "")
            {
                city = "London";
            }
            if (postcode == "")
            {
                postcode = city;
            }
            if (countrycode == "")
            {
                postcode = "GB";
            }
            string paymentGocardresult = "Error";
            GoCardCustomers cs = new GoCardCustomers();
            cs.email = email;
            cs.given_name = name;
            cs.family_name = familyname;
            cs.address_line1 = address1;
            cs.address_line2 = address2;
            cs.city = city;
            cs.postal_code = postcode;
            cs.country_code = countrycode;
            MetadataType md = new MetadataType();
            md.salesforce_id = salesid;
            EnterGoCard myobject = new EnterGoCard();
            cs.metadata = md;
            myobject.customers = cs;

            try
            {
                //Go-cardless payment
                var httpGocardRequest = (HttpWebRequest)WebRequest.Create("https://api.gocardless.com/customers");
                httpGocardRequest.UserAgent = "Fiddler";
                httpGocardRequest.Headers.Add("GoCardless-Version", "2015-07-06");
                httpGocardRequest.Headers.Add("Authorization", "Bearer live_Fp5pA--EXjJNkBZZVXv869vIt9eOzLLIj1RtTEMs");
                httpGocardRequest.Accept = "application/json";
                httpGocardRequest.ContentType = "application/json";
                httpGocardRequest.Timeout = 180000;
                httpGocardRequest.Method = "POST";
                using (var streamGocardWriters = new StreamWriter(httpGocardRequest.GetRequestStream()))
                {
                    string jsonGocard = new JavaScriptSerializer().Serialize(myobject);
                    streamGocardWriters.Write(jsonGocard);
                    streamGocardWriters.Flush();
                    streamGocardWriters.Dispose();
                    HttpWebResponse httpGocardResponce = (HttpWebResponse)httpGocardRequest.GetResponse();
                    Stream paymentGocardStream = httpGocardResponce.GetResponseStream();
                    StreamReader paymentGocardreader = new StreamReader(paymentGocardStream);
                    paymentGocardresult = paymentGocardreader.ReadToEnd();
                    paymentGocardStream.Close();
                    paymentGocardreader.Close();
                }
            }
            catch (WebException ex)
            {
                Log.Error("goCardnewcustomer : " + ex.Message);
            }
            return paymentGocardresult;
        }

        private static string goCardnewbankAccountOtherCountries(string ACholdername, string ACnumber, string ACbranch, string countrycode, string customerid)
        {
            //string bankcode = "";
            //string accno = "1"; // need to declare some value
            ACnumber = ACnumber.Replace("IBAN", "");
            ACnumber = ACnumber.Replace(" ", "");
            //if (ACnumber.Length > 14)
            // {

            //     accno = ACnumber.Substring(9);
            //     bankcode = ACnumber.Substring(4, 5);
            // }
            string paymentGocardresult = "Error";
            CustomerBankAccounts GCBA = new CustomerBankAccounts();
            GCBA.account_holder_name = ACholdername;
            GCBA.iban = ACnumber;
            //  GCBA.account_number = accno;
            //   GCBA.bank_code = bankcode; // Sortcode
            //   GCBA.country_code = countrycode; // Twodigit
            CustBankLinks GCBL = new CustBankLinks();
            GCBL.customer = customerid;// "CU000252WCM7CZ"
            GCBA.links = GCBL;
            CustBankMainInput myBankAcc = new CustBankMainInput();
            myBankAcc.customer_bank_accounts = GCBA;
            //CustBankMainOutput myGoCard = new CustBankMainOutput();  //  Return

            //  if (accno.Length==11)
            Log.Info("myBankAcc : " + JsonConvert.SerializeObject(myBankAcc));
            try
            {
                //  GoCardless Bank Account Creation
                var httpGocardRequest = (HttpWebRequest)WebRequest.Create("https://api.gocardless.com/customer_bank_accounts");
                httpGocardRequest.UserAgent = "Fiddler";
                httpGocardRequest.Headers.Add("GoCardless-Version", "2015-07-06");
                httpGocardRequest.Headers.Add("Authorization", "Bearer live_Fp5pA--EXjJNkBZZVXv869vIt9eOzLLIj1RtTEMs");
                httpGocardRequest.Accept = "application/json";
                httpGocardRequest.ContentType = "application/json";
                httpGocardRequest.Timeout = 180000;
                httpGocardRequest.Method = "POST";
                using (var streamGocardWriters = new StreamWriter(httpGocardRequest.GetRequestStream()))
                {
                    string jsonGocard = new JavaScriptSerializer().Serialize(myBankAcc);
                    streamGocardWriters.Write(jsonGocard);
                    streamGocardWriters.Flush();
                    streamGocardWriters.Dispose();
                    HttpWebResponse httpGocardResponce = (HttpWebResponse)httpGocardRequest.GetResponse();
                    Stream paymentGocardStream = httpGocardResponce.GetResponseStream();
                    StreamReader paymentGocardreader = new StreamReader(paymentGocardStream);
                    paymentGocardresult = paymentGocardreader.ReadToEnd();
                    paymentGocardStream.Close();
                    paymentGocardreader.Close();
                }
            }
            catch (WebException ex)
            {
                Log.Error("goCardnewbankAccount : " + ex.Message);
            }
            return paymentGocardresult;
        }

        private static string goCardnewbankAccountAT(string ACholdername, string ACnumber, string ACbranch, string countrycode, string customerid)
        {
            string bankcode = "";
            string accno = "1"; // need to declare some value
            ACnumber = ACnumber.Replace(" ", "");
            if (ACnumber.Length > 14)
            {
                accno = ACnumber.Substring(9);
                bankcode = ACnumber.Substring(4, 5);
            }
            string paymentGocardresult = "Error";
            CustomerBankAccountsAT GCBA = new CustomerBankAccountsAT();
            GCBA.account_holder_name = ACholdername;
            GCBA.account_number = accno;
            GCBA.bank_code = bankcode; // Sortcode
            GCBA.country_code = countrycode; // Twodigit
            CustBankLinks GCBL = new CustBankLinks();
            GCBL.customer = customerid;// "CU000252WCM7CZ"
            GCBA.links = GCBL;
            CustBankMainInputAT myBankAcc = new CustBankMainInputAT();
            myBankAcc.customer_bank_accounts = GCBA;
            Log.Info("myBankAcc : " + JsonConvert.SerializeObject(myBankAcc));
            try
            {
                //  GoCardless Bank Account Creation
                var httpGocardRequest = (HttpWebRequest)WebRequest.Create("https://api.gocardless.com/customer_bank_accounts");
                httpGocardRequest.UserAgent = "Fiddler";
                httpGocardRequest.Headers.Add("GoCardless-Version", "2015-07-06");
                httpGocardRequest.Headers.Add("Authorization", "Bearer live_Fp5pA--EXjJNkBZZVXv869vIt9eOzLLIj1RtTEMs");
                httpGocardRequest.Accept = "application/json";
                httpGocardRequest.ContentType = "application/json";
                httpGocardRequest.Timeout = 180000;
                httpGocardRequest.Method = "POST";
                using (var streamGocardWriters = new StreamWriter(httpGocardRequest.GetRequestStream()))
                {
                    string jsonGocard = new JavaScriptSerializer().Serialize(myBankAcc);
                    streamGocardWriters.Write(jsonGocard);
                    streamGocardWriters.Flush();
                    streamGocardWriters.Dispose();
                    HttpWebResponse httpGocardResponce = (HttpWebResponse)httpGocardRequest.GetResponse();
                    Stream paymentGocardStream = httpGocardResponce.GetResponseStream();
                    StreamReader paymentGocardreader = new StreamReader(paymentGocardStream);
                    paymentGocardresult = paymentGocardreader.ReadToEnd();
                    paymentGocardStream.Close();
                    paymentGocardreader.Close();
                }
            }
            catch (WebException ex)
            {
                Log.Error("goCardnewbankAccount : " + ex.Message);
            }
            return paymentGocardresult;
        }

        private static string MandateInsert(string customerid, string mobileno, string mandate_id, int isvalid, string gocard_customerid, string mandate_status)
        {
            string returnResult = "Error";
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.MCMCRMConnection))
                {
                    conn.Open();
                    var sp = "Paym_insert_direct_debit_account_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @customerid = customerid,
                                @mobileno = mobileno,
                                @mandate_id = mandate_id,
                                @isvalid = isvalid,
                                @gocard_customerid = gocard_customerid,
                                @mandate_status = mandate_status
                            },
                            commandType: CommandType.StoredProcedure);
                    returnResult = "Success";
                }
            }
            catch (Exception ex)
            {
                Log.Error("MandateInsert : " + ex.Message);
            }
            return returnResult;
        }

        //Getting Basic Details
        public static IEnumerable<CRM_API.Models.BasicDetail> BasicDetails(string SubscriberID)
        {
            //TODO : Need to comment
            //List<CRM_API.Models.BasicDetail> lstOutput = new List<Models.BasicDetail>();
            //CRM_API.Models.BasicDetail output = new Models.BasicDetail();
            //output.subscriberid = 1646932;
            //output.cybersourceid = "VMUK-PP150-RX-910566";
            //output.freesimid = 1432814;
            //output.branchsortcode = "600008";
            //output.accountnumber = "39508145";
            //output.accountnumber = "39508145";
            //output.accountname = "R JAIPURIA";
            //output.amountpaidtag = "Pay Monthly15 GBP";
            //output.status = "Active";
            //lstOutput.Add(output);
            //return lstOutput;

            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.MCMCRMConnection))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.BasicDetail>("c3pm_getCandidateDDAccountInformation", new
                    {
                        @subscriberid = SubscriberID
                    }, commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.BasicDetail>();
                }
            }
            catch (SqlException ex)
            {
                Log.Error("c3pm_getCandidateDDAccountInformation : " + ex.Message);
                throw new Exception("BL c3pm_getCandidateDDAccountInformation : " + ex.Message);
            }
        }

        //GoCardless
        public class GoCardCustomers
        {
            public string email { get; set; }
            public string given_name { get; set; }
            public string family_name { get; set; }
            public string address_line1 { get; set; }
            public string address_line2 { get; set; }
            public string city { get; set; }
            public string postal_code { get; set; }
            public string country_code { get; set; }
            public MetadataType metadata { get; set; }
        }

        public class MetadataType
        {
            public string salesforce_id { get; set; }
        }

        public class EnterGoCard
        {
            public GoCardCustomers customers { get; set; }
        }

        static string BytesToStringConverted(byte[] bytes)
        {
            using (var stream = new MemoryStream(bytes))
            {
                using (var streamReader = new StreamReader(stream))
                {
                    return streamReader.ReadToEnd();
                }
            }
        }

        public class CustomerBankAccounts
        {
            public string account_number { get; set; }
            public string branch_code { get; set; }
            public string account_holder_name { get; set; }
            public string country_code { get; set; }
            public CustBankLinks links { get; set; }
            public string iban { get; set; }
        }

        public class CustomerBankAccountsAT
        {
            public string account_number { get; set; }
            public string bank_code { get; set; }
            public string account_holder_name { get; set; }
            public string country_code { get; set; }
            public CustBankLinks links { get; set; }
        }

        public class CustBankLinks
        {
            public string customer { get; set; }
        }

        public class CustBankMainInput
        {
            public CustomerBankAccounts customer_bank_accounts { get; set; }
        }

        public class CustBankMainInputAT
        {
            public CustomerBankAccountsAT customer_bank_accounts { get; set; }
        }

        public class MandatesInput
        {
            public string scheme { get; set; }
            public MandateIn metadata { get; set; }
            public MandateLinks links { get; set; }
        }

        public class MandateLinks
        {
            public string customer_bank_account { get; set; }
            public string creditor { get; set; }
        }

        public class MandateIn
        {
            public string contract { get; set; }
        }

        public class Mandatereinstate
        {
            public metadatareinstate metadata { get; set; }
        }

        public class metadatareinstate
        {
            public string ticket_id { get; set; }
        }

        public class MandatereinstateCreate
        {
            public Mandatereinstate data { get; set; }
        }

        public class MandateCreate
        {
            public MandatesInput mandates { get; set; }
        }
    }
}
