﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;
namespace CRM_API.DB.Subscriber
{
    public class SProc
    {
        public static int submitsubscriberinformation(Models.SimSwapSearch values)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var result = conn.Query<int>(
                            "crm3_subs_create1", new
                            {
                                @sitecode = values.Sitecode,
                                @title = values.Title,
                                @firstname = values.Firstname,
                                @lastname = values.Lastname,
                                @houseno = values.HouseNo,
                                @address1 = values.Address1,
                                @address2 = values.Address2,
                                @city = values.City,
                                @postcode = values.Postcode,
                                @state = values.State,
                                @countrycode = values.CountryCode,
                                @telephone = values.Telephone,
                                @mobilephone = values.MobilePhone,
                                @fax = values.Fax,
                                @email = values.Email,
                                @birthdate = values.BirthDate,
                                @personnumber = values.PersonNumber,
                                @question = values.SecurityQuestion,
                                @answer = values.SecurityAnswer,
                                @subscribertype = 0,
                                @ssn = values.Ssn,
                                @personalidtype = 0,
                                @referenceno = values.reference,
                                @comment = values.Comment,
                                @sourcereg = "CRM"
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    return result ;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public static int insertFreeSIm(int subscriberId, int freesimstatus, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    //old SP:crm3_create_freesim 
                    //New request from CRM not coming new request so we changed New stored procedure.
                    //new SP:crm3_create_freesim_SS
                    conn.Open();
                    var result = conn.Query<int>(
                            "crm3_create_freesim_SS", new
                            {
                                @subscriberid = subscriberId,
                                @freesimstatus = freesimstatus,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    return result;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public static Models.Subscriber.CountryCode GetCountryCode(string country,string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var result = conn.Query<Models.Subscriber.CountryCode>(
                            "crm3_country_get_by_id_v2", new
                            {
                                @countrycode = country,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    return result;
                }
            }
            catch (Exception ex)
            {
                Models.Subscriber.CountryCode result = new Models.Subscriber.CountryCode();
                return result;
            }
        }

        public static IEnumerable<CRM_API.Models.Subscriber.TypeofSubscriber> ListSubscriberType(string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.Subscriber.TypeofSubscriber>(
                            "crm3_r_subscriber_type", new
                            {
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.Subscriber.TypeofSubscriber>();
                }
            }
            catch (Exception e)
            {
                throw new Exception("BL ListSubscriberType: " + e.Message);
            }
        }
    }
}
namespace CRM_API.Models.Subscriber
{
    public class TypeofSubscriber
    {
        public int SubscriberType { get; set; }
        public string TypeName { get; set; }
        public string TypeDesc { get; set; }
    }
}
