﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;

namespace CRM_API.DB
{
    public class FreeCreditDB
    {  
        public static List<FreeCreditOutput> GetDetails(FreeCreditInput req)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<FreeCreditOutput>(
                            "crm_get_free_offers", new
                            {
                                sitecode = req.sitecode,
                                //date_from = req.date_from,
                                //date_to = req.date_to
                            }, commandType: CommandType.StoredProcedure);

                    //List<CRM_API.Models.FreeCreditOutput> result = new List<FreeCreditOutput>();

                    //FreeCreditOutput obj1 = new FreeCreditOutput();
                    //obj1.Action = "Subscribe";
                    //obj1.FreeCreditmode = "Test";
                    //obj1.FreeCreditName = "Test1223";
                    //obj1.Price = 3.20;
                    //obj1.ValidityPeriod = "1 month";
                    //obj1.ProductID = 1;
                    //obj1.ValidityPeriod = "2";
                    //result.Add(obj1);

                    return result.ToList() ?? new List<CRM_API.Models.FreeCreditOutput>();
                }
            }
            catch (Exception ex)
            {
                return new List<CRM_API.Models.FreeCreditOutput>();
            }
        }
        public static List<FreeCreditSubscribeOutput> Subscribe(FreeCreditSubscribeInput req)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<FreeCreditSubscribeOutput>(
                            "crm_subscribe_free_offers", new
                            {
                                sitecode = req.sitecode,
                                mobileno = req.mobileno,
                                iccid= req.iccid,
                                bundleid = req.bundleid,
                                paymode  = req.paymode,
                                is_promo = req.is_promo,
                                process_by = req.process_by,
                                crm_user = req.crm_user
                            }, commandType: CommandType.StoredProcedure); 
                    return result.ToList() ?? new List<CRM_API.Models.FreeCreditSubscribeOutput>();
                }
            }
            catch (Exception ex)
            {
                FreeCreditSubscribeOutput obj = new FreeCreditSubscribeOutput();
                obj.errcode = -1;
                obj.errmsg = "Failure";
                List<CRM_API.Models.FreeCreditSubscribeOutput> obj1 = new List<FreeCreditSubscribeOutput>();
                obj1.Add(obj);
                return obj1;
            }
        }
    }
}
