﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;

namespace CRM_API.DB.GoCheap
{
    public class SProc
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        const string sitecode = "GCM";

        public static CRM_API.Models.GoCheapValidateResult IsActiveMobileNo(GoCheapOrder order)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.SiteCRMConnection(sitecode)))
                {
                    conn.Open();

                    var result = conn.Query<CRM_API.Models.GoCheapValidateResult>(
                            "check_account_mobileno ", new
                            {
                                @mobileno = order.mobileno
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    Log.Debug("GoCheap.IsActiveMobileNo: {0}", result.errmsg);
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("GoCheap.IsActiveMobileNo: {0}", ex.Message);
                return new CRM_API.Models.GoCheapValidateResult() { errcode = -1, errmsg = ex.Message };
            }
        }

        public static CRM_API.Models.GoCheapBalanceResult TopUpByCCDC(GoCheapOrder order)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.SiteESPConnection(sitecode)))
                {
                    conn.Open();

                    var p = new DynamicParameters();
                    p.Add("@mobileno", order.mobileno);
                    p.Add("@paymentid", order.paymentid);
                    p.Add("@paymentref", order.paymentref);
                    p.Add("@productid", order.productid);
                    p.Add("@amount", order.amount);
                    p.Add("@ccdc_curr", order.currency);
                    p.Add("@calledby", order.order_source);
                    p.Add("@errcode", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    p.Add("@errmsg", dbType: DbType.String, direction: ParameterDirection.Output, size: 64);
                    p.Add("@incentif_data", dbType: DbType.String, direction: ParameterDirection.Output, size: 10);
                    p.Add("@total_notif", dbType: DbType.String, direction: ParameterDirection.Output, size: 255);

                    conn.Execute("tp_do_topup_process_byccdc_v2",
                            p,
                            commandType: CommandType.StoredProcedure);

                    CRM_API.Models.GoCheapBalanceResult result = new GoCheapBalanceResult();
                    result.errcode = p.Get<int>("@errcode");
                    result.errmsg = p.Get<string>("@errmsg");
                    result.incentif_data = p.Get<string>("@incentif_data");
                    result.total_notif = p.Get<string>("@total_notif");

                    Log.Debug("GoCheap.TopUpByCCDC: {0}", result.errmsg);
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("GoCheap.TopUpByCCDC: {0}", ex.Message);
                return new CRM_API.Models.GoCheapBalanceResult() { errcode = -1, errmsg = ex.Message };
            }
        }

        public static CRM_API.Models.GoCheapBalanceResult UpdateBalance(GoCheapOrder order)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.SiteESPConnection(sitecode)))
                {
                    conn.Open();

                    var p = new DynamicParameters();
                    p.Add("@mobileno", order.mobileno);
                    p.Add("@paymentid", order.paymentid);
                    p.Add("@paymentref", order.paymentref);
                    p.Add("@productid", order.productid);
                    p.Add("@amount", order.amount);
                    p.Add("@ccdc_curr", order.currency);
                    p.Add("@calledby", order.order_source);

                    var result = conn.Query<CRM_API.Models.GoCheapBalanceResult>(
                            "tp_do_topup_process_wrapper ", 
                            p,
                            commandType: CommandType.StoredProcedure).FirstOrDefault();
                    Log.Debug("GoCheap.UpdateBalance: {0}", result.errmsg);
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("GoCheap.UpdateBalance: {0}", ex.Message);
                return new CRM_API.Models.GoCheapBalanceResult() { errcode = -1, errmsg = ex.Message };
            }
        }

        public static int CreateSubscription(GoCheapOrder order)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.SiteCRMConnection(sitecode)))
                {
                    conn.Open();

                    var result = conn.Query<int>(
                            "subs_create_new_v2", new
                            {
                                @title = order.title,
                                @firstname = order.first_name,
                                @lastname = order.last_name,
                                @houseno = order.housenumber,
                                @address1 = order.address1,
                                @address2 = order.address2,
                                @city = order.town,
                                @postcode = order.postcode,
                                @state = order.state,
                                @countrycode = order.countrycode,
                                @telephone = order.telephone,
                                @mobilephone = order.mobileno,
                                @fax= order.fax,
                                @email = order.email,
                                @birthdate = order.date_of_birth,
                                @question = order.question,
                                @answer = order.answer,
                                @subscribertype = order.subscribertype,
                                @sourcereg = order.source_reg,
                                @subscriberchannel = order.order_source
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    Log.Debug("GoCheap.CreateSubscription: Subscription ID No. {0}", result);
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("GoCheap.CreateSubscription: {0}", ex.Message);
                return -1;
            }
        }

        public static int CreateFreeSim(GoCheapOrder order)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.SiteCRMConnection(sitecode)))
                {
                    conn.Open();

                    var result = conn.Query<int>(
                            "create_freesim", new
                            {
                                @subscriberid = order.subscription_id,
                                @freesimstatus = order.freesimstatus
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    Log.Debug("GoCheap.CreateFreeSim: Result ID No. {0}", result);
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("GoCheap.CreateFreeSim: {0}", ex.Message);
                return -1;
            }
        }

        public static int CreateFreeSimWithCredit(GoCheapOrder order)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.SiteCRMConnection(sitecode)))
                {
                    conn.Open();

                    var result = conn.Query<int>(
                            "vmuksim_create_sim_with_credit", new
                            {
                                @subscriberid = order.subscription_id,
                                @freesimstatus = order.freesimstatus,
                                @freesimtype = order.freesimtype,
                                @cybersourceid = order.paymentref
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    Log.Debug("GoCheap.CreateFreeSimWithCredit: Result ID No. {0}", result);
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("GoCheap.CreateFreeSimWithCredit: {0}", ex.Message);
                return -1;
            }
        }

        public static SimOrder InsertSimOrderRecords(GoCheapOrder order)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<SimOrder>(
                            "crm3_insert_freesim", new
                            {
                                @crm_login = order.crm_login,
                                @title = order.title,
                                @first_name = order.first_name,
                                @last_name = order.last_name,
                                @address1 = order.address1,
                                @address2 = order.address2,
                                @town = order.town,
                                @postcode = order.postcode,
                                @country = order.country,
                                @email = order.email,
                                @contact = order.contact,
                                @mobileno = order.mobileno,
                                @payment_ref = order.paymentref,
                                @ordersim_url = order.order_url,
                                @subscription_id = order.subscription_id,
                                @id_type = order.id_type,
                                @id_number = order.id_number,
                                @dob = order.date_of_birth,
                                @sitecode = order.sitecode,
                                @mundio_product = order.mundio_product,
                                @order_source = order.order_source,
                                @source_reg = order.source_reg
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    Log.Debug("GoCheap.InsertSIMOrderRecords: {0}", result.errmsg);
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("GoCheap.InsertSIMOrderRecords: {0}", ex.Message);
                return new SimOrder() { errcode = -1, errmsg = ex.Message };
            }
        }

        public static Common.ErrCodeMsg InsertVerification(GoCheapOrder order)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.SiteCRMConnection(sitecode)))
                {
                    conn.Open();

                    var result = conn.Query<Common.ErrCodeMsg>(
                            "vmuk_verification_insert_v3", new
                            {
                                @msisdn = order.mobileno,
                                @verifcode = order.verificationcode,
                                @status = order.status,
                                @password = order.password,
                                @fname = order.first_name,
                                @lname = order.last_name,
                                @email = order.email,
                                @lang = order.languange,
                                @gender = order.gender,
                                @dob = order.date_of_birth,
                                @bysms = order.verificationbysms,
                                @byemail = order.verificationbyemail,
                                @houseno = order.housenumber,
                                @address1 = order.address1,
                                @address2 = order.address2,
                                @city = order.town,
                                @postcode = order.postcode
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    Log.Debug("GoCheap.InsertVerification: {0}", result.errmsg);
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("GoCheap.InsertVerification: {0}", ex.Message);
                return new Common.ErrCodeMsg() { errcode = -1, errmsg = ex.Message };
            }
        }

        public static GoCheapLoginResult PersonalLogin(GoCheapOrder order)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.SiteCRMConnection(sitecode)))
                {
                    conn.Open();

                    var result = conn.Query<GoCheapLoginResult>(
                            "vmuk_personal_login", new
                            {
                                @msisdn = order.mobileno,
                                @passwd = order.password
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    Log.Debug("GoCheap.PersonalLogin: {0}", result.errmsg);
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("GoCheap.PersonalLogin: {0}", ex.Message);
                return new GoCheapLoginResult { errcode = -1, errmsg = ex.Message };
            }
        }

        public static GoCheapPersonalInfo GetPersonalInfo(GoCheapOrder order)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.SiteCRMConnection(sitecode)))
                {
                    conn.Open();

                    var result = conn.Query<GoCheapPersonalInfo>(
                            "vmuk_get_persinfo", new
                            {
                                @mobileno = order.mobileno,
                                @subscriberid = order.subscription_id
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    Log.Debug("GoCheap.GetPersonalInfo: {0}", result.errmsg);
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("GoCheap.GetPersonalInfo: {0}", ex.Message);
                return new GoCheapPersonalInfo { errcode = -1, errmsg = ex.Message };
            }
        }

        public static Common.ErrCodeMsg UpdatePersonalInfo(GoCheapOrder order)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.SiteCRMConnection(sitecode)))
                {
                    conn.Open();

                    var result = conn.Query<Common.ErrCodeMsg>(
                            "vmuk_update_persinfo", new
                            {
                                @subscriberid = order.subscription_id,
                                @mobileno = order.mobileno,
                                @firstname = order.first_name,
                                @lastname = order.last_name,
                                @address = order.address1,
                                @city = order.town,
                                @postcode = order.postcode,
                                @countrycode = order.countrycode,
                                @email = order.email,
                                @mobilephone = order.mobileno,
                                @landlinenumber = order.telephone,
                                @language = order.languange,
                                @gender = order.gender,
                                @bySMS = order.verificationbysms,
                                @byEmail = order.verificationbyemail,
                                @birthDate = order.date_of_birth
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    Log.Debug("GoCheap.UpdatePersonalInfo: {0}", result.errmsg);
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("GoCheap.UpdatePersonalInfo: {0}", ex.Message);
                return new Common.ErrCodeMsg { errcode = -1, errmsg = ex.Message };
            }
        }

        public static GoCheapPersonalInfo GetPersonalInfoForgotPassword(GoCheapOrder order)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.SiteCRMConnection(sitecode)))
                {
                    conn.Open();

                    var result = conn.Query<GoCheapPersonalInfo>(
                            "vmuk_get_forgot_password_bymobile", new
                            {
                                @msisdn = order.mobileno,
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    Log.Debug("GoCheap.GetPersonalInfoForgotPassword: {0}", result.errmsg);
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("GoCheap.GetPersonalInfoForgotPassword: {0}", ex.Message);
                return new GoCheapPersonalInfo { errcode = -1, errmsg = ex.Message };
            }
        }

        public static GoCheapAccountInfo GetAccountInfo(GoCheapOrder order)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.SiteESPConnection(sitecode)))
                {
                    conn.Open();

                    var p = new DynamicParameters();
                    p.Add("@userinfo", order.mobileno);
                    p.Add("@usertype", 2);
                    p.Add("@mobileno", dbType: DbType.String, direction: ParameterDirection.Output, size: 32);
                    p.Add("@iccid", dbType: DbType.String, direction: ParameterDirection.Output, size: 20);
                    p.Add("@tc", dbType: DbType.String, direction: ParameterDirection.Output, size: 3);
                    p.Add("@cc", dbType: DbType.String, direction: ParameterDirection.Output, size: 8);
                    p.Add("@bc", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    p.Add("@sc", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    p.Add("@trffclass", dbType: DbType.String, direction: ParameterDirection.Output, size: 4);
                    p.Add("@accbal", dbType: DbType.Double, direction: ParameterDirection.Output);
                    p.Add("@acccur", dbType: DbType.String, direction: ParameterDirection.Output, size: 3);
                    p.Add("@errcode", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    p.Add("@errmsg", dbType: DbType.String, direction: ParameterDirection.Output, size: 64);

                    conn.Execute("get_mvno_account_info",
                            p,
                            commandType: CommandType.StoredProcedure);

                    CRM_API.Models.GoCheapAccountInfo result = new GoCheapAccountInfo();
                    result.errcode = p.Get<int>("@errcode");
                    result.errmsg = p.Get<string>("@errmsg");
                    result.mobileno = p.Get<string>("@mobileno");
                    result.iccid = p.Get<string>("@iccid");
                    result.telcocode = p.Get<string>("@tc");
                    result.custcode = p.Get<string>("@cc");
                    result.batchcode = p.Get<int>("@bc");
                    result.serialcode = p.Get<int>("@sc");
                    result.trffclass = p.Get<string>("@trffclass");
                    result.balance = p.Get<double>("@accbal");
                    result.currcode = p.Get<string>("@acccur");

                    Log.Debug("GoCheap.GetAccountInfo: {0}", result.errmsg);
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("GoCheap.GetAccountInfo: {0}", ex.Message);
                return new CRM_API.Models.GoCheapAccountInfo() { errcode = -1, errmsg = ex.Message };
            }
        }

        public static IEnumerable<GoCheapTopUpLogResult> GetTopUpLog(GoCheapOrder order)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.SiteMVNOConnection(sitecode)))
                {
                    conn.Open();

                    var p = new DynamicParameters();
                    p.Add("@mobileno", order.mobileno);
                    p.Add("@method", order.method);
                    p.Add("@startdate", order.startdate);
                    p.Add("@enddate", order.enddate);
                    p.Add("@errcode", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    p.Add("@errmsg", dbType: DbType.String, direction: ParameterDirection.Output, size: 200);

                    var result = conn.Query<GoCheapTopUpLogResult>("get_topup_log",
                            p,
                            commandType: CommandType.StoredProcedure);
                    /*
                    result.errcode = p.Get<int>("@errcode");
                    result.errmsg = p.Get<string>("@errmsg");
                    */
                    Log.Debug("GoCheap.GetTopUpLog: {0}", p.Get<string>("@errmsg"));
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("GoCheap.GetTopUpLog: {0}", ex.Message);
                return new List<GoCheapTopUpLogResult>();
            }
        }

        public static Common.ErrCodeMsg ResetPassword(GoCheapOrder order)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.SiteCRMConnection(sitecode)))
                {
                    conn.Open();

                    var result = conn.Query<Common.ErrCodeMsg>(
                            "vmuk_reset_password", new
                            {
                                @msisdn = order.mobileno,
                                @subscriberid = order.subscription_id,
                                @new_password = order.new_password,
                                @old_password = order.password,
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    Log.Debug("GoCheap.ResetPassword: {0}", result.errmsg);
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("GoCheap.ResetPassword: {0}", ex.Message);
                return new Common.ErrCodeMsg { errcode = -1, errmsg = ex.Message };
            }
        }
    }
}
