﻿using CRM_API.Models;
using Dapper;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM_API.DB.SimSwap
{
    public class SProc
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public static CRM_API.Models.Common.ErrCodeMsgExt PutSimSwap(string mobileno, string new_iccid, string sitecode, string verify, string user, string jiraticket, float charge,string SwapType)
        {
            try
            {
                bool aStatus = false;
                if (sitecode == "MCM")
                {
                    var brandType = CRM_API.DB.Customer.SProc.CustomerDetail("VMUK", mobileno).ToArray();
                    if (brandType[0].bill_type == "PAYM") { aStatus = true; }
                }
                if (aStatus)
                {
                    using (var conn = new SqlConnection(Helpers.Config.PMBOConnection))
                    {
                        var result = conn.Query<CRM_API.Models.Common.ErrCodeMsgExt>(
                                    "crm_sim_swap_pm", new
                                    
                                    {
                                        @msisdn = mobileno.Trim(),
                                        @iccid_new = new_iccid.Trim(),
                                        @verification = verify.Trim(),
                                        @user = user.Trim(),
                                        @jira_ticket = jiraticket.Trim(),
                                        @charge = charge
                                    }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                        return result ?? new CRM_API.Models.Common.ErrCodeMsgExt();
                    }
                }
                else
                {
                    //PAY-G
                    using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                    {
                        var p = new DynamicParameters();
                        p.Add("@msisdn", mobileno.Trim());
                        p.Add("@iccid_new", new_iccid.Trim());
                        p.Add("@verification", verify.Trim());
                        p.Add("@user", user.Trim());
                        p.Add("@jira_ticket", jiraticket.Trim());
                        p.Add("@charge", charge);
                        p.Add("@sitecode", sitecode.Trim());
                        p.Add("@swaptype", Convert.ToInt32(SwapType.Trim()));   
                        p.Add("@balance_old", dbType: DbType.Decimal, direction: ParameterDirection.Output);
                        p.Add("@afterbal", dbType: DbType.Decimal, direction: ParameterDirection.Output);
                        p.Add("@errcode", dbType: DbType.Int32, direction: ParameterDirection.Output);
                        p.Add("@errmsg", dbType: DbType.String, direction: ParameterDirection.Output, size: 100);
                        
                        conn.Execute("crm3_crm_sim_swap_with_output", p, commandType: CommandType.StoredProcedure);
                        CRM_API.Models.Common.ErrCodeMsgExt result = new CRM_API.Models.Common.ErrCodeMsgExt();
                        result.errcode = p.Get<int>("@errcode");
                        result.errmsg = p.Get<string>("@errmsg");
                        result.balance_old = 0;
                        result.afterbal = 0;
                        return result ?? new CRM_API.Models.Common.ErrCodeMsgExt();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL PutSimSwap: " + ex.Message);
            }
        }

        //31-Dec-2018 : Moorthy : Added for SIM Reports
        public static IEnumerable<FillSIMReportsRef> FillSIMReports(string mobileNo, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<FillSIMReportsRef>(
                            "", new
                            {
                                @mobileno = mobileNo,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<FillSIMReportsRef>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.SimSwap.FillSIMReports: {0}", ex.Message);
                throw new Exception("CRM_API.DB.SimSwap.FillSIMReports: " + ex.Message);
            }
        }
    }
}
