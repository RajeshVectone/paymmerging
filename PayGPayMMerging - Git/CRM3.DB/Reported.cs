﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;
namespace CRM_API.DB.Report
{
   public class Reported
    {
        public static IEnumerable<Models.ReportsMo.reportsMobileRSP> GetIssueList(string subscriber, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<Models.ReportsMo.reportsMobileRSP>(
                            "crm3_CRM_Get_Issue_History_By_Mobileno", new
                            {
                                @mobileno = subscriber,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<Models.ReportsMo.reportsMobileRSP>();
                }
            }
            catch (Exception ex)
            {
                List<Models.ReportsMo.reportsMobileRSP> resValue = new List<Models.ReportsMo.reportsMobileRSP>();
                return resValue;
            }
        }
        public static IEnumerable<Models.ReportsMo.reportsMobileRSP> GetIssueTrackerBy(string subscriber,string end, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<Models.ReportsMo.reportsMobileRSP>(
                            "crm3_CRM_Get_Issue_History_by_Date", new
                            {
                                @from_date = subscriber,
                                @to_date=end,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<Models.ReportsMo.reportsMobileRSP>();
                }
            }
            catch (Exception ex)
            {
                List<Models.ReportsMo.reportsMobileRSP> resValue = new List<Models.ReportsMo.reportsMobileRSP>();
                return resValue;
            }
        }

    }
}