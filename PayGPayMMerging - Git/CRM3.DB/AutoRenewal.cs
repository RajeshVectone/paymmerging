﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;

namespace CRM_API.DB.AutoRenewal
{
    public class AutoRenewal
    {
        #region Declarations
        private static readonly Logger Log = LogManager.GetCurrentClassLogger(); 
        #endregion

        #region GetAutoRenwalInfo
        public static IEnumerable<CRM_API.Models.AutoRenewal.AutoRenewalOutput> GetAutoRenwalInfo(string mobileno, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.AutoRenewal.AutoRenewalOutput>(
                            "crm_get_mvno_auto_renewal_info", new
                            {
                                @mobileno = mobileno,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.AutoRenewal.AutoRenewalOutput>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL GetAutoRenwalInfo: " + ex.Message);
            }
        } 
        #endregion

        #region EnableDisableProcess
        public static IEnumerable<CRM_API.Models.AutoRenewal.Output> EnableDisableProcess(string mobileno, string iccid, string crm_user, int processflag, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.AutoRenewal.Output>(
                            "crm_mvno_enable_disable_process", new
                            {
                                @mobileno = mobileno,
                                @iccid = iccid,
                                @crm_user = crm_user,
                                @processflag = processflag,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result != null && result.Count() > 0 ? result : new List<CRM_API.Models.AutoRenewal.Output>() { new CRM_API.Models.AutoRenewal.Output { errcode = -1, errmsg = "No result found." } };
                }
            }
            catch (Exception ex)
            {
                return new List<CRM_API.Models.AutoRenewal.Output>() { new CRM_API.Models.AutoRenewal.Output { errcode = -1, errmsg = ex.Message } };
                //throw new Exception("BL EnableDisableProcess: " + ex.Message);
            }
        } 
        #endregion
    }
}
