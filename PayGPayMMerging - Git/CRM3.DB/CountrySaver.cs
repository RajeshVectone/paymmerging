﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Dapper;
using System.Threading.Tasks;
using CRM_API.Models.CountrySaver;
using NLog;


namespace CRM_API.DB.CountrySaver
{
    public class SProc
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        //private static string SP_GETLIST_CS = "crm3_countrysaver_getlist";
        private static string SP_GETLIST_CS = "crm3_countrysaver_getlist_new";
        //private static string SP_GETLIST_CS_NEW = "crm3_countrysaver_getlist_new_V1";
        private static string SP_GETLIST_CS_NEW_V2 = "crm3_countrysaver_getlist_paym_v2";

        private static string SP_GETLIST_STATUS = "crm3_countrysaver_getstatus";
        private static string SP_SUBMIT_RENEW = "crm3_countrysaver_renew";
        private static string SP_SUBMIT_REACTIVATE = "crm3_countrysaver_reactivate";

        private static string SP_GETLIST_CS_Subscription_LIST = "Subscription_History_CountrySaver_PayG";
        private static string SP_GETLIST_CS_ActivateCancelling_LIST = "Crm3_Cancle_By_Account_Id";
        private static string SP_GETLIST_CS_REACTIVATEBYBALANCE_LIST = "CRM3_FSB_Bundle_Reactivation_V1";


        public static IEnumerable<CountrySaverList> GetListCSaver(AdditionalReqInfo Model)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<CountrySaverList>(SP_GETLIST_CS, new
                    {
                        @mobileno = Model.Mobileno,
                        @sitecode = Model.Sitecode
                    }, commandType: CommandType.StoredProcedure, commandTimeout: 300
                    );

                    return result ?? new List<CountrySaverList>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("CountrySaverList: " + ex.Message);
            }
        }


        //New by karthik
        public static IEnumerable<CountrySaverList_New> GetListCSaver_New(AdditionalReqInfo Model)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    //var result = conn.Query<CountrySaverList_New>(SP_GETLIST_CS_NEW, new
                    var result = conn.Query<CountrySaverList_New>(SP_GETLIST_CS_NEW_V2, new
                    {
                        @mobileno = Model.Mobileno,
                        @sitecode = Model.Sitecode
                    }, commandType: CommandType.StoredProcedure, commandTimeout: 300
                    );

                    return result ?? new List<CountrySaverList_New>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("CountrySaverList: " + ex.Message);
            }
        }

        //Cancelling mode change code logic
        //New by karthik
        public static IEnumerable<Models.Common.ErrCodeMsg> ChangeCancelmode(AdditionalReqInfo Model)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Models.Common.ErrCodeMsg>(SP_GETLIST_CS_ActivateCancelling_LIST, new
                    {
                        @MAIN_MSISDN = Model.accountid,
                        @FNF_NO = Model.DestinationCli
                    }, commandType: CommandType.StoredProcedure, commandTimeout: 300
                    );

                    return result ?? new List<Models.Common.ErrCodeMsg>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("CountrySaverList: " + ex.Message);
            }
        }

        public static IEnumerable<CountrySaverPaymentHistory> GetViewSubscriptionlist(AdditionalReqInfo Model)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<CountrySaverPaymentHistory>(SP_GETLIST_CS_Subscription_LIST, new
                    {
                        @mobileno = Model.Mobileno,
                        @sitecode = Model.Sitecode,
                        @BundleId = Model.bundleid
                    }, commandType: CommandType.StoredProcedure, commandTimeout: 300
                    );

                    return result ?? new List<CountrySaverPaymentHistory>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("CountrySaverList: " + ex.Message);
            }
        }
        public static IEnumerable<Models.Common.ErrCodeMsg> Bybalnce(AdditionalReqInfo Model)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Models.Common.ErrCodeMsg>(SP_GETLIST_CS_REACTIVATEBYBALANCE_LIST, new
                    {
                        @mobileno = Model.Mobileno,
                        @sitecode = Model.Sitecode
                    }, commandType: CommandType.StoredProcedure, commandTimeout: 300
                    );

                    return result ?? new List<Models.Common.ErrCodeMsg>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("CountrySaverList: " + ex.Message);
            }
        }
        //Saved Card Details
        public static async Task<IEnumerable<CRM_API.Models.PaymentProfileCustomer>> GetCardList(string mobileno, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result =
                          Task.Run(() =>
                          {
                              return conn.Query<CRM_API.Models.PaymentProfileCustomer>(
                                      "crm3_get_customer_payment_profile", new
                                      {
                                          @msisdn = mobileno,
                                          @sitecode = sitecode
                                      }, commandType: CommandType.StoredProcedure);
                          });
                    return await result ?? new List<CRM_API.Models.PaymentProfileCustomer>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.LLOTG.SProc.GetCardList: " + ex.Message);
                throw new Exception("CRM_API.DB.LLOTG.SProc.GetCardList: " + ex.Message);
            }
        }

        public static CRM_API.Models.errmessage Reactivate(string mobileno, string sitecode, string paymentreferance, int payment_status, int use_for_renewal)
        {

            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.errmessage>(
                        "CRM3_FSB_Bundle_Reactivation_BY_CC", new
                        {
                            @Mobile_No = mobileno,
                            @Site_Code = sitecode,
                            @merchant_ref_code = paymentreferance,
                            @payment_status = payment_status,
                            @use_for_renewal = use_for_renewal
                        }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    return result ?? new CRM_API.Models.errmessage();
                }

            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.LLOTG.SProc.Reactivate: " + ex.Message);
                throw new Exception("CRM_API.DB.LLOTG.SProc.Reactivate: " + ex.Message);
            }
        }

        public static CRM_API.Models.errmessage ChangeToPendingStatus(string main_msisdn, string dest_msisdn, string reference_id)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.errmessage>(
                        "crm_country_saver_reactivation_log_insert", new
                        {
                            @main_msisdn = main_msisdn,
                            @dest_msisdn = dest_msisdn,
                            @reference_id = reference_id
                        }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    return result ?? new CRM_API.Models.errmessage();
                }

            }
            catch (Exception ex)
            {
                Log.Error("CRM_API.DB.LLOTG.SProc.Reactivate: " + ex.Message);
                throw new Exception("CRM_API.DB.LLOTG.SProc.Reactivate: " + ex.Message);
            }
        }


        public static IEnumerable<StatusList> GetListStatus(AdditionalReqInfo Model)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<StatusList>(SP_GETLIST_STATUS, new
                    {
                        @sitecode = Model.Sitecode
                    }, commandType: CommandType.StoredProcedure
                    );

                    return result ?? new List<StatusList>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("CountrySaverList: " + ex.Message);
            }
        }

        public static Models.Common.ErrCodeMsg SubmitRenew(AdditionalReqInfo Model)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var p = new DynamicParameters();
                    p.Add("@reg_idx", Convert.ToInt32(Model.RegIDX));
                    p.Add("@sitecode", Model.Sitecode);
                    p.Add("@errcode", dbType: DbType.Int32, size: 3, direction: ParameterDirection.Output);
                    p.Add("@errmessage", dbType: DbType.String, size: 255, direction: ParameterDirection.Output);
                    conn.Execute(SP_SUBMIT_RENEW, p, null, null, CommandType.StoredProcedure);

                    Model.errcode = p.Get<Int32>("errcode");
                    Model.errmsg = p.Get<string>("errmessage");

                    return new Models.Common.ErrCodeMsg()
                    {
                        errcode = Model.errcode,
                        errmsg = Model.errmsg
                    };
                }
            }
            catch (Exception ex)
            {
                return new Models.Common.ErrCodeMsg()
                {
                    errcode = -1,
                    errmsg = ex.Message
                };
            }
        }

        public static Models.Common.ErrCodeMsg SubmitReactivate(AdditionalReqInfo Model)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var p = new DynamicParameters();
                    p.Add("@reg_idx", Convert.ToInt32(Model.RegIDX));
                    p.Add("@sitecode", Model.Sitecode);
                    p.Add("@errcode", dbType: DbType.Int32, size: 3, direction: ParameterDirection.Output);
                    p.Add("@errmessage", dbType: DbType.String, size: 255, direction: ParameterDirection.Output);
                    p.Add("@paymode", Convert.ToInt32(Model.Paymode));
                    conn.Execute(SP_SUBMIT_REACTIVATE, p, null, null, CommandType.StoredProcedure);

                    Model.errcode = p.Get<Int32>("errcode");
                    //Model.errcode = Convert.ToInt32(p.Get<string>("errcode"));      // param in DB varchar
                    Model.errmsg = p.Get<string>("errmessage");

                    return new Models.Common.ErrCodeMsg()
                    {
                        errcode = Model.errcode,
                        errmsg = Model.errmsg
                    };
                }

            }
            catch (Exception ex)
            {
                return new Models.Common.ErrCodeMsg()
                {
                    errcode = -1,
                    errmsg = ex.Message
                };
            }
        }

        public static Models.Common.ErrCodeMsg SubmitCancel(AdditionalReqInfo Model)
        {
            try
            {
                return new Models.Common.ErrCodeMsg()
                {
                    errcode = 0,
                    errmsg = "Cancel Country Saver Couldn't be done by CRM"
                };

            }
            catch (Exception ex)
            {
                return new Models.Common.ErrCodeMsg()
                {
                    errcode = -1,
                    errmsg = ex.Message
                };
            }
        }
    }
}
