﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;
using System.IO;
using System.Net.Mail;
using System.Configuration;

namespace CRM_API.DB
{
    public class Home
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();


        public static IEnumerable<CRM_API.Models.SearchAgentPerformanceOutput> SearchAgentPerformance(string fromDate, string toDate)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.SearchAgentPerformanceOutput>(
                            "email_queue_search_agent_performance", new
                            {
                                @date_from = fromDate != "" ? fromDate : null,
                                @date_to = toDate != "" ? toDate : null
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.SearchAgentPerformanceOutput>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("SearchAgentPerformance() : " + ex.Message);
                throw new Exception("SearchAgentPerformance() : " + ex.Message);
            }
        }

        public static IEnumerable<CRM_API.Models.SearchUnresolvedTaskOutput> SearchUnresolvedTask(string fromDate, string toDate)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.SearchUnresolvedTaskOutput>(
                            "email_queue_search_unresolved_task", new
                            {
                                @date_from = fromDate != "" ? fromDate : null,
                                @date_to = toDate != "" ? toDate : null
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.SearchUnresolvedTaskOutput>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("SearchUnresolvedTask() : " + ex.Message);
                throw new Exception("SearchUnresolvedTask() : " + ex.Message);
            }
        }


        public static IEnumerable<CRM_API.Models.SearchOverviewTaskOutput> SearchOverviewTask(string fromDate, string toDate)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.SearchOverviewTaskOutput>(
                            "email_queue_search_overview_task", new
                            {
                                @date_from = fromDate != "" ? fromDate : null,
                                @date_to = toDate != "" ? toDate : null
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.SearchOverviewTaskOutput>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("SearchUnresolvedTask() : " + ex.Message);
                throw new Exception("SearchUnresolvedTask() : " + ex.Message);
            }
        }
        
        public static IEnumerable<CRM_API.Models.SearchGrapViewOutput> SearchGrapView(string fromDate, string toDate)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.SearchGrapViewOutput>(
                            "email_queue_search_grap_view", new
                            {
                                @date_from = fromDate != "" ? fromDate : null,
                                @date_to = toDate != "" ? toDate : null
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.SearchGrapViewOutput>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("SearchGrapView() : " + ex.Message);
                throw new Exception("SearchGrapView() : " + ex.Message);
            }
        }
    }
}
