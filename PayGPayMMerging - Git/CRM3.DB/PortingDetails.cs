﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;
using CRM_API.Models.PortingModel;

namespace CRM_API.DB.Porting
{
    public class PortingDetails
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public static PortingModelOutput GetPortingDetails(string mobileno, string sitecode)
        {            
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<PortingModelOutput>(
                            "crm_get_mnp_related_details", new
                            {
                                @mobileno = mobileno,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    return result ?? new PortingModelOutput();
                }
            }
            catch (Exception ex)
            {
                Log.Error("BL GetPortingDetails: {0}", ex.Message);
                throw new Exception("BL GetPortingDetails: " + ex.Message);
            }
        }
        public static IEnumerable<PortingHistoryModelOutput> GetPortingHistoryDetails(string mobileno, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<PortingHistoryModelOutput>(
                            "crm_portin_ballance_breakup_by_mobileno", new
                            {
                                @mobileno = mobileno,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<PortingHistoryModelOutput>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("BL GetPortingHistoryDetails: {0}", ex.Message);
                throw new Exception("BL GetPortingHistoryDetails: " + ex.Message);
            }
        }
    }
}