﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;

namespace CRM_API.DB.Enquiry
{

    // Added By Saroj On 22/05/2019

    public class SProc
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        //User Role
        public static IEnumerable<UserRole> GetUserRoleInfo()
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var result = conn.Query<UserRole>(
                            "crm_get_user_role_info", new
                            {

                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<UserRole>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("BL crm_get_user_role_info: {0}", ex.Message);
                throw new Exception("BL crm_get_user_role_info: " + ex.Message);
            }
        }
        public static IEnumerable<EnquiryOutput> EnquiryDetails(EnquiryInput objEnquiryInput)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var result = conn.Query<EnquiryOutput>(
                            "crm_get_enqiry_info", new
                            {
                                @date_from = objEnquiryInput.date_from,
                                @date_to = objEnquiryInput.date_to,
                                @Enquiry_ID = objEnquiryInput.Enquiry_ID,
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<EnquiryOutput>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("BL crm_get_enqiry_info: {0}", ex.Message);
                throw new Exception("BL crm_get_enqiry_info: " + ex.Message);
            }
        }
        public static IEnumerable<EnquiryComments> EnquiryCommentsInfo(EnquiryInput objEnquiryInput)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var result = conn.Query<EnquiryComments>(
                            "crm_get_enqiry_comments_info", new
                            {
                                @enqiry_id = objEnquiryInput.Enquiry_ID 
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<EnquiryComments>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("BL crm_get_enqiry_info: {0}", ex.Message);
                throw new Exception("BL crm_get_enqiry_info: " + ex.Message);
            }
        }

        public static IEnumerable<EnquiryOutput> EnquiryDetailsbyID(EnquiryInput objEnquiryInput)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var result = conn.Query<EnquiryOutput>(
                            "crm_get_enqiry_info_ID", new
                            {   
                                @Enquiry_ID = objEnquiryInput.Enquiry_ID
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<EnquiryOutput>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("BL crm_get_enqiry_info: {0}", ex.Message);
                throw new Exception("BL crm_get_enqiry_info: " + ex.Message);
            }
        }
        public static IEnumerable<CommonModels> InsertHistory(EnquiryInput objEnquiryInput)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var result = conn.Query<CommonModels>(
                            "crm_create_enqiry_comments", new
                            {
                                @enqiry_id = objEnquiryInput.Enquiry_ID,
                                @comments = objEnquiryInput.Comments,
                                @create_by = objEnquiryInput.create_by

                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CommonModels>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("BL crm_get_enqiry_info: {0}", ex.Message);
                throw new Exception("BL crm_get_enqiry_info: " + ex.Message);
            }
        }
        public static EnquiryInput NewEnquiryInsert(string ENQUIRER_NAME, string MOBILENO, string EMAIL, string EXISTING_NETWORK, string COUNTRY, int ENQUIRY_TYPE, string QUERY, string COMMENTS, int ACTION, int ENQUIRY_ID, int FK_STATUS_ID, int FK_AGENT_ID, int FK_CLOSE_AGENT)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var result = conn.Query<EnquiryInput>(
                            "crm_create_enqiry_detail", new
                            {
                                @Enquirer_Name = ENQUIRER_NAME,
                                @Mobileno = MOBILENO,
                                @Email = EMAIL,
                                @Existing_Network = EXISTING_NETWORK,
                                @Country = COUNTRY,
                                @Enquiry_Type = ENQUIRY_TYPE,
                                @Query = QUERY,
                                @Comments = COMMENTS,
                                @Action = ACTION,
                                @Enquiry_ID = ENQUIRY_ID,
                                @fk_status_id = FK_STATUS_ID,
                                @fk_agent_id = FK_AGENT_ID
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    return result ?? new EnquiryInput();
                }
            }
            catch (Exception ex)
            {
                Log.Error("BL crm_create_enqiry_detail: {0}", ex.Message);
                throw new Exception("BL crm_create_enqiry_detail: " + ex.Message);
            }
        }

        public static IEnumerable<EnquiryCountry> GetCountryInfo()
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var result = conn.Query<EnquiryCountry>(
                            "crm3_get_all_country_list", new
                            {

                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<EnquiryCountry>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("BL GetCountryInfo: {0}", ex.Message);
                throw new Exception("BL GetCountryInfo: " + ex.Message);
            }
        }
         public static IEnumerable<EnquiryType> GetEnquiryType()
         {
             try
             {
                 using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                 {
                     conn.Open();
                     var result = conn.Query<EnquiryType>(
                             "crm_get_enqiry_type", new
                             {

                             }, commandType: CommandType.StoredProcedure);

                     return result ?? new List<EnquiryType>();
                 }
             }
             catch (Exception ex)
             {
                 Log.Error("BL EnquiryType: {0}", ex.Message);
                 throw new Exception("BL EnquiryType: " + ex.Message);
             }
         }
        //BindStatus
         public static IEnumerable<BindStatus> GetBindStatus(string userid)
         {
             try
             {
                 using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                 {
                     conn.Open();
                     var result = conn.Query<BindStatus>(
                             "email_get_queue_status", new
                             {
                                 @userid = userid,
                             }, commandType: CommandType.StoredProcedure);

                     return result ?? new List<BindStatus>();
                 }
             }
             catch (Exception ex)
             {
                 Log.Error("BL GetBindStatus: {0}", ex.Message);
                 throw new Exception("BL GetBindStatus: " + ex.Message);
             }
         }
    }
}