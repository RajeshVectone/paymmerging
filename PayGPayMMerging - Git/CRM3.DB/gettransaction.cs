﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace CRM_API.DB
{
    public class gettransaction
    {

        public static IEnumerable<CRM_API.Models.marketinggrid> geterrrCodedownload(string spName, string paymentCode)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.MCMCRMConnectionMarketing))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.marketinggrid>(spName, new
                    {
                        @mode = paymentCode
                    }, commandType: CommandType.StoredProcedure
                    );

                    return result ?? new List<CRM_API.Models.marketinggrid>();
                }
            }
            catch (SqlException ex)
            {
                throw new Exception(spName + "   :  " + ex.Message);
            }
        }


        public static IEnumerable<CRM_API.Models.marketinggrid_v2> geterrrCodedownload_v1(string product, string type, string fromdate, string todate, string loggedin, string toppedup)
        {
            try
            {
                

                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    
                    conn.Open();
                    
                    var result = conn.Query<CRM_API.Models.marketinggrid_v2>("Rpt_get_email_marketing_reports", new
                    {
                        @product = product,
                        @type=type,
                        @from_date=fromdate,
                        @to_date=todate,
                        @logged_in=loggedin,
                        @topped_up=toppedup
                    }, commandType: CommandType.StoredProcedure ,commandTimeout:0
                    );

                    return result ?? new List<CRM_API.Models.marketinggrid_v2>();
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("Rpt_get_email_marketing_reports" + "   :  " + ex.Message);
            }
        }
        public static IEnumerable<CRM_API.Models.DDgoSubmit> GocardDownload(string Mode , string sitecode)
        {
            try
            {
                string Status = "";
                if (Mode == "1")
                {
                    Status = "pending_submission";
                }
                else if (Mode == "2")
                {
                    Status = "paid_out";
                }
                else if (Mode == "3")
                {
                    Status = "Failed";
                }
                string ConnStr = "";
                if (sitecode.ToString().ToUpper() == "VMUK")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnection.ToString();
                }
                if (sitecode.ToString().ToUpper() == "VMNL")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnectionVMNL.ToString();
                }
                if (sitecode.ToString().ToUpper() == "VMFR")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnectionVMFR.ToString();
                }
                if (sitecode.ToString().ToUpper() == "VMAT")
                {
                    ConnStr = CRM_API.Helpers.Config.MCMCRMConnectionVMAT.ToString();
                }
                using (var conn = new SqlConnection(ConnStr))
                {
                    conn.Open();
                    var result = conn.Query<CRM_API.Models.DDgoSubmit>("gocard_dd_cust_transaction_details", new { @status = Status ,@period= ""},
                        commandType: CommandType.StoredProcedure);
                    return result ?? new List<CRM_API.Models.DDgoSubmit>();

                }
            }
            catch (SqlException ex)
            {
                throw new Exception("gocard_dd_cust_transaction_details   :  " + ex.Message);
            }
        }
    }
}
