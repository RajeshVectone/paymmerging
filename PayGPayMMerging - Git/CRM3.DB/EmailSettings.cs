﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;
using System.IO;
using System.Net.Mail;

namespace CRM_API.DB
{
    public class EmailSettings
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public static IEnumerable<CRM_API.Models.EmailSettingsCommon> InsertSubCategory(EmailSettingsModel model)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.EmailSettingsCommon>(
                            "crm_create_issue_complaint_master", new
                            {
                                @function_Id = model.function_Id,
                                @function_code = model.function_code,
                                @Complaint_Code = model.Complaint_Code,
                                @Complaint_Name = model.Complaint_Name,
                                @process_type = model.process_type,
                                @Complaint_Id = model.Complaint_Id
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.EmailSettingsCommon>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("InsertSubCategory() : " + ex.Message);
                throw new Exception("InsertSubCategory() : " + ex.Message);
            }
        }


        public static IEnumerable<CRM_API.Models.EmailSettingsCommon> InsertCategory(EmailSettingsModel model)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.EmailSettingsCommon>(
                            "crm_create_issue_function_master", new
                            {
                                @issue_id = model.issue_id,
                                @function_code = model.function_code,
                                @Function_Name = model.Function_Name,
                                @process_type = model.process_type,
                                @issue_function_id = model.issue_function_id
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.EmailSettingsCommon>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("InsertCategory() : " + ex.Message);
                throw new Exception("InsertCategory() : " + ex.Message);
            }
        }

        public static IEnumerable<CRM_API.Models.EmailSettingsCommon> InsertTicketType(EmailSettingsModel model)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.EmailSettingsCommon>(
                            "crm_create_issue_type_master", new
                            {
                                @Issue_Code = model.Issue_Code,
                                @Issue_Name = model.Issue_Name,
                                @Issue_Desc = model.Issue_Desc,
                                @process_type = model.process_type,
                                @issue_type_id = model.issue_type_id
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.EmailSettingsCommon>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("InsertTicketType() : " + ex.Message);
                throw new Exception("InsertTicketType() : " + ex.Message);
            }
        }

        public static IEnumerable<CRM_API.Models.EmailSettingsModel> GetRuleAssigning()
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.EmailSettingsModel>(
                            "email_get_create_rule", new
                            {

                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.EmailSettingsModel>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetEmailAssigning() : " + ex.Message);
                throw new Exception("GetEmailAssigning() : " + ex.Message);
            }
        }

        public static IEnumerable<CRM_API.Models.RuleAssigningOutput> UpdateRuleAssigning(EmailSettingsModel model)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.RuleAssigningOutput>(
                            "email_queue_create_rule", new
                            {
                                @queue_type = model.queue_type,
                                @queue_per_user = model.queue_per_user,
                                @processtype = model.processtype,
                                @template_id = model.template_id,
                                @esclation_delay = model.esclation_delay
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.RuleAssigningOutput>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("UpdateRuleAssigning() : " + ex.Message);
                throw new Exception("UpdateRuleAssigning() : " + ex.Message);
            }
        }

        public static IEnumerable<CRM_API.Models.SMSAssigningOutput> GetSMSAssigning()
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.SMSAssigningOutput>(
                            "email_queue_get_sms_template", new
                            {

                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.SMSAssigningOutput>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetSMSAssigning() : " + ex.Message);
                throw new Exception("GetSMSAssigning() : " + ex.Message);
            }
        }

        public static IEnumerable<CRM_API.Models.EmailSettingsCommon> UpdateSMSAssigning(EmailSettingsModel model)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.EmailSettingsCommon>(
                            "email_queue_create_sms_template", new
                            {
                                @sms_text = model.sms_text,
                                @status = model.status,
                                @template_name = model.template_name,
                                @processtype = model.processtype,
                                @template_id = model.template_id
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.EmailSettingsCommon>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("UpdateSMSAssigning() : " + ex.Message);
                throw new Exception("UpdateSMSAssigning() : " + ex.Message);
            }
        }

        public static IEnumerable<CRM_API.Models.EmailAssigningOutput> GetEmailAssigning()
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.EmailAssigningOutput>(
                            "email_queue_get_email_template", new
                            {

                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.EmailAssigningOutput>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetEmailAssigning() : " + ex.Message);
                throw new Exception("GetEmailAssigning() : " + ex.Message);
            }
        }

        public static IEnumerable<CRM_API.Models.EmailSettingsCommon> UpdateEmailAssigning(string email_subject,string filename, int status, string template_name,int processtype, int template_id, string email_blob )
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    var result = conn.Query<CRM_API.Models.EmailSettingsCommon>(
                            "email_queue_create_email_template", new
                            {
                                @email_subject = email_subject,
                                @filename = filename,
                                @status = status,
                                @template_name = template_name,
                                @processtype = processtype,
                                @template_id = template_id,
                                @email_blob = email_blob
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CRM_API.Models.EmailSettingsCommon>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("UpdateEmailAssigning() : " + ex.Message);
                throw new Exception("UpdateEmailAssigning() : " + ex.Message);
            }
        }
    }
}
