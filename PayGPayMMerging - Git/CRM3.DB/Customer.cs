﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using CRM_API.Models;
using NLog;
using Newtonsoft.Json;

namespace CRM_API.DB.Customer
{
    public class SProc
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public static IEnumerable<CustomerSearchResult> Search(string searchBy, string searchValue)
        {            
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<CustomerSearchResult>(
                            "crm3_search_customer_v2", new
                            {
                                @country = Helpers.Session.CurrentUser.site_code,
                                @searchby = searchBy,
                                @searchvalue = searchValue
                            }, commandType: CommandType.StoredProcedure);                    

                    return result ?? new List<CustomerSearchResult>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("BL Search: {0}", ex.Message);
                throw new Exception("BL Search: " + ex.Message);
            }
        }
        public static IEnumerable<CustomerSearchResult> Search_v2(string product_code, string searchBy, string searchValue)
        {
            if (searchValue.Contains("PAY") == true)
            {
                searchValue = searchValue.Split('-')[0].ToString();
            }
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    //05-Jan-2016 : Moorthy Modified to use the same SP for all countries
                    ////07-Aug-2015 : Moorthy Added to get the Sim_Type (Connection Type)
                    //if (product_code == "VMUK")
                    //{
                        var result = conn.Query<CustomerSearchResult>(
                                "crm3_search_customer_PO_Dual", new
                                {
                                    @mundio_product = product_code,
                                    @searchby = searchBy,
                                    @searchvalue = searchValue
                                }, commandType: CommandType.StoredProcedure , commandTimeout:300 );
                        return result ?? new List<CustomerSearchResult>();
                    //}
                    //else
                    //{
                    //    var result = conn.Query<CustomerSearchResult>(
                    //            "crm3_search_customer_V1", new
                    //            {
                    //                @mundio_product = product_code,
                    //                @searchby = searchBy,
                    //                @searchvalue = searchValue
                    //            }, commandType: CommandType.StoredProcedure);
                    //    return result ?? new List<CustomerSearchResult>();
                    //}
                }
            }
            catch (Exception ex)
            {
                Log.Error("BL Search_v2: {0}", ex.Message);
                //throw new Exception("BL Search_v2: " + ex.Message);
                return new List<CustomerSearchResult>();
            }
        }

        public static IEnumerable<CustomerSearchResult> CustomerDetail(string product_code, string mobileno)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<CustomerSearchResult>(
                            "crm3_mobile_cust_info", new
                            {
                                @mundio_product = product_code,
                                @mobile_no = mobileno
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<CustomerSearchResult>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("BL CRM_API.DB.Customer.CustomerDetail: {0}", ex.Message);
                throw new Exception("BL CRM_API.DB.Customer.CustomerDetail: " + ex.Message);
            }
        }

        public static Models.CustomerSearchResult Search_Cust(string product_code, string searchBy, string searchValue)
        {
            try
            {
                //string country = System.Web.HttpContext.Current.Session["GlobalProduct"].ToString();
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<CustomerSearchResult>(
                            "crm3_search_customer_V1", new
                            {
                                @mundio_product = product_code,
                                @searchby = searchBy,
                                @searchvalue = searchValue
                            }, commandType: CommandType.StoredProcedure);

                    return result.FirstOrDefault() ?? new Models.CustomerSearchResult();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BL CRM_API.DB.CustomerSearchResult.Search_Cust: " + ex.Message);
            }
        }

        public static MvnoAccount SubscriberInfo(string mobileno, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    // Old SP:crm3_esp_mvno_account_list_by_mobileNo
                    //Port out SP:crm3_esp_mvno_account_list_by_mobileNo_Po
                    conn.Open();

                    var result = conn.Query<MvnoAccount>(
                            "crm3_esp_mvno_account_list_by_mobileNo_Po", new
                            {
                                @mobileno = mobileno,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    return result ?? new MvnoAccount();
                }
            }
            catch (Exception ex)
            {
                Log.Error("BL SubscriberInfo: {0}", ex.Message);
                throw new Exception("BL SubscriberInfo: " + ex.Message);
            }
        }
        public static CustomerSearchResult CustomerPersonalInfo(string product_code, string mobileno,string sitecode)
        {
            try
            {
                var subsInfo = SubscriberInfo(mobileno, sitecode);
                var espinfo = ESPAccountInfo("ICS", subsInfo.custcode, subsInfo.batchcode, subsInfo.serialcode, sitecode);

                //06-Feb-2016 : Modified : For 447451044498 mobile no product code changed to VMUK
                //var product = CRM_API.DB.Product.ProductSProc.GetProductByMobile(mobileno);
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<CustomerSearchResult>(
                            //"crm3_subs_search_bycode", new
                            "crm3_subs_search_bycode_v2", new
                            {
                                @custcode = subsInfo.custcode,
                                @batchcode = subsInfo.batchcode,
                                @serialcode = subsInfo.serialcode,
                                @sitecode = sitecode,
                                //CRM Enhancement 2
                                @mobileno = mobileno
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    if (result != null)
                    {
                        result.SimType = espinfo.SIMType;
                        //result.product = product.mundio_product ?? product_code;
                        //result.product = product_code;
                        result.product = result.brand;
                        //06-Feb-2017 : Moorthy : Modified for 'input string not in correct format'
                        result.balance = Decimal.Parse(espinfo.Balance.ToString(),  System.Globalization.NumberStyles.AllowExponent|System.Globalization.NumberStyles.AllowDecimalPoint | System.Globalization.NumberStyles.AllowLeadingSign);//decimal.Parse(espinfo.Balance.ToString());
                        //25-Dec-2018 : Moorthy : Get Bonus Balance
                        result.bonus_balance = Decimal.Parse(espinfo.bonus_balance.ToString(), System.Globalization.NumberStyles.AllowExponent | System.Globalization.NumberStyles.AllowDecimalPoint);
                    }
                    else
                    {
                        result = new CustomerSearchResult();
                        result.SimType = espinfo.SIMType;
                        //result.product = product.mundio_product ?? product_code;
                        result.product = product_code;
                        //06-Feb-2017 : Moorthy : Modified for 'input string not in correct format'
                        result.balance = Decimal.Parse(espinfo.Balance.ToString(), System.Globalization.NumberStyles.AllowExponent | System.Globalization.NumberStyles.AllowDecimalPoint | System.Globalization.NumberStyles.AllowLeadingSign);//decimal.Parse(espinfo.Balance.ToString());
                        //25-Dec-2018 : Moorthy : Get Bonus Balance
                        result.bonus_balance = Decimal.Parse(espinfo.bonus_balance.ToString(), System.Globalization.NumberStyles.AllowExponent | System.Globalization.NumberStyles.AllowDecimalPoint);
                    }

                    return result ?? new CustomerSearchResult();
                }
            }
            catch (Exception ex)
            {
                Log.Error("BL CustomerPersonalInfo: {0}", ex.Message);
                throw new Exception("BL CustomerPersonalInfo: " + ex.Message);
            }
        }
        public static ESPAccount ESPAccountInfo(string telcode, string custcode, int batchcode, int serialcode, string sitecode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<ESPAccount>(
                            "crm3_rp1_view_account_detail_bycode", new
                            {
                                @telcocode = telcode,
                                @custcode = custcode,
                                @batchcode = batchcode,
                                @serialcode = serialcode,
                                @sitecode = sitecode
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    return result ?? new ESPAccount();
                }
            }
            catch (Exception ex)
            {
                Log.Error("BL ESPAccountInfo: {0}", ex.Message);
                throw new Exception("BL ESPAccountInfo: " + ex.Message);
            }
        }

        public static CustomerSearchResult CustomerDetailInfo(string product_code, string mobileno, string mode)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    //5Jan2015 : Moorthy Modeified the SP 'crm3_mobile_cust_info' to 'crm3_mobile_cust_info_PO_Dual'
                    var result = conn.Query<CustomerSearchResult>(
                            "crm3_mobile_cust_info_PO_Dual", new
                            {
                                @mundio_product = product_code,
                                @mobile_no = mobileno,
                                @mode = mode
                            }, commandType: CommandType.StoredProcedure , commandTimeout:300).FirstOrDefault();

                    return result ?? new CustomerSearchResult();
                }
            }
            catch (Exception ex)
            {
                Log.Error("BL CustomerDetailInfo: {0}", ex.Message);
                throw new Exception("BL CustomerDetailInfo: " + ex.Message);
            }
        }
        //Added by karthik Jira:CRMT-196
        public static CustomerTariffdetails CustomertariffInfo(string product_code, string mobile_no)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    //Old SP:crm3_Tariff_cust_info  

                    //New SP:crm3_Tariff_cust_info_V1  /Added extra two fileed/
                    var result = conn.Query<CustomerTariffdetails>(
                            "crm3_Tariff_cust_info", new
                            {
                                @product_code = product_code,
                                @mobile_no = mobile_no
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    return result ?? new CustomerTariffdetails();

                }
            }
            catch (Exception ex)
            {
                Log.Error("BL CustomertariffDetailInfo: {0}", ex.Message);
                throw new Exception("BL CustomertariffDetailInfo: " + ex.Message);
            }
        }
        public static CustomerSearchResult CustomerDetailInfo1(string mobileno)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<CustomerSearchResult>(
                            "Get_Productcode_By_Mobileno", new
                            {
                                @mobileno = mobileno
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    return result ?? new CustomerSearchResult();
                }
            }
            catch (Exception ex)
            {
                Log.Error("BL CustomerDetailInfo: {0}", ex.Message);
                throw new Exception("BL CustomerDetailInfo: " + ex.Message);
            }
        }
        public static CustomerSearchResult GetByMobileNo(string mobileNo)
        {
            var resValues = Search("MobileNo", mobileNo);
            return resValues != null ? resValues.LastOrDefault() : null;
        }

        public static CustomerSearchResult GetByMobileNo(string mobileNo, string product_code)
        {
            var resValues = Search_v2(product_code,"MobileNo", mobileNo);
            //return resValues != null ? resValues.LastOrDefault() : null;
            return resValues != null ? resValues.LastOrDefault() : null;
        }

        public static int GetSIMVersion(string mobileNo)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = (conn.Query<int>(
                            "crm3_get_sim_version ", new
                            {
                                @mobileno = mobileNo
                            }, commandType: CommandType.StoredProcedure)).FirstOrDefault();

                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("BL GetSIMVersion: {0}", ex.Message);
                return 0;
            }
        }

        public static IEnumerable<Customer2in1Data> List2in1Order(string mobileNo)
        {
            try
            {
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Customer2in1Data>(
                            "crm3_2in1_search_orders ", new
                            {
                                @mobileno = mobileNo
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<Customer2in1Data>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("BL List2in1Order: {0}", ex.Message);
                throw new Exception("BL List2in1Order: " + ex.Message);
            }
        }

        public static IEnumerable<HistoryLog> History2in1Order(string mobileNo)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<HistoryLog>(
                            "crm3_2in1_search_history ", new
                            {
                                @mobileno = mobileNo
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<HistoryLog>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("BL History2in1Order: {0}", ex.Message);
                throw new Exception("BL History2in1Order: " + ex.Message);
            }
        }

        public static Customer2in1Data Get2in1Order(int orderId)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Customer2in1Data>(
                            "crm3_2in1_get_order ", new
                            {
                                @orderId = orderId
                            }, commandType: CommandType.StoredProcedure);

                    return result.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Log.Error("BL Get2in1Order: {0}", ex.Message);
                return new Customer2in1Data();
            }
        }

        public static Customer2in1Order Get2in1OrderDelivery(int orderId)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Customer2in1Order>(
                            "crm3_2in1_get_order_delivey ", new
                            {
                                @orderId = orderId
                            }, commandType: CommandType.StoredProcedure);

                    return result.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Log.Error("BL Get2in1OrderDelivery: {0}", ex.Message);
                return new Customer2in1Order();
            }
        }

        public static Common.ErrCodeMsg UpdateCustomer(CRM_API.Models.CustomerUpdatedata Result)
        {
         try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<Common.ErrCodeMsg>(
                            "crm3_subs_update", new
                            {
                                @subscriberid = Result.SubscriberID,
                                @title = Result.Title,
                                @firstname = Result.FirstName,
                                @lastname = Result.LastName,
                                @personnumber = string.Empty,
                                @personalidtype = 1,
                                @houseno = Result.Houseno,
                                @address1 = Result.address1,
                                @address2 = Result.address2,
                                @city = Result.city,
                                @postcode = Result.postcode,
                                @state = string.Empty,
                                @countrycode = Result.country,
                                @telephone = Result.Telephone,
                                @mobilephone = string.Empty,
                                @fax = string.Empty,
                                @email = Result.Email,
                                @birthdate = Result.BirthDate,
                                @question = string.Empty,
                                @answer = Result.SecurityAnswer,
                                @subscribertype = 1,
                                @referenceno = string.Empty,
                                @comment = string.Empty,
                                @sitecode = Result.sitecode
                                
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    Log.Debug("Update Customer: {0}",  result.errmsg);
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("Update Customer BL: {0}", ex.Message);
                return new Common.ErrCodeMsg() { errcode = -1, errmsg = ex.Message };
            }

        }

        public static int InsertCustomer(CRM_API.Models.CustomerSearchResult In,string sitecode,string mobileno)
        {
            try
            {
                string CountryCode = CRM_API.Helpers.MVNOAccount.MVNOAccount.GetCountryCode(sitecode); 
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();

                    var result = conn.Query<int>(
                            "crm3_subs_create_line", new
                            {
                                @title = string.Empty ,
                                @firstname = In.first_name,
                                @lastname = In.last_name,
                                @houseno = In.Houseno == string.Empty ? string.Empty : In.Houseno,
                                @address1 = In.address1 == string.Empty ? string.Empty : In.address1,
                                @address2 = In.address2 == string.Empty ? string.Empty : In.address2,
                                @city = In.city == string.Empty ? string.Empty : In.city,
                                @postcode = In.postcode == string.Empty ? string.Empty : In.postcode,
                                @state = string.Empty,
                                @countrycode = CountryCode,
                                @telephone = string.Empty,
                                @mobilephone = mobileno,
                                @fax = string.Empty,
                                @email = In.Email == string.Empty?string.Empty:In.Email,
                                @birthdate = In.dob.ToString() == string.Empty? DateTime.Now : In.dob,
                                @question = string.Empty,
                                @answer = string.Empty,
                                @subscribertype = 1,
                                @mobileno = mobileno,
                                @iccid = In.iccid,
                                @sitecode = sitecode

                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    
                    return result;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }

        }

        //Cancel Location
        public static IEnumerable<SimDetails> SimDetails(string ICCID,string SiteCode)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var result = conn.Query<SimDetails>(
                            "crm3_crm_get_sim_details", new
                            {
                                @iccid = ICCID,
                                @sitecode = SiteCode
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<SimDetails>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("BL crm3_crm_get_sim_details: {0}", ex.Message);
                throw new Exception("BL crm3_crm_get_sim_details: " + ex.Message);
            }
        }

        public static CustomerSearchResult NewUserInsert(int id, string Username, string Password, string Status, string AssignEmails, string FirstName, string LastName, string Email, string CrtUsername, string Permis, int role_id)
        {
            try
            {
                int opr_mode = 1;
                if (id > 0 )
                {
                    opr_mode = 2; 
                }
                using (var conn = new SqlConnection(Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var result = conn.Query<CustomerSearchResult>(
                            "crm3_auth_users_register", new
                            {
                                @user_login =Username,
                                @user_password =Password,
                                @login_status = Status,
                                @assign_email = AssignEmails,
                                @first_name = FirstName,
                                @last_name =LastName,
                                @email  =Email,
                                @created_by =CrtUsername,
                                @role_id =Permis,
                                @sitecode = "MCM",
                                @opr_mode = opr_mode,
                                @user_role = role_id
                            }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    return result ?? new CustomerSearchResult();
                }
            }
            catch (Exception ex)
            {
                Log.Error("BL CustomerDetailInfo: {0}", ex.Message);
                throw new Exception("BL CustomerDetailInfo: " + ex.Message);
            }
        }

        //User Role
        public static IEnumerable<UserRole> GetUserRoleInfo()
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var result = conn.Query<UserRole>(
                            "crm_get_user_role_info", new
                            {
                               
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<UserRole>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("BL crm_get_user_role_info: {0}", ex.Message);
                throw new Exception("BL crm_get_user_role_info: " + ex.Message);
            }
        }


        //30-May-2019 : Moorthy : Added for Notification Popup
        public static IEnumerable<LatestOpenComplaints> LatestOpenComplaints(string mobileno)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var result = conn.Query<LatestOpenComplaints>(
                            "crm_get_open_tickets_by_mobileno", new
                            {
                                @mobileno = mobileno,
                                @status = 0
                            }, commandType: CommandType.StoredProcedure);

                    return result ?? new List<LatestOpenComplaints>();
                }
            }
            catch (Exception ex)
            {
                Log.Error("BL crm_get_open_tickets_by_mobileno: {0}", ex.Message);
                throw new Exception("BL crm_get_open_tickets_by_mobileno: " + ex.Message);
            }
        }
    }
}