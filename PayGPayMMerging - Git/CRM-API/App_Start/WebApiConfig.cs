﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace CRM_API
{
    public static class WebApiConfig
    {
        //// 31-Jul-2015 : Moorthy Added
        public static string UrlPrefix { get { return "api"; } }
        public static string UrlPrefixRelative { get { return "~/api"; } }

        public static void Register(HttpConfiguration config)
        {
            
            // 31-Jul-2015 : Moorthy Modified
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: WebApiConfig.UrlPrefix + "/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            // 31-Jul-2015 : Moorthy Commented
            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);
            config.Routes.MapHttpRoute(
                name: "ApiKeyValue",
                routeTemplate: "api/{controller}/{key}/{value}",
                defaults: new { key = RouteParameter.Optional, value = RouteParameter.Optional }
            );
        }
    }
}