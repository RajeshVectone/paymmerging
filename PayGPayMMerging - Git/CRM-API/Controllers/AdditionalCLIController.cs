﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NLog;
using CRM_API.Models.CountrySaver;
using System.Data.SqlClient;
using System.Data;
using System.Threading.Tasks;

namespace CRM_API.Controllers
{
    /*
    * Add / Remove Additional CLI in Country Saver tab
    *  By Hari @23-02-2015 
    *  Requirement : Add / Remove Additional CLI in Country Saver tab
    * Param : msisdn (mandatory), datefrom/dateend, recipient name
    * 
    */

    public class AdditionalCLIController : ApiController
    {
        // GET api/AdditionalCLI        
        public async Task<HttpResponseMessage> Get([FromUri]AdditionalReqInfo Model)
        {     
            //switch (Model.InfoType)
            //{
                //case AdditionInfo.GETCLIList :
                    return Request.CreateResponse(HttpStatusCode.OK, await GetCLIList(Model));
                //default: throw new EntryPointNotFoundException("Parameter InfoType Not Recognized");
            //}

        }

        private async Task<HttpResponseMessage> GetCLIList(AdditionalReqInfo Model)
        {
            return Request.CreateResponse(HttpStatusCode.OK, await GetListCLI(Model));
        }

        private async Task<IEnumerable<CRM_API.Models.CountrySaver.AdditionalCLI>> GetListCLI(AdditionalReqInfo Model)
        {
            return await Task.Run(() =>
            {
                var result = CRM_API.DB.CLI.SProc.GetListCLI(Model);
                return result;
            });
        }

        // POST api/AdditionalCLI/
        //[HttpPost]        
        public async Task<HttpResponseMessage> Post(CRM_API.Models.CountrySaver.AdditionalCLI Model)
        {
            //Log.Debug("--------------------------------------------------------------------");
            DB.Common.ErrCodeMsg errResult = new DB.Common.ErrCodeMsg()
           {
               errcode = -1,
               errsubject = "General Error",
               errmsg = "Empty New History"
           };

            //Log.Debug("AdditionalCLIController.Post: Started");
            if (Model != null)
            {
                //string mobileno = Model.Mobileno;
                //string destno = Model.Mobileno;   
                switch (Model.InfoType)
                {                   
                    case AdditionInfo.ADDCLI:
                        //Get AccountId                        
                        //Models.Common.ErrCodeMsg result = AddNewCLI(Model);
                        //return Request.CreateResponse(HttpStatusCode.OK, result);
                        return Request.CreateResponse(HttpStatusCode.OK, await AddNewCLI(Model));

                    case AdditionInfo.DELETECLI:
                        //Models.Common.ErrCodeMsg resultDelete = DeleteNewCLI(Model.SubscriberId, Model.FandFNo, Model.CLI);
                        //return Request.CreateResponse(HttpStatusCode.OK, resultDelete);
                        return Request.CreateResponse(HttpStatusCode.OK, await DeleteNewCLI(Model));
                    
                    case AdditionInfo.VALIDATECLI:
                        //Models.Common.ErrCodeMsg resultValidate = ValidateVectoneCLI(Model);
                        //return Request.CreateResponse(HttpStatusCode.OK, resultValidate);
                        return Request.CreateResponse(HttpStatusCode.OK, await ValidateVectoneCLI(Model));
                            
                    default:
                        return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.CountrySaver.AdditionalCLI>().ToArray());
                             
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.CustomerCSBHistory>().ToArray());
            }

        }

        private async Task<IEnumerable<CRM_API.Models.CountrySaver.AdditionalCLI>> GetList(AdditionalReqInfo model)
        {
            
            return await Task.Run(() =>
            {
                IEnumerable<CRM_API.Models.CountrySaver.AdditionalCLI> resAdditionalCLI = CRM_API.DB.CLI.SProc.GetListCLI(model);
                return resAdditionalCLI;
            });
        }
      
        //Add CLI
        public Models.Common.ErrCodeMsg AddCLI(CRM_API.Models.CountrySaver.AdditionalCLI cliViewModel, String accountId, String soapFormatFilePath)
        {
            try
            {
                //Log.Info("Start CSB_HSB.AddCLI {0}", cliViewModel.CLI);
                Models.Common.ErrCodeMsg errCodeMsg = new Models.Common.ErrCodeMsg();
                String status = "";
                using (WebClient client = new WebClient())
                {
                    
                    String newCLIAddUrl = null;
                    String newCLIAddxmlns = null;
                    

                    //if (accountId.Substring(0, 3).CompareTo("200") == 0)
                    //{
                        newCLIAddUrl = System.Configuration.ConfigurationSettings.AppSettings["NewCLIFreedomAddUrl"].ToString();
                        newCLIAddxmlns = System.Configuration.ConfigurationSettings.AppSettings["NewCLIFreedomAddxmlns"].ToString();
                    //}
                    //else if (accountId.Substring(0, 2).CompareTo("55") == 0)
                    //{
                    //    newCLIAddUrl = System.Configuration.ConfigurationSettings.AppSettings["NewCLIUnlimitedAddUrl"].ToString();
                    //    newCLIAddxmlns = System.Configuration.ConfigurationSettings.AppSettings["NewCLIUnlimitedAddxmlns"].ToString();
                    //}

                    List<String> fieldvalues = new List<String>();
                    fieldvalues.Add(newCLIAddxmlns);
                    fieldvalues.Add(accountId);
                    //fieldvalues.Add(cliViewModel.Mobileno);
                    fieldvalues.Add(cliViewModel.CLI);
                    fieldvalues.Add(cliViewModel.Product_Type);
                    if (!(String.IsNullOrEmpty(newCLIAddUrl)))
                    {
                        String resp = GetWebClient(newCLIAddUrl, soapFormatFilePath, fieldvalues);
                        String XmlString = resp.Replace("&lt;", "<");
                        XmlString = XmlString.Replace("&gt;", ">");
                        System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
                        xmlDoc.LoadXml(XmlString);
                        System.Xml.XmlNodeList elemList = xmlDoc.GetElementsByTagName("AddCLIResult");
                        //System.Xml.XmlNode xmlNode = xmlDoc.SelectSingleNode("string");
                        System.Xml.XmlNode xmlNode = elemList.Item(0);


                        if (xmlNode != null)
                            status = xmlNode.InnerText;
                    }
                    else
                    {
                        status = "Please enter a valid number!!";
                    }
                    /*
                    FREEDOM.FREEDOMSoapClient serv;
                    serv = new FREEDOM.FREEDOMSoapClient();
                    serv.AddCLI(accountId,cliViewModel.CLI,cliViewModel.Type);
                    */
                    /*if (FREEDOM.FREEDOMSoa)
                     */

                    /*
                    if (!(String.IsNullOrEmpty(newCLIAddUrl)))
                    {
                        byte[] respBytes = client.UploadValues(newCLIAddUrl, fields);
                        string resp = client.Encoding.GetString(respBytes);
                        String XmlString = resp.Replace("&lt;", "<");
                        XmlString = XmlString.Replace("&gt;", ">");
                        System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
                        xmlDoc.LoadXml(XmlString);
                        System.Xml.XmlNode xmlNode = xmlDoc.SelectSingleNode("string");

                        if (xmlNode != null)
                            status = xmlNode.InnerText;
                    }
                    else
                    {
                        status = "Please enter a valid number!!";
                    }
                    */
                }
                if (String.Compare(status, "Success", true) == 0)
                {
                    errCodeMsg.errcode = 0;
                    errCodeMsg.errmsg = "Success";
                }
                else
                {
                    errCodeMsg.errcode = -1;
                    errCodeMsg.errmsg = status;
                }
                return errCodeMsg;
            }
            catch (Exception e)
            {
                string str_err = string.Format("AdditionalCLIController.AddCLI: {0}", e.Message);
                //Log.Info(str_err);
                return new Models.Common.ErrCodeMsg() { errcode = -1, errmsg = e.ToString() };
            }
        }

        public string GetWebClient(String destUri, String xmlFilePath, List<String> paramValues)
        {
            WebClient client = new WebClient();
            client.Headers.Add("content-type", "text/xml");
            System.IO.StreamReader reader = new System.IO.StreamReader(xmlFilePath);
            object[] args = paramValues.Cast<object>().ToArray();
            string data = String.Format(reader.ReadToEnd(), args);
            reader.Close();
            string response = client.UploadString(destUri, data);
            return response;
        }

        //Delete CLI
        public Models.Common.ErrCodeMsg DeleteCLI(CRM_API.Models.CountrySaver.AdditionalCLI cliViewModel, String AccountNumber, String soapFormatFilePath)
        {
            try
            {
                //Log.Info("Start CSB_HSB.DeleteCLI {0} {1}", cliViewModel.CLI, cliViewModel.Type);
                Models.Common.ErrCodeMsg errCodeMsg = new Models.Common.ErrCodeMsg();
                String status = "";
                using (WebClient client = new WebClient())
                {
                    //NameValueCollection fields = new NameValueCollection();
                    //fields.Add("ACCOUNT_NUMBER", accountId);
                    //fields.Add("CLI_NO", cliViewModel.CLI);
                    String newCLIDeleteUrl = null;
                    String newCLIDeletexmlns = null;
                    /*
                    if (cliViewModel.CLI.Substring(0, 4).CompareTo("0063") == 0 || cliViewModel.CLI.Substring(0, 2).CompareTo("63") == 0)
                    {
                        newCLIDeleteUrl = ConfigurationSettings.AppSettings["NewCLIFreedomDeleteUrl"].ToString();
                        newCLIDeletexmlns = ConfigurationSettings.AppSettings["NewCLIFreedomAddxmlns"].ToString();
                    }
                    else if (cliViewModel.CLI.Substring(0, 4).CompareTo("0094") == 0 || cliViewModel.CLI.Substring(0, 2).CompareTo("94") == 0)
                    {
                        newCLIDeleteUrl = ConfigurationSettings.AppSettings["NewCLIUnlimitedDeleteUrl"].ToString();
                        newCLIDeletexmlns = ConfigurationSettings.AppSettings["NewCLIUnlimitedAddxmlns"].ToString();
                    }
                     */
                    if (AccountNumber.Substring(0, 3).CompareTo("200") == 0)
                    {
                        newCLIDeleteUrl = System.Configuration.ConfigurationSettings.AppSettings["NewCLIFreedomDeleteUrl"].ToString();
                        newCLIDeletexmlns = System.Configuration.ConfigurationSettings.AppSettings["NewCLIFreedomDeletexmlns"].ToString();
                    }
                    else if (AccountNumber.Substring(0, 2).CompareTo("55") == 0)
                    {
                        newCLIDeleteUrl = System.Configuration.ConfigurationSettings.AppSettings["NewCLIUnlimitedDeleteUrl"].ToString();
                        newCLIDeletexmlns = System.Configuration.ConfigurationSettings.AppSettings["NewCLIUnlimitedDeletexmlns"].ToString();
                    }

                    if (!(String.IsNullOrEmpty(newCLIDeleteUrl)))
                    {
                        //byte[] respBytes = client.UploadValues(newCLIDeleteUrl, fields);
                        //string resp = client.Encoding.GetString(respBytes);
                        List<String> fieldvalues = new List<String>();
                        fieldvalues.Add(newCLIDeletexmlns);
                        fieldvalues.Add(AccountNumber);
                        fieldvalues.Add(cliViewModel.CLI);
                        String resp = GetWebClient(newCLIDeleteUrl, soapFormatFilePath, fieldvalues);
                        String XmlString = resp.Replace("&lt;", "<");
                        XmlString = XmlString.Replace("&gt;", ">");
                        System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
                        xmlDoc.LoadXml(XmlString);
                        System.Xml.XmlNodeList elemList = xmlDoc.GetElementsByTagName("DeleteCLIResult");
                        //System.Xml.XmlNode xmlNode = xmlDoc.SelectSingleNode("string");
                        System.Xml.XmlNode xmlNode = elemList.Item(0);
                        if (xmlNode != null)
                            status = xmlNode.InnerText;
                    }
                    else
                    {
                        status = "Please enter a valid number!!";
                    }


                }
                if (String.Compare(status, "Success", true) == 0)
                {
                    errCodeMsg.errcode = 0;
                    errCodeMsg.errmsg = "Success";
                }
                else
                {
                    errCodeMsg.errcode = -1;
                    errCodeMsg.errmsg = status;
                }
                return errCodeMsg;
            }
            catch (Exception e)
            {
                string str_err = string.Format("AdditionalCLIController.DeleteCLI: {0}", e.Message);
                //Log.Info(str_err);
                return new Models.Common.ErrCodeMsg() { errcode = -1, errmsg = e.ToString() };
            }

        }

        private async Task<Models.Common.ErrCodeMsg> AddNewCLI(CRM_API.Models.CountrySaver.AdditionalCLI Model)
        {
            try
            {
                return await Task.Run(() =>
                {
                    Boolean validate = CRM_API.DB.CLI.SProc.ValidateCLI(Model.CLI);
                    if (validate == false)
                    {
                        var resy = new Models.Common.ErrCodeMsg() { errcode = -1, errmsg = "Please enter a valid number!!" };
                        return resy;
                    }
                    else
                    {
                        //check prefix for new CLI with CLI
                        validate = CRM_API.DB.CLI.SProc.ValidateCLIPrefix(Model.FandFNo, Model.Mobileno, Model.Brand, Model.CLI);
                        if (validate == false)
                        {
                            var resy = new Models.Common.ErrCodeMsg() { errcode = -1, errmsg = "Please enter a valid number!!" };
                            return resy;
                        }
                        else
                        {                                      
                            var result = AddCLI(Model, Model.AccountId, System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/AddCLI.xml"));
                            var resy = new Models.Common.ErrCodeMsg() { errcode = result.errcode, errmsg = result.errmsg };
                            return resy;
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private String GetAccountId(int subscriberId)
        {
            CRM_API.DB.LLOTG.Payment payment = new CRM_API.DB.LLOTG.Payment();
            List<Models.NewSubscription> newSubscriptionList = payment.GetOperatorDetails(subscriberId.ToString());
            Models.NewSubscription newSubscription = newSubscriptionList.FirstOrDefault();
            return newSubscription.REG_SWITCH_NO;
        }

        //public Models.Common.ErrCodeMsg DeleteNewCLI(int SubscriberId, string FFNumber, string CLI)
        //public Models.Common.ErrCodeMsg DeleteNewCLI(CRM_API.Models.CountrySaver.AdditionalCLI Model)
        private async Task<Models.Common.ErrCodeMsg> DeleteNewCLI(CRM_API.Models.CountrySaver.AdditionalCLI Model)
        {
            try
            {
                return await Task.Run(() =>
                {
                    //CRM_API.Models.CountrySaver.AdditionalCLI _cliViewModel = new CRM_API.Models.CountrySaver.AdditionalCLI();
                    //_cliViewModel.CLI = CLI;
                    //String AccountId = GetAccountId(Model.SubscriberId);
                    var result = DeleteCLI(Model, Model.AccountId, System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/DeleteCLI.xml"));

                    var resy = new Models.Common.ErrCodeMsg() { errcode = result.errcode, errmsg = result.errmsg };
                    //var resy = new Mvc.Code.Common.ErrCodeMsgExt { err_code = -1, err_message = "Fail" };
                    return resy;
                });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public Models.Common.ErrCodeMsg ValidateVectoneCLI(CRM_API.Models.CountrySaver.AdditionalCLI Model)
        private async Task<Models.Common.ErrCodeMsg> ValidateVectoneCLI(CRM_API.Models.CountrySaver.AdditionalCLI Model)
        {
            try
            {
                return await Task.Run(() =>
                {
                    Boolean validate = CRM_API.DB.CLI.SProc.ValidateCLI(Model.CLI);
                    if (validate == false)
                    {
                        var resy = new Models.Common.ErrCodeMsg { errcode = -1, errmsg = "Please enter a valid number!!" };
                        return resy;
                    }
                    else
                    {
                        //check prefix for new CLI with CLI
                        validate = CRM_API.DB.CLI.SProc.ValidateCLIPrefix(Model.FandFNo, Model.Mobileno, Model.Brand, Model.CLI);
                        if (validate == false)
                        {
                            var resy = new Models.Common.ErrCodeMsg { errcode = -1, errmsg = "Please enter a valid number!!" };
                            return resy;
                        }
                        else
                        {
                            var resy = new Models.Common.ErrCodeMsg { errcode = 0, errmsg = "Number is valid" };
                            return resy;
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
