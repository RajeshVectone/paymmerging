﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NLog;

namespace CRM_API.Controllers
{
    public class AuthController : ApiController
    {
        //
        // GET: /Auth/


        //POST api/auth
        public HttpResponseMessage POST(Models.LoginMsg Token)
        {
            CRM_API.Models.SessionInfo resValues = CRM_API.DB.Auth.SProc.GetSessionInfo(Token.token);

            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.SessionInfo>());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues);    
        }

    }
}
