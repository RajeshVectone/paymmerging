﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CRM_API.Controllers
{
    public class ProcessPaymentCC_Controller : ApiController
    {
        // GET api/processpaymentcc_
        public HttpResponseMessage Get(string CustomerIds,string sitecode)
        {
            //  CRM_API.Models.ManualMarking values = new CRM_API.Models.ManualMarking();
            var result = CRM_API.DB.PMBODDOperations.ProcessPaymentCC(CustomerIds,sitecode);
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
        // GET api/processpaymentcc_/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/processpaymentcc_
        public void Post([FromBody]string value)
        {
        }

        // PUT api/processpaymentcc_/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/processpaymentcc_/5
        public void Delete(int id)
        {
        }
    }
}
