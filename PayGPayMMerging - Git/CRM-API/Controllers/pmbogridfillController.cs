﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Script.Serialization;
using NLog;


namespace CRM_API.Controllers
{
    public class pmbogridfillController : ApiController
    {
        //
        // GET: /pmbogridfill/
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public HttpResponseMessage Get(string value, string CollectionType,string SiteCode)
        {
          //  CRM_API.Models.ManualMarking values = new CRM_API.Models.ManualMarking();
            if (CollectionType == "CF")
            {
                var result = CRM_API.DB.PMBODDOperations.getmanualmarkinggrid(value,SiteCode);
                return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
            }
            else
            {
                var result = CRM_API.DB.PMBODDOperations.getmanualmarkingDDgrid(value,SiteCode);
                return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
            }
        }
    }
}

