﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml;

namespace CRM_API.Controllers
{
    public class FinViewstransactionController : ApiController
    {
        //
        // GET: /FinViewstransaction/

        //public HttpResponseMessage Get(string Transation_Status, string Rejection_Status, string Product_Code, string Customer_Name, string payment_Ref, string mobile_Number, string EmailAddress, string ICCID, string startdate, string enddate)
        //{
        public HttpResponseMessage Get(string paymentMode, string date)
        {
            try
            {
                ////var result = CRM_API.DB.viewtransaction.getpaymentdetails();
                //var result = CRM_API.DB.viewtransaction.getpaymentdetailsdownloads_New(Transation_Status, Rejection_Status, Product_Code, Customer_Name, payment_Ref, mobile_Number, EmailAddress, ICCID, startdate, enddate);
                //return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
                //var result = CRM_API.DB.PMBOPayway.getPayWayFiles();

                if (paymentMode == "0")
                {
                    //var result = CRM_API.DB.viewtransaction.getpaymentdetails();
                    var result = CRM_API.DB.viewtransaction.getpaymentdetailsdownloads(date);
                    return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
                }
                else if (paymentMode == "100")
                {
                    var result1 = CRM_API.DB.viewtransaction.getserrrCodedownload("get_latest_details_from_tpayment_by_errcode_sv6", paymentMode, date);
                    return Request.CreateResponse(HttpStatusCode.OK, result1.ToArray());
                }
                else if (paymentMode == "101")
                {
                    var result2 = CRM_API.DB.viewtransaction.getserrrCodedownload("get_latest_details_from_tpayment_by_errcode_sv7", paymentMode, date);
                    return Request.CreateResponse(HttpStatusCode.OK, result2.ToArray());
                }
                else if (paymentMode == "200")
                {
                    var result3 = CRM_API.DB.viewtransaction.getserrrCodedownload("get_latest_details_from_tpayment_by_errcode_sv8", paymentMode, date);
                    return Request.CreateResponse(HttpStatusCode.OK, result3.ToArray());
                }
                else if (paymentMode == "201")
                {
                    var result4 = CRM_API.DB.viewtransaction.getserrrCodedownload("get_latest_details_from_tpayment_by_errcode_sv9", paymentMode, date);
                    return Request.CreateResponse(HttpStatusCode.OK, result4.ToArray());
                }
                //05-Feb-2019 : Moorthy : Added for auto Topup Report
                else if (paymentMode == "202")
                {
                    var result5 = CRM_API.DB.viewtransaction.GetAutoTopupReport(date);
                    return Request.CreateResponse(HttpStatusCode.OK, result5.ToArray());
                }
                //12-Feb-2019 : Moorthy : Added for General Report
                else if (paymentMode == "203")
                {
                    var result6 = CRM_API.DB.viewtransaction.GetGeneralReport(date);
                    return Request.CreateResponse(HttpStatusCode.OK, result6.ToArray());
                }
                return Request.CreateResponse(HttpStatusCode.OK, new List<CRM_API.Models.financeviewtransdownload>());

            }
            catch (WebException ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new List<CRM_API.Models.financeviewtrans>());
            }
        }

    }
}
