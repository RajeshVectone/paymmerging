﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NLog;

namespace CRM_API.Controllers
{
    public class CustomerOportalController : ApiController
    {
        //
        // GET: api/Customer/

        /*public HttpResponseMessage Get(string key, string value)
        {
            IEnumerable<CRM_API.Models.CustomerSearchResult> resValues =
                CRM_API.DB.Customer.SProc.Search(key, value);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.CustomerSearchResult>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }*/

        // POST: api/CustomerOportal

        public HttpResponseMessage Post(CRM_API.Models.CustomerOportalList Search)
        {
            if (Search != null)
            {
                switch (Search.order_type)
                {
                  
                    case CRM_API.Models.CustomerOportalOrderType.CustomerList:
                        return CustomerList(Search.saver_operator, Search.date_from, Search.date_to);

                    default:
                        return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.CustomerOportalList>().ToArray());
                }

            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.CustomerOportalList>().ToArray());
            }
        }

        public HttpResponseMessage CustomerList(string saver_operator, DateTime date_from, DateTime date_to)
        {
            IEnumerable<CRM_API.Models.CustomerOportalList> resHistoryValues =
                           CRM_API.DB.CustomerOportal.SProc.Search(saver_operator, date_from, date_to);

            if (resHistoryValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.CustomerOportalList>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resHistoryValues.ToArray());
        }
    }
}
