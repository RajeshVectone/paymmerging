﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Xml;
using CRM_API.DB;
using CRM_API.DB.BreakageUsage;
using CRM_API.Models;
using CRM_API.Models.BreakageUsageModels;
using Mandrill;
using Mandrill.Model;
using Newtonsoft.Json;
using NLog;
using CRM_API.Models.SIMActivation;

namespace CRM_API.Controllers
{
    public class SIMActivationHistoryController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public async Task<HttpResponseMessage> Post(SIMActivationModels Model)
        {
            try
            {
                List<SIMActivationModels> resultValues = SIMActivationHistory.GetSIMActivationHistoryList(Model.date_from, Model.date_to).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, resultValues);
            }
            catch (WebException ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new List<SMSIssueHistoryListOutput>());
            }
        }

    }
}
