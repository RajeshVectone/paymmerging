﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Http;
using CRM_API.Models;
using Mandrill;
using Mandrill.Model;

namespace CRM_API.Controllers
{
    public class PriorityController : ApiController
    {
        private HttpResponseMessage Post([FromBody]PriorityInput Model)
        {
            IEnumerable<CRM_API.Models.PriorityOutput> resValues = CRM_API.DB.EmailGenerateTicket.Priority(Model.sitecode);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.PriorityOutput>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }
    }

}
