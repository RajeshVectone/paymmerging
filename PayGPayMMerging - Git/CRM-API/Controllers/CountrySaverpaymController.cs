﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using CRM_API.Models.CountrySaverPAYM;
using CRM_API.DB.CountrySaverPAYM;

namespace CRM_API.Controllers
{
    /*
     * Country Saver
    By Mamin @28-01-2014 @myamin_amzah@yahoo.com
    Requirement : get List of Country Saver
     * Param : sitecode, msisdn (mandatory)
     * 
    */
    public class CountrySaverPAYMController : ApiController
    {
        public async Task<HttpResponseMessage> Get([FromUri]AdditionalReqInfo Model)
        {
            var result = new Models.Common.ErrCodeMsg() { errcode = -1, errmsg = "Default Value" };
            try
            {
                return await GetSelection(Model);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new Models.Common.ErrCodeMsg()
                {
                    errcode = -1,
                    errmsg = ex.Message
                });
            }
        }

        private async Task<HttpResponseMessage> GetSelection(AdditionalReqInfo Model)
        {
            switch (Model.InfoType)
            {
                case AdditionInfo.GETSTATUS:
                    return Request.CreateResponse(HttpStatusCode.OK, await GetStatus(Model));
                default: throw new EntryPointNotFoundException("Parameter InfoType Not Recognized");
            }
        }

        private async Task<IEnumerable<StatusList>> GetStatus(AdditionalReqInfo Model)
        {
            return await Task.Run(() =>
            {
                var result = SProc.GetListStatus(Model);
                return result;
            });
        }

        private async Task<IEnumerable<CountrySaverList>> GetList(AdditionalReqInfo Model)
        {
            Model = InitiateObj(Model);
            return await Task.Run(() =>
            {
                var result = SProc.GetListCSaver(Model);
                return result;
            });
        }

        /*Added by Hari - 19 Jan 2015 - for getting subscription history*/
        private async Task<IEnumerable<CountrySaverList_New>> GetBundleCountrySaverDetail(AdditionalReqInfo Model)
        {
            //IList<Models.LOTG.Collection> result = new List<Models.LOTG.Collection>();
            return await Task.Run(() =>
            {
                var result = SProc.GetBundleCountrySaverDetail(Model.Mobileno, Model.Sitecode);
                return result;
            });
        }
        /*End added*/

        public async Task<HttpResponseMessage> Post(AdditionalReqInfo Model)
        {
            try
            {
                switch (Model.InfoType)
                {
                    case AdditionInfo.GETList: return Request.CreateResponse(HttpStatusCode.OK, await GetList(Model));
                    case AdditionInfo.RENEW: return Request.CreateResponse(HttpStatusCode.OK, await Renew(Model));
                    case AdditionInfo.REACTIVATE: return Request.CreateResponse(HttpStatusCode.OK, await Reactivate(Model));
                    //case AdditionInfo.CANCEL: return Request.CreateResponse(HttpStatusCode.OK, await CancelCS(Model));
                    case AdditionInfo.CANCEL: return Request.CreateResponse(HttpStatusCode.OK, await CancelCS_New(Model));
                    case AdditionInfo.GETListLLOM: return Request.CreateResponse(HttpStatusCode.OK, await GetBundleCountrySaverDetail(Model));
                    case AdditionInfo.GetListCLI: return Request.CreateResponse(HttpStatusCode.OK, await GetListCLI(Model));
                    case AdditionInfo.ViewSubscriptionhistory: return Request.CreateResponse(HttpStatusCode.OK, await GetSubscriptionlist(Model));
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed,
                    new Models.Common.ErrCodeMsg()
                    {
                        errcode = -1,
                        errmsg = e.Message,
                        errsubject = "General Error"
                    });
            }
            return await Task.Run(() =>
            {
                return Request.CreateResponse(HttpStatusCode.OK, Model);
            });
        }

        private async Task<IEnumerable<CountrySaverPaymentHistory>> GetSubscriptionlist(AdditionalReqInfo Model)
        {
            try
            {
                return await Task.Run(() =>
                {
                    IEnumerable<CountrySaverPaymentHistory> countrySaverList = SProc.GetViewSubscriptionlist(Model);
                    return countrySaverList;
                });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private async Task<Models.Common.ErrCodeMsg> Renew(AdditionalReqInfo Model)
        {
            bool validate = true;
            if (String.IsNullOrEmpty(Model.RegIDX) || Model.RegIDX == "") validate = false;
            try
            {
                if (validate)
                {
                    var res = await Task.Run(() => SProc.SubmitRenew(Model));
                    return new Models.Common.ErrCodeMsg()
                    {
                        errcode = res.errcode,
                        errmsg = res.errmsg
                    };
                }
                else
                {
                    return new Models.Common.ErrCodeMsg()
                    {
                        errcode = -1,
                        errmsg = "Validation Failed, RegIDX is null"
                    };
                }
            }
            catch (Exception ex)
            {
                return new Models.Common.ErrCodeMsg()
                {
                    errcode = -1,
                    errmsg = ex.Message
                };
            }
        }

        private async Task<Models.Common.ErrCodeMsg> Reactivate(AdditionalReqInfo Model)
        {
            bool validate = true;
            if (String.IsNullOrEmpty(Model.RegIDX) || Model.RegIDX == "") validate = false;
            try
            {
                if (validate)
                {
                    var res = await Task.Run(() => SProc.SubmitReactivate(Model));
                    return new Models.Common.ErrCodeMsg()
                    {
                        errcode = res.errcode,
                        errmsg = res.errmsg
                    };
                }
                else
                {
                    return new Models.Common.ErrCodeMsg()
                    {
                        errcode = -1,
                        errmsg = "Validation Failed, RegIDX is null"
                    };
                }
            }
            catch (Exception ex)
            {
                return new Models.Common.ErrCodeMsg()
                {
                    errcode = -1,
                    errmsg = ex.Message
                };
            }
        }

        private async Task<IEnumerable<Models.Common.ErrCodeMsg>> CancelCS_New(AdditionalReqInfo Model)
        {
            //Model = InitiateObj(Model);
            return await Task.Run(() =>
            {
                var result = SProc.ChangeCancelmode(Model);
                return result;
            });
        }

        private async Task<Models.Common.ErrCodeMsg> CancelCS(AdditionalReqInfo Model)
        {
            bool validate = true;
            if (String.IsNullOrEmpty(Model.RegIDX) || Model.RegIDX == "") validate = false;
            try
            {
                if (validate)
                {
                    var res = await Task.Run(() => SProc.SubmitCancel(Model));
                    return new Models.Common.ErrCodeMsg()
                    {
                        errcode = res.errcode,
                        errmsg = res.errmsg
                    };
                }
                else
                {
                    return new Models.Common.ErrCodeMsg()
                    {
                        errcode = -1,
                        errmsg = "Validation Failed, RegIDX is null"
                    };
                }
            }
            catch (Exception ex)
            {
                return new Models.Common.ErrCodeMsg()
                {
                    errcode = -1,
                    errmsg = ex.Message
                };
            }
        }

        private AdditionalReqInfo InitiateObj(AdditionalReqInfo Model)
        {
            if (String.IsNullOrEmpty(Model.Mobileno))
                Model.Mobileno = "1111";      //hardcoded avoid exception in sp            
            return Model;
        }

        //private async Task<IEnumerable<CRM_API.Models.CountrySaver.CountrySaverList>> GetListCLI(AdditionalReqInfo Model)
        //{
        //    try
        //    {
        //        return await Task.Run(() =>
        //        {
        //            IEnumerable<CRM_API.Models.CountrySaver.CountrySaverList> countrySaverList = CRM_API.DB.CountrySaver.SProc.GetListCSaver(Model);
        //            foreach (CRM_API.Models.CountrySaver.CountrySaverList countrySaver in countrySaverList)
        //            {
        //                List<CRM_API.Models.CountrySaver.AdditionalCLI> lstAdditionalCLI = CRM_API.DB.CountrySaver.SProc.GetListCLI(countrySaver).ToList();
        //                countrySaver.AdditionalCLIs = lstAdditionalCLI;
        //            }
        //            return countrySaverList;
        //        });
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        private async Task<IEnumerable<AdditionalCLI>> GetListCLI(AdditionalReqInfo Model)
        {
            try
            {
                return await Task.Run(() =>
                {
                    List<AdditionalCLI> lstAdditionalCLI = SProc.GetAdditionalCLI(Model).ToList();
                    return lstAdditionalCLI;
                });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

