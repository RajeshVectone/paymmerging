﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
namespace CRM_API.Controllers
{
    /// <summary>
    /// Austria Porting Api
    /// api/atporting
    /// </summary>
    public class ATPortingController : ApiController
    {
        public async Task<HttpResponseMessage> Get([FromUri]CRM_API.Models.Porting.AT.AdditionalReqInfo Model)
        {
            var result = new Models.Common.ErrCodeMsg() { errcode = -1, errmsg = "Default Value" };
            try
            {
                return await GetSelection(Model);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new Models.Common.ErrCodeMsg()
                {
                    errcode = -1,
                    errmsg = e.Message
                });
            }
        }
        private async Task<HttpResponseMessage> GetSelection(CRM_API.Models.Porting.AT.AdditionalReqInfo Model)
        {
            switch (Model.InfoType)
            {
                //TODO: Add Other Retrieve information
                case Models.Porting.AT.AdditionInfo.ListCountry:
                    return Request.CreateResponse(HttpStatusCode.OK, await GetCountryList(Model.Sitecode));
                case Models.Porting.AT.AdditionInfo.ListSubscriberType:
                    return Request.CreateResponse(HttpStatusCode.OK, await GetSubscriberType(Model.Sitecode));
                case Models.Porting.AT.AdditionInfo.ListDonor:
                    return Request.CreateResponse(HttpStatusCode.OK, await GetDonorOperator(Model.Sitecode));
                case Models.Porting.AT.AdditionInfo.ListCancel:
                    return Request.CreateResponse(HttpStatusCode.OK, await GetCancelList(Model.PortingCode, Model.Msisdn,Model.Sitecode));
                case Models.Porting.AT.AdditionInfo.ListReceipientOp: 
                    return Request.CreateResponse(HttpStatusCode.OK);
                default: throw new EntryPointNotFoundException("Parameter InfoType Not Recognized");
            }
        }

        public async Task<HttpResponseMessage> Post(Models.Porting.AT.PortinEntry Model)
        {
            try
            {
                switch (Model.ActionType)
                {
                    case Models.Porting.AT.PortingAction.RequestPorting: break;
                    case Models.Porting.AT.PortingAction.RequestCancel: 
                        return Request.CreateResponse(HttpStatusCode.OK,DoCancelRequestPorting(Model));
                    case Models.Porting.AT.PortingAction.RequestNeuvInfoList: break;
                    case Models.Porting.AT.PortingAction.SendNeuvInfo: break;
                    case Models.Porting.AT.PortingAction.TransactionReport: break;
                    case Models.Porting.AT.PortingAction.Testing:
                        return TestMode(Model);
                    default:
                        throw new EntryPointNotFoundException("Parameter ActionType Not Defined");
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new Models.Common.ErrCodeMsg()
                {
                    errcode = -1,
                    errmsg = e.Message
                });
            }
            return await PostResult(Model);
        }
        private async Task<HttpResponseMessage> PostResult(Models.Porting.AT.PortinEntry Model)
        {
            return await Task.Run<HttpResponseMessage>(() =>
            {
                return Request.CreateResponse(HttpStatusCode.OK, Model);
            });
        }
        private async Task<IEnumerable<DB.Ref.Country.Country>> GetCountryList(string Sitecode)
        {
            return await DB.Ref.Country.Country.ListAllCountry_Async(Sitecode);
        }
        private async Task<IEnumerable<DB.Ref.Country.Country>> GetCountryByID(string Id, string Sitecode)
        {
            return await DB.Ref.Country.Country.CountryById_Async(Id, Sitecode);
        }
        private async Task<IEnumerable<CRM_API.Models.Subscriber.TypeofSubscriber>> GetSubscriberType(string Sitecode)
        {
            return await Task.Run<IEnumerable<CRM_API.Models.Subscriber.TypeofSubscriber>>(() =>
            {
                return DB.Subscriber.SProc.ListSubscriberType(Sitecode);
            });
        }
        private async Task<IEnumerable<Models.Porting.AT.SoapReceiver.Transactions.Ping>> GetDonorOperator(string Sitecode)
        {
            return await Task.Run<IEnumerable<Models.Porting.AT.SoapReceiver.Transactions.Ping>>(() =>
            {
                return DB.Ref.Donor.AT.SProc.GetDonorOperator(Sitecode);
            });
        }
        private async Task<HttpResponseMessage> DoRequestPorting(Models.Porting.AT.PortinEntry M)
        {
            try
            {
                bool isValid = false;
                var result = new Models.Common.ErrCodeMsg() { errcode = 0, errmsg = "Request Porting Send & Save Succesfull." };
                if (!DB.Porting.AT.SProc.DateIsWorkingDay(M.DesiredPortedDate))
                    throw new Exception("Desired porting date is invalid, should be on a working day");
                //TODO: compare DesiredPortedDate with Min and Max working date
                var dateRequestMin = DB.Porting.AT.SProc.GetDatePlusXWorkingDay(M.DesiredPortedDate, 3, M.Sitecode);
                var dateRequestMax = DB.Porting.AT.SProc.GetDatePlusXWorkingDay(M.DesiredPortedDate, 61, M.Sitecode);
                if (M.DesiredPortedDate < dateRequestMin || M.DesiredPortedDate >= dateRequestMax)
                    throw new Exception("Desired porting date is invalid, should be greater than 3 days and less than 60 days");

                //TODO: call function to check if msisdn is valid
                if (DB.Porting.AT.SProc.CheckMsisdnAtMvnoAccount(M.BBMsisdn.Trim()))
                {
                    var oPort = CRM_API.Porting.AT.Porting.SetObjPorting(M);
                    var oTrans = CRM_API.Porting.AT.Porting.SetObjTrans(M);
                    var oNuv = CRM_API.Porting.AT.Porting.SetSubscriberObject(M);
                    var arlMobile = CRM_API.Porting.AT.Porting.SetMobileObject(M);
                    var resx = DB.Porting.AT.SProc.CheckPortingCode(M.PortingCode, M.Sitecode, ref isValid);
                    if (!isValid)
                        return await Task.Run<HttpResponseMessage>(() => { return Request.CreateResponse(HttpStatusCode.OK, resx); });
                    else
                    {
                        //TODO : Implement SaveAndSendRequestPorting();
                        result = SaveAndSendRequestPorting(oTrans, oNuv, oPort, arlMobile, M);

                    }
                }
                else
                    throw new Exception("Barablu MSISDN is not valid");

                return await Task.Run<HttpResponseMessage>(() => { return Request.CreateResponse(HttpStatusCode.OK, result); });
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new Models.Common.ErrCodeMsg()
                {
                    errcode = -1,
                    errmsg = e.Message
                });
            }
        }
        private Models.Common.ErrCodeMsg SaveAndSendRequestPorting(CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Transactions oTrans,
            CRM_API.Models.Porting.AT.SoapReceiver.Transactions.NuvInfo oNuv,
            CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Porting oPort,
            List<CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Mobile> arlMobile,
            Models.Porting.AT.PortinEntry M)
        {
            var result = new Models.Common.ErrCodeMsg() { errcode = -1, errmsg = "Default Result" };
            try
            {
                var resx = DB.Porting.AT.SProc.InsertPortingIn(oTrans, oNuv, oPort, M.Sitecode);
                if (resx.errcode == 0)
                {
                    //TODO : Implement SendPortingRequest [UnDone]
                    var ReqPorting = CRM_API.Porting.AT.Porting.SendPortingRequest(oPort, oTrans, arlMobile, M.Sitecode);
                    //var resy = Helpers.Porting.AT.SoapReciever.RequestPorting(ReqPorting, M.Sitecode);
                }
            }
            catch (Exception e)
            {
                throw new Exception("api/atporting SaveAndSendRequestPorting: " + e.Message);
            }
            return result;
        }

        private HttpResponseMessage TestMode(Models.Porting.AT.PortinEntry M)
        {
            //var result = Helpers.Porting.AT.SoapReciever.GetRequestID(M.Sitecode);
            var result = Helpers.Porting.AT.SoapReciever.RequestPorting(new CRM_API.Models.Porting.AT.SoapReceiver.Transactions.ReqPortingType(),"BAU");
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        #region Cancel request Method
        private async Task<IEnumerable<Models.Porting.AT.PortingList>> GetCancelList(string PortingCode,string Msisdn,string SiteCode)
        {
            return await Task.Run<IEnumerable<Models.Porting.AT.PortingList>>(() =>
            {
                return DB.Porting.AT.SProc.GetCancellationList(PortingCode, Msisdn,SiteCode);
            });
        }
        private Models.Common.ErrCodeMsg DoCancelRequestPorting(Models.Porting.AT.PortinEntry Model)
        {
            Models.Common.ErrCodeMsg result = new Models.Common.ErrCodeMsg();
            Austria.ReqCancelType ReqCancelService = new Austria.ReqCancelType();
            CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Transactions oTrans = CRM_API.Porting.AT.Porting.SetObjTransForCancel(Model);
            CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Cancel oCancel = CRM_API.Porting.AT.Porting.SetObjCancel(Model);
            try
            {
                result = CRM_API.DB.Porting.AT.SProc.RequestCancel(oTrans, oCancel);
                if (result.errcode == 0)
                {
                    
                    CRM_API.Models.Porting.AT.SoapReceiver.Transactions.ReqCancelType ReqCancel = CRM_API.DB.Porting.AT.SProc.SendReqCancel(oTrans, oCancel);
                    
                    //Sending cancel request to the webservice
                    ReqCancelService.CancellationCode = ReqCancel.CancellationCode;
                    ReqCancelService.Header.RequestID = ReqCancel.Header.RequestID;
                    ReqCancelService.Header.Receiver = ReqCancel.Header.Receiver;
                    ReqCancelService.Header.Sender = ReqCancel.Header.Sender;
                    ReqCancelService.Header.Version = ReqCancel.Header.Version;
                    ReqCancelService.Header.EnvFlag = ReqCancel.Header.EnvFlag;
                    ReqCancelService.Header.Timestamp = ReqCancel.Header.Timestamp;
                    new Austria.ServiceWebsvcSoapClient().requestCancellation(ReqCancelService);

                    result = CRM_API.DB.Porting.AT.SProc.UpdateTransactionsCancel(oTrans.RequestId, oCancel.PortingCode, oTrans.PortingStatus, 0);
                    if (result.errcode != 0)
                    {
                        throw new Exception("Error Saving Cancel Request Update Transactions");
                    }
                    result.errmsg = "OK";
                }
                else
                {
                    throw new Exception("Error Saving Cancel Request");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("api/atporting DoCancelRequestPorting : " + ex.Message);
            }
            return result;
        }
        #endregion

        #region Send Request Nuev Info(Out)
        private async Task<IEnumerable<Models.Porting.AT.PortingList>> GetReceipientOperatorList(string sitecode)
        {
            return await Task.Run<IEnumerable<Models.Porting.AT.PortingList>>(() =>
            {
                return DB.Porting.AT.SProc.GetReceipientListNuevo(sitecode);
            });
        }
        private Models.Common.ErrCodeMsg SendRequestNuevo(Models.Porting.AT.PortinEntry Model)
        {
            Models.Common.ErrCodeMsg result = new Models.Common.ErrCodeMsg();

            Austria.ServiceWebsvcSoapClient oSrv = new Austria.ServiceWebsvcSoapClient();
            var arlMbl = new List<CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Mobile>();
            var oNuv = CRM_API.Porting.AT.Porting.SetObjNuv(Model);

            arlMbl = CRM_API.Porting.AT.Porting.SetMobileObjForNuevo(Model);
            var oTrans = CRM_API.Porting.AT.Porting.SetObjTransForNuevo(Model,oNuv.RequestId);
            try
            {
                result = DB.Porting.AT.SProc.insertTransaction(oTrans, "RQN", 2, null);
                if (result.errcode != 0)
                {
                    result.errmsg = "Can't Insert Transaction";
                    return result;
                }

                result = DB.Porting.AT.SProc.InsertNuvInfo(oNuv,arlMbl);
                if (result.errcode != 0)
                {
                    result.errmsg = "Can't Insert Nuv Info";
                    return result;
                }
                Austria.ReqNuevInfoType oReqNuev = SendReqNuv(oNuv, oTrans, arlMbl, Model.Sitecode);
                oSrv.requestNuevInfo(oReqNuev);
            }
            catch (Exception ex)
            {
                throw new Exception("api/atporting SendRequestNuevo : " + ex.Message);
            }
            return result;
        }
        public Austria.ReqNuevInfoType SendReqNuv(CRM_API.Models.Porting.AT.SoapReceiver.Transactions.NuvInfo oNuv,
            CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Transactions oTrans,List<CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Mobile> arlMbl,string sitecode)
        {

            Austria.ReqNuevInfoType oReqNuev = new Austria.ReqNuevInfoType();
            Austria.CustomerType oCust = new Austria.CustomerType();
            Austria.MsisdnSpecType[] oMsisdn = new Austria.MsisdnSpecType[arlMbl.Count];
            Austria.ServiceInfoType oServInfo = new Austria.ServiceInfoType();
            Austria.StatusType oStatus = new Austria.StatusType();

            //Header
            oReqNuev.Header.RequestID = CRM_API.Helpers.GeneralConverter.ToLong(oTrans.RequestId);
            oReqNuev.Header.Receiver = oTrans.Receiver;
            oReqNuev.Header.Sender = Helpers.Config.GetPrefixFile(sitecode);
            oReqNuev.Header.Version = Helpers.Config.GetVersion(sitecode);
            oReqNuev.Header.EnvFlag = Helpers.Config.GetEnvFlag(sitecode);
            oReqNuev.Header.Timestamp = Helpers.GeneralConverter.ToDateTime(DateTime.Now);

            oCust.Prepaid.PUKSpecified = true;
            oCust.Prepaid.PUK = CRM_API.Helpers.GeneralConverter.ToInt32(oNuv.Puk);
            oReqNuev.Customer = oCust;
            oReqNuev.MsisdnSpec = new Austria.MsisdnSpecType[arlMbl.Count];
            for (int i = 0; i < arlMbl.Count; i++)
            {
                oReqNuev.MsisdnSpec[i] = new Austria.MsisdnSpecType();
                if (arlMbl[i].Msisdn == string.Empty || arlMbl[i].Msisdn == null)
                {
                    //oReqNuev.MsisdnSpec[i].MsisdnRange = new MsisdnRangeType();
                    oReqNuev.MsisdnSpec[i].MsisdnRange.MsisdnStart = arlMbl[i].MsisdnStart;
                    oReqNuev.MsisdnSpec[i].MsisdnRange.MsisdnEnd = arlMbl[i].MsisdnEnd;
                    oReqNuev.MsisdnSpec[i].MsisdnRange.RoutingCount = arlMbl[i].RoutingCount;
                }
                else
                    oReqNuev.MsisdnSpec[i].Msisdn = arlMbl[i].Msisdn;
            }
            return oReqNuev;
        }
        #endregion

        #region Request nuev Info(out) List
        //TODO : Request Nuev Info List
        #endregion

        #region transaction Report
        //TODO : Transaction Report
        #endregion
    }
}
namespace CRM_API.Porting.AT
{
    public class Porting
    {
        public static CRM_API.Models.Porting.AT.SoapReceiver.Transactions.ReqPortingType SendPortingRequest(
            CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Porting objPorting,
            CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Transactions objTrans, 
            List<CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Mobile> arlMobile, string sitecode){
            //ServicePortingAT oSrv = new ServicePortingAT();
            CRM_API.Models.Porting.AT.SoapReceiver.Transactions.ReqPortingType oReqPort = new CRM_API.Models.Porting.AT.SoapReceiver.Transactions.ReqPortingType();
            CRM_API.Models.Porting.AT.SoapReceiver.Transactions.PortingRequestType[] oResp = new CRM_API.Models.Porting.AT.SoapReceiver.Transactions.PortingRequestType[arlMobile.Count];
            oReqPort.PortingRequest = new CRM_API.Models.Porting.AT.SoapReceiver.Transactions.PortingRequestType[arlMobile.Count];
            oReqPort.Header = CRM_API.Porting.AT.Porting.SetObjectHeader(objTrans, sitecode);
            oReqPort.PortingCode = objPorting.PortingCode;
            oReqPort.DesiredPortingDate = Helpers.GeneralConverter.ToDateTime(objPorting.DesiredPortDate);
            for (int i = 0; i < arlMobile.Count; i++)
            {
                oResp[i] = new CRM_API.Models.Porting.AT.SoapReceiver.Transactions.PortingRequestType();
                oResp[i].MsisdnSpec = new CRM_API.Models.Porting.AT.SoapReceiver.Transactions.MsisdnSpecType();
                if (arlMobile[i].Msisdn == string.Empty || arlMobile[i].Msisdn == null)
                {
                    oResp[i].MsisdnSpec.MsisdnRange = new CRM_API.Models.Porting.AT.SoapReceiver.Transactions.MsisdnRangeType();
                    oResp[i].MsisdnSpec.MsisdnRange.MsisdnStart = arlMobile[i].MsisdnStart;
                    oResp[i].MsisdnSpec.MsisdnRange.MsisdnEnd = arlMobile[i].MsisdnEnd;
                    oResp[i].MsisdnSpec.MsisdnRange.RoutingCount = arlMobile[i].RoutingCount;
                }
                else
                    oResp[i].MsisdnSpec.Msisdn = arlMobile[i].Msisdn;
                oReqPort.PortingRequest[i] = oResp[i];
            }
            return oReqPort;
        }

        public static CRM_API.Models.Porting.AT.SoapReceiver.Transactions.HeaderType SetObjectHeader(CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Transactions oTrans, string sitecode)
        {
            CRM_API.Models.Porting.AT.SoapReceiver.Transactions.HeaderType oHeader = new CRM_API.Models.Porting.AT.SoapReceiver.Transactions.HeaderType();
            oHeader.RequestID = CRM_API.Helpers.GeneralConverter.ToLong(oTrans.RequestId);
            oHeader.Receiver = oTrans.Receiver;
            oHeader.Sender = Helpers.Config.GetPrefixFile(sitecode);
            oHeader.MNP_exp = oTrans.MnpExp;
            oHeader.MNP_imp = oTrans.MnpImp;
            oHeader.Version = Helpers.Config.GetVersion(sitecode);
            oHeader.EnvFlag = Helpers.Config.GetEnvFlag(sitecode);
            oHeader.Timestamp = Helpers.GeneralConverter.ToDateTime(DateTime.Now);
            return oHeader;
        }
        public static CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Porting SetObjPorting(Models.Porting.AT.PortinEntry M)
        {
            //DateTime datePortingTemp = DateTime.ParseExact(tbDesiredPortingDate.Text, "dd/MM/yyyy", new System.Globalization.CultureInfo(System.Globalization.CultureInfo.CurrentCulture.Name));
            return new CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Porting()
            {
                Msisdn = M.PortedMsisdn,
                VoiceMail = M.VoiceMail,
                PortingCode = M.PortingCode,
                LCode = 0,
                LDesc = Helpers.Porting.AT.EnumErr.ToText(0),
                GCode = 0,
                GDesc = Helpers.Porting.AT.EnumErr.ToText(0),
                PortingType = 1,
                DesiredPortDate = M.DesiredPortedDate.ToString("yyyy-MM-dd"),
                MsgStatus = 0
            };
        }
        public static CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Transactions SetObjTrans(Models.Porting.AT.PortinEntry M)
        {
            //TODO : Implement Austria SOAP Reciever [Done]
            var oTrans = new CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Transactions();
            oTrans.RequestId = Helpers.Porting.AT.SoapReciever.GetRequestID(M.Sitecode);
            oTrans.Version = Helpers.Config.GetVersion(M.Sitecode);
            oTrans.EnvFlag = Helpers.Config.GetEnvFlag(M.Sitecode);
            oTrans.TimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            oTrans.Sender = Helpers.Config.GetPrefixFile(M.Sitecode);
            oTrans.Receiver = M.DonorCode;
            oTrans.MnpImp = Helpers.Config.GetPrefixFile(M.Sitecode);
            oTrans.MnpExp = M.DonorCode;
            oTrans.PortingStatus = 4;
            oTrans.PortingState = "RQP";
            return oTrans;
        }
        public static CRM_API.Models.Porting.AT.SoapReceiver.Transactions.NuvInfo SetSubscriberObject(Models.Porting.AT.PortinEntry M)
        {
            var oNuv = new CRM_API.Models.Porting.AT.SoapReceiver.Transactions.NuvInfo();
            oNuv.Prefix = M.Title;
            oNuv.FirstName = M.Firstname;
            oNuv.LastName = M.Lastname;
            oNuv.HouseNo = M.Houseno;
            oNuv.Address = M.Address1;
            oNuv.Address2 = M.Address2;
            oNuv.City = M.City;
            oNuv.State = M.State;
            oNuv.Postcode = M.Postcode;
            oNuv.Country = M.CountryCode;
            oNuv.Telephone = M.Telephone;
            oNuv.Fax = M.Fax;
            oNuv.Email = M.Fax;
            oNuv.BirthDate = M.DOB.ToString("dd/MMM/yyyy");
            oNuv.SubType = M.SubscriberType;
            oNuv.Msisdn = M.BBMsisdn;
            oNuv.CustomerId = "0";
            //oNuv.CustomerId = Universal.SelectedSubscriber != null ? Universal.SelectedSubscriber.SubscriberID.ToString() : "0";
            //oNuv.CustomerId = Universal.SelectedSubscriber != null ? Universal.SelectedSubscriber.SubscriberID.ToString() : "";
            oNuv.CompanyName = M.CompanyName;

            return oNuv;
        }
        public static List<CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Mobile> SetMobileObject(Models.Porting.AT.PortinEntry M)
        {
            var arlMobile = new List<CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Mobile>();
            var oMbl = new CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Mobile();
            oMbl.Msisdn = M.PortedMsisdn;
            arlMobile.Add(oMbl);
            if (M.VoiceMail != string.Empty)
            {
                // change reference to same object oMbl for voice number to oMbl2
                // referencing to same object results in 2 same number [voice number],
                // as object was passed by reference through method
                CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Mobile oMbl2 = new CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Mobile();
                oMbl2.Msisdn = M.VoiceMail;
                oMbl2.IsVoiceMail = 1;
                arlMobile.Add(oMbl2);
            }
            return arlMobile;
        }
        public static CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Cancel SetObjCancel(Models.Porting.AT.PortinEntry M)
        {
            var oCancel = new CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Cancel();
            oCancel.CancelCode = M.CancelCode;
            oCancel.PortingType = 1;
            oCancel.MsgStatus = 0;
            oCancel.PortingCode = M.PortingCode;
            oCancel.GCode = 0;
            oCancel.GDesc = CRM_API.Helpers.Porting.AT.EnumErr.ToText(oCancel.GCode);
            return oCancel;
        }
        public static CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Transactions SetObjTransForCancel(Models.Porting.AT.PortinEntry M)
        {
            //TODO : Implement Austria SOAP Reciever [Done]
            var oTrans = new CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Transactions();
            oTrans.RequestId = Helpers.Porting.AT.SoapReciever.GetRequestID(M.Sitecode);
            oTrans.Version = Helpers.Config.GetVersion(M.Sitecode);
            oTrans.EnvFlag = Helpers.Config.GetEnvFlag(M.Sitecode);
            oTrans.TimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            oTrans.Sender = Helpers.Config.GetPrefixFile(M.Sitecode);
            oTrans.Receiver = M.DonorCode;
            oTrans.PortingStatus = 7;
            oTrans.PortingState = "RQC";
            return oTrans;
        }
        public static CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Transactions SetObjTransForNuevo(Models.Porting.AT.PortinEntry M,string RequestId)
        {
            //TODO : Implement Austria SOAP Reciever [Done]
            var oTrans = new CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Transactions();
            oTrans.RequestId = RequestId;
            oTrans.Version = Helpers.Config.GetVersion(M.Sitecode);
            oTrans.EnvFlag = Helpers.Config.GetEnvFlag(M.Sitecode);
            oTrans.TimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            oTrans.Sender = Helpers.Config.GetPrefixFile(M.Sitecode);
            oTrans.Receiver = M.DonorCode;
            oTrans.MnpImp = Helpers.Config.GetPrefixFile(M.Sitecode);
            oTrans.MnpExp = M.DonorCode;
            oTrans.PortingStatus = 1;
            oTrans.PortingState = "RQN";
            return oTrans;
        }
        public static CRM_API.Models.Porting.AT.SoapReceiver.Transactions.NuvInfo SetObjNuv(Models.Porting.AT.PortinEntry M)
        {
            Austria.ServiceWebsvcSoapClient oS = new Austria.ServiceWebsvcSoapClient();
            var oNuv = new CRM_API.Models.Porting.AT.SoapReceiver.Transactions.NuvInfo();
            oNuv.RequestId = oS.GetRequestID();
            oNuv.Puk = M.PUK;
            oNuv.MsgStatus = 0;
            oNuv.PortingType = 1;
            oNuv.GCode = 0;
            oNuv.GDesc = CRM_API.Helpers.Porting.AT.EnumErr.ToText(oNuv.GCode);
            oNuv.Msisdn = M.Msisdn;
            return oNuv;
        }
        public static List<CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Mobile> SetMobileObjForNuevo(Models.Porting.AT.PortinEntry M)
        {
            var arlMbl = new List<CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Mobile>();
            var oMbl = new CRM_API.Models.Porting.AT.SoapReceiver.Transactions.Mobile();
            oMbl.Msisdn = M.Msisdn;
            oMbl.LCode = 0;
            oMbl.LDesc = CRM_API.Helpers.Porting.AT.EnumErr.ToText(oMbl.LCode);
            oMbl.IsVoc = 0;
            arlMbl.Add(oMbl);
            return arlMbl;
        }
    }
}
