﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CRM_API.DB.BreakageUsage;
using CRM_API.Models.BreakageUsageModels;

namespace CRM_API.Controllers
{
    public class BreakageUsageController : ApiController
    {
        //POST api/BreakageUsage/
        public HttpResponseMessage Post(BreakageUsageInput Model)
        {
            List<BreakageUsageOutput> resultValues = BreakageUsage.GetBreakageUsageInfo(Model).ToList();
            return Request.CreateResponse(HttpStatusCode.OK, resultValues.ToArray());
        }
    }
}