﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;
using NLog;


namespace CRM_API.Controllers
{

    /// <summary>
    /// PAMY DD Stetup For Back Office
    /// api/PAMYDDStetup
    /// </summary>
    public class PAYMDDStetupController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        /// <summary>
        /// PAMY DD Stetup For Back Office
        /// Get : /api/PAYMDDStetup
        /// </summary>
        public HttpResponseMessage Get()
        {
            try
            {
                var result = CRM_API.DB.PAYMDDStetup.GetAllBank();
                return Request.CreateResponse(HttpStatusCode.OK, result.ToList());
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new List<CRM_API.Models.getBankList>());
            }
        }

        /// <summary>
        /// PAMY DD Stetup For Back Office
        /// POST : /api/PAYMDDStetup
        /// </summary>
        public HttpResponseMessage Post(CRM_API.Models.validationBank objValidationBank)
        {
            try
            {
                Log.Debug("ProcessPayNow: Started");
                //30-Jan-2017 : Moorthy : Added for product code
                System.Web.HttpContext.Current.Session["GlobalProduct"] = objValidationBank.productCode;

                //07-Feb-2017 : Moorthy : Modified for VMAT & VMNL (Validation not required) 
                //if (objValidationBank.productCode == "VMUK")
                //{
                //    List<CRM_API.Models.getBankList> result = CRM_API.DB.PAYMDDStetup.CheckAccount(objValidationBank).ToList();
                //    Log.Debug("ProcessPayNow: Returned" + result[0].Result);
                //    if (result[0].Result == "-1" || result[0].Result == "False")
                //    {
                //        //Calling the Save Function. 
                //        result[0].errmsg = "Invalid Account";
                //        result[0].errcode = -1;
                //        return Request.CreateResponse(HttpStatusCode.OK, result.ToList());
                //    }
                //}

                //Save the Direct Debit Account.
                Log.Debug("ProcessPayNow: Suscess Process");
                var lastresult = CRM_API.DB.PAYMDDStetup.BasicDetails(Convert.ToString(objValidationBank.subscriberID)).ToList();
                if (lastresult == null || lastresult.Count == 0)
                {
                    List<CRM_API.Models.Common.ErrCodeMsg> outputList = new List<Models.Common.ErrCodeMsg>();
                    CRM_API.Models.Common.ErrCodeMsg output = new Models.Common.ErrCodeMsg();
                    output.errcode = -1;
                    output.errmsg = string.Format("Basic Details for the Subscriber ID : {0} is empty!", objValidationBank.subscriberID);
                    outputList.Add(output);
                    return Request.CreateResponse(HttpStatusCode.OK, outputList);
                }
                Log.Debug("ProcessPayNow: basic details Returned" + lastresult[0].freesimid);
                CRM_API.Models.AddDDSetUp objAddDDSetUp = new Models.AddDDSetUp();
                //Setting values
                objAddDDSetUp.subscriberid = lastresult[0].subscriberid;
                objAddDDSetUp.freesimid = lastresult[0].freesimid;
                objAddDDSetUp.paymentref = "";
                objAddDDSetUp.sort_code = objValidationBank.sortCode;
                objAddDDSetUp.account_num = objValidationBank.accoutNumber;
                objAddDDSetUp.account_name = objValidationBank.accountName;
                //objAddDDSetUp.amount = float.Parse(lastresult[0].amountpaidtag);
                objAddDDSetUp.amountPaidTag = lastresult[0].amountpaidtag;
                objAddDDSetUp.updateby = objValidationBank.updateby;
                objAddDDSetUp.bank_name = objValidationBank.bankName;

                //07-Feb-2017 : Moorthy : Added for VMFR (Branch Code) 
                objAddDDSetUp.branch_code = objValidationBank.branchCode;

                //02-Jun-2017 : Moorthy : Added for VMFR/VMAT/VMNL (Country) 
                objAddDDSetUp.country = objValidationBank.country;

                //03-Dec-2018 : Moorthy : Added for Mandate Creation in Add New Direct Debit Details screen
                objAddDDSetUp.mobileno = objValidationBank.mobileNo;
                objAddDDSetUp.sitecode = objValidationBank.sitecode;
                objAddDDSetUp.pp_customer_id = objValidationBank.pp_customer_id;

                Log.Debug("ProcessPayNow: DD before Insert");
                var finalResults = CRM_API.DB.PAYMDDStetup.saveDirectDebit(objAddDDSetUp, objValidationBank.productCode);
                Log.Debug("ProcessPayNow: After Insert" + JsonConvert.SerializeObject(finalResults));
                return Request.CreateResponse(HttpStatusCode.OK, finalResults.ToList());

            }
            catch (Exception ex)
            {
                Log.Error("ProcessPayNow: " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new List<CRM_API.Models.validationBank>());
            }
        }
    }
}
