﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CRM_API.Controllers
{
    public class CustomerOverviewController : ApiController
    {
        public HttpResponseMessage Post(CRM_API.Models.CustomerOverviewPageInput input)
        {
            System.Web.HttpContext.Current.Session["GlobalProduct"] = input.Product_Code;

            CRM_API.Models.CustomerOverviewPageOutput resValues =
                CRM_API.DB.Customer.CustomerOverview.GetCustomerOverview(input.MobileNo, input.Sitecode);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new CRM_API.Models.CustomerOverviewPageOutput());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues);
        }
    }
}
