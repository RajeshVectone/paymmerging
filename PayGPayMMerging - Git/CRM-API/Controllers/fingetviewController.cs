﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Web.Http;
using System.Xml;
using Newtonsoft.Json;
using NLog;

namespace CRM_API.Controllers
{
    public class fingetviewController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        //
        // GET: /fingetview/



        public HttpResponseMessage POST(CRM_API.Models.Sucess_failed objSucess_failed)
        {
            try
            {

                //var result = CRM_API.DB.viewtransaction.getMerchentIDs(objValues.merchID.ToString());
                //IEnumerable<CRM_API.Models.getafterpaymentdetails> results= CRM_API.DB.viewtransaction.afterpayment(objSucess_failed.MerchID, objSucess_failed.Reason, "payment reversed successfully");
                // var results = CRM_API.DB.viewtransaction.afterpayment(objSucess_failed.MerchID, objSucess_failed.Reason, "payment reversed successfully");
                //// CRM_API.Models.getafterpaymentdetails result = results.FirstOrDefault();
                // return Request.CreateResponse(HttpStatusCode.OK, results.ToArray());

                // // 0-- Create 1 -- revert
                string param = objSucess_failed.MerchID;
                string[] split = param.Split(':');
                string merchid = split[0];
                string accountid = split[1];
                string amount = split[2];
                if (objSucess_failed.Create_revert == "1")
                {
                    var result = CRM_API.DB.viewtransaction.getTransaction("http://82.113.74.28:8033/Transaction.svc/subscription/revCreate?referenceId=" + merchid);
                    if (result)
                    {

                        Log.Debug(string.Format("paymenttest{0} {1} {2}{3}", param, merchid, accountid, amount));
                        var results = CRM_API.DB.viewtransaction.afterpayment_cancel(merchid, accountid, objSucess_failed.Reason, "Payment reversed successfully");

                        //var results = CRM_API.DB.viewtransaction.afterpayment(merchid, objSucess_failed.Reason, "payment reversed successfully");
                        //var results = CRM_API.DB.viewtransaction.afterpayment_topup(accountid, merchid, amount, objSucess_failed.Reason, "Transaction reversed successfully");                        
                        return Request.CreateResponse(HttpStatusCode.OK, results.ToArray());
                    }
                    else
                    {

                        Log.Debug(string.Format("paymenttest{0} {1} {2}{3}", param, merchid, accountid, amount));
                        var results = CRM_API.DB.viewtransaction.afterpayment(merchid, objSucess_failed.Reason, "payment reversed failure");
                        return Request.CreateResponse(HttpStatusCode.OK, results.ToArray());

                    }
                }


                else
                {
                    var result = CRM_API.DB.viewtransaction.getTransaction("http://82.113.74.28:8033/Transaction.svc/subscription/dmCreate?referenceId=" + merchid);
                    if (result)
                    {                     
                        


                        // var results = CRM_API.DB.viewtransaction.afterpayment(merchid, objSucess_failed.Reason, "Transaction Accepted successfully");
                        //var results = CRM_API.DB.viewtransaction.afterpayment_topup(merchid,accountid,amount, objSucess_failed.Reason, "Transaction Accepted successfully");
                        CRM_API.Models.getafterpaymentdetails_topup[] results = CRM_API.DB.viewtransaction.afterpayment_topup(accountid, merchid, amount, objSucess_failed.Reason, "Transaction Accepted successfully").ToArray();
                        Helpers.SendSMS sms = new Helpers.SendSMS();
                        sms.Send(false, accountid, results[0].sms_sender, results[0].sms_text, 5000);
                        Log.Debug(string.Format("paymenttest{0} {1} {2}{3}", param, merchid, accountid, amount));
                        string prevbal = results[0].prevbal;
                        string afterbal = results[0].afterbal;
                        string bonuscredit = results[0].freecredit;
                        bool topup = Topupsucess(accountid, amount, merchid, prevbal, afterbal, bonuscredit);
                        return Request.CreateResponse(HttpStatusCode.OK, results.ToArray());
                    }
                    else
                    {

                        Log.Debug(string.Format("paymenttest{0} {1} {2}{3}", param, merchid, accountid, amount));
                        var results = CRM_API.DB.viewtransaction.afterpayment(merchid, objSucess_failed.Reason, "Transaction Accepted failure");
                        return Request.CreateResponse(HttpStatusCode.OK, results.ToArray());
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, CRM_API.DB.viewtransaction.afterpayment(objSucess_failed.MerchID, objSucess_failed.Reason, "payment reversed successfully").ToArray());
            }
            catch (WebException ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new List<CRM_API.Models.getafterpaymentdetails>());
            }
            return Request.CreateResponse(HttpStatusCode.OK, new List<CRM_API.Models.getafterpaymentdetails>());
        }


        public string getproduct(string mobileno)
        {
            string surl = "http://sws.vectone.com/v1/mobile/" + mobileno;
            string productname = "";
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(surl);
            req.UserAgent = "Mozilla";
            req.Headers["user"] = "tester";
            req.Method = "GET";
            req.Timeout = 180000;
            req.ContentType = "application/json";
            try
            {
                using (var memoryStream = new MemoryStream())
                {
                    HttpWebResponse res = (HttpWebResponse)req.GetResponse();
                    Stream resStream = res.GetResponseStream();
                    StreamReader reader = new StreamReader(resStream);
                    string resultt = reader.ReadToEnd();
                    res.Close();
                    RootObject list = JsonConvert.DeserializeObject<RootObject>(resultt);
                    productname = list.ProductCode + list.Country.Id;

                    return productname;
                }
            }
            catch (Exception ex)
            {
                return productname;
            }
        }


        public bool Topupsucess(string accountid, string amount, string merchid, string prevbal, string afterbal, string bonuscredit)
        {
            try
            {
                string mailSubject = "Topup Confirmation.";
                string mailLocation;
                string name = getproduct(accountid);
                if(name=="VMGB")
                {       
                mailLocation = System.Web.HttpContext.Current.Server.MapPath("~/Email_template/vmuktemail.htm").TrimEnd('/');
                StreamReader mailContent_file = new StreamReader(mailLocation);
                string mailContent = mailContent_file.ReadToEnd();
                mailContent_file.Close();
                mailContent = mailContent
                .Replace("[MerchantReference]", merchid)
                .Replace("[TopupAmount]", amount)
                .Replace("[VectoneNo]", accountid)
                .Replace("[BalanceAfter]", afterbal)
                .Replace("[BalancePrev]", prevbal)
                .Replace("[Bonus]", bonuscredit);
                // Send the email
                MailAddress mailFrom = new MailAddress("noreply@vectonemobile.co.uk", "Vectone Mobile");
                MailAddressCollection mailTo = new MailAddressCollection();
                string email = getmail(accountid);
                mailTo.Add(new MailAddress(email, "customer"));
                return CRM_API.Helpers.EmailSending.Send(true, mailFrom, mailTo, null, null, mailSubject, mailContent);
                }
                 return CRM_API.Helpers.EmailSending.Send(false,null,null,null,null,null,null);
            }
            catch (Exception ex)
            {
                Log.Error("PAYMPlanBundlesController.EmailCancelPlan", ex.Message);
                return false;
            }

        }

        public static string getmail(string mobileno)
        {
            try
            {
                SqlCommand sql = new SqlCommand("getpaymentcustemailid", new SqlConnection(CRM_API.Helpers.Config.PaymentConnection));
                sql.CommandType = CommandType.StoredProcedure;
                sql.Parameters.Add("@account_id", SqlDbType.VarChar, 16).Value = mobileno;
                return (ExecuteScalar(sql, "email") ?? "").ToString();
            }
            catch (Exception ex)
            {               
                return null;
            }

        }


        public static object ExecuteScalar(SqlCommand command, string columnName)
        {
            object result = null;
            command.Connection.Open();
            using (SqlDataReader dataReader = command.ExecuteReader(CommandBehavior.KeyInfo))
            {
                if (dataReader.Read() && columnName != null)
                    result = dataReader[columnName];
            }
            command.Connection.Close();
            return result;
        }


    }
    }

