﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CRM_API.DB;
namespace CRM_API.Controllers
{
    public class MobileNoReportsController : ApiController
    {
        public HttpResponseMessage Post(CRM_API.Models.ReportsMo.reportsMobileReq Model)
        {
            try
            {
                switch (Model.Step)
                {
                    case Models.ReportsMo.Search.Mob:
                        return GetIssueTrackerBy(Model);
                    case Models.ReportsMo.Search.Date:
                        return GetIssueDetail(Model);
                    default: break;
                }
            }
            catch (Exception)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new CRM_API.Models.Common.ErrCodeMsg());
            }
            return Request.CreateResponse(HttpStatusCode.ExpectationFailed, Model);
        }
        private HttpResponseMessage GetIssueTrackerBy(CRM_API.Models.ReportsMo.reportsMobileReq In)
        {
            //var result = new List<CRM_API.Models.Issue.IssueList>();
            var result = CRM_API.DB.Report.Reported.GetIssueList(In.mobileno,In.sitecode);
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
        private HttpResponseMessage GetIssueDetail(CRM_API.Models.ReportsMo.reportsMobileReq In)
        {
            var result = CRM_API.DB.Report.Reported.GetIssueTrackerBy(In.startdate,In.enddate,In.sitecode);
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

       
    }
}