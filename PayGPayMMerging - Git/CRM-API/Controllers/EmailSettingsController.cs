﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Web;
using System.Web.Http;
using CRM_API.Models;

namespace CRM_API.Controllers
{
    public class EmailSettingsController : ApiController
    {
        public HttpResponseMessage Post(EmailSettingsModel model)
        {
            if (model != null)
            {
                switch (model.modetype)
                {
                    case Models.EmailSettingsModeType.GETRULEASSIGNING:
                        return GetRuleAssigning();
                    case Models.EmailSettingsModeType.UPDATERULEASSIGNING:
                        return UpdateRuleAssigning(model);
                    case Models.EmailSettingsModeType.GETSMSASSIGNING:
                        return GetSMSAssigning();
                    case Models.EmailSettingsModeType.UPDATESMSASSIGNING:
                        return UpdateSMSAssigning(model);
                    case Models.EmailSettingsModeType.GETEMAILASSIGNING:
                        return GetEmailAssigning();
                    case Models.EmailSettingsModeType.INSERTTICKETTYPE:
                        return InsertTicketType(model);
                    case Models.EmailSettingsModeType.INSERTCATEGORY:
                        return InsertCategory(model);
                    case Models.EmailSettingsModeType.INSERTSUBCATEGORY:
                        return InsertSubCategory(model);
                    default:
                        return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.EmailSettingsModel>().ToArray());
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.EmailModel>().ToArray());
            }
        }

        private HttpResponseMessage InsertSubCategory(EmailSettingsModel model)
        {
            IEnumerable<CRM_API.Models.EmailSettingsCommon> resValues = CRM_API.DB.EmailSettings.InsertSubCategory(model);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.EmailSettingsCommon>().ToArray());
            else
            {
                resValues.ElementAt(0).sitecode = model.sitecode;
                resValues.ElementAt(0).function_Id = model.function_Id;
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
            }
        }

        private HttpResponseMessage InsertCategory(EmailSettingsModel model)
        {
            IEnumerable<CRM_API.Models.EmailSettingsCommon> resValues = CRM_API.DB.EmailSettings.InsertCategory(model);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.EmailSettingsCommon>().ToArray());
            else
            {
                resValues.ElementAt(0).sitecode = model.sitecode;
                resValues.ElementAt(0).issue_id = model.issue_id;
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
            }
        }

        private HttpResponseMessage InsertTicketType(EmailSettingsModel model)
        {
            IEnumerable<CRM_API.Models.EmailSettingsCommon> resValues = CRM_API.DB.EmailSettings.InsertTicketType(model);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.EmailSettingsCommon>().ToArray());
            else
            {
                resValues.ElementAt(0).sitecode = model.sitecode;
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
            }
        }

        private HttpResponseMessage GetEmailAssigning()
        {
            IEnumerable<CRM_API.Models.EmailAssigningOutput> resValues = CRM_API.DB.EmailSettings.GetEmailAssigning();
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.EmailAssigningOutput>().ToArray());
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
            }
        }

        private HttpResponseMessage UpdateSMSAssigning(EmailSettingsModel model)
        {
            IEnumerable<CRM_API.Models.EmailSettingsCommon> resValues = CRM_API.DB.EmailSettings.UpdateSMSAssigning(model);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.EmailSettingsCommon>().ToArray());
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
            }
        }

        private HttpResponseMessage GetSMSAssigning()
        {
            IEnumerable<CRM_API.Models.SMSAssigningOutput> resValues = CRM_API.DB.EmailSettings.GetSMSAssigning();
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.SMSAssigningOutput>().ToArray());
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
            }
        }

        private HttpResponseMessage GetRuleAssigning()
        {
            IEnumerable<CRM_API.Models.EmailSettingsModel> resValues = CRM_API.DB.EmailSettings.GetRuleAssigning();
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.EmailSettingsModel>().ToArray());
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
            }
        }

        private HttpResponseMessage UpdateRuleAssigning(EmailSettingsModel model)
        {
            IEnumerable<CRM_API.Models.RuleAssigningOutput> resValues = CRM_API.DB.EmailSettings.UpdateRuleAssigning(model);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.RuleAssigningOutput>().ToArray());
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
            }
        }
    }
}
