﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using NLog;

namespace CRM_API.Controllers
{
    public class GoCheapController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        // POST api/gocheap/
        [HttpPost]
        public HttpResponseMessage POST(CRM_API.Models.GoCheapOrder orderRecord)
        {
            Log.Debug("--------------------------------------------------------------------");

            DB.Common.ErrCodeMsg errResult = new DB.Common.ErrCodeMsg()
            {
                errcode = -1,
                errsubject = "General Error",
                errmsg = "Empty New Order"
            };

            Log.Debug("GCMController.Post: Started");
            Log.Info(new JavaScriptSerializer().Serialize(orderRecord));

            if (orderRecord != null)
            {
                switch (orderRecord.order_type)
                {
                    case Models.GoCheapOrderType.ValidateMobileNo:
                        return Validate(orderRecord);
                    case Models.GoCheapOrderType.TopUp:
                        return TopUp(orderRecord);
                    case Models.GoCheapOrderType.UpdateBalance:
                        return UpdateBalance(orderRecord);
                    case Models.GoCheapOrderType.InsertNewOrder:
                        return InsertSIMOrder(orderRecord);
                    case Models.GoCheapOrderType.PersonalLogin:
                        return PersonalLogin(orderRecord);
                    case Models.GoCheapOrderType.UpdatePersonalInfo:
                        return UpdatePersonalInfo(orderRecord);
                    case Models.GoCheapOrderType.GetPersonalInfoForgotPassword:
                        return PersonalInfoForgotPassword(orderRecord);
                    case Models.GoCheapOrderType.GetAccountInfo:
                        return GetAccountInfo(orderRecord);
                    case Models.GoCheapOrderType.ResetPassword:
                        return DoResetPasswword(orderRecord);
                    case Models.GoCheapOrderType.Default:
                    default:
                        errResult.errmsg = "Unknown Order Type";
                        return Request.CreateResponse(HttpStatusCode.OK, errResult);
                }
            }
            else
            {
                Log.Debug(string.Format("GoCheapController.Post: Result Subject={0}; Msg={1}", errResult.errsubject, errResult.errmsg));
                return Request.CreateResponse(HttpStatusCode.OK, errResult);
            }
        }

        HttpResponseMessage Validate(CRM_API.Models.GoCheapOrder orderRecord)
        {
            DB.Common.ErrCodeMsg errResult = new DB.Common.ErrCodeMsg()
            {
                errcode = -1,
                errsubject = "Top Up Error"
            };

            if (string.IsNullOrEmpty(orderRecord.mobileno))
            {
                errResult.errmsg = "Empty Mobile Number";
                return Request.CreateResponse(HttpStatusCode.OK, errResult);
            }
            else
            {
                CRM_API.Models.GoCheapValidateResult result = CRM_API.DB.GoCheap.SProc.IsActiveMobileNo(orderRecord);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
        }

        HttpResponseMessage TopUp(CRM_API.Models.GoCheapOrder orderRecord)
        {
            DB.Common.ErrCodeMsg errResult = new DB.Common.ErrCodeMsg()
            {
                errcode = -1,
                errsubject = "Top Up Error"
            };

            if (string.IsNullOrEmpty(orderRecord.mobileno))
            {
                errResult.errmsg = "Empty Mobile Number";
                return Request.CreateResponse(HttpStatusCode.OK, errResult);
            }
            else if (orderRecord.amount <= 0)
            {
                errResult.errmsg = "Amount is empty";
                return Request.CreateResponse(HttpStatusCode.OK, errResult);
            }
            else
            {
                CRM_API.Models.GoCheapBalanceResult result = CRM_API.DB.GoCheap.SProc.TopUpByCCDC(orderRecord);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
        }

        HttpResponseMessage UpdateBalance(CRM_API.Models.GoCheapOrder orderRecord)
        {
            DB.Common.ErrCodeMsg errResult = new DB.Common.ErrCodeMsg()
            {
                errcode = -1,
                errsubject = "Top Up Error"
            };

            if (string.IsNullOrEmpty(orderRecord.mobileno))
            {
                errResult.errmsg = "Empty Mobile Number";
                return Request.CreateResponse(HttpStatusCode.OK, errResult);
            }
            else if (orderRecord.amount <= 0)
            {
                errResult.errmsg = "Amount is empty";
                return Request.CreateResponse(HttpStatusCode.OK, errResult);
            }
            else
            {
                CRM_API.Models.GoCheapBalanceResult result = CRM_API.DB.GoCheap.SProc.UpdateBalance(orderRecord);
                result.errsubject = "Top Up";
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
        }

        HttpResponseMessage InsertSIMOrder(CRM_API.Models.GoCheapOrder orderRecord)
        {
            CRM_API.Models.SimOrder errResult = new CRM_API.Models.SimOrder();

#if DEBUG
            orderRecord.date_of_birth = new DateTime(1915, 10, 14);
#endif
            orderRecord.subscription_id = CRM_API.DB.GoCheap.SProc.CreateSubscription(orderRecord);

            if (orderRecord.subscription_id <= 0)
            {
                errResult.errcode = -1;
                errResult.errmsg = "Failed to create subscription data";
                errResult.errsubject = "SIM Order Failed";
            }
            else
            {
                if (orderRecord.freesimstatus == 0)
                    orderRecord.freesimstatus = 1;
                int freesimStatus = 0;
                if (string.IsNullOrEmpty(orderRecord.paymentref))
                    freesimStatus = CRM_API.DB.GoCheap.SProc.CreateFreeSim(orderRecord);
                else
                    freesimStatus = CRM_API.DB.GoCheap.SProc.CreateFreeSimWithCredit(orderRecord);
                //errResult = CRM_API.DB.GoCheap.SProc.InsertSimOrderRecords(orderRecord);

                if (freesimStatus > 0)
                {
                    errResult.errsubject = "SIM Ordered Successfully";
                    errResult.errmsg = string.Format("You have placed an order for a new sim successfully. Subscription ID: {0}", orderRecord.subscription_id);
                }
                else
                {
                    errResult.errsubject = "SIM Order Failed";
                    if (string.IsNullOrEmpty(orderRecord.paymentref) || orderRecord.paymentref == "-1")
                        errResult.errmsg = string.Format("{0}", errResult.errmsg);
                    else
                        errResult.errmsg = string.Format("{0}. Order Number {1}", errResult.errmsg, orderRecord.paymentref);
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, errResult);
        }

        HttpResponseMessage InsertVerification(CRM_API.Models.GoCheapOrder orderRecord)
        {
            DB.Common.ErrCodeMsg errResult = new DB.Common.ErrCodeMsg()
            {
                errcode = -1,
                errsubject = "Verification Error"
            };

            if (string.IsNullOrEmpty(orderRecord.mobileno))
            {
                errResult.errmsg = "Empty Mobile Number";
                return Request.CreateResponse(HttpStatusCode.OK, errResult);
            }
            else
            {
                DB.Common.ErrCodeMsg result = CRM_API.DB.GoCheap.SProc.InsertVerification(orderRecord);
                result.errsubject = "Verification";
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
        }

        HttpResponseMessage PersonalLogin(CRM_API.Models.GoCheapOrder orderRecord)
        {
            DB.Common.ErrCodeMsg errResult = new DB.Common.ErrCodeMsg()
            {
                errcode = -1,
                errsubject = "Login Error"
            };

            if (string.IsNullOrEmpty(orderRecord.mobileno))
            {
                errResult.errmsg = "Empty Mobile Number";
                return Request.CreateResponse(HttpStatusCode.OK, errResult);
            }
            else if (string.IsNullOrEmpty(orderRecord.password))
            {
                errResult.errmsg = "Empty Password";
                return Request.CreateResponse(HttpStatusCode.OK, errResult);
            }
            else
            {
                CRM_API.Models.GoCheapLoginResult result = CRM_API.DB.GoCheap.SProc.PersonalLogin(orderRecord);
                result.errsubject = "Login";
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
        }

        HttpResponseMessage GetPersonalInfo(CRM_API.Models.GoCheapOrder orderRecord)
        {
            DB.Common.ErrCodeMsg errResult = new DB.Common.ErrCodeMsg()
            {
                errcode = -1,
                errsubject = "Get Personal Info Error"
            };

            if (string.IsNullOrEmpty(orderRecord.mobileno))
            {
                errResult.errmsg = "Empty Mobile Number";
                return Request.CreateResponse(HttpStatusCode.OK, errResult);
            }
            else if (orderRecord.subscription_id <= 0)
            {
                errResult.errmsg = "Empty Subscriber Id";
                return Request.CreateResponse(HttpStatusCode.OK, errResult);
            }
            else
            {
                CRM_API.Models.GoCheapPersonalInfo result = CRM_API.DB.GoCheap.SProc.GetPersonalInfo(orderRecord);
                result.errsubject = "Get Personal Info";
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
        }

        HttpResponseMessage UpdatePersonalInfo(CRM_API.Models.GoCheapOrder orderRecord)
        {
            DB.Common.ErrCodeMsg errResult = new DB.Common.ErrCodeMsg()
            {
                errcode = -1,
                errsubject = "Update Personal Info Error"
            };

            if (orderRecord.subscription_id <= 0)
            {
                errResult.errmsg = "Empty Subscriber Id";
                return Request.CreateResponse(HttpStatusCode.OK, errResult);
            }
            else
            {
                DB.Common.ErrCodeMsg result = CRM_API.DB.GoCheap.SProc.UpdatePersonalInfo(orderRecord);
                result.errsubject = "Update Personal Info";
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
        }

        HttpResponseMessage PersonalInfoForgotPassword(CRM_API.Models.GoCheapOrder orderRecord)
        {
            DB.Common.ErrCodeMsg errResult = new DB.Common.ErrCodeMsg()
            {
                errcode = -1,
                errsubject = "Get Personal Info Error"
            };

            if (string.IsNullOrEmpty(orderRecord.mobileno))
            {
                errResult.errmsg = "Empty Mobile Number";
                return Request.CreateResponse(HttpStatusCode.OK, errResult);
            }
            else
            {
                CRM_API.Models.GoCheapPersonalInfo result = CRM_API.DB.GoCheap.SProc.GetPersonalInfoForgotPassword(orderRecord);
                result.errsubject = "Get Personal Info";
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
        }

        HttpResponseMessage GetAccountInfo(CRM_API.Models.GoCheapOrder orderRecord)
        {
            DB.Common.ErrCodeMsg errResult = new DB.Common.ErrCodeMsg()
            {
                errcode = -1,
                errsubject = "Get Account Info Error"
            };

            if (string.IsNullOrEmpty(orderRecord.mobileno))
            {
                errResult.errmsg = "Empty Mobile Number";
                return Request.CreateResponse(HttpStatusCode.OK, errResult);
            }
            else
            {
                CRM_API.Models.GoCheapAccountInfo result = CRM_API.DB.GoCheap.SProc.GetAccountInfo(orderRecord);
                result.errsubject = "Get Account Info";
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
        }

        HttpResponseMessage GetTopUpLog(CRM_API.Models.GoCheapOrder orderRecord)
        {
            DB.Common.ErrCodeMsg errResult = new DB.Common.ErrCodeMsg()
            {
                errcode = -1,
                errsubject = "Get Top Up Log Error"
            };

            if (string.IsNullOrEmpty(orderRecord.mobileno))
            {
                errResult.errmsg = "Empty Mobile Number";
                return Request.CreateResponse(HttpStatusCode.OK, errResult);
            }
            else
            {
                IEnumerable<CRM_API.Models.GoCheapTopUpLogResult> result = CRM_API.DB.GoCheap.SProc.GetTopUpLog(orderRecord);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
        }

        HttpResponseMessage DoResetPasswword(CRM_API.Models.GoCheapOrder orderRecord)
        {
            DB.Common.ErrCodeMsg errResult = new DB.Common.ErrCodeMsg()
            {
                errcode = -1,
                errsubject = "Do Reset Password Error"
            };

            if (string.IsNullOrEmpty(orderRecord.password) || string.IsNullOrEmpty(orderRecord.new_password))
            {
                errResult.errmsg = "Empty Password/New Password";
                return Request.CreateResponse(HttpStatusCode.OK, errResult);
            }
            else
            {
                errResult = CRM_API.DB.GoCheap.SProc.ResetPassword(orderRecord);
                if (errResult.errcode == 0)
                {
                    errResult.errmsg = "Password has been changed successfully";
                }
                return Request.CreateResponse(HttpStatusCode.OK, errResult);
            }
        }
    }
}
