﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;

namespace CRM_API.Controllers
{
    public class CustomerTotalUsageHistoryController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public dynamic Post(CRM_API.Models.TotalUsageHistory.CustomerTotalUsageHistoryRequest x)
        {
            try
            {
                Log.Info(JsonConvert.SerializeObject(x));
                switch (x.RequestType)
                {
                    case Models.TotalUsageHistory.Action.History:
                        return GetListProfile(x);
                    case Models.TotalUsageHistory.Action.GetSubscribedBundlePackagesCDR:
                        return GetSubscribedBundlePackagesCDR(x);
                    case Models.TotalUsageHistory.Action.GetCallHistorySearchCBS:
                        return GetCallHistorySearchCBS(x);
                    default: break;
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new Models.Common.ErrCodeMsg()
                {
                    errcode = -1,
                    errmsg = "CustomerTotalUsageHistory:" + e.Message
                });
            }
            return Request.CreateResponse(HttpStatusCode.OK, x);
        }

        //28-Dec-2018 : Moorthy : Added for Bundle breakdown
        private dynamic GetCallHistorySearchCBS(Models.TotalUsageHistory.CustomerTotalUsageHistoryRequest x)
        {
            IEnumerable<CRM_API.Models.TotalUsageHistory.GetCallHistorySearchCBSRef> result = CRM_API.DB.TotalUsageHistory.SProc.GetCallHistorySearchCBS(x.Msisdn, x.Date_From1, x.Date_To1, x.search_type, x.Sitecode, x.PackageID,x.BundleName);
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        //28-Dec-2018 : Moorthy : Added for Bundle breakdown
        private dynamic GetSubscribedBundlePackagesCDR(CRM_API.Models.TotalUsageHistory.CustomerTotalUsageHistoryRequest x)
        {
            IEnumerable<CRM_API.Models.TotalUsageHistory.GetSubscribedBundlePackagesCDRRef> result = CRM_API.DB.TotalUsageHistory.SProc.GetSubscribedBundlePackagesCDR(x.Msisdn, x.Sitecode, x.search_type);
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        private dynamic GetListProfile(CRM_API.Models.TotalUsageHistory.CustomerTotalUsageHistoryRequest x)
        {
            IEnumerable<CRM_API.Models.TotalUsageHistory.Customerpaymenthistory> result = (CRM_API.DB.TotalUsageHistory.SProc.GettotalusagehistoryCall(x.Msisdn, x.Sitecode, x.Date_From1, x.Date_To1)
                                                                                                .Concat(CRM_API.DB.TotalUsageHistory.SProc.GettotalusagehistorySMS(x.Msisdn, x.Sitecode, x.Date_From1, x.Date_To1))
                                                                                                .Concat(CRM_API.DB.TotalUsageHistory.SProc.GettotalusagehistoryData(x.Msisdn, x.Sitecode, x.Date_From1, x.Date_To1))
                                                                                                .Concat(CRM_API.DB.TotalUsageHistory.SProc.GettotalusagehistoryLLOM(x.Msisdn, x.Sitecode, x.Date_From1, x.Date_To1)).OrderByDescending(s => s.paydate));
            return Request.CreateResponse(HttpStatusCode.OK, result);

        }
    }
}
