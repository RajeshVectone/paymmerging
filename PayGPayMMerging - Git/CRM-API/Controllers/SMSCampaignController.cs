﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using NLog;
using System.Text;
using System.Net.Http.Headers;

namespace CRM_API.Controllers
{
    public class SMSCampaignController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        // GET api/customer2in1/MobileNo
        //public HttpResponseMessage Get(string id)
        //{
        //    IEnumerable<CRM_API.Models.Customer2in1Records> resValues =
        //        CRM_API.DB.Customer2in1.SProc.List2in1Records(id);

        //    if (resValues == null)
        //        return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.Customer2in1Records>().ToArray());
        //    else
        //        return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        //}

        // POST api/bundle/
        [HttpPost]
        public HttpResponseMessage Post(CRM_API.Models.SMSCampaignRequest req)
        {
            DB.Common.ErrCodeMsg errResult = new DB.Common.ErrCodeMsg()
           {
               errcode = -1,
               errsubject = "General Error",
               errmsg = "Empty New History"
           };
            try
            {
                //31-Jul-2015 : Moorthy Added
                System.Web.HttpContext.Current.Session["GlobalProduct"] = req.ProductCode;
                switch (req.Action)
                {
                    case CRM_API.Models.CampaignModelType.GetCustomerWiseSMSNotification:
                        return GetCustomerWiseSMSNotification(req);
                    case Models.CampaignModelType.SendSMS:
                        return SendSMS(req);
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, errResult);
        }

        private HttpResponseMessage GetCustomerWiseSMSNotification(CRM_API.Models.SMSCampaignRequest requestData)
        {
            IEnumerable<CRM_API.Models.CustomerWiseSMSNotification> resValues = CRM_API.DB.SMSCampaign.SProc.GetCustomerWiseSMSNotification(requestData.MobileNo, requestData.Sitecode);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.CustomerWiseSMSNotification>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        private HttpResponseMessage SendSMS(CRM_API.Models.SMSCampaignRequest requestData)
        {
            string smsURL =  CRM_API.DB.SMSCampaign.SProc.GetSMSGatewayInfo(requestData.Sitecode, requestData.MobileNo);
            var smsResponse = CRM_API.Helpers.SendSMS.SMSSendOurGateway("Vectone", requestData.MobileNo, requestData.SMSText, smsURL);
            IEnumerable<CRM_API.Models.InsertSMSNotifyTextOutput> resValues = CRM_API.DB.SMSCampaign.SProc.InsertSMSNotifyText(requestData.MobileNo, requestData.SMSText, requestData.LoggedUser, smsResponse == "-1" ? 0 : 1, requestData.smsType, requestData.LoggedUser, "Agent Send SMS via CRM");
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.InsertSMSNotifyTextOutput>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }


        //private HttpResponseMessage GetSMSChangeMarketingHistory(CRM_API.Models.SMSCampaignRequest requestData)
        //{
        //    IEnumerable<CRM_API.Models.SMSMarketingHistory> resValues = CRM_API.DB.SMSCampaign.SProc.GetSMSMarketingHistory(requestData.MobileNo, requestData.Sitecode);
        //    if (resValues == null)
        //        return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.PAYMPlan>().ToArray());
        //    else
        //        return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        //}
       
        //private HttpResponseMessage GetEmailChangeMarketingHistory(CRM_API.Models.SMSCampaignRequest requestData)
        //{
        //    IEnumerable<CRM_API.Models.SMSMarketingHistory> resValues = CRM_API.DB.SMSCampaign.SProc.GetSMSMarketingHistory(requestData.MobileNo, requestData.Sitecode);
        //    if (resValues == null)
        //        return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.PAYMPlan>().ToArray());
        //    else
        //        return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        //}
       
        //private HttpResponseMessage GetCampaignList(CRM_API.Models.SMSCampaignRequest requestData)
        //{
        //    IEnumerable<CRM_API.Models.SMSMarketingHistory> resValues = CRM_API.DB.SMSCampaign.SProc.GetSMSMarketingHistory(requestData.MobileNo, requestData.Sitecode);
        //    if (resValues == null)
        //        return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.PAYMPlan>().ToArray());
        //    else
        //        return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        //}
    }
}
