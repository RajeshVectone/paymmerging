﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Http;
using CRM_API.Models;
using Mandrill;
using Mandrill.Model;

namespace CRM_API.Controllers
{
    public class EmailGenerateTicketController : ApiController
    {
        public HttpResponseMessage Post(EmailGenerateTicketModels Model)
        {
            // System.Web.HttpContext.Current.Session["GlobalProduct"]
            if (Model != null)
            {
                switch (Model.ModeType)
                {
                    case GenerateTicketModeType.ESCALATIONTEAM:
                        return GetEscalationTeamInfo(Model.role_id);
                    case GenerateTicketModeType.STATUS:
                        return GetStatusInfo(Model.userid);
                    case GenerateTicketModeType.TICKETTYPE:
                        return GetTicketTypeInfo(Model.sitecode);
                    case GenerateTicketModeType.CATEGORY:
                        return GetCategoryInfo(Model.sitecode, Model.Issue_Id);
                    case GenerateTicketModeType.SUBCATEGORY:
                        return GetSubCategoryInfo(Model.sitecode, Model.Function_Id);
                    //case GenerateTicketModeType.GENERATETICKET:
                    //    return await GenerateTicket(Model);
                    case GenerateTicketModeType.VIEWTICKET:
                        return ViewTicket(Model);
                    default:
                        return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.EmailModel>().ToArray());
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.EmailModel>().ToArray());
            }
        }

        public HttpResponseMessage GetSubCategoryInfo(string sitecode, int Function_Id)
        {
            IEnumerable<CRM_API.Models.SubCategoryInfo> resValues = CRM_API.DB.EmailGenerateTicket.GetSubCategoryInfo(sitecode, Function_Id);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.SubCategoryInfo>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        public HttpResponseMessage GetCategoryInfo(string sitecode, int Issue_Id)
        {
            IEnumerable<CRM_API.Models.CategoryInfo> resValues = CRM_API.DB.EmailGenerateTicket.GetCategoryInfo(sitecode, Issue_Id);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.CategoryInfo>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        public HttpResponseMessage GetTicketTypeInfo(string sitecode)
        {
            IEnumerable<CRM_API.Models.TicketTypeInfo> resValues = CRM_API.DB.EmailGenerateTicket.GetTicketTypeInfo(sitecode);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.TicketTypeInfo>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        public HttpResponseMessage GetEscalationTeamInfo(int role_id)
        {
            IEnumerable<CRM_API.Models.EscalationTeamInfo> resValues = CRM_API.DB.EmailGenerateTicket.GetEscalationTeamInfo(role_id);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.EscalationTeamInfo>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        public HttpResponseMessage GetStatusInfo(int userid)
        {
            IEnumerable<CRM_API.Models.StatusInfo> resValues = CRM_API.DB.EmailGenerateTicket.GetStatusInfo(userid);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.StatusInfo>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        private HttpResponseMessage ViewTicket(EmailGenerateTicketModels Model)
        {
            IEnumerable<CRM_API.Models.ViewTicketOutput> resValues = CRM_API.DB.EmailGenerateTicket.ViewTicket(Model.pkqueueid);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.ViewTicketOutput>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }
    }

}
