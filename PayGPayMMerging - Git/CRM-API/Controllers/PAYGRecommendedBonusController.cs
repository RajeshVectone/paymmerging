﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using NLog;
using System.IO;
using System.Net.Mail;

namespace CRM_API.Controllers
{
    public class PAYGRecommendedBonusController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        // GET api/PAYGRecommendedBonus/MobileNo
        public HttpResponseMessage Get(string MobileNo, string Site_Code)
        {
            Log.Debug("--------------------------------------------------------------------");
            return GetOverviewList(new Models.PAYGRecommendedBonus() { User_Id = MobileNo, Site_Code = Site_Code });
        }

        // POST api/PAYGRecommendedBonus/
        [HttpPost]
        public HttpResponseMessage POST(CRM_API.Models.PAYGRecommendedBonus requestData)
        {
            Log.Debug("--------------------------------------------------------------------");

            DB.Common.ErrCodeMsg errResult = new DB.Common.ErrCodeMsg()
            {
                errcode = -1,
                errsubject = "General Error",
                errmsg = "Empty New Order"
            };

            Log.Debug("PAYGRecommendedBonusController.Post: Started");
            Log.Info(new JavaScriptSerializer().Serialize(requestData));
            if (requestData != null)
            {
                switch (requestData.requestType)
                {
                    case Models.PAYGRecommendedBonusRequestType.RBOverView:
                        return GetOverviewList(requestData);
                    case Models.PAYGRecommendedBonusRequestType.RBReferralList:
                        return GetRBReferralHistoryList(requestData);
                    case Models.PAYGRecommendedBonusRequestType.RBTransactionList:
                        return GetRBTransctionHistoryList(requestData);
                    case Models.PAYGRecommendedBonusRequestType.RBBindReferralStatus:
                        return GetRBReferralStatus(requestData);
                    case Models.PAYGRecommendedBonusRequestType.RBBindReferralSimStatus:
                        return GetRBReferralSimStatus(requestData);
                    case Models.PAYGRecommendedBonusRequestType.RBBindRechargeMode:
                        return GetRBRechargeMode(requestData);
                    case Models.PAYGRecommendedBonusRequestType.RBBindTransactionType:
                        return GetTransaction_Transfer_Type(requestData);
                    case Models.PAYGRecommendedBonusRequestType.RBReferralDetailsView:
                        return GetRBReferralHistoryView(requestData);
                    case Models.PAYGRecommendedBonusRequestType.RBReferralDetailsUpdate:
                        return GetRBReferralHistoryUpdate(requestData);
                    case Models.PAYGRecommendedBonusRequestType.RBTransactionDetailsView:
                        return GetRBTransactionHistoryView(requestData);
                    case Models.PAYGRecommendedBonusRequestType.RBTransactionDetailsUpdate:
                        return GetRBTransactionHistoryUpdate(requestData);
                    case Models.PAYGRecommendedBonusRequestType.RBBindPaymentStatus:
                        return GetTransaction_Payment_Status(requestData);
                    default:
                        errResult.errmsg = "Unknown Order Type";
                        break;
                }
                requestData.errcode = errResult.errcode;
                requestData.errmsg = errResult.errmsg;
            }
            else
            {
                requestData = new Models.PAYGRecommendedBonus()
                {
                    errcode = errResult.errcode,
                    errmsg = errResult.errmsg
                };
            }
            Log.Debug(string.Format("PAYGRecommendedBonusController.Post: Result Subject={0}; Msg={1}", errResult.errsubject, errResult.errmsg));
            return Request.CreateResponse(HttpStatusCode.OK, requestData);
        }

        private HttpResponseMessage GetOverviewList(Models.PAYGRecommendedBonus requestData)
        {
            Log.Debug("PAYGRecommendedBonusController.GetOverviewList: Started");
            IEnumerable<CRM_API.Models.PAYGRBOverview> resValues = CRM_API.DB.PAYGRecommendedBonus.SProc.GetRBOverView(requestData.User_Id, requestData.Site_Code);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.PAYGRBOverview>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        private HttpResponseMessage GetRBReferralHistoryList(Models.PAYGRecommendedBonus requestData)
        {
            Log.Debug("PAYGRecommendedBonusController.GetRBReferralHistoryList: Started");
            IEnumerable<CRM_API.Models.PAYGRBReferralHistory> resValues = CRM_API.DB.PAYGRecommendedBonus.SProc.GetRBReferralHistory(requestData.User_Id, requestData.Site_Code, requestData.Referral_Status, requestData.SIM_Status, requestData.Recharge_Mode);

            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.PAYGRBReferralHistory>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }



        private HttpResponseMessage GetRBReferralHistoryView(Models.PAYGRecommendedBonus requestData)
        {
            Log.Debug("PAYGRecommendedBonusController.GetRBReferralHistoryView: Started");
            IEnumerable<CRM_API.Models.PAYGRBReferralDetailView> resValues = CRM_API.DB.PAYGRecommendedBonus.SProc.GetRBReferralDetailsView(requestData.User_Id, requestData.Site_Code, requestData.Referral_Id);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.PAYGRBReferralDetailView>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        private HttpResponseMessage GetRBReferralHistoryUpdate(Models.PAYGRecommendedBonus requestData)
        {
            Log.Debug("PAYGRecommendedBonusController.GetRBReferralHistoryUpdate: Started");
            IEnumerable<CRM_API.Models.PAYGRBReferralDetailView> resValues = CRM_API.DB.PAYGRecommendedBonus.SProc.GetRBReferralDetailsUpdate(requestData.User_Id, requestData.Site_Code, requestData.Referral_Id, requestData.Credit_Status_Desc, requestData.SIM_Status_Desc, requestData.User_Status_Desc);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.PAYGRBReferralDetailView>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        

        private HttpResponseMessage GetRBTransctionHistoryList(Models.PAYGRecommendedBonus requestData)
        {
            Log.Debug("PAYGRecommendedBonusController.GetRBTransctionHistoryList: Started");
            IEnumerable<CRM_API.Models.PAYGRBTransactionHistory> resValues = CRM_API.DB.PAYGRecommendedBonus.SProc.GetRBTransactionHistory(requestData.User_Id, requestData.Site_Code, requestData.Transfer_Type, requestData.Payment_Status);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.PAYGRBTransactionHistory>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        private HttpResponseMessage GetRBTransactionHistoryView(Models.PAYGRecommendedBonus requestData)
        {
            Log.Debug("PAYGRecommendedBonusController.GetRBTransactionHistoryView: Started");
            IEnumerable<CRM_API.Models.PAYGRBTransactionDetailView> resValues = CRM_API.DB.PAYGRecommendedBonus.SProc.GetRBTransactionDetailsView(requestData.User_Id, requestData.Site_Code, requestData.Trans_Id);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.PAYGRBTransactionDetailView>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        private HttpResponseMessage GetRBTransactionHistoryUpdate(Models.PAYGRecommendedBonus requestData)
        {
            Log.Debug("PAYGRecommendedBonusController.GetRBTransactionHistoryUpdate: Started");
            IEnumerable<CRM_API.Models.PAYGRBTransactionDetailView> resValues = CRM_API.DB.PAYGRecommendedBonus.SProc.GetRBTransactionDetailsUpdate(requestData.User_Id, requestData.Site_Code, requestData.Trans_Id, requestData.Transfer_Status_Desc, requestData.Payment_Status_Desc, requestData.Payment_Status);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.PAYGRBTransactionDetailView>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }


        #region DropDownlist
        private HttpResponseMessage GetRBReferralStatus(Models.PAYGRecommendedBonus requestData)
        {
            Log.Debug("PAYGRecommendedBonusController.GetRBReferralStatus: Started");
            IEnumerable<CRM_API.Models.PAYGRBReferral_ReferralStatus> resValues = CRM_API.DB.PAYGRecommendedBonus.SProc.BindReferralStatus(requestData.User_Id,requestData.Site_Code);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.PAYGRBReferral_ReferralStatus>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        private HttpResponseMessage GetRBReferralSimStatus(Models.PAYGRecommendedBonus requestData)
        {
            Log.Debug("PAYGRecommendedBonusController.GetRBReferralSimStatus: Started");
            IEnumerable<CRM_API.Models.PAYGRBReferral_ReferralSimStatus> resValues = CRM_API.DB.PAYGRecommendedBonus.SProc.BindReferralSimStatus(requestData.User_Id, requestData.Site_Code);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.PAYGRBReferral_ReferralSimStatus>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        private HttpResponseMessage GetRBRechargeMode(Models.PAYGRecommendedBonus requestData)
        {
            Log.Debug("PAYGRecommendedBonusController.GetRBRechargeMode: Started");
            IEnumerable<CRM_API.Models.PAYGRBReferral_RechargeMode> resValues = CRM_API.DB.PAYGRecommendedBonus.SProc.BindRechargeMode(requestData.User_Id, requestData.Site_Code);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.PAYGRBReferral_RechargeMode>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        private HttpResponseMessage GetTransaction_Transfer_Type(Models.PAYGRecommendedBonus requestData)
        {
            Log.Debug("PAYGRecommendedBonusController.GetTransaction_Transfer_Type: Started");
            IEnumerable<CRM_API.Models.PAYGRBTransaction_Transfer_Type> resValues = CRM_API.DB.PAYGRecommendedBonus.SProc.BindTransaction_Transfer_Type(requestData.Site_Code);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.PAYGRBTransaction_Transfer_Type>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        private HttpResponseMessage GetTransaction_Payment_Status(Models.PAYGRecommendedBonus requestData)
        {
            Log.Debug("PAYGRecommendedBonusController.GetTransaction_Payment_Status: Started");
            IEnumerable<CRM_API.Models.PAYGRBTransaction_Payment_Status> resValues = CRM_API.DB.PAYGRecommendedBonus.SProc.BindTransaction_Payment_Status(requestData.Site_Code);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.PAYGRBTransaction_Payment_Status>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        } 
        #endregion
    }
}
