﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NLog;
using System.IO;
using System.Text;
using System.Configuration;

namespace CRM_API.Controllers
{
    public class BundleController : ApiController
    {
        private const string smsTemplate = @"?service-name=port&destination-addr={0}&originator-addr-type=5&originator-addr={1}&payload-type=text&message={2}";

        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        //
        // GET api/bundle/

        public HttpResponseMessage Get(string id, string sitecode)
        {
            IEnumerable<CRM_API.Models.Bundle.SubscribedBundle> resValues =
                CRM_API.DB.Bundle.SProc.SubscribedBundle(id, sitecode);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.Bundle.SubscribedBundle>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }


        // POST api/bundle/
        [HttpPost]
        public HttpResponseMessage Post(CRM_API.Models.Bundle.BundleSearch resBundle)
        {
            DB.Common.ErrCodeMsg errResult = new DB.Common.ErrCodeMsg()
           {
               errcode = -1,
               errsubject = "General Error",
               errmsg = "Empty New History"
           };
            var result = new CRM_API.Models.Bundle.AvailableBundle();
            switch (resBundle.Bundle_Type)
            {
                case CRM_API.Models.Bundle.BundleType.ViewAvailableBundle:
                    return AvailableBundle(resBundle);
                case Models.Bundle.BundleType.ViewSubscribedBundle:
                    return SubsribedBundle(resBundle);
                //Added by karthik CRMT-196 Internet profile
                case Models.Bundle.BundleType.InternetProfile:
                    return InternetProfile(resBundle);
                case Models.Bundle.BundleType.GPRSSetting:
                    return GetGPRSSetting(resBundle);
                case Models.Bundle.BundleType.SubscribeBundle:
                    BundleSubscribe(resBundle, out errResult);
                    return Request.CreateResponse(HttpStatusCode.OK, errResult);
                case Models.Bundle.BundleType.InternetProfileSMS:
                    return SendGPRSSettings(resBundle);
                case Models.Bundle.BundleType.UnsubscribeBundle:
                    if (resBundle.UnsubscribeType == Models.Bundle.UnsubscribeType.expirydate)
                    {
                        UnsubscribeBundleExpiry(resBundle, out errResult);
                    }
                    else if (resBundle.UnsubscribeType == Models.Bundle.UnsubscribeType.immediately)
                    {
                        UnsubscribeBundleImmediately(resBundle, out errResult);
                    }
                    else
                    {
                        errResult.errcode = -1;
                        errResult.errmsg = "Unsubscribe Type Empty";
                        errResult.errsubject = "General Error";
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, errResult);
                case Models.Bundle.BundleType.GetLimitedMinInfoByBundleId:
                    return GetLimitedMinInfoByBundleId(resBundle);
                case Models.Bundle.BundleType.ExchangeBundle:
                    return ExchangeBundle(resBundle);
                default: break;
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        private HttpResponseMessage ExchangeBundle(Models.Bundle.BundleSearch resBundle)
        {
            IEnumerable<CRM_API.Models.Bundle.ExchangeBundleRef> resValues =
                          CRM_API.DB.Bundle.SProc.ExchangeBundle(resBundle);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.OK, new List<CRM_API.Models.Bundle.ExchangeBundleRef>() { new CRM_API.Models.Bundle.ExchangeBundleRef { errcode = -1, errmsg = "Failed" } }.ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        //Bundle Name Click
        //CRM Enhancement 2
        private HttpResponseMessage GetLimitedMinInfoByBundleId(Models.Bundle.BundleSearch resBundle)
        {
            IEnumerable<CRM_API.Models.Bundle.LimitedMinInfoByBundleIdRef> resValues =
                          CRM_API.DB.Bundle.SProc.GetLimitedMinInfoByBundleId(resBundle.sitecode, resBundle.config_id, resBundle.msisdn, resBundle.international_tariff, resBundle.subscription_date);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.Bundle.LimitedMinInfoByBundleIdRef>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        HttpResponseMessage AvailableBundle(CRM_API.Models.Bundle.BundleSearch resBundle)
        {
            IEnumerable<CRM_API.Models.Bundle.AvailableBundle> resValues =
                           CRM_API.DB.Bundle.SProc.AvailableBundle(resBundle.balance, resBundle.tariffclass, resBundle.bundle_name, resBundle.sitecode);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.Bundle.SubscribedBundle>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }
        HttpResponseMessage SubsribedBundle(CRM_API.Models.Bundle.BundleSearch resBundle)
        {
            IEnumerable<CRM_API.Models.Bundle.SubscribedBundle> resValues =
                           CRM_API.DB.Bundle.SProc.SubscribedBundle(resBundle.msisdn, resBundle.sitecode);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.Bundle.SubscribedBundle>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }
        //Added by karthik CRMT-196 internet Profile 
        HttpResponseMessage InternetProfile(CRM_API.Models.Bundle.BundleSearch resBundle)
        {
            IEnumerable<CRM_API.Models.Bundle.internetprofile> resValues =
                           CRM_API.DB.Bundle.SProc.internetprofile(resBundle.msisdn, resBundle.sitecode);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.Bundle.internetprofile>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }
        HttpResponseMessage GetGPRSSetting(CRM_API.Models.Bundle.BundleSearch resBundle)
        {
            IEnumerable<CRM_API.Models.Bundle.internetprofile> resValues =
                           CRM_API.DB.Bundle.SProc.GetGPRSSettingsMsg(resBundle.sitecode, resBundle.msisdn);

            var ret = GPRS_SettingSMS(resBundle.sitecode, resBundle.msisdn);
            // DB.Common.ErrCodeMsg errResult = new DB.Common.ErrCodeMsg();
            DB.Common.ErrCodeMsg errResult = new DB.Common.ErrCodeMsg();
            //string res = InternetProfileSMS(resBundle ,out errResult);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.Bundle.internetprofile>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }
        public bool GPRS_SettingSMS(string sitecode, string Desination_number)
        {
            try
            {
                //  string text = string.Format("You are recevied gprs config sms");
                string iOriginatorAddrType = "111";
                string address = "111";
                string res = new Helpers.SendSMS().Send_SMSGPRSSetting(sitecode, Desination_number, iOriginatorAddrType, address);
                //if (res != null && res.Count() > 0)
                //{
                //    return !(res[0] == "-1");
                //}
                //else
                //    return false;
                return false;
            }
            catch (Exception ex)
            {
                Log.Error("BundleController.GPRS_SettingSMS", ex.Message);
                return false;
            }
        }
        void BundleSubscribe(CRM_API.Models.Bundle.BundleSearch resBundle, out DB.Common.ErrCodeMsg errResult)
        {

            errResult = new DB.Common.ErrCodeMsg();
            errResult = DB.Bundle.SProc.SubscribeBundle(resBundle.sitecode, 2, resBundle.msisdn, resBundle.bundleid, 0, "Web CRM3", resBundle.pack_dest);
            errResult.errsubject = "Subscribe Bundle";
            if (errResult.errcode == 0)
                errResult.errmsg = "Subscribe Success";
        }

        HttpResponseMessage SendGPRSSettings(CRM_API.Models.Bundle.BundleSearch resBundle)
        {
            bool res = Send(resBundle.msisdn, "111", resBundle.sitecode.ToString());
            if (res == true)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new Models.Common.ErrCodeMsg { errcode = 0, errmsg = "Success" });
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, new Models.Common.ErrCodeMsg { errcode = 0, errmsg = "Failed" });
            }
        }

        public static bool Send(string destination, string originator, string Sitecode)
        {
            try
            {
                string sServerApiUrl = "";

                CRM_API.DB.Common.GetSmsIwmscUrlOutput output = CRM_API.DB.Bundle.SProc.GetSMSUrl(Sitecode);

                sServerApiUrl = output.sms_iwmsc_url;
                string msg = ConfigurationManager.AppSettings[Sitecode];

                int lenStr = msg.Length;

                int smsCount = 0;
                if (lenStr % 280 != 0)
                    smsCount = (lenStr / 280) + 1;
                else
                    smsCount = lenStr / 280;

                for (int j = 0; j < smsCount; j++)
                {
                    string smsMsg = "";
                    if (j == smsCount - 1)
                        smsMsg = msg.Substring(j * 280, lenStr - (280 * j));
                    else
                        smsMsg = msg.Substring(j * 280, 280);

                    string postData = "orig-addr=111" +
                    "&orig-noa=0" +
                    "&support-long=1" +
                    "&dest-addr=" + destination +
                    "&dest-noa=1" +
                    "&tp-dcs=21" +
                    "&tp-pid=245" +
                    "&tp-udhi=1" +
                    "&tp-ud=" + smsMsg +
                    "&payload-type=text";


                    //Send Request
                    HttpWebRequest Webrequest = (HttpWebRequest)WebRequest.Create(sServerApiUrl);
                    Webrequest.Method = "POST";
                    Webrequest.ContentType = "application/x-www-form-urlencoded";

                    ASCIIEncoding enc = new ASCIIEncoding();
                    byte[] data = enc.GetBytes(postData);
                    Webrequest.ContentLength = data.Length;

                    Stream newStream = Webrequest.GetRequestStream();
                    newStream.Write(data, 0, data.Length);
                    newStream.Close();

                    //Get the Response
                    HttpWebResponse wr = (HttpWebResponse)Webrequest.GetResponse();
                    StreamReader srd = new StreamReader(wr.GetResponseStream());
                    string resulXmlFromWebService = srd.ReadToEnd();
                    Log.Info("GPRS Settings : resulXmlFromWebService : " + resulXmlFromWebService);
                    return true;
                }
            }
            catch (Exception ex)
            {
                Log.Error("GPRS Settings : " + ex.Message);
            }
            return false;
        }


        void UnsubscribeBundleExpiry(CRM_API.Models.Bundle.BundleSearch resBundle, out DB.Common.ErrCodeMsg errResult)
        {
            errResult = new DB.Common.ErrCodeMsg();

            errResult = DB.Bundle.SProc.UnsubscribeBundleExpiry(resBundle.sitecode, 2, resBundle.msisdn, resBundle.bundleid, "CRM - " + resBundle.crm_user);
            errResult.errsubject = "Unsubscribe Bundle";
            if (errResult.errcode == 0)
                errResult.errmsg = "Unsubscribe Success";
        }
        void UnsubscribeBundleImmediately(CRM_API.Models.Bundle.BundleSearch resBundle, out DB.Common.ErrCodeMsg errResult)
        {
            errResult = new DB.Common.ErrCodeMsg();

            errResult = DB.Bundle.SProc.UnsubscribeBundleImmediately(resBundle.sitecode, 2, resBundle.msisdn, resBundle.bundleid, "Web CRM3");
            errResult.errsubject = "Unsubscribe Bundle";
            if (errResult.errcode == 0)
                errResult.errmsg = "Unsubscribe Success";
        }
    }
}
