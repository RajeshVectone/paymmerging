﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using CRM_API.Models.Porting.SE;
using CRM_API.DB.Porting.SE;

namespace CRM_API.Controllers
{
    /*
     * Porting for Sweden
    By Mamin @16-01-2014 @myamin_amzah@yahoo.com
    There's 4 feature in sweden porting
    its 
    1. request porting, 
    2. list of port in (search by MSISDN, search by porting date, search by requested date)
    3. list of port out (search by MSISDN, search by Porting date, 
    4. list of recently rejected port in
    */
    public class SEPortingController : ApiController
    {
        public async Task<HttpResponseMessage> Get([FromUri]AdditionalReqInfo Model)
        {
            var result = new Models.Common.ErrCodeMsg() { errcode = -1, errmsg = "Default Value" };
            try
            {                
                return await GetSelection(Model);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new Models.Common.ErrCodeMsg()
                {
                    errcode = -1,
                    errmsg = ex.Message
                });
            }             
        }

        private async Task<HttpResponseMessage> GetSelection(AdditionalReqInfo Model)
        {
            switch(Model.InfoType) {
                case AdditionInfo.ListDonor:
                    return Request.CreateResponse(HttpStatusCode.OK, await GetDonorOperator());
                case AdditionInfo.Detail:
                    return Request.CreateResponse(HttpStatusCode.OK, await GetPortDetail(Model.PortID));
                default: throw new EntryPointNotFoundException("Parameter InfoType Not Recognized");
            }
        }

        private async Task<IEnumerable<DonorSERes>> GetDonorOperator()
        {
            var result = await Task.Run(() => SProc.GetDonor());
            
            return result.Select(r => new DonorSERes()
            {
                DonorID = r.donorid,
                DonorName = r.donorname
            });
        }

        private async Task<PortingDetail> GetPortDetail(string PortID)
        {
            var r = await Task.Run(() => SProc.GetPortingDetail(PortID));
            return new PortingDetail()
            {
                PortStatus = r.port_status,
                Description = r.port_status_desc,
                MSISDN = r.msisdn,
                VectoneMSISDN = r.bb_msisdn,
                PortingDate = r.port_datetime,
                DNOCutOffTime = r.dno_cutoff_time,
                DNOName = r.dno_name,
                DonorServiceProvider = r.dsp_name,
                PersonalCorporateID = r.subs_pincin,
                Name = r.subs_name,
                Address = r.subs_addr,
                Postal = r.subs_postal,
                City = r.subs_city,
                Department = r.contact_dept,
                ContactPerson = r.contact_person,
                ContactPhone = r.contact_phone,
                ContactFax = r.contact_fax,
                ContactEmail = r.contact_email,
                OrderRejectCode = r.order_reject_cause_code,
                OrderRejectDesc = r.order_reject_cause_expl,
                LastUpdate = r.last_update
            };            
        }


        private PortinEntry InitiateObj (PortinEntry Model) { 
       
            if (String.IsNullOrEmpty(Model.PortingDateFrom)) 
                Model.PortingDateFrom = DateTime.Now.ToString("dd/MM/yyyy");
            if (String.IsNullOrEmpty(Model.PortingDateTo)) 
                Model.PortingDateTo = DateTime.Now.ToString("dd/MM/yyyy");
            if (String.IsNullOrEmpty(Model.RequestDateFrom)) 
                Model.RequestDateFrom = DateTime.Now.ToString("dd/MM/yyyy");
            if (String.IsNullOrEmpty(Model.RequestDateTo)) 
                Model.RequestDateTo = DateTime.Now.ToString("dd/MM/yyyy");
            if (String.IsNullOrEmpty(Model.PortingTime))
                Model.RequestDateTo = DateTime.Now.ToString("dd/MM/yyyy");
            if (String.IsNullOrEmpty(Model.PortingHour))
                Model.PortingHour = DateTime.Now.ToString("HH");    //get hour only
            if (String.IsNullOrEmpty(Model.MSISDN)) 
                Model.MSISDN = "1111";      //hardcoded avoid exception in sp 
            if (String.IsNullOrEmpty(Model.VectoneMSISDN))
                Model.VectoneMSISDN = "1111";      //hardcoded avoid exception in sp 
            return Model;
        }

        private async Task<IEnumerable<PortListResult>> GetPortinList(PortinEntry Model)
        {

            Model = InitiateObj(Model);
            IEnumerable<PortingList> result;            
            switch (Model.ActionType)
            {
                case PortingAction.PortinListByDate: result = await Task.Run(() => SProc.GetPortInByPortinDate(Model)); break;
                case PortingAction.PortinListByMSISDN: result = await Task.Run(() => SProc.GetPortInByMSISDN(Model)); break;
                case PortingAction.PortinListByRequestDate: result = await Task.Run(() => SProc.GetPortInByRequestDate(Model)); break;
                default:
                    throw new EntryPointNotFoundException("Parameter ActionType Not Defined");
            }

            return result.Select(r => new PortListResult()
            {
                PortID = r.port_id,
                MSISDN = r.msisdn,
                VectoneMSISDN = r.bb_msisdn,
                RequestedDate = r.created_date,
                PortingDate = r.port_datetime,
                SubscriberName = r.subs_name,
                ContactPhone = r.contact_phone,
                Email = r.contact_email,
                PortinStatus = r.port_status_desc,
                LastUpdate = r.last_update
            });
        }

        private async Task<IEnumerable<PortListResult>> GetPortOutList(PortinEntry Model)
        {

            Model = InitiateObj(Model);
            IEnumerable<PortingList> result;
            switch (Model.ActionType)
            {
                case PortingAction.PortoutListByDate: result = await Task.Run(() => SProc.GetPortOutByPortingDate(Model)); break;
                case PortingAction.PortoutListByMSISDN: result = await Task.Run(() => SProc.GetPortOutByMSISDN(Model)); break;
                default:
                    throw new EntryPointNotFoundException("Parameter ActionType Not Defined");
            }

            return result.Select(r => new PortListResult()
            {
                PortID = r.port_id,
                MSISDN = r.msisdn,
                PortingDate = r.port_datetime,
                SubscriberName = r.subs_name,
                ContactPhone = r.contact_phone,
                Email = r.contact_email,
                PortinStatus = r.port_status_desc,
                LastUpdate = r.last_update
            });
        }
        
        public async Task<HttpResponseMessage> Post(PortinEntry Model)
        {
            try
            {
                switch (Model.InfoType)
                {
                    case AdditionInfo.Portin:
                        return Request.CreateResponse(HttpStatusCode.OK, await GetPortinList(Model));
                    case AdditionInfo.Portout:
                        return Request.CreateResponse(HttpStatusCode.OK, await GetPortOutList(Model));
                    case AdditionInfo.PortinSubmitRequest:
                        return Request.CreateResponse(HttpStatusCode.OK, await SubmitRequest(Model));
                    case AdditionInfo.Test: break;
                    default:
                        throw new EntryPointNotFoundException("Parameter ActionType Not Defined");
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed,
                    new Models.Common.ErrCodeMsg()
                    {
                        errcode = -1,
                        errmsg = e.Message,
                        errsubject = "General Error"
                    });
            }
            return Request.CreateResponse(HttpStatusCode.OK, Model);
        }

        private async Task<Models.Common.ErrCodeMsg> SubmitRequest(PortinEntry Model)
        {
            bool validate = ValidateInput(Model);
            try
            {
                if (validate)
                {
                    var res =  await Task.Run(() => SProc.SubmitPortin(Model));
                    return new Models.Common.ErrCodeMsg(){
                        errcode = res.errcode,
                        errmsg = res.errmsg
                    };
                }
                else
                {
                    return new Models.Common.ErrCodeMsg()
                    {
                        errcode = -1,
                        errmsg = "Validation Failed"
                    };
                }
            }
            catch (Exception ex)
            {
                return new Models.Common.ErrCodeMsg()
                {
                    errcode = -1,
                    errmsg = ex.Message
                };
            }
        }

        // this is temporary for dummy data/process of submit request
        private bool ValidateInput(PortinEntry Model) {
            Model = InitiateObj(Model);
            if (Model == null) return false;
            return true;
        
        }
    }
}

