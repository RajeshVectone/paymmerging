﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CRM_API.Controllers
{
    /// <summary>
    /// PAMY Bill Generations
    /// api/PAYMBill
    /// </summary>
    public class PAYMBillController : ApiController
    {
        /// <summary>
        /// PAMY Bill Get All the Bank Details
        /// api/PAYMBill
        /// </summary>
        public HttpResponseMessage Get(string MobileNo, string ProductCode)
        {
            try
            {
                //CRM Enhancement 2
                string SiteCode = "MCM";
                if (ProductCode == "VMFR")
                    SiteCode = "MFR";
                else if (ProductCode == "VMAT")
                    SiteCode = "BAU";
                else if (ProductCode == "VMNL")
                    SiteCode = "BNL";
                else if (ProductCode == "VMBE")
                    SiteCode = "MBE";
                //31-Jul-2015 : Moorthy Added
                System.Web.HttpContext.Current.Session["GlobalProduct"] = ProductCode;
                var result = CRM_API.DB.PAYMBillGeneration.getBillGeneraion(MobileNo, SiteCode);
                return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
            }
            catch (WebException ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new List<CRM_API.Models.PAYMDetails>());
            }
        }

        /// <summary>
        /// PAMY Get Out Standing Amount
        /// api/PAYMBill
        /// </summary>      
        public HttpResponseMessage Post(CRM_API.Models.PAYMMobile objPAYMMobile)
        {
            try
            {
                //31-Jul-2015 : Moorthy Added
                System.Web.HttpContext.Current.Session["GlobalProduct"] = objPAYMMobile.ProductCode;
                var result = CRM_API.DB.PAYMBillGeneration.getOutStandingAmount(objPAYMMobile);
                return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
            }
            catch (WebException ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new List<CRM_API.Models.PAYMDetails>());
            }
        }
    }
}
