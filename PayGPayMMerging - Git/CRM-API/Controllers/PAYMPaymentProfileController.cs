﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CRM_API.Controllers
{
    /// <summary>
    /// PAYM Payment Profile
    /// api/PAYMPaymentProfile
    /// </summary>
    public class PAYMPaymentProfileController : ApiController
    {
        /// <summary>
        /// PAYMPaymentProfile
        /// GET /api/PAYMPaymentProfile
        /// </summary>
        public HttpResponseMessage Get(string MobileNo, string ProductCode)
        {
            try
            {
                //31-Jul-2015 : Moorthy Added
                System.Web.HttpContext.Current.Session["GlobalProduct"] = ProductCode;
                var result = CRM_API.DB.PAYMPaymentProfile.getAllPaymentProfile(MobileNo);
                return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
            }
            catch (WebException ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new List<CRM_API.Models.PaymPaymentHistroy>());
            }
        }

        /// <summary>
        /// PAYMPaymentProfile
        /// POST /api/PAYMPaymentProfile
        /// </summary>
        public HttpResponseMessage Post(CRM_API.Models.getDetailsByNo mygetDetailsByNo)
        {
            try
            {
                //08-Aug-2015 : Moorthy Added
                System.Web.HttpContext.Current.Session["GlobalProduct"] = mygetDetailsByNo.ProductCode;
                if (mygetDetailsByNo.Paymentmode == "1")
                {
                    var results = CRM_API.DB.PAYMPaymentProfile.getCreditCard(mygetDetailsByNo.MobileNo, mygetDetailsByNo.CardNumber);
                    return Request.CreateResponse(HttpStatusCode.OK, results.ToArray());
                }
                else
                {
                    var result = CRM_API.DB.PAYMPaymentProfile.getBankAccount(mygetDetailsByNo.MobileNo, mygetDetailsByNo.CardNumber);
                    return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
                }
            }
            catch (WebException ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new List<CRM_API.Models.PAYMBankAccount>());
            }
        }

    }
}
