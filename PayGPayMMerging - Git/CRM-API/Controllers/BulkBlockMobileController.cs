﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NLog;


using System.Data.OleDb;
using System.Data;
using System.Text;
using CRM_API.Models.BlockUnBlockModels;

namespace CRM_API.Controllers
{
    public class BulkBlockMobileController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
       
        // POST api/HappyBundle/
        [HttpPost]
        public HttpResponseMessage Post()
        {
            DataTable dtdata = new DataTable();

            HttpResponseMessage result = null;
            var httpRequest =System.Web.HttpContext.Current.Request;

            // Check if files are available
            if (httpRequest.Files.Count > 0)
            {
                var files = new List<string>();

                // interate the files and save on the server
                foreach (string file in httpRequest.Files)
                {

                    var postedFile = httpRequest.Files[file];

                    var filePath = System.Web.HttpContext.Current.Server.MapPath("~/Excel/" + postedFile.FileName);
                    var fileextension = System.IO.Path.GetExtension(filePath);

                    postedFile.SaveAs(filePath);
                    files.Add(filePath);
                    
                    //if (postedFile.FileName == "Bluk_ICCIDNumber_Bluk_BlockList.xls" || postedFile.FileName == "Bluk_ICCIDNumber_Bluk_BlockList.xlsx")
                    //{
                        if (fileextension == ".xls" || fileextension == ".xlsx")
                        {

                            //if (System.IO.File.Exists(filePath))
                            //{

                            //    System.IO.File.Delete(filePath);
                            //}
                            //Request.Files["file"].SaveAs(filePath);

                            string excelConnectionString = string.Empty;
                            excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                            //connection String for xls file format.
                            if (fileextension == ".xls")
                            {
                                excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                            }
                            //connection String for xlsx file format.
                            else if (fileextension == ".xlsx")
                            {

                                excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                            }
                            //Create Connection to Excel work book and add oledb namespace
                            System.Data.OleDb.OleDbConnection excelConnection = new System.Data.OleDb.OleDbConnection(excelConnectionString);
                            excelConnection.Open();
                            System.Data.DataTable dt = new System.Data.DataTable();
                            dt = excelConnection.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, null);
                            if (dt == null)
                            {
                                return null;
                            }

                            String[] excelSheets = new String[dt.Rows.Count];
                            int t = 0;
                            //excel data saves in temp file here.
                            foreach (DataRow row in dt.Rows)
                            {
                                excelSheets[t] = row["TABLE_NAME"].ToString();
                                t++;
                            }
                            OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);
                            string query = string.Format("Select * from [{0}]", excelSheets[0]);
                            using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                            {
                                dataAdapter.Fill(dtdata);
                            }
                            excelConnection1.Close();
                            excelConnection.Close();


                            foreach (DataColumn column in dtdata.Columns)
                            {
                                string name = column.ColumnName;
                                int countcolumn=dtdata.Columns.Count;
                                if (countcolumn <= 2)
                                {

                                    if (name == "Mobile")
                                    {
                                        /** Mobile value **/
                                        List<BlukBlock> MobileListAdd = new List<BlukBlock>();
                                        for (int i = 0; i < dtdata.Rows.Count; i++)
                                        {
                                            BlukBlock MobileList = new BlukBlock();
                                            MobileList.Mobile = dtdata.Rows[i]["Mobile"].ToString();
                                            MobileList.errcode = 0;
                                            MobileList.errmsg = "File named " + postedFile.FileName + " has been uploaded Successfully";
                                            MobileListAdd.Add(MobileList);

                                        }
                                        return Request.CreateResponse(HttpStatusCode.OK, MobileListAdd.ToArray());
                                        /** Mobile value **/

                                    }
                                    if (name == "SNO")
                                    { }
                                    else
                                    {
                                        List<BlukBlock> ICCIDListAdd = new List<BlukBlock>();
                                        BlukBlock ICCIDList = new BlukBlock();
                                        ICCIDList.errcode = -1;
                                        ICCIDList.errmsg = "Upload Excel Sheet Filed name was wrong";
                                        ICCIDListAdd.Add(ICCIDList);
                                        return Request.CreateResponse(HttpStatusCode.OK, ICCIDListAdd.ToArray());
                                    }
                                }
                                else
                                {
                                    List<BlukBlock> ICCIDListAdd = new List<BlukBlock>();
                                    BlukBlock ICCIDList = new BlukBlock();
                                    ICCIDList.errcode = -1;
                                    ICCIDList.errmsg = "Upload Excel Sheet was wrong";
                                    ICCIDListAdd.Add(ICCIDList);
                                    return Request.CreateResponse(HttpStatusCode.OK, ICCIDListAdd.ToArray());
                                }
                            }

                        }                
                   
                }

                // return result
                result = Request.CreateResponse(HttpStatusCode.BadRequest, "File Fromat was wrong");
            }
            else
            {
                // return BadRequest (no file(s) available)
                result = Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            return result;
        }
           
       
    }
}
