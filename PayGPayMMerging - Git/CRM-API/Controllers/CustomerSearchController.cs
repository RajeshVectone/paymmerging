﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CRM_API.Controllers
{
    public class CustomerSearchController : ApiController
    {
        // GET api/customersearch/MobileNo/4475190
        public HttpResponseMessage Get(string key, string value)
        {
            IEnumerable<CRM_API.Models.CustomerSearchResult> resValues =
                CRM_API.DB.Customer.SProc.Search(key, value);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.CustomerSearchResult>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        // GET api/customersearch/
        public HttpResponseMessage Get(string id)
        {
            IEnumerable<CRM_API.Models.CustomerSearchResult> resValues =
                CRM_API.DB.Customer.SProc.Search("MobileNo", id);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new CRM_API.Models.CustomerSearchResult());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.FirstOrDefault());
        }

        // POST api/customersearch

        // Search By Dony
        //public HttpResponseMessage Post([FromBody]string key, [FromBody]string value)
        //{
        //    IEnumerable<CRM_API.Models.CustomerSearchResult> resValues =
        ////        CRM_API.DB.Customer.SProc.Search(key, value);
        //    if (resValues == null)
        //        return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.CustomerSearchResult>().ToArray());
        //    else
        //        return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        //}

        public HttpResponseMessage Post(CRM_API.Models.CustomerSearchPage Search)
        {
            if (Search != null)
            {
                //31-Jul-2015 : Moorthy Added
                System.Web.HttpContext.Current.Session["GlobalProduct"] = Search.Product_Code;
                switch (Search.Search_Type)
                {
                    case Models.CustomerSearchType.SearchCustomer:
                        return CustomerSearchData(Search.Product_Code, Search.SearchText, Search.SearchBy);
                    case Models.CustomerSearchType.SearchCustomerDetail:
                        return CustomerDetailInfo(Search);
                    //Added by karthik Jira:CRMT-196
                    case Models.CustomerSearchType.TariffCustomerinfo:
                        return CustomerTariffInfo(Search);
                    case Models.CustomerSearchType.GeneralInfo:
                        return CustomerGeneralInfo(Search);
                    case Models.CustomerSearchType.LatestOpenComplaints:
                        return LatestOpenComplaints(Search);  
                    default:
                        return CustomerSearchData(Search.Product_Code, Search.SearchText, Search.SearchBy);
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.CustomerCSBHistory>().ToArray());
            }
        }

        //30-May-2019 : Moorthy : Added for Notification Popup
        private HttpResponseMessage LatestOpenComplaints(CRM_API.Models.CustomerSearchPage In)
        {
            IEnumerable<CRM_API.Models.LatestOpenComplaints> resHistoryValues =
                           CRM_API.DB.Customer.SProc.LatestOpenComplaints(In.SearchText);
            if (resHistoryValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.LatestOpenComplaints>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resHistoryValues);
        }

        public static string CustomerType(string Product_Code, string SearchText, string Sitecode)
        {
            string SubscriberID;
            CRM_API.Models.CustomerSearchResult resHistoryValues = CRM_API.DB.Customer.SProc.CustomerDetailInfo(Product_Code, SearchText, "");
            var TempRes = resHistoryValues;

            CRM_API.Models.CustomerSearchResult resAccountInfo = CRM_API.DB.Customer.SProc.CustomerPersonalInfo(Product_Code, SearchText, Sitecode);
            if (resAccountInfo.SubscriberID == null || resAccountInfo.SubscriberID == "0")
            {
                SubscriberID = CRM_API.DB.Customer.SProc.InsertCustomer(TempRes, Sitecode, SearchText).ToString();
                resHistoryValues = CRM_API.DB.Customer.SProc.CustomerDetailInfo(Product_Code, SearchText, "");
                resAccountInfo = CRM_API.DB.Customer.SProc.CustomerPersonalInfo(Product_Code, SearchText, Sitecode);
            }
            #region remap object

            resHistoryValues.BirthDate = resAccountInfo.BirthDate;
            resHistoryValues.MobileNo = resHistoryValues.mobileno;
            resHistoryValues.ICCID = resHistoryValues.iccid;
            resHistoryValues.SimType = resAccountInfo.SimType;
            resHistoryValues.product = resAccountInfo.product;
            resHistoryValues.balance = resAccountInfo.balance;

            #endregion


            return resHistoryValues.bill_type.ToString();
            //if (resHistoryValues == null)
            //    return resHistoryValues;
            //else
            //    return Request.CreateResponse(HttpStatusCode.OK, resHistoryValues);
        }

        private HttpResponseMessage CustomerDetailInfo(CRM_API.Models.CustomerSearchPage In)
        {
            string SubscriberID;
            CRM_API.Models.CustomerSearchResult resHistoryValues = CRM_API.DB.Customer.SProc.CustomerDetailInfo(In.Product_Code, In.SearchText, In.SIM_Type);
            var TempRes = resHistoryValues;

            CRM_API.Models.CustomerSearchResult resAccountInfo = CRM_API.DB.Customer.SProc.CustomerPersonalInfo(In.Product_Code, In.SearchText, In.Sitecode);
            if (resAccountInfo.SubscriberID == null || resAccountInfo.SubscriberID == "0")
            {
                SubscriberID = CRM_API.DB.Customer.SProc.InsertCustomer(TempRes, In.Sitecode, In.SearchText).ToString();
                resHistoryValues = CRM_API.DB.Customer.SProc.CustomerDetailInfo(In.Product_Code, In.SearchText, In.SIM_Type);
                resAccountInfo = CRM_API.DB.Customer.SProc.CustomerPersonalInfo(In.Product_Code, In.SearchText, In.Sitecode);
            }
            #region remap object

            resHistoryValues.first_name = resAccountInfo.FirstName;
            resHistoryValues.last_name = resAccountInfo.LastName;
            resHistoryValues.Email = resAccountInfo.Email;
            resHistoryValues.Houseno = resAccountInfo.Houseno;
            resHistoryValues.address1 = resAccountInfo.address1;
            resHistoryValues.address2 = resAccountInfo.address2;
            resHistoryValues.city = resAccountInfo.city;
            resHistoryValues.postcode = resAccountInfo.postcode;
            resHistoryValues.BirthDate = resAccountInfo.BirthDate;
            resHistoryValues.MobileNo = resHistoryValues.mobileno;
            resHistoryValues.ICCID = resHistoryValues.iccid;
            resHistoryValues.SimType = resAccountInfo.SimType;
            resHistoryValues.product = resAccountInfo.product;
            resHistoryValues.balance = resAccountInfo.balance;
            resHistoryValues.ConnectionStatus = resHistoryValues.ConnectionStatus;
            // Added
            resHistoryValues.SubscriberID = resAccountInfo.SubscriberID;
            //13-Jan-2017 : Moorthy Added for ID File Download Func
            resHistoryValues.proof_file_name = resAccountInfo.proof_file_name;
            //15-Jun-2017 : Moorthy Added for Security Answer
            resHistoryValues.securityanswer = resAccountInfo.securityanswer;
            //27-Nov-2018 : Moorthy Added for Alternate Number
            resHistoryValues.Telephone = resAccountInfo.Telephone;

            //Added : 22-Dec-2018 : CRM Enhancement
            resHistoryValues.bonus_balance = resAccountInfo.bonus_balance;
            resHistoryValues.passwd = resAccountInfo.passwd;

            //Added : 11-Jan-2019 : SIM Activation details for AT
            resHistoryValues.act_flag = resAccountInfo.act_flag;
            resHistoryValues.act_by = resAccountInfo.act_by;
            resHistoryValues.act_on = resAccountInfo.act_on;
            #endregion

            if (resHistoryValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, resHistoryValues);
            else
                return Request.CreateResponse(HttpStatusCode.OK, resHistoryValues);
        }

        private HttpResponseMessage CustomerSearchData(string Product_Code, string SearchText, string SearchBy)
        {
            IEnumerable<CRM_API.Models.CustomerSearchResult> resValues =
                CRM_API.DB.Customer.SProc.Search_v2(Product_Code, SearchBy, SearchText);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.CustomerSearchResult>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        private HttpResponseMessage CustomerGeneralInfo(CRM_API.Models.CustomerSearchPage In)
        {
            CRM_API.Models.CustomerSearchResult resHistoryValues =
                           CRM_API.DB.Customer.SProc.CustomerPersonalInfo(In.Product_Code, In.SearchText, In.Sitecode);
            if (resHistoryValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.CustomerCSBHistory>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resHistoryValues);
        }
        //Added by karthik Jira:CRMT-196
        private HttpResponseMessage CustomerTariffInfo(CRM_API.Models.CustomerSearchPage In)
        {
            CRM_API.Models.CustomerTariffdetails restariffValues = CRM_API.DB.Customer.SProc.CustomertariffInfo(In.Product_Code, In.SearchText);
            // return Request.CreateResponse(HttpStatusCode.OK, restariffValues);
            if (restariffValues == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, restariffValues);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, restariffValues);
            }

        }
    }
}
