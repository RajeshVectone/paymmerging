﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using CRM_API.Models.CountrySaver;
using CRM_API.DB.CountrySaver;
using CRM_API.Models;
using NLog;
using CRM_API.DB.Common;
using System.Web.Script.Serialization;
using CRM_API.DB.LLOTG;



namespace CRM_API.Controllers
{
    /*
     * Country Saver
    By Mamin @28-01-2014 @myamin_amzah@yahoo.com
    Requirement : get List of Country Saver
     * Param : sitecode, msisdn (mandatory)
     * 
    */
    public class CountrySaverController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public async Task<HttpResponseMessage> Get([FromUri]AdditionalReqInfo Model)
        {
            var result = new Models.Common.ErrCodeMsg() { errcode = -1, errmsg = "Default Value" };
            try
            {
                return await GetSelection(Model);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new Models.Common.ErrCodeMsg()
                {
                    errcode = -1,
                    errmsg = ex.Message
                });
            }
        }

        private async Task<HttpResponseMessage> GetSelection(AdditionalReqInfo Model)
        {
            //switch (Model.InfoType)
            //{
            //case AdditionInfo.GETSTATUS:
            return Request.CreateResponse(HttpStatusCode.OK, await GetStatus(Model));
            //default: throw new EntryPointNotFoundException("Parameter InfoType Not Recognized");
            //}
        }

        private async Task<IEnumerable<StatusList>> GetStatus(AdditionalReqInfo Model)
        {
            return await Task.Run(() =>
            {
                var result = CRM_API.DB.CountrySaver.SProc.GetListStatus(Model);
                return result;
            });
        }

        private async Task<IEnumerable<CountrySaverList_New>> GetList(AdditionalReqInfo Model)
        {
            //Model = InitiateObj(Model);
            return await Task.Run(() =>
            {
                var result = CRM_API.DB.CountrySaver.SProc.GetListCSaver_New(Model);
                return result;
            });
        }

        //private async Task<HttpResponseMessage> GetCLIList(AdditionalReqInfo Model)
        //{
        //    return Request.CreateResponse(HttpStatusCode.OK, await GetListCLI(Model));
        //}

        //private async Task<IEnumerable<CRM_API.Models.CountrySaver.AdditionalCLI>> GetListCLI(AdditionalReqInfo Model)
        //{
        //    return await Task.Run(() =>
        //    {
        //        var result = CRM_API.DB.CLI.SProc.GetListCLI(Model);
        //        return result;
        //    });
        //}


        //private async Task<HttpResponseMessage> GetCLIList(AdditionalReqInfo Model)
        //{
        //    return Request.CreateResponse(HttpStatusCode.OK, await GetListCLI(Model));
        //}

        //Old code
        private async Task<IEnumerable<CRM_API.Models.CountrySaver.CountrySaverList>> GetListCLI(AdditionalReqInfo Model)
       // private async Task<IEnumerable<AdditionalCLI>> GetListCLI(AdditionalReqInfo Model)
        {

            try
            {

                
                return await Task.Run(() =>
                {
                    /* kannan 
                     *List<CRM_API.Models.CountrySaver.AdditionalCLI> lstAdditionalCLI =  CRM_API.DB.CLI.SProc.GetListCLINew(Model).ToList();
                    return lstAdditionalCLI;
                    */

                    //02-Jan-2015 : Moorthy : Added to display the Additional CLI details
                    //List<CRM_API.Models.CountrySaver.AdditionalCLI> lstAdditionalCLI = CRM_API.DB.CLI.SProc.GetAdditionalCLI(Model).ToList();
                    //return lstAdditionalCLI;

                    

                    IEnumerable<CRM_API.Models.CountrySaver.CountrySaverList> countrySaverList = CRM_API.DB.CountrySaver.SProc.GetListCSaver(Model);
                    foreach (CRM_API.Models.CountrySaver.CountrySaverList countrySaver in countrySaverList)
                    {
                        List<CRM_API.Models.CountrySaver.AdditionalCLI> lstAdditionalCLI = CRM_API.DB.CLI.SProc.GetListCLINew(Model).ToList();
                        countrySaver.AdditionalCLIs = lstAdditionalCLI;
                       
                    }
                    return countrySaverList;
                   
                });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        ////New code
        //private async Task<IEnumerable<CRM_API.Models.CountrySaver.CountrySaverList_New>> GetListCLI_new(AdditionalReqInfo Model)
        //{
        //    try
        //    {
        //        return await Task.Run(() =>
        //        {

        //            IEnumerable<CRM_API.Models.CountrySaver.CountrySaverList_New> countrySaverList = CRM_API.DB.CountrySaver.SProc.GetListCSaver_New(Model);
        //            foreach (CRM_API.Models.CountrySaver.CountrySaverList_New countrySaver in countrySaverList)
        //            {
        //                List<CRM_API.Models.CountrySaver.AdditionalCLI> lstAdditionalCLI = CRM_API.DB.CLI.SProc.GetListCLI(countrySaver).ToList();
        //                countrySaver.AdditionalCLIs = lstAdditionalCLI;
        //            }
        //            return countrySaverList;
        //        });
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        private async Task<IEnumerable<CRM_API.Models.CountrySaver.CountrySaverPaymentHistory>> GetSubscriptionlist(AdditionalReqInfo Model)
        {
            try
            {
                return await Task.Run(() =>
                {

                    IEnumerable<CRM_API.Models.CountrySaver.CountrySaverPaymentHistory> countrySaverList = CRM_API.DB.CountrySaver.SProc.GetViewSubscriptionlist(Model);

                    return countrySaverList;
                });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        // POST api/CountrySaver/
        [HttpPost]
        public async Task<HttpResponseMessage> Post(AdditionalReqInfo Model)
        {
            try
            {
                Log.Info("  Country Saver Function start :");
                switch (Model.InfoType)
                {
                    case AdditionInfo.GETList: return Request.CreateResponse(HttpStatusCode.OK, await GetList(Model));

                    case AdditionInfo.RENEW: return Request.CreateResponse(HttpStatusCode.OK, await Renew(Model));
                    case AdditionInfo.REACTIVATE: return Request.CreateResponse(HttpStatusCode.OK, await Reactivate(Model));

                    //case AdditionInfo.CANCEL: return Request.CreateResponse(HttpStatusCode.OK, await CancelCS(Model));
                    case AdditionInfo.CANCEL: return Request.CreateResponse(HttpStatusCode.OK, await CancelCS_New(Model));
                    case AdditionInfo.ViewSubscriptionhistory: return Request.CreateResponse(HttpStatusCode.OK, await GetSubscriptionlist(Model));
                    case AdditionInfo.GETCLIList: return Request.CreateResponse(HttpStatusCode.OK, await GetListCLI(Model));
                    //  case AdditionInfo.GETCLIList: return Request.CreateResponse(HttpStatusCode.OK, await GetListCLI(MoIdel)); 

                    //Saved Card Details
                    case AdditionInfo.SavedCardDetails: return Request.CreateResponse(HttpStatusCode.OK, await GetCardList(Model));
                    case AdditionInfo.REACTIVATEBYBALANCE: return Request.CreateResponse(HttpStatusCode.OK, await ReactiveByBalance(Model));
                    case AdditionInfo.REACTIVATEBYEXISTINGCARD: return Request.CreateResponse(HttpStatusCode.OK, await ReactiveByExistingCard(Model));
                    case AdditionInfo.REACTIVATEBYNEWCARD: return Request.CreateResponse(HttpStatusCode.OK, await ReactiveByNewCard(Model));

                }
                Log.Info(" Country Saver Function end:");
            }
            catch (Exception e)
            {
                Log.Info("  Country Saver Error occurred   :" + e.Message);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed,
                    new Models.Common.ErrCodeMsg()
                    {
                        errcode = -1,
                        errmsg = e.Message,
                        errsubject = "General Error"
                    });
                Log.Info(" Country Saver Error occurred :" + e.StackTrace);
            }
            return await Task.Run(() =>
            {
                return Request.CreateResponse(HttpStatusCode.OK, Model);
            });
        }

        private async Task<Models.Common.ErrCodeMsg> Renew(AdditionalReqInfo Model)
        {
            bool validate = true;
            if (String.IsNullOrEmpty(Model.RegIDX) || Model.RegIDX == "") validate = false;
            try
            {
                if (validate)
                {
                    var res = await Task.Run(() => CRM_API.DB.CountrySaver.SProc.SubmitRenew(Model));
                    return new Models.Common.ErrCodeMsg()
                    {
                        errcode = res.errcode,
                        errmsg = res.errmsg
                    };
                }
                else
                {
                    return new Models.Common.ErrCodeMsg()
                    {
                        errcode = -1,
                        errmsg = "Validation Failed, RegIDX is null"
                    };
                }
            }
            catch (Exception ex)
            {
                return new Models.Common.ErrCodeMsg()
                {
                    errcode = -1,
                    errmsg = ex.Message
                };
            }
        }

        private async Task<Models.Common.ErrCodeMsg> Reactivate(AdditionalReqInfo Model)
        {
            bool validate = true;
            if (String.IsNullOrEmpty(Model.RegIDX) || Model.RegIDX == "") validate = false;
            try
            {
                if (validate)
                {
                    var res = await Task.Run(() => CRM_API.DB.CountrySaver.SProc.SubmitReactivate(Model));
                    return new Models.Common.ErrCodeMsg()
                    {
                        errcode = res.errcode,
                        errmsg = res.errmsg
                    };
                }
                else
                {
                    return new Models.Common.ErrCodeMsg()
                    {
                        errcode = -1,
                        errmsg = "Validation Failed, RegIDX is null"
                    };
                }
            }
            catch (Exception ex)
            {
                return new Models.Common.ErrCodeMsg()
                {
                    errcode = -1,
                    errmsg = ex.Message
                };
            }
        }

        private async Task<Models.Common.ErrCodeMsg> CancelCS(AdditionalReqInfo Model)
        {
            bool validate = true;
            if (String.IsNullOrEmpty(Model.Mobileno) || Model.RegIDX == "") validate = false;
            try
            {
                if (validate)
                {
                    var res = await Task.Run(() => CRM_API.DB.CountrySaver.SProc.SubmitCancel(Model));
                    return new Models.Common.ErrCodeMsg()
                    {
                        errcode = res.errcode,
                        errmsg = res.errmsg
                    };
                }
                else
                {
                    return new Models.Common.ErrCodeMsg()
                    {
                        errcode = -1,
                        errmsg = "Validation Failed, RegIDX is null"
                    };
                }
            }
            catch (Exception ex)
            {
                return new Models.Common.ErrCodeMsg()
                {
                    errcode = -1,
                    errmsg = ex.Message
                };
            }
        }

        private async Task<IEnumerable<Models.Common.ErrCodeMsg>> CancelCS_New(AdditionalReqInfo Model)
        {
            //Model = InitiateObj(Model);
            return await Task.Run(() =>
            {
                var result = CRM_API.DB.CountrySaver.SProc.ChangeCancelmode(Model);
                return result;
            });
        }
        private async Task<IEnumerable<Models.Common.ErrCodeMsg>> ReactiveByBalance(AdditionalReqInfo Model)
        {
            //Model = InitiateObj(Model);
            return await Task.Run(() =>
            {
                var result = CRM_API.DB.CountrySaver.SProc.Bybalnce(Model);
                return result;
            });
        }
        //Saved Card Details 
        /// <summary>
        /// LLOTG Get saved Card List
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        //private async Task<HttpResponseMessage> GetCardList(AdditionalReqInfo Model)
        //{
        //    return await Task.Run(() =>
        //    {
        //        var result = CRM_API.DB.LLOTG.SProc.GetCardList(Model.Mobileno, Model.Sitecode).Result;
        //        return Request.CreateResponse(HttpStatusCode.OK, result);
        //    });
        //}

        private async Task<HttpResponseMessage> GetCardList(AdditionalReqInfo Model)
        {
            //Model = InitiateObj(Model);
            return await Task.Run(() =>
            {
                var result = CRM_API.DB.CountrySaver.SProc.GetCardList(Model.Mobileno, Model.Sitecode).Result;
                return Request.CreateResponse(HttpStatusCode.OK, result);
            });
        }
        private async Task<errmessage> ReactiveByExistingCard(AdditionalReqInfo x)
        {

            try
            {
                Log.Info("  Payment by exsting card  start  :");

                string[] inputValues = x.LoggedUser.Split(',');

                string CLI = x.Mobileno;
                string Sitecode = x.Sitecode;
                string Brand = inputValues[0];
                string CardNumber = inputValues[3].Replace(" ", "");
                string CardCVV = inputValues[2];
                string amount = inputValues[1];
                // amount = "1";



                string IpAdd = System.Web.HttpContext.Current.Request.UserHostAddress;
                string mailtemplate = "";

                //CTPHomeSaverResultViewModel _ctpHomeSaverResultViewModel = GetPaymentByCard(CardNumber, CardCVV, CLI, Sitecode, Brand, amount, IpAdd, Reg_Number, LLOTG_country, LLOTG_paymode, mailtemplate, svc_id, mode);
                errmessage _ctpHomeSaverResultViewModel = GetPaymentByCard(CardNumber, CardCVV, CLI, Sitecode, Brand, amount, IpAdd, mailtemplate);

                var resy = new errmessage { errcode = _ctpHomeSaverResultViewModel.errcode, errmsg = _ctpHomeSaverResultViewModel.errmsg, errsubject = _ctpHomeSaverResultViewModel.errsubject };
                // return resy;
                return await Task.Run(() =>
                {
                    // return Request.CreateResponse(HttpStatusCode.OK, resy);
                    // return Request.CreateResponse(HttpStatusCode.OK, _ctpHomeSaverResultViewModel);
                    return resy;
                });
                //Log.Info("  Payment by exsting card  End  :");
            }
            catch (Exception e)
            {
                Log.Info("  Payment by exsting error :" + e.StackTrace);
                Log.Info("  Payment by exsting error :" + e.Message);
                Log.Error("Error Data" + e.Message);
            }

            //return Request.CreateResponse(HttpStatusCode.OK, "error data");
            return new errmessage { errmsg = "Error Data" };
        }


        //private CTPHomeSaverResultViewModel GetPaymentByCard(string CardNumber, string CardCVV, string CLI, string Sitecode, string Brand, string amount, string IpAdd, string Reg_Number, string LLOTG_country, string LLOTG_paymode, string mailtemplate, string svc_id, int mode)
        private errmessage GetPaymentByCard(string CardNumber, string CardCVV, string CLI, string Sitecode, string Brand, string amount, string IpAdd, string mailtemplate)
        {


            CTPHomeSaverPostModel _ctpHomeSaverPostModel = new CTPHomeSaverPostModel();
            errmessage _ctpHomeSaverResultViewModel;
            //try
            //{
            Log.Info("  Payment by exsting card  start2  :");
            Log.Info(new JavaScriptSerializer().Serialize(_ctpHomeSaverPostModel));
            CRM_API.DB.LLOTG.Payment pay = new CRM_API.DB.LLOTG.Payment();
            _ctpHomeSaverPostModel.CLI = CLI;
            _ctpHomeSaverPostModel.CardCVV = CardCVV;
            _ctpHomeSaverPostModel.CardNumber = CardNumber;
            _ctpHomeSaverPostModel.ApplicationCode = Brand;
            _ctpHomeSaverPostModel.SiteCode = Sitecode;

            _ctpHomeSaverPostModel.CostTotal = Convert.ToDouble(amount);

            _ctpHomeSaverPostModel.IpAdd = System.Web.HttpContext.Current.Request.UserHostAddress;


            var cardDetails = pay.GetCardList(CLI, Sitecode);


            _ctpHomeSaverPostModel.Currency = cardDetails.Where(c => c.CardNumber == CardNumber).Select(m => m.Currency).SingleOrDefault().ToString();
            _ctpHomeSaverPostModel.Email = "s.anandhakumar@vectone.com";

            //CTPHomeSaverResultViewModel _ctpHomeSaverResultViewModel = ReactivateBundleByExistingCard(_ctpHomeSaverPostModel, Reg_Number, LLOTG_country, LLOTG_paymode, amount, mailtemplate, svc_id, mode);
            _ctpHomeSaverResultViewModel = ReactivateBundleByExistingCard(_ctpHomeSaverPostModel, amount, mailtemplate);

            return _ctpHomeSaverResultViewModel;

            //Log.Info("  Payment by exsting card  End2  :");
            //Log.Info(new JavaScriptSerializer().Serialize(_ctpHomeSaverResultViewModel));
            //}
            //catch (Exception e)
            //{
            //    Log.Error("Payment by exsting card  Start2 :" + e.Message);
            //}


        }


        //private CTPHomeSaverResultViewModel ReactivateBundleByExistingCard(CTPHomeSaverPostModel inputData, string Reg_Number, string LLOTG_country, string LLOTG_paymode, string amount, string mailtemplate, string svc_id, int mode)
        private errmessage ReactivateBundleByExistingCard(CTPHomeSaverPostModel inputData, string amount, string mailtemplate)
        {
            Log.Info("  Payment by exsting card  Start3  :");
            Log.Info(new JavaScriptSerializer().Serialize(inputData));
            inputData.errcode = -1;
            inputData.errmsg = "General Exception";

            CRM_API.DB.LLOTG.Payment csPayment = new CRM_API.DB.LLOTG.Payment();
            var result = new errmessage();


            //Check empty or null value for payment
            if (string.IsNullOrWhiteSpace(inputData.CardCVV)
                || string.IsNullOrWhiteSpace(inputData.CardNumber)
                || string.IsNullOrWhiteSpace(inputData.SiteCode) || string.IsNullOrWhiteSpace(inputData.ApplicationCode)
                || string.IsNullOrWhiteSpace(inputData.Email) || string.IsNullOrWhiteSpace(inputData.Currency))
            {

                result.errcode = -1;
                result.errmsg = "Can't process payment. Some parameter has empty or null value.";
                Log.Info("Pay After 3Ds : Some Parameter has empty or null value");
                return result;
            }

            //payment
            csPayment.CountrySaverSubcriptionCharge(inputData.SiteCode, inputData.ApplicationCode, inputData.CLI, inputData.CardNumber, inputData.Currency, inputData.CostTotal, inputData.CardCVV);

            //If payment success

            if (csPayment.ReasonCode == "0")
            {

                if (inputData.CLI != null)
                {
                    string paymentreferance = csPayment.MerchantReferenceCode;
                    int payment_status = 1;
                    int use_for_renewal = 1;
                    //var msg = CRM_API.DB.LLOTG.SProc.Reactivate(inputData.CLI, inputData.SiteCode, Reg_Number + "," + svc_id + "," + LLOTG_paymode, mode);

                    //CRM_API.Models.Common.ErrCodeMsg msg = CRM_API.DB.CountrySaver.SProc.Reactivate(inputData.CLI, inputData.SiteCode, paymentreferance, payment_status, use_for_renewal);

                    CRM_API.Models.errmessage msg = CRM_API.DB.CountrySaver.SProc.Reactivate(inputData.CLI, inputData.SiteCode, paymentreferance, payment_status,
use_for_renewal);
                    result.errcode = msg.errcode;
                    result.errmsg = msg.errmsg;
                    result.errsubject = msg.errsubject;
                    //  result.errcode = msg;
                }
                else
                {
                    result.errcode = -1;
                    result.errmsg = "User IDX Not Found";
                }
            }
            else
            {
                result.errcode = -1;
                result.errmsg = csPayment.Decision;
            }

            if (result.errcode == 1)
            {
            }
            Log.Info("  Payment by exsting card  End3  :");
            Log.Info(new JavaScriptSerializer().Serialize(result));

            return result;
        }

        //New Card Reactivte Details
        private async Task<CTPHomeSaverResultViewModel> ReactiveByNewCard(AdditionalReqInfo x)
        {
            string MobileNo = x.Mobileno;
            string Sitecode = x.Sitecode;
            int mode = x.mode;
            string[] cancelValues = x.LoggedUser.Split('^');
            string PaymentMethodCardType = cancelValues[0];
            string PaymentMethodCCNumber = cancelValues[1];
            string PaymentMethodFirstName = cancelValues[2];
            string PaymentMethodLastName = cancelValues[3];
            string PaymentMethodExpiryMonth = cancelValues[4];
            string PaymentMethodExpiryYear = cancelValues[5];
            string PaymentMethodCardVerfCode = cancelValues[6];
            string PaymentMethodHouseNo = cancelValues[7];
            string PaymentMethodAddress = cancelValues[8];
            string PaymentMethodCity = cancelValues[9];
            string PaymentMethodCountry = cancelValues[10];
            string PaymentMethodPostcode = cancelValues[11];
            string PaymentMethodEmail = cancelValues[12];
            string PaymentMethodPhone = cancelValues[13];
            string PaymentMethodIssueNumber = cancelValues[14];
            string amount = cancelValues[15];
            string Brand = cancelValues[16];
            //string regno = cancelValues[17];
            //string paymode = cancelValues[18];
            //string svcid = cancelValues[19];
            string currency = cancelValues[17];
            string destinationNumber = cancelValues[18];
            string mailtemplate = "";

            //CTPHomeSaverResultViewModel returnValue = GetSaveProductandCCDetail(PaymentMethodCardType, PaymentMethodCCNumber, PaymentMethodExpiryMonth, PaymentMethodExpiryYear, PaymentMethodIssueNumber, PaymentMethodCardVerfCode, PaymentMethodFirstName, PaymentMethodLastName, PaymentMethodPostcode, PaymentMethodHouseNo, PaymentMethodAddress, PaymentMethodCity, PaymentMethodCountry, PaymentMethodEmail, PaymentMethodPhone, MobileNo, Brand, amount, mailtemplate, regno, Sitecode, paymode, svcid, currency, mode);

            CTPHomeSaverResultViewModel returnValue = GetSaveProductandCCDetail(PaymentMethodCardType, PaymentMethodCCNumber, PaymentMethodExpiryMonth, PaymentMethodExpiryYear, PaymentMethodIssueNumber, PaymentMethodCardVerfCode, PaymentMethodFirstName, PaymentMethodLastName, PaymentMethodPostcode, PaymentMethodHouseNo, PaymentMethodAddress, PaymentMethodCity, PaymentMethodCountry, PaymentMethodEmail, PaymentMethodPhone, MobileNo, Brand, amount, mailtemplate, Sitecode, currency, destinationNumber);

            //var resy = new CRM_API.DB.Common.ErrCodeMsg { errcode = returnValue.errcode, errmsg = returnValue.errmsg, };
            var resy = new CRM_API.Models.CTPHomeSaverResultViewModel { errcode = returnValue.errcode, errmsg = returnValue.errmsg, };
            //var resy = new Mvc.Code.Common.ErrCodeMsgExt { errcode = 0, errmsg = "Success", Text = "test" };

            return await Task.Run(() =>
            {
                // return Request.CreateResponse(HttpStatusCode.OK, returnValue);
                return resy;
            });

        }

        //private CTPHomeSaverResultViewModel GetSaveProductandCCDetail(string cardName, string cardNumber, string cardExpireMonth, string cardExpireYear, string IssueNo, string cardCVV, string cardFirstName, string cardLastName, string PaymentMethodPostcode, string PaymentMethodHouseNo, string cardBillAddress, string cardCity, string cardCountry, string Email, string Phone, string CLI, string Brand, string amount, string mailtemplate, string regno, string Sitecode, string paymode, string svcid, string currency, int mode)

        private CTPHomeSaverResultViewModel GetSaveProductandCCDetail(string cardName, string cardNumber, string cardExpireMonth, string cardExpireYear, string IssueNo, string cardCVV, string cardFirstName, string cardLastName, string PaymentMethodPostcode, string PaymentMethodHouseNo, string cardBillAddress, string cardCity, string cardCountry, string Email, string Phone, string CLI, string Brand, string amount, string mailtemplate, string Sitecode, string currency, string destinationNumber)
        {
            CRM_API.DB.LLOTG.Payment csPayment = new CRM_API.DB.LLOTG.Payment();

            CTPHomeSaverPostModel cardModel = new CTPHomeSaverPostModel();
            //Payment csPayment = new Payment();
            cardModel.CardNumber = cardNumber;
            cardModel.CardType = cardName;
            cardModel.CardExpireMonth = cardExpireMonth;
            cardModel.CardExpireYear = cardExpireYear;
            cardModel.CardFirstName = cardFirstName;
            cardModel.CardLastName = cardLastName;
            cardModel.CardCity = cardCity;
            cardModel.CardCountry = cardCountry;
            cardModel.CardBillAddress = cardBillAddress;
            cardModel.CardCVV = cardCVV;
            cardModel.Email = Email;
            cardModel.CardPostCode = PaymentMethodPostcode;
            cardModel.CardNumber = cardNumber;
            cardModel.CardExpireMonth = cardExpireMonth;
            cardModel.CardExpireYear = cardExpireYear;
            cardModel.CardFirstName = cardFirstName;
            cardModel.CardLastName = cardLastName;
            cardModel.CardBillAddress = cardBillAddress;
            cardModel.CardCity = cardCity;
            cardModel.CardCountry = cardCountry;
            cardModel.CardCVV = cardCVV;
            cardModel.CardPostCode = PaymentMethodPostcode;
            cardModel.CardPhone = Phone;

            cardModel.DestinationNumber = destinationNumber;

            //cardModel.IpAdd = System.Web.HttpContext.Current.Request.UserHostAddress;
            string hostName = Dns.GetHostName(); // Retrive the Name of HOST
            //cardModel.IpAdd = Dns.GetHostByName(hostName).AddressList[0].ToString();
            cardModel.IpAdd = "192.168.1.230";


            //cardModel.SubscriberId = SubscriberId;


            //string FandFNoTmp = FandFNo;
            //if (FandFNo.Substring(0, 2) == "00")
            //    FandFNoTmp = FandFNo.Substring(2);
            //int regId = csPayment.GetIdxUser(AccountNo, FandFNoTmp, Brand);
            //ProductModel product = csPayment.GetSitecodeByMobile(Brand);
            //String brandType = product.mundio_product;
            //BundleModel GetBundleDescription = csPayment.GetBundleDescription(regno.ToString(), Brand);

            cardModel.SiteCode = Sitecode;
            cardModel.ApplicationCode = Brand;
            //cardModel.AccountId = AccountNo;
            cardModel.CLI = CLI;


            cardModel.Currency = currency;

            if (!(String.IsNullOrEmpty(amount)))
            {
                cardModel.CostTotal = Double.Parse(amount);
            }
            else
            {
                cardModel.CostTotal = 0;
            }
            cardModel.Bypass3Ds = true;
            cardModel.state = "";
            //cardModel.c
            /*
            inputData.CardCVV = CVVNo;
            inputData.SiteCode = "";
            inputData.ApplicationCode = "";
            inputData.AccountId = "";
            inputData.CardNumber = creditCardNo;
            inputData.Currency = "";
            inputData.CostTotal =Double.Parse( currentBalance);
             */
            var returnValue = ReactivateByNewCard(cardModel, mailtemplate, amount);
            return returnValue;

        }

        private CTPHomeSaverResultViewModel ReactivateByNewCard(CTPHomeSaverPostModel inputData, string mailtemplate, string amount)
        {
            CRM_API.DB.LLOTG.Payment csPayment = new CRM_API.DB.LLOTG.Payment();
            CTPHomeSaverResultViewModel result = new CTPHomeSaverResultViewModel();

            Log.Info("Start ReactivateByNewCard:");
            Log.Info(new JavaScriptSerializer().Serialize(inputData)); Log.Info("Start PayWithCard:");

            //CTPHomeSaverResultViewModel result = new CTPHomeSaverResultViewModel();
            inputData.SourceReg = "LLOM_CRM";
            string stackTrace = new JavaScriptSerializer().Serialize(inputData);
            //Payment csPayment = new Payment();

            FamilyNumber familyNumber = new FamilyNumber()
            {
                Number = inputData.DestinationNumber,
                Name = string.Format("{0} {1}", inputData.DestinationFirstName, inputData.DestinationLastName),
                LastName = inputData.DestinationLastName,
                FirstName = inputData.DestinationFirstName,
                Country = inputData.CardCountry,
                PostCode = inputData.DestinationPostCode,
                Street = inputData.DestinationStreet,
                Town = inputData.DestinationTown
            };
            result.Country = inputData.CardCountry;
            result.DestinationNumber = inputData.DestinationNumber;
            result.MainNumber = inputData.MainNumber;
            inputData.CostTotal = inputData.CostTotal;

            result.PayReference = csPayment.MerchantReferenceCode;

            //As of now, am sending the state as countrysaver or LLOM to find the productcode in Authorize method (19-01-2016);
            inputData.state = "countrysaver";

            csPayment.Authorize(
    new Models.CardDetails()
    {
        CardAddress = inputData.CardBillAddress,
        CardCity = inputData.CardCity,
        CardCountry = inputData.CardCountry,
        CardCVV = inputData.CardCVV,
        CardExpireMonth = inputData.CardExpireMonth,
        CardExpireYear = inputData.CardExpireYear,
        CardFirstName = inputData.CardFirstName,
        CardLastName = inputData.CardLastName,
        CardNumber = inputData.CardNumber,
        CardType = inputData.CardType,
        CardPhone = inputData.CardPhone,
        CardPostCode = inputData.CardPostCode
    }, inputData.SiteCode, inputData.ApplicationCode, inputData.CLI, inputData.Email, inputData.Currency, inputData.CostTotal, inputData.Bypass3Ds, inputData.state, inputData.IpAdd);

            //reason code 475 = go to 3D secure page
            if (csPayment.ReasonCode == "475")
            {
                Log.Debug("Entering 3Ds");

                result.errcode = 0;
                result.errmsg = "REDIRECT TO 3DS";
                result.AcsURL = csPayment.AcsURL;
                result.MerchantRefCode = csPayment.MerchantReferenceCode;
                result.PaReq = csPayment.PaReq;
                result.ReasonCode = csPayment.ReasonCode;
                result.SubscriptionId = csPayment.SubscriptionId;
                result.Decision = csPayment.Decision;
                result.Description = csPayment.Description;
                return result;
            }

            if (csPayment.Decision == "ACCEPT")
            {
                //If payment success
                if (csPayment.ReasonCode == "100")
                {
                    //string FandFNoTmp = FandFNo;
                    //if (FandFNo.Substring(0, 2) == "00")
                    //    FandFNoTmp = FandFNo.Substring(2);
                    //int UserIDX = csPayment.GetIdxUser(inputData.AccountId, FandFNoTmp, inputData.ApplicationCode);

                    if (inputData.CLI != null)
                    {


                        string paymentreferance = csPayment.MerchantReferenceCode;
                        int payment_status = 1;
                        int use_for_renewal = 1;
                        //var msg = CRM_API.DB.LLOTG.SProc.Reactivate(inputData.CLI, inputData.SiteCode, Reg_Number + "," + svc_id + "," + LLOTG_paymode, mode);

                        //CRM_API.Models.Common.ErrCodeMsg msg = CRM_API.DB.CountrySaver.SProc.Reactivate(inputData.CLI, inputData.SiteCode, paymentreferance, payment_status, use_for_renewal);

                        CRM_API.Models.errmessage msg = CRM_API.DB.CountrySaver.SProc.Reactivate(inputData.CLI, inputData.SiteCode, paymentreferance, payment_status, use_for_renewal);
                        result.errcode = msg.errcode;
                        result.errmsg = msg.errmsg;

                        if (result.errcode == 0)
                        {
                            CRM_API.DB.CountrySaver.SProc.ChangeToPendingStatus(inputData.CLI, inputData.DestinationNumber, paymentreferance);
                        }

                        //var msg = CRM_API.DB.CountrySaver.SProc.Reactivate(inputData.CLI, inputData.SiteCode, regno + "," + svcid + "," + paymode, mode);
                        //result.errcode = msg;

                    }
                    else
                    {
                        result.errcode = -1;
                        result.errmsg = "User IDX Not Found";
                    }

                }
                else
                {
                    result.errcode = -1;
                }

                if (result.errcode == 0)
                {


                }
                Log.Info("End ReactivateByNewCard :");

                ErrMsg resMsg = new ErrMsg();
            }

            else
            {
                result.MerchantRefCode = csPayment.MerchantReferenceCode;
                result.errcode = -1;
                result.errmsg = csPayment.Decision;
            }
            Log.Info("End PayWithCard");
            return result;

        }

    }
}

