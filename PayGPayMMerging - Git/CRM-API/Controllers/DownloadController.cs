﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace CRM_API.Controllers
{
    public class Download1Controller : Controller
    {
        //
        // GET: /Download/

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult GoDownloadPayment()
        {
            StringBuilder str = new StringBuilder();
            str.Append("<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>SMS Routing</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv=\"content-type\" content=\"text/plain; charset=UTF-8\"/></head><body>");
            str.Append("<head><style>.xlLongDate{mso-number-format:'dd-mm-yyyy hh:mm:ss AM/PM';}.xlText{mso-number-format:'@';}</style></head>");
            str.Append("<table>");
            str.Append("<thead>");
            str.Append("<tr>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>ReferenceId</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Sitecode</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Payment Agent</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Service Type</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Productcode</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Paymentmode</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Accountid</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;f ;ont-weight:bold'>CCNo</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;f ;ont-weight:bold'>Currency</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;f ;ont-weight:bold'>Createddate</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;f ;ont-weight:bold'>Subscriptionid</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Status</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;f ;ont-weight:bold'>Code</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;f ;ont-weight:bold'>Reason for Reject</th>");       
            str.Append("</tr>");
            str.Append("</thead>");
            str.Append("<tbody>");

            //var result = CRM_API.DB.viewtransaction.getpaymentdetails();
       
            //List<CRM_API.Models.financeviewtrans> result = CRM_API.DB.viewtransaction.gentprofilegetlistdownload();
            //foreach (SMSRouting_API.Models.SMSPriceprofileget val in (result as IEnumerable<SMSRouting_API.Models.SMSPriceprofileget>))
            //{
            //    str.Append("<tr>");

            //    str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.REFERENCE_ID as string) ? "" : val.REFERENCE_ID) + "</td>");
            //    str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.SITECODE as string) ? "" : (val.SITECODE.ToString())) + "</td>");
            //    str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.PAYMENT_AGENT as string) ? "" : (val.PAYMENT_AGENT.ToString())) + "</td>");
            //    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.SERVICE_TYPE as string) ? "" : (val.SERVICE_TYPE.ToString())) + "</td>");
            //    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.PRODUCT_CODE as string) ? "" : (val.PRODUCT_CODE.ToString())) + "</td>");
            //    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.PAYMENT_MODE as string) ? "" : (val.PAYMENT_MODE.ToString())) + "</td>");
            //    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.ACCOUNT_ID as string) ? "" : (val.ACCOUNT_ID.ToString())) + "</td>");
            //    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.CC_NO as string) ? "" : (val.CC_NO.ToString())) + "</td>");
            //    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.TOTALAMOUNT as string) ? "" : (val.TOTALAMOUNT.ToString())) + "</td>");
            //    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.CURRENCY as string) ? "" : (val.CURRENCY.ToString())) + "</td>");
            //    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.CREATED_DATE as string) ? "" : (val.CREATED_DATE.ToString())) + "</td>");
            //    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.SUBSCRIPTIONID as string) ? "" : (val.SUBSCRIPTIONID.ToString())) + "</td>");
            //    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.CURRENT_STATUS as string) ? "" : (val.CURRENT_STATUS.ToString())) + "</td>");
            //    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.CS_ERROR_CODE as string) ? "" : (val.CS_ERROR_CODE.ToString())) + "</td>");
            //    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.ReasonforReject as string) ? "" : (val.ReasonforReject.ToString())) + "</td>");
            //    str.Append("</tr>");
            //}
            str.Append("</tbody>");
            str.Append("</table>");
            str.Append("</body>");
            str.Append("</html>");

            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=SMS_Profile_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(str.ToString());
            string _random = Guid.NewGuid().ToString();

            TempData[_random] = str.ToString();
            var resy = "";// new SMSRouting_API.Models.Common.ErrCodeMsgExt { errcode = 0, errmsg = "Download Ready", Text = _random };
            return Json(resy, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public FileContentResult GoDownloadPayments(string id)
        {
            var result = (string)TempData[id];
            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=SMS_Routing_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(result.ToString());
            return new FileContentResult(temp, "application/vnd.ms-excel");
        }

    }
}
