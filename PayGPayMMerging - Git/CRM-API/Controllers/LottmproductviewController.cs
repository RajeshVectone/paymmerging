﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml;

namespace CRM_API.Controllers
{
    public class LottmproductviewController : ApiController
    {
        //
        // GET: /Lottmproductview/

        public HttpResponseMessage get()
        {
            try
            {
                var result = CRM_API.DB.viewtransaction.Lotgproductview();
                return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
            }
            catch (WebException ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new List<CRM_API.Models.financeviewtrans>());
            }
        }

    }
}
