﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Web.Http;
using NLog;
using CRM_API.Models.Docs;

namespace CRM_API.Controllers
{
    public class DocsController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public HttpResponseMessage Post(DocModel Model)
        {
            if (Model != null)
            {
                switch (Model.modetype)
                {
                    case ModeType.GETUPLOADEDDOCINFO:
                        return GetUploadedDocInfo(Model.category_id, Model.is_archive_flag, Model.product_name);
                    case ModeType.GETDOCCATEGORYINFO:
                        return GetDocCategoryInfo();
                    case ModeType.CREATEDOCCATEGORY:
                        return CreateDocCategory(Model.category_id, Model.category_name, Model.process_type);
                    case ModeType.INSERTDOCUPLOADINFO:
                        return InsertDocUploadInfo(Model.category_id, Model.filename, Model.filepath, Model.upload_by, Model.doc_id, Model.processtype, Model.product_name);
                    case ModeType.UPDATEDOCUPLOADINFO:
                        return UpdateDocUploadInfo(Model.doc_id, Model.processtype);
                    default:
                        return Request.CreateResponse(HttpStatusCode.NotFound, new List<DocModelCommon>().ToArray());
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<DocModelCommon>().ToArray());
            }
        }

        private HttpResponseMessage UpdateDocUploadInfo(int doc_id, int processtype)
        {
            IEnumerable<DocModelCommon> resValues = CRM_API.DB.Docs.UpdateDocUploadInfo(doc_id, processtype);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<DocModelCommon>().ToArray());
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
            }
        }

        private HttpResponseMessage InsertDocUploadInfo(int category_id, string filename, string filepath, string upload_by, int doc_id, int processtype, string product_name)
        {
            IEnumerable<DocModelCommon> resValues = CRM_API.DB.Docs.InsertDocUploadInfo(category_id, filename, filepath, upload_by, doc_id, processtype, product_name);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<DocModelCommon>().ToArray());
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
            }
        }

        #region CreateDocCategory
        private HttpResponseMessage CreateDocCategory(int category_id, string category_name, int process_type)
        {
            IEnumerable<DocModelCommon> resValues = CRM_API.DB.Docs.CreateDocCategory(category_id, category_name, process_type);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<DocModelCommon>().ToArray());
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
            }
        }
        #endregion

        #region GetDocCategoryInfo
        private HttpResponseMessage GetDocCategoryInfo()
        {
            IEnumerable<DocCategory> resValues = CRM_API.DB.Docs.GetDocCategoryInfo();
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<DocCategory>().ToArray());
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
            }
        }
        #endregion

        #region GetUploadedDocInfo
        private HttpResponseMessage GetUploadedDocInfo(int category_id, int is_archive_flag, string product_name)
        {
            IEnumerable<UploadedDocInfo> resValues = CRM_API.DB.Docs.GetUploadedDocInfo(category_id, is_archive_flag, product_name);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<UploadedDocInfo>().ToArray());
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
            }
        }
        #endregion

    }

}
