﻿using CRM_API.DB.Common;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace CRM_API.Controllers
{
    public class LOTGController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        //private static readonly string logStrTmp = "Control: {0}, API: {1}, Message {2}";

        public async Task<dynamic> Get([FromUri]string Id)
        {
            string logStrTmp = string.Format("Start: {0} | Param: {1}",this.Request.RequestUri.ToString(),Id);            
            Models.Common.ErrCodeMsg result = new Models.Common.ErrCodeMsg();
            try
            {

            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new Models.Common.ErrCodeMsg()
                {
                    errcode = -1,
                    errmsg = e.Message
                });
            }
            return await Task.Run(() =>{
                return Request.CreateResponse(HttpStatusCode.OK);
            });
        }
        public async Task<dynamic> Post(Models.LOTG.Request Req)
        {
            string msgStr = string.Format("{0} | Message : {1}", Request.RequestUri.ToString(), JsonConvert.SerializeObject(Req));
            Log.Info(msgStr);
            try
            {
                switch (Req.Action)
                {
                    case Models.LOTG.LOTG_Action.List: return await GetList(Req);
                    case Models.LOTG.LOTG_Action.ViewDetail: return await GetDetail(Req);
                    case Models.LOTG.LOTG_Action.ChangePayment: return await DoChangePayment(Req);
                    case Models.LOTG.LOTG_Action.ManualRenew: return await DoRenewContract(Req); 
                    case Models.LOTG.LOTG_Action.StopContract: return await DoStopContract(Req); 
                    /*Added on 17 Jan 2015 - by Hari*/
                    case Models.LOTG.LOTG_Action.BundleDetail: return await GetBundleDetail(Req); 
                    /*End added*/
                    /*Added on 17 Jan 2015 - by Hari*/
                    case Models.LOTG.LOTG_Action.LLOMDetail: return await GetBundleLLOMDetail(Req);
                    /*End added*/
                    /*Added on 19 Jan 2015 - by Hari*/
                    case Models.LOTG.LOTG_Action.BundleSubscriptionHistory: return await GetBundleSubscriptionHistory(Req);
                    //case Models.LOTG.LOTG_Action.LLOMSubscriptionHistory: return await GetLLOMSubscriptionHistory(Req);
                    case Models.LOTG.LOTG_Action.LLOMSubscriptionHistory: return await GetLLOMSubscriptionHistory_v1(Req);
                    case Models.LOTG.LOTG_Action.CountrySaverSubscriptionHistory: return await GetCountrySaverSubscriptionHistory(Req);
                    case Models.LOTG.LOTG_Action.BundleConfirmCancel: return await DoBundleConfirmCancel(Req);
                    case Models.LOTG.LOTG_Action.LLOMConfirmCancel: return await DoLLOMConfirmCancel(Req);
                    case Models.LOTG.LOTG_Action.CountrySaverConfirmCancel: return await DoCountrySaverConfirmCancel(Req);
                    /*End added*/                   

                }
            }
            catch (Exception e)
            {
                string errStr = string.Format ("{0} | Error : {1}",Request.RequestUri.ToString(),e.Message);
                Log.Error(errStr);
                return Request.CreateResponse(HttpStatusCode.OK, new Models.Common.ErrCodeMsg() { 
                    errcode = -1 ,
                    errmsg = errStr
                });
            }
            return await Task.Run(() =>
            {
                return Request.CreateResponse(HttpStatusCode.OK, Req);
            });
        }
        private async Task<HttpResponseMessage> GetList(Models.LOTG.Request x)
        {
            //IList<Models.LOTG.Collection> result = new List<Models.LOTG.Collection>();
            return await Task.Run(() =>
            {
                var result = CRM_API.DB.LOTG.SProc.GetCollectionAsync(x.MobileNo, x.Sitecode).Result;
                return Request.CreateResponse(HttpStatusCode.OK, result);
            });
        }
        private async Task<HttpResponseMessage> GetDetail(Models.LOTG.Request x)
        {
            //IList<Models.LOTG.Collection> result = new List<Models.LOTG.Collection>();
            return await Task.Run(() =>
            {
                var result = CRM_API.DB.LOTG.SProc.GetDetailAsync(x.Reg_Number, x.Sitecode).Result;
                return Request.CreateResponse(HttpStatusCode.OK, result);
            });
        }
        
        /*Added by Hari - 17 Jan 2015 - for getting bundle details*/
        private async Task<HttpResponseMessage> GetBundleLLOMDetail(Models.LOTG.Request x)
        {
            //IList<Models.LOTG.Collection> result = new List<Models.LOTG.Collection>();
            return await Task.Run(() =>
            {
                var result = CRM_API.DB.LOTG.SProc.GetCollectionLLOMAsync(x.MobileNo, x.Sitecode).Result;
                return Request.CreateResponse(HttpStatusCode.OK, result);
            });
        }
        /*End added*/

        /*Added by Hari - 19 Jan 2015 - for getting subscription history*/
        private async Task<HttpResponseMessage> GetLLOMSubscriptionHistory(Models.LOTG.Request x)
        {
            //IList<Models.LOTG.Collection> result = new List<Models.LOTG.Collection>();
            return await Task.Run(() =>
            {
                var result = CRM_API.DB.LOTG.SProc.GetLLOMSubscriptionHistory(x.MobileNo, x.Sitecode,x.Id).Result;
                return Request.CreateResponse(HttpStatusCode.OK, result);
            });
        }
        /*End added*/

        /*Added by Hari - 19 Jan 2015 - for getting subscription history*/
        private async Task<HttpResponseMessage> GetLLOMSubscriptionHistory_v1(Models.LOTG.Request x)
        {
            //IList<Models.LOTG.Collection> result = new List<Models.LOTG.Collection>();
            return await Task.Run(() =>
            {
                var result = CRM_API.DB.LOTG.SProc.GetLLOMSubscriptionHistory_v1(x.MobileNo, x.Sitecode, x.Id).Result;
                return Request.CreateResponse(HttpStatusCode.OK, result);
            });
        }
        /*End added*/


        /*Added by Hari - 19 Jan 2015 - for getting subscription history*/
        private async Task<HttpResponseMessage> GetBundleSubscriptionHistory(Models.LOTG.Request x)
        {
            //IList<Models.LOTG.Collection> result = new List<Models.LOTG.Collection>();
            return await Task.Run(() =>
            {
                var result = CRM_API.DB.LOTG.SProc.GetBundleSubscriptionHistory(x.MobileNo, x.Sitecode,x.Id).Result;
                return Request.CreateResponse(HttpStatusCode.OK, result);
            });
        }
        /*End added*/

        /*Added by Hari - 19 Jan 2015 - for getting subscription history*/
        private async Task<HttpResponseMessage> GetCountrySaverSubscriptionHistory(Models.LOTG.Request x)
        {
            //IList<Models.LOTG.Collection> result = new List<Models.LOTG.Collection>();
            return await Task.Run(() =>
            {
                var result = CRM_API.DB.LOTG.SProc.GetCountrySaverSubscriptionHistory(x.MobileNo, x.Sitecode,x.Id).Result;
                return Request.CreateResponse(HttpStatusCode.OK, result);
            });
        }
        /*End added*/

        /*Added by Hari - 19 Jan 2015 - for getting subscription history*/
        private async Task<HttpResponseMessage> GetBundleDetail(Models.LOTG.Request x)
        {
            //IList<Models.LOTG.Collection> result = new List<Models.LOTG.Collection>();
            return await Task.Run(() =>
            {
                var result = CRM_API.DB.LOTG.SProc.GetBundleDetail(x.MobileNo, x.Sitecode).Result;
                return Request.CreateResponse(HttpStatusCode.OK, result);
            });
        }
        /*End added*/

        /*Added by Hari - 19 Jan 2015 - for getting subscription history*/
        private async Task<HttpResponseMessage> DoBundleConfirmCancel(Models.LOTG.Request x)
        {
            //IList<Models.LOTG.Collection> result = new List<Models.LOTG.Collection>();
            return await Task.Run(() =>
            {
                var result = CRM_API.DB.LOTG.SProc.BundleConfirmCancel(x.MobileNo, x.Sitecode, x.Id,x.LoggedUser);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            });
        }
        /*End added*/

        /*Added by Hari - 19 Jan 2015 - for getting subscription history*/
        private async Task<HttpResponseMessage> DoLLOMConfirmCancel(Models.LOTG.Request x)
        {
            //IList<Models.LOTG.Collection> result = new List<Models.LOTG.Collection>();
            return await Task.Run(() =>
            {
                var result = CRM_API.DB.LOTG.SProc.LLOMConfirmCancel(x.MobileNo, x.Sitecode, x.Reg_Number, x.Id).Result;
                return Request.CreateResponse(HttpStatusCode.OK, result);
            });
        }
        /*End added*/
        
        /*Added by Hari - 19 Jan 2015 - for cancel country saver subscription*/
        private async Task<HttpResponseMessage> DoCountrySaverConfirmCancel(Models.LOTG.Request x)
        {
            //IList<Models.LOTG.Collection> result = new List<Models.LOTG.Collection>();
            return await Task.Run(() =>
            {
                var result = CRM_API.DB.CountrySaverPAYM.SProc.CancelBundleCountrySaverDetail(x.Sitecode, 2, x.MobileNo, DateTime.Now, x.Id, 0, x.LoggedUser,x.AccountNo, x.Pack_Dest);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            });
        }
        /*End added*/


        private async Task<HttpResponseMessage> DoChangePayment(Models.LOTG.Request x)
        {
            //Models.LOTG.Request result = new Models.LOTG.Request();
            GeneralChecking(x);
            return await Task.Run(() =>
            {
                var result = CRM_API.DB.LOTG.SProc.ChangePaymentMode(x.MobileNo, x.Reg_Number,x.Sitecode, (int)x.Payment_Type, x.LoggedUser).Result;
                return Request.CreateResponse(HttpStatusCode.OK, result);
            });
        }
        private async Task<HttpResponseMessage> DoRenewContract(Models.LOTG.Request x)
        {
            //Models.LOTG.Request result = new Models.LOTG.Request();
            GeneralChecking(x);
            return await Task.Run(() =>
            {
                var result = CRM_API.DB.LOTG.SProc.RenewContract(x.MobileNo,x.Reg_Number, x.Sitecode, x.LoggedUser).Result;
                return Request.CreateResponse(HttpStatusCode.OK, result);
            });
        }
        private async Task<HttpResponseMessage> DoStopContract(Models.LOTG.Request x)
        {
            //Models.LOTG.Request result = new Models.LOTG.Request();
            GeneralChecking(x);
            return await Task.Run(() =>
            {
                var result = CRM_API.DB.LOTG.SProc.UnsubscribeContract(x.MobileNo,x.Reg_Number, x.Sitecode, x.LoggedUser).Result;
                return Request.CreateResponse(HttpStatusCode.OK, result);
            });
        }
        private void GeneralChecking(Models.LOTG.Request x)
        {
            if (string.IsNullOrEmpty(x.MobileNo))
                throw new Exception("Mobile Number Cannot Empty.");
            if (string.IsNullOrEmpty(x.Sitecode))
                throw new Exception("Sitecode Cannot Empty.");
            if (string.IsNullOrEmpty(x.LoggedUser))
                throw new Exception("Required Username.");
        }
    }
}
