﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NLog;

namespace CRM_API.Controllers
{
    public class Customer2in1SwapController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private string senderSMS = "111";

        // POST api/customer2in1swap
        [HttpPost]
        public HttpResponseMessage Post(CRM_API.Models.Customer2in1Swap orderToSwap)
        {

            CRM_API.Models.SMSMsgReturn resSwapMsg = new Models.SMSMsgReturn()
            {
                errsubject = "Obsolote Function",
                errcode = -1,
                errmsg = "Please use api/customer2in1"
            };

            //if (orderToSwap != null && (orderToSwap.order_id <= 0 || string.IsNullOrEmpty(orderToSwap.swapiccid)))
            //    return Request.CreateResponse(HttpStatusCode.PreconditionFailed, new DB.Common.ErrCodeMsg() { errcode = -1, errmsg = "Empty variable" });

            //Helpers.SendSMS sms = new Helpers.SendSMS();

            
//#if !DEBUG
//            resSwapMsg = 
//                CRM_API.DB.Customer2in1.SProc.SimSwap(orderToSwap.order_id, orderToSwap.swapiccid, orderToSwap.crm_login);
//            if (resSwapMsg != null)
//            {
//                Log.Debug("--------------------------------------------------------");
//                Log.Debug(string.Format("{0} SIM Swap {1}:{2} {3} {4} {5}",
//                    orderToSwap.mobileno,
//                    resSwapMsg.errcode, resSwapMsg.errmsg,
//                    orderToSwap.swapiccid, orderToSwap.crm_login, resSwapMsg.sms_msg));

//                if (resSwapMsg.errcode == 0)
//                {
//                    //4.       Cancel Location Update
//                    string[] resUpdate = new Helpers.HLR().CancelLocation(orderToSwap.mobileno);
//                    int status = -1;
//                    if (resUpdate.Length > 0)
//                        int.TryParse(resUpdate[0], out status);
//                    Log.Debug(string.Format("{0} Location Update Status {1}", orderToSwap.mobileno, status));
//                    /*
//                    if (status == -1)
//                    {
//                        resSwapMsg.errcode = -1;
//                        resSwapMsg.errmsg = "Failed to send Location Update";
//                    }
//                     */
//                }

//                if (resSwapMsg.errcode == 0)
//                {
//                    // Update status
//                    CRM_API.DB.Customer2in1.SProc.OrderStatusUpdate(orderToSwap.order_id, "ACTIVE", orderToSwap.crm_login);
//                    Log.Debug(string.Format("{0} Change Status to Active", orderToSwap.mobileno));
//                    // Send using the originator sms
//                    sms.Send(false, orderToSwap.mobileno, senderSMS, resSwapMsg.sms_msg, 100);
//                    Log.Debug(string.Format("{0} SMS Notification #1 {1} {2}", orderToSwap.mobileno, senderSMS, resSwapMsg.sms_msg));
//                    // Send using the MMI sms
//                    sms.Send(true, orderToSwap.mobileno, senderSMS, resSwapMsg.sms_msg, 100);
//                    Log.Debug(string.Format("{0} SMS Notification #2 {1} {2}", orderToSwap.mobileno, senderSMS, resSwapMsg.sms_msg));
//                }
//                Log.Debug("--------------------------------------------------------");
//            }
//#endif

            return Request.CreateResponse(HttpStatusCode.OK, resSwapMsg);
        }
    }
}
