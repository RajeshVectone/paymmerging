﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CRM_API.Controllers
{
    public class BUBController : ApiController
    {
        //
        // GET: /BUB/

        public HttpResponseMessage get(string mobileno)
        {
            try
            {
                var result = CRM_API.DB.viewtransaction.BUsearch(mobileno);
                return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());

            }
            catch (WebException ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new List<CRM_API.Models.financeviewtrans>());
            }
        }

    }
}
