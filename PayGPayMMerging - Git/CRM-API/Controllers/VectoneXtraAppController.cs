﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CRM_API.Controllers
{
    public class VectoneXtraAppController : ApiController
    {
        public HttpResponseMessage Post(CRM_API.Models.CustomerOverviewPageInput input)
        {
            System.Web.HttpContext.Current.Session["GlobalProduct"] = input.Product_Code;
            switch (input.Type)
            {
                case CRM_API.Models.CustomerOverviewPageType.GetVectoneXtraDetails:
                    CRM_API.Models.VectoneXtraAppOutput resValues = CRM_API.DB.Customer.VectoneXtraApp.GetVectoneXtraDetails(input.MobileNo);
                    if (resValues == null)
                        return Request.CreateResponse(HttpStatusCode.NotFound, new CRM_API.Models.VectoneXtraAppOutput());
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, resValues);
                    }
                //CRM Enhancement 2
                case CRM_API.Models.CustomerOverviewPageType.GetVectoneXtraCallHistory:
                    IEnumerable<CRM_API.Models.VectoneXtraCallHistoryOutput> resValues2 = CRM_API.DB.Customer.VectoneXtraApp.GetVectoneXtraCallHistory(input.MobileNo, input.DateFrom, input.DateTo, input.Sitecode, input.PackageID);
                    if (resValues2 == null)
                        return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.VectoneXtraAppOutput>());
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, resValues2);
                    }
                default:
                    return Request.CreateResponse(HttpStatusCode.NotFound, new CRM_API.Models.VectoneXtraAppOutput());
            }

        }
    }
}
