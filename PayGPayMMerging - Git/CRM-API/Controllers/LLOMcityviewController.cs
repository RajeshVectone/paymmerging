﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml;

namespace CRM_API.Controllers
{
    public class LLOMcityviewController : ApiController
    {
        //
        // GET: /LLOMcityview/

        public HttpResponseMessage get()
        {
            try
            {
                var result = CRM_API.DB.viewtransaction.lottmcityview();
                return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
            }
            catch (WebException ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new List<CRM_API.Models.financeviewtrans>());
            }
        }

    }
}
