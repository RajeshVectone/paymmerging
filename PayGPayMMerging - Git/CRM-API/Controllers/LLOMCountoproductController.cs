﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml;

namespace CRM_API.Controllers
{
    public class LLOMCountoproductController : ApiController
    {
        //
        // GET: /LLOMCountoproduct/

        public HttpResponseMessage get(string country)
        {
            try
            {
                var result = CRM_API.DB.viewtransaction.lotgcountproduct(country);
                return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
            }
            catch (WebException ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new List<CRM_API.Models.financeviewtrans>());
            }
        }


    }
}
