﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CRM_API.Controllers
{
    public class SIMOrderController : ApiController
    {
        // Post api/simorder
        [HttpPost]
        public HttpResponseMessage Post(CRM_API.Models.SIMOrderItem simorderRequest)
        {
            if (simorderRequest != null)
            {
                switch (simorderRequest.action_type)
                {
                    case Models.SIMOrderActionType.SearchByNameNew:
                        return SearchByNameNewRequest(simorderRequest);
                    case Models.SIMOrderActionType.SearchByNameProcessed:
                        return SearchByNameProcessed(simorderRequest);
                    case Models.SIMOrderActionType.SearchByNameUnprocessed:
                        return SearchByNameUnprocessed(simorderRequest);
                    case Models.SIMOrderActionType.SearchBySIMIdNew:
                        return SearchBySIMIdNewRequest(simorderRequest);
                    case Models.SIMOrderActionType.SearchBySIMIdProcessed:
                        return SearchBySIMIdProcessed(simorderRequest);
                    case Models.SIMOrderActionType.SearchBySIMIdUnprocessed:
                        return SearchBySIMIdUnprocessed(simorderRequest);
                    case Models.SIMOrderActionType.SearchByOrderId:
                        return SearchByOrderId(simorderRequest);
                    default:
                        return EmptyResult();
                }

            }
            else
            {
                return EmptyResult();
            }
        }

        HttpResponseMessage EmptyResult()
        {
            return Request.CreateResponse(HttpStatusCode.NotImplemented,
                new DB.Common.ErrCodeMsg() { errcode = -1, errmsg = "Not Implemented", errsubject = "Error" });
        }

        HttpResponseMessage SearchByNameNewRequest(CRM_API.Models.SIMOrderItem simorderRequest)
        {
            Models.SIMOrderRequest requestParams = new Models.SIMOrderRequest() {
                fromdate = simorderRequest.fromdate,
                todate = simorderRequest.todate,
                status = "NEW ORDER",
                max_result = simorderRequest.max_result,
                full_name = simorderRequest.search_text
            };

            IEnumerable<CRM_API.Models.SIMOrderItem> resultValues = CRM_API.DB.SIMOrder.SProc.Search(requestParams);

            return Request.CreateResponse(HttpStatusCode.OK, resultValues);
        }

        HttpResponseMessage SearchByNameProcessed(CRM_API.Models.SIMOrderItem simorderRequest)
        {
            Models.SIMOrderRequest requestParams = new Models.SIMOrderRequest()
            {
                fromdate = simorderRequest.fromdate,
                todate = simorderRequest.todate,
                status = "DISPATCHED",
                max_result = simorderRequest.max_result,
                full_name = simorderRequest.search_text
            };

            IEnumerable<CRM_API.Models.SIMOrderItem> resultValues = CRM_API.DB.SIMOrder.SProc.Search(requestParams);

            return Request.CreateResponse(HttpStatusCode.OK, resultValues);
        }

        HttpResponseMessage SearchByNameUnprocessed(CRM_API.Models.SIMOrderItem simorderRequest)
        {
            Models.SIMOrderRequest requestParams = new Models.SIMOrderRequest()
            {
                fromdate = simorderRequest.fromdate,
                todate = simorderRequest.todate,
                status = "NEW ORDER",
                max_result = simorderRequest.max_result,
                full_name = simorderRequest.search_text
            };

            IEnumerable<CRM_API.Models.SIMOrderItem> resultValues = CRM_API.DB.SIMOrder.SProc.Search(requestParams);

            return Request.CreateResponse(HttpStatusCode.OK, resultValues);
        }

        HttpResponseMessage SearchBySIMIdNewRequest(CRM_API.Models.SIMOrderItem simorderRequest)
        {
            Models.SIMOrderRequest requestParams = new Models.SIMOrderRequest()
            {
                fromdate = simorderRequest.fromdate,
                todate = simorderRequest.todate,
                status = "NEW ORDER",
                max_result = simorderRequest.max_result,
                sim_id = simorderRequest.search_text
            };

            IEnumerable<CRM_API.Models.SIMOrderItem> resultValues = CRM_API.DB.SIMOrder.SProc.Search(requestParams);

            return Request.CreateResponse(HttpStatusCode.OK, resultValues);
        }

        HttpResponseMessage SearchBySIMIdProcessed(CRM_API.Models.SIMOrderItem simorderRequest)
        {
            Models.SIMOrderRequest requestParams = new Models.SIMOrderRequest()
            {
                fromdate = simorderRequest.fromdate,
                todate = simorderRequest.todate,
                status = "DISPATCHED",
                max_result = simorderRequest.max_result,
                sim_id = simorderRequest.search_text
            };

            IEnumerable<CRM_API.Models.SIMOrderItem> resultValues = CRM_API.DB.SIMOrder.SProc.Search(requestParams);

            return Request.CreateResponse(HttpStatusCode.OK, resultValues);
        }

        HttpResponseMessage SearchBySIMIdUnprocessed(CRM_API.Models.SIMOrderItem simorderRequest)
        {
            Models.SIMOrderRequest requestParams = new Models.SIMOrderRequest()
            {
                fromdate = simorderRequest.fromdate,
                todate = simorderRequest.todate,
                status = "NEW ORDER",
                max_result = simorderRequest.max_result,
                sim_id= simorderRequest.search_text
            };

            IEnumerable<CRM_API.Models.SIMOrderItem> resultValues = CRM_API.DB.SIMOrder.SProc.Search(requestParams);

            return Request.CreateResponse(HttpStatusCode.OK, resultValues);
        }
        
        HttpResponseMessage SearchByOrderId(CRM_API.Models.SIMOrderItem simorderRequest)
        {
            Models.SIMOrderRequest requestParams = new Models.SIMOrderRequest()
            {
                fromdate = simorderRequest.fromdate,
                todate = simorderRequest.todate,
                max_result = simorderRequest.max_result,
                full_name = simorderRequest.search_text,
                order_id = (int)simorderRequest.freesim_order_id
            };
            CRM_API.Models.SIMOrderItem resultValues = CRM_API.DB.SIMOrder.SProc.SearchByOrderId(requestParams);
            return Request.CreateResponse(HttpStatusCode.OK, resultValues);
        }
    }
}
