﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CRM_API.Models.Common;
using CRM_API.Models.CTP;
using System.Web.Script.Serialization;

namespace CRM_API.Controllers
{
    public class CTPController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        [HttpPost]
        public dynamic POST(SearchEntry x)
        {
            dynamic result = new ErrCodeMsg();
            Log.Info("CRM_API.Controllers.Post: Started");
            Log.Info(new JavaScriptSerializer().Serialize(x));
            try
            {
                switch (x.ActionType)
                {
                    case Models.CTP.Action.OptionList:
                        result = CRM_API.DB.CTP.SProc.GetSearchOption();
                        break;
                    case Models.CTP.Action.SearchList:
                        result = SearchCTPOrder(x);
                        break;
                    case Models.CTP.Action.DetailInfo:
                        break;
                    default: throw new ArgumentNullException("Unrecognize CTP Action Type");
                }
            }
            catch (Exception e)
            {
                string msg = string.Format("CRM_API.Controllers.Post : {0}",e.Message );
                Log.Debug(msg);
                var err = new ErrCodeMsg(){errcode = -1,errmsg = msg};
                return Request.CreateResponse(HttpStatusCode.OK, err);
            }
            Log.Info("CRM_API.Controllers.Post: Ended");
            return result;
        }
        private dynamic SearchCTPOrder(SearchEntry x)
        {
            var resx = CRM_API.DB.CTP.SProc.GetSearchOption().Where(w => w.Label.ToLower().Contains(x.SearchBy.ToLower()));
            if (resx == null)
                throw new NullReferenceException("Step "+((int)x.ActionType).ToString()+ " CTP Search Option Not Valid.");
            if (resx.Count() > 1)
                throw new DuplicateWaitObjectException("Step " + ((int)x.ActionType).ToString() + " Duplicated Search Option CTP.");

            //TODO : Get result from CRM_API.DB.CTP.SProc.SearchCTP (undone).
            
            return null;
        }
    }
}
