﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using CRM_API.Models.AirTimeTransfer;
using CRM_API.DB.AirTimeTransfer;

namespace CRM_API.Controllers
{
    /*
     * Air Time Transfer 
    By Mamin @27-01-2014 @myamin_amzah@yahoo.com
    Requirement : get List of AirTime Transfer
     * Param : msisdn (mandatory), datefrom/dateend, recipient name
     * 
    */
    public class AirTimeTransferController : ApiController
    {
        public async Task<HttpResponseMessage> Get([FromUri]AdditionalReqInfo Model)
        {
            var result = new Models.Common.ErrCodeMsg() { errcode = -1, errmsg = "Default Value" };
            try
            {                
                //return await GetSelection(Model);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new Models.Common.ErrCodeMsg()
                {
                    errcode = -1,
                    errmsg = ex.Message
                });
            }
            return await Task.Run(() =>
            {
                return Request.CreateResponse(HttpStatusCode.OK);
            });
        }

        private async Task<IEnumerable<AirTimeTransferRes>> GetList(AdditionalReqInfo Model)
        {
            Model = InitiateObj(Model);
            return await Task.Run(() =>
            {
                var result = SProc.GetListAirTimeTransfer(Model);
                return result;
            });
        }

        public async Task<HttpResponseMessage> Post(AdditionalReqInfo Model)
        {
            try
            {
                switch (Model.InfoType)
                {
                    case AdditionInfo.GETList: return Request.CreateResponse(HttpStatusCode.OK, await GetList(Model));
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed,
                    new Models.Common.ErrCodeMsg()
                    {
                        errcode = -1,
                        errmsg = e.Message,
                        errsubject = "General Error"
                    });
            }
            return await Task.Run(() =>
            {
                return Request.CreateResponse(HttpStatusCode.OK, Model);
            });
        }

        private AdditionalReqInfo InitiateObj(AdditionalReqInfo Model)
        {
            // if param date null, SP will also search this date range
            if (String.IsNullOrEmpty(Model.Datefrom))
                Model.Datefrom = DateTime.Now.AddYears(-20).ToString("dd/MM/yyyy");        //year minus 10
            if (String.IsNullOrEmpty(Model.Dateto))
                Model.Dateto = DateTime.Now.AddYears(1).ToString("dd/MM/yyyy");        //year plus 1
            if (String.IsNullOrEmpty(Model.Mobileno))
                Model.Mobileno = "1111";      //hardcoded avoid exception in sp            
            return Model;
        }
    }
}

