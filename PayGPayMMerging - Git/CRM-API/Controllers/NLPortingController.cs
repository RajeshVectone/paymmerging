﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Configuration;

namespace CRM_API.Controllers
{
    /// <summary>
    /// Web Api Porting  For Country Netherland
    /// </summary>
    public class NLPortingController : ApiController
    {
        // POST api/nlporting
        /// <summary>
        /// POST api/nlporting
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        public HttpResponseMessage Post(Models.Porting.NLSubmitEntry Model)
        {
           
            try
            {
                switch (Model.SubType)
                {
                    case Models.Porting.PortingStep.GetServiceProvider:  //SubType = 100
                        return GetServiceProvider(Model);
                    case Models.Porting.PortingStep.Submit:  //SubType = 2
                        return SubmitRequest(Model);
                    case Models.Porting.PortingStep.GetStateList:  //SubType = 101
                        return GetStateList(Model.Sitecode);
                    case Models.Porting.PortingStep.TrxList:   //SubType = 102
                        return SearchPorting(Model);
                    case Models.Porting.PortingStep.Viewporting:  //SubType = 103
                        return ViewPortingDetail(Model.MsgIdentifier, Model.Sitecode);
                    case Models.Porting.PortingStep.PortingPerformed:       //SubType = 104
                        return PerformPorting(Model);
                    case Models.Porting.PortingStep.Revise:     //SubType = 3
                        return EditRequest(Model);
                    case Models.Porting.PortingStep.Cancel:     //SubType = 5
                        return CancelRequest(Model);
                    default:
                        Model.errcode = -1;
                        Model.errmsg = "Empty Parameter [SubType]";
                        Model.errsubject = "General Error";
                        break;
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed,
                    new Models.Common.ErrCodeMsg()
                    {
                        errcode = -1,
                        errmsg = e.Message,
                        errsubject = "General Error"
                    });
            }
            return Request.CreateResponse(HttpStatusCode.OK, Model);
        }

        private HttpResponseMessage GetServiceProvider(Models.Porting.NLSubmitEntry In)
        {
            // OptType = "SP" (To Get Service Provider List)
            // OptType = "NO" (To Get Network Operator List)

            IEnumerable<Models.Porting.NLServiceProviderList> result = CRM_API.DB.PortingNL.SProc.GetServiceProvider(In.OptType, In.ProviderCode, In.Sitecode);
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        private HttpResponseMessage SubmitRequest(Models.Porting.NLSubmitEntry In)
        {
            try
            {
                if (ValidateDate(In) == true)
                {
                    Models.Porting.NLSubmitEntry result = CRM_API.DB.PortingNL.SProc.SubmitEntry(In);
                        return Request.CreateResponse(HttpStatusCode.OK,result);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new Models.Common.ErrCodeMsg()
                    {
                        errcode = -1,
                        errmsg = In.errmsg
                    });
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new Models.Common.ErrCodeMsg()
                {
                    errcode = -1,
                    errmsg = ex.Message
                });
            }
        }

        private HttpResponseMessage ViewPortingDetail(string MsgIdentifier, string sitecode)
        {
            Models.Porting.NLPortingList result = CRM_API.DB.PortingNL.SProc.ViewDetailPortingList(MsgIdentifier,sitecode);
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        private HttpResponseMessage SearchPorting(Models.Porting.NLSubmitEntry In)
        {
            IEnumerable<Models.Porting.NLPortingList> result = CRM_API.DB.PortingNL.SProc.PortingList(In);
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        private HttpResponseMessage GetStateList(string sitecode)
        {
            IEnumerable<Models.Porting.StateList> result = CRM_API.DB.PortingNL.SProc.GetState(sitecode);
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        private bool ValidateDate(Models.Porting.NLSubmitEntry In)
        {
            try
            {
                if (CheckReqDate(In))
                    return true;
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                In.errmsg = ex.Message;
                return false;
            }
        }

        public static bool CheckReqDate( Models.Porting.NLSubmitEntry In)
        {
            if (In.RequestDate < ProposeReqDate(5).AddMinutes(-15))
            {
                In.errmsg = "Request Date is less than 5 working days.";
                return false;
            }
            if (In.RequestDate > ProposeReqDate(120))
            {
                In.errmsg = "Request Date should not less than 5 working days and not more than 120 days.";
                return false;
            }
            if (In.RequestDate > ProposeReqDate(5).AddMinutes(-15) && In.RequestDate < ProposeReqDate(120))
            {
                if (!IsWorkingDay(In.RequestDate))
                {
                    In.errmsg = "Request Date is not a working day.";
                    return false;
                }
            }

            return true;
        }

        public static DateTime ProposeReqDate(int WorkDays)
        {
            //WorkDays=5 || should be in 5 working days later
            int workingDays = 0;
            DateTime PropDate = DateTime.Now;
            for (int i = 0; i < 200; i++)
            {
                PropDate = PropDate.AddDays(1);
                if (IsWorkingDay(PropDate))
                    workingDays++;

                if (workingDays == WorkDays)
                    break;
            }

            return PropDate.AddMinutes(15);
        }

        public static bool IsWorkingDay(DateTime dt)
        {
            if (dt.DayOfWeek == DayOfWeek.Saturday)
                return false;
            if (dt.DayOfWeek == DayOfWeek.Sunday)
                return false;

            return true;
        }

        private HttpResponseMessage PerformPorting(Models.Porting.NLSubmitEntry In)
        {

                    CRM_API.DB.Common.ErrCodeMsg result = CRM_API.DB.PortingNL.SProc.PortingPerformed(In);
                    return Request.CreateResponse(HttpStatusCode.OK, result);

        }

        private HttpResponseMessage CancelRequest(Models.Porting.NLSubmitEntry In)
        {

                CRM_API.DB.Common.ErrCodeMsg result = CRM_API.DB.PortingNL.SProc.CancelPorting(In);
                return Request.CreateResponse(HttpStatusCode.OK,result);
        }

        private HttpResponseMessage EditRequest(Models.Porting.NLSubmitEntry In)
        {

            CRM_API.DB.Common.ErrCodeMsg result = CRM_API.DB.PortingNL.SProc.EditPorting(In);
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
    }
}
