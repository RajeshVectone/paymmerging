﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using NLog;
using System.IO;
using System.Net.Mail;

namespace CRM_API.Controllers
{
    public class SuspendServiceController : ApiController
    {
        // GET api/suspendservice

        public HttpResponseMessage Get(string UpdatedBy, string CustIDs, string sitecode)
        {
            var result = CRM_API.DB.PMBODDOperations.SuspendService(UpdatedBy, CustIDs , sitecode);
            //Code to Send SMS and e-Mail
            var BatchId = result[0].Batch_Id;
            try
            {
                var result2 = CRM_API.DB.PMBODDOperations.GetMobileNumbersAndEmail(BatchId, sitecode);
                List<CRM_API.Models.SMSMobileNosEmailAccs> MobNoEmailList = result2.ToList();
                //LiveOnly
                for (int i = 0; i < MobNoEmailList.Count; i++)
                {
                    SMS_SuspendService(MobNoEmailList[i].Mobile_No.Trim());
                    EmailSuspensionOfService(MobNoEmailList[i].EMail_Id.Trim(), MobNoEmailList[i].Customer_Name, MobNoEmailList[i].Mobile_No.Trim());
                }

                //SMS_SuspendService("447465187104");
                //EmailSuspensionOfService("r.murali@vectone.com", "Murali", "447465187104");
                //SMS_SuspendService("447465187105");
                //EmailSuspensionOfService("p.tamilmani@vectone.com", "Praveen", "447465187105");

                return Request.CreateResponse(HttpStatusCode.OK, "SMS and Email successfully sent to " + MobNoEmailList.Count.ToString() + " customers.");
            }
            catch (WebException ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
            }
           // return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
        }

        // GET api/suspendservice/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/suspendservice
        //public void Post([FromBody]string value)
        //{
        //}

        //Modified by BSK for Improvement : Jira CRMT-141
        public HttpResponseMessage Post(CRM_API.Models.SuspendServiceNewModel SSmodel)
        {
            var result = CRM_API.DB.PMBODDOperations.SuspendService(SSmodel.UpdatedBy, SSmodel.CustIDs, SSmodel.sitecode);
            //Code to Send SMS and e-Mail
            var BatchId = result[0].Batch_Id;
            try
            {
                var result2 = CRM_API.DB.PMBODDOperations.GetMobileNumbersAndEmail(BatchId,SSmodel.sitecode);
                List<CRM_API.Models.SMSMobileNosEmailAccs> MobNoEmailList = result2.ToList();
                //LiveOnly
                for (int i = 0; i < MobNoEmailList.Count; i++)
                {
                    SMS_SuspendService(MobNoEmailList[i].Mobile_No.Trim());
                    EmailSuspensionOfService(MobNoEmailList[i].EMail_Id.Trim(), MobNoEmailList[i].Customer_Name, MobNoEmailList[i].Mobile_No.Trim());
                }

                //SMS_SuspendService("447465187104");
                //EmailSuspensionOfService("r.murali@vectone.com", "Murali", "447465187104");
                //SMS_SuspendService("447465187105");
                //EmailSuspensionOfService("p.tamilmani@vectone.com", "Praveen", "447465187105");

                return Request.CreateResponse(HttpStatusCode.OK, "SMS and Email successfully sent to " + MobNoEmailList.Count.ToString() + " customers.");
            }
            catch (WebException ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
            }
            // return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());

        }

        // PUT api/suspendservice/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/suspendservice/5
        public void Delete(int id)
        {
        }

        public bool SMS_SuspendService(string mobileNo)
        {
            try
            {
                string PAYMTestMobileNo = System.Configuration.ConfigurationManager.AppSettings["PAYMTestMobileNo"];
                if (!string.IsNullOrEmpty(PAYMTestMobileNo))
                    mobileNo = PAYMTestMobileNo;

                //string smsText = string.Format("Your last Payment was UnSuccessful on {1:dd-MM-yyyy}", newplan, start_date);
                string smsText = string.Format("Your Service have been suspended! Please Contact Vectone Customer Service Center immediately for any Queries!");
                string[] res = new Helpers.SendSMS().Send(false, mobileNo, "111", smsText, 0);
                if (res != null && res.Count() > 0)
                {
                    return !(res[0] == "-1");
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                //Log.Error("PAYMPlanBundlesController.SMSChangePlan", ex.Message);
                return false;
            }
        }

        //public bool EmailSuspensionOfService(string email, string fullname, string mobileNo, string oldPlanName, string newPlanName, float amount, string pay_reference)
        public bool /* HttpResponseMessage */ EmailSuspensionOfService(string email, string fullname, string mobileNo)
        {
            try
            {
                string PAYMTestEMail = System.Configuration.ConfigurationManager.AppSettings["PAYMTestEMail"];
                if (!string.IsNullOrEmpty(PAYMTestEMail))
                    email = PAYMTestEMail;

                string mailSubject = "Service Suspended – Pay Monthly.";
                string mailLocation;
                     mailLocation = System.Web.HttpContext.Current.Server.MapPath("~/Email_template/PMBO_Suspend_Service.htm").TrimEnd('/');
                StreamReader mailContent_file = new StreamReader(mailLocation);
                string mailContent = mailContent_file.ReadToEnd();
                mailContent_file.Close();
                mailContent = mailContent
                .Replace("[FullName]", fullname)
                .Replace("[CustomerNumber]", mobileNo);
                //.Replace("[ExistingPlan]", oldPlanName)
                //.Replace("[NewPlan]", newPlanName)
                //.Replace("[Amount]", string.Format("{0:N2", amount))
                //.Replace("[PaymentReferenceNumber]", pay_reference);
                // Send the email
                MailAddress mailFrom = new MailAddress("noreply@vectonemobile.co.uk", "Vectone Mobile");
                MailAddressCollection mailTo = new MailAddressCollection();
                mailTo.Add(new MailAddress(email, string.Format("{0}", fullname)));
                return CRM_API.Helpers.EmailSending.Send(true, mailFrom, mailTo, null, null, mailSubject, mailContent);
                //  var boolemail 
                //return Request.CreateResponse(HttpStatusCode.OK, "some message");
            }
            catch (Exception ex)
            {
                //Log.Error("PAYMPlanBundlesController.EmailChangePlan", ex.Message);
              //  return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
                return false;
            }

        }
    }
}
