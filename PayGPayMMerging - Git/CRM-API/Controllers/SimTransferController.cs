﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace CRM_API.Controllers
{
    public class SimTransferController : ApiController
    {
        // GET api/simtransfer

        // GET api/simtransfer/5

        // POST api/simtransfer
        public async Task<HttpResponseMessage> Post(Models.SimTransfer.RequestModel Model)
        {
            try
            {
                switch (Model.SubType)
                {
                    case Models.SimTransfer.RequestType.GetLog:
                        return await GetSimTransferLog(Model);
                }
                return Request.CreateResponse(HttpStatusCode.OK, Model);
            }
            catch (Exception e)
            {
                Model.errcode = -1;
                Model.errmsg = "POST api/simtransfer: " + e.Message;
                return Request.CreateResponse(HttpStatusCode.OK, Model);
            }
        }
        private async Task<HttpResponseMessage> GetSimTransferLog(Models.SimTransfer.RequestModel Ix)
        {
            return await Task.Run<HttpResponseMessage>(() =>
            {
                var result = CRM_API.DB.SimTransfer.SProc.GetSimTransferLogData(Ix.Msisdn, Ix.Sitecode);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            });
        }
    }
}
