﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;

namespace CRM_API.Controllers
{
    public class CustomerPaymentProfileController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public dynamic Post(CRM_API.Models.CustomerProfile.CustomerPaymentRequest x)
        {
            try
            {
                Log.Info("---------- #Start CustomerPaymentProfile ------------------");
                Log.Info(JsonConvert.SerializeObject(x));
                switch (x.RequestType)
                {
                    case Models.CustomerProfile.Action.ListProfile: return GetListProfile(x);
                    case Models.CustomerProfile.Action.DetailInfo: return GetDetailInfo(x);
                    case Models.CustomerProfile.Action.SetDefaultProfile: return SetDefaultProfile(x);
                    case Models.CustomerProfile.Action.SettingInfo: break;
                    case Models.CustomerProfile.Action.SettingUpdate: return SettingUpdate(x);
                    case Models.CustomerProfile.Action.DoBlockCard: return DoBlockCard(x);
                    default: break;
                }
            }
            catch (Exception e)
            {
                Log.Error("CustomerPaymentProfile:" + e.Message);

                return Request.CreateResponse(HttpStatusCode.OK, new Models.Common.ErrCodeMsg()
                {
                    errcode = -1,
                    errmsg = "CustomerPaymentProfile:" + e.Message
                });
            }
            Log.Info("-------------- #End CustomerPaymentProfile --------------------");
            return Request.CreateResponse(HttpStatusCode.OK, x);
        }

        //22-Feb-2019 : Moorthy : Added for Block Card
        private dynamic DoBlockCard(Models.CustomerProfile.CustomerPaymentRequest x)
        {
            var result = DB.PaymentProfile.SProc.DoBlockCard(x.Msisdn, x.LastCCNumber, x.card_status, x.CalledBy);
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        private dynamic GetListProfile(CRM_API.Models.CustomerProfile.CustomerPaymentRequest x)
        {
            var result = DB.PaymentProfile.SProc.GetListProfile(x.Msisdn, x.Sitecode);
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
        private dynamic GetDetailInfo(CRM_API.Models.CustomerProfile.CustomerPaymentRequest x)
        {
            var result = DB.PaymentProfile.SProc.DetailProfile(x.LastCCNumber, x.Msisdn, x.Sitecode);
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
        private dynamic SetDefaultProfile(CRM_API.Models.CustomerProfile.CustomerPaymentRequest x)
        {
            var result = DB.PaymentProfile.SProc.SetDefaultProfile(x.LastCCNumber, x.Msisdn, x.Sitecode);
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
        private dynamic GetSettingInfo(CRM_API.Models.CustomerProfile.CustomerPaymentRequest x)
        {

            return Request.CreateResponse(HttpStatusCode.OK, x);
        }
        private dynamic SettingUpdate(CRM_API.Models.CustomerProfile.CustomerPaymentRequest x)
        {
            var result = DB.PaymentProfile.SProc.SetAutopupProfile(x.LastCCNumber, x.Msisdn, x.AxTopupAmount, x.Currency, x.AxTopupStatus, x.Sitecode, x.PaymentType);
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
    }
}
