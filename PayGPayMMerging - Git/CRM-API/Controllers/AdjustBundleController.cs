﻿using CRM_API.Models.PortingModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CRM_API.Models;

namespace CRM_API.Controllers
{
    public class AdjustBundleController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage Post(CRM_API.Models.AdjustBundleModelsInput input)
        {
            IEnumerable<CRM_API.Models.AdjustBundleModelsOutput> resValues = CRM_API.DB.AdjustBundle.GetPackInfoByBundleId(input);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<AdjustBundleModelsOutput>() { new AdjustBundleModelsOutput { errcode = -1, errmsg = "General Error" } });
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues);
        }


    }
}
