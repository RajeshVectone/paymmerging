﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml;
using CRM_API.DB.BreakageUsage;
using CRM_API.Models.BreakageUsageModels;

namespace CRM_API.Controllers
{
    public class FinBreakagereportController : ApiController
    {
        public HttpResponseMessage Post(BreakageUsageInput Model)
        {
            try
            {
                List<BreakageUsageOutput> resultValues = BreakageUsage.GetBreakageUsageInfo(Model).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, resultValues.ToArray());
            }
            catch (WebException ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new List<BreakageUsageOutput>());
            }
        }

    }
}
