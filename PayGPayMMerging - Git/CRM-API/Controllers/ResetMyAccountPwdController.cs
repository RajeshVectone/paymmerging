﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dapper;
using Mandrill;
using Mandrill.Model;
using Newtonsoft.Json;
using NLog;

namespace CRM_API.Controllers
{
    public class ResetMyAccountPwdController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public class ResetMyAccountPwdRef
        {
            public int errcode { get; set; }
            public string errmsg { get; set; }
            public string first_name { get; set; }
            public string email { get; set; }
        }

        public HttpResponseMessage Get(string sitecode, string msisdn, string password, string firstname, string email, string crm_user, int process_type = 0)
        {
            Log.Info("ResetMyAccountPwd"); 
            try
            {
                if (process_type == 0)
                {
                    using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                    {
                        conn.Open();
                        var result = conn.Query<ResetMyAccountPwdRef>("crm_reset_password", new
                        {
                            @sitecode = sitecode,
                            @brand = 1,
                            @msisdn = msisdn,
                            @password = password
                        }, commandType: CommandType.StoredProcedure);
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    }
                }
                else
                {
                    //Email Trigger
                    if (sitecode == "MCM" || sitecode == "BAU")
                    {
                        try
                        {
                            var api = new MandrillApi(ConfigurationManager.AppSettings["MAILCHIMPAPIKEY"]);
                            var message = new MandrillMessage();
                            message.AddGlobalMergeVars("FNAME", firstname);
                            message.AddTo(email);
                            IList<MandrillSendMessageResponse> response = api.Messages.SendTemplateAsync(message, ConfigurationManager.AppSettings["MAILCHIMPMYVECTONEFORGOTPASSWORD_" + sitecode]).Result;
                            Log.Info("ResetMyAccountPwd Mail Response : " + JsonConvert.SerializeObject(response));
                        }
                        catch (Exception ex)
                        {
                            Log.Error("ResetMyAccountPwd Mail Error : " + ex.Message);
                        }
                    }
                    //SMS Trigger
                    if (sitecode == "MCM" || sitecode == "BAU")
                    {
                        try
                        {
                            string smsURL = CRM_API.DB.SMSCampaign.SProc.GetSMSGatewayInfo(sitecode, msisdn);
                            var smsResponse = CRM_API.Helpers.SendSMS.SMSSendOurGateway("Vectone", msisdn, ConfigurationManager.AppSettings["SMSMESSAGEMYVECTONEFORGOTPASSWORD"], smsURL);
                            CRM_API.DB.SMSCampaign.SProc.InsertSMSNotifyText(msisdn, ConfigurationManager.AppSettings["SMSMESSAGEMYVECTONEFORGOTPASSWORD_" + sitecode], crm_user, smsResponse == "-1" ? 0 : 1, "-", firstname, "Reset My Vectone Password");
                            Log.Info("ResetMyAccountPwd : SMS : Response : " + smsResponse);
                        }
                        catch (Exception ex)
                        {
                            Log.Error("ResetMyAccountPwd SMS Error : " + ex.Message);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, new List<ResetMyAccountPwdRef>() { new ResetMyAccountPwdRef { errcode = 0, errmsg = "Success" } });
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new List<ResetMyAccountPwdRef>() { new ResetMyAccountPwdRef { errcode = -1, errmsg = ex.Message } });
            }
        }
    }
}
