﻿using CRM_API.Models.Issue;
using CRM_API.Models.PortingModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CRM_API.Controllers
{
    public class ItPWDIHistoryController : ApiController
    {
        public HttpResponseMessage Post(CRM_API.Models.Issue.IssueHistoryDetailInput input)
        {
            IEnumerable<IssueHistoryDetailOutput> resValues = CRM_API.DB.IssueList.GetIssueHistoryDetailList(input);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<IssueHistoryDetailOutput>());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }


    }
}
