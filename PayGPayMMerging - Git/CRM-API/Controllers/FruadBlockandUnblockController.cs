﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NLog;
using System.Text;
using System.IO;
using CRM_API.Models.FruadBlockUnBlockModels;
using CRM_API.DB.FruadBlockUnBlock;

namespace CRM_API.Controllers
{
    public class FruadBlockandUnblockController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        // POST api/FruadBlockandUnblock/
        [HttpPost]
        public HttpResponseMessage Post(FruadBlockUnBlockInput input)
        {
            DB.Common.ErrCodeMsg errResult = new DB.Common.ErrCodeMsg()
           {
               errcode = -1,
               errsubject = "General Error",
               errmsg = "Empty New History"
           };
            switch (input.Block_Type)
            {
                case FruadBlockUnBlockMode.GetBlockReason:
                    return GetBlockReason();
                case FruadBlockUnBlockMode.DoBlockUnblockRequest:
                    return DoBlockUnblockRequest(input);
                case FruadBlockUnBlockMode.GetBlockUnblockRequest:
                    return GetBlockUnblockRequest(input);
                case FruadBlockUnBlockMode.DoBlockConfirmation:
                    return DoBlockConfirmation(input);
                default: break;
            }
            return Request.CreateResponse(HttpStatusCode.OK, errResult);
        }

        HttpResponseMessage GetBlockReason()
        {
            IEnumerable<BlockReason> output = SProc.GetBlockReason();
            if (output == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<BlockReason>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, output.ToArray());
        }

        HttpResponseMessage DoBlockUnblockRequest(FruadBlockUnBlockInput input)
        {
            IEnumerable<BlockCommon> output = SProc.DoBlockUnblockRequest(input.USERTYPE, input.USERINFO, input.REQTYPE, input.REASON_ID, input.FK_REQ_USERID, input.Product, input.SiteCode);
            if (output == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<BlockCommon>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, output.ToArray());
        }

        HttpResponseMessage GetBlockUnblockRequest(FruadBlockUnBlockInput input)
        {
            IEnumerable<BlockUnblockRequestOutput> output = SProc.GetBlockUnblockRequest(input.FK_REQ_USERID, input.role_id);
            if (output == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<BlockUnblockRequestOutput>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, output.ToArray());
        }

        HttpResponseMessage DoBlockConfirmation(FruadBlockUnBlockInput input)
        {
            IEnumerable<BlockCommon> output = SProc.DoBlockConfirmation(input.FK_REQ_USERID, input.role_id, input.REQID, input.loginuser_name);
            if (output == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<BlockCommon>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, output.ToArray());
        }
    }
}
