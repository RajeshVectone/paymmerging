﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CRM_API.Controllers
{
    /// <summary>
    /// PMBO DD operations Search
    /// api/PMBODDoperations
    /// </summary>
    public class PMBODDoperationsController : ApiController
    {
        /// <summary>
        /// PMBO DD operations Search
        /// api/PMBODDOperations?DDOperations={0}&fromdate={1}&todate={2}&CCTransactionID={3}&SubscriberID={4}&SortCode={5}&AccountNumber={6}
        /// </summary>
        public HttpResponseMessage Get(string DDOperations, string FromDate, string ToDate, string CCTransactionID, string SubscriberID, string SortCode, string AccountNumber ,string SiteCode)
        {
            try
            {
                CRM_API.Models.PMBOSearch objSearch = new Models.PMBOSearch();
                objSearch.FromDate = FromDate;
                objSearch.ToDate = ToDate;
                objSearch.CCTransactionID = CCTransactionID;
                objSearch.SubscriberID = SubscriberID;
                objSearch.SortCode = SortCode;
                objSearch.AccountNumber = AccountNumber;
                objSearch.SiteCode = SiteCode;
                
                if (DDOperations == "1")
                {
                    var result = CRM_API.DB.PMBODDOperations.getPMBODDNewInstructions(objSearch);
                    return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
                }
                else if (DDOperations == "3")
                {
                    var result = CRM_API.DB.PMBODDOperations.getPMBODDCollectionInstructions(objSearch);
                    return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
                }
                return Request.CreateResponse(HttpStatusCode.OK, new List<CRM_API.Models.PMBODDOperations>());
            }
            catch (WebException ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new List<CRM_API.Models.PMBODDOperations>());
            }
        }        
    }
}
