﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CRM_API.Models;
using NLog;

namespace CRM_API.Controllers
{
    public class EmailSettingsUploadController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public HttpResponseMessage Post()
        {
            try
            {
                string id = System.Web.HttpContext.Current.Request.Form["id"];
                string template_name = System.Web.HttpContext.Current.Request.Form["template_name"];
                string email_subject = System.Web.HttpContext.Current.Request.Form["email_subject"];
                string filename = System.Web.HttpContext.Current.Request.Form["filename"];
                int processtype = Convert.ToInt32(System.Web.HttpContext.Current.Request.Form["processtype"]);
                string email_blob = System.Web.HttpContext.Current.Request.Form["email_blob"];

                //System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
                //if (hfc != null && hfc.Count > 0)
                //{
                //    System.Web.HttpPostedFile hpf = hfc[0];
                //    fullfilename = Path.GetFileName(hpf.FileName);
                //    hpf.SaveAs(Path.Combine(ConfigurationManager.AppSettings["QUEUEEMAILTEMPLATEPATH"], fullfilename));
                //    IEnumerable<CRM_API.Models.EmailSettingsCommon> resValues = CRM_API.DB.EmailSettings.UpdateEmailAssigning(email_subject, fullfilename, 1, template_name, 2, Convert.ToInt32(id), email_blob);
                //    if (resValues == null)
                //        return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.EmailSettingsCommon>().ToArray());
                //    else
                //    {
                //        return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
                //    }
                //}
                //else if (filename != "")
                //{
                IEnumerable<CRM_API.Models.EmailSettingsCommon> resValues = CRM_API.DB.EmailSettings.UpdateEmailAssigning(email_subject, filename, 1, template_name, processtype, Convert.ToInt32(id), email_blob);
                if (resValues == null)
                    return Request.CreateResponse(HttpStatusCode.OK, new List<CRM_API.Models.EmailSettingsCommon>() { new CRM_API.Models.EmailSettingsCommon { errcode = -1, errmsg = "Failure" } }.ToArray());
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
                }
                //}
                //else
                //{
                //    return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.EmailSettingsCommon>().ToArray());
                //}
            }
            catch (Exception ex)
            {
                Log.Error("EmailSettingsUpload : Error : " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.OK, new List<CRM_API.Models.EmailSettingsCommon>() { new CRM_API.Models.EmailSettingsCommon { errcode = -1, errmsg = "Failure" } }.ToArray());
            }
        }
    }
}
