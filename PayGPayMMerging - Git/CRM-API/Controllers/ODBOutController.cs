﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dapper;

namespace CRM_API.Controllers
{
    public class ODBOutController : ApiController
    {
        public class ODBOutRef
        {
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }

        public HttpResponseMessage Get(string sitecode, string iccid, string calledby, int status)
        {
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
                {
                    conn.Open();
                    var result = conn.Query<ODBOutRef>("crm_enable_disable_odb", new
                    {
                        @sitecode = sitecode,
                        @iccid = iccid,
                        @calledby = calledby,
                        @status = status
                    }, commandType: CommandType.StoredProcedure);

                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new List<ODBOutRef>() { new ODBOutRef { errcode = -1, errmsg = ex.Message } });
            }
        }
    }
}
