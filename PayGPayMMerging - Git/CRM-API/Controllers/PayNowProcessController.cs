﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NLog;
using Dapper;
using Payment.Cybersource;
using Newtonsoft.Json;
using CRM_API.Models._3rdParty.CTP;
using CRM_API.DB.Common;
using System.Configuration;
using Mandrill;
using Mandrill.Model;



namespace CRM_API.Controllers
{
    public class PayNowProcessController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        // GET api/paynowprocess
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        public HttpResponseMessage Get(string MobileNo, double OutstandingAmt, string updatedBy, string siteCode, string productCode)
        {
            try
            {
                return ProcessPayNow(MobileNo, OutstandingAmt, updatedBy, productCode, siteCode);
            }
            catch (WebException ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new List<CRM_API.Models.PAYMDetails>());
            }
        }

        public HttpResponseMessage Get(string firstname, string email, string sitecode)
        {
            List<ErrCodeMsg> resValues = new List<ErrCodeMsg>();
            ErrCodeMsg output = new ErrCodeMsg() { errcode = -1, errmsg = "Mail sent failure!" };
            try
            {
                var api = new MandrillApi(ConfigurationManager.AppSettings["MAILCHIMPAPIKEY"]);
                var message = new MandrillMessage();
                message.AddGlobalMergeVars("FNAME", firstname);
                message.AddTo(email);
                IList<MandrillSendMessageResponse> response = api.Messages.SendTemplateAsync(message, ConfigurationManager.AppSettings["MAILCHIMPSIMONLYEXCESSUSAGENOTREGISTEREDCUSTOMER_" + sitecode]).Result;
                Log.Info("ChargebackSuspension Mail Response : " + JsonConvert.SerializeObject(response));
                if (response != null && response.ElementAt(0).Status == 0)
                {
                    output.errcode = 0;
                    output.errmsg = "Mail sent successfully!";
                }
                resValues.Add(output);
            }
            catch (Exception ex)
            {
                Log.Error("ChargebackSuspension Mail Error : " + ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        // GET api/paynowprocess/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/paynowprocess
        public void Post([FromBody]string value)
        {
        }

        // PUT api/paynowprocess/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/paynowprocess/5
        public void Delete(int id)
        {
        }

        private HttpResponseMessage ProcessPayNow(string MobileNo, double OutstandingAmt, string updatedBy, string productCode, string siteCode)
        {
            System.Web.HttpContext.Current.Session["GlobalProduct"] = productCode;

            Log.Debug("ProcessPayNow: Started");

            List<CRM_API.DB.Common.ErrCodeMsg> errValues = new List<CRM_API.DB.Common.ErrCodeMsg>();
            int pay_status = 0;

            CRM_API.Helpers.Payment payment = new Helpers.Payment();

            var currency = CRM_API.DB.PAYMPlanBundles.SProc.GetCurrency(siteCode);
            var subscriptionList = CRM_API.DB.Payment.GetSubscribtionList(MobileNo);
            var Pay_Ref = "";

            if (subscriptionList != null && subscriptionList.Count() > 0)
            {
                Log.Debug("PayNowProcessController.ProcessPayNow: Charge Saved Card");
                //payment.CybersourceCharge(requestData.MobileNo, MobileNo, requestData.SiteCode, "VMUK", string.Format("PP{0:00}", requestData.newplan), requestData.SubscriptionID, currency, requestData.totalvalue);
                var pp_customer_id = CRM_API.DB.PMBODDOperations.GetCustomerID(MobileNo);

                if (pp_customer_id != null && pp_customer_id != "No-Customer")
                {
                    //Pay_Ref = string.Format("PMBO-DDPN{0:yyyyMMHHmmss}-{1}", DateTime.Now, pp_customer_id);
                    //payment.CybersourceCharge(
                    //    Pay_Ref,
                    //    MobileNo,
                    //    "MCM",
                    //    "PMBO",
                    //    Pay_Ref,
                    //    subscriptionList.FirstOrDefault().SubscriptionID,
                    //    currency,
                    //    OutstandingAmt);

                    double result;
                    string SubscrpID = subscriptionList.FirstOrDefault().SubscriptionID.ToString();

                    if (double.TryParse(subscriptionList.FirstOrDefault().SubscriptionID, out result)) // this condition for Cybersoure subscriber avoid 
                    {
                        errValues.Add(new DB.Common.ErrCodeMsg() { errcode = -1, errmsg = string.Format("Payment Failed due to: {0}", "This Customer is not Available in RealEx !!!") });
                        if (subscriptionList.Count() == 2)
                        {
                            if (double.TryParse(subscriptionList.ElementAt(1).SubscriptionID, out result)) // this condition for Cybersoure subscriber avoid 
                            {
                                errValues.Add(new DB.Common.ErrCodeMsg() { errcode = -1, errmsg = string.Format("Payment Failed due to: {0}", "This Customer is not Available in RealEx !!!") });
                                return Request.CreateResponse(HttpStatusCode.NotFound, errValues.ToArray());
                            }
                            SubscrpID = subscriptionList.ElementAt(1).SubscriptionID.ToString();
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.NotFound, errValues.ToArray());
                        }
                    }
                    Pay_Ref = string.Format("PMBO-DDPN{0:yyyyMMHHmmss}-{1}-RX", DateTime.Now, pp_customer_id);
                    Models.Payment.CardDetails requestData = new Models.Payment.CardDetails();
                    payment.AuthorizeWithout3DS(requestData as Models.Payment.CardDetails, siteCode, MobileNo, "PMBO", "", currency, OutstandingAmt, MobileNo, SubscrpID, Pay_Ref, productCode);
                }
                else
                {
                    errValues.Add(new DB.Common.ErrCodeMsg() { errcode = -1, errmsg = string.Format("Payment Failed due to: {0}", "Customer Id not Available!") });
                    return Request.CreateResponse(HttpStatusCode.NotFound, errValues.ToArray());
                }
            }
            else
            {
                errValues.Add(new DB.Common.ErrCodeMsg() { errcode = -1, errmsg = string.Format("Payment Failed due to: {0}", "Subscription Id not Available!") });
                return Request.CreateResponse(HttpStatusCode.NotFound, errValues.ToArray());
            }

            DateTime end_date = DateTime.Now, start_date = DateTime.Now;
            if (payment.Decision == "ACCEPT" || payment.Decision == "APPROVAL")
            {
                Log.Debug("PayNowProcessController.ProcessPayNow: Payment Success");
                pay_status = 1;
                errValues.Add(new DB.Common.ErrCodeMsg() { errcode = 0, errmsg = string.Format("Payment Successfully Processed - {0}", Pay_Ref) });
            }
            else
            {
                Log.Error("PayNowProcessController.ProcessPayNow: Payment Failed");
                pay_status = 0;
                errValues.Add(new DB.Common.ErrCodeMsg() { errcode = -1, errmsg = string.Format("Payment Failed due to: {0}", payment.Description != "" ? payment.Description : payment.Decision) });
            }

            if (!string.IsNullOrEmpty(payment.MerchantReferenceCode))
            {
                //CRM_API.DB.PAYMPlanBundles.SProc.PaymentInsert(DateTime.Now, payment.MerchantReferenceCode, "credit or debit card", (float)OutstandingAmt, pay_status, updatedBy);
                CRM_API.DB.PAYMPayments.SProc.PaymentInsert(DateTime.Now, MobileNo, Pay_Ref, "credit or debit card", (float)OutstandingAmt, pay_status, updatedBy);
            }

            return Request.CreateResponse(HttpStatusCode.OK, errValues.ToArray());
        }

        //private HttpResponseMessage ProcessPayNow(string MobileNo, double OutstandingAmt, string updatedBy)
        //{
        //    Log.Debug("ProcessPayNow: Started");

        //    List<CRM_API.DB.Common.ErrCodeMsg> errValues = new List<CRM_API.DB.Common.ErrCodeMsg>();
        //    int pay_status = 0;

        //    CRM_API.Helpers.Payment payment = new Helpers.Payment();

        //    var currency = CRM_API.DB.PAYMPlanBundles.SProc.GetCurrency("UK");
        //    var subscriptionList = CRM_API.DB.Payment.GetSubscribtionList(MobileNo);

        //    if(subscriptionList !=null)
        //    {
        //        Log.Debug("PayNowProcessController.ProcessPayNow: Charge Saved Card");
        //        //payment.CybersourceCharge(requestData.MobileNo, MobileNo, requestData.SiteCode, "VMUK", string.Format("PP{0:00}", requestData.newplan), requestData.SubscriptionID, currency, requestData.totalvalue);
        //        var pp_customer_id = CRM_API.DB.PMBODDOperations.GetCustomerID(MobileNo);

        //        if (pp_customer_id != null)
        //            payment.CybersourceCharge(string.Format("PMBO-DDPN{0:yyyyMM}-{1}", DateTime.Now, pp_customer_id), MobileNo, "MCM", "PMBO", string.Format("PMBO-DDPN{0:yyyyMM}-{1}", DateTime.Now, pp_customer_id), subscriptionList.FirstOrDefault().SubscriptionID, currency, OutstandingAmt);
        //        else
        //        {
        //            errValues.Add(new DB.Common.ErrCodeMsg() { errcode = -1, errmsg = string.Format("Payment Failed due to: {0}", "Customer Id not Available!") });
        //            return Request.CreateResponse(HttpStatusCode.NotFound, errValues.ToArray());
        //        }
        //    }

        //    DateTime end_date = DateTime.Now, start_date = DateTime.Now;
        //    if (payment.Decision == "ACCEPT")
        //    {
        //        Log.Debug("PayNowProcessController.ProcessPayNow: Payment Success");
        //        pay_status = 1;
        //        errValues.Add(new DB.Common.ErrCodeMsg() { errcode = 0, errmsg = "Payment Successfully Processed!"});
        //    }
        //    else
        //    {
        //        Log.Error("PayNowProcessController.ProcessPayNow: Payment Failed");
        //        pay_status = 0;
        //        errValues.Add(new DB.Common.ErrCodeMsg() { errcode = -1, errmsg = string.Format("Payment Failed due to: {0}", payment.Description) });
        //    }

        //    if (!string.IsNullOrEmpty(payment.MerchantReferenceCode))
        //    {
        //       // CRM_API.DB.PAYMPlanBundles.SProc.PaymentInsert(DateTime.Now, payment.MerchantReferenceCode, "credit or debit card", (float) OutstandingAmt, pay_status, updatedBy);
        //    }

        //    return Request.CreateResponse(HttpStatusCode.OK, errValues.ToArray());
        //}

    }
}

