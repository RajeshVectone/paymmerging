﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NLog;
using System.Text;
using System.IO;

namespace CRM_API.Controllers
{
    public class BlockandUnblockController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
       
        // POST api/HappyBundle/
        [HttpPost]
        public HttpResponseMessage Post(CRM_API.Models.BlockUnBlockModels.BlockUnblock resvalue)
        {
            DB.Common.ErrCodeMsg errResult = new DB.Common.ErrCodeMsg()
           {
               errcode = -1,
               errsubject = "General Error",
               errmsg = "Empty New History"
           };
            var result = new CRM_API.Models.BlockUnBlockModels.BlockUnblock();
            switch (resvalue.Block_Type)
            {
                
                case Models.BlockUnBlockModels.BlockUnBlockmode.type:
                    return Getverifyblockunblockaccount(resvalue);
                case Models.BlockUnBlockModels.BlockUnBlockmode.BlockUnblock:
                    return Getblockunblockaccount(resvalue);
                case Models.BlockUnBlockModels.BlockUnBlockmode.Reports:
                    return Getblockunblockreports(resvalue);              
                default: break;
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

       
        HttpResponseMessage Getverifyblockunblockaccount(CRM_API.Models.BlockUnBlockModels.BlockUnblock resvalue)
        {
            IEnumerable<CRM_API.Models.BlockUnBlockModels.BlockUnblock> resValues =
                           CRM_API.DB.BlockUnBlockModels.SProc.Getverifybothblockandunblock(resvalue.Process_By, resvalue.product, resvalue.sitecode, resvalue.type, resvalue.mode, resvalue.reason, resvalue.account_info);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.HappyBundle.HappyBundlelist>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        HttpResponseMessage Getblockunblockaccount(CRM_API.Models.BlockUnBlockModels.BlockUnblock resvalue)
        {
            IEnumerable<CRM_API.Models.BlockUnBlockModels.BlockUnblock> resValues =
                           CRM_API.DB.BlockUnBlockModels.SProc.GetbothblockandunblockSP(resvalue.Process_By, resvalue.product, resvalue.sitecode, resvalue.type, resvalue.mode, resvalue.reason, resvalue.account_info);
            if (resValues.Count() > 0)
            {
                Helpers.SendSMS sms = new Helpers.SendSMS();
                if (resValues.FirstOrDefault().errcode == 0)
                {
                    foreach (CRM_API.Models.BlockUnBlockModels.BlockUnblock resValuesloop in resValues)
                    {
                        string mode;
                        int node_type;
                        mode = "http";
                        node_type = 1;
                        string urll;
                        string vlr_number;
                        string imsi_active;
                        imsi_active = resValuesloop.imsi_active;
                        vlr_number = resValuesloop.vlr_number;
                        urll = resValuesloop.url;
                        string postData = "data-vlr-number=" + vlr_number + "&data-imsi=" + imsi_active + "&mode=" + mode + "&data-node-type="+node_type;

                        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(urll);
                        request.Method = "POST";
                        request.ContentType = "application/x-www-form-urlencoded";
                        ASCIIEncoding enc = new ASCIIEncoding();
                        byte[] data = enc.GetBytes(postData);
                        request.ContentLength = data.Length;
                        Stream newStream = request.GetRequestStream();
                        newStream.Write(data, 0, data.Length);
                        newStream.Close();
                        HttpWebResponse smsRes = (HttpWebResponse)request.GetResponse();
                        Stream resStream = smsRes.GetResponseStream();
                        Log.Debug("Cancel Location Success");
                        Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                        StreamReader readStream = new StreamReader(resStream, encode);
                    }
                }
            }
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.HappyBundle.HappyBundlelist>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        HttpResponseMessage Getblockunblockreports(CRM_API.Models.BlockUnBlockModels.BlockUnblock resvalue)
        {
            IEnumerable<CRM_API.Models.BlockUnBlockModels.BlockUnBlockReports> resValues =
                           CRM_API.DB.BlockUnBlockModels.SProc.Getbothblockandunblockreports(resvalue.product , resvalue.mode, resvalue.StartDate, resvalue.EndDate);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.HappyBundle.HappyBundlelist>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

       
    }
}
