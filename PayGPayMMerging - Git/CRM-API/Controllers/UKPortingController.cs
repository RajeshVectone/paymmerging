﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace CRM_API.Controllers
{
    /// <summary>
    /// Web Api Porting  For Country United Kingdom
    /// </summary>
    public class UKPortingController : ApiController
    {
        // POST api/ukporting
        /// <summary>
        /// POST api/ukporting
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        public async Task<HttpResponseMessage> AsyncPost(string Id, Models.Porting.UKSubmitEntry Model)
        {
            try
            {
                if (!Id.ToLower().Equals("async"))
                    throw new MissingMemberException("is not async request");
                switch (Model.SubType)
                {
                    case Models.Porting.PortingStep.ReportSMS:
                        return await Async_GetReportSMS(Model);
                }
                return Request.CreateResponse(HttpStatusCode.OK, Model);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e);
            }
        }
        public HttpResponseMessage Post(Models.Porting.UKSubmitEntry Model)
        {
            try
            {
                switch (Model.SubType)
                {
                    case Models.Porting.PortingStep.Validate:
                        return Portin_Validate(Model);
                    case Models.Porting.PortingStep.Submit:
                        return DoSubmitEntry(Model);
                    case Models.Porting.PortingStep.CheckExistingRequest:
                        return CheckExistingRequest(Model);
                    case Models.Porting.PortingStep.Cancel:
                        return DoCancelRequest(Model);
                    case Models.Porting.PortingStep.Revise:
                        return DoReviseRequest(Model);
                    case Models.Porting.PortingStep.ReportSMS:
                        return GetReportSMS(Model);
                    case Models.Porting.PortingStep.ReportSMSDetail:
                        return GetReportSMSDetail(Model);
                    case Models.Porting.PortingStep.ReportSMSDetailByMsisdn:
                        return GetReportSMSDetailByMsisdn(Model);
                    case Models.Porting.PortingStep.ReportSMSDetail_Cancel:
                        return ReportSMSDetail_DoCancelRequest(Model);
                    case Models.Porting.PortingStep.ReportSMSDetail_Submit:
                        return ReportSMSDetail_DoSubmitRequest(Model);
                    case Models.Porting.PortingStep.TestingStep:
                        return GetTestingStep(Model);
                    case Models.Porting.PortingStep.SendSMSManual:
                        return DoSendSMS(Model);
                    default:
                        Model.errcode = -1;
                        Model.errmsg = "Empty Parameter [SubType]";
                        Model.errsubject = "General Error";
                        break;
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed,
                    new Models.Common.ErrCodeMsg()
                    {
                        errcode = -1,
                        errmsg = e.Message,
                        errsubject = "General Error"
                    });
            }
            return Request.CreateResponse(HttpStatusCode.OK, Model);
        }
        private static void SendSMS_Manual(Models.Porting.UKSubmitEntry In, ref Models.Common.ErrCodeMsg result,
            string portin_msisdn, string bb_msisdn, DateTime port_date)
        {
            int brand;
            int? _isUkBrand = null;
            string product = "{PRODUCT}";
            string smsMessage = string.Empty;
            if (DB.Porting.SProc.BrandIsUk1(In.Msisdn, In.Sitecode, ref _isUkBrand))
            {
                brand = (int)Models.Porting.PortingBrand.UK1Brand;
                smsMessage = DB.Porting.SProc.GetSMSContent("APPROVE_PI_CALL_CTR_UK1", In.Sitecode);
                if (string.IsNullOrEmpty(smsMessage))
                {
                    //throw new Exception("UK1 SMS content not found");
                    result.errcode = 0;
                    result.errmsg = "UK1 SMS content not found";
                }
                smsMessage = smsMessage.Replace("{MSISDN}", portin_msisdn);
                smsMessage = smsMessage.Replace("{PORT_DATE}", port_date.ToString("dd/MM/yy"));
            }
            else
            {
                smsMessage = DB.Porting.SProc.GetSMSContent("APPROVE_PI_CALL_CTR", In.Sitecode);
                DB.Porting.SProc.BrandIsUk1(bb_msisdn.Trim(), In.Sitecode, ref _isUkBrand);
                brand = _isUkBrand.Value;
                if (string.IsNullOrEmpty(smsMessage) || brand == (int)Models.Porting.PortingBrand.UnknownBrand)
                {
                    result.errcode = 0;
                    result.errmsg = "Port In Submitted, with Sms Content Not Found";
                }
                smsMessage = smsMessage.Replace("{MSISDN}", portin_msisdn);
                string simType = "{SIM_TYPE}";
                if (brand == (int)Models.Porting.PortingBrand.VectoneBrand)
                    simType = "Vectone SIM";
                else if (brand == (int)Models.Porting.PortingBrand.DelightBrand)
                    simType = "Delight SIM";

                smsMessage = smsMessage.Replace("{SIM_TYPE}", simType);
                smsMessage = smsMessage.Replace("{PORT_DATE}", port_date.ToString("dd/MM/yy"));


                if (brand == (int)Models.Porting.PortingBrand.VectoneBrand)
                    product = "Vectone Mobile";
                else if (brand == (int)Models.Porting.PortingBrand.DelightBrand)
                    product = "Delight Mobile";

                smsMessage = smsMessage.Replace("{PRODUCT}", product);
            }
            if (portin_msisdn.IndexOf("00") == 0)
                portin_msisdn = "44" + portin_msisdn.Substring(2);
            else if (portin_msisdn.IndexOf("0") == 0)
                portin_msisdn = "44" + portin_msisdn.Substring(1);

            var smsResp = new Helpers.SendSMS().Send(false, portin_msisdn, product, smsMessage, 0);
            result.errmsg += ": " + smsResp.ToString();
            result.errmsg = "Request porting for MSISDN " + portin_msisdn + " has been approved." +
        "The MSISDN will be ported at " + port_date.ToString("MMMM dd, yyyy") +
         ". This info will also sent via SMS to customer's mobile phone.";
        }
        private static void SendSMS_SubmitPortin(Models.Porting.UKSubmitEntry In, ref Models.Common.ErrCodeMsg result,
            Models.Porting.Syniverse.SubmitRequest reqObj, Models.Porting.Syniverse.SubmitRequestResp resObj)
        {
            try
            {
                int? brand;
                string bbIccid = string.Empty;
                int? _isUkBrand = null;
                string product = string.Empty;
                string smsMessage = string.Empty;
                if (DB.Porting.SProc.BrandIsUk1(In.Msisdn, In.Sitecode, ref _isUkBrand))
                {
                    brand = (int)Models.Porting.PortingBrand.UK1Brand;
                    smsMessage = DB.Porting.SProc.GetSMSContent("APPROVE_PI_CALL_CTR_UK1", In.Sitecode);
                    if (string.IsNullOrEmpty(smsMessage))
                    {
                        throw new Exception("UK1 SMS content not found");
                        //result.errcode = 0;
                        //result.errmsg = "UK1 SMS content not found";
                    }
                    smsMessage = smsMessage.Replace("{MSISDN}", reqObj.EntryKey.Msisdn);
                    smsMessage = smsMessage.Replace("{PORT_DATE}", resObj.PortDate.ToString("dd/MM/yy"));
                }
                else
                {
                    smsMessage = DB.Porting.SProc.GetSMSContent("APPROVE_PI_CALL_CTR", In.Sitecode);
                    DB.Porting.SProc.BrandIsUk1(reqObj.EntryKey.BbMsisdn.Trim(), In.Sitecode, ref _isUkBrand);
                    brand = DB.Porting.SProc.GetBrand(reqObj.EntryKey.BbMsisdn.Trim(), In.Sitecode, ref bbIccid);
                    if (string.IsNullOrEmpty(smsMessage) || brand == (int)Models.Porting.PortingBrand.UnknownBrand)
                    {
                        throw new Exception("Sms content not found or brand is unknown");
                        //result.errcode = 0;
                        //result.errmsg = "Port In Submitted, with Sms Content Not Found";
                    }
                    smsMessage = smsMessage.Replace("{MSISDN}", reqObj.EntryKey.Msisdn);
                    string simType = "{SIM_TYPE}";
                    if (brand == (int)Models.Porting.PortingBrand.VectoneBrand)
                        simType = "Vectone SIM";
                    else if (brand == (int)Models.Porting.PortingBrand.DelightBrand)
                        simType = "Delight SIM";

                    smsMessage = smsMessage.Replace("{SIM_TYPE}", simType);
                    smsMessage = smsMessage.Replace("{PORT_DATE}", resObj.PortDate.ToString("dd/MM/yy"));


                    if (brand == (int)Models.Porting.PortingBrand.VectoneBrand)
                        product = "Vectone Mobile";
                    else if (brand == (int)Models.Porting.PortingBrand.DelightBrand)
                        product = "Delight Mobile";

                    smsMessage = smsMessage.Replace("{PRODUCT}", product);
                }
                //var smsResp = new Helpers.SendSMS().Send(false, reqObj.EntryKey.Msisdn, product, smsMessage, 0);
                var smsResp = new Helpers.SendSMS().Send_SMSPorting(brand ?? -1, reqObj.EntryKey.Msisdn, smsMessage,In.Sitecode);
                result.errmsg += ": " + smsResp.ToString();
                result.errmsg = "Request porting for MSISDN " + reqObj.EntryKey.Msisdn + " has been approved." +
            "The MSISDN will be ported at " + resObj.PortDate.ToString("MMMM dd, yyyy") +
             ". This info will also sent via SMS to customer's mobile phone.";
            }
            catch (Exception e)
            {
                result.errcode = -1;
                result.errmsg = "An error was encountered when sending SMS to the customer: " + e.Message;
            }
        }
        private static void SendSMS_SubmitPortinBySMS(Models.Porting.UKSubmitEntry In, ref Models.Common.ErrCodeMsg result,
           Models.Porting.Syniverse.SubmitRequest reqObj, Models.Porting.Syniverse.SubmitRequestResp resObj)
        {
            try
            {
                string smsMessage = string.Empty;
                string bbIccid = string.Empty;
                int? brand;
                string product = string.Empty;

                smsMessage = DB.Porting.SProc.GetSMSContent("APPROVE_PI_SMS", In.Sitecode);
                brand = DB.Porting.SProc.GetBrand(In.Msisdn, In.Sitecode, ref bbIccid);
                if (string.IsNullOrEmpty(smsMessage) || brand == (int)Models.Porting.PortingBrand.UnknownBrand)
                {
                    throw new Exception("Sms content not found or brand is unknown");
                    //result.errcode = 0;
                    //result.errmsg = "Port In Submitted, with Sms Content Not Found";
                }
                smsMessage = smsMessage.Replace("{MSISDN}", reqObj.EntryKey.Msisdn);
                string simType = "{SIM_TYPE}";
                if (brand == (int)Models.Porting.PortingBrand.VectoneBrand)
                    simType = "Vectone SIM";
                else if (brand == (int)Models.Porting.PortingBrand.DelightBrand)
                    simType = "Delight SIM";

                smsMessage = smsMessage.Replace("{SIM_TYPE}", simType);
                smsMessage = smsMessage.Replace("{PORT_DATE}", resObj.PortDate.ToString("dd/MM/yy"));


                if (brand == (int)Models.Porting.PortingBrand.VectoneBrand)
                    product = "Vectone Mobile";
                else if (brand == (int)Models.Porting.PortingBrand.DelightBrand)
                    product = "Delight Mobile";

                smsMessage = smsMessage.Replace("{PRODUCT}", product);
                var smsResp = new Helpers.SendSMS().Send_SMSPorting(brand ?? -1 , reqObj.EntryKey.Msisdn, smsMessage,In.Sitecode);

                var resx = DB.Porting.SProc.UpdatePortInSmsStatus(In.SmsId, 100, In.Sitecode);
                result.errmsg += ": " + smsResp.ToString();
                result.errmsg = "Request porting for MSISDN " + reqObj.EntryKey.Msisdn + " has been approved." +
            "The MSISDN will be ported at " + resObj.PortDate.ToString("MMMM dd, yyyy") +
             ". This info will also sent via SMS to customer's mobile phone.";

            }
            catch (Exception e)
            {
                result.errcode = -1;
                result.errmsg = "An error was encountered when sending SMS to the customer: " + e.Message;
            }
        }
        private HttpResponseMessage CheckExistingRequest(Models.Porting.UKSubmitEntry In)
        {
            Models.Porting.ExistingRequest result = CRM_API.DB.Porting.SProc.CheckExistingRequest(In.PAC, In.Msisdn, In.Sitecode);
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
        private HttpResponseMessage DoCancelRequest(Models.Porting.UKSubmitEntry In)
        {
            int counter = 0;
            Models.Porting.CancelRequest result = new Models.Porting.CancelRequest();
            try
            {
            Retry:
                string reqCode = DB.Porting.SProc.GetRequestCode(In.Sitecode);
                string sessionId = Helpers.Porting.Syniverse.Logon();
                Models.Porting.Syniverse.CancelRequest reqObj = Helpers.Porting.Syniverse.CancelRequest(In.PAC.Trim(), In.Msisdn.Trim(), In.PortDate, In.ContactName, In.ContactDetail, reqCode);
                string reqXml = CRM_API.Helpers.Porting.XmlAdapter.CreateCookieXml(sessionId) +
                    CRM_API.Helpers.Porting.XmlAdapter.CreateCancelRequestXml(reqObj);
                string resXml = CRM_API.Helpers.Porting.XmlAdapter.send_xml(reqXml);
                #region if error
                if (resXml.Contains("error"))
                {
                    CRM_API.Models.Porting.Syniverse.Error errObj = Helpers.Porting.XmlAdapter.ReceiveErrorXml(resXml);
                    if ((errObj.ErrorCode == 1601 || errObj.ErrorCode == 999901) && counter < 3)
                    {
                        sessionId = Helpers.Porting.Syniverse.Logon();
                        //HttpContent.Session["sessionId"] = sessionId;
                        counter++;
                        goto Retry;
                    }
                    else
                    {
                        result.errcode = errObj.ErrorCode;
                        result.errmsg = String.Format("{0} (Syniverse error code {1})", errObj.Detail[1], errObj.ErrorCode.ToString());
                    }
                    DB.Porting.SProc.SaveTransactionLog(In.PAC, In.PortinMsisdn, "(RNO)CancelRequest", result.errcode, null,
                       CRM_API.Helpers.Config.NetworkOperatorCode, null, In.Sitecode
                       );
                }
                #endregion
                #region if success
                else
                {
                    CRM_API.Models.Porting.Syniverse.CancelRequestResp resObj = CRM_API.Helpers.Porting.XmlAdapter.ReceiveCancelRequestRespXml(resXml);
                    DB.Porting.SProc.SaveTransactionLog(reqObj.EntryKey.Pac, reqObj.EntryKey.Msisdn,
                        "(RNO)CancelRequest", 0, null, Helpers.Config.NetworkOperatorCode, null, In.Sitecode);
                    result = CRM_API.DB.Porting.SProc.SaveCancelRequestResp(reqObj, resObj, In.Sitecode);
                }
                #endregion
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new Models.Common.ErrCodeMsg()
                {
                    errcode = -1,
                    errmsg = ex.Message
                });
            }
        }
        private HttpResponseMessage DoReviseRequest(Models.Porting.UKSubmitEntry In)
        {
            int counter = 0;
            DB.Common.ErrCodeMsg result = new DB.Common.ErrCodeMsg()
            {
                errcode = 0,
                errmsg = "Portin Number Revised"
            };
            try
            {
            Retry:
                string reqCode = DB.Porting.SProc.GetRequestCode(In.Sitecode);
                string sessionId = Helpers.Porting.Syniverse.Logon();
                Models.Porting.Syniverse.ReviseRequest reqObj = Helpers.Porting.Syniverse.CreateReviseRequest(In.PAC.Trim(), In.Msisdn.Trim(), In.PortDate, In.ContactName, In.ContactDetail, reqCode);
                string reqXml = CRM_API.Helpers.Porting.XmlAdapter.CreateCookieXml(sessionId) +
                    CRM_API.Helpers.Porting.XmlAdapter.CreateReviseRequestXml(reqObj);
                string resXml = CRM_API.Helpers.Porting.XmlAdapter.send_xml(reqXml);
                #region if error
                if (resXml.Contains("error"))
                {
                    CRM_API.Models.Porting.Syniverse.Error errObj = Helpers.Porting.XmlAdapter.ReceiveErrorXml(resXml);
                    if ((errObj.ErrorCode == 1601 || errObj.ErrorCode == 999901) && counter < 3)
                    {
                        sessionId = Helpers.Porting.Syniverse.Logon();
                        //HttpContent.Session["sessionId"] = sessionId;
                        counter++;
                        goto Retry;
                    }
                    else
                    {
                        result.errcode = errObj.ErrorCode;
                        result.errmsg = String.Format("{0} (Syniverse error code {1})", errObj.Detail[1], errObj.ErrorCode.ToString());
                    }
                    DB.Porting.SProc.SaveTransactionLog(In.PAC, In.PortinMsisdn, "(RNO)ReviseRequest", result.errcode, null,
                       CRM_API.Helpers.Config.NetworkOperatorCode, null, In.Sitecode
                       );
                }
                #endregion
                #region if success
                else
                {
                    CRM_API.Models.Porting.Syniverse.ReviseRequestResp resObj = CRM_API.Helpers.Porting.XmlAdapter.ReceiveReviseRequestRespXml(resXml);
                    DB.Porting.SProc.SaveTransactionLog(reqObj.EntryKey.Pac, reqObj.EntryKey.Msisdn,
                        "(RNO)ReviseRequest", 0, null, Helpers.Config.NetworkOperatorCode, null, In.Sitecode);
                    result = CRM_API.DB.Porting.SProc.SaveReviseRequestResp(reqObj, resObj, In.Sitecode);
                }
                #endregion
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new Models.Common.ErrCodeMsg()
                {
                    errcode = -1,
                    errmsg = ex.Message
                });
            }
        }
        private HttpResponseMessage DoSendSMS(Models.Porting.UKSubmitEntry In)
        {
            var result = new Models.Common.ErrCodeMsg() { errcode = 0, errmsg = "Success Send SMS." };
            SendSMS_Manual(In, ref result, In.PortinMsisdn, In.Msisdn, In.PortDate);
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
        private HttpResponseMessage DoSubmitEntry(Models.Porting.UKSubmitEntry In, string Methodby = "Default")
        {
            int counter = 0;
            Models.Common.ErrCodeMsg result = new Models.Common.ErrCodeMsg()
            {
                errcode = 0,
                errmsg = "Portin Number Submitted"
            };
            try
            {
            Retry:
                string reqCode = DB.Porting.SProc.GetRequestCode(In.Sitecode);
                string sessionId = Helpers.Porting.Syniverse.Logon();
                Models.Porting.Syniverse.SubmitRequest reqObj = Helpers.Porting.Syniverse.CreateSubmitRequest(
                    In.PAC.Trim(), In.PortinMsisdn.Trim(), In.PortDate, In.ContactName, In.ContactDetail, In.LoggedUser.Trim(),
                    "(CRM3)Submitted via Call Centre at " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                    In.Msisdn.Trim(), reqCode
                    );
                string reqXml = CRM_API.Helpers.Porting.XmlAdapter.CreateCookieXml(sessionId) +
                    CRM_API.Helpers.Porting.XmlAdapter.CreateSubmitRequestXml(reqObj);
                string resXml = CRM_API.Helpers.Porting.XmlAdapter.send_xml(reqXml);

                #region If error
                if (resXml.Contains("error"))
                {
                    CRM_API.Models.Porting.Syniverse.Error errObj = Helpers.Porting.XmlAdapter.ReceiveErrorXml(resXml);
                    if ((errObj.ErrorCode == 1601 || errObj.ErrorCode == 999901) && counter < 3)
                    {
                        sessionId = Helpers.Porting.Syniverse.Logon();
                        //HttpContent.Session["sessionId"] = sessionId;
                        counter++;
                        goto Retry;
                    }
                    else if (errObj.ErrorCode == 307) // port date too late, message from syniverse isn't user friendly
                    {
                        result.errcode = errObj.ErrorCode;
                        result.errmsg = "The PAC and MSISDN supplied matches a current entry on the web site, but the PAC already expired at the submitted port date. (Syniverse error code 307)";
                    }
                    else
                    {

                        result.errcode = errObj.ErrorCode;
                        result.errmsg = String.Format("{0} (Syniverse error code {1})", errObj.Detail[1], errObj.ErrorCode.ToString());
                    }
                    DB.Porting.SProc.SaveTransactionLog(In.PAC, In.PortinMsisdn, "(RNO)SubmitRequest", result.errcode, null,
                        CRM_API.Helpers.Config.NetworkOperatorCode, null, In.Sitecode
                        );
                }
                #endregion
                #region If success
                else
                {
                    var resObj = CRM_API.Helpers.Porting.XmlAdapter.ReceiveSubmitRequestRespXml(resXml);
                    var repObj = Helpers.Porting.Syniverse.CreateSubmitRequestReport(reqObj.EntryKey.Pac, reqObj.EntryKey.Msisdn, reqCode);

                    string repXml = CRM_API.Helpers.Porting.XmlAdapter.CreateCookieXml(sessionId) + CRM_API.Helpers.Porting.XmlAdapter.CreateReportXml(repObj);
                    var repResObj = Helpers.Porting.XmlAdapter.ReceiveReportRespXml(CRM_API.Helpers.Porting.XmlAdapter.send_report_xml(repXml));
                    try
                    {
                        resObj.DnoCode = repResObj.ReportRecord.FirstOrDefault().DnoCode;
                    }
                    catch
                    {
                        resObj.DnoCode = "";
                    }
                    DB.Porting.SProc.SaveTransactionLog(reqObj.EntryKey.Pac, reqObj.EntryKey.Msisdn,
                        "(RNO)SubmitRequest", 0, resObj.DnoCode, Helpers.Config.NetworkOperatorCode, null, In.Sitecode);
                    DB.Porting.SProc.SaveSubmitRequestResp(repObj.Pac, repObj.Msisdn, reqObj.EntryKey.BbMsisdn, reqObj.UseSuggestedDateOnConflict ? 1 : 0,
                        resObj.PortDateWarning ? 1 : 0, resObj.PortDate, resObj.DnoCode, In.errcode, reqObj.SpOptionalData.PrivateData.AccNo,
                        reqObj.SpOptionalData.PrivateData.Ref, reqObj.SpOptionalData.PrivateData.SpId,
                        reqObj.SpOptionalData.ContactInfo.ContactName, reqObj.SpOptionalData.ContactInfo.ContactDetails,
                        1, In.errmsg, In.Sitecode);
                    if (Methodby.ToLower() == "sms")
                        SendSMS_SubmitPortinBySMS(In, ref result, reqObj, resObj);
                    else
                        SendSMS_SubmitPortin(In, ref result, reqObj, resObj);
                }
                #endregion
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new Models.Common.ErrCodeMsg()
                {
                    errcode = -1,
                    errmsg = e.Message
                });
            }
        }
        private HttpResponseMessage GetReportSMS(Models.Porting.UKSubmitEntry In)
        {
            var result = DB.Porting.SProc.ReportPortInSms((int)In.SMSReportFilter, In.Sitecode);
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
        private async Task<HttpResponseMessage> Async_GetReportSMS(Models.Porting.UKSubmitEntry In)
        {
            var result = DB.Porting.SProc.ReportPortInSms_Async((int)In.SMSReportFilter, In.Sitecode);
            return Request.CreateResponse(HttpStatusCode.OK, await result);
        }
        private HttpResponseMessage GetReportSMSDetail(Models.Porting.UKSubmitEntry In)
        {
            var result = DB.Porting.SProc.GetPortInSmsById(In.SmsId, In.Sitecode);
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
        private HttpResponseMessage GetReportSMSDetailByMsisdn(Models.Porting.UKSubmitEntry In)
        {
            var result = DB.Porting.SProc.GetPortInSmsByMsisdn(In.PortinMsisdn,In.PAC, In.Sitecode);
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
        private HttpResponseMessage GetTestingStep(Models.Porting.UKSubmitEntry In)
        {
            int? isUkBrand = null;
            var result = new KeyValuePair<string, bool>("IsUkBrand", DB.Porting.SProc.BrandIsUk1(In.Msisdn, In.Sitecode, ref isUkBrand));
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
        private HttpResponseMessage Portin_Validate(Models.Porting.UKSubmitEntry In)
        {
            var result = CRM_API.DB.Porting.SProc.MsisdnValidate(In.Msisdn, In.LastDigitIccid, In.Sitecode);
            if (result.errcode == 0)
                result.errmsg = "Valid";
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
        private HttpResponseMessage ReportSMSDetail_DoCancelRequest(Models.Porting.UKSubmitEntry In)
        {
            var result = DB.Porting.SProc.UpdatePortInSmsStatus(In.SmsId, 1, In.Sitecode);
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
        private HttpResponseMessage ReportSMSDetail_DoSubmitRequest(Models.Porting.UKSubmitEntry In)
        {
            In.ContactDetail = string.Empty;
            In.ContactName = string.Empty;
            return DoSubmitEntry(In,"SMS");;
        }
    }
}
