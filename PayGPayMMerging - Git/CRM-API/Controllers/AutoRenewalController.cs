﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Mandrill.Model;
using Newtonsoft.Json;
using NLog;

namespace CRM_API.Controllers
{
    public class AutoRenewalController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        //POST api/AutoRenewal/
        public HttpResponseMessage Post(CRM_API.Models.AutoRenewal.AutoRenewalInfo Model)
        {
            switch (Model.ModelType)
            {
                case CRM_API.Models.AutoRenewal.AutoRenewalType.GetAutoRenwalInfo:
                    return GetAutoRenwalInfo(Model);
                case CRM_API.Models.AutoRenewal.AutoRenewalType.EnableDisableProcess:
                    return EnableDisableProcess(Model);
                default:
                    return Request.CreateResponse(HttpStatusCode.OK, new CRM_API.Models.AutoRenewal.Output { errcode = -1, errmsg = "Failure" });
            }
        }

        private HttpResponseMessage GetAutoRenwalInfo(CRM_API.Models.AutoRenewal.AutoRenewalInfo Model)
        {
            CRM_API.Models.AutoRenewal.AutoRenewalOutput resultValues = CRM_API.DB.AutoRenewal.AutoRenewal.GetAutoRenwalInfo(Model.mobileno, Model.sitecode).FirstOrDefault();
            return Request.CreateResponse(HttpStatusCode.OK, resultValues);
        }

        private HttpResponseMessage EnableDisableProcess(CRM_API.Models.AutoRenewal.AutoRenewalInfo Model)
        {
            CRM_API.Models.AutoRenewal.Output resultValues = CRM_API.DB.AutoRenewal.AutoRenewal.EnableDisableProcess(Model.mobileno, Model.iccid, Model.crm_user, Model.processflag, Model.sitecode).FirstOrDefault();
            //Email Trigger
            if (Model.processflag == 1 && resultValues != null && resultValues.errcode == 0)
            {
                if (Model.sitecode == "MCM" || Model.sitecode == "BAU")
                {
                    try
                    {
                        var api = new Mandrill.MandrillApi(ConfigurationManager.AppSettings["MAILCHIMPAPIKEY"]);
                        var message = new MandrillMessage();
                        message.AddGlobalMergeVars("FNAME", Model.firstname);
                        message.AddGlobalMergeVars("CURRENTDATE", resultValues.cur_date);
                        message.AddGlobalMergeVars("CURRENTTIME", resultValues.cur_time);
                        message.AddGlobalMergeVars("EXPIRYDATE", resultValues.exp_date);
                        message.AddTo(Model.email);
                        IList<MandrillSendMessageResponse> response = api.Messages.SendTemplateAsync(message, ConfigurationManager.AppSettings["MAILCHIMPAUTORENEWALDISABLEDBYCUSTOMER_" + Model.sitecode]).Result;
                        Log.Info("AutoRenewal Mail Response : " + JsonConvert.SerializeObject(response));
                    }
                    catch (Exception ex)
                    {
                        Log.Error("AutoRenewal Mail Error : " + ex.Message);
                    }
                    //SMS Trigger
                    try
                    {
                        string smsURL = CRM_API.DB.SMSCampaign.SProc.GetSMSGatewayInfo(Model.sitecode, Model.mobileno);
                        var smsResponse = CRM_API.Helpers.SendSMS.SMSSendOurGateway("Vectone", Model.mobileno, resultValues.errmsg, smsURL);
                        CRM_API.DB.SMSCampaign.SProc.InsertSMSNotifyText(Model.mobileno, resultValues.errmsg, Model.crm_user, smsResponse == "-1" ? 0 : 1, "-", Model.firstname, "Disabled auto-renewal");
                        Log.Info("AutoRenewal : SMS : Response : " + smsResponse);
                    }
                    catch (Exception ex)
                    {
                        Log.Error("AutoRenewal SMS Error : " + ex.Message);
                    }
                }
            }
            else if (Model.processflag == 2 && resultValues != null && resultValues.errcode == 0)
            {
                if (Model.sitecode == "MCM")
                {
                    //SMS Trigger
                    try
                    {
                        string smsURL = CRM_API.DB.SMSCampaign.SProc.GetSMSGatewayInfo(Model.sitecode, Model.mobileno);
                        var smsResponse = CRM_API.Helpers.SendSMS.SMSSendOurGateway("Vectone", Model.mobileno, ConfigurationManager.AppSettings["SMSMESSAGEAUTORENEWALENABLEDBYCUSTOMER"], smsURL);
                        CRM_API.DB.SMSCampaign.SProc.InsertSMSNotifyText(Model.mobileno, resultValues.errmsg, Model.crm_user, smsResponse == "-1" ? 0 : 1, "-", Model.firstname, "Enabled auto-renewal");
                        Log.Info("AutoRenewal : SMS : Response : " + smsResponse);
                    }
                    catch (Exception ex)
                    {
                        Log.Error("AutoRenewal SMS Error : " + ex.Message);
                    }
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, resultValues);
        }
    }
}