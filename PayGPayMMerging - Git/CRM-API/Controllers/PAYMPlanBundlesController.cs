﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using NLog;
using System.IO;
using System.Net.Mail;
using Mandrill;
using System.Configuration;
using Mandrill.Model;
using Newtonsoft.Json;
using CRM_API.DB.Common;
using CRM_API.Models;

namespace CRM_API.Controllers
{
    public class PAYMPlanBundlesController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        // GET api/PAYMPlanBundles/MobileNo
        public HttpResponseMessage Get(string MobileNo)
        {
            Log.Debug("--------------------------------------------------------------------");
            return GetPlanList(new Models.PAYMPlanBundleRequest() { MobileNo = MobileNo, CalledBy = "CRM_API_GET" });
        }

        // POST api/PAYMPlanBundles/
        [HttpPost]
        public HttpResponseMessage POST(CRM_API.Models.PAYMPlanBundleRequest requestData)
        {
            Log.Debug("--------------------------------------------------------------------");

            DB.Common.ErrCodeMsg errResult = new DB.Common.ErrCodeMsg()
            {
                errcode = -1,
                errsubject = "General Error",
                errmsg = "Empty New Order"
            };

            Log.Debug("PAYMPlanBundlesController.Post: Started");
            Log.Info(new JavaScriptSerializer().Serialize(requestData));
            if (requestData != null)
            {
                if (string.IsNullOrEmpty(requestData.CalledBy))
                {
                    requestData.CalledBy = "CRM_API_POST";
                }
                //31-Jul-2015 : Moorthy Added
                System.Web.HttpContext.Current.Session["GlobalProduct"] = requestData.ProductCode;
                switch (requestData.requestType)
                {
                    case Models.PAYMPlanBundleRequestType.PlanList:
                        return GetPlanList(requestData);
                    case Models.PAYMPlanBundleRequestType.ListPlanDetails:
                        return GetPlanDetails(requestData);
                    case Models.PAYMPlanBundleRequestType.ListPlanHistory:
                        return GetPlanHistory(requestData);
                    case Models.PAYMPlanBundleRequestType.ListPlanRef:
                        return GetPlanRef(requestData);
                    case Models.PAYMPlanBundleRequestType.GetPlanDetail:
                        return GetPlanDetail(requestData);
                    case Models.PAYMPlanBundleRequestType.GetBundleDetail:
                        return GetBundleDetail(requestData);
                    case Models.PAYMPlanBundleRequestType.GetAccountBalance:
                        return GetAccountBalance(requestData);
                    case Models.PAYMPlanBundleRequestType.RequestChangeLowerPlan:
                        return DoRequestChangeLowerPlan(requestData);
                    case Models.PAYMPlanBundleRequestType.RequestChangeHigherPlan:
                        return DoRequestChangeHigherPlan(requestData);
                    case Models.PAYMPlanBundleRequestType.ScheduleCancelPlan:
                        return DoRequestScheduleCancelPlan(requestData);
                    case Models.PAYMPlanBundleRequestType.ImmediateCancelPlan:
                        return DoImmediateScheduleCancelPlan(requestData);
                    //Added by Hari  - 21-01-2015 - for Reactivate LLOM
                    case Models.PAYMPlanBundleRequestType.BundleReactivate:
                        return DoBundleReactivate(requestData);
                    case Models.PAYMPlanBundleRequestType.LLOMReactivate:
                        return DoLLOMReactivate(requestData);
                    case Models.PAYMPlanBundleRequestType.CountrySaverReactivate:
                        return DoCountrySaverReactivate(requestData);
                    //Added by Elango for Jira CRMT-216
                    case Models.PAYMPlanBundleRequestType.RequestCreditLimit:
                        return RequestForChangeCreditLimit(requestData);
                    case Models.PAYMPlanBundleRequestType.GetCreditLimit:
                        return GetCreditLimit(requestData);
                    case Models.PAYMPlanBundleRequestType.ApproveCreditLimit:
                        return ApproveForChangeCreditLimit(requestData);
                    case Models.PAYMPlanBundleRequestType.RejectCreditLimit:
                        return RejectForChangeCreditLimit(requestData);
                    case Models.PAYMPlanBundleRequestType.GetRestrictExcessUsage:
                        return GetRestrictExcessUsage(requestData);
                    case Models.PAYMPlanBundleRequestType.InsertRestrictExcessUsage:
                        return InsertRestrictExcessUsage(requestData);
                    case Models.PAYMPlanBundleRequestType.ListBundleCategory:
                        return ListBundleCategory(requestData);
                    case Models.PAYMPlanBundleRequestType.ListBundleByCategoryId:
                        return GetBundleListByCategoryId(requestData);
                    case Models.PAYMPlanBundleRequestType.ExchangeBundle:
                        return ExchangeBundle(requestData);
                    default:
                        errResult.errmsg = "Unknown Order Type";
                        break;
                }
                requestData.errcode = errResult.errcode;
                requestData.errmsg = errResult.errmsg;
            }
            else
            {
                requestData = new Models.PAYMPlanBundleRequest()
                {
                    errcode = errResult.errcode,
                    errmsg = errResult.errmsg
                };
            }
            Log.Debug(string.Format("PAYMPlanBundlesController.Post: Result Subject={0}; Msg={1}", errResult.errsubject, errResult.errmsg));
            return Request.CreateResponse(HttpStatusCode.OK, requestData);
        }

        private HttpResponseMessage ExchangeBundle(Models.PAYMPlanBundleRequest requestData)
        {
            IEnumerable<CRM_API.Models.Common.ErrCodeMsg> resValues = CRM_API.DB.PAYMPlanBundles.SProc.ExchangeBundle(requestData);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.OK, new List<CRM_API.Models.Common.ErrCodeMsg>() { new CRM_API.Models.Common.ErrCodeMsg { errcode = -1, errmsg = "Failure" } }.ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        private HttpResponseMessage GetBundleListByCategoryId(Models.PAYMPlanBundleRequest requestData)
        {
            Log.Debug("PAYMPlanBundlesController.GetBundleListByCategoryId: Started");
            IEnumerable<CRM_API.Models.BundleListByCategoryIdRef> resValues = CRM_API.DB.PAYMPlanBundles.SProc.GetBundleListByCategoryId(requestData.SiteCode, requestData.CategoryId);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.BundleListByCategoryIdRef>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        private HttpResponseMessage ListBundleCategory(Models.PAYMPlanBundleRequest requestData)
        {
            Log.Debug("PAYMPlanBundlesController.ListBundleCategory: Started");
            IEnumerable<CRM_API.Models.BundleCategorryRef> resValues = CRM_API.DB.PAYMPlanBundles.SProc.ListBundleCategory(requestData.SiteCode);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.BundleCategorryRef>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        private HttpResponseMessage InsertRestrictExcessUsage(Models.PAYMPlanBundleRequest requestData)
        {
            Log.Debug("PAYMPlanBundlesController.InsertRestrictExcessUsage: Started");
            IEnumerable<CRM_API.Models.Common.ErrCodeMsg> resValues = CRM_API.DB.PAYMPlanBundles.SProc.InsertRestrictExcessUsage(requestData.MobileNo, requestData.SiteCode, requestData.status, requestData.updated_by);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.Common.ErrCodeMsg>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        private HttpResponseMessage GetRestrictExcessUsage(Models.PAYMPlanBundleRequest requestData)
        {
            Log.Debug("PAYMPlanBundlesController.GetRestrictExcessUsage: Started");
            IEnumerable<CRM_API.Models.GetRestrictExcessUsageOutput> resValues = CRM_API.DB.PAYMPlanBundles.SProc.GetRestrictExcessUsage(requestData.MobileNo, requestData.SiteCode);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.GetRestrictExcessUsageOutput>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        private HttpResponseMessage GetPlanList(Models.PAYMPlanBundleRequest requestData)
        {
            Log.Debug("PAYMPlanBundlesController.GetPlanList: Started");
            //IEnumerable<CRM_API.Models.PAYMPlan> resValues2 = new IEnumerable<CRM_API.Models.PAYMPlan>();
            //IEnumerable<CRM_API.Models.PAYMPlan> resValues2 
            //return Request.CreateResponse(HttpStatusCode.OK, new resValues2.ToArray());

            IEnumerable<CRM_API.Models.PAYMPlan> resValues = CRM_API.DB.PAYMPlanBundles.SProc.ListPlans(requestData.MobileNo, requestData.CalledBy, requestData.ProductCode, requestData.SiteCode);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.PAYMPlan>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        //Added by Elango for Jira CRMT-216 Start
        private HttpResponseMessage RequestForChangeCreditLimit(Models.PAYMPlanBundleRequest requestData)
        {
            Log.Debug("PAYMPlanBundlesController.RequestForChangeCreditLimit: Started");
            IEnumerable<CRM_API.Models.PAYMPlan> resValues = CRM_API.DB.PAYMPlanBundles.SProc.RequestForChangeCreditLimit(requestData.MobileNo, requestData.RequestCreditAmount, requestData.CalledBy);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.PAYMPlan>().ToArray());
            else
            {
                EmailChangeCreditLimit(requestData.username, "", requestData.MobileNo);
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
            }
        }
        private HttpResponseMessage GetCreditLimit(Models.PAYMPlanBundleRequest requestData)
        {
            Log.Debug("PAYMPlanBundlesController.RequestForChangeCreditLimit: Started");
            IEnumerable<CRM_API.Models.PAYMPlan> resValues = CRM_API.DB.PAYMPlanBundles.SProc.GetCreditLimit(requestData.MobileNo);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.PAYMPlan>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }
        private HttpResponseMessage ApproveForChangeCreditLimit(Models.PAYMPlanBundleRequest requestData)
        {
            Log.Debug("PAYMPlanBundlesController.ApproveForChangeCreditLimit: Started");
            IEnumerable<CRM_API.Models.PAYMPlan> resValues = CRM_API.DB.PAYMPlanBundles.SProc.ApproveForChangeCreditLimit(requestData.MobileNo, requestData.RequestCreditAmount, requestData.CalledBy);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.PAYMPlan>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }
        private HttpResponseMessage RejectForChangeCreditLimit(Models.PAYMPlanBundleRequest requestData)
        {
            Log.Debug("PAYMPlanBundlesController.RejectForChangeCreditLimit: Started");
            IEnumerable<CRM_API.Models.PAYMPlan> resValues = CRM_API.DB.PAYMPlanBundles.SProc.RejectForChangeCreditLimit(requestData.MobileNo, requestData.CalledBy);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.PAYMPlan>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }
        public bool EmailChangeCreditLimit(string fromemail, string toemail, string mobileNo)
        {
            try
            {
                string PAYMTestEMail = System.Configuration.ConfigurationManager.AppSettings["PAYMTestEMail"];
                if (!string.IsNullOrEmpty(PAYMTestEMail))
                {
                    //fromemail = PAYMTestEMail;
                    toemail = PAYMTestEMail;
                }

                string mailSubject = "Request for Change Credit Limit.";
                string mailLocation;
                mailLocation = System.Web.HttpContext.Current.Server.MapPath("~/Email_template/PayM_ChangeCreditLimit.htm").TrimEnd('/');

                StreamReader mailContent_file = new StreamReader(mailLocation);
                string mailContent = mailContent_file.ReadToEnd();
                mailContent_file.Close();
                mailContent = mailContent
                .Replace("[CustomerNumber]", mobileNo);
                // Send the email
                MailAddress mailFrom = new MailAddress(fromemail);
                MailAddressCollection mailTo = new MailAddressCollection();
                mailTo.Add(new MailAddress(toemail));
                return CRM_API.Helpers.EmailSending.Send(true, mailFrom, mailTo, null, null, mailSubject, mailContent);
            }
            catch (Exception ex)
            {
                Log.Error("PAYMPlanBundlesController.EmailChangeCreditLimit", ex.Message);
                return false;
            }

        }
        //Added by Elango for Jira CRMT-216 End

        private HttpResponseMessage GetPlanDetails(Models.PAYMPlanBundleRequest requestData)
        {
            Log.Debug("PAYMPlanBundlesController.GetPlanDetails: Started");
            IEnumerable<CRM_API.Models.PAYMPlanDetail> resValues = CRM_API.DB.PAYMPlanBundles.SProc.ListPlanDetails(requestData.MobileNo, requestData.CalledBy);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.PAYMPlanDetail>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        private HttpResponseMessage GetPlanHistory(Models.PAYMPlanBundleRequest requestData)
        {
            Log.Debug("PAYMPlanBundlesController.GetPlanHistory: Started");
            IEnumerable<CRM_API.Models.PAYMPlanHistory> resValues = CRM_API.DB.PAYMPlanBundles.SProc.ListPlanHistory(requestData.MobileNo, requestData.CalledBy);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.PAYMPlanHistory>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        private HttpResponseMessage GetPlanRef(Models.PAYMPlanBundleRequest requestData)
        {
            Log.Debug("PAYMPlanBundlesController.GetPlanRef: Started");
            IEnumerable<CRM_API.Models.PAYMPlanRef> resValues = CRM_API.DB.PAYMPlanBundles.SProc.ListPlanRef();
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.PAYMPlanRef>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        private HttpResponseMessage GetPlanDetail(Models.PAYMPlanBundleRequest requestData)
        {
            Log.Debug("PAYMPlanBundlesController.GetPlanDetail: Started");
            IEnumerable<CRM_API.Models.PlanDetail> resValues = CRM_API.DB.PAYMPlanBundles.SProc.GetPlanDetail(requestData.BundleId);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.PlanDetail>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        private HttpResponseMessage GetBundleDetail(Models.PAYMPlanBundleRequest requestData)
        {
            Log.Debug("PAYMPlanBundlesController.GetBundleDetail: Started");
            //06-Feb-2017 : Moorthy : Added ProductCode 
            IEnumerable<CRM_API.Models.PlanDetail> resValues = CRM_API.DB.PAYMPlanBundles.SProc.GetBundleDetail(requestData.oldplantext, requestData.ProductCode, requestData.BundleId);
            if (resValues == null || resValues.Count() == 0)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.PlanDetail>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        private HttpResponseMessage GetAccountBalance(Models.PAYMPlanBundleRequest requestData)
        {
            Log.Debug("PAYMPlanBundlesController.GetAccountBalance: Started");
            IEnumerable<CRM_API.Models.AccountBalance> resValues = CRM_API.DB.PAYMPlanBundles.SProc.GetAccountBalance(requestData.MobileNo);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.AccountBalance>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        private HttpResponseMessage DoRequestChangeLowerPlan(Models.PAYMPlanBundleRequest requestData)
        {
            string productcode = GetProductCode(requestData.SiteCode);
            System.Web.HttpContext.Current.Session["GlobalProduct"] = productcode;

            Log.Debug("PAYMPlanBundlesController.DoRequestChangeLowerPlan: Started");

            CRM_API.Models.PAYMPlanAccountInfo resAccInfo = CRM_API.DB.PAYMPlanBundles.SProc.GetAccountInfo(
                requestData.MobileNo, requestData.CalledBy).FirstOrDefault();

            if (resAccInfo == null || string.IsNullOrEmpty(resAccInfo.MobileNo))
            {
                var errValues = new List<CRM_API.DB.Common.ErrCodeMsg>();
                errValues.Add(new DB.Common.ErrCodeMsg() { errcode = -1, errmsg = "Cannot find any customer" });
                return Request.CreateResponse(HttpStatusCode.OK, errValues.ToArray());
            }

            DateTime end_date = requestData.prevplan_end_date;
            DateTime start_date = requestData.newplan_start_date;
            if (requestData.newplan_start_date < DateTime.Now)
            {
                start_date = DateTime.Now.AddMonths(1);
                start_date = new DateTime(start_date.Year, start_date.Month, 1);
            }
            if (requestData.prevplan_end_date < DateTime.Now || end_date > start_date)
            {
                end_date = start_date.AddMinutes(-1);
            }
            IEnumerable<ChangePlanOutput> resValues =
                CRM_API.DB.PAYMPlanBundles.SProc.ChangePlan(requestData.MobileNo,
                end_date, start_date, requestData.newplan, requestData.BundleId, requestData.CalledBy,
                "n/a", "n/a", 0, 1);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<ChangePlanOutput>().ToArray());
            else if (resValues.Count() > 0)
            {
                //TODO : Commented
                if (resValues.ElementAt(0).errcode == 0)
                {
                    Helpers.SendSMS sms = new Helpers.SendSMS();
                    SMSChangePlan(requestData.MobileNo, requestData.newplan, start_date);
                    if (requestData.SiteCode == "MCM" || requestData.SiteCode == "BAU")
                    {
                        EmailChangePlan(resAccInfo.Email, resAccInfo.FullName, resAccInfo.MobileNo, resAccInfo.CurrentPlanName, requestData.newplantext, 0, "", resValues.ElementAt(0).bundleprice, resValues.ElementAt(0).bundlename, !String.IsNullOrEmpty(resValues.ElementAt(0).bundle_call) ? resValues.ElementAt(0).bundle_call.Replace(" UK Minutes", "") : "", !String.IsNullOrEmpty(resValues.ElementAt(0).bundle_sms) ? resValues.ElementAt(0).bundle_sms.Replace(" UK Texts", "") : "", !String.IsNullOrEmpty(resValues.ElementAt(0).bundle_data) ? resValues.ElementAt(0).bundle_data.Replace("GB Data", "") : "", !String.IsNullOrEmpty(resValues.ElementAt(0).bundle_v2v) ? resValues.ElementAt(0).bundle_v2v : "", resValues.ElementAt(0).billing_date, requestData.SiteCode);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
            }
            return Request.CreateResponse(HttpStatusCode.OK, new List<CRM_API.DB.Common.ErrCodeMsg>().ToArray());
        }

        private string GetProductCode(string siteCode)
        {
            string productcode = "VMUK";
            if (siteCode == "MCM")
                productcode = "VMUK";
            else if (siteCode == "BAU")
                productcode = "VMAT";
            else if (siteCode == "MFR")
                productcode = "VMFR";
            else if (siteCode == "BNL")
                productcode = "VMNL";
            return productcode;
        }

        private HttpResponseMessage DoRequestChangeHigherPlan(Models.PAYMPlanBundleRequest requestData)
        {
            string productcode = GetProductCode(requestData.SiteCode);
            System.Web.HttpContext.Current.Session["GlobalProduct"] = productcode;

            Log.Debug("PAYMPlanBundlesController.DoRequestChangeHigherPlan: Started");

            List<CRM_API.DB.Common.ErrCodeMsg> errValues = new List<CRM_API.DB.Common.ErrCodeMsg>();
            int pay_status = 0;

            CRM_API.Models.PAYMPlanAccountInfo resAccInfo = CRM_API.DB.PAYMPlanBundles.SProc.GetAccountInfo(
                requestData.MobileNo, requestData.CalledBy).FirstOrDefault();

            if (resAccInfo == null || string.IsNullOrEmpty(resAccInfo.MobileNo))
            {
                errValues.Add(new DB.Common.ErrCodeMsg() { errcode = -1, errmsg = "Cannot find any customer" });
                return Request.CreateResponse(HttpStatusCode.OK, errValues.ToArray());
            }

            DateTime end_date = requestData.prevplan_end_date;
            DateTime start_date = requestData.newplan_start_date;
            if (requestData.newplan_start_date < DateTime.Now)
            {
                start_date = DateTime.Now.AddMonths(1);
                start_date = new DateTime(start_date.Year, start_date.Month, 1);
            }
            if (requestData.prevplan_end_date < DateTime.Now || end_date > start_date)
            {
                end_date = start_date.AddMinutes(-1);
            }

            CRM_API.Helpers.Payment payment = new Helpers.Payment();
            var currency = CRM_API.DB.PAYMPlanBundles.SProc.GetCurrency(requestData.card_country);
            if (string.IsNullOrEmpty(requestData.SubscriptionID))
            {
                Log.Debug("PAYMPlanBundlesController.DoRequestChangeHigherPlan: Charge New Card");
                payment.Authorize(requestData as Models.Payment.CardDetails, requestData.SiteCode, requestData.MobileNo, string.Format("PP{0:00}", requestData.totalvalue), resAccInfo.Email, currency, requestData.totalvalue, productcode);
                requestData.SubscriptionID = payment.SubscriptionId;
            }
            else
            {
                Log.Debug("PAYMPlanBundlesController.DoRequestChangeHigherPlan: Charge Saved Card");
                //string MerchantReferenceCode = string.Format("{0}-{1}-{2:yyMMddHHmm}", "VMUK", string.Format("PP{0:00}", requestData.newplan), DateTime.Now);
                //payment.CybersourceCharge(MerchantReferenceCode, requestData.MobileNo, requestData.SiteCode, "VMUK", string.Format("PP{0:00}", requestData.newplan), requestData.SubscriptionID, currency, requestData.totalvalue);
                string referenceCode = string.Format("{0}-{1}-{2:yyMMddHHmm}", productcode, string.Format("PP{0:00}-RX", requestData.totalvalue), DateTime.Now);
                payment.AuthorizeWithout3DS(requestData as Models.Payment.CardDetails, requestData.SiteCode, requestData.MobileNo, string.Format("PP{0:00}", requestData.totalvalue), resAccInfo.Email, currency, requestData.totalvalue, requestData.MobileNo, requestData.SubscriptionID, referenceCode, productcode);
            }

            if (payment.Decision == "ACCEPT" || payment.Decision == "APPROVAL")
            {
                Log.Debug("PAYMPlanBundlesController.DoRequestChangeHigherPlan: Payment Success");
                IEnumerable<ChangePlanOutput> resValues =
                    CRM_API.DB.PAYMPlanBundles.SProc.ChangePlan(requestData.MobileNo,
                    end_date, start_date, requestData.newplan, requestData.BundleId, requestData.CalledBy,
                    "credit or debit card", payment.MerchantReferenceCode, requestData.totalvalue, 1);
                if (resValues != null && resValues.Count() > 0)
                {
                    if (resValues.FirstOrDefault().errcode == 0)
                    {
                        pay_status = 1;
                        Helpers.SendSMS sms = new Helpers.SendSMS();
                        SMSChangePlan(requestData.MobileNo, requestData.newplan, start_date);
                        if (requestData.SiteCode == "MCM" || requestData.SiteCode == "BAU")
                        {
                            EmailChangePlan(resAccInfo.Email, resAccInfo.FullName, resAccInfo.MobileNo, resAccInfo.CurrentPlanName, requestData.newplantext, 0, "", resValues.ElementAt(0).bundleprice, resValues.ElementAt(0).bundlename, !String.IsNullOrEmpty(resValues.ElementAt(0).bundle_call) ? resValues.ElementAt(0).bundle_call.Replace(" UK Minutes", "") : "", !String.IsNullOrEmpty(resValues.ElementAt(0).bundle_sms) ? resValues.ElementAt(0).bundle_sms.Replace(" UK Texts", "") : "", !String.IsNullOrEmpty(resValues.ElementAt(0).bundle_data) ? resValues.ElementAt(0).bundle_data.Replace("GB Data", "") : "", !String.IsNullOrEmpty(resValues.ElementAt(0).bundle_v2v) ? resValues.ElementAt(0).bundle_v2v : "", resValues.ElementAt(0).billing_date, requestData.SiteCode);
                            //errValues.Add(resValues.FirstOrDefault());
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
                    }
                    else
                    {
                        //errValues.AddRange(resValues);
                        return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
                    }
                }
                else
                {
                    errValues.Add(new DB.Common.ErrCodeMsg() { errcode = -1, errmsg = string.Format("Payment Failed due to: {0}", payment.Description) });
                }
            }
            else
            {
                Log.Error("PAYMPlanBundlesController.DoRequestChangeHigherPlan: Payment Failed");
                errValues.Add(new DB.Common.ErrCodeMsg() { errcode = -1, errmsg = string.Format("Payment Failed due to: {0}", payment.Description) });
            }

            if (!string.IsNullOrEmpty(payment.MerchantReferenceCode))
            {
                CRM_API.DB.PAYMPayments.SProc.PaymentInsert(DateTime.Now, requestData.MobileNo, payment.MerchantReferenceCode, "credit or debit card", requestData.totalvalue, pay_status, requestData.CalledBy);
            }

            return Request.CreateResponse(HttpStatusCode.OK, errValues.ToArray());
        }

        public bool SMSChangePlan(string mobileNo, double newplan, DateTime start_date)
        {
            try
            {
                string PAYMTestMobileNo = System.Configuration.ConfigurationManager.AppSettings["PAYMTestMobileNo"];
                if (!string.IsNullOrEmpty(PAYMTestMobileNo))
                    mobileNo = PAYMTestMobileNo;

                string smsText = string.Format("Your request to Change plan is completed successfully. Your new plan £{0:N2} will be activated on {1:dd-MM-yyyy}", newplan, start_date);
                string[] res = new Helpers.SendSMS().Send(false, mobileNo, "111", smsText, 0);
                if (res != null && res.Count() > 0)
                {
                    return !(res[0] == "-1");
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                Log.Error("PAYMPlanBundlesController.SMSChangePlan", ex.Message);
                return false;
            }
        }

        public bool EmailChangePlan(string email, string fullname, string mobileNo, string oldPlanName, string newPlanName, float amount, string pay_reference, string bundleprice, string bundlename, string bundle_call, string bundle_sms, string bundle_data, string bundle_v2v, string billing_date, string sitecode)
        {
            try
            {
                try
                {
                    var api = new MandrillApi(ConfigurationManager.AppSettings["MAILCHIMPAPIKEY"]);
                    var message = new MandrillMessage();
                    message.AddGlobalMergeVars("FNAME", fullname);
                    message.AddGlobalMergeVars("PLANNAME", newPlanName);
                    message.AddGlobalMergeVars("NEXTBILLINGDATE", billing_date);
                    message.AddGlobalMergeVars("TOPUPAMOUNT", amount);
                    message.AddGlobalMergeVars("bundlename", bundlename);
                    message.AddGlobalMergeVars("amount", bundleprice);
                    message.AddGlobalMergeVars("GB", bundle_data);
                    message.AddGlobalMergeVars("minutes", bundle_call);
                    message.AddGlobalMergeVars("SMS", bundle_sms);
                    message.AddGlobalMergeVars("v2vminutes", bundle_v2v);
                    message.AddTo(email);
                    IList<MandrillSendMessageResponse> response = api.Messages.SendTemplateAsync(message, ConfigurationManager.AppSettings["MAILCHIMPSIMONLYUPGRADEPLAN_" + sitecode]).Result;
                    Log.Info("EmailChangePlan Mail Response : " + JsonConvert.SerializeObject(response));
                }
                catch (Exception ex)
                {
                    Log.Error("EmailChangePlan Mail Error : " + ex.Message);
                    return false;
                }
                return true;
                //string PAYMTestEMail = System.Configuration.ConfigurationManager.AppSettings["PAYMTestEMail"];
                //if (!string.IsNullOrEmpty(PAYMTestEMail))
                //    email = PAYMTestEMail;

                //string mailSubject = "Change of plan confirmation – Pay Monthly.";
                //string mailLocation;
                //if (amount <= 0)
                //    mailLocation = System.Web.HttpContext.Current.Server.MapPath("~/Email_template/PayM_ChangePlan.htm").TrimEnd('/');
                //else
                //    mailLocation = System.Web.HttpContext.Current.Server.MapPath("~/Email_template/PayM_ChangePlan_Upgrade.htm").TrimEnd('/');
                //StreamReader mailContent_file = new StreamReader(mailLocation);
                //string mailContent = mailContent_file.ReadToEnd();
                //mailContent_file.Close();
                //mailContent = mailContent
                //.Replace("[FullName]", fullname)
                //.Replace("[CustomerNumber]", mobileNo)
                //.Replace("[ExistingPlan]", oldPlanName)
                //.Replace("[NewPlan]", newPlanName)
                //.Replace("[Amount]", string.Format("{0:N2", amount))
                //.Replace("[PaymentReferenceNumber]", pay_reference);
                //// Send the email
                //MailAddress mailFrom = new MailAddress("noreply@vectonemobile.co.uk", "Vectone Mobile");
                //MailAddressCollection mailTo = new MailAddressCollection();
                //mailTo.Add(new MailAddress(email, string.Format("{0}", fullname)));
                //return CRM_API.Helpers.EmailSending.Send(true, mailFrom, mailTo, null, null, mailSubject, mailContent);
            }
            catch (Exception ex)
            {
                Log.Error("PAYMPlanBundlesController.EmailChangePlan", ex.Message);
                return false;
            }

        }

        private HttpResponseMessage DoRequestScheduleCancelPlan(Models.PAYMPlanBundleRequest requestData)
        {
            Log.Debug("PAYMPlanBundlesController.DoRequestScheduleCancelPlan: Started : " + JsonConvert.SerializeObject(requestData));

            CRM_API.Models.PAYMPlanAccountInfo resAccInfo = CRM_API.DB.PAYMPlanBundles.SProc.GetAccountInfo(requestData.MobileNo, requestData.CalledBy).FirstOrDefault();

            if (resAccInfo == null || string.IsNullOrEmpty(resAccInfo.MobileNo))
            {
                var errValues = new List<CRM_API.DB.Common.ErrCodeMsg>();
                errValues.Add(new DB.Common.ErrCodeMsg() { errcode = -1, errmsg = "Cannot find any customer" });
                return Request.CreateResponse(HttpStatusCode.OK, errValues.ToArray());
            }

            DateTime end_date = requestData.prevplan_end_date;
            if (requestData.prevplan_end_date < DateTime.Now)
            {
                end_date = DateTime.Now.AddMonths(1);
                end_date = new DateTime(end_date.Year, end_date.Month, 1);
            }
            IEnumerable<CRM_API.DB.Common.ErrCodeMsg> resValues = CRM_API.DB.PAYMPlanBundles.SProc.CancelPlan(requestData.MobileNo, false,
                end_date, requestData.CalledBy, 0);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.DB.Common.ErrCodeMsg>().ToArray());
            else if (resValues.Count() > 0)
            {
                if (resValues.FirstOrDefault().errcode == 0)
                {
                    Helpers.SendSMS sms = new Helpers.SendSMS();
                    SMSCancelPlan(requestData.MobileNo, end_date, requestData.SiteCode, requestData.CalledBy, resAccInfo.FullName);
                    if (requestData.SiteCode == "MCM" || requestData.SiteCode == "BAU")
                    {
                        EmailCancelPlan(resAccInfo.Email, resAccInfo.FullName, resAccInfo.MobileNo, resAccInfo.CurrentPlanName, requestData.newplan, "n/a", requestData.SiteCode);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
            }
            return Request.CreateResponse(HttpStatusCode.OK, new List<CRM_API.DB.Common.ErrCodeMsg>().ToArray());
        }

        private HttpResponseMessage DoImmediateScheduleCancelPlan(Models.PAYMPlanBundleRequest requestData)
        {
            Log.Debug("PAYMPlanBundlesController.DoImmediateScheduleCancelPlan: Started : " + JsonConvert.SerializeObject(requestData));

            string productcode = GetProductCode(requestData.SiteCode);
            System.Web.HttpContext.Current.Session["GlobalProduct"] = productcode;

            List<CRM_API.DB.Common.ErrCodeMsg> errValues = new List<CRM_API.DB.Common.ErrCodeMsg>();
            int pay_status = 0;

            CRM_API.Models.PAYMPlanAccountInfo resAccInfo = CRM_API.DB.PAYMPlanBundles.SProc.GetAccountInfo(requestData.MobileNo, requestData.CalledBy).FirstOrDefault();

            if (resAccInfo == null || string.IsNullOrEmpty(resAccInfo.MobileNo))
            {
                errValues.Add(new DB.Common.ErrCodeMsg() { errcode = -1, errmsg = "Cannot find any customer" });
                return Request.CreateResponse(HttpStatusCode.OK, errValues.ToArray());
            }

            DateTime end_date = requestData.prevplan_end_date;
            if (requestData.prevplan_end_date < DateTime.Now)
            {
                end_date = DateTime.Now;
            }

            CRM_API.Helpers.Payment payment = new Helpers.Payment();
            var currency = CRM_API.DB.PAYMPlanBundles.SProc.GetCurrency(requestData.card_country);
            if (string.IsNullOrEmpty(requestData.SubscriptionID))
            {
                Log.Debug("PAYMPlanBundlesController.DoImmediateScheduleCancelPlan: Charge New Card");
                payment.Authorize(requestData as Models.Payment.CardDetails, requestData.SiteCode, requestData.MobileNo, string.Format("PP{0:00}", requestData.totalvalue), resAccInfo.Email, currency, requestData.totalvalue, productcode);
                requestData.SubscriptionID = payment.SubscriptionId;
            }
            else
            {
                Log.Debug("PAYMPlanBundlesController.DoImmediateScheduleCancelPlan: Charge Saved Card");
                //payment.CybersourceCharge(requestData.MobileNo, requestData.MobileNo, requestData.SiteCode, "VMUK", string.Format("PP{0:00}", requestData.newplan), requestData.SubscriptionID, currency, requestData.totalvalue);
                string referenceCode = string.Format("{0}-{1}-{2:yyMMddHHmm}", productcode, string.Format("PP{0:00}-RX", requestData.totalvalue), DateTime.Now);
                payment.AuthorizeWithout3DS(requestData as Models.Payment.CardDetails, requestData.SiteCode, requestData.MobileNo, string.Format("PP{0:00}", requestData.totalvalue), resAccInfo.Email, currency, requestData.totalvalue, requestData.MobileNo, requestData.SubscriptionID, referenceCode, productcode);
            }

            if (payment.Decision == "ACCEPT" || payment.Decision == "APPROVAL")
            {
                Log.Debug("PAYMPlanBundlesController.DoImmediateScheduleCancelPlan: Payment Success");

                IEnumerable<CRM_API.DB.Common.ErrCodeMsg> resValues = CRM_API.DB.PAYMPlanBundles.SProc.CancelPlan(requestData.MobileNo, true,
                    end_date, requestData.CalledBy, 0);
                if (resValues != null && resValues.Count() > 0)
                {
                    if (resValues.FirstOrDefault().errcode == 0)
                    {
                        pay_status = 1;
                        Helpers.SendSMS sms = new Helpers.SendSMS();
                        SMSCancelPlan(requestData.MobileNo, end_date, requestData.SiteCode, requestData.CalledBy, resAccInfo.FullName);
                        if (requestData.SiteCode == "MCM" || requestData.SiteCode == "BAU")
                        {
                            EmailCancelPlan(resAccInfo.Email, resAccInfo.FullName, resAccInfo.MobileNo, resAccInfo.CurrentPlanName, requestData.newplan, payment.MerchantReferenceCode, requestData.SiteCode);
                        }
                        errValues.Add(resValues.FirstOrDefault());
                    }
                    else
                    {
                        errValues.AddRange(resValues);
                    }
                }
                else
                {
                    errValues.Add(new DB.Common.ErrCodeMsg() { errcode = -1, errmsg = string.Format("Payment Failed due to: {0}", payment.Description) });
                }
            }
            else
            {
                Log.Error("PAYMPlanBundlesController.DoImmediateScheduleCancelPlan: Payment Failed");
                errValues.Add(new DB.Common.ErrCodeMsg() { errcode = -1, errmsg = string.Format("Payment Failed due to: {0}", payment.Description) });
            }

            if (!string.IsNullOrEmpty(payment.MerchantReferenceCode))
            {
                CRM_API.DB.PAYMPayments.SProc.PaymentInsert(DateTime.Now, requestData.MobileNo, payment.MerchantReferenceCode, "credit or debit card", requestData.totalvalue, pay_status, requestData.CalledBy);
            }

            return Request.CreateResponse(HttpStatusCode.OK, errValues.ToArray());
        }

        public void SMSCancelPlan(string mobileNo, DateTime end_date, string sitecode, string crm_user, string fullname)
        {
            //SMS Trigger
            try
            {
                string smsURL = CRM_API.DB.SMSCampaign.SProc.GetSMSGatewayInfo(sitecode, mobileNo);
                var smsResponse = CRM_API.Helpers.SendSMS.SMSSendOurGateway("Vectone", mobileNo, ConfigurationManager.AppSettings["SMSMESSAGECANCELSPLANCALLINGCUSTOMERSERVICE_"+ sitecode], smsURL);
                CRM_API.DB.SMSCampaign.SProc.InsertSMSNotifyText(mobileNo, string.Format(ConfigurationManager.AppSettings["SMSMESSAGECANCELSPLANCALLINGCUSTOMERSERVICE_" + sitecode], end_date.ToString("dd-MM-yyyy")), crm_user, smsResponse == "-1" ? 0 : 1, "-", fullname, "Agent marks the SIM Only account for cancellation");
                Log.Info("SMSCancelPlan : SMS : Response : " + smsResponse);
            }
            catch (Exception ex)
            {
                Log.Error("SMSCancelPlan SMS Error : " + ex.Message);
            }
            //OldCode
            //try
            //{
            //    string PAYMTestMobileNo = System.Configuration.ConfigurationManager.AppSettings["PAYMTestMobileNo"];
            //    if (!string.IsNullOrEmpty(PAYMTestMobileNo))
            //        mobileNo = PAYMTestMobileNo;

            //    string smsText = string.Format("Your request to cancel plan is completed successfully. Your plan will be dactivate on {1:dd-MM-yyyy}", end_date);
            //    string[] res = new Helpers.SendSMS().Send(false, mobileNo, "111", smsText, 0);
            //    if (res != null && res.Count() > 0)
            //    {
            //        return !(res[0] == "-1");
            //    }
            //    else
            //        return false;
            //}
            //catch (Exception ex)
            //{
            //    Log.Error("PAYMPlanBundlesController.SMSCancelPlan", ex.Message);
            //    return false;
            //}
        }

        public bool EmailCancelPlan(string email, string fullname, string mobileNo, string oldPlanName, double amount, string pay_reference, string sitecode)
        {
            try
            {
                try
                {
                    var api = new MandrillApi(ConfigurationManager.AppSettings["MAILCHIMPAPIKEY"]);
                    var message = new MandrillMessage();
                    message.AddGlobalMergeVars("FNAME", fullname);
                    message.AddTo(email);
                    IList<MandrillSendMessageResponse> response = api.Messages.SendTemplateAsync(message, ConfigurationManager.AppSettings["MAILCHIMPCANCELSPLANCALLINGCUSTOMERSERVICE_" + sitecode]).Result;
                    Log.Info("EmailCancelPlan Mail Response : " + JsonConvert.SerializeObject(response));
                }
                catch (Exception ex)
                {
                    Log.Error("EmailCancelPlan Mail Error : " + ex.Message);
                    return false;
                }
                return true;
                //string PAYMTestEMail = System.Configuration.ConfigurationManager.AppSettings["PAYMTestEMail"];
                //if (!string.IsNullOrEmpty(PAYMTestEMail))
                //    email = PAYMTestEMail;

                //string mailSubject = "Cancel of plan confirmation – Pay Monthly.";
                //string mailLocation;
                //if (amount <= 0)
                //    mailLocation = System.Web.HttpContext.Current.Server.MapPath("~/Email_template/PayM_CancelPlan.htm").TrimEnd('/');
                //else
                //    mailLocation = System.Web.HttpContext.Current.Server.MapPath("~/Email_template/PayM_CancelPlan_Pay.htm").TrimEnd('/');
                //StreamReader mailContent_file = new StreamReader(mailLocation);
                //string mailContent = mailContent_file.ReadToEnd();
                //mailContent_file.Close();
                //mailContent = mailContent
                //.Replace("[FullName]", fullname)
                //.Replace("[CustomerNumber]", mobileNo)
                //.Replace("[ExistingPlan]", oldPlanName)
                //.Replace("[NewPlan]", oldPlanName)
                //.Replace("[Amount]", string.Format("{0:N2", amount))
                //.Replace("[PaymentReferenceNumber]", pay_reference);
                //// Send the email
                //MailAddress mailFrom = new MailAddress("noreply@vectonemobile.co.uk", "Vectone Mobile");
                //MailAddressCollection mailTo = new MailAddressCollection();
                //mailTo.Add(new MailAddress(email, string.Format("{0}", fullname)));
                //return CRM_API.Helpers.EmailSending.Send(true, mailFrom, mailTo, null, null, mailSubject, mailContent);
            }
            catch (Exception ex)
            {
                Log.Error("PAYMPlanBundlesController.EmailCancelPlan", ex.Message);
                return false;
            }

        }

        private HttpResponseMessage DoBundleReactivate(Models.PAYMPlanBundleRequest requestData)
        {
            string productcode = GetProductCode(requestData.SiteCode);
            System.Web.HttpContext.Current.Session["GlobalProduct"] = productcode;

            Log.Debug("PAYMPlanBundlesController.DoBundleReactivate: Started");
            List<CRM_API.DB.Common.ErrCodeMsg> errValues = new List<CRM_API.DB.Common.ErrCodeMsg>();

            int pay_status = 0;
            //need to uncomment


            CRM_API.Models.PAYMPlanAccountInfo resAccInfo = CRM_API.DB.PAYMPlanBundles.SProc.GetAccountInfo(
                requestData.MobileNo, requestData.CalledBy).FirstOrDefault();

            if (resAccInfo == null || string.IsNullOrEmpty(resAccInfo.MobileNo))
            {
                errValues.Add(new DB.Common.ErrCodeMsg() { errcode = -1, errmsg = "Cannot find any customer" });
                return Request.CreateResponse(HttpStatusCode.OK, errValues.ToArray());
            }

            CRM_API.Helpers.Payment payment = new Helpers.Payment();
            var currency = CRM_API.DB.PAYMPlanBundles.SProc.GetCurrency(requestData.card_country);
            if (string.IsNullOrEmpty(requestData.SubscriptionID))
            {
                Log.Debug("PAYMPlanBundlesController.DoRequestChangeHigherPlan: Charge New Card");
                payment.Authorize(requestData as Models.Payment.CardDetails, requestData.SiteCode, requestData.MobileNo, string.Format("PP{0:00}", requestData.totalvalue), resAccInfo.Email, currency, requestData.totalvalue, productcode);
                requestData.SubscriptionID = payment.SubscriptionId;
            }
            else
            {
                Log.Debug("PAYMPlanBundlesController.DoRequestChangeHigherPlan: Charge Saved Card");
                //string MerchantReferenceCode = string.Format("{0}-{1}-{2:yyMMddHHmm}", "VMUK", string.Format("PP{0:00}", requestData.newplan), DateTime.Now);
                //payment.CybersourceCharge(MerchantReferenceCode, requestData.MobileNo, requestData.SiteCode, "VMUK", string.Format("PP{0:00}", requestData.newplan), requestData.SubscriptionID, currency, requestData.totalvalue);
                string referenceCode = string.Format("{0}-{1}-{2:yyMMddHHmm}", productcode, string.Format("PP{0:00}-RX", requestData.totalvalue), DateTime.Now);
                payment.AuthorizeWithout3DS(requestData as Models.Payment.CardDetails, requestData.SiteCode, requestData.MobileNo, string.Format("PP{0:00}", requestData.totalvalue), resAccInfo.Email, currency, requestData.totalvalue, requestData.MobileNo, requestData.SubscriptionID, referenceCode, productcode);
            }

            if (payment.Decision == "ACCEPT" || payment.Decision == "APPROVAL")
            {

                Log.Debug("PAYMPlanBundlesController.DoRequestChangeHigherPlan: Payment Success");
                IEnumerable<CRM_API.DB.Common.ErrCodeMsg> resValues = CRM_API.DB.PAYMPlanBundles.SProc.BundleReactivate(requestData.MobileNo, requestData.BundleId, requestData.SiteCode, requestData.Reg_Number, requestData.CalledBy);

                //IEnumerable<CRM_API.DB.Common.ErrCodeMsg> resValues = new List<CRM_API.DB.Common.ErrCodeMsg>();

                if (resValues != null && resValues.Count() > 0)
                {
                    if (resValues.FirstOrDefault().errcode == -1)
                    {
                        errValues.AddRange(resValues);
                    }
                    else
                    {
                        pay_status = 1;
                        Helpers.SendSMS sms = new Helpers.SendSMS();
                        SMS_AddOnService(requestData.MobileNo, 2, requestData.BundleId.ToString());
                        //EmailLLOMReactivate(resAccInfo.Email, resAccInfo.FullName, resAccInfo.MobileNo, resAccInfo.CurrentPlanName, requestData.newplan, payment.MerchantReferenceCode);
                        errValues.Add(resValues.FirstOrDefault());
                    }
                }
                else
                {
                    errValues.Add(new DB.Common.ErrCodeMsg() { errcode = -1, errmsg = string.Format("Payment Failed due to: {0}", payment.Description) });
                }
            }
            else
            {
                Log.Error("PAYMPlanBundlesController.DoBundleReactivate: Payment Failed");
                errValues.Add(new DB.Common.ErrCodeMsg() { errcode = -1, errmsg = string.Format("Payment Failed due to: {0}", payment.Description) });
            }

            if (!string.IsNullOrEmpty(payment.MerchantReferenceCode))
            {
                CRM_API.DB.PAYMPayments.SProc.PaymentInsert(DateTime.Now, requestData.MobileNo, payment.MerchantReferenceCode, "credit or debit card", requestData.totalvalue, pay_status, requestData.CalledBy);
            }
            return Request.CreateResponse(HttpStatusCode.OK, errValues.ToArray());
        }

        private HttpResponseMessage DoLLOMReactivate(Models.PAYMPlanBundleRequest requestData)
        {
            string productcode = GetProductCode(requestData.SiteCode);
            System.Web.HttpContext.Current.Session["GlobalProduct"] = productcode;

            Log.Debug("PAYMPlanBundlesController.DoLLOMReactivate: Started");
            List<CRM_API.DB.Common.ErrCodeMsg> errValues = new List<CRM_API.DB.Common.ErrCodeMsg>();


            int pay_status = 0;
            //need to uncomment

            CRM_API.Models.PAYMPlanAccountInfo resAccInfo = CRM_API.DB.PAYMPlanBundles.SProc.GetAccountInfo(
                requestData.MobileNo, requestData.CalledBy).FirstOrDefault();

            if (resAccInfo == null || string.IsNullOrEmpty(resAccInfo.MobileNo))
            {
                errValues.Add(new DB.Common.ErrCodeMsg() { errcode = -1, errmsg = "Cannot find any customer" });
                return Request.CreateResponse(HttpStatusCode.OK, errValues.ToArray());
            }

            CRM_API.Helpers.Payment payment = new Helpers.Payment();
            var currency = CRM_API.DB.PAYMPlanBundles.SProc.GetCurrency(requestData.card_country);
            if (string.IsNullOrEmpty(requestData.SubscriptionID))
            {
                Log.Debug("PAYMPlanBundlesController.DoRequestChangeHigherPlan: Charge New Card");
                payment.Authorize(requestData as Models.Payment.CardDetails, requestData.SiteCode, requestData.MobileNo, string.Format("PP{0:00}", requestData.totalvalue), resAccInfo.Email, currency, requestData.totalvalue, productcode);
                requestData.SubscriptionID = payment.SubscriptionId;
            }
            else
            {
                Log.Debug("PAYMPlanBundlesController.DoRequestChangeHigherPlan: Charge Saved Card");
                //string MerchantReferenceCode = string.Format("{0}-{1}-{2:yyMMddHHmm}", "VMUK", string.Format("PP{0:00}", requestData.newplan), DateTime.Now);
                //payment.CybersourceCharge(MerchantReferenceCode, requestData.MobileNo, requestData.SiteCode, "VMUK", string.Format("PP{0:00}", requestData.newplan), requestData.SubscriptionID, currency, requestData.totalvalue);
                string referenceCode = string.Format("{0}-{1}-{2:yyMMddHHmm}", productcode, string.Format("PP{0:00}-RX", requestData.totalvalue), DateTime.Now);
                payment.AuthorizeWithout3DS(requestData as Models.Payment.CardDetails, requestData.SiteCode, requestData.MobileNo, string.Format("PP{0:00}", requestData.totalvalue), resAccInfo.Email, currency, requestData.totalvalue, requestData.MobileNo, requestData.SubscriptionID, referenceCode, productcode);
            }

            if (payment.Decision == "ACCEPT" || payment.Decision == "APPROVAL")
            {
                Log.Debug("PAYMPlanBundlesController.DoRequestChangeHigherPlan: Payment Success");
                IEnumerable<CRM_API.DB.Common.ErrCodeMsg> resValues =
                    CRM_API.DB.PAYMPlanBundles.SProc.LLOMReactivate(requestData.MobileNo, requestData.SiteCode, requestData.Reg_Number, requestData.SvcId, requestData.payment_mode);


                if (resValues != null && resValues.Count() > 0)
                {
                    if (resValues.FirstOrDefault().errcode == -1)
                    {
                        errValues.AddRange(resValues);
                    }
                    else
                    {
                        pay_status = 1;
                        Helpers.SendSMS sms = new Helpers.SendSMS();
                        SMS_AddOnService(requestData.MobileNo, 1, "LLOM");
                        //EmailLLOMReactivate(resAccInfo.Email, resAccInfo.FullName, resAccInfo.MobileNo, resAccInfo.CurrentPlanName, requestData.newplan, payment.MerchantReferenceCode);
                        errValues.Add(resValues.FirstOrDefault());
                    }
                }
                else
                {
                    errValues.Add(new DB.Common.ErrCodeMsg() { errcode = -1, errmsg = string.Format("Payment Failed due to: {0}", payment.Description) });
                }
            }
            else
            {
                Log.Error("PAYMPlanBundlesController.DoBundleReactivate: Payment Failed");
                errValues.Add(new DB.Common.ErrCodeMsg() { errcode = -1, errmsg = string.Format("Payment Failed due to: {0}", payment.Description) });
            }

            if (!string.IsNullOrEmpty(payment.MerchantReferenceCode))
            {
                CRM_API.DB.PAYMPayments.SProc.PaymentInsert(DateTime.Now, requestData.MobileNo, payment.MerchantReferenceCode, "credit or debit card", requestData.totalvalue, pay_status, requestData.CalledBy);
            }

            return Request.CreateResponse(HttpStatusCode.OK, errValues.ToArray());
        }

        //public bool SMS_AddOnService(string mobileNo, DateTime end_date)
        public bool SMS_AddOnService(string mobileNo, int Type, string Bundle)
        {
            try
            {
                string SMSContent = "";
                string PAYMTestMobileNo = System.Configuration.ConfigurationManager.AppSettings["PAYMTestMobileNo"];
                if (!string.IsNullOrEmpty(PAYMTestMobileNo))
                    mobileNo = PAYMTestMobileNo;
                //if (Type == 0) // Activation of the Bundle
                //{ SMSContent = "Dear Customer, The Pakistan Pocket Saver Bundle is activated on your number for 30 days. You can enjoy 1000 free mins to any Pakistan Zong numbers."; }
                //else
                if (Type == 1) // LLOM
                { SMSContent = string.Format("Dear Customer,Your subscription has been renewed for the LLOM number - {0}. ", mobileNo); }
                else if (Type == 2) // Reactivation of the Bundle
                { SMSContent = "The Pakistan Pocket Saver Bundle is reactivated on your number for 30 days. You can enjoy 1000 free mins to any Pakistan numbers."; }
                else if (Type == 3) // Reactivation of the Country Saver Bundle
                {
                    // SMSContent = "The Pakistan Country Saver Bundle is reactivated for F&F CLI. You can enjoy 1000 free mins to this destination.";
                    //The <Country Name>Country Saver Bundle is reactivated for <F&F CLI>. You can enjoy 1000 free mins to this destination.
                    SMSContent = "Dear Customer,";
                    SMSContent = string.Format("The {0}  is reactivated  for {1}. You can enjoy 1000 free mins to this destination.", Bundle, mobileNo);


                }

                string[] res = new Helpers.SendSMS().Send(false, mobileNo, "111", SMSContent, 0);
                if (res != null && res.Count() > 0)
                {
                    return !(res[0] == "-1");
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                Log.Error("PAYMPlanBundlesController.SMSCancelPlan", ex.Message);
                return false;
            }
        }

        private HttpResponseMessage DoCountrySaverReactivate(Models.PAYMPlanBundleRequest requestData)
        {
            string productcode = GetProductCode(requestData.SiteCode);
            System.Web.HttpContext.Current.Session["GlobalProduct"] = productcode;

            Log.Debug("PAYMPlanBundlesController.DoCountrySaverReactivate: Started");
            List<CRM_API.DB.Common.ErrCodeMsg> errValues = new List<CRM_API.DB.Common.ErrCodeMsg>();

            int pay_status = 0;
            string MerchantReferenceCode = "";
            //need to uncomment

            CRM_API.Models.PAYMPlanAccountInfo resAccInfo = CRM_API.DB.PAYMPlanBundles.SProc.GetAccountInfo(
                requestData.MobileNo, requestData.CalledBy).FirstOrDefault();

            if (resAccInfo == null || string.IsNullOrEmpty(resAccInfo.MobileNo))
            {
                errValues.Add(new DB.Common.ErrCodeMsg() { errcode = -1, errmsg = "Cannot find any customer" });
                return Request.CreateResponse(HttpStatusCode.OK, errValues.ToArray());
            }

            //CRM_API.Helpers.Payment payment = new Helpers.Payment();
            //var currency = CRM_API.DB.PAYMPlanBundles.SProc.GetCurrency(requestData.card_country);

            CRM_API.Helpers.Payment payment = new Helpers.Payment();

            var result = DB.PaymentProfile.SProc.GetListProfile(requestData.MobileNo, requestData.SiteCode);

            //CRM_API.Helpers.Payment payment = new Helpers.Payment();
            var currency = CRM_API.DB.PAYMPlanBundles.SProc.GetCurrency(requestData.card_country);
            //string currency = result.Where(c => c.CardNumber == requestData.card_number || requestData.card_number.Substring(requestData.card_number.Length - c.CardNumber.Length) == c.CardNumber).Select(m => m.Currency).Single().ToString();

            //if (result.Where(c => c.CardNumber == requestData.card_number || requestData.card_number.Substring(requestData.card_number.Length - c.CardNumber.Length) == c.CardNumber).Count()==0)
            //{
            //    requestData.SubscriptionID = null;
            //}

            //requestData.SubscriptionID = null;

            if (!(string.IsNullOrEmpty(requestData.card_post_code)))
            {
                Log.Debug("PAYMPlanBundlesController.DoRequestChangeHigherPlan: Charge New Card");
                payment.Authorize(requestData as Models.Payment.CardDetails, requestData.SiteCode, requestData.MobileNo, string.Format("PP{0:00}", requestData.totalvalue), resAccInfo.Email, currency, requestData.totalvalue, productcode);
                //requestData.SubscriptionID = payment.SubscriptionId;
            }
            else
            {
                Log.Debug("PAYMPlanBundlesController.DoRequestChangeHigherPlan: Charge Saved Card");
                //MerchantReferenceCode = string.Format("{0}-{1}-{2:yyMMddHHmm}", "VMUK", string.Format("PP{0:00}", requestData.newplan), DateTime.Now);
                //payment.CybersourceCharge(MerchantReferenceCode, requestData.MobileNo, requestData.SiteCode, "VMUK", string.Format("PP{0:00}", requestData.newplan), requestData.SubscriptionID, currency, requestData.totalvalue);
                string referenceCode = string.Format("{0}-{1}-{2:yyMMddHHmm}", productcode, string.Format("PP{0:00}-RX", requestData.totalvalue), DateTime.Now);
                payment.AuthorizeWithout3DS(requestData as Models.Payment.CardDetails, requestData.SiteCode, requestData.MobileNo, string.Format("PP{0:00}", requestData.totalvalue), resAccInfo.Email, currency, requestData.totalvalue, requestData.MobileNo, requestData.SubscriptionID, referenceCode, productcode);
            }

            if (payment.Decision == "ACCEPT" || payment.Decision == "APPROVAL")
            {
                Log.Debug("PAYMPlanBundlesController.DoRequestChangeHigherPlan: Payment Success");
                IEnumerable<CRM_API.DB.Common.ErrCodeMsg> resValues =
                    //CRM_API.DB.PAYMPlanBundles.SProc.CountrySaverReactivate(requestData.SiteCode, 2, requestData.MobileNo, requestData.BundleId, requestData.payment_mode , requestData.Reg_Number);
                    CRM_API.DB.PAYMPlanBundles.SProc.CountrySaverReactivate(requestData.SubscriptionID, payment.MerchantReferenceCode, 1, 1, "UK");



                if (resValues != null && resValues.Count() > 0)
                {
                    if (resValues.FirstOrDefault().errcode == 0)
                    {
                        pay_status = 1;
                        Helpers.SendSMS sms = new Helpers.SendSMS();
                        SMS_AddOnService(requestData.MobileNo, 3, requestData.BundleId.ToString());
                        //EmailLLOMReactivate(resAccInfo.Email, resAccInfo.FullName, resAccInfo.MobileNo, resAccInfo.CurrentPlanName, requestData.newplan, payment.MerchantReferenceCode);
                        errValues.Add(resValues.FirstOrDefault());
                    }
                    else
                    {
                        errValues.AddRange(resValues);
                    }
                }
                else
                {
                    errValues.Add(new DB.Common.ErrCodeMsg() { errcode = -1, errmsg = string.Format("Payment Failed due to: {0}", payment.Description) });
                }
            }
            else
            {
                Log.Error("PAYMPlanBundlesController.DoBundleReactivate: Payment Failed");
                errValues.Add(new DB.Common.ErrCodeMsg() { errcode = -1, errmsg = string.Format("Payment Failed due to: {0}", payment.Description) });
            }

            if (!string.IsNullOrEmpty(payment.MerchantReferenceCode))
            {
                CRM_API.DB.PAYMPayments.SProc.PaymentInsert(DateTime.Now, requestData.MobileNo, payment.MerchantReferenceCode, "credit or debit card", requestData.totalvalue, pay_status, requestData.CalledBy);
            }

            return Request.CreateResponse(HttpStatusCode.OK, errValues.ToArray());
        }

    }
}
