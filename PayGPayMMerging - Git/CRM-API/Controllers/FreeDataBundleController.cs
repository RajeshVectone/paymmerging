﻿using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using CRM_API.Models;
using CRM_API.DB;
using Newtonsoft.Json;

namespace CRM_API.Controllers
{
    public class FreeDataBundleController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public HttpResponseMessage Post(FreeDataBundleInput input)
        {
            try
            {
                Log.Info("FreeDataBundle : " + JsonConvert.SerializeObject(input));

                switch (input.Type)
                {
                    case FreeDataBundleType.GetBundleGroup:
                        return GetBundleGroup(input);
                    case FreeDataBundleType.GetBundleListInfo:
                        return GetBundleListInfo(input);
                    case FreeDataBundleType.BundleSubscribeFree:
                        return BundleSubscribeFree(input);
                    case FreeDataBundleType.GetFreelySubscribedBundleReport:
                        return GetFreelySubscribedBundleReport(input);
                    default:
                        return Request.CreateResponse(HttpStatusCode.NotFound, new List<FreeDataBundleOutput>());
                }
            }
            catch (Exception ex)
            {
                Log.Error("FreeDataBundle : " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new List<FreeDataBundleOutput>() { new FreeDataBundleOutput() { errcode = -1, errmsg = ex.Message } });
            }
        }

        private HttpResponseMessage GetFreelySubscribedBundleReport(FreeDataBundleInput input)
        {
            IEnumerable<GetFreelySubscribedBundleReportOutput> resValues = FreeDataBundleDB.GetFreelySubscribedBundleReport(input);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<GetFreelySubscribedBundleReportOutput>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        private HttpResponseMessage BundleSubscribeFree(FreeDataBundleInput input)
        {
            IEnumerable<BundleSubscribeFreeOutput> resValues = FreeDataBundleDB.BundleSubscribeFree(input);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<BundleSubscribeFreeOutput>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        private HttpResponseMessage GetBundleListInfo(FreeDataBundleInput input)
        {
            IEnumerable<GetBundleListInfoOutput> resValues = FreeDataBundleDB.GetBundleListInfo(input);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<GetBundleListInfoOutput>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        private HttpResponseMessage GetBundleGroup(FreeDataBundleInput input)
        {
            IEnumerable<GetBundleGroupOutput> resValues = FreeDataBundleDB.GetBundleGroup(input);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<GetBundleGroupOutput>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }
    }
}
