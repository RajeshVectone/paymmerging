﻿using CRM_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NLog;
using System.Web.Script.Serialization;
using System.IO;
using System.Net.Mail;
using Mandrill;
using System.Configuration;
using Newtonsoft.Json;
using Mandrill.Model;

namespace CRM_API.Controllers
{
    public class OrderManagementController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        [HttpGet]
        public HttpResponseMessage Get(string Mobileno, string Sitecode, string sim)
        {
            var resvalue = CRM_API.DB.OrderManagement.SimOrderbyMobile(Mobileno, Sitecode);
            return Request.CreateResponse(HttpStatusCode.OK, resvalue);
        }

        [HttpPost]
        public HttpResponseMessage post(string Mobileno, string Sitecode, string sim)
        {
            var resvalue = CRM_API.DB.SimTransfer.SProc.GetSimTransferLogData(Mobileno, Sitecode);
            return Request.CreateResponse(HttpStatusCode.OK, resvalue);
        }

        //Added : 24-Dec-2018 - Send a SIM Card
        [HttpPut]
        public HttpResponseMessage Put(CRM_API.Models.SIMOrderItem Inx)
        {
            Log.Debug("OrderManagementController.Put: Started");
            Log.Info(new JavaScriptSerializer().Serialize(Inx));
            bool isfreesim2in1;
            try
            {
                if (!Inx.IsCancel) //Sim Order Replacement
                {
                    isfreesim2in1 = CheckIs2in1Order(Inx);
                    var resx = CRM_API.DB.SIMOrder.SProc.ListOfStatus().Where(w => Inx.order_status.ToLower().Contains(w.Value.ToLower()));
                    CheckInput(Inx, resx);
                    var resy = resx.FirstOrDefault();
                    int freesim_orderid;
                    string freesim = Inx.freesim_order_id;

                    if (isfreesim2in1)
                    {
                        if (resy.Id == 4)
                            CRM_API.DB.SIMOrder.SProc.ValidateIccidInput(Inx.iccid, Inx.freesim_order_id, "M", Inx.Sitecode);
                        freesim_orderid = Int32.Parse(freesim.Substring(3, freesim.Length - 3));
                        var result = CRM_API.DB.SIMOrder.SProc.UpdateFreeSimOrder2in1(Inx.mimsi_order_id, freesim_orderid, resy.Value, Inx.Sitecode, Inx.logged_user, Inx.iccid, null, Inx.royal_mail_reference);
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    }
                    else
                    {
                        if (resy.Id == 4)
                            CRM_API.DB.SIMOrder.SProc.ValidateIccidInput(Inx.iccid, Inx.freesim_order_id, "", Inx.Sitecode);
                        var M = CRM_API.DB.SIMOrder.SProc.SearchByOrderId(Inx.freesim_order_id.ToString(), "freesimid", Inx.Sitecode);

                        if (resy.Id == 9)
                        {
                            M.sitecode = Inx.Sitecode;
                            //M.iccid = Inx.iccid;
                            CRM_API.DB.Common.ErrCodeMsg errCodeMsg = CRM_API.DB.SIMOrder.SProc.UpdateFreeSimWithReplacement(M.sitecode, Inx.freesim_order_id, M.iccid, Inx.iccid, "CRM", 1, Inx.Address1, Inx.Address2, Inx.Address3, Inx.Postcode, Inx.City);
                            var result = new Models.Common.ErrCodeMsg() { errcode = errCodeMsg.errcode, errmsg = errCodeMsg.errmsg };
                            return Request.CreateResponse(HttpStatusCode.OK, result);
                        }
                        else
                        {
                            M.sitecode = Inx.Sitecode;
                            M.iccid = Inx.iccid;

                            if (CRM_API.DB.SIMOrder.SProc.UpdateFreeSimOrder(M, resy.Id, Inx.logged_user, Inx.royal_mail_reference))
                            {
                                /* Email sending sim dispatch  start */
                                string email = Inx.email;
                                string fullname = Inx.first_name;
                                string oredrid = Inx.freesim_order_id;
                                string trackingnumber = Inx.royal_mail_reference;
                                if (!string.IsNullOrEmpty(email))
                                {
                                    Simdispatch(email, fullname, trackingnumber, oredrid);
                                }
                                /* Email sending sim dispatch  End */
                                var result = new Models.Common.ErrCodeMsg() { errcode = 0, errmsg = "Updated" };
                                return Request.CreateResponse(HttpStatusCode.OK, result);


                            }
                            else
                            {
                                var result = new Models.Common.ErrCodeMsg() { errcode = -1, errmsg = "Update Failed" };
                                return Request.CreateResponse(HttpStatusCode.OK, result);
                            }
                        }
                    }
                }
                else //Cancel Sim Order
                {
                    int freesimid = Convert.ToInt32(Inx.freesim_order_id);
                    var result = CRM_API.DB.SIMOrder.SProc.CancelFreeSimOrder(freesimid, Inx.mobileno, Inx.Sitecode, Inx.logged_user);
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                    //if (result != null && result.ElementAt(0).errcode == 0 && !String.IsNullOrEmpty(result.ElementAt(0).email))
                    //{
                    //    if (Inx.Sitecode == "MCM")
                    //    {
                    //        //Email Trigger
                    //        try
                    //        {
                    //            var api = new MandrillApi(ConfigurationManager.AppSettings["MAILCHIMPAPIKEY"]);
                    //            var message = new MandrillMessage();
                    //            message.AddGlobalMergeVars("FNAME", result.ElementAt(0).firstname);
                    //            message.AddTo(result.ElementAt(0).email);
                    //            var response = api.Messages.SendTemplateAsync(message, ConfigurationManager.AppSettings["MAILCHIMPCANCELSSIMONLYBYCALLINGCUSTOMERSERVICE"]).Result;
                    //            Log.Info("Cancel Sim Order Mail Response : " + JsonConvert.SerializeObject(response));
                    //        }
                    //        catch (Exception ex)
                    //        {
                    //            Log.Error("Cancel Sim Order Mail Error : " + ex.Message);
                    //        }
                    //        //SMS Trigger
                    //        try
                    //        {
                    //            string smsURL = CRM_API.DB.SMSCampaign.SProc.GetSMSGatewayInfo(Inx.Sitecode, Inx.mobileno);
                    //            var smsResponse = CRM_API.Helpers.SendSMS.SMSSendOurGateway("Vectone", Inx.mobileno, ConfigurationManager.AppSettings["SMSMESSAGECANCELSSIMONLYBYCALLINGCUSTOMERSERVICE"], smsURL);
                    //            CRM_API.DB.SMSCampaign.SProc.InsertSMSNotifyText(Inx.mobileno, ConfigurationManager.AppSettings["SMSMESSAGECANCELSSIMONLYBYCALLINGCUSTOMERSERVICE"], Inx.logged_user, smsResponse == "-1" ? 0 : 1, "-", result.ElementAt(0).firstname);
                    //            Log.Info("ResetMyAccountPwd : SMS : Response : " + smsResponse);
                    //        }
                    //        catch (Exception ex)
                    //        {
                    //            Log.Error("ResetMyAccountPwd SMS Error : " + ex.Message);
                    //        }
                    //    }
                    //}
                }
            }
            catch (Exception e)
            {
                Log.Error("OrderManagementController.Put:" + e.Message);
                return Request.CreateResponse(HttpStatusCode.OK, new Models.Common.ErrCodeMsg() { errcode = -1, errmsg = e.Message });
            }
        }

        private static bool CheckIs2in1Order(CRM_API.Models.SIMOrderItem Inx)
        {
            bool isfreesim2in1;
            if (Inx.freesim_order_id.ToString().Contains("M"))
                isfreesim2in1 = true;
            else
                isfreesim2in1 = false;

            if (Inx.mimsi_order_id == 0)
                isfreesim2in1 = false;
            else
                isfreesim2in1 = true;

            return isfreesim2in1;
        }

        private static void CheckInput(CRM_API.Models.SIMOrderItem Inx, IEnumerable<Models.RefInfo> resx)
        {
            if (resx == null)
                throw new NullReferenceException("Order Status Not Recognized");
            if (resx.Count() > 1)
                throw new Exception("Duplicate Status Retrive Please Contact Your DB Admin");
            if (Inx.logged_user == null)
                throw new Exception("Please provide CS Username");
            if (string.IsNullOrEmpty(Inx.logged_user))
                throw new Exception("Empty Username CS, Please provide first");
        }

        /* Email sim dispatch function start  */
        public bool Simdispatch(string email, string fullname, string trackingnumber, string orderid)
        {
            try
            {
                //email = "k.karthikeyan@mundio.com";
                string mailsubjectstart = "Your order ID ";
                string mailsubjectend = " has been dispatched.";
                string mailsubjectmiddle = orderid;
                string mailSubject = mailsubjectstart + mailsubjectmiddle + mailsubjectend;
                string mailLocation;
                mailLocation = System.Web.HttpContext.Current.Server.MapPath("~/Email_template/SIM_Dispatch.html").TrimEnd('/');
                StreamReader mailContent_file = new StreamReader(mailLocation);
                string mailContent = mailContent_file.ReadToEnd();
                mailContent_file.Close();
                mailContent = mailContent
                .Replace("[FullName]", fullname)
                .Replace("[trackingnumber]", trackingnumber);
                // Send the email
                MailAddress mailFrom = new MailAddress("noreply@vectonemobile.co.uk", "Vectone Mobile");
                MailAddressCollection mailTo = new MailAddressCollection();
                mailTo.Add(new MailAddress(email, string.Format("{0}", fullname)));
                return CRM_API.Helpers.EmailSending.Send(true, mailFrom, mailTo, null, null, mailSubject, mailContent);
            }
            catch (Exception ex)
            {
                Log.Error("SIMOrderSearchController.Simdispatch", ex.Message);
                return false;
            }
        }
    }
}
