﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CRM_API.Controllers
{
    public class CustomerInfoController : ApiController
    {
        [HttpPost()]
        public HttpResponseMessage Put(CRM_API.Models.CustomerUpdatedata Model)
        {
            if (Model != null)
                return UpdateData(Model);
            //return Request.CreateResponse(HttpStatusCode.OK,errResult);
            else
            {
                DB.Common.ErrCodeMsg errResult = new DB.Common.ErrCodeMsg()
                {
                    errcode = -1,
                    errsubject = "General Error",
                    errmsg = "Subscriber Id: N/A" 
                };
                return Request.CreateResponse(HttpStatusCode.NotFound, errResult);
            }
        }

        private HttpResponseMessage UpdateData(CRM_API.Models.CustomerUpdatedata resValues)
        {
            var errResult = new DB.Common.ErrCodeMsg();
            errResult = DB.Customer.SProc.UpdateCustomer(resValues);
            errResult.errsubject = "Update Customer Info";
            if (errResult.errcode == 0)
            {
                errResult.errmsg = "Customer Data Have Been Updated";
            }
            return Request.CreateResponse(HttpStatusCode.OK, errResult);

        }
    }
}
