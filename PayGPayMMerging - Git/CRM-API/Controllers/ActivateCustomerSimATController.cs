﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dapper;
using Mandrill;
using Mandrill.Model;
using Newtonsoft.Json;
using NLog;

namespace CRM_API.Controllers
{
    public class ActivateCustomerSimATController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public class ActivateCustomerSimATInput
        {
            public string sitecode { get; set; }
            public string iccid { get; set; }
            public string mobileno { get; set; }
            public string email { get; set; }
            public string birthdate { get; set; }
            public string first_name { get; set; }
            public string last_name { get; set; }
            public string postcode { get; set; }
            public string address { get; set; }
            public string called_by { get; set; }
        }

        public class ActivateCustomerSimATOutput
        {
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }

        public class SimactivationactivatecustomersimOutput
        {
            public int errcode { get; set; }
            public string errmsg { get; set; }

            public int send_email_flag { get; set; }
            public int send_sms_flag { get; set; }
            public string bundle1_national_call { get; set; }
            public string bundle1_inter_national_call { get; set; }
            public string bundl1_sms { get; set; }
            public string bundl1_data { get; set; }
            public string bundle1_v2v { get; set; }
            public string bundle2_national_call { get; set; }
            public string bundle2_inter_national_call { get; set; }
            public string bundl2_sms { get; set; }
            public string bundl2_data { get; set; }
            public string bundle2_v2v { get; set; }
            public int brand_type { get; set; }
        }

        public HttpResponseMessage Post(ActivateCustomerSimATInput req)
        {
            Log.Info("ActivateCustomerSimAT : Input : " + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(CRM_API.Helpers.Config.SIMACTIVATION))
                {
                    conn.Open();
                    var result = conn.Query<dynamic>("simactivation_activate_customer_sim", new
                    {
                        @iccid = req.iccid,
                        @mobileno = req.mobileno,
                        @act_user_id = "",
                        @sitecode = req.sitecode,
                        @first_name = req.first_name,
                        @last_name = req.last_name,
                        @postcode = req.postcode,
                        @address = req.address,
                        @passport_id = "",
                        @mrc_code = "",
                        @belgium_id = "",
                        @file_path = "",
                        @birthdate = req.birthdate,
                        @expire_date = "",
                        @source_address = "CRM",
                        @valid_from = "",
                        @lang_code = "DE",
                        @portin_flag = "",
                        @portin_mobile_number = "",
                        @portin_iccid = "",
                        @called_by = "CRM - " + req.called_by,
                        @gov_doc_check_flag = "",
                        @image_path2 = "",
                        @image_path3 = "",
                        @email = req.email,
                        @failure_by_gov = "",
                        @doc_type = "",
                        @failed_reason = ""
                    }, commandType: CommandType.StoredProcedure);


                    Log.Info("ActivateCustomerSimAT : SP Output : " + JsonConvert.SerializeObject(result));

                    List<SimactivationactivatecustomersimOutput> OutputList = new List<SimactivationactivatecustomersimOutput>();

                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new SimactivationactivatecustomersimOutput()
                        {
                            send_email_flag = r.send_email_flag,
                            bundle1_national_call = r.bundle1_national_call,
                            bundle1_inter_national_call = r.bundle1_inter_national_call,
                            bundl1_sms = r.bundl1_sms,
                            bundl1_data = r.bundl1_data,
                            bundle1_v2v = r.bundle1_v2v,
                            bundle2_national_call = r.bundle2_national_call,
                            bundle2_inter_national_call = r.bundle2_inter_national_call,
                            bundl2_sms = r.bundl2_sms,
                            bundl2_data = r.bundl2_data,
                            bundle2_v2v = r.bundle2_v2v,
                            brand_type = r.brand_type,
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));

                        if (OutputList != null && OutputList.Count > 0 && OutputList[0].errcode == 0 && OutputList[0].send_email_flag == 1 && !String.IsNullOrEmpty(req.email))
                        {
                            try
                            {
                                //Send Email
                                var api = new MandrillApi(ConfigurationManager.AppSettings["MAILCHIMPAPIKEY"]);
                                var message = new MandrillMessage();
                                message.AddTo(req.email, req.first_name);
                                message.AddGlobalMergeVars("FNAME", req.first_name);
                                message.AddGlobalMergeVars("MobileNumber", req.mobileno);
                                message.AddGlobalMergeVars("ATGB", OutputList[0].bundl1_data);
                                message.AddGlobalMergeVars("ATminutes", OutputList[0].bundle1_national_call);
                                message.AddGlobalMergeVars("ATSMS", OutputList[0].bundl1_sms);
                                message.AddGlobalMergeVars("ATV2Vminutes", OutputList[0].bundle1_v2v);
                                message.AddGlobalMergeVars("AIOGB", OutputList[0].bundl2_data);
                                message.AddGlobalMergeVars("INTminutes", OutputList[0].bundle2_inter_national_call);
                                message.AddGlobalMergeVars("AIOminutes", OutputList[0].bundle2_national_call);
                                message.AddGlobalMergeVars("AIOSMS", OutputList[0].bundl2_sms);
                                message.AddGlobalMergeVars("AIOV2Vminutes", OutputList[0].bundle2_v2v);
                                string templateName = "";
                                if (OutputList[0].brand_type == 2)
                                {
                                    templateName = "dmat64-sim-registration-success";
                                    //templateName = ConfigurationManager.AppSettings["DMAILCHIMPTEMPLATENAME_EN"];
                                    //if (req.lang_code.ToUpper() == "DE")
                                    //    templateName = ConfigurationManager.AppSettings["DMAILCHIMPTEMPLATENAME"];
                                }
                                else
                                {
                                    templateName = "a64-sim-registration-success-at";
                                    //templateName = ConfigurationManager.AppSettings["MAILCHIMPTEMPLATENAME_EN"];
                                    //if (req.lang_code.ToUpper() == "DE")
                                    //    templateName = ConfigurationManager.AppSettings["MAILCHIMPTEMPLATENAME"];
                                }
                                var mailResponse = api.Messages.SendTemplateAsync(message, templateName).Result;
                                Log.Info("Mail Response : " + Newtonsoft.Json.JsonConvert.SerializeObject(mailResponse));
                            }
                            catch (Exception ex)
                            {
                                Log.Info("Mail Error : " + ex.Message);
                            }
                        }
                    }
                    else
                    {
                        SimactivationactivatecustomersimOutput outputobj = new SimactivationactivatecustomersimOutput();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "Failure";
                        OutputList.Add(outputobj);
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, OutputList);
                }
                

                //}
                //else
                //{
                //    //Email Trigger
                //    if (sitecode == "MCM" || sitecode == "BAU")
                //    {
                //        try
                //        {
                //            var api = new MandrillApi(ConfigurationManager.AppSettings["MAILCHIMPAPIKEY"]);
                //            var message = new MandrillMessage();
                //            message.AddGlobalMergeVars("FNAME", firstname);
                //            message.AddTo(email);
                //            IList<MandrillSendMessageResponse> response = api.Messages.SendTemplateAsync(message, ConfigurationManager.AppSettings["MAILCHIMPMYVECTONEFORGOTPASSWORD_" + sitecode]).Result;
                //            Log.Info("ResetMyAccountPwd Mail Response : " + JsonConvert.SerializeObject(response));
                //        }
                //        catch (Exception ex)
                //        {
                //            Log.Error("ResetMyAccountPwd Mail Error : " + ex.Message);
                //        }
                //    }
                //    //SMS Trigger
                //    if (sitecode == "MCM" || sitecode == "BAU")
                //    {
                //        try
                //        {
                //            string smsURL = CRM_API.DB.SMSCampaign.SProc.GetSMSGatewayInfo(sitecode, msisdn);
                //            var smsResponse = CRM_API.Helpers.SendSMS.SMSSendOurGateway("Vectone", msisdn, ConfigurationManager.AppSettings["SMSMESSAGEMYVECTONEFORGOTPASSWORD"], smsURL);
                //            CRM_API.DB.SMSCampaign.SProc.InsertSMSNotifyText(msisdn, ConfigurationManager.AppSettings["SMSMESSAGEMYVECTONEFORGOTPASSWORD_" + sitecode], crm_user, smsResponse == "-1" ? 0 : 1, "-", firstname, "Reset My Vectone Password");
                //            Log.Info("ResetMyAccountPwd : SMS : Response : " + smsResponse);
                //        }
                //        catch (Exception ex)
                //        {
                //            Log.Error("ResetMyAccountPwd SMS Error : " + ex.Message);
                //        }
                //    }
                //    return Request.CreateResponse(HttpStatusCode.OK, new List<ActivateCustomerSimATOutput>() { new ActivateCustomerSimATOutput { errcode = 0, errmsg = "Success" } });
                //}
            }
            catch (Exception ex)
            {
                Log.Error("ActivateCustomerSimAT  : " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.OK, new List<ActivateCustomerSimATOutput>() { new ActivateCustomerSimATOutput { errcode = -1, errmsg = ex.Message } });
            }
        }
    }
}
