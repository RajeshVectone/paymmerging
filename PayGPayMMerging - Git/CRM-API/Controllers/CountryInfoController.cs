﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CRM_API.Controllers
{
    public class CountryInfoController : ApiController
    {
        // GET api/countryinfo
        public HttpResponseMessage Get()
        {
            return GetAllCountryList();
        }
        public HttpResponseMessage Get(string Id)
        {
            return GetCountryNameBySitecode(Id);
        }
        private HttpResponseMessage GetAllCountryList()
        {
            var result = CRM_API.DB.Country.CountrySProc.GetAllCountries();

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
        private HttpResponseMessage GetCountryNameBySitecode(string Code)
        {
            var result = CRM_API.DB.Country.CountrySProc.GetAllCountries().Where(w => w.Sitecode.Equals(Code)).FirstOrDefault();
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
        //// GET api/countryinfo/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST api/countryinfo
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT api/countryinfo/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/countryinfo/5
        //public void Delete(int id)
        //{
        //}

        //POST api/countryinfo

        
    }
}
