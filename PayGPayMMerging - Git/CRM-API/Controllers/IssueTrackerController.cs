﻿using CRM_API.DB.Common;
using CRM_API.Models.Issue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NLog;
using System.Net.Mail;
using System.Text;
using System.Net.Mime;

namespace CRM_API.Controllers
{
    /// <summary>
    /// Web/Api IssueTracker
    /// </summary>
    public class IssueTrackerController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        // GET api/issuetracker
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        // GET api/issuetracker/xxxxx
        /// <summary>
        /// Get All Issue Tracker by Id Name
        /// </summary>
        /// <param name="id">Value by name</param>
        /// <returns></returns>

        public HttpResponseMessage Get(string id, string val = "", string sitecode = "")
        {
            try
            {
                switch (id)
                {
                    case "problem_category":
                        break;
                    default: break;
                }
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            catch (Exception)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new List<CRM_API.Models.Issue.IssueList>());
            }
        }
        // POST api/issuetracker
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>

        public HttpResponseMessage Post(CRM_API.Models.Issue.IssueModels Model)
        {
            try
            {
                switch (Model.SearchType)
                {
                    case Models.Issue.SearchType.ProblemListAll:
                        return GetIssueTrackerBy(Model);
                    case Models.Issue.SearchType.ProblemDetail://Problem Details
                        return GetIssueDetail(Model);
                    case Models.Issue.SearchType.IssueCategory:
                        return GetIssueCategory(Model);
                    case Models.Issue.SearchType.IssueCategoryDetail:
                        return GetIssueCategoryDetail(Model);
                    case Models.Issue.SearchType.InsertIssue:
                        return InsertIssue(Model);
                    case Models.Issue.SearchType.UpdateIssue:
                        return UpdateIssue(Model);
                    case Models.Issue.SearchType.IssueHistory:
                        return GetIssueHistory(Model);
                    case Models.Issue.SearchType.IssueCategory1://Call Type
                        return GetIssueCategory1(Model);
                    case Models.Issue.SearchType.IssueCategory2://Sub Type
                        return GetIssueCategoryDetails(Model);
                    case Models.Issue.SearchType.IssueCategory3://Category Type
                        return GetComplaint(Model);
                    case Models.Issue.SearchType.InsertIssue1:
                        return IssueCategory4(Model);
                    case Models.Issue.SearchType.DeadCall:
                        return IssueCategorydeadcall(Model);
                    case Models.Issue.SearchType.Contacttype://Contact type
                        return GetContacttype(Model);
                    case Models.Issue.SearchType.compliantpe://Complian type
                        return Getcompliantpe(Model);
                    case Models.Issue.SearchType.intervaltype://inteval type
                        return Getintervaltype(Model);
                    case Models.Issue.SearchType.AssignedTo://inteval type
                        return GetAssignedTo(Model);
                    case Models.Issue.SearchType.AssignPriority://inteval type
                        return GetAssignPriority(Model);
                    default: break;
                }
            }
            catch (Exception)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new CRM_API.Models.Common.ErrCodeMsg());
            }
            return Request.CreateResponse(HttpStatusCode.ExpectationFailed, Model);
        }

        private HttpResponseMessage GetComplaint(IssueModels In)
        {
            IEnumerable<IssueCategory3> problemCategoryDetaip = CRM_API.DB.Issue.SProc.GetProblemCategoryDetaip(In.Function_Id, In.Sitecode);
            return base.Request.CreateResponse<IssueCategory3[]>(HttpStatusCode.OK, problemCategoryDetaip.ToArray<IssueCategory3>());
        }

        private HttpResponseMessage IssueCategory4(IssueModels In)
        {
            ErrCodeMsg errCodeMsg = CRM_API.DB.Issue.SProc.InsertIssu1(In);
            if (errCodeMsg.errcode != 0)
            {
                errCodeMsg.errsubject = "General Error";
            }
            else
            {
                errCodeMsg.errsubject = "Success";

                //31-Dec-2018 : Moorthy : Added for Creating Outlook Meeting
                try
                {
                    if (In.Comp_Status != null && In.Comp_Status.Trim() == "Call Back Arranged")
                    {
                        MailMessage msg = new MailMessage();

                        //  Set up the different mime types contained in the message
                        System.Net.Mime.ContentType textType = new System.Net.Mime.ContentType("text/plain");
                        System.Net.Mime.ContentType HTMLType = new System.Net.Mime.ContentType("text/html");
                        System.Net.Mime.ContentType calendarType = new System.Net.Mime.ContentType("text/calendar");

                        //  Add parameters to the calendar header
                        calendarType.Parameters.Add("method", "REQUEST");
                        calendarType.Parameters.Add("name", "meeting.ics");

                        DateTime start = DateTime.Now;
                        DateTime end = start;
                        string organizerName = "CRM - Admin";
                        string location = "CRM";
                        //28-Jan-2019 : Modified for last point in : CRM Feedback  Call tracking.docx
                        string summary = In.Issue_Desc + " \r\n " + In.interval_comment;
                        string organizerEmail = "noreply@vectonemobile.co.uk";
                        string subject = "Meeting from CRM ";
                        if (In.SIM_Type.ToUpper() == "PAYG")
                        {
                            location = string.Format("http://192.168.1.230:9876/Customer/TabIssuesTracking/{0}-PAYG", In.Mobile_No);
                            subject = subject + " - " + string.Format("http://192.168.1.230:9876/Customer/TabIssuesTracking/{0}-PAYG", In.Mobile_No);
                        }
                        else
                        {
                            location = string.Format("http://192.168.1.230:9876/PAYM/TabIssuesTracking/{0}-PAYM", In.Mobile_No);
                            subject = subject + " - " + string.Format("http://192.168.1.230:9876/PAYM/TabIssuesTracking/{0}-PAYM", In.Mobile_No);
                        }

                        MailAddressCollection attendeeList = new MailAddressCollection();
                        attendeeList.Add(new MailAddress(In.agent_email_id, In.Created_By));
                        //28-Jan-2019 : Added for last point in : CRM Feedback  Call tracking.docx
                        attendeeList.Add(new MailAddress("p.tamilmani@vectone.com", "Praveen Tamilmani"));
                        attendeeList.Add(new MailAddress("k.sivachelvan@vectone.com", "Keerthana Sivachelvan"));
                        attendeeList.Add(new MailAddress("k.Vinayagamoorthy@vectone.com", "Vinayagamoorthy"));
                        attendeeList.Add(new MailAddress("h.dhanasekaran@vectone.com", "Dhanasekaran"));
                        attendeeList.Add(new MailAddress("b.ahamedbasha@vectone.com", "Ahamedbasha"));
                        //attendeeList.Add(new MailAddress("CustomerServiceEscalationTeam@vectone.com", "Customer Service Escalation Team"));

                        if (In.intreval.Contains("mins"))
                            start = start.AddMinutes(Convert.ToDouble(In.intreval.Replace(" mins", "")));
                        else
                            start = start.AddHours(Convert.ToDouble(In.intreval.Replace(" hr", "")));

                        end = start.AddHours(1);

                        //  Create message body parts
                        //  create the Body in text format
                        string bodyText = "Type:Single Meeting\r\nOrganizer: {0}\r\nStart Time:{1}\r\nEnd Time:{2}\r\nTime Zone:{3}\r\nLocation: {4}\r\n\r\n*~*~*~*~*~*~*~*~*~*\r\n\r\n{5}";
                        bodyText = string.Format(bodyText,
                            organizerName,
                            start.ToLongDateString() + " " + start.ToLongTimeString(),
                            end.ToLongDateString() + " " + end.ToLongTimeString(),
                            System.TimeZone.CurrentTimeZone.StandardName,
                            location,
                            summary);

                        AlternateView textView = AlternateView.CreateAlternateViewFromString(bodyText, textType);
                        msg.AlternateViews.Add(textView);

                        //create the Body in HTML format
                        string bodyHTML = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 3.2//EN\">\r\n<HTML>\r\n<HEAD>\r\n<META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; charset=utf-8\">\r\n<META NAME=\"Generator\" CONTENT=\"MS Exchange Server version 6.5.7652.24\">\r\n<TITLE>{0}</TITLE>\r\n</HEAD>\r\n<BODY>\r\n<!-- Converted from text/plain format -->\r\n<P><FONT SIZE=2>Type:Single Meeting<BR>\r\nOrganizer:{1}<BR>\r\nStart Time:{2}<BR>\r\nEnd Time:{3}<BR>\r\nTime Zone:{4}<BR>\r\nLocation:{5}<BR>\r\n<BR>\r\n*~*~*~*~*~*~*~*~*~*<BR>\r\n<BR>\r\n{6}<BR>\r\n</FONT>\r\n</P>\r\n\r\n</BODY>\r\n</HTML>";
                        bodyHTML = string.Format(bodyHTML,
                            summary,
                            organizerName,
                            start.ToLongDateString() + " " + start.ToLongTimeString(),
                            end.ToLongDateString() + " " + end.ToLongTimeString(),
                            System.TimeZone.CurrentTimeZone.StandardName,
                            location,
                            summary);

                        AlternateView HTMLView = AlternateView.CreateAlternateViewFromString(bodyHTML, HTMLType);
                        msg.AlternateViews.Add(HTMLView);

                        //create the Body in VCALENDAR format
                        string calDateFormat = "yyyyMMddTHHmmssZ";
                        string bodyCalendar = "BEGIN:VCALENDAR\r\nMETHOD:REQUEST\r\nPRODID:Microsoft CDO for Microsoft Exchange\r\nVERSION:2.0\r\nBEGIN:VTIMEZONE\r\nTZID:(GMT-06.00) Central Time (US & Canada)\r\nX-MICROSOFT-CDO-TZID:11\r\nBEGIN:STANDARD\r\nDTSTART:16010101T020000\r\nTZOFFSETFROM:-0500\r\nTZOFFSETTO:-0600\r\nRRULE:FREQ=YEARLY;WKST=MO;INTERVAL=1;BYMONTH=11;BYDAY=1SU\r\nEND:STANDARD\r\nBEGIN:DAYLIGHT\r\nDTSTART:16010101T020000\r\nTZOFFSETFROM:-0600\r\nTZOFFSETTO:-0500\r\nRRULE:FREQ=YEARLY;WKST=MO;INTERVAL=1;BYMONTH=3;BYDAY=2SU\r\nEND:DAYLIGHT\r\nEND:VTIMEZONE\r\nBEGIN:VEVENT\r\nDTSTAMP:{8}\r\nDTSTART:{0}\r\nSUMMARY:{7}\r\nUID:{5}\r\nATTENDEE;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;RSVP=TRUE;CN=\"{9}\":MAILTO:{9}\r\nACTION;RSVP=TRUE;CN=\"{4}\":MAILTO:{4}\r\nORGANIZER;CN=\"{3}\":mailto:{4}\r\nLOCATION:{2}\r\nDTEND:{1}\r\nDESCRIPTION:{7}\\N\r\nSEQUENCE:1\r\nPRIORITY:5\r\nCLASS:\r\nCREATED:{8}\r\nLAST-MODIFIED:{8}\r\nSTATUS:CONFIRMED\r\nTRANSP:OPAQUE\r\nX-MICROSOFT-CDO-BUSYSTATUS:BUSY\r\nX-MICROSOFT-CDO-INSTTYPE:0\r\nX-MICROSOFT-CDO-INTENDEDSTATUS:BUSY\r\nX-MICROSOFT-CDO-ALLDAYEVENT:FALSE\r\nX-MICROSOFT-CDO-IMPORTANCE:1\r\nX-MICROSOFT-CDO-OWNERAPPTID:-1\r\nX-MICROSOFT-CDO-ATTENDEE-CRITICAL-CHANGE:{8}\r\nX-MICROSOFT-CDO-OWNER-CRITICAL-CHANGE:{8}\r\nBEGIN:VALARM\r\nACTION:DISPLAY\r\nDESCRIPTION:REMINDER\r\nTRIGGER;RELATED=START:-PT00H15M00S\r\nEND:VALARM\r\nEND:VEVENT\r\nEND:VCALENDAR\r\n";
                        bodyCalendar = string.Format(bodyCalendar,
                            start.ToUniversalTime().ToString(calDateFormat),
                            end.ToUniversalTime().ToString(calDateFormat),
                            location,
                            organizerName,
                            organizerEmail,
                            Guid.NewGuid().ToString("B"),
                            summary,
                            subject,
                            DateTime.Now.ToUniversalTime().ToString(calDateFormat),
                            attendeeList.ToString());

                        AlternateView calendarView = AlternateView.CreateAlternateViewFromString(bodyCalendar, calendarType);
                        calendarView.TransferEncoding = TransferEncoding.SevenBit;
                        msg.AlternateViews.Add(calendarView);

                        //  Adress the message
                        msg.From = new MailAddress(organizerEmail);
                        foreach (MailAddress attendee in attendeeList)
                        {
                            msg.To.Add(attendee);
                        }
                        msg.Subject = subject;

                        SmtpClient sc = new SmtpClient();
                        sc.Send(msg);
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("Outlook : Error : " + ex.Message);
                }
            }
            return base.Request.CreateResponse<ErrCodeMsg>(HttpStatusCode.OK, errCodeMsg);
        }

        private HttpResponseMessage GetIssueCategory1(IssueModels In)
        {
            IEnumerable<IssueCategory1> problemCategory1 = CRM_API.DB.Issue.SProc.GetProblemCategory1(In.Sitecode);
            return base.Request.CreateResponse<IssueCategory1[]>(HttpStatusCode.OK, problemCategory1.ToArray<IssueCategory1>());
        }

        private HttpResponseMessage GetIssueCategoryDetail(IssueModels In)
        {
            IEnumerable<IssueCategoryDetail> problemCategoryDetail = CRM_API.DB.Issue.SProc.GetProblemCategoryDetail(In.Sitecode, In.problemcategorytype, In.categorytypename);
            return base.Request.CreateResponse<IssueCategoryDetail[]>(HttpStatusCode.OK, problemCategoryDetail.ToArray<IssueCategoryDetail>());
        }

        private HttpResponseMessage GetIssueCategoryDetails(IssueModels In)
        {
            IEnumerable<IssueCategory2> problemCategoryDetail1 = CRM_API.DB.Issue.SProc.GetProblemCategoryDetail1(In.Issue_Id, In.Sitecode);
            return base.Request.CreateResponse<IssueCategory2[]>(HttpStatusCode.OK, problemCategoryDetail1.ToArray<IssueCategory2>());
        }

        private HttpResponseMessage GetIssueTrackerBy(CRM_API.Models.Issue.IssueModels In)
        {
            //var result = new List<CRM_API.Models.Issue.IssueList>();
            var result = CRM_API.DB.Issue.SProc.GetIssueList(In.SubscriberID, In.Sitecode, In.mobileno).OrderByDescending(s => s.SubmitDate);
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
        private HttpResponseMessage GetIssueDetail(CRM_API.Models.Issue.IssueModels In)
        {
            var result = CRM_API.DB.Issue.SProc.GetIssueListByID(In.ProblemID.ToString(), In.Sitecode).FirstOrDefault();
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        private HttpResponseMessage GetIssueCategory(CRM_API.Models.Issue.IssueModels In)
        {
            IEnumerable<CRM_API.Models.Issue.IssueCategory> result = CRM_API.DB.Issue.SProc.GetProblemCategory(In.Sitecode);
            return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
        }
        private HttpResponseMessage GetContacttype(CRM_API.Models.Issue.IssueModels In)
        {
            IEnumerable<CRM_API.Models.Issue.Contact_Type> result = CRM_API.DB.Issue.SProc.Getcontacttype(In.ProblemID, In.Sitecode);
            return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
        }
        private HttpResponseMessage Getcompliantpe(CRM_API.Models.Issue.IssueModels In)
        {
            IEnumerable<CRM_API.Models.Issue.Complaint_type> result = CRM_API.DB.Issue.SProc.Getcompliantpe(In.ProblemID, In.Sitecode);
            return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
        }
        private HttpResponseMessage Getintervaltype(CRM_API.Models.Issue.IssueModels In)
        {
            IEnumerable<CRM_API.Models.Issue.timeinterval> result = CRM_API.DB.Issue.SProc.Getintervaltype(In.ProblemID, In.Sitecode);
            return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
        }
        //GetAssignedTo
        private HttpResponseMessage GetAssignedTo(CRM_API.Models.Issue.IssueModels In)
        {
            IEnumerable<CRM_API.Models.Issue.AssignedTo> result = CRM_API.DB.Issue.SProc.GetAssignedTo(In.ProblemID, In.Sitecode);
            return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
        }
        //GetAssignPriority
        private HttpResponseMessage GetAssignPriority(CRM_API.Models.Issue.IssueModels In)
        {
            IEnumerable<CRM_API.Models.Issue.Priority> result = CRM_API.DB.Issue.SProc.GetAssignPriority(In.ProblemID, In.Sitecode);
            return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
        }
        private HttpResponseMessage InsertIssue(CRM_API.Models.Issue.IssueModels In)
        {
            DB.Common.ErrCodeMsg result = CRM_API.DB.Issue.SProc.InsertIssue(In);
            if (result.errcode == 0)
            {
                result.errsubject = "Success";
            }
            else
            {
                result.errsubject = "General Error";
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
        private HttpResponseMessage IssueCategorydeadcall(CRM_API.Models.Issue.IssueModels In)
        {
            DB.Common.ErrCodeMsg result = CRM_API.DB.Issue.SProc.InsertIssudeadcall(In);
            if (result.errcode == 0)
            {
                result.errsubject = "Success";
                result.errmsg = "Issue Has Successfully Added";
            }
            else
            {
                result.errsubject = "General Error";
                result.errmsg = "Can't Add Issue";
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
        private HttpResponseMessage UpdateIssue(CRM_API.Models.Issue.IssueModels In)
        {
            DB.Common.ErrCodeMsg result = CRM_API.DB.Issue.SProc.UpdateIssue(In);
            if (result.errcode == 0)
            {
                result.errsubject = "Success";
            }
            else
            {
                result.errsubject = "General Error";
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        private HttpResponseMessage GetIssueHistory(CRM_API.Models.Issue.IssueModels In)
        {
            IEnumerable<CRM_API.Models.Issue.IssueHistory> result = CRM_API.DB.Issue.SProc.GetIssueHistory(In.Sitecode, In.ProblemID);
            return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
        }
    }
}
