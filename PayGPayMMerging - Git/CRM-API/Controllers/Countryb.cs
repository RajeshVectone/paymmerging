﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CRM_API.Controllers
{
    public class Countryb
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int Code { get; set; }
        public string Currency { get; set; }
        public bool IsSelf { get; set; }
    }
}
