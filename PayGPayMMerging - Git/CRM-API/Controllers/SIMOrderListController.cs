﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using NLog;


namespace CRM_API.Controllers
{
    public class SIMOrderListController : ApiController
    {
        //// POST api/simorderlist/action
        //[HttpPost]
        //public HttpResponseMessage Post([FromUri]string id, [FromBody]object inRequest)
        //{
        //    if (!string.IsNullOrEmpty(id))
        //    {
        //        switch (id.ToLower())
        //        {
        //            case "search":
        //                if (inRequest == null)
        //                    return Request.CreateResponse(HttpStatusCode.OK, new List<CRM_API.Models.SIMOrderItem>());
        //                IEnumerable<CRM_API.Models.SIMOrderItem> listResult =
        //                    CRM_API.DB.SIMOrder.SProc.List(inRequest as CRM_API.Models.SIMOrderRequest);
        //                return Request.CreateResponse(HttpStatusCode.OK, listResult);
        //            case "dispatch":
        //                if (inRequest != null)
        //                {
        //                    CRM_API.DB.Common.ErrCodeMsg resValue =
        //                        CRM_API.DB.SIMOrder.SProc.DoLabelling(inRequest as CRM_API.Models.SIMOrderLabelRequest);
        //                    return Request.CreateResponse(HttpStatusCode.OK, resValue);
        //                }
        //                else
        //                {
        //                    return Request.CreateResponse(HttpStatusCode.PreconditionFailed, new CRM_API.DB.Common.ErrCodeMsg());
        //                }
        //            case "listdispatched":
        //                int maxResult = 20;
        //                int.TryParse(inRequest as string, out maxResult);
        //                IEnumerable<CRM_API.Models.SIMOrderItem> listDispatched = CRM_API.DB.SIMOrder.SProc.ListLastDispatch(maxResult);
        //                return Request.CreateResponse(HttpStatusCode.OK, listDispatched);
        //            case "dolabelling":
        //                if (inRequest != null)
        //                {
        //                    CRM_API.DB.Common.ErrCodeMsg resValue = CRM_API.DB.SIMOrder.SProc.DoLabelling(inRequest as CRM_API.Models.SIMOrderLabelRequest);
        //                    return Request.CreateResponse(HttpStatusCode.OK, resValue);
        //                }
        //                else
        //                {
        //                    return Request.CreateResponse(HttpStatusCode.PreconditionFailed, 
        //                        new CRM_API.DB.Common.ErrCodeMsg());
        //                }
        //            default:
        //                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Invalid action");
        //        }
        //    }
        //    else
        //    {
        //        return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Empty action");
        //    }
        //}

        // GET api/simorderlist/id
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public HttpResponseMessage Get(int id,string sitecode = null)
        {
            Log.Debug("SIMOrderListController.Post: Started");
            Log.Info(string.Format("Id= {0}, sitecode= {1}",id,sitecode??"Null"));
            try
            {
                IEnumerable<CRM_API.Models.FreesimOrderItem> listResult = CRM_API.DB.SIMOrder.SProc.ListLastDispatch2in1(id);
                var result = CRM_API.DB.SIMOrder.SProc.GetFreesimOrderGroup("sentdate", DateTime.Now.ToString("yyyy-MM-dd"),
                    DateTime.Now.AddDays(1).ToString("yyyy-MM-dd"), sitecode).OrderByDescending(o => o.sentdate).Take(id - listResult.Count());
                foreach (CRM_API.Models.FreesimOrderItem item in result)
                {
                    CRM_API.Models.FreesimOrderItem resi = CRM_API.DB.SIMOrder.SProc.SearchByOrderId(((int)item.freesimid).ToString(), "freesimid", sitecode);
                    item.iccid = resi.iccid;
                }
                return Request.CreateResponse(HttpStatusCode.OK, result.Union(listResult));
            }
            catch (Exception e)
            {
                Log.Error(e.Message);
                return Request.CreateResponse(HttpStatusCode.OK, new CRM_API.DB.Common.ErrCodeMsg() { errcode = -1, errmsg = e.Message });
            }
        }

        // POST api/simorderlist
        [HttpPost]
        public HttpResponseMessage Post(CRM_API.Models.SIMOrderLabelRequest labelRequest)
        {
            Log.Debug("SIMOrderListController.Post: Started");
            Log.Info(new JavaScriptSerializer().Serialize(labelRequest));
            try
            {
                if (labelRequest != null)
                {
                    CRM_API.DB.Common.ErrCodeMsg resValue = new DB.Common.ErrCodeMsg() ;
                    if (labelRequest.freesimid.Contains("M"))
                        resValue = CRM_API.DB.SIMOrder.SProc.DoLabelling2in1Order(labelRequest);
                    else
                    {
                        var M = CRM_API.DB.SIMOrder.SProc.SearchByOrderId(labelRequest.freesimid, "freesimid", labelRequest.sitecode);
                        
                        if (M.freesimid == null)
                            throw new Exception("Freesimid Not Found");

                        M.sitecode = labelRequest.sitecode;
                        M.iccid = labelRequest.iccid;

                        if (CRM_API.DB.SIMOrder.SProc.UpdateFreeSimOrder(M, 4, labelRequest.crm_login))
                        {
                            resValue.errcode = 0;resValue.errmsg = "Updated";
                        }
                        else
                            resValue.errcode = -1; resValue.errmsg = "Failed To Update Please Check Iccid.";
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, resValue);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.PreconditionFailed, new CRM_API.DB.Common.ErrCodeMsg());
                }
            }
            catch (Exception e)
            {
                Log.Error(e.Message);
                return Request.CreateResponse(HttpStatusCode.OK, new CRM_API.DB.Common.ErrCodeMsg() { errcode = -1 , errmsg = e.Message });
            }
        }
    }
}
