﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CRM_API.Controllers
{
    public class FreeswitchController : ApiController
    {
        public dynamic Post(Models.Freeswitch.RequestWS ix)
        {
            switch (ix.ReqType)
            {
                case Models.Freeswitch.Action.Login: return DoAuthorize(ix);
                case Models.Freeswitch.Action.Logout: return DoLogout(ix);
                case Models.Freeswitch.Action.CallExt: return DoCallExt(ix);
                case Models.Freeswitch.Action.CallNumber: return DoCallNumber(ix);
                default: break;
            }
            return Request.CreateResponse(HttpStatusCode.OK, ix);
        }
        private dynamic DoAuthorize(Models.Freeswitch.RequestWS ix)
        {
            var result = DB.SProc.Freeswitch.MyAccountLogin(ix.Username, ix.Password);
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
        private dynamic DoCallExt(Models.Freeswitch.RequestWS ix)
        {
            var result = Helpers.Freeswitch.FreeswitchWS.ExtCallExt(ix.DestNumber, ix.OrigExt);
            return Request.CreateResponse(HttpStatusCode.OK, ix);
        }
        private dynamic DoCallNumber(Models.Freeswitch.RequestWS ix)
        {
            var result = Helpers.Freeswitch.FreeswitchWS.ExtCallMobile(ix.DestNumber, ix.OrigExt);
            return Request.CreateResponse(HttpStatusCode.OK, ix);
        }
        private dynamic DoLogout(Models.Freeswitch.RequestWS ix)
        {

            return Request.CreateResponse(HttpStatusCode.OK, ix);
        }
    }
}
