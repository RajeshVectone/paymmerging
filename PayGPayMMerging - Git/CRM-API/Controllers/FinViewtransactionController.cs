﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml;

namespace CRM_API.Controllers
{
    public class FinViewtransactionController : ApiController
    {
        //
        // GET: /FinViewtransaction/

        public HttpResponseMessage Get(string paymentMode)
        {
            try
            {
                //var result = CRM_API.DB.PMBOPayway.getPayWayFiles();
                if (paymentMode == null)
                {
                    //var result = CRM_API.DB.viewtransaction.getpaymentdetails();
                    var result = CRM_API.DB.viewtransaction.getpaymentdetailsdownload();
                    return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
                }
                else if (paymentMode == "100")
                {
                    var result1 = CRM_API.DB.viewtransaction.geterrrCodedownload("get_latest_details_from_tpayment_by_errcode_V6", paymentMode);
                    return Request.CreateResponse(HttpStatusCode.OK, result1.ToArray());
                }
                else if (paymentMode == "101")
                {
                    var result2 = CRM_API.DB.viewtransaction.geterrrCodedownload("get_latest_details_from_tpayment_by_errcode_V7", paymentMode);
                    return Request.CreateResponse(HttpStatusCode.OK, result2.ToArray());
                }
                else if (paymentMode == "200")
                {
                    var result3 = CRM_API.DB.viewtransaction.geterrrCodedownload("get_latest_details_from_tpayment_by_errcode_V8", paymentMode);
                    return Request.CreateResponse(HttpStatusCode.OK, result3.ToArray());
                }
                else if (paymentMode == "201")
                {
                    var result4 = CRM_API.DB.viewtransaction.geterrrCodedownload("get_latest_details_from_tpayment_by_errcode_V9", paymentMode);
                    return Request.CreateResponse(HttpStatusCode.OK, result4.ToArray());
                }
                else if (paymentMode == "301")
                {
                    //var result = CRM_API.DB.viewtransaction.getpaymentdetails();
                    var result = CRM_API.DB.viewtransaction.getpaymentdetailsautotop();
                    return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
                }
                return Request.CreateResponse(HttpStatusCode.OK, new List<CRM_API.Models.financeviewtransdownload>());

            }
            catch (WebException ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new List<CRM_API.Models.financeviewtrans>());
            }
        }

        //POST : api/FinViewtransaction      

        public HttpResponseMessage Post(CRM_API.Models.Values objValues)
        {
            try
            {
                //Calling the SP
                var result = CRM_API.DB.viewtransaction.getMerchentIDs(objValues.merchID.ToString());
                return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
            }
            catch (WebException ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new List<CRM_API.Models.financeviewtrans>());
            }
        }

        //PUT : api/FinViewtransaction   
        //public HttpResponseMessage Post(CRM_API.Models.Sucess_failed objSucess_failed)
        //{
        //    try
        //    {
        //        // // 0-- Create 1 -- revert
        //        //if (objSucess_failed.Create_revert == "1")
        //        //{
        //        //    var result = CRM_API.DB.viewtransaction.getTransaction("http://82.113.74.28:8022/Transaction.svc/subscription/revCreate?referenceId=" + objSucess_failed.MerchID);
        //        //    if (result)
        //        //        //var result = CRM_API.DB.viewtransaction.getMerchentIDs(objValues.merchID.ToString());
        //        //        return Request.CreateResponse(HttpStatusCode.OK, CRM_API.DB.viewtransaction.afterpayment(objSucess_failed.MerchID, objSucess_failed.Reason, "payment reversed successfully").ToArray());
        //        //    else
        //        //        return Request.CreateResponse(HttpStatusCode.OK, CRM_API.DB.viewtransaction.afterpayment(objSucess_failed.MerchID, objSucess_failed.Reason, "payment reversed failure").ToArray());
        //        //}
        //        //else
        //        //{
        //        //    var result = CRM_API.DB.viewtransaction.getTransaction("http://82.113.74.28:8022/Transaction.svc/subscription/dmCreate?referenceId=" + objSucess_failed.MerchID);
        //        //    if (result)
        //        //        return Request.CreateResponse(HttpStatusCode.OK, CRM_API.DB.viewtransaction.afterpayment(objSucess_failed.MerchID, objSucess_failed.Reason, "payment credit successfully").ToArray());
        //        //    else
        //        //        return Request.CreateResponse(HttpStatusCode.OK, CRM_API.DB.viewtransaction.afterpayment(objSucess_failed.MerchID, objSucess_failed.Reason, "payment credit failure").ToArray());
        //        //}
        //        return Request.CreateResponse(HttpStatusCode.OK, CRM_API.DB.viewtransaction.afterpayment(objSucess_failed.MerchID, objSucess_failed.Reason, "payment reversed successfully").ToArray());
        //    }
        //    catch (WebException ex)
        //    {
        //        return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new List<CRM_API.Models.getafterpaymentdetails>());
        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, new List<CRM_API.Models.getafterpaymentdetails>());
        //}
    }
}
