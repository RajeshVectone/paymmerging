﻿using System.Configuration;
using System.Net.Mail;
using AutoMapper;
using System.IO;
using CRM_API.DB.Common;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using CRM_API.Models;
using CRM_API.DB.LLOTG;
using System.Web;

namespace CRM_API.Controllers
{
    public class LLOTGController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        /// <summary>
        /// LLOTG Get
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="x"></param>
        /// <returns></returns>
        public async Task<dynamic> Get([FromUri]string Id, Models.Request x, [FromUri]string country, [FromUri]string paytype, [FromUri]string sitecode)
        {
            string logStrTmp = string.Format("Start: {0} | Param: {1}", this.Request.RequestUri.ToString(), Id);
            Models.Common.ErrCodeMsg result = new Models.Common.ErrCodeMsg();

            try
            {
                switch (Id)
                {
                    case "GetPlanpackage":
                        return await GetPlanpackage(country, paytype, sitecode);
                    case "GetViewSubscriptionHistory":
                        return await GetViewSubscriptionHistory(x);
                    //case "ChangePayMode":
                    //    return await ChangePayMode(x);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new Models.Common.ErrCodeMsg()
                {
                    errcode = -1,
                    errmsg = e.Message
                });
            }
            return await Task.Run(() =>
            {
                return Request.CreateResponse(HttpStatusCode.OK);
            });
        }

        /// <summary>
        /// LLOTG Post
        /// </summary>
        /// <param name="Req"></param>
        /// <returns></returns>
        public async Task<dynamic> Post(Models.Request Req)
        {
            string msgStr = string.Format("{0} | Message : {1}", Request.RequestUri.ToString(), JsonConvert.SerializeObject(Req));
            Log.Info(msgStr);
            try
            {
                switch (Req.Action)
                {
                    case Models.LLOTG_Action.List: return await GetList(Req);
                    case Models.LLOTG_Action.ViewSubscriptionHistory: return await GetViewSubscriptionHistory(Req);
                    case Models.LLOTG_Action.CancelSubscription: return await DoCancelSubscription(Req);
                    case Models.LLOTG_Action.getSavedCC: return await GetCardList(Req);
                    case Models.LLOTG_Action.PayByBalance: return await PayByBalance(Req);
                    case Models.LLOTG_Action.GetUserBalance: return await GetUserBalance(Req);
                    case Models.LLOTG_Action.Reactivate: return await Reactivate(Req);
                    case Models.LLOTG_Action.PayByExisting: return await PaymentByCard(Req);
                    case Models.LLOTG_Action.PayByNewCard: return await SaveProductandCCDetail(Req);
                    case Models.LLOTG_Action.ChangePayMode: return await ChangePayMode(Req);
                    case Models.LLOTG_Action.SuspendedtoCancelled: return await GetSuspendedtocancel(Req);
                }
            }
            catch (Exception e)
            {
                string errStr = string.Format("{0} | Error : {1}", Request.RequestUri.ToString(), e.Message);
                Log.Error(errStr);
                return Request.CreateResponse(HttpStatusCode.OK, new Models.Common.ErrCodeMsg()
                {
                    errcode = -1,
                    errmsg = errStr
                });
            }
            return await Task.Run(() =>
            {
                return Request.CreateResponse(HttpStatusCode.OK, Req);
            });
        }


        /// <summary>
        /// LLOTG suspended to cancel 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        private async Task<HttpResponseMessage> GetSuspendedtocancel(Models.Request x)
        {
            return await Task.Run(() =>
            {
                var result = CRM_API.DB.LLOTG.SProc.Suspendedtocancel(x.MobileNo, x.Sitecode, x.LoggedUser);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            });
        }


        /// <summary>
        /// LLOTG Change Pay Mode
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        private async Task<HttpResponseMessage> ChangePayMode(Models.Request x)
        {
            return await Task.Run(() =>
            {
                var result = CRM_API.DB.LLOTG.SProc.ChangePayMode(x.MobileNo, x.Sitecode, x.LoggedUser);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            });
        }
        /// <summary>
        /// LLOTG Get View Subscription History
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        private async Task<HttpResponseMessage> GetViewSubscriptionHistory(Models.Request x)
        {
            return await Task.Run(() =>
            {
                var result = CRM_API.DB.LLOTG.SProc.GetViewSubscriptionHistory(x.MobileNo, x.Reg_Number, x.Sitecode).Result;
                return Request.CreateResponse(HttpStatusCode.OK, result);
            });
        }
        /// <summary>
        /// LLOTG Reactivate
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        private async Task<HttpResponseMessage> Reactivate(Models.Request x)
        {
            return await Task.Run(() =>
            {
                var result = CRM_API.DB.LLOTG.SProc.Reactivate(x.MobileNo, x.Sitecode, x.LoggedUser, x.mode);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            });
        }
        /// <summary>
        /// LLOTG GetUserBalance
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        private async Task<HttpResponseMessage> GetUserBalance(Models.Request x)
        {
            return await Task.Run(() =>
            {
                var result = CRM_API.DB.LLOTG.SProc.GetUserBalance(x.MobileNo, x.Sitecode, x.LoggedUser).Result;
                return Request.CreateResponse(HttpStatusCode.OK, result);
            });
        }
        /// <summary>
        /// LLOTG Pay By Balance
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        private async Task<HttpResponseMessage> PayByBalance(Models.Request x)
        {
            return await Task.Run(() =>
            {
                var result = CRM_API.DB.LLOTG.SProc.PayByBalance(x.MobileNo, x.LoggedUser, x.Sitecode);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            });
        }
        /// <summary>
        /// LLOTG Cancel Subscription
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        private async Task<HttpResponseMessage> DoCancelSubscription(Models.Request x)
        {
            return await Task.Run(() =>
            {
                var result = CRM_API.DB.LLOTG.SProc.DoCancelSubscription(x.MobileNo, x.Sitecode, x.LoggedUser);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            });
        }
        /// <summary>
        /// LLOTG Get saved Card List
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        private async Task<HttpResponseMessage> GetCardList(Models.Request x)
        {
            return await Task.Run(() =>
            {
                var result = CRM_API.DB.LLOTG.SProc.GetCardList(x.MobileNo, x.Sitecode).Result;
                return Request.CreateResponse(HttpStatusCode.OK, result);
            });
        }
        /// <summary>
        /// LLOTG GetPlanpackage
        /// </summary>
        /// <returns></returns>
        private async Task<HttpResponseMessage> GetPlanpackage(string country, string paytype, string sitecode)
        {
            //IList<Models.LOTG.Collection> result = new List<Models.LOTG.Collection>();
            return await Task.Run(() =>
            {
                var result = CRM_API.DB.LLOTG.SProc.getLLOTGPlans(country, paytype, sitecode).Result;
                return Request.CreateResponse(HttpStatusCode.OK, result);
            });
        }
        /// <summary>
        /// LLOTG grid GetList
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        private async Task<HttpResponseMessage> GetList(Models.Request x)
        {
            //IList<Models.LOTG.Collection> result = new List<Models.LOTG.Collection>();
            return await Task.Run(() =>
            {
                var result = CRM_API.DB.LLOTG.SProc.GetCollectionAsync(x.MobileNo, x.Sitecode).Result;
                return Request.CreateResponse(HttpStatusCode.OK, result);
            });
        }
        /// <summary>
        /// LLOTG existing PaymentByCard
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        private async Task<HttpResponseMessage> PaymentByCard(Models.Request x)
        {

            string[] inputValues = x.LoggedUser.Split(',');
            string CardNumber = inputValues[2].Replace(" ", "");
            string CardCVV = inputValues[3];
            string CLI = x.MobileNo;
            string Sitecode = x.Sitecode;
            string Reg_Number = inputValues[4];
            string LLOTG_country = inputValues[5];
            string LLOTG_paymode = inputValues[6];
            string Brand = inputValues[0];
            string amount = inputValues[1];
            string svc_id = inputValues[7];
            int mode = x.mode;
            string IpAdd = System.Web.HttpContext.Current.Request.UserHostAddress;
            string mailtemplate = "";

            CTPHomeSaverResultViewModel _ctpHomeSaverResultViewModel = GetPaymentByCard(CardNumber, CardCVV, CLI, Sitecode, Brand, amount, IpAdd, Reg_Number, LLOTG_country, LLOTG_paymode, mailtemplate, svc_id, mode);
            var resy = new ErrCodeMsg { errcode = _ctpHomeSaverResultViewModel.errcode, errmsg = _ctpHomeSaverResultViewModel.errmsg, errsubject = _ctpHomeSaverResultViewModel.errsubject };

            return await Task.Run(() =>
            {
                return Request.CreateResponse(HttpStatusCode.OK, _ctpHomeSaverResultViewModel);
            });

        }
        /// <summary>
        /// LLOTG GetPaymentByCard
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        private CTPHomeSaverResultViewModel GetPaymentByCard(string CardNumber, string CardCVV, string CLI, string Sitecode, string Brand, string amount, string IpAdd, string Reg_Number, string LLOTG_country, string LLOTG_paymode, string mailtemplate, string svc_id, int mode)
        {


            CRM_API.DB.LLOTG.Payment pay = new CRM_API.DB.LLOTG.Payment();

            CTPHomeSaverPostModel _ctpHomeSaverPostModel = new CTPHomeSaverPostModel();

            _ctpHomeSaverPostModel.CLI = CLI;

            _ctpHomeSaverPostModel.CardCVV = CardCVV;
            _ctpHomeSaverPostModel.CardNumber = CardNumber;
            _ctpHomeSaverPostModel.ApplicationCode = Brand;
            _ctpHomeSaverPostModel.SiteCode = Sitecode;

            _ctpHomeSaverPostModel.CostTotal = Convert.ToDouble(amount);

            _ctpHomeSaverPostModel.IpAdd = System.Web.HttpContext.Current.Request.UserHostAddress;


            var cardDetails = pay.GetCardList(CLI, Sitecode);


            _ctpHomeSaverPostModel.Currency = cardDetails.Where(c => c.CardNumber == CardNumber).Select(m => m.Currency).SingleOrDefault().ToString();
            _ctpHomeSaverPostModel.Email = "s.anandhakumar@vectone.com";

            CTPHomeSaverResultViewModel _ctpHomeSaverResultViewModel = ReactivateBundleByExistingCard(_ctpHomeSaverPostModel, Reg_Number, LLOTG_country, LLOTG_paymode, amount, mailtemplate, svc_id, mode);

            return _ctpHomeSaverResultViewModel;
        }
        /// <summary>
        /// LLOTG ReactivateBundleByExistingCard
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        private CTPHomeSaverResultViewModel ReactivateBundleByExistingCard(CTPHomeSaverPostModel inputData, string Reg_Number, string LLOTG_country, string LLOTG_paymode, string amount, string mailtemplate, string svc_id, int mode)
        {
            Log.Info("Start ReactivateBundleByExistingCard :");
            Log.Info(new JavaScriptSerializer().Serialize(inputData));
            inputData.errcode = -1;
            inputData.errmsg = "General Exception";

            CRM_API.DB.LLOTG.Payment csPayment = new CRM_API.DB.LLOTG.Payment();
            var result = new CTPHomeSaverResultViewModel();

            CRM_API.Models.Common.ErrCodeMsg errCodeMsg = csPayment.validation(inputData.CLI, inputData.SiteCode, Reg_Number + "," + svc_id + "," + LLOTG_paymode, mode);
            if (errCodeMsg != null && errCodeMsg.errcode == 0)
            {
                //Check empty or null value for payment
                if (string.IsNullOrWhiteSpace(inputData.CardCVV)
                    || string.IsNullOrWhiteSpace(inputData.CardNumber)
                    || string.IsNullOrWhiteSpace(inputData.SiteCode) || string.IsNullOrWhiteSpace(inputData.ApplicationCode)
                    || string.IsNullOrWhiteSpace(inputData.Email) || string.IsNullOrWhiteSpace(inputData.Currency))
                {

                    result.errcode = -1;
                    result.errmsg = "Can't process payment. Some parameter has empty or null value.";
                    Log.Info("Pay After 3Ds : Some Parameter has empty or null value");
                    return result;
                }

                //payment
                csPayment.SubcriptionCharge(inputData.SiteCode, inputData.ApplicationCode, inputData.CLI, inputData.CardNumber, inputData.Currency, inputData.CostTotal, inputData.CardCVV);

                //If payment success

                if (csPayment.ReasonCode == "0")
                {

                    if (Reg_Number != null)
                    {

                        var msg = CRM_API.DB.LLOTG.SProc.Reactivate(inputData.CLI, inputData.SiteCode, Reg_Number + "," + svc_id + "," + LLOTG_paymode, mode);
                        result.errcode = msg.errcode;
                        result.errmsg = msg.errmsg;
                        result.errsubject = msg.errsubject;
                    }
                    else
                    {
                        result.errcode = -1;
                        result.errmsg = "User IDX Not Found";
                    }
                }
                else
                {
                    result.errcode = -1;
                }
                Log.Info("End ReactivateBundleByExistingCard :");
                Log.Info(new JavaScriptSerializer().Serialize(result));
            }
            else
            {
                result.errcode = -1;
                result.errmsg = errCodeMsg.errmsg;
                result.errsubject = errCodeMsg.errsubject;
                Log.Info(new JavaScriptSerializer().Serialize(result));
            }
            return result;

        }
        /// <summary>
        /// LLOTG Save new ProductandCCDetail
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        private async Task<HttpResponseMessage> SaveProductandCCDetail(Models.Request x)
        {


            string MobileNo = x.MobileNo;
            string Sitecode = x.Sitecode;
            int mode = x.mode;
            string[] cancelValues = x.LoggedUser.Split('^');
            string PaymentMethodCardType = cancelValues[0];
            string PaymentMethodCCNumber = cancelValues[1];
            string PaymentMethodFirstName = cancelValues[2];
            string PaymentMethodLastName = cancelValues[3];
            string PaymentMethodExpiryMonth = cancelValues[4];
            string PaymentMethodExpiryYear = cancelValues[5];
            string PaymentMethodCardVerfCode = cancelValues[6];
            string PaymentMethodHouseNo = cancelValues[7];
            string PaymentMethodAddress = cancelValues[8];
            string PaymentMethodCity = cancelValues[9];
            string PaymentMethodCountry = cancelValues[10];
            string PaymentMethodPostcode = cancelValues[11];
            string PaymentMethodEmail = cancelValues[12];
            string PaymentMethodPhone = cancelValues[13];
            string PaymentMethodIssueNumber = cancelValues[14];
            string amount = cancelValues[15];
            string Brand = cancelValues[16];
            string regno = cancelValues[17];
            string paymode = cancelValues[18];
            string svcid = cancelValues[19];
            string currency = cancelValues[20];
            string mailtemplate = "";

            CTPHomeSaverResultViewModel returnValue = GetSaveProductandCCDetail(PaymentMethodCardType, PaymentMethodCCNumber, PaymentMethodExpiryMonth, PaymentMethodExpiryYear, PaymentMethodIssueNumber, PaymentMethodCardVerfCode, PaymentMethodFirstName, PaymentMethodLastName, PaymentMethodPostcode, PaymentMethodHouseNo, PaymentMethodAddress, PaymentMethodCity, PaymentMethodCountry, PaymentMethodEmail, PaymentMethodPhone, MobileNo, Brand, amount, mailtemplate, regno, Sitecode, paymode, svcid, currency, mode);
            var resy = new CRM_API.DB.Common.ErrCodeMsg { errcode = returnValue.errcode, errmsg = returnValue.errmsg, };
            //var resy = new Mvc.Code.Common.ErrCodeMsgExt { errcode = 0, errmsg = "Success", Text = "test" };

            return await Task.Run(() =>
            {
                return Request.CreateResponse(HttpStatusCode.OK, returnValue);
            });

        }
        /// <summary>
        /// GetSaveProductandCCDetail
        /// </summary>
        /// <param name="cardName"></param>
        /// <param name="cardNumber"></param>
        /// <param name="cardExpireMonth"></param>
        /// <param name="cardExpireYear"></param>
        /// <param name="IssueNo"></param>
        /// <param name="cardCVV"></param>
        /// <param name="cardFirstName"></param>
        /// <param name="cardLastName"></param>
        /// <param name="PaymentMethodPostcode"></param>
        /// <param name="PaymentMethodHouseNo"></param>
        /// <param name="cardBillAddress"></param>
        /// <param name="cardCity"></param>
        /// <param name="cardCountry"></param>
        /// <param name="Email"></param>
        /// <param name="Phone"></param>
        /// <param name="CLI"></param>
        /// <param name="Brand"></param>
        /// <param name="amount"></param>
        /// <param name="mailtemplate"></param>
        /// <param name="regno"></param>
        /// <param name="Sitecode"></param>
        /// <param name="paymode"></param>
        /// <returns></returns>
        private CTPHomeSaverResultViewModel GetSaveProductandCCDetail(string cardName, string cardNumber, string cardExpireMonth, string cardExpireYear, string IssueNo, string cardCVV, string cardFirstName, string cardLastName, string PaymentMethodPostcode, string PaymentMethodHouseNo, string cardBillAddress, string cardCity, string cardCountry, string Email, string Phone, string CLI, string Brand, string amount, string mailtemplate, string regno, string Sitecode, string paymode, string svcid, string currency, int mode)
        {
            CRM_API.DB.LLOTG.Payment csPayment = new CRM_API.DB.LLOTG.Payment();

            CTPHomeSaverPostModel cardModel = new CTPHomeSaverPostModel();
            //Payment csPayment = new Payment();
            cardModel.CardNumber = cardNumber;
            cardModel.CardType = cardName;
            cardModel.CardExpireMonth = cardExpireMonth;
            cardModel.CardExpireYear = cardExpireYear;
            cardModel.CardFirstName = cardFirstName;
            cardModel.CardLastName = cardLastName;
            cardModel.CardCity = cardCity;
            cardModel.CardCountry = cardCountry;
            cardModel.CardBillAddress = cardBillAddress;
            cardModel.CardCVV = cardCVV;
            cardModel.Email = Email;
            cardModel.CardPostCode = PaymentMethodPostcode;
            cardModel.CardNumber = cardNumber;
            cardModel.CardExpireMonth = cardExpireMonth;
            cardModel.CardExpireYear = cardExpireYear;
            cardModel.CardFirstName = cardFirstName;
            cardModel.CardLastName = cardLastName;
            cardModel.CardBillAddress = cardBillAddress;
            cardModel.CardCity = cardCity;
            cardModel.CardCountry = cardCountry;
            cardModel.CardCVV = cardCVV;
            cardModel.CardPostCode = PaymentMethodPostcode;
            cardModel.CardPhone = Phone;
            //cardModel.IpAdd = System.Web.HttpContext.Current.Request.UserHostAddress;
            //Need to uncomment while publish

            string hostName = Dns.GetHostName(); // Retrive the Name of HOST
            //cardModel.IpAdd = Dns.GetHostByName(hostName).AddressList[0].ToString();
            cardModel.IpAdd = "192.168.1.230";

            //cardModel.SubscriberId = SubscriberId;


            //string FandFNoTmp = FandFNo;
            //if (FandFNo.Substring(0, 2) == "00")
            //    FandFNoTmp = FandFNo.Substring(2);
            //int regId = csPayment.GetIdxUser(AccountNo, FandFNoTmp, Brand);
            //ProductModel product = csPayment.GetSitecodeByMobile(Brand);
            //String brandType = product.mundio_product;
            //BundleModel GetBundleDescription = csPayment.GetBundleDescription(regno.ToString(), Brand);

            cardModel.SiteCode = Sitecode;
            cardModel.ApplicationCode = Brand;
            //cardModel.AccountId = AccountNo;
            cardModel.CLI = CLI;


            cardModel.Currency = currency;

            if (!(String.IsNullOrEmpty(amount)))
            {
                cardModel.CostTotal = Double.Parse(amount);
            }
            else
            {
                cardModel.CostTotal = 0;
            }
            cardModel.Bypass3Ds = true;
            cardModel.state = "";
            //cardModel.c
            /*
            inputData.CardCVV = CVVNo;
            inputData.SiteCode = "";
            inputData.ApplicationCode = "";
            inputData.AccountId = "";
            inputData.CardNumber = creditCardNo;
            inputData.Currency = "";
            inputData.CostTotal =Double.Parse( currentBalance);
             */
            var returnValue = ReactivateByNewCard(cardModel, regno, mailtemplate, paymode, amount, svcid, mode);
            return returnValue;

        }
        /// <summary>
        /// ReactivateByNewCard
        /// </summary>
        /// <param name="inputData"></param>
        /// <param name="regno"></param>
        /// <param name="mailtemplate"></param>
        /// <param name="paymode"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        private CTPHomeSaverResultViewModel ReactivateByNewCard(CTPHomeSaverPostModel inputData, string regno, string mailtemplate, string paymode, string amount, string svcid, int mode)
        {
            CRM_API.DB.LLOTG.Payment csPayment = new CRM_API.DB.LLOTG.Payment();
            CTPHomeSaverResultViewModel result = new CTPHomeSaverResultViewModel();
            CRM_API.Models.Common.ErrCodeMsg errCodeMsg = csPayment.validation(inputData.CLI, inputData.SiteCode, regno, mode);
            if (errCodeMsg != null && errCodeMsg.errcode == 0)
            {
                Log.Info("Start ReactivateByNewCard:");
                Log.Info(new JavaScriptSerializer().Serialize(inputData)); Log.Info("Start PayWithCard:");

                //CTPHomeSaverResultViewModel result = new CTPHomeSaverResultViewModel();
                inputData.SourceReg = "LLOM_CRM";
                string stackTrace = new JavaScriptSerializer().Serialize(inputData);
                //Payment csPayment = new Payment();

                FamilyNumber familyNumber = new FamilyNumber()
                {
                    Number = inputData.DestinationNumber,
                    Name = string.Format("{0} {1}", inputData.DestinationFirstName, inputData.DestinationLastName),
                    LastName = inputData.DestinationLastName,
                    FirstName = inputData.DestinationFirstName,
                    Country = inputData.CardCountry,
                    PostCode = inputData.DestinationPostCode,
                    Street = inputData.DestinationStreet,
                    Town = inputData.DestinationTown
                };
                result.Country = inputData.CardCountry;
                result.DestinationNumber = inputData.DestinationNumber;
                result.MainNumber = inputData.MainNumber;
                inputData.CostTotal = inputData.CostTotal;



                result.PayReference = csPayment.MerchantReferenceCode;
                csPayment.Authorize(
        new Models.CardDetails()
        {
            CardAddress = inputData.CardBillAddress,
            CardCity = inputData.CardCity,
            CardCountry = inputData.CardCountry,
            CardCVV = inputData.CardCVV,
            CardExpireMonth = inputData.CardExpireMonth,
            CardExpireYear = inputData.CardExpireYear,
            CardFirstName = inputData.CardFirstName,
            CardLastName = inputData.CardLastName,
            CardNumber = inputData.CardNumber,
            CardType = inputData.CardType,
            CardPhone = inputData.CardPhone,
            CardPostCode = inputData.CardPostCode
        }, inputData.SiteCode, inputData.ApplicationCode, inputData.CLI, inputData.Email, inputData.Currency, inputData.CostTotal, inputData.Bypass3Ds, inputData.state, inputData.IpAdd);

                //reason code 475 = go to 3D secure page
                if (csPayment.ReasonCode == "475")
                {
                    Log.Debug("Entering 3Ds");

                    result.errcode = 0;
                    result.errmsg = "REDIRECT TO 3DS";
                    result.AcsURL = csPayment.AcsURL;
                    result.MerchantRefCode = csPayment.MerchantReferenceCode;
                    result.PaReq = csPayment.PaReq;
                    result.ReasonCode = csPayment.ReasonCode;
                    result.SubscriptionId = csPayment.SubscriptionId;
                    result.Decision = csPayment.Decision;
                    result.Description = csPayment.Description;
                    return result;
                }

                if (csPayment.Decision == "ACCEPT")
                {
                    //If payment success
                    if (csPayment.ReasonCode == "100")
                    {
                        //string FandFNoTmp = FandFNo;
                        //if (FandFNo.Substring(0, 2) == "00")
                        //    FandFNoTmp = FandFNo.Substring(2);
                        //int UserIDX = csPayment.GetIdxUser(inputData.AccountId, FandFNoTmp, inputData.ApplicationCode);
                        if (regno != null)
                        {
                            var msg = CRM_API.DB.LLOTG.SProc.Reactivate(inputData.CLI, inputData.SiteCode, regno + "," + svcid + "," + paymode, mode);
                            result.errcode = msg.errcode;
                            if (result.errcode == 0)
                            {
                                result.errmsg = "Reactivation Successfully Done.";
                            }
                            else
                            {
                                result.errmsg = msg.errmsg;
                            }                            
                        }
                        else
                        {
                            result.errcode = -1;
                            result.errmsg = "User IDX Not Found";
                        }
                    }
                    else
                    {
                        result.errcode = -1;
                    }

                    if (result.errcode == 0)
                    {

                        //result.errmsg=
                    }
                    Log.Info("End ReactivateByNewCard :");

                    //ErrMsg resMsg = new ErrMsg();
                    /*  try
                      {
                          if (csPayment.RegisterWithPayment(new FamilySaverRegistration()
                          {
                              Login = inputData.AccountId,
                              Main = new CliNumber() { Number = inputData.MainNumber, Type = csPayment.GetCliType(inputData.MainNumber, inputData.SiteCode) },
                              Number1 = new CliNumber() { Number = inputData.AdditionalNumber1, Type = csPayment.GetCliType(inputData.AdditionalNumber1, inputData.SiteCode) },
                              Number2 = new CliNumber() { Number = inputData.AdditionalNumber2, Type = csPayment.GetCliType(inputData.AdditionalNumber2, inputData.SiteCode) },
                              Number3 = new CliNumber() { Number = inputData.AdditionalNumber3, Type = csPayment.GetCliType(inputData.AdditionalNumber3, inputData.SiteCode) },

                              FamilyNumber = familyNumber,
                              SourceReg = inputData.SourceReg,
                              Amount = (float)inputData.CostTotal,
                              PayMode = 2,
                              PayRef = csPayment.MerchantReferenceCode,
                              HistoryId = 0,
                              ProductId = inputData.ApplicationCode
                          }
                          , out resMsg))
                          {

                              result.errcode = 0;
                              result.errmsg = resMsg.Message;
                              result.MerchantRefCode = csPayment.MerchantReferenceCode;
                              result.PaReq = csPayment.PaReq;
                              result.ReasonCode = csPayment.ReasonCode;
                              result.SubscriptionId = csPayment.SubscriptionId;
                              result.Decision = csPayment.Decision;
                              result.Description = csPayment.Description;
                          }
                          else
                          {
                              Log.Info(result.errmsg);
                              result.errcode = 0;
                              result.errmsg = "Payment Success";
                              result.AcsURL = csPayment.AcsURL;
                              result.MerchantRefCode = inputData.referenceCode;
                              result.PaReq = csPayment.PaReq;
                              result.ReasonCode = csPayment.ReasonCode;
                              result.SubscriptionId = csPayment.SubscriptionId;
                              result.Decision = csPayment.Decision;
                              result.Description = csPayment.Description;
                          }
                      }
                      catch (Exception ex)
                      {
                          Log.Info(ex);
                          string email = "s.anandhakumar@vectone.com";
                          result.errcode = 0;
                          result.errmsg = "Payment Success";
                          result.AcsURL = csPayment.AcsURL;
                          result.MerchantRefCode = inputData.referenceCode;
                          result.PaReq = csPayment.PaReq;
                          result.ReasonCode = csPayment.ReasonCode;
                          result.SubscriptionId = csPayment.SubscriptionId;
                          result.Decision = csPayment.Decision;
                          result.Description = csPayment.Description;
                      }*/
                }

                else
                {
                    result.MerchantRefCode = csPayment.MerchantReferenceCode;
                    result.Description = csPayment.Description;
                    if (result.Description.ToUpper().Contains("FAILED") || result.Description.ToUpper().Contains("FAILS"))
                    {
                        result.errmsg = "Payment Failed";
                    }
                    result.errcode = -1;
                }
                Log.Info("End PayWithCard");
                return result;
            }
            else
            {
                result.errcode = -1;
                result.errmsg = errCodeMsg.errmsg;
                result.errsubject = errCodeMsg.errsubject;
                Log.Info(result.errmsg);
                return result;
            }
        }

        private void SendMail(string msg, string title, string toAddress)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(ConfigurationSettings.AppSettings["SMTPServer"].ToString());
                if (!String.IsNullOrEmpty(toAddress))
                {
                    foreach (var address in toAddress.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        mail.To.Add(address);
                    }
                }
                else
                {
                    Log.Info("No Mail id find for so we send r.murali@vectone.com");
                    mail.To.Add(new MailAddress("r.murali@vectone.com"));
                }

                mail.From = new MailAddress(ConfigurationSettings.AppSettings["EMAILDONOTREPLY"].ToString());

                mail.Bcc.Add(new MailAddress("s.anandhakumar@vectone.com"));
                mail.Bcc.Add(new MailAddress("r.murali@vectone.com"));
                mail.Bcc.Add(new MailAddress("p.benney@vectone.com"));
                mail.Bcc.Add(new MailAddress("p.loganathan@vectone.com"));
                mail.Bcc.Add(new MailAddress("d.tharmasirirajah@mundio.com"));
                mail.Subject = title;

                mail.IsBodyHtml = true;
                string htmlBody;

                htmlBody = msg;

                mail.Body = htmlBody;

                //SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential(ConfigurationSettings.AppSettings["SMTPLogin"].ToString(), ConfigurationSettings.AppSettings["SMTPPassword"].ToString());
                // SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
            }
            catch (Exception e)
            {
                Log.Error(e.StackTrace.ToString());
            }
        }
        //payment by Rajesh 
        private void GeneralChecking(Models.Request x)
        {
            if (string.IsNullOrEmpty(x.MobileNo))
                throw new Exception("Mobile Number Cannot Empty.");
            if (string.IsNullOrEmpty(x.Sitecode))
                throw new Exception("Sitecode Cannot Empty.");
            if (string.IsNullOrEmpty(x.LoggedUser))
                throw new Exception("Required Username.");
        }
    }
}
