﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CRM_API.Controllers
{
    /// <summary>
    /// PMBO PayWay Get the Values.
    /// /api/PMBOPayWay
    /// </summary>
    public class PMBOPayWayController : ApiController
    {
        /// <summary>
        /// PMBO PayWay Get the Values
        /// /api/PMBOPayWay/
        /// </summary>
        public HttpResponseMessage Get(string sitecode , string ind )
        {
            try
            {
                
                if (ind != null && ind != "")
                {
                    if (ind == "1")
                    {
                        var result = CRM_API.DB.PMBOPayway.goSubmit(sitecode);
                        return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
                    }
                    else if (ind == "2")
                    {
                        var result = CRM_API.DB.PMBOPayway.goSuccess(sitecode);
                        return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
                    }
                    else if (ind == "3")
                    {
                        var result = CRM_API.DB.PMBOPayway.goReject(sitecode);
                        return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new List<CRM_API.Models.PMBODDOperations>());
                    }
                }
                else
                {
                    var result = CRM_API.DB.PMBOPayway.getPayWayFiles(sitecode);
                    return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
                }
                
            }
            catch (WebException ex)
            {   
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new List<CRM_API.Models.PMBODDOperations>());
            }
        }
    }
}
