﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NLog;
using System.Text;
using System.IO;
using CRM_API.Models.ChargebackSuspension;
using CRM_API.DB.ChargebackSuspension;
using Newtonsoft.Json;
using Mandrill;
using System.Configuration;
using Mandrill.Model;

namespace CRM_API.Controllers
{
    public class ChargebackSuspensionController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public HttpResponseMessage Get()
        {
            IList<Models.ProductModel> result = new List<Models.ProductModel>();
            try
            {
                result = CRM_API.DB.Product.ProductSProc.GetAllProductsForChargeback().ToList();
            }
            catch (Exception)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, result);
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        // POST api/FruadBlockandUnblock/
        [HttpPost]
        public HttpResponseMessage Post(ChargebackSuspensionInput input)
        {
            DB.Common.ErrCodeMsg errResult = new DB.Common.ErrCodeMsg()
           {
               errcode = -1,
               errsubject = "General Error",
               errmsg = "Empty New History"
           };
            switch (input.Mode_Type)
            {
                case ChargebackSuspensionMode.DoInsertChargebackTransaction:
                    return DoInsertChargebackTransaction(input);
                case ChargebackSuspensionMode.GetRequestedChargeback:
                    return GetRequestedChargeback(input);
                case ChargebackSuspensionMode.DoApproveRequestedChargeback:
                    return DoApproveRequestedChargeback(input);
                case ChargebackSuspensionMode.DoUnblockRequest:
                    return DoUnblockRequest(input);
                case ChargebackSuspensionMode.GetChargebackByAccountId:
                    return GetChargebackByAccountId(input);
                case ChargebackSuspensionMode.GetChargebackCollectionPayment:
                    return GetChargebackCollectionPayment(input);
                case ChargebackSuspensionMode.GetRequestedChargebackCount:
                    return GetRequestedChargebackCount(input);
                case ChargebackSuspensionMode.SendMail:
                    return SendMail(input);  
                default: break;
            }
            return Request.CreateResponse(HttpStatusCode.OK, errResult);
        }

        #region SendMail
        private HttpResponseMessage SendMail(ChargebackSuspensionInput Info)
        {
            List<SuspensionCommon> resValues = new List<SuspensionCommon>();
            SuspensionCommon output = new SuspensionCommon() { errcode = -1, errmsg = "Mail sent failure!" };
            try
            {
                if (Info.sitecode == "MCM" || Info.sitecode == "BAU")
                {
                    var api = new MandrillApi(ConfigurationManager.AppSettings["MAILCHIMPAPIKEY"]);
                    var message = new MandrillMessage();
                    //message.FromEmail = ConfigurationManager.AppSettings["QUEUEEMAILMAILFROM"];
                    message.AddGlobalMergeVars("FNAME", Info.firstname);
                    message.AddTo(Info.email);
                    IList<MandrillSendMessageResponse> response = api.Messages.SendTemplateAsync(message, ConfigurationManager.AppSettings["MAILCHIMPMYVECTONECHARGEBACK_" + Info.sitecode]).Result;
                    Log.Info("ChargebackSuspension Mail Response : " + JsonConvert.SerializeObject(response));
                    if (response != null && response.ElementAt(0).Status == 0)
                    {
                        output.errcode = 0;
                        output.errmsg = "Mail sent successfully!";
                    }
                    resValues.Add(output);
                }
            }
            catch (Exception ex)
            {
                Log.Error("ChargebackSuspension Mail Error : " + ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }
        #endregion

        private HttpResponseMessage GetChargebackCollectionPayment(ChargebackSuspensionInput input)
        {
            Log.Info("GetChargebackCollectionPayment :  Input : " + input.mobileno);
            IEnumerable<GetChargebackCollectionPaymentOutput> output = SProc.GetChargebackCollectionPayment(input.mobileno);
            if (output == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<GetChargebackCollectionPaymentOutput>().ToArray());
            else
            {
                Log.Info("GetChargebackCollectionPayment :  Output : " + JsonConvert.SerializeObject(output));
                return Request.CreateResponse(HttpStatusCode.OK, output.ToArray());
            }
        }

        private HttpResponseMessage GetRequestedChargebackCount(ChargebackSuspensionInput input)
        {
            Log.Info("GetRequestedChargebackCount :  Input : " + input.date_from + "," + input.date_to);
            IEnumerable<ChargebackCountOutput> output = SProc.GetRequestedChargebackCount(input.date_from, input.date_to);
            if (output == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<ChargebackCountOutput>().ToArray());
            else
            {
                Log.Info("GetRequestedChargebackCount :  Output : " + JsonConvert.SerializeObject(output));
                return Request.CreateResponse(HttpStatusCode.OK, output.ToArray());
            }
        }

        private HttpResponseMessage GetChargebackByAccountId(ChargebackSuspensionInput input)
        {
            Log.Info("GetRequestedByAccountId :  Input : " + input.mobileno);
            IEnumerable<RequestedChargebackOutput> output = SProc.GetChargebackByAccountId(input.mobileno);
            if (output == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<SuspensionCommon>().ToArray());
            else
            {
                Log.Info("GetRequestedByAccountId :  Output : " + JsonConvert.SerializeObject(output));
                return Request.CreateResponse(HttpStatusCode.OK, output.ToArray());
            }
        }

        HttpResponseMessage DoInsertChargebackTransaction(ChargebackSuspensionInput input)
        {
            Log.Info("DoInsertChargebackTransaction :  Input : " + JsonConvert.SerializeObject(input));
            IEnumerable<SuspensionCommon> output = SProc.DoInsertChargebackTransaction(input.product_code, input.sitecode, input.pay_reference, input.transaction_date, input.mobileno, input.reason, input.chargeback_claim_date, input.entered_by, input.proces_type, input.req_type, input.trans_amount, input.customer_name, input.chargeback_fee);
            if (output == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<SuspensionCommon>().ToArray());
            else
            {
                Log.Info("DoInsertChargebackTransaction :  Output : " + JsonConvert.SerializeObject(output));
                return Request.CreateResponse(HttpStatusCode.OK, output.ToArray());
            }
        }

        HttpResponseMessage GetRequestedChargeback(ChargebackSuspensionInput input)
        {
            IEnumerable<RequestedChargebackOutput> output = SProc.GetRequestedChargeback(input.userid, input.role_id, input.status, input.date_from, input.date_to, input.search_flag);
            if (output == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<RequestedChargebackOutput>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, output.ToArray());
        }

        HttpResponseMessage DoApproveRequestedChargeback(ChargebackSuspensionInput input)
        {
            Log.Info("DoApproveRequestedChargeback :  Input : " + JsonConvert.SerializeObject(input));
            IEnumerable<SuspensionCommon> output = SProc.DoApproveRequestedChargeback(input.userid, input.role_id, input.sitecode, input.pay_reference, input.mobileno, input.approved_user, input.req_type);
            if (output == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<SuspensionCommon>().ToArray());
            else
            {
                if (input.req_type == 1 && output != null && output.ElementAt(0).errcode == 0)
                {
                    if (input.sitecode == "MCM")
                    {
                        //SMS Trigger
                        try
                        {
                            string smsURL = CRM_API.DB.SMSCampaign.SProc.GetSMSGatewayInfo(input.sitecode, input.mobileno);
                            var smsResponse = CRM_API.Helpers.SendSMS.SMSSendOurGateway("Vectone", input.mobileno, ConfigurationManager.AppSettings["SMSMESSAGECHARGEBACK"], smsURL);
                            CRM_API.DB.SMSCampaign.SProc.InsertSMSNotifyText(input.mobileno, ConfigurationManager.AppSettings["SMSMESSAGECHARGEBACK"], input.approved_user, smsResponse == "-1" ? 0 : 1, "-", input.approved_user, "Chargeback Dispute");
                            Log.Info("DoApproveRequestedChargeback : SMS : Response : " + smsResponse);
                        }
                        catch (Exception ex)
                        {
                            Log.Error("DoApproveRequestedChargeback SMS Error : " + ex.Message);
                        }
                    }
                }
                Log.Info("DoApproveRequestedChargeback :  Output : " + JsonConvert.SerializeObject(output));
                return Request.CreateResponse(HttpStatusCode.OK, output.ToArray());
            }
        }

        HttpResponseMessage DoUnblockRequest(ChargebackSuspensionInput input)
        {
            Log.Info("DoUnblockRequest :  Input : " + JsonConvert.SerializeObject(input));
            IEnumerable<SuspensionCommon> output = SProc.DoUnblockRequest(input.pay_reference, input.mobileno);
            if (output == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<SuspensionCommon>().ToArray());
            else
            {
                Log.Info("DoUnblockRequest :  Output : " + JsonConvert.SerializeObject(output));
                return Request.CreateResponse(HttpStatusCode.OK, output.ToArray());
            }
        }
    }
}
