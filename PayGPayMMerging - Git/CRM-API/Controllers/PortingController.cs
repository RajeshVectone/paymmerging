﻿using CRM_API.Models.PortingModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CRM_API.Controllers
{
    public class PortingController : ApiController
    {
        public HttpResponseMessage Post(CRM_API.Models.CustomerOverviewPageInput input)
        {
            System.Web.HttpContext.Current.Session["GlobalProduct"] = input.Product_Code;
            //CRM_API.Models.PortingModel.PortingModelOutput resValues = null;
            //resValues = new Models.PortingModel.PortingModelOutput();
            //resValues.charging_fee = 1.0;
            //resValues.paccode = "TestPAC";
            //resValues.portin_operator = "TestOperator";
            PortingModelOutput resValues = CRM_API.DB.Porting.PortingDetails.GetPortingDetails(input.MobileNo, input.Sitecode);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new PortingModelOutput());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues);
        }


    }
}
