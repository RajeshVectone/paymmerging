﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dapper;

namespace CRM_API.Controllers
{
    public class SimResetController : ApiController
    {
        public HttpResponseMessage Get(string Msisdn, string Sitecode)
        {
            using (var conn = new SqlConnection(CRM_API.Helpers.Config.DefaultConnection))
            {
                conn.Open();
                var p = new DynamicParameters();
                //p.Add("@mobileno", Msisdn);
                //p.Add("@sitecode", Sitecode);
                conn.Execute("crm3_set_mnrf", p, commandType: CommandType.StoredProcedure);
                CRM_API.DB.Common.ErrCodeMsg resvalue = new CRM_API.DB.Common.ErrCodeMsg();
                resvalue.errcode = 0;
                resvalue.errmsg = "Update Successfully.";
                resvalue.errsubject = "Update Process";
                return Request.CreateResponse(HttpStatusCode.OK, resvalue);
            }
        }

        public HttpResponseMessage post(string Msisdn, string Iccid, string Sitecode)
        {
            var result = new CRM_API.Models.SIM.SIMInfo();
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
    }
}
