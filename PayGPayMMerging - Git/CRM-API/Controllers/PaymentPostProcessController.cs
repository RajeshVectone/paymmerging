﻿using CRM_API.Models._3rdParty.CTP;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CRM_API.Controllers
{
    public class PaymentPostProcessController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private CRM_API.Helpers.Payment payment = new Helpers.Payment();
        public dynamic Post(PaymentPostProcessModel Mods, dynamic xMods = null) 
        {
            try
            {
                switch (Mods.Sitecode){
                    case "CTP" :
                        CTPPostProcess(Mods.ActionType, xMods as CRM_API.Models._3rdParty.CTP.Payment);
                        break;
                    default:break;
                }
                return null;
            }
            catch (Exception e)
            {
                Log.Error(string.Format("PaymentPostProcessController.Post: Error {0}", e.Message));
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new CRM_API.Models.Common.ErrCodeMsg() { errcode = -1, errmsg = e.Message });
            }
        }
        private dynamic CTPPostProcess(ProcessType Step, CRM_API.Models._3rdParty.CTP.Payment x)
        {
            switch (Step)
            {
                case ProcessType.Topup:
                    return payment.DoTopup(x.AccId, x.MerchantReferenceCode, "CTP");
                default: break;
            }
            return null;
        }
    }
}
