﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using CRM_API.Models.CountrySaver;
using CRM_API.DB.CountrySaver;
using NLog;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using CRM_API.Models;
using System.IO;
using System.Text;
namespace CRM_API.Controllers
{
    public class CancellocationController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public async Task<HttpResponseMessage> Get(string ICCID, string Sitecode, string type)
        {
                Log.Debug("Cancel Location.GET: Started");
                try
                {
                 using (var client = new HttpClient())
                {
                    var Location = CRM_API.DB.SIMOrder.SProc.GetFreeSimByFullnameGroupBycancel(ICCID, Sitecode, type);               
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
                    string mode;
                     int node_type;
                    mode = "http";
                    node_type = 2; // MODEW = 1 
                    string urll;
                    string vlr_number;
                    string imsi_active;
                    imsi_active = Location.ElementAt(0).imsi_active;
                    vlr_number = Location.ElementAt(0).vlr_number;
                    urll = Location.ElementAt(0).url;              
                    string postData ="data-vlr-number=" + vlr_number + "&data-imsi=" + imsi_active+"&mode="+mode;

                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(urll);
                    request.Method = "POST";
                    request.ContentType = "application/x-www-form-urlencoded";
                    ASCIIEncoding enc = new ASCIIEncoding();
                    byte[] data = enc.GetBytes(postData);
                    request.ContentLength = data.Length;
                    Stream newStream = request.GetRequestStream();
                    newStream.Write(data, 0, data.Length);
                    newStream.Close();
                    HttpWebResponse smsRes = (HttpWebResponse)request.GetResponse();
                    Stream resStream = smsRes.GetResponseStream();
                    Log.Debug("Cancel Location Success");
                    Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                    StreamReader readStream = new StreamReader(resStream, encode);
                    return Request.CreateResponse(HttpStatusCode.OK, Location);
                    }
                }
                catch (Exception e)
                {
                    return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new Models.Common.ErrCodeMsg() { errcode = -1, errmsg = e.Message });
                }
            }

        public int errcode { get; set; }

        public string errmsg { get; set; }
    }
}
