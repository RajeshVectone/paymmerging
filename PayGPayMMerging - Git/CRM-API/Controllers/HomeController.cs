﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CRM_API.Models;
using Mandrill;
using Mandrill.Model;

namespace CRM_API.Controllers
{
    public class HomeController : ApiController
    {
        public HttpResponseMessage Post(HomeModels Model)
        {
            if (Model != null)
            {
                switch (Model.modetype)
                {
                    case HomeModeType.SEARCHAGENTPERFORMANCE:
                        return SearchAgentPerformance(Model.fromdate, Model.todate);
                    case HomeModeType.SEARCHUNRESOLVEDTASK:
                        return SearchUnresolvedTask(Model.fromdate, Model.todate);
                    case HomeModeType.SEARCHOVERVIEWTASK:
                        return SearchOverviewTask(Model.fromdate, Model.todate);
                    case HomeModeType.SEARCHGRAPVIEW:
                        return SearchGrapView(Model.fromdate, Model.todate);
                    default:
                        return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.HomeModels>().ToArray());
                }

            }
            return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.HomeModels>().ToArray());
        }

        

        private HttpResponseMessage SearchOverviewTask(string fromDate, string toDate)
        {
            IEnumerable<CRM_API.Models.SearchOverviewTaskOutput> resValues = CRM_API.DB.Home.SearchOverviewTask(fromDate, toDate);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.SearchOverviewTaskOutput>().ToArray());
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
            }
        }

        private HttpResponseMessage SearchUnresolvedTask(string fromDate, string toDate)
        {
            IEnumerable<CRM_API.Models.SearchUnresolvedTaskOutput> resValues = CRM_API.DB.Home.SearchUnresolvedTask(fromDate, toDate);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.SearchUnresolvedTaskOutput>().ToArray());
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
            }
        }

        private HttpResponseMessage SearchAgentPerformance(string fromDate, string toDate)
        {
            IEnumerable<CRM_API.Models.SearchAgentPerformanceOutput> resValues = CRM_API.DB.Home.SearchAgentPerformance(fromDate, toDate);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.SearchAgentPerformanceOutput>().ToArray());
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
            }
        }

        private HttpResponseMessage SearchGrapView(string fromDate, string toDate)
        {
            IEnumerable<CRM_API.Models.SearchGrapViewOutput> resValues = CRM_API.DB.Home.SearchGrapView(fromDate, toDate);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.SearchGrapViewOutput>().ToArray());
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
            }
        }

    }
}
