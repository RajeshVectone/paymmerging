﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NLog;

namespace CRM_API.Controllers
{
    //5Jan2015 : Moorthy : Added Jira CRMT:198
    public class CDR_v1Controller : ApiController
    {
        //
        // GET: /CDR/
        // POST : api/cdr/
        [HttpPost]
        public HttpResponseMessage POST(CRM_API.Models.CDR.CallHistorySearch In)
        {
            DB.Common.ErrCodeMsg errResult = new DB.Common.ErrCodeMsg()
            {
                errcode = -1,
                errsubject = "General Error",
                errmsg = "Empty New Order"
            };
            try
            {
                System.Web.HttpContext.Current.Session["GlobalProduct"] = In.ProductCode;
                switch (In.HistoryType)
                {
                    case Models.CDR.CDRType.CallHistory:
                        return GetCallHistory(In);//-->1
                    case Models.CDR.CDRType.SMSHistory:
                        return GetSMSHistory(In);//-->2
                    case Models.CDR.CDRType.GPRSHistory:
                        return GetGPRSHistory(In);//-->3
                    case Models.CDR.CDRType.AllHistory:
                        return GetAllHistory(In);//-->0
                    //Added by karthik Jira CRMT-198
                    case Models.CDR.CDRType.CallandSMS:
                        return GetCallandSMSHistory(In);//-->5
                    //25-Jan-2019 : Moorthy : Added for View Prefix
                    case Models.CDR.CDRType.ViewPrefix:
                        return ViewPrefix();//-->6
                    //CRM Enhancement 2
                    case Models.CDR.CDRType.ExcessUsage:
                        return GetExcessUsage(In);//-->7
                    default: break;
                }
            }
            catch (Exception ex)
            {
                errResult.errmsg = "WEB-API: " + ex.Message;
                errResult.errcode = -1;
            }
            return Request.CreateResponse(HttpStatusCode.OK, errResult);
        }

        //CRM Enhancement 2
        private HttpResponseMessage GetExcessUsage(CRM_API.Models.CDR.CallHistorySearch In)
        {
            IEnumerable<CRM_API.Models.CDR.CallHistory_v1> result =
                           CRM_API.DB.CDR.SProc_v1.AllHistory(In.mobileno, In.Date_From, In.Date_To, In.Sitecode, In.Packageid, 0).OrderByDescending(o => o.Date_Time);
            if (result == null || result.Count() == 0)
                return Request.CreateResponse(HttpStatusCode.OK, new List<CRM_API.Models.CDR.CallHistory_v1>().ToArray());
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, result.Where(r => r.Status == 0).ToArray());
            }
        }

        //25-Jan-2019 : Moorthy : Added for View Prefix
        private HttpResponseMessage ViewPrefix()
        {
            IEnumerable<CRM_API.Models.CDR.ViewHistoryOutput> result = CRM_API.DB.CDR.SProc_v1.ViewPrefix();
            if (result == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.CDR.ViewHistoryOutput>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
        }

        //Added by karthik Jira:CRMT-198
        private HttpResponseMessage GetCallHistory(CRM_API.Models.CDR.CallHistorySearch In)
        {

            IEnumerable<CRM_API.Models.CDR.CallHistory_v1> result =
                           CRM_API.DB.CDR.SProc_v1.GetCallHistory(In.mobileno, In.Date_From, In.Date_To, In.Sitecode, In.Packageid, 1).OrderByDescending(o => o.Date_Time);
            ////Added by karthik Jira:CRMT-8
            //if (In.Sitecode.ToLower() == "gcm")
            //    result.Union(CRM_API.DB.CDR.SProc_v1.GocheapcallCDR(In.mobileno, In.Date_From, In.Date_To, In.Sitecode, 1));

            if (result == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.CDR.CallHistory_v1>().ToArray());
            else if (result.Count() == 0)
                return Request.CreateResponse(HttpStatusCode.OK, new List<CRM_API.Models.CDR.CallHistory_v1>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
        }
        //Added by karthik Jira:CRMT-198
        private HttpResponseMessage GetGPRSHistory(CRM_API.Models.CDR.CallHistorySearch In)
        {
            IEnumerable<CRM_API.Models.CDR.CallHistory_v1> result =
                          CRM_API.DB.CDR.SProc_v1.GetGPRSHistory(In.mobileno, In.Date_From, In.Date_To, In.Sitecode, In.Packageid, 3).OrderByDescending(o => o.Date_Time);
            ////Added by karthik Jira:CRMT-8
            //if (In.Sitecode.ToLower() == "gcm")
            //    result.Union(CRM_API.DB.CDR.SProc_v1.GocheapGPRSCDR(In.mobileno, In.Date_From, In.Date_To, In.Sitecode, 3));

            if (result == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.CDR.CallHistory_v1>().ToArray());
            else if (result.Count() == 0)
                return Request.CreateResponse(HttpStatusCode.OK, new List<CRM_API.Models.CDR.CallHistory_v1>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
        }
        //Added by karthik Jira:CRMT-198
        private HttpResponseMessage GetSMSHistory(CRM_API.Models.CDR.CallHistorySearch In)
        {

            IEnumerable<CRM_API.Models.CDR.CallHistory_v1> result =
                           CRM_API.DB.CDR.SProc_v1.GetSMSHistory(In.mobileno, In.Date_From, In.Date_To, In.Sitecode, In.Packageid, 2).OrderByDescending(o => o.Date_Time);
            ////Added by karthik Jira:CRMT-8
            //if (In.Sitecode.ToLower() == "gcm")
            //    result.Union(CRM_API.DB.CDR.SProc_v1.GocheapSMSCDR(In.mobileno, In.Date_From, In.Date_To, In.Sitecode, 2));

            if (result == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.CDR.CallHistory_v1>().ToArray());
            else if (result.Count() == 0)
                return Request.CreateResponse(HttpStatusCode.OK, new List<CRM_API.Models.CDR.CallHistory_v1>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
        }
        //Added by karthik Jira:CRMT-198
        private HttpResponseMessage GetAllHistory(CRM_API.Models.CDR.CallHistorySearch In)
        {
            IEnumerable<CRM_API.Models.CDR.CallHistory_v1> result =
                           CRM_API.DB.CDR.SProc_v1.AllHistory(In.mobileno, In.Date_From, In.Date_To, In.Sitecode, In.Packageid, 0).OrderByDescending(o => o.Date_Time);
            ////Added by karthik Jira:CRMT-8
            //if (In.Sitecode.ToLower() == "gcm")
            //    result.Union(CRM_API.DB.CDR.SProc_v1.GocheapALLCDR(In.mobileno, In.Date_From, In.Date_To, In.Sitecode, 0));

            if (result == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.CDR.CallHistory_v1>().ToArray());
            else if (result.Count() == 0)
                return Request.CreateResponse(HttpStatusCode.OK, new List<CRM_API.Models.CDR.CallHistory_v1>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
        }
        //Added by karthik Jira:CRMT-198
        private HttpResponseMessage GetCallandSMSHistory(CRM_API.Models.CDR.CallHistorySearch In)
        {
            IEnumerable<CRM_API.Models.CDR.CallHistory_v1> result =
                           CRM_API.DB.CDR.SProc_v1.CallandSMSHistory(In.mobileno, In.Date_From, In.Date_To, In.Sitecode, In.Packageid, 5).OrderByDescending(o => o.Date_Time);
            //if (In.Sitecode.ToLower() == "gcm")
            //    result.Union(CRM_API.DB.CDR.SProc_v1.GocheapALLCDR(In.mobileno, In.Date_From, In.Date_To, In.Sitecode, 5));

            if (result == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.CDR.CallHistory_v1>().ToArray());
            else if (result.Count() == 0)
                return Request.CreateResponse(HttpStatusCode.OK, new List<CRM_API.Models.CDR.CallHistory_v1>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
        }

    }
}
