﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NLog;

namespace CRM_API.Controllers
{
    public class CustomerCSBController : ApiController
    {

        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private static string currency = "GBP";
        private static double chargeCSB = 0.00;
        // GET api/customercsb/MobileNo
        public HttpResponseMessage Get(string id)
        {
            IEnumerable<CRM_API.Models.CustomerCSBRecords> resValues =
                CRM_API.DB.CustomerCSB.SProc.List(id);

            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.CustomerCSBRecords>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        // POST api/customercsb/
        [HttpPost]
        public HttpResponseMessage Post(CRM_API.Models.CustomerCSBHistory resHistory)
        {
            Log.Debug("--------------------------------------------------------------------");
            DB.Common.ErrCodeMsg errResult = new DB.Common.ErrCodeMsg()
           {
               errcode = -1,
               errsubject = "General Error",
               errmsg = "Empty New History"
           };
            Log.Debug("Customer2in1Controller.Post: Started");
            if (resHistory != null)
            {
                switch (resHistory.order_records)
                {
                    case Models.CustomerCSBOrderRecords.History:
                        return History(resHistory.mobileno, resHistory.destno);

                    case Models.CustomerCSBOrderRecords.Cancelation:
                        Cancelation(resHistory, out errResult);
                        return Request.CreateResponse(HttpStatusCode.OK, errResult);

                    case Models.CustomerCSBOrderRecords.ReactivatedByCard:
                        ReactivateOrderByCard(resHistory, out errResult);
                        return Request.CreateResponse(HttpStatusCode.OK, errResult);

                    case Models.CustomerCSBOrderRecords.ChangePaymentToBalance:
                        ChangePaymentToBalance(resHistory, out errResult);
                        return Request.CreateResponse(HttpStatusCode.OK, errResult);

                    case Models.CustomerCSBOrderRecords.ChangePaymentToCard:
                        ChangePaymentToCard(resHistory, out errResult);
                        return Request.CreateResponse(HttpStatusCode.OK, errResult);

                    case Models.CustomerCSBOrderRecords.ReactivatedByBalance:
                        ReactivateOrderByBalance(resHistory, out errResult);
                        return Request.CreateResponse(HttpStatusCode.OK, errResult);

                    case Models.CustomerCSBOrderRecords.NotesList:
                        return NotesList(resHistory.account_id);

                    case Models.CustomerCSBOrderRecords.NotesAdd:
                        NotesAdd(resHistory, out errResult);
                        return Request.CreateResponse(HttpStatusCode.OK, errResult);

                    case Models.CustomerCSBOrderRecords.GetCountry:
                        return CountryList(resHistory.mobileno, resHistory.site_code);



                    default:
                        return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.CustomerCSBHistory>().ToArray());
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.CustomerCSBHistory>().ToArray());
            }

        }

        public HttpResponseMessage History(string mobileno, string destno)
        {
            IEnumerable<CRM_API.Models.CustomerCSBHistory> resHistoryValues =
                           CRM_API.DB.CustomerCSB.SProc.ListHistory(mobileno, destno);
            if (resHistoryValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.CustomerCSBHistory>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resHistoryValues.ToArray());
        }

        void Cancelation(CRM_API.Models.CustomerCSBHistory CsbToCancel, out DB.Common.ErrCodeMsg errResult)
        {
            Log.Debug("CountryCSBController.Cancel");
            errResult = new DB.Common.ErrCodeMsg();

            errResult = DB.CustomerCSB.SProc.Cancel(CsbToCancel.mobileno, CsbToCancel.destno, 0);
            errResult.errsubject = "Cancel Bundle";
            if (errResult.errcode == 0)
            {
                errResult.errmsg = "Country Saver Bundle has succesfully cancelled";
            }

        }
        void ReactivateByCard(CRM_API.Models.CustomerCSBHistory CSBToReactivate, out DB.Common.ErrCodeMsg errResult)
        {
            Log.Debug("CustomerCSBController.ReActivation");
            errResult = new DB.Common.ErrCodeMsg();

            errResult = DB.CustomerCSB.SProc.Cancel(CSBToReactivate.mobileno, CSBToReactivate.destno, 1);

            if (errResult.errcode == 0)
            {
                errResult.errsubject = "Reactivation Success";
                errResult.errmsg = "Payment is success and bundle is activated successfully";
            }
            else
            {
                errResult.errsubject = "Reactivation Failure";
            }
        }

        void ChangePaymentToBalance(CRM_API.Models.CustomerCSBHistory CSBChangeToBalance, out DB.Common.ErrCodeMsg errResult)
        {
            Log.Debug("CustomerCSBController.ChangePaymentToBalance");
            errResult = new DB.Common.ErrCodeMsg();

            errResult = DB.CustomerCSB.SProc.ChangeRenewalMethod(CSBChangeToBalance.mobileno, CSBChangeToBalance.destno, "Balance");
            errResult.errsubject = "Change Payment to Balance";
            if (errResult.errcode == 0)
                errResult.errmsg = "Successfully changed";

        }

        void ChangePaymentToCard(CRM_API.Models.CustomerCSBHistory CSBChangeToCard, out DB.Common.ErrCodeMsg errResult)
        {
            string productcode = GetProductCode(CSBChangeToCard.site_code);
            System.Web.HttpContext.Current.Session["GlobalProduct"] = productcode;

            Log.Debug("CustomerCSBController.ChangePaymentToCard");
            errResult = new DB.Common.ErrCodeMsg();
            string email = string.IsNullOrEmpty(CSBChangeToCard.email) ? "a.sunandar@mundio.com" : CSBChangeToCard.email;

            CRM_API.Helpers.Payment payment = new Helpers.Payment();

            ChargeBySitecode(CSBChangeToCard);
            CSBChangeToCard.monthly_charge = chargeCSB;

            payment.Authorize(CSBChangeToCard as CRM_API.Models.Payment.CardDetails, CSBChangeToCard.site_code, CSBChangeToCard.mobileno, email, currency, 0.00, productcode);
            CSBChangeToCard.SubscriptionID = payment.SubscriptionId;
            errResult = DB.CustomerCSB.SProc.ChangeSubscriptionId(CSBChangeToCard);

            if (payment.Decision == "ACCEPT")
            {
                errResult = DB.CustomerCSB.SProc.ChangeRenewalMethod(CSBChangeToCard.mobileno, CSBChangeToCard.destno, "Card");
                errResult.errsubject = "Change Payment to Card";
                if (errResult.errcode == 0)
                    errResult.errmsg = "Successfully changed";
            }
            else
            {
                errResult.errsubject = "Change Payment to Card";
                errResult.errmsg = "Change Payment to Card Failure";
            }
        }

        private string GetProductCode(string siteCode)
        {
            string productcode = "VMUK";
            if (siteCode == "MCM")
                productcode = "VMUK";
            else if (siteCode == "BAU")
                productcode = "VMAT";
            else if (siteCode == "MFR")
                productcode = "VMFR";
            else if (siteCode == "BNL")
                productcode = "VMNL";
            return productcode;
        }

        void ReactivateOrderByCard(CRM_API.Models.CustomerCSBHistory newOrder, out DB.Common.ErrCodeMsg errResult)
        {
            string productcode = GetProductCode(newOrder.site_code);
            System.Web.HttpContext.Current.Session["GlobalProduct"] = productcode;

            //TODO -- Check site code to charge

            ChargeBySitecode(newOrder);

            Log.Debug("CustomerCSBController.ReactivateOrderByCard");
            // TODO -- Check email
            string email = string.IsNullOrEmpty(newOrder.email) ? "dony.isnandi@mundio.com" : newOrder.email;

            // Charge the card first
            CRM_API.Helpers.Payment payment = new Helpers.Payment();

            // user select a new card
            if (string.IsNullOrEmpty(newOrder.SubscriptionID))
            {
                Log.Debug("CustomerCSBController.Post: Charge New Card");
                //payment.Authorize(newOrder as Models.Payment.CardDetails,
                //    newOrder.site_code, newOrder.mobileno, email, currency, chargeCSB);
                //newOrder.SubscriptionID = payment.SubscriptionId;
                string referenceCode = string.Format("{0}-{1}-{2:yyMMddHHmm}", productcode, string.Format("2SIMC{0:00}-RX", chargeCSB), DateTime.Now);
                payment.AuthorizeWithout3DS(newOrder as Models.Payment.CardDetails, newOrder.site_code, newOrder.mobileno, string.Format("PP{0:00}", chargeCSB), email, currency, chargeCSB, newOrder.mobileno, newOrder.SubscriptionID, referenceCode, productcode);
            }
            else
            {
                Log.Debug("CustomerCSBController.Post: Charge Saved Card");
                //payment.CybersourceCharge(newOrder.mobileno, newOrder.mobileno, newOrder.SubscriptionID, currency, chargeCSB);
                string referenceCode = string.Format("{0}-{1}-{2:yyMMddHHmm}", productcode, string.Format("2SIMC{0:00}-RX", chargeCSB), DateTime.Now);
                payment.AuthorizeWithout3DS(newOrder as Models.Payment.CardDetails, newOrder.site_code, newOrder.mobileno, string.Format("PP{0:00}", chargeCSB), email, currency, chargeCSB, newOrder.mobileno, newOrder.SubscriptionID, referenceCode, productcode);
            }

            if (payment.Decision == "ACCEPT" || payment.Decision == "APPROVAL")
            {
                newOrder.payref = payment.MerchantReferenceCode;
                ReactivateByCard(newOrder, out errResult);
            }
            else
            {
                errResult = new DB.Common.ErrCodeMsg()
                {
                    errcode = -1,
                    errsubject = "Reactivation Failure",
                    errmsg = "Payment Failed"
                };
                /*if (string.IsNullOrEmpty(payment.MerchantReferenceCode) || payment.MerchantReferenceCode == "-1")
                    errResult.errmsg = string.Format("{0}", payment.Description);
                else
                    errResult.errmsg = string.Format("{0} Order Number {1}", payment.Description, payment.MerchantReferenceCode);*/
            }
        }

        private static void ChargeBySitecode(CRM_API.Models.CustomerCSBHistory newOrder)
        {
            if (newOrder.site_code == "BAU")
            {
                chargeCSB = 25.00;
                currency = "EUR";
            }
            else if (newOrder.site_code == "BSE")
            {
                chargeCSB = 200.00;
                currency = "SEK";
            }
            else if (newOrder.site_code == "MFI")
            {
                chargeCSB = 20.00;
                currency = "EUR";
            }
            else if (newOrder.site_code == "MFR")
            {
                chargeCSB = 20.00;
                currency = "EUR";
            }
            else if (newOrder.site_code == "BDK")
            {
                chargeCSB = 200.00;
                currency = "DKK";
            }
            else if (newOrder.site_code == "BNL")
            {
                chargeCSB = 20.00;
                currency = "EUR";
            }
            else if (newOrder.site_code == "MCM")
            {
                chargeCSB = 20.00;
                currency = "GBP";
            }
            //return chargeCSB;
        }

        void ReactivateOrderByBalance(CRM_API.Models.CustomerCSBHistory newOrder, out DB.Common.ErrCodeMsg errResult)
        {
            Log.Debug("CustomerCSBController.ReactivatedByBalance");
            errResult = new DB.Common.ErrCodeMsg();

            IEnumerable<CRM_API.Models.CustomerCSBHistory> resHistoryValues =
                           CRM_API.DB.CustomerCSB.SProc.BalanceCharge(newOrder.order_id);

            if (resHistoryValues.ElementAt(0).errcode == 0)
            {
                ReactivateByCard(newOrder, out errResult);
            }
            else
            {
                errResult.errsubject = "Reactivation Failure";
                errResult.errcode = resHistoryValues.ElementAt(0).errcode;
                errResult.errmsg = resHistoryValues.ElementAt(0).errmsg.ToString();
            }

        }

        public HttpResponseMessage NotesList(string account_id)
        {
            IEnumerable<CRM_API.Models.CustomerCSBNotes> resNotesValues =
                           CRM_API.DB.CustomerCSB.SProc.NotesList(account_id);
            if (resNotesValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.CustomerCSBHistory>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resNotesValues.ToArray());
        }

        void NotesAdd(CRM_API.Models.CustomerCSBHistory notesadd, out DB.Common.ErrCodeMsg errResult)
        {
            Log.Debug("CountryCSBController.AddNotes");
            errResult = new DB.Common.ErrCodeMsg();

            errResult = DB.CustomerCSB.SProc.AddNotes(notesadd.account_id, notesadd.description, notesadd.user_login);
            errResult.errsubject = "Add Notes";
            if (errResult.errcode == 0)
            {
                errResult.errmsg = "Note Has Been Added Successfully";
            }
        }

        public HttpResponseMessage CountryList(string mobileno, string site_code)
        {
            IEnumerable<CRM_API.Models.CustomerCSBRecords> resNotesValues =
                                      CRM_API.DB.CustomerCSB.SProc.CountryList(mobileno, site_code);
            if (resNotesValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.CustomerCSBHistory>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resNotesValues.ToArray());
        }


    }
}
