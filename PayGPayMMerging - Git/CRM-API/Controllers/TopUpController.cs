﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NLog;

namespace CRM_API.Controllers
{
    public class TopUpController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public HttpResponseMessage Post(CRM_API.Models.TopupSearch Model)
        {
            var result = Model;
            DB.Common.ErrCodeMsg errResult = new DB.Common.ErrCodeMsg()
           {
               errcode = -1,
               errsubject = "General Error",
               errmsg = "Empty Data"
           };
            //06-Aug-2015 : Moorthy Added
            System.Web.HttpContext.Current.Session["GlobalProduct"] = Model.Product_Code;
            switch (Model.SearchType)
            {
                case Models.InfoType.TopUpLog:
                    return GetTopupLog(Model);
                //20-Feb-2019 : Moorthy : Added for New Cancel Topup
                //case Models.InfoType.TopupStatus:
                //    return GetTopupStatus(Model);
                case Models.InfoType.TopupStatus:
                    return GetTopupStatusNew(Model);
                case Models.InfoType.CancelAutoTopup:
                    return CancelTopup(Model);
                    //return Request.CreateResponse(HttpStatusCode.OK,errResult);
                //25-Feb-2019 : Moorthy : CRMIMP-18 - Daily usage history
                case Models.InfoType.DailyUsageHistory:
                    return DailyUsageHistory(Model);
                default: break;
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        //25-Feb-2019 : Moorthy : CRMIMP-18 - Daily usage history
        private HttpResponseMessage DailyUsageHistory(Models.TopupSearch In)
        {
            var result = DB.Payment.DailyUsageHistory(In.Msisdn, In.Sitecode, In.StartDate.Value, In.EndDate.Value);
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        private HttpResponseMessage GetTopupLog(CRM_API.Models.TopupSearch In)
        {
            var result = DB.Payment.GetTopupLog(In.Msisdn, In.Sitecode,In.StartDate.Value, In.EndDate.Value);
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        private HttpResponseMessage GetTopupStatus(CRM_API.Models.TopupSearch In)
        {
            string Last6Digit;
            var result = DB.Payment.GetTopupStatus(In.Msisdn, In.Sitecode);
            GetSavedCardDetails(In.Msisdn,out Last6Digit);
            result.Last6digitsCC = Last6Digit;
            result.errsubject = "Auto Topup Status";
            if (String.IsNullOrEmpty(result.Last6digitsCC))
            {
                result.errmsg = "Empty Credit Card Detail";
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        //20-Feb-2019 : Moorthy : Added for New Cancel Topup
        private HttpResponseMessage GetTopupStatusNew(CRM_API.Models.TopupSearch In)
        {
            var result = DB.Payment.GetTopupStatusNew(In.Msisdn, In.Sitecode);
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        void GetSavedCardDetails(string account, out string Last6Digits)
        {
            IEnumerable<CRM_API.Models.Payment.Subscription> CCValues = DB.Payment.GetSubscribtionList(account);
            if (CCValues.Count() != 0 )
            {
                var FirstRow = CCValues.OrderByDescending(r => r.expirydate).FirstOrDefault();
                Last6Digits = FirstRow.Last6digitsCC.ToString();
            }
            else
            {
                Last6Digits = string.Empty;
            }
        }

        private HttpResponseMessage CancelTopup(CRM_API.Models.TopupSearch In)
        {
            var errResult = new DB.Common.ErrCodeMsg();

            errResult = DB.Payment.CancelTopup(In.Msisdn, In.submit_by, In.Sitecode);
            errResult.errsubject = "Cancel Auto Topup";
            if (errResult.errcode == 1)
            {
                errResult.errcode = 0;
                errResult.errmsg = "Auto Topup has been canceled";

                if (In.Sitecode == "MCM")
                {
                    //SMS Trigger
                    try
                    {
                        string smsURL = CRM_API.DB.SMSCampaign.SProc.GetSMSGatewayInfo(In.Sitecode, In.Msisdn);
                        var smsResponse = CRM_API.Helpers.SendSMS.SMSSendOurGateway("Vectone", In.Msisdn, ConfigurationManager.AppSettings["SMSMESSAGEDISABLEAUTOTOPUP"], smsURL);
                        CRM_API.DB.SMSCampaign.SProc.InsertSMSNotifyText(In.Msisdn, ConfigurationManager.AppSettings["SMSMESSAGEDISABLEAUTOTOPUP"], In.submit_by, smsResponse == "-1" ? 0 : 1, "-", In.submit_by, "Disabled auto top-up");
                        Log.Info("CancelTopup : SMS : Response : " + smsResponse);
                    }
                    catch (Exception ex)
                    {
                        Log.Error("CancelTopup SMS Error : " + ex.Message);
                    }
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, errResult);
        }
    }
}
