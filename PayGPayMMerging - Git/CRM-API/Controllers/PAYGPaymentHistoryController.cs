﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CRM_API.Models;
using NLog;


namespace CRM_API.Controllers
{
    /// <summary>
    /// PAYM Payment History
    /// api/PAYMPaymentHistory
    /// </summary>
    public class PAYGPaymentHistoryController: ApiController
    {
        /// <summary>
        /// PAYM Get the Pyment History
        /// api/PAYMPaymentHistory
        /// </summary>
        /// 

        private readonly static Logger Log;

		static PAYGPaymentHistoryController()
		{
			PAYGPaymentHistoryController.Log = LogManager.GetCurrentClassLogger();
		}

        public PAYGPaymentHistoryController()
		{
		}
        [HttpGet]
        public IEnumerable<CRM_API.Models.History> GetFilteredPastPaymentList(string mobileno, string decision, string paymethod, string paytype, string startdate, string enddate, string user_role)
        {
            IEnumerable<CRM_API.Models.History> histories;
            Logger log = PAYGPaymentHistoryController.Log;
            string[] strArrays = new string[] { "GET FILTERED PAST PAYMENT ", mobileno, "[Status:", decision, "][PayMethod:", paymethod, "][PayType:", paytype, "]" };
            log.Info(string.Concat(strArrays));
            try
            {
                List<CRM_API.Models.History> histories1 = new List<CRM_API.Models.History>();
                if (paytype == null || paytype.Contains("top") || paytype.Contains("aut"))
                {
                    List<History> filteredPastTopUp = (List<History>)CRM_API.DB.PaymentHistroy.GetFilteredPastTopUp(mobileno, decision, paymethod, paytype, startdate, enddate); //PaymentGetFilteredPastPayment
                    if (filteredPastTopUp != null)
                    {
                        histories1.AddRange(filteredPastTopUp);
                    }
                }
                if ((paytype == null || paytype.Contains("pur")) && (paymethod == null || paymethod.Equals("2")))
                {
                    List<CRM_API.Models.History> filteredPastPurchased = (List<CRM_API.Models.History>)CRM_API.DB.PaymentHistroy.GetFilteredPastPurchased(mobileno, startdate, enddate); //crm_get_customers_filters_v2
                    if (filteredPastPurchased != null)
                    {
                        if (decision != null)
                        {
                            if (decision.Equals("2"))
                            {
                                filteredPastPurchased = (
                                    from x in filteredPastPurchased
                                    where x.Status.ToLower().Equals("failed")
                                    select x).ToList<CRM_API.Models.History>();
                            }
                            if (decision.Equals("1"))
                            {
                                filteredPastPurchased = (
                                    from x in filteredPastPurchased
                                    where x.Status.ToLower().Equals("success")
                                    select x).ToList<CRM_API.Models.History>();
                            }
                        }
                        foreach (CRM_API.Models.History history in filteredPastPurchased)
                        {
                            history.PaymentMethod = "Credit Card";
                            history.PaymentType = "Purchase";
                        }
                        histories1.AddRange(filteredPastPurchased);
                    }
                }
                if (paytype == null && (paymethod == null || paymethod.Equals("5")))
                {
                    List<History> filteredPastCreditTransfer = (List<History>)CRM_API.DB.PaymentHistroy.GetFilteredPastCreditTransfer(mobileno,startdate,enddate);
                    if (filteredPastCreditTransfer != null)
                    {
                        if (decision != null)
                        {
                            if (decision.Equals("2"))
                            {
                                filteredPastCreditTransfer = (
                                    from x in filteredPastCreditTransfer
                                    where x.Status.ToLower().Equals("failed")
                                    select x).ToList<History>();
                            }
                            if (decision.Equals("1"))
                            {
                                filteredPastCreditTransfer = (
                                    from x in filteredPastCreditTransfer
                                    where x.Status.ToLower().Equals("success")
                                    select x).ToList<History>();
                            }
                        }
                        foreach (History history1 in
                            from x in filteredPastCreditTransfer
                            where x.Status.ToLower().Equals("success")
                            select x)
                        {
                            history1.Status = "Successful";
                        }
                        histories1.AddRange(filteredPastCreditTransfer);
                    }
                }
                if (decision == "1")
                {
                    decision = "0";
                }
                if (decision == "2")
                {
                    decision = "-1";
                }
                if (paymethod == "4")
                {
                    decision = "1";
                }
                if ((paytype == null || paytype.Contains("bun")) && paymethod == null)
                {
                    List<CRM_API.Models.History> filteredPastBundleSub = (List<CRM_API.Models.History>)CRM_API.DB.PaymentHistroy.GetFilteredPastBundleSub(mobileno, decision, paymethod,startdate,enddate);
                    if (filteredPastBundleSub != null)
                    {
                        histories1.AddRange(filteredPastBundleSub);
                    }
                }
                histories = histories1;
                foreach (var item in histories)
                {
                    item.ViewRefund = user_role == "1" ? 1 : 0;
                }
            }
            catch (Exception exception1)
            {
                Exception exception = exception1;
                PAYGPaymentHistoryController.Log.Error(string.Concat("Error: ", exception.Message));
                HttpResponseMessage httpResponseMessage = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(exception.Message)
                };
                throw new HttpResponseException(httpResponseMessage);
            }
            return histories;
        }
        public HttpResponseMessage Get(string mobileno, string sitecode)
        {
            try
            {
                var result = CRM_API.DB.PaymentHistroy.getPaymentDetails(mobileno, sitecode);
                return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
            }
            catch (WebException ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new List<CRM_API.Models.History>());
            }

        }
    }
}
