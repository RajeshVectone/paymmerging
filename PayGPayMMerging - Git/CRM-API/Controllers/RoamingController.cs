﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CRM_API.Controllers
{
    public class RoamingController : ApiController
    {

        //POST api/Roaming/
        public HttpResponseMessage Post(CRM_API.Models.Roaming.RoamingInfo_Request Model)
        {
            //var result = new CRM_API.Models.Roaming.RoamingInfo();

            return GetRoamingInfo(Model);
            //return Request.CreateResponse(HttpStatusCode.OK, CRM_API.DB.Roaming.Roaming.RoamingInfo(Model.ICCID, Model.MOBILENO, Model.SITECODE).FirstOrDefault());
        }

        private HttpResponseMessage GetRoamingInfo(CRM_API.Models.Roaming.RoamingInfo_Request Model)
        {
            CRM_API.Models.Roaming.RoamingInfo resultValues = CRM_API.DB.Roaming.Roaming.RoamingInfo(Model.ICCID, Model.MOBILENO, Model.SITECODE).FirstOrDefault();

            return Request.CreateResponse(HttpStatusCode.OK, resultValues);
        }
    }
}