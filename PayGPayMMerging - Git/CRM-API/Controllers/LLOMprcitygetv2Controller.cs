﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml;


namespace CRM_API.Controllers
{
    public class LLOMprcitygetv2Controller : ApiController
    {
        //
        // GET: /LLOMprcitygetv2/


        public HttpResponseMessage get(string product, string city)
        {
            try
            {
                var result = CRM_API.DB.viewtransaction.lottmproductcityview_v5(product, city);
                return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());

            }
            catch (WebException ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new List<CRM_API.Models.financeviewtrans>());
            }
        }

    }
}
