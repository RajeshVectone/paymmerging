﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CRM_API.Controllers
{
    public class BUBLController : ApiController
    {
        //
        // GET: /BUBL/

        public HttpResponseMessage get(string mobileno,string user,string status,string reason)
        {
            try
            {
                var result = CRM_API.DB.viewtransaction.BUBsearch(mobileno,user,status,reason);
                return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());

            }
            catch (WebException ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new List<CRM_API.Models.financeviewtrans>());
            }
        }

    }
}
