﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CRM_API.Models;
using Mandrill;
using Mandrill.Model;
using Newtonsoft.Json;
using NLog;

namespace CRM_API.Controllers
{
    public class FreeSIMOrderController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public HttpResponseMessage Post(FreeSIMOrderInfo Info)
        {
            if (Info != null)
            {
                Log.Info("FreeSIMOrder : PlaceOrder : Input : {0}", JsonConvert.SerializeObject(Info));
                Info.ip_address = System.Web.HttpContext.Current.Request.UserHostAddress;
                switch (Info.Type)
                {
                    case FreeSIMOrderType.PlaceOrder:
                        return PlaceOrder(Info);
                    case FreeSIMOrderType.ReferAFriend:
                        return ReferAFriend(Info);
                    default: 
                        return Request.CreateResponse(HttpStatusCode.OK, new List<FreeSIMOrderResponse>() { new FreeSIMOrderResponse { errcode = -1, errmsg = "Something went wrong!" } }.ToArray());
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, new List<FreeSIMOrderResponse>() { new FreeSIMOrderResponse { errcode = -1, errmsg = "Input is empty!" } }.ToArray());
            }
        }

        #region ReferAFriend
        private HttpResponseMessage ReferAFriend(FreeSIMOrderInfo Info)
        {
            List<FreeSIMOrderResponse> resValues = new List<FreeSIMOrderResponse>();
            FreeSIMOrderResponse output = new FreeSIMOrderResponse() { errcode = -1, errmsg = "Mail sent failure!" };
            //Email Trigger
            try
            {
                if (Info.sitecode == "MCM" || Info.sitecode == "BAU")
                {
                    var api = new MandrillApi(ConfigurationManager.AppSettings["MAILCHIMPAPIKEY"]);
                    var message = new MandrillMessage();
                    message.AddGlobalMergeVars("FNAME", Info.firstname);
                    message.AddTo(Info.email);
                    IList<MandrillSendMessageResponse> response = api.Messages.SendTemplateAsync(message, ConfigurationManager.AppSettings["MAILCHIMPREFERAFRIENDOFFEREMAIL_" + Info.sitecode]).Result;
                    Log.Info("ReferAFriend Mail Response : " + JsonConvert.SerializeObject(response));
                    if (response != null && response.ElementAt(0).Status == 0)
                    {
                        output.errcode = 0;
                        output.errmsg = "Mail sent successfully!";
                    }
                    resValues.Add(output);
                }
            }
            catch (Exception ex)
            {
                Log.Error("ReferAFriend Mail Error : " + ex.Message);
            }
            //SMS Trigger
            try
            {
                string smsURL = CRM_API.DB.SMSCampaign.SProc.GetSMSGatewayInfo(Info.sitecode, Info.mobilephone);
                var smsResponse = CRM_API.Helpers.SendSMS.SMSSendOurGateway("Vectone", Info.mobilephone, ConfigurationManager.AppSettings["SMSMESSAGEREFERAFRIEND"], smsURL);
                CRM_API.DB.SMSCampaign.SProc.InsertSMSNotifyText(Info.mobilephone, ConfigurationManager.AppSettings["SMSMESSAGEREFERAFRIEND"], Info.username, smsResponse == "-1" ? 0 : 1, "-", Info.username, "Refer a friend");
                Log.Info("ReferAFriend : SMS : Response : " + smsResponse);
            }
            catch (Exception ex)
            {
                Log.Error("ReferAFriend SMS Error : " + ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        } 
        #endregion

        #region PlaceOrder
        private HttpResponseMessage PlaceOrder(FreeSIMOrderInfo Info)
        {
            IEnumerable<FreeSIMOrderResponse> resValues = CRM_API.DB.FreeSIMOrder.SProc.PlaceOrder(Info);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.OK, new List<FreeSIMOrderResponse>() { new FreeSIMOrderResponse { errcode = -1, errmsg = "Failed" } }.ToArray());
            else
            {
                resValues.ElementAt(0).freesimid_out = resValues.ElementAt(0).freesimid_out.IndexOf(",") == 0 ? resValues.ElementAt(0).freesimid_out.Substring(1, resValues.ElementAt(0).freesimid_out.Length - 1) : resValues.ElementAt(0).freesimid_out; 
                try
                {
                    if (Info.sitecode == "MCM" || Info.sitecode == "BAU")
                    {
                        if (resValues.ElementAt(0).errcode == 0)
                        {
                            var api = new Mandrill.MandrillApi(ConfigurationManager.AppSettings["MAILCHIMPAPIKEY"]);
                            var message = new MandrillMessage();
                            //message.FromEmail = ConfigurationManager.AppSettings["QUEUEEMAILMAILFROM"];
                            message.AddGlobalMergeVars("FNAME", Info.firstname);
                            message.AddGlobalMergeVars("orderid", resValues.ElementAt(0).freesimid_out);
                            message.AddTo(Info.email);
                            IList<MandrillSendMessageResponse> response = api.Messages.SendTemplateAsync(message, ConfigurationManager.AppSettings["MAILCHIMPFREESIMORDER_" + Info.sitecode]).Result;
                            Log.Info("PlaceOrder Mail Response : " + JsonConvert.SerializeObject(response));
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("PlaceOrder Mail Error : " + ex.Message);
                }
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
            }
        }
        #endregion

    }
}
