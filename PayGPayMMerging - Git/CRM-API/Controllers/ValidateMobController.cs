﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml;

namespace CRM_API.Controllers
{
    public class ValidateMobController : ApiController
    {
        //
        // GET: /ValidateMob/


        public HttpResponseMessage get(string val)
        {
            try
            {
                CRM_API.Models.mobvalidation values=new CRM_API.Models.mobvalidation();
                List<CRM_API.Models.mobvalidation> objList = CRM_API.DB.viewtransaction.lottmmobvalidation(val);
                values.errcode = objList[0].errcode;
                values.errmsg = objList[0].errmsg;
                return Request.CreateResponse(HttpStatusCode.OK, values);
            }
            catch (WebException ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new List<CRM_API.Models.financeviewtrans>());
            }
        }

    }
}
