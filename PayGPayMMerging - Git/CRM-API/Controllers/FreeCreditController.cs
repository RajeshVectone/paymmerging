﻿using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using CRM_API.Models;
using CRM_API.DB;

namespace CRM_API.Controllers
{
    public class FreeCreditController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public HttpResponseMessage Post(FreeCreditInput input)
        {
            try
            {
                IEnumerable<FreeCreditOutput> resultValues = FreeCreditDB.GetDetails(input);
                if (resultValues == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound, new List<FreeCreditOutput>());
                else
                    return Request.CreateResponse(HttpStatusCode.OK, resultValues.ToArray());
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new List<CRM_API.Models.FreeCreditOutput>());
            }
        }
    }
}
