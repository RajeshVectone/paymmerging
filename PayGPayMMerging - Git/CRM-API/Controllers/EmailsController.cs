﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Web.Http;
using CRM_API.Models;
using Mandrill;
using Mandrill.Model;
using NLog;

namespace CRM_API.Controllers
{
    public class EmailsController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public HttpResponseMessage Post(EmailModel Model)
        {
            if (Model != null)
            {
                switch (Model.modetype)
                {
                    case Models.ModeType.GETEMAILQUEUEINFO:
                        return GetEmailQueueInfo(Model.request_id);
                    case Models.ModeType.GETAGENTINFO:
                        return GetAgentInfo();
                    //case Models.ModeType.EMAILQUEUEASSIGNTOUSER:
                    //    return EmailQueueAssignToUser(Model);
                    case Models.ModeType.GETEMAILQUEUEASSIGNEDINFO:
                        return GetEmailQueueAssignedInfo(Model.userid, Model.queue_id, Model.role_id, Model.status_id);
                    //case Models.ModeType.EMAILQUEUEASSIGNEDREPLY:
                    //    return EmailQueueSaveReplyText(Model.queue_id, Model.email_to, Model.email_cc, Model.email_subject, Model.email_body, Model.is_closed, Model.filename);
                    case Models.ModeType.GETMYEMAILOPENMAILITEMS:
                        return GetMyEmailOpenItems(Model.queue_id);
                    case Models.ModeType.GETMYEMAILOPENTICKETITEMS:
                        return GetEmailQueueCustomerHistory(Model.customer_email_id);
                    case Models.ModeType.GETEMAILVIEWTICKETSBYUSER:
                        return GetEmailViewTicketsByUser(Model.userid, Model.roleid, Model.status_id);
                    //case Models.ModeType.CHANGEEMAILQUEUESTATUS:
                    //    return ChangeEmailQueueStatus(Model.queue_id, Model.queue_status);
                    case Models.ModeType.REMOVEEMAILFROMLIST:
                        return RemoveEmailFromList(Model.request_id, Model.agent_name);
                    default:
                        return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.EmailModel>().ToArray());
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.EmailModel>().ToArray());
            }
        }

        //My Tickets
        private HttpResponseMessage GetEmailViewTicketsByUser(int userid, int roleid, int status_id = -1)
        {
            IEnumerable<CRM_API.Models.GetEmailViewTicketsByUserOutput> resValues = CRM_API.DB.Emails.GetEmailViewTicketsByUser(userid, roleid, status_id);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.GetEmailViewTicketsByUserOutput>().ToArray());
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
            }
        }

        private HttpResponseMessage GetEmailQueueCustomerHistory(string customer_email_id)
        {
            IEnumerable<CRM_API.Models.GetEmailQueueCustomerHistoryOutput> resValues = CRM_API.DB.Emails.GetEmailQueueCustomerHistory(customer_email_id);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.GetEmailQueueCustomerHistoryOutput>().ToArray());
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
            }
        }

        private HttpResponseMessage GetMyEmailOpenItems(int queue_id)
        {
            IEnumerable<CRM_API.Models.GetMyEmailOpenItemsOutput> resValues = CRM_API.DB.Emails.GetMyEmailOpenItems(queue_id);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.GetMyEmailOpenItemsOutput>().ToArray());
            else
            {
                if (resValues.Count() > 0)
                {
                    for (int i = 0; i < resValues.Count(); i++)
                    {
                        if (resValues.ElementAt(i).no_of_attachaments > 0)
                        {
                            List<EmailAttachments> lstEmailAttachments = new List<EmailAttachments>();
                            for (int j = 0; j < resValues.ElementAt(i).no_of_attachaments; j++)
                            {
                                try
                                {
                                    string networkPath = ConfigurationManager.AppSettings["EMAILQUEUEATTACHMENTBASEPATH"] + queue_id;
                                    NetworkCredential credentials = new NetworkCredential("administrator", "Vicarage_2008", "SQUAY");
                                    string myNetworkPath = string.Empty;

                                    using (new CRM_API.Helpers.ConnectToSharedFolder(networkPath, credentials))
                                    {
                                        var fileList = Directory.GetFiles(networkPath);

                                        if (fileList.Length > 0)
                                        {
                                            foreach (var item in fileList)
                                            {
                                                EmailAttachments attachment = new EmailAttachments();
                                                attachment.filename = Path.GetFileName(item.ToString());
                                                attachment.fileurl = ConfigurationManager.AppSettings["EMAILQUEUEATTACHMENTBASEURL"] + queue_id + "/" + Path.GetFileName(item.ToString());
                                                attachment.extension = Path.GetExtension(item.ToString()).Replace(".", "");
                                                attachment.extension = (attachment.extension.ToLower() != "doc" && attachment.extension.ToLower() != "bmp" && attachment.extension.ToLower() != "docx" && attachment.extension.ToLower() != "img" && attachment.extension.ToLower() != "jpeg" && attachment.extension.ToLower() != "jpg" && attachment.extension.ToLower() != "pdf" && attachment.extension.ToLower() != "png" && attachment.extension.ToLower() != "ppt" && attachment.extension.ToLower() != "txt" && attachment.extension.ToLower() != "log" && attachment.extension.ToLower() != "xls" && attachment.extension.ToLower() != "xlsx" && attachment.extension.ToLower() != "zip" && attachment.extension.ToLower() != "msg" && attachment.extension.ToLower() != "html" && attachment.extension.ToLower() != "htm" && attachment.extension.ToLower() != "pptx") ? "attachment" : attachment.extension.ToLower();
                                                lstEmailAttachments.Add(attachment);
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Log.Error("GetMyEmailOpenItems Attachments : " + ex.Message);
                                }
                            }
                            resValues.ElementAt(i).lstEmailAttachments = lstEmailAttachments;
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
            }
        }

        //private HttpResponseMessage EmailQueueAssignToUser(EmailModel Model)
        //{
        //    Model.mailLocation = System.Web.HttpContext.Current.Server.MapPath("~/Email_template/EmailQueueTemplate.html").TrimEnd('/');
        //    IEnumerable<CRM_API.Models.EmailEueueAssignToUserOutput> resValues = CRM_API.DB.Emails.EmailQueueAssignToUser(Model);
        //    if (resValues == null)
        //        return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.EmailEueueAssignToUserOutput>().ToArray());
        //    else
        //    {
        //        return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        //    }
        //}

        private HttpResponseMessage GetEmailQueueInfo(int request_id)
        {
            IEnumerable<CRM_API.Models.EmailModel> resValues = CRM_API.DB.Emails.GetEmailQueueInfo(request_id);

            if (request_id != 0 && resValues != null && resValues.Count() > 0 && resValues.ElementAt(0).no_of_attachaments > 0)
            {
                try
                {
                    List<CRM_API.Models.EmailAttachments> lstEmailAttachments = new List<EmailAttachments>();

                    string networkPath = ConfigurationManager.AppSettings["EMAILQUEUEATTACHMENTBASEPATH"] + resValues.ElementAt(0).pk_queue_id;
                    NetworkCredential credentials = new NetworkCredential("administrator", "Vicarage_2008", "SQUAY");
                    string myNetworkPath = string.Empty;

                    using (new CRM_API.Helpers.ConnectToSharedFolder(networkPath, credentials))
                    {
                        var fileList = Directory.GetFiles(networkPath);

                        if (fileList.Length > 0)
                        {
                            foreach (var item in fileList)
                            {
                                EmailAttachments attachment = new EmailAttachments();
                                attachment.filename = Path.GetFileName(item.ToString());
                                attachment.fileurl = ConfigurationManager.AppSettings["EMAILQUEUEATTACHMENTBASEURL"] + resValues.ElementAt(0).pk_queue_id + "/" + Path.GetFileName(item.ToString());
                                attachment.extension = Path.GetExtension(item.ToString()).Replace(".", "");
                                attachment.extension = (attachment.extension.ToLower() != "doc" && attachment.extension.ToLower() != "bmp" && attachment.extension.ToLower() != "docx" && attachment.extension.ToLower() != "img" && attachment.extension.ToLower() != "jpeg" && attachment.extension.ToLower() != "jpg" && attachment.extension.ToLower() != "pdf" && attachment.extension.ToLower() != "png" && attachment.extension.ToLower() != "ppt" && attachment.extension.ToLower() != "txt" && attachment.extension.ToLower() != "log" && attachment.extension.ToLower() != "xls" && attachment.extension.ToLower() != "xlsx" && attachment.extension.ToLower() != "zip" && attachment.extension.ToLower() != "msg" && attachment.extension.ToLower() != "html" && attachment.extension.ToLower() != "htm" && attachment.extension.ToLower() != "pptx") ? "attachment" : attachment.extension.ToLower();
                                lstEmailAttachments.Add(attachment);
                            }
                            resValues.ElementAt(0).lstEmailAttachments = lstEmailAttachments;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("GetEmailQueueInfo Attachment : " + ex.Message);
                }
            }

            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.EmailModel>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        public HttpResponseMessage GetAgentInfo()
        {
            IEnumerable<CRM_API.Models.AgentInfoModels> resValues = CRM_API.DB.Emails.GetAgentInfo();
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.AgentInfoModels>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        public HttpResponseMessage GetEmailQueueAssignedInfo(int user_id, int queue_id = 0, int role_id = 0, int status_id = -1)
        {
            IEnumerable<CRM_API.Models.GetEmailQueueAssignedInfoOutput> resValues = CRM_API.DB.Emails.GetEmailQueueAssignedInfo(user_id, queue_id, role_id, status_id);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.GetEmailQueueAssignedInfoOutput>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }
        //Remove Email
        private HttpResponseMessage RemoveEmailFromList(int request_id, string username)
        {
            IEnumerable<CRM_API.Models.ChangeEmailQueueStatusOutput> resValues = CRM_API.DB.Emails.RemoveEmailFromList(request_id, username);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.ChangeEmailQueueStatusOutput>().ToArray());
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
            }
        }
    }

}
