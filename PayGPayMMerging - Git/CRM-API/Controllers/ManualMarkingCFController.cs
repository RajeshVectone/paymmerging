﻿using System;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Globalization;
using System.Web.Http;
using System.Web.Script.Serialization;
using NLog;


namespace CRM_API.Controllers
{
    public class ManualMarkingCFController : ApiController
    {
        //
        // GET: /ManualMarkingCF/

        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public HttpResponseMessage Get(int UpldID, string UpldBy, string CollectionType,string SiteCode)
        {
            //  CRM_API.Models.ManualMarking values = new CRM_API.Models.ManualMarking();
            if (CollectionType == "CF")
            {
                var result = CRM_API.DB.PMBODDOperations.getManualMarkUploadCollectionFailure(UpldID,UpldBy);
                List<CRM_API.Models.ManualMarkUploadCF> lstRet = result.ToList();
                return Request.CreateResponse(HttpStatusCode.OK, lstRet[0].SuccessMesg);
            }
            else if (CollectionType == "DD")
            {
                var result = CRM_API.DB.PMBODDOperations.getManualMarkUploadDDCancellation(UpldID, UpldBy);
                List<CRM_API.Models.ManualMarkUploadCF> lstRet = result.ToList();
                string error = "-1";
                for (int l=0;l< lstRet.Count ;l++)
                {
                    string SMS1 = "TEST Dear Customer, We have been intimated by Your bank that the Direct Debit provided for Pay Monthly contract is cancelled." ;
                    string SMS2 = "TEST Please contact Vectone Support team on 322 immediately to provide alternate direct debit details to avoid service disruption.";
                    if (lstRet[0].mobileno.Trim() != "")
                    {
                        string mobileno = lstRet[0].mobileno.Trim() ;
                        string[] res1 = new Helpers.SendSMS().Send(false, mobileno, "111", SMS1, 0);
                        string[] res2 = new Helpers.SendSMS().Send(false, mobileno, "111", SMS2, 0); 
                        if (res1 != null && res1.Count() > 0)
                        {
                                error += "-1";
                        }
                    }
                }
               
                return Request.CreateResponse(HttpStatusCode.OK, lstRet[0].SuccessMesg);
                    

            }
            else
                return Request.CreateResponse(HttpStatusCode.PartialContent, "Nothing!");
        }

    }
}
