﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NLog;

namespace CRM_API.Controllers
{
    public class HappyBundleDaillingNumberController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        //
        // GET api/HappyBundle/

        public HttpResponseMessage Get()
        {
            IEnumerable<CRM_API.Models.HappyBundle.countrylist> resValues =
                CRM_API.DB.HappyBundle.SProc.SubscribedBundle();
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.HappyBundle.countrylist>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }


        // POST api/HappyBundle/
        [HttpPost]
        public HttpResponseMessage Post(CRM_API.Models.HappyBundle.HappyBundleSearch resBundle)
        {
            DB.Common.ErrCodeMsg errResult = new DB.Common.ErrCodeMsg()
           {
               errcode = -1,
               errsubject = "General Error",
               errmsg = "Empty New History"
           };
            var result = new CRM_API.Models.HappyBundle.HappyBundleSearch();
            switch (resBundle.Bundle_Type)
            {
                //Added by karthik CRMT-231 HappyBundle
                case Models.HappyBundle.HappyBundleType.HappyBundleList:
                    return GetHappyBundleDailling(resBundle);
                case Models.HappyBundle.HappyBundleType.HappyBundleListInsert:
                    return InsertDaillingnumber(resBundle);
                case Models.HappyBundle.HappyBundleType.Bundleid:
                    return Checkingbundleid(resBundle);  
                default: break;
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
    
        //Added by karthik CRMT-231 Happy bundle Dailling number
        HttpResponseMessage GetHappyBundleDailling(CRM_API.Models.HappyBundle.HappyBundleSearch resBundle)
        {
            IEnumerable<CRM_API.Models.HappyBundle.HappyBundlelist> resValues =
                           CRM_API.DB.HappyBundle.SProc.HappyBundleDaillingNumberList(resBundle.msisdn, resBundle.sitecode);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.HappyBundle.HappyBundlelist>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        //Added by karthik CRMT-231 Happy bundle Dailling number
        HttpResponseMessage InsertDaillingnumber(CRM_API.Models.HappyBundle.HappyBundleSearch resBundle)
        {
            IEnumerable<CRM_API.Models.HappyBundle.HappyBundlelist> resValues =
                           CRM_API.DB.HappyBundle.SProc.InsertHappyBundleDaillingnumber(resBundle.msisdn, resBundle.sitecode,resBundle.Dailnumber);
            /*** Happy Bundle Dailling Number SMS Send  ***/
            if (resValues.Count() > 0)
            {
                Helpers.SendSMS sms = new Helpers.SendSMS();
                if (resValues.FirstOrDefault().errcode == 0)
                {
                    foreach(var result in resValues)
                    {
                         string Mappingnumber=result.Mapping_number;
                         HappyBundleDaillingSMS(resBundle.msisdn, resBundle.Dailnumber, Mappingnumber);
                    }
                    
                   
                }
            }
            /*** Happy Bundle Dailling Number SMS Send  ***/
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.HappyBundle.HappyBundlelist>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }
        //Added by karthik CRMT-231 Happy bundle Dailling number
        HttpResponseMessage Checkingbundleid(CRM_API.Models.HappyBundle.HappyBundleSearch resBundle)
        {
            IEnumerable<CRM_API.Models.HappyBundle.HappyBundlelist> resValues =
                           CRM_API.DB.HappyBundle.SProc.GetBundleidlist(resBundle.msisdn, resBundle.sitecode);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.HappyBundle.HappyBundlelist>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }
        public bool HappyBundleDaillingSMS(string mobileNo, string destinationnumber, string Mappingnumber)
        {
            try
            {
                string smsText = string.Format("National Number added successfully. You can call +{0} by dialing +{1} Thank you.", destinationnumber, Mappingnumber);
                string[] res = new Helpers.SendSMS().Send(false, mobileNo, "111", smsText, 0);
                if (res != null && res.Count() > 0)
                {
                    return !(res[0] == "-1");
                }
                else
                 return false;
            }
            catch (Exception ex)
            {
                Log.Error("PAYGHappyBundleDaillingNumberController.HappyBundleDaillingSMS", ex.Message);
                return false;
            }
        }
    }
}
