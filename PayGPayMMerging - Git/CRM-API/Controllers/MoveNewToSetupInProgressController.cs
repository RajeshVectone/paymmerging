﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CRM_API.Models;

namespace CRM_API.Controllers
{
    public class MoveNewToSetupInProgressController : ApiController
    {
        // GET api/movenewtosetupinprogress
        public string Get(string CustId_AccNo_SortCode)
        {
            var result = CRM_API.DB.PMBODDOperations.MoveNewCustomersToSetupInProgress(CustId_AccNo_SortCode);
            return result ;
        }

        // GET api/movenewtosetupinprogress/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/movenewtosetupinprogress
        //public void Post([FromBody]string value)
        //{
        //}
        //Modified by BSK for Improvement : Jira CRMT-141
        public HttpResponseMessage Post(CRM_API.Models.SetupInProgress CustId_AccNo_SortCode)
        {
            var result = CRM_API.DB.PMBODDOperations.UpdateSetupInProgress(CustId_AccNo_SortCode);
            return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
        }

        // PUT api/movenewtosetupinprogress/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/movenewtosetupinprogress/5
        public void Delete(int id)
        {
        }
    }
}
