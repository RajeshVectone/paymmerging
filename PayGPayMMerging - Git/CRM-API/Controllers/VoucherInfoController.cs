﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CRM_API.Controllers
{
    public class VoucherInfoController : ApiController
    {
        public HttpResponseMessage Post(CRM_API.Models.CheckVoucher Model)
        {
            try
            {
                switch (Model.SearchType)
                {
                    case Models.CheckBy.CBS:
                        return GetVoucherByCBS(Model);
                    case Models.CheckBy.PIN:
                        return GetVoucherByPin(Model);
                    default: break;
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, 
                    new Models.Common.ErrCodeMsg() 
                    { 
                        errcode = -1, errmsg = "Post Method:"+ ex.Message, errsubject= "General Info"                        
                    });
            }
            return Request.CreateResponse(HttpStatusCode.OK, Model);
        }
        private HttpResponseMessage GetVoucherByCBS(CRM_API.Models.CheckVoucher In)
        {
            try
            {
                var result = CRM_API.DB.Voucher.SProc.VoucherCheckbyCBS(In.SearchText, In.Sitecode);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK,
                    new CRM_API.Models.VoucherInfo()
                    {
                        errcode = -1,
                        errmsg = "[GetVoucherByCBS]: " + ex.Message,
                        errsubject = "General Info"
                    });
            }
        }
        private HttpResponseMessage GetVoucherByPin(CRM_API.Models.CheckVoucher In)
        {
            try
            {
                var result = CRM_API.DB.Voucher.SProc.VoucherCheckbyPin(In.SearchText, In.Sitecode);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK,
                    new CRM_API.Models.VoucherInfo()
                    {
                        errcode = -1,
                        errmsg = "[GetVoucherByPin]: " + ex.Message,
                        errsubject = "General Info"
                    });
            }
        }
    }
}
