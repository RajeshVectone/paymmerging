﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NLog;

namespace CRM_API.Controllers
{
    public class CDRController : ApiController
    {
        //
        // GET: /CDR/
        // POST : api/cdr/
        [HttpPost]
        public HttpResponseMessage POST(CRM_API.Models.CDR.CallHistorySearch In)
        {
            DB.Common.ErrCodeMsg errResult = new DB.Common.ErrCodeMsg()
            {
                errcode = -1,
                errsubject = "General Error",
                errmsg = "Empty New Order"
            };
            try
            {
                switch (In.HistoryType)
                {
                    case Models.CDR.CDRType.CallHistory:
                        return GetCallHistory(In);
                    case Models.CDR.CDRType.SMSHistory:
                        return GetSMSHistory(In);
                    case Models.CDR.CDRType.GPRSHistory:
                        return GetGPRSHistory(In);
                    default: break;
                }
            }
            catch (Exception ex)
            {
                errResult.errmsg = "WEB-API: " + ex.Message;
                errResult.errcode = -1;
            }
            return Request.CreateResponse(HttpStatusCode.OK, errResult);
        }
        private HttpResponseMessage GetCallHistory(CRM_API.Models.CDR.CallHistorySearch In)
        {
            IEnumerable<CRM_API.Models.CDR.CallHistory> result = CRM_API.Helpers.History.CDRHistory.GetDataCallHistoryByMsisdn(In.mobileno, In.Date_from_month_string, In.Date_from_day_string, In.Date_To_day_string, In.Date_To_year_string);
            if (In.Sitecode.ToLower() == "gcm")
                result.Union(CRM_API.DB.CDR.SProc.GocheapCallCDR(In.mobileno, In.Date_From, In.Date_To, In.Sitecode,1));
                        
            if (result == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.CDR.CallHistory>().ToArray());
            else if (result.Count() == 0)
                return Request.CreateResponse(HttpStatusCode.OK, new List<CRM_API.Models.CDR.CallHistory>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
        }
        private HttpResponseMessage GetGPRSHistory(CRM_API.Models.CDR.CallHistorySearch In)
        {
            IEnumerable<CRM_API.Models.CDR.GPRSHistory> result = CRM_API.Helpers.History.CDRHistory.GetDataGPRSByMsisdn(In.mobileno, In.Date_From_date_string, In.Date_To_date_string);
            if (In.Sitecode.ToLower() == "gcm")
                result.Union(CRM_API.DB.CDR.SProc.GocheapGPRSCDR(In.mobileno, In.Date_From, In.Date_To, In.Sitecode,3));
            if (result == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.CDR.GPRSHistory>().ToArray());
            else if (result.Count() == 0)
                return Request.CreateResponse(HttpStatusCode.OK, new List<CRM_API.Models.CDR.GPRSHistory>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
        }

        private HttpResponseMessage GetSMSHistory(CRM_API.Models.CDR.CallHistorySearch In)
        {
            In.PageperRow = In.PageperRow == 0 ? 11 : In.PageperRow;
            In.PageNumber = In.PageNumber == 0 ? 1 : In.PageNumber;

            IEnumerable<CRM_API.Models.CDR.SMSHistory> result = CRM_API.DB.SMSCdr.SProc.GetSMSCDR(In.mobileno, In.Date_From, In.Date_To, In.Sitecode, In.PageNumber, In.PageperRow);
            if (In.Sitecode.ToLower() == "gcm")
                result.Union(CRM_API.DB.CDR.SProc.GocheapSMSCDR(In.mobileno, In.Date_From, In.Date_To, In.Sitecode,2));    
            if (result == null )
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.CDR.SMSHistory>().ToArray());
            else if (result.Count() == 0)
                return Request.CreateResponse(HttpStatusCode.OK, new List<CRM_API.Models.CDR.SMSHistory>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
        }
    }
}
