﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using NLog;
using System.Text;
using System.Diagnostics;
using System.Web.Hosting;
using System.IO;
using System.Web;
using AttributeRouting.Web.Mvc;

namespace CRM_API.Controllers
{
    public class Customer2in1Controller : ApiController
    {
        private string currency = "GBP";
        private const double charge2in1 = 5.00;
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private string senderSMS = "111";
        private string activateTextRestart2 = "Local Roaming activated successfully. Please restart your mobile for the changes to take into effect.";
        private string activateTextRestart1 = "Your SIM is not compatible for Local Roaming. Please contact our customer support team at 322 for alternate options.";

        private string activateTextRestart = "We have just added a new number as requested. Please restart your handset to complete the configuration.";

        // GET api/customer2in1/MobileNo
        public HttpResponseMessage Get(string id)
        {

            IEnumerable<CRM_API.Models.Customer2in1Records> resValues =
                CRM_API.DB.Customer2in1.SProc.List2in1Records1(id);


            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.Customer2in1Records>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        // POST api/customer2in1/
        
        [HttpPost]
        public HttpResponseMessage POST(CRM_API.Models.Customer2in1Records orderRecord)
        {
            Log.Debug("--------------------------------------------------------------------");
            

            DB.Common.ErrCodeMsg errResult = new DB.Common.ErrCodeMsg()
            {
                errcode = -1,
                errsubject = "General Error",
                errmsg = "Empty New Order"
            };

            Log.Debug("Customer2in1Controller.Post: Started");
            Log.Info(new JavaScriptSerializer().Serialize(orderRecord));
            if (orderRecord != null)
            {
                //31-Jul-2015 : Moorthy Added
                System.Web.HttpContext.Current.Session["GlobalProduct"] = orderRecord.product_code;
                // Fix an empty sitecode
                if (string.IsNullOrEmpty(orderRecord.sitecode))
                {
                    if (string.IsNullOrEmpty(orderRecord.product_code))
                    {
                        orderRecord.sitecode = CRM_API.DB.Sitecode.SProc.GetSiteCodeByMobileNo(orderRecord.mobileno);
                    }
                    else
                    {
                        orderRecord.sitecode = CRM_API.DB.Sitecode.SProc.GetSiteCodeByBrand(orderRecord.product_code);
                    }
                }

                switch (orderRecord.order_type)
                {
                    case Models.Customer2in1OrderType.NewOrder:
                        // Check the validity of data
                        if (IsValidFreesimOrder(orderRecord, ref errResult))
                            NewOrder(orderRecord, ref errResult);
                        break;
                    case Models.Customer2in1OrderType.NewOrderByBalance:
                        NewOrderByBalance(orderRecord, out errResult);
                        break;
                    case Models.Customer2in1OrderType.NewOrderByCard:
                        NewOrderByCard(orderRecord, ref errResult);
                        break;
                    case Models.Customer2in1OrderType.ReActivateByBalance:
                        ReActivateByBalance(orderRecord, ref errResult);
                        break;
                    case Models.Customer2in1OrderType.ReActivateByCard:
                        ReActivateByCard(orderRecord, ref errResult);
                        break;
                    case Models.Customer2in1OrderType.ChangePaymentToBalance:
                        ChangePaymentToBalance(orderRecord, ref errResult);
                        break;
                    case Models.Customer2in1OrderType.ChangePaymentToCard:
                        ChangePaymentToCard(orderRecord, ref errResult);
                        break;
                    case Models.Customer2in1OrderType.Cancellation:
                        Cancel(orderRecord, ref errResult);
                        break;
                    case Models.Customer2in1OrderType.ActivateOTA:
                        return ActivateOTA(orderRecord);
                    case Models.Customer2in1OrderType.ActivateOTAFree:
                        return ActivateOTAFree(orderRecord);
                    case Models.Customer2in1OrderType.ActivateSWAP:
                        return ActivateSwap(orderRecord);
                    case Models.Customer2in1OrderType.InsertSIMOrder:
                        return InsertSIMOrder(orderRecord);
                    case Models.Customer2in1OrderType.ReactivationNoPayment:
                        ReActivateByCardNoPayment(orderRecord, out errResult);
                        break;
                    case Models.Customer2in1OrderType.NewCountryList:
                        return newCountryList(orderRecord);
                    case Models.Customer2in1OrderType.GetCountryCurrency:
                        return GetCountryCurrency(orderRecord);
                    case Models.Customer2in1OrderType.ActivateOTAVectoneLocal2in1:
                        return ActivateOTAVectoneLocal2in1(orderRecord);
                    case Models.Customer2in1OrderType.ActivateOTAVectone2in1:
                        return ActivateOTAVectone2in1(orderRecord);
                    default:
                        errResult.errmsg = "Unknown Order Type";
                        break;

                }
                orderRecord.errcode = errResult.errcode;
                orderRecord.errmsg = errResult.errmsg;
            }
            else
            {
                orderRecord = new Models.Customer2in1Records()
                {
                    errcode = errResult.errcode,
                    errmsg = errResult.errmsg
                };
            }
            Log.Debug(string.Format("Customer2in1Controller.Post: Result Subject={0}; Msg={1}", errResult.errsubject, errResult.errmsg));
            return Request.CreateResponse(HttpStatusCode.OK, errResult);
        }

        bool IsValidFreesimOrder(CRM_API.Models.Customer2in1Records newOrder, ref DB.Common.ErrCodeMsg errResult)
        {
            bool result = false;

            if (string.IsNullOrEmpty(newOrder.first_name))
            {
                errResult.errcode = -1;
                errResult.errmsg = "Empty First Name";
                errResult.errsubject = "Order Failed";
            }
            else if (string.IsNullOrEmpty(newOrder.last_name))
            {
                errResult.errcode = -1;
                errResult.errmsg = "Empty Last Name";
                errResult.errsubject = "Order Failed";
            }
            else if (string.IsNullOrEmpty(newOrder.address1))
            {
                errResult.errcode = -1;
                errResult.errmsg = "Empty Address 1";
                errResult.errsubject = "Order Failed";
            }
            else if (string.IsNullOrEmpty(newOrder.postcode))
            {
                errResult.errcode = -1;
                errResult.errmsg = "Empty PostCode";
                errResult.errsubject = "Order Failed";
            }
            else if (string.IsNullOrEmpty(newOrder.country))
            {
                errResult.errcode = -1;
                errResult.errmsg = "Empty Country";
                errResult.errsubject = "Order Failed";
            }
            else
                result = true;

            return result;
        }

        void NewOrder(CRM_API.Models.Customer2in1Records newOrder, ref DB.Common.ErrCodeMsg errResult)
        {
            if (!IsValidFreesimOrder(newOrder, ref errResult))
                return;

            string home_country = CRM_API.DB.Country.CountrySProc.GetCountryByMSIDN(newOrder.mobileno);

            Log.Debug(string.Format("Customer2in1Controller.NewOrder: {0}", home_country));

            errResult = CRM_API.DB.Customer2in1.SProc.Insert2in1Records(newOrder, charge2in1, Request.RequestUri.PathAndQuery);

            if (errResult.errcode == 0)
            {
                errResult.errsubject = "Ordered Successfully";
                errResult.errmsg = string.Format("You have placed an order for 2 in 1 {0}-{1} successfully. An email will be sent to the customer shortly. Order Number {2}", home_country, newOrder.imsi_country, newOrder.payment_ref);
                
            }
            else
            {
                errResult.errsubject = "Order Failed";
                if (string.IsNullOrEmpty(newOrder.payment_ref) || newOrder.payment_ref == "-1")
                    errResult.errmsg = string.Format("{0}", errResult.errmsg);
                else
                    errResult.errmsg = string.Format("{0}. Order Number {1}", errResult.errmsg, newOrder.payment_ref);
            }

            Log.Debug(string.Format("Customer2in1Controller.NewOrder: {0}", errResult.errmsg));
        }

        void NewOrderByBalance(CRM_API.Models.Customer2in1Records newOrder, out DB.Common.ErrCodeMsg errResult)
        {
            Log.Debug("Customer2in1Controller.NewOrderByBalance");

            string home_country = CRM_API.DB.Country.CountrySProc.GetCountryByMSIDN(newOrder.mobileno);

            //errResult = CRM_API.DB.Customer2in1.SProc.Insert2in1RecordsByCredit(newOrder, charge2in1, Request.RequestUri.PathAndQuery);
            errResult = CRM_API.DB.Customer2in1.SProc.Insert2in1RecordsByCounty(newOrder.mobileno, newOrder.imsi_country);
            if (errResult.errcode == 0)
            {
                errResult.errsubject = "Ordered Successfully";
                //errResult.errmsg = string.Format("You have placed an order for 2 in 1 {0}-{1} successfully. An email will be sent to the customer shortly. Balance deducted {2:0.00}", home_country, newOrder.imsi_country, charge2in1);
                errResult.errmsg = string.Format("You have placed an order for 2 in 1 {0}-{1} successfully. An email will be sent to the customer shortly.", home_country, newOrder.imsi_country, charge2in1);
            }
            else
                errResult.errsubject = "Order Failed";
        }


        void NewOrderByCard(CRM_API.Models.Customer2in1Records newOrder, ref DB.Common.ErrCodeMsg errResult)
        {
            string productcode = GetProductCode(newOrder.sitecode);
            System.Web.HttpContext.Current.Session["GlobalProduct"] = productcode;

            Log.Debug("Customer2in1Controller.NewOrderByCard");

            // Check the validity of data
            if (!IsValidFreesimOrder(newOrder, ref errResult))
                return;

            // TODO -- Check email
            string email = string.IsNullOrEmpty(newOrder.email) ? "dony.isnandi@mundio.com" : newOrder.email;

            // Charge the card first
            CRM_API.Helpers.Payment payment = new Helpers.Payment();

            //Get Currency and price
            var Currency = CRM_API.DB.Customer2in1.SProc.Get2in1Currency(newOrder.product_code, newOrder.imsi_country);

            // user select a new card
            if (string.IsNullOrEmpty(newOrder.SubscriptionID))
            {
                Log.Debug("Customer2in1Controller.Post: Charge New Card");
                payment.Authorize(newOrder as Models.Payment.CardDetails,
                    newOrder.sitecode, newOrder.mobileno, email, Currency.Currency, Currency.amount, productcode);
                newOrder.SubscriptionID = payment.SubscriptionId;
            }
            else
            {
                Log.Debug("Customer2in1Controller.Post: Charge Saved Card");
                //payment.CybersourceCharge(newOrder.mobileno, newOrder.mobileno, newOrder.SubscriptionID, Currency.Currency, Currency.amount);
                string referenceCode = string.Format("{0}-{1}-{2:yyMMddHHmm}", "VMUK", string.Format("PP{0:00}-RX", Currency.amount), DateTime.Now);
                Models.Payment.CardDetails requestData = new Models.Payment.CardDetails();
                payment.AuthorizeWithout3DS(requestData as Models.Payment.CardDetails, "MCM", newOrder.mobileno, "PMBO", "", Currency.Currency, Currency.amount, newOrder.mobileno, newOrder.SubscriptionID, referenceCode, productcode);
            }

            if (payment.Decision == "ACCEPT" || payment.Decision == "APPROVAL")
            {
                newOrder.payment_ref = payment.MerchantReferenceCode;
                NewOrder(newOrder, ref errResult);
                /*
                errResult = CRM_API.DB.Customer2in1.SProc.Insert2in1Records(newOrder, charge2in1, Request.RequestUri.PathAndQuery);

                if (errResult.errcode == 0)
                {
                    errResult.errsubject = "Ordered Successfully";
                    errResult.errmsg = string.Format("You have placed an order for 2 in 1 UK-{0} successfully. An email will be sent to the customer shortly. Order Number {1}", newOrder.imsi_country, payment.MerchantReferenceCode);
                }
                else
                {
                    errResult.errsubject = "Order Failed";
                    if (string.IsNullOrEmpty(payment.MerchantReferenceCode) || payment.MerchantReferenceCode == "-1")
                        errResult.errmsg = string.Format("{0}", errResult.errmsg);
                    else
                        errResult.errmsg = string.Format("{0}. Order Number {1}", errResult.errmsg, payment.MerchantReferenceCode);
                }
                */
            }
            else
            {
                errResult.errcode = -1;
                errResult.errsubject = "Payment Failed";
                if (string.IsNullOrEmpty(payment.MerchantReferenceCode) || payment.MerchantReferenceCode == "-1")
                    errResult.errmsg = string.Format("{0}", payment.Description);
                else
                    errResult.errmsg = string.Format("{0} Order Number {1}", payment.Description, payment.MerchantReferenceCode);
            }
        }

        void ReActivateByBalance(CRM_API.Models.Customer2in1Records order, ref DB.Common.ErrCodeMsg errResult)
        {
            Log.Debug("Customer2in1Controller.ReActivateByBalance");
            if (order.order_id <= 0)
            {
                errResult.errcode = -1;
                errResult.errsubject = "General Error";
                errResult.errmsg = "Empty New Order";
            }
            else
            {
                order = CRM_API.DB.Customer2in1.SProc.GetStatus(order.order_id, "", "", order.crm_login);
                if (string.IsNullOrEmpty(order.order_status) || order.order_status != "INACTIVE")
                {
                    errResult.errcode = -1;
                    errResult.errsubject = "General Error";
                    errResult.errmsg = "2in1 cannot be re-activate";
                }
                else
                {
                    errResult = CRM_API.DB.Customer2in1.SProc.ReActivateByCredit(order.order_id, order.crm_login, charge2in1);
                    if (errResult.errcode == 0)
                    {
                        string home_country = CRM_API.DB.Country.CountrySProc.GetCountryByMSIDN(order.mobileno);
                        errResult.errsubject = "ReActivated Successfully";
                        errResult.errmsg = string.Format("You have reactivated 2 in 1 {0}-{1} successfully. Balance deducted GBP{2:0.00}", home_country, order.imsi_country, charge2in1);
                    }
                    else
                        errResult.errsubject = "ReActivated Failed";
                }
            }
        }

        private string GetProductCode(string siteCode)
        {
            string productcode = "VMUK";
            if (siteCode == "MCM")
                productcode = "VMUK";
            else if (siteCode == "BAU")
                productcode = "VMAT";
            else if (siteCode == "MFR")
                productcode = "VMFR";
            else if (siteCode == "BNL")
                productcode = "VMNL";
            return productcode;
        }

        void ReActivateByCard(CRM_API.Models.Customer2in1Records order, ref DB.Common.ErrCodeMsg errResult)
        {
            string productcode = GetProductCode(order.sitecode);
            System.Web.HttpContext.Current.Session["GlobalProduct"] = productcode;

            Log.Debug("Customer2in1Controller.ReActivateByCard");
            if (order.order_id <= 0)
            {
                errResult.errcode = -1;
                errResult.errsubject = "General Error";
                errResult.errmsg = "Empty New Order";
            }
            else
            {
                CRM_API.Models.Customer2in1Records currentStat = CRM_API.DB.Customer2in1.SProc.GetStatus(order.order_id, order.SubscriptionID, order.mobileno, order.crm_login);
                if (string.IsNullOrEmpty(currentStat.order_status) || currentStat.order_status != "INACTIVE")
                {
                    errResult.errcode = -1;
                    errResult.errsubject = "General Error";
                    errResult.errmsg = "2in1 cannot be re-activate";
                }
                else
                {
                    // TODO -- Check email
                    string email = string.IsNullOrEmpty(order.email) ? "dony.isnandi@mundio.com" : order.email;

                    // Charge the card first
                    CRM_API.Helpers.Payment payment = new Helpers.Payment();

                    //Get Currency and price
                    string origincountry = order.country;
                    if (string.IsNullOrEmpty(origincountry))
                        origincountry = order.card_country;
                    var Currency = CRM_API.DB.Customer2in1.SProc.Get2in1Currency(order.product_code, origincountry);

                    // user select a new card
                    if (string.IsNullOrEmpty(order.SubscriptionID))
                    {
                        Log.Debug("Customer2in1Controller.ReActivateByCard: Charge New Card");
                        payment.Authorize(order as Models.Payment.CardDetails, order.sitecode, order.mobileno, email, Currency.Currency, Currency.amount, productcode);
                        order.SubscriptionID = payment.SubscriptionId;
                        // TODO -- Update Subscription Id
                    }
                    else
                    {
                        Log.Debug("Customer2in1Controller.ReActivateByCard: Charge Saved Card");
                        //payment.CybersourceCharge(order.mobileno, order.mobileno, order.SubscriptionID, Currency.Currency, Currency.amount);
                        string referenceCode = string.Format("{0}-{1}-{2:yyMMddHHmm}", "VMUK", string.Format("PP{0:00}-RX", Currency.amount), DateTime.Now);
                        Models.Payment.CardDetails requestData = new Models.Payment.CardDetails();
                        payment.AuthorizeWithout3DS(requestData as Models.Payment.CardDetails, "MCM", order.mobileno, "PMBO", "", Currency.Currency, Currency.amount, order.mobileno, order.SubscriptionID, referenceCode, productcode);
                    }

                    if (payment.Decision == "ACCEPT")
                    {
                        string home_country = CRM_API.DB.Country.CountrySProc.GetCountryByMSIDN(order.mobileno);
                        DB.Customer2in1.SProc.ChangeRenewalMethod(order.order_id, order.crm_login, 2, order.SubscriptionID);
                        errResult.errcode = 0;
                        errResult.errsubject = "ReActivated Successfully";
                        errResult.errmsg = string.Format("You have reactivated 2 in 1 {0}-{1} successfully. Payment Number {2}", home_country, order.imsi_country, payment.MerchantReferenceCode);
                    }
                    else
                    {
                        errResult.errcode = -1;
                        errResult.errsubject = "ReActivated Failed";
                        errResult.errmsg = payment.Description;
                    }
                }
            }
        }

        void ChangePaymentToBalance(CRM_API.Models.Customer2in1Records order, ref DB.Common.ErrCodeMsg errResult)
        {
            Log.Debug("Customer2in1Controller.ChangePaymentToBalance");
            if (order.order_id <= 0)
            {
                errResult.errcode = -1;
                errResult.errsubject = "General Error";
                errResult.errmsg = "Empty New Order";
            }
            else
            {
                order = CRM_API.DB.Customer2in1.SProc.GetStatus(order.order_id, "", "", order.crm_login);
                if (string.IsNullOrEmpty(order.order_status) || order.order_status != "ACTIVE")
                {
                    errResult.errcode = -1;
                    errResult.errsubject = "General Error";
                    errResult.errmsg = "2in1 status should be active";
                }
                else
                {
                    errResult = DB.Customer2in1.SProc.ChangeRenewalMethod(order.order_id, order.crm_login, 1, string.Empty);
                    errResult.errsubject = "Change Payment to Credit";
                    if (errResult.errcode == 0)
                        errResult.errmsg = "Successfully changed";
                }
            }
        }

        void ChangePaymentToCard(CRM_API.Models.Customer2in1Records order, ref DB.Common.ErrCodeMsg errResult)
        {
            Log.Debug("Customer2in1Controller.ChangePaymentToCard");
            if (order.order_id <= 0)
            {
                errResult.errcode = -1;
                errResult.errsubject = "General Error";
                errResult.errmsg = "Empty New Order";
            }
            else
            {
                order = CRM_API.DB.Customer2in1.SProc.GetStatus(order.order_id, order.SubscriptionID, "", order.crm_login);
                if (string.IsNullOrEmpty(order.order_status) || order.order_status != "ACTIVE")
                {
                    errResult.errcode = -1;
                    errResult.errsubject = "General Error";
                    errResult.errmsg = "2in1 status should be active";
                }
                else
                {
                    // TODO - Change the Subscription too
                    errResult = DB.Customer2in1.SProc.ChangeRenewalMethod(order.order_id, order.crm_login, 2, order.SubscriptionID);
                    errResult.errsubject = "Change Payment to Card";
                    if (errResult.errcode == 0)
                        errResult.errmsg = "Successfully changed";
                }
            }
        }

        void Cancel(CRM_API.Models.Customer2in1Records orderToCancel, ref DB.Common.ErrCodeMsg errResult)
        {
            Log.Debug("Customer2in1Controller.Cancel");
            if (orderToCancel.order_id <= 0)
            {
                errResult.errcode = -1;
                errResult.errsubject = "Cancel Subscription";
                errResult.errmsg = "Empty Order";
            }
            else
            {
                errResult = DB.Customer2in1.SProc.Cancel(orderToCancel.order_id, orderToCancel.crm_login);
                errResult.errsubject = "Cancel Subscription";
                if (errResult.errcode == 0)
                {
                    errResult.errmsg = "2in1 Subscription has succesfully cancelled";
                }
            }
        }

        //24-Jan-2017 : Modified for ActivateOTA not working 
        HttpResponseMessage ActivateOTA(CRM_API.Models.Customer2in1Records orderToActivate)
        {
            try
            {
                int iThreadSleep = 20000;
                
                string filename = string.Empty;
                //path create 
                string file = "Country.csv";
                string filePath = HostingEnvironment.MapPath("~/App_Data/" + "/" + file);
                //check exists file
                try
                {
                    if (System.IO.File.Exists(filePath))
                    {
                        System.GC.Collect();
                        System.GC.WaitForPendingFinalizers();
                        System.IO.File.Delete(filePath);
                    }
                }
                catch { }

                // else if (!System.IO.File.Exists(file))
                {

                    //System.IO.FileStream f = System.IO.File.Create(file);
                    //f.Close();
                    StringBuilder sb = new StringBuilder();
                    var Currency2 = CRM_API.DB.Customer2in1.SProc.Get2in1Currency2(orderToActivate.mobileno, orderToActivate.imsi_country);
                    var Currency1 = CRM_API.DB.Customer2in1.SProc.Get2in1Currency1(orderToActivate.mobileno, orderToActivate.imsi_country);



                    string OTA = Convert.ToString(Currency1.SPI) + " " + Convert.ToString(Currency1.KIC) + " " + Convert.ToString(Currency1.KID) + " " + Convert.ToString(Currency1.TAR) + " " + Convert.ToString(Currency1.Counter);
                    if (Currency1.errcode == 0)
                    {
                        //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F80A0A40000026F00A0DC030412034672616E6365FFFFFFFFFFFFFFFFFFFFFFFFA0A40000026F02A0DC02040302F803");
                        //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F80A0A40000026F07A0DC010409082940809021000000A0A40000026F07A0DC020409082943108901000000A0A40000026F78A0DC0104020001A0A40000");
                        //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F80A0A40000026F42A0DC010429534D532043656E7472652031FDFFFFFFFFFFFFFFFFFFFFFFFF07911356039900F0FFFFFFFFFFFFFF");
                        //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F20A0A40000026F07A0DC020409082943108901000000A0A40000026F78A0DC0104020001");
                        //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F80A0A40000026F42A0DC010429534D532043656E7472652031FDFFFFFFFFFFFFFFFFFFFFFFFF07913357999999F0FFFFFFFFFFFFFF");
                        //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F80A0A40000026F07A0DC030409082980138901000000A0A40000026F78A0DC0304020001");
                        //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F80A0A40000026F42A0DC030429534D532043656E7472652031FDFFFFFFFFFFFFFFFFFFFFFFFF0791448729199965FFFFFFFFFFFFFF");
                        //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F80A0A40000026F42A0DC030429534D532043656E7472652031FDFFFFFFFFFFFFFFFFFFFFFFFF0791448729199965FFFFFFFFFFFFFF");
                        sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd1));
                        sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd2));
                        sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd3));
                        sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd4));
                        sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd5));
                        sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd6));
                        sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd7));
                        sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd8));
                        byte[] temp = System.Text.Encoding.UTF8.GetBytes(sb.ToString());
                        System.IO.File.WriteAllBytes(filePath, temp);
                        Process notePad = new Process();
                        //   notePad.StartInfo.FileName = @"C:\Users\user\Desktop\CRM\CRM-API\App_Data\OTA.exe";
                        notePad.StartInfo.FileName = HostingEnvironment.MapPath("~/App_Data/" + "/" + "OTA.exe");
                        notePad.StartInfo.WorkingDirectory = HostingEnvironment.MapPath("~/App_Data/");
                        notePad.StartInfo.Arguments = OTA + " " + file;
                        notePad.StartInfo.RedirectStandardOutput = true;
                        notePad.StartInfo.UseShellExecute = false;
                        notePad.Start();
                        StreamReader s = notePad.StandardOutput;
                        String output1 = s.ReadToEnd();
                        string test = output1;
                        string[] result = test.Replace("\r\n", "\n").Split("\n".ToCharArray());
                        string uni1 = result[0].ToString();
                        string uni2 = result[1].ToString();
                        string uni3 = result[2].ToString();
                        string uni4 = result[3].ToString();
                        string[] uni11 = uni1.Split(',');
                        string uni12 = uni1[0].ToString();
                        string uni13 = uni1[1].ToString();
                        var uni14 = uni1[2];
                        string[] results;
                        string[] results1;
                        string[] results2;
                        string[] results3;
                        string[] results4;
                        string[] results5;
                        string[] results6;
                        string[] results7;
                        results = result[0].Split(',');
                        results1 = result[1].Split(',');
                        results2 = result[2].Split(',');
                        results3 = result[3].Split(',');
                        results4 = result[4].Split(',');
                        results5 = result[5].Split(',');
                        results6 = result[6].Split(',');
                        results7 = result[7].Split(',');
                        string first;
                        string first03;
                        string first1;
                        string first13;
                        string first2;
                        string first23;
                        string first3;
                        string first33;
                        string first4;
                        string first43;
                        string first5;
                        string first53;
                        string first6;
                        string first63;
                        string first7;
                        string first73;
                        first = results[1];
                        first03 = results[2];
                        first1 = results1[1];
                        first13 = results1[2];
                        first2 = results2[1];
                        first23 = results2[2];
                        first3 = results3[1];
                        first33 = results3[2];
                        first4 = results4[1];
                        first43 = results4[2];
                        first5 = results5[1];
                        first53 = results5[2];
                        first6 = results6[1];
                        first63 = results6[2];
                        first7 = results7[1];
                        first73 = results7[2];
                        notePad.WaitForExit();

                        // 1.       Send OTA
                        Helpers.SendSMS sms = new Helpers.SendSMS();
                        //KeyValuePair<int, IEnumerable<CRM_API.Models.OTAString>> listOTA =
                        //    CRM_API.DB.Customer2in1.SProc.List2in1OTAString(orderToActivate.order_id, orderToActivate.crm_login);

                        if (uni1 != Convert.ToString(0))
                        {
                            Log.Debug("--------------------------------------------------------");

                            DB.Common.ErrCodeMsg resCodeMsg = new DB.Common.ErrCodeMsg()
                            {
                                errcode = 0,
                                errsubject = "Activation - Compatible SIM",
                                errmsg = "OK"
                            };


                            // foreach (var item in result)
                            {
                                // Send using the originator sms

                                string[] resSms = sms.SendOTA(orderToActivate.mobileno, first, first03, 12000);
                                System.Threading.Thread.Sleep(iThreadSleep);
                                string[] resSms1 = sms.SendOTA(orderToActivate.mobileno, first1, first13, 12000);
                                System.Threading.Thread.Sleep(iThreadSleep);
                                string[] resSms2 = sms.SendOTA(orderToActivate.mobileno, first2, first23, 12000);
                                System.Threading.Thread.Sleep(iThreadSleep);
                                string[] resSms3 = sms.SendOTA(orderToActivate.mobileno, first3, first33, 12000);
                                System.Threading.Thread.Sleep(iThreadSleep);
                                string[] resSms4 = sms.SendOTA(orderToActivate.mobileno, first4, first43, 12000);
                                System.Threading.Thread.Sleep(iThreadSleep);
                                string[] resSms5 = sms.SendOTA(orderToActivate.mobileno, first5, first53, 12000);
                                System.Threading.Thread.Sleep(iThreadSleep);
                                string[] resSms6 = sms.SendOTA(orderToActivate.mobileno, first6, first63, 12000);
                                System.Threading.Thread.Sleep(iThreadSleep);
                                string[] resSms7 = sms.SendOTA(orderToActivate.mobileno, first7, first73, 12000);

                                //1.A. Log
                                int status = -1;
                                if (resSms.Length > 0)
                                    int.TryParse(resSms[0], out status);

                                if (resCodeMsg.errcode == 0 && status == -1)
                                {
                                    resCodeMsg.errcode = -1;
                                    resCodeMsg.errmsg = "Failed to send OTA string";
                                }
                                if (resCodeMsg.errmsg != "Failed to send OTA string")
                                {
                                    sms.Send(false, orderToActivate.mobileno, senderSMS, activateTextRestart, 5000);
                                    Log.Debug(string.Format("{0} SMS Notification #1 {1} {2}", orderToActivate.mobileno, senderSMS, activateTextRestart));
                                    // Send using the MMI sms
                                    sms.Send(true, orderToActivate.mobileno, senderSMS, activateTextRestart, 5000);
                                    Log.Debug(string.Format("{0} SMS Notification #2 {1} {2}", orderToActivate.mobileno, senderSMS, activateTextRestart));
                                    var Currency3 = CRM_API.DB.Customer2in1.SProc.Get2in1Currency3(orderToActivate.mobileno, Currency1.OTA_Customer_Id);
                                }
                                Log.Debug("--------------------------------------------------------");
                                return Request.CreateResponse(HttpStatusCode.OK, resCodeMsg);
                            }
                            return Request.CreateResponse(HttpStatusCode.OK, resCodeMsg);
                        }
                    }
                    else if (Currency1.errcode == -1)
                    {
                        //31-Aug-2015: Moorthy : Added to display the 'Already 2 in 1 Activated' error message
                        return Request.CreateResponse(HttpStatusCode.OK, new DB.Common.ErrCodeMsg() { errcode = -1, errmsg = Currency1.errMsg });
                    }
                    else
                    {
                        sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd1));
                        sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd2));
                        sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd3));
                        //sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd5));
                        byte[] temp = System.Text.Encoding.UTF8.GetBytes(sb.ToString());
                        System.IO.File.WriteAllBytes(filePath, temp);
                        Process notePad = new Process();
                        //   notePad.StartInfo.FileName = @"C:\Users\user\Desktop\CRM\CRM-API\App_Data\OTA.exe";
                        notePad.StartInfo.FileName = HostingEnvironment.MapPath("~/App_Data/" + "/" + "OTA.exe");
                        notePad.StartInfo.WorkingDirectory = HostingEnvironment.MapPath("~/App_Data/");
                        notePad.StartInfo.Arguments = OTA + " " + file;
                        notePad.StartInfo.RedirectStandardOutput = true;
                        notePad.StartInfo.UseShellExecute = false;
                        notePad.Start();
                        StreamReader s = notePad.StandardOutput;
                        String output1 = s.ReadToEnd();
                        string test = output1;
                        string[] result = test.Replace("\r\n", "\n").Split("\n".ToCharArray());
                        string uni1 = result[0].ToString();
                        string uni2 = result[1].ToString();
                        string uni3 = result[2].ToString();
                        string uni4 = result[3].ToString();
                        string[] uni11 = uni1.Split(',');
                        string uni12 = uni1[0].ToString();
                        string uni13 = uni1[1].ToString();
                        var uni14 = uni1[2];
                        string[] results;
                        string[] results1;
                        string[] results2;
                        string[] results3;
                        string[] results4;
                        results = result[0].Split(',');
                        results1 = result[1].Split(',');
                        results2 = result[2].Split(',');
                        // results3 = result[3].Split(',');
                        // results4 = result[4].Split(',');
                        string first;
                        string first03;
                        string first1;
                        string first13;
                        string first2;
                        string first23;
                        string first3;
                        string first33;
                        string first4;
                        string first43;
                        first = results[1];
                        first03 = results[2];
                        first1 = results1[1];
                        first13 = results1[2];
                        first2 = results2[1];
                        first23 = results2[2];
                        //first3 = results3[1];
                        //first33 = results3[2];
                        //first4 = results4[1];
                        //first43 = results4[2];
                        notePad.WaitForExit();

                        // 1.       Send OTA
                        Helpers.SendSMS sms = new Helpers.SendSMS();
                        //KeyValuePair<int, IEnumerable<CRM_API.Models.OTAString>> listOTA =
                        //    CRM_API.DB.Customer2in1.SProc.List2in1OTAString(orderToActivate.order_id, orderToActivate.crm_login);

                        if (uni1 != Convert.ToString(0))
                        {
                            Log.Debug("--------------------------------------------------------");

                            DB.Common.ErrCodeMsg resCodeMsg = new DB.Common.ErrCodeMsg()
                            {
                                errcode = 0,
                                errsubject = "Activation - Compatible SIM",
                                errmsg = "OK"
                            };
                            // foreach (var item in result)
                            {
                                // Send using the originator sms

                                string[] resSms = sms.SendOTA(orderToActivate.mobileno, first1, first03, 12000);
                                System.Threading.Thread.Sleep(iThreadSleep);
                                string[] resSms1 = sms.SendOTA(orderToActivate.mobileno, first1, first13, 12000);
                                System.Threading.Thread.Sleep(iThreadSleep);
                                string[] resSms2 = sms.SendOTA(orderToActivate.mobileno, first2, first23, 12000);
                                //string[] resSms3 = sms.SendOTA(orderToActivate.mobileno, first3, first33, 12000);
                                //string[] resSms4 = sms.SendOTA(orderToActivate.mobileno, first4, first43, 12000);

                                //1.A. Log
                                int status = -1;
                                if (resSms.Length > 0)
                                    int.TryParse(resSms[0], out status);

                                if (resCodeMsg.errcode == 0 && status == -1)
                                {
                                    resCodeMsg.errcode = -1;
                                    resCodeMsg.errmsg = "Failed to send OTA string";
                                }
                                if (resCodeMsg.errmsg != "Failed to send OTA string")
                                {
                                    sms.Send(false, orderToActivate.mobileno, senderSMS, activateTextRestart, 5000);
                                    Log.Debug(string.Format("{0} SMS Notification #1 {1} {2}", orderToActivate.mobileno, senderSMS, activateTextRestart));
                                    // Send using the MMI sms
                                    sms.Send(true, orderToActivate.mobileno, senderSMS, activateTextRestart, 5000);
                                    Log.Debug(string.Format("{0} SMS Notification #2 {1} {2}", orderToActivate.mobileno, senderSMS, activateTextRestart));
                                    var Currency3 = CRM_API.DB.Customer2in1.SProc.Get2in1Currency3(orderToActivate.mobileno, Currency1.OTA_Customer_Id);
                                }
                                Log.Debug("--------------------------------------------------------");
                                return Request.CreateResponse(HttpStatusCode.OK, resCodeMsg);
                            }
                            return Request.CreateResponse(HttpStatusCode.OK, resCodeMsg);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, new DB.Common.ErrCodeMsg() { errcode = -1, errmsg = "Empty OTA " });
                }

                return Request.CreateResponse(HttpStatusCode.OK, new DB.Common.ErrCodeMsg() { errcode = -1, errmsg = "Empty OTA " });
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                Log.Error(ex.InnerException);
                Log.Error(ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.OK, new DB.Common.ErrCodeMsg() { errcode = -1, errmsg = ex.Message });
            }
        }
        #region Commented
        //HttpResponseMessage ActivateOTA(CRM_API.Models.Customer2in1Records orderToActivate)
        //{
        //    try
        //    {
        //        Log.Debug("ActivateOTA Start");
        //        string filename = string.Empty;
        //        //path create 
        //        string file = "Country.csv";
        //        string filePath = HostingEnvironment.MapPath("~/App_Data/" + "/" + file);
        //        //check exists file
        //        try
        //        {
        //            Log.Debug("file exist or not checking");
        //            if (System.IO.File.Exists(filePath))
        //            {
        //                System.GC.Collect();
        //                System.GC.WaitForPendingFinalizers();
        //                System.IO.File.Delete(filePath);
        //            }
        //        }
        //        catch { }

        //        // else if (!System.IO.File.Exists(file))
        //        {

        //            //System.IO.FileStream f = System.IO.File.Create(file);
        //            //f.Close();
        //            StringBuilder sb = new StringBuilder();
        //            Log.Debug("currency2 result started");
        //            var Currency2 = CRM_API.DB.Customer2in1.SProc.Get2in1Currency2(orderToActivate.mobileno, orderToActivate.imsi_country);
        //            Log.Debug("currency2 result ended");

        //            Log.Debug("currency1 result started");
        //            var Currency1 = CRM_API.DB.Customer2in1.SProc.Get2in1Currency1(orderToActivate.mobileno, orderToActivate.imsi_country);
        //            Log.Debug("currency1 result ended");



        //            string OTA = Convert.ToString(Currency1.SPI) + " " + Convert.ToString(Currency1.KIC) + " " + Convert.ToString(Currency1.KID) + " " + Convert.ToString(Currency1.TAR) + " " + Convert.ToString(Currency1.Counter);

        //            if (Currency1.errcode == 0)
        //            {
        //                //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F80A0A40000026F00A0DC030412034672616E6365FFFFFFFFFFFFFFFFFFFFFFFFA0A40000026F02A0DC02040302F803");
        //                //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F80A0A40000026F07A0DC010409082940809021000000A0A40000026F07A0DC020409082943108901000000A0A40000026F78A0DC0104020001A0A40000");
        //                //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F80A0A40000026F42A0DC010429534D532043656E7472652031FDFFFFFFFFFFFFFFFFFFFFFFFF07911356039900F0FFFFFFFFFFFFFF");
        //                //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F20A0A40000026F07A0DC020409082943108901000000A0A40000026F78A0DC0104020001");
        //                //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F80A0A40000026F42A0DC010429534D532043656E7472652031FDFFFFFFFFFFFFFFFFFFFFFFFF07913357999999F0FFFFFFFFFFFFFF");
        //                //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F80A0A40000026F07A0DC030409082980138901000000A0A40000026F78A0DC0304020001");
        //                //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F80A0A40000026F42A0DC030429534D532043656E7472652031FDFFFFFFFFFFFFFFFFFFFFFFFF0791448729199965FFFFFFFFFFFFFF");
        //                //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F80A0A40000026F42A0DC030429534D532043656E7472652031FDFFFFFFFFFFFFFFFFFFFFFFFF0791448729199965FFFFFFFFFFFFFF");
        //                sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd1));
        //                sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd2));
        //                sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd3));
        //                sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd4));
        //                sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd5));
        //                sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd6));
        //                sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd7));
        //                sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd8));
        //                byte[] temp = System.Text.Encoding.UTF8.GetBytes(sb.ToString());

        //                System.IO.File.WriteAllBytes(filePath, temp);

        //                Log.Debug("before notepad");

        //                Process notePad = new Process();
        //                //   notePad.StartInfo.FileName = @"C:\Users\user\Desktop\CRM\CRM-API\App_Data\OTA.exe";
        //                notePad.StartInfo.FileName = HostingEnvironment.MapPath("~/App_Data/" + "/" + "OTA.exe");

        //                Log.Debug("after exe");

        //                notePad.StartInfo.WorkingDirectory = HostingEnvironment.MapPath("~/App_Data/");
        //                //notePad.StartInfo.Arguments = OTA + " " + file;

        //                notePad.StartInfo.Arguments = file;      
        //                notePad.StartInfo.UseShellExecute = false;
        //                notePad.StartInfo.RedirectStandardOutput = true;
        //                notePad.Start();


        //                StreamReader s = notePad.StandardOutput;
        //                String output1 = s.ReadToEnd();

        //                Log.Debug("before output");

        //                string test = output1;
        //                string[] result = test.Replace("\r\n", "\n").Split("\n".ToCharArray());
        //                string uni1 = result[0].ToString();
        //                string uni2 = result[1].ToString();
        //                string uni3 = result[2].ToString();
        //                string uni4 = result[3].ToString();
        //                string[] uni11 = uni1.Split(',');
        //                string uni12 = uni1[0].ToString();
        //                string uni13 = uni1[1].ToString();
        //                var uni14 = uni1[2];
        //                string[] results;
        //                string[] results1;
        //                string[] results2;
        //                string[] results3;
        //                string[] results4;
        //                string[] results5;
        //                string[] results6;
        //                string[] results7;
        //                results = result[0].Split(',');
        //                results1 = result[1].Split(',');
        //                results2 = result[2].Split(',');
        //                results3 = result[3].Split(',');
        //                results4 = result[4].Split(',');
        //                results5 = result[5].Split(',');
        //                results6 = result[6].Split(',');
        //                results7 = result[7].Split(',');
        //                string first;
        //                string first03;
        //                string first1;
        //                string first13;
        //                string first2;
        //                string first23;
        //                string first3;
        //                string first33;
        //                string first4;
        //                string first43;
        //                string first5;
        //                string first53;
        //                string first6;
        //                string first63;
        //                string first7;
        //                string first73;
        //                //first = results[1];
        //                //first03 = results[2];
        //                //first1 = results1[1];
        //                //first13 = results1[2];
        //                //first2 = results2[1];
        //                //first23 = results2[2];
        //                //first3 = results3[1];
        //                //first33 = results3[2];
        //                //first4 = results4[1];
        //                //first43 = results4[2];
        //                //first5 = results5[1];
        //                //first53 = results5[2];
        //                //first6 = results6[1];
        //                //first63 = results6[2];
        //                //first7 = results7[1];
        //                //first73 = results7[2];

        //                first = results[0];
        //                first03 = results[0];
        //                first1 = results1[0];
        //                first13 = results1[0];
        //                first2 = results2[0];
        //                first23 = results2[0];
        //                first3 = results3[0];
        //                first33 = results3[0];
        //                first4 = results4[0];
        //                first43 = results4[0];
        //                first5 = results5[0];
        //                first53 = results5[0];
        //                first6 = results6[0];
        //                first63 = results6[0];
        //                first7 = results7[0];
        //                first73 = results7[0];
        //                notePad.WaitForExit();

        //                // 1.       Send OTA
        //                Helpers.SendSMS sms = new Helpers.SendSMS();
        //                //KeyValuePair<int, IEnumerable<CRM_API.Models.OTAString>> listOTA =
        //                //    CRM_API.DB.Customer2in1.SProc.List2in1OTAString(orderToActivate.order_id, orderToActivate.crm_login);

        //                if (uni1 != Convert.ToString(0))
        //                {
        //                    Log.Debug("--------------------------------------------------------");

        //                    DB.Common.ErrCodeMsg resCodeMsg = new DB.Common.ErrCodeMsg()
        //                    {
        //                        errcode = 0,
        //                        errsubject = "Activation - Compatible SIM",
        //                        errmsg = "OK"
        //                    };


        //                    // foreach (var item in result)
        //                    {
        //                        // Send using the originator sms

        //                        Log.Debug("start 0");


        //                       string[] resSms = sms.SendOTA(orderToActivate.mobileno, first, first03, 12000);                               
        //                        Log.Debug("end of first first 0");


        //                        string[] resSms1 = sms.SendOTA(orderToActivate.mobileno, first1, first13, 12000);

        //                        Log.Debug("end of first 1 first13");


        //                        string[] resSms2 = sms.SendOTA(orderToActivate.mobileno, first2, first23, 12000);

        //                        Log.Debug("end of first 2 first23");


        //                        string[] resSms3 = sms.SendOTA(orderToActivate.mobileno, first3, first33, 12000);

        //                        Log.Debug("end of first 3 first33");

        //                        string[] resSms4 = sms.SendOTA(orderToActivate.mobileno, first4, first43, 12000);

        //                        Log.Debug("end of first 4 first43");


        //                        string[] resSms5 = sms.SendOTA(orderToActivate.mobileno, first5, first53, 12000);

        //                        Log.Debug("end of first 5 first53");

        //                        string[] resSms6 = sms.SendOTA(orderToActivate.mobileno, first6, first63, 12000);

        //                        Log.Debug("end of first 6 first63");

        //                        string[] resSms7 = sms.SendOTA(orderToActivate.mobileno, first7, first73, 12000);

        //                        Log.Debug("end of first 7 first73");

        //                        //1.A. Log
        //                        int status = -1;
        //                        if (resSms.Length > 0)
        //                            int.TryParse(resSms[0], out status);





        //                        if (resCodeMsg.errcode == 0 && status == -1)
        //                        {
        //                            resCodeMsg.errcode = -1;
        //                            resCodeMsg.errmsg = "Failed to send OTA string";
        //                        }
        //                        if (resCodeMsg.errmsg != "Failed to send OTA string")
        //                        {

        //                            Log.Debug("sms is going to start...");

        //                            sms.Send(false, orderToActivate.mobileno, senderSMS, activateTextRestart, 5000);


        //                            Log.Debug("sms is sending...");


        //                            Log.Debug(string.Format("{0} SMS Notification #1 {1} {2}", orderToActivate.mobileno, senderSMS, activateTextRestart));
        //                            // Send using the MMI sms

        //                            Log.Debug("using the mmi-sms starting");


        //                            sms.Send(true, orderToActivate.mobileno, senderSMS, activateTextRestart, 5000);


        //                            Log.Debug("using the mmi-sms ending...");

        //                            Log.Debug(string.Format("{0} SMS Notification #2 {1} {2}", orderToActivate.mobileno, senderSMS, activateTextRestart));
        //                            var Currency3 = CRM_API.DB.Customer2in1.SProc.Get2in1Currency3(orderToActivate.mobileno, Currency1.OTA_Customer_Id);

        //                            Log.Debug("currency3Result");
        //                        }
        //                        Log.Debug("--------------------------------------------------------");
        //                        return Request.CreateResponse(HttpStatusCode.OK, resCodeMsg);
        //                    }
        //                    return Request.CreateResponse(HttpStatusCode.OK, resCodeMsg);
        //                }
        //            }
        //            else if (Currency1.errcode == -1)
        //            {
        //                //31-Aug-2015: Moorthy : Added to display the 'Already 2 in 1 Activated' error message                       
        //                Log.Debug("Already 2 in 1 Activated error message");

        //                return Request.CreateResponse(HttpStatusCode.OK, new DB.Common.ErrCodeMsg() { errcode = -1, errmsg = Currency1.errMsg });
        //            }
        //            else
        //            {                       
        //                Log.Debug("else part started");

        //                sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd1));
        //                sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd2));
        //                sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd3));
        //                //sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd5));
        //                byte[] temp = System.Text.Encoding.UTF8.GetBytes(sb.ToString());
        //                System.IO.File.WriteAllBytes(filePath, temp);


        //                Log.Debug("else part process started");

        //                Process notePad = new Process();
        //                //   notePad.StartInfo.FileName = @"C:\Users\user\Desktop\CRM\CRM-API\App_Data\OTA.exe";
        //                notePad.StartInfo.FileName = HostingEnvironment.MapPath("~/App_Data/" + "/" + "OTA.exe");
        //                notePad.StartInfo.WorkingDirectory = HostingEnvironment.MapPath("~/App_Data/");
        //                notePad.StartInfo.Arguments = file;// OTA + " " + file;
        //                notePad.StartInfo.RedirectStandardOutput = true;
        //                notePad.StartInfo.UseShellExecute = false;
        //                notePad.Start();

        //                StreamReader s = notePad.StandardOutput;
        //                String output1 = s.ReadToEnd();
        //                string test = output1;
        //                string[] result = test.Replace("\r\n", "\n").Split("\n".ToCharArray());
        //                string uni1 = result[0].ToString();
        //                string uni2 = result[1].ToString();
        //                string uni3 = result[2].ToString();
        //                string uni4 = result[3].ToString();
        //                string[] uni11 = uni1.Split(',');
        //                string uni12 = uni1[0].ToString();
        //                string uni13 = uni1[1].ToString();
        //                var uni14 = uni1[2];
        //                string[] results;
        //                string[] results1;
        //                string[] results2;
        //                string[] results3;
        //                string[] results4;
        //                results = result[0].Split(',');
        //                results1 = result[1].Split(',');
        //                results2 = result[2].Split(',');
        //                // results3 = result[3].Split(',');
        //                // results4 = result[4].Split(',');
        //                string first;
        //                string first03;
        //                string first1;
        //                string first13;
        //                string first2;
        //                string first23;
        //                string first3;
        //                string first33;
        //                string first4;
        //                string first43;


        //                first = results[0];
        //                first03 = results[0];
        //                first1 = results1[0];
        //                first13 = results1[0];
        //                first2 = results2[0];
        //                first23 = results2[0];


        //                //first = results[1];
        //                //first03 = results[2];
        //                //first1 = results1[1];
        //                //first13 = results1[2];
        //                //first2 = results2[1];
        //                //first23 = results2[2];


        //                //first3 = results3[1];
        //                //first33 = results3[2];
        //                //first4 = results4[1];
        //                //first43 = results4[2];
        //                notePad.WaitForExit();

        //                // 1.       Send OTA
        //                Helpers.SendSMS sms = new Helpers.SendSMS();
        //                //KeyValuePair<int, IEnumerable<CRM_API.Models.OTAString>> listOTA =
        //                //    CRM_API.DB.Customer2in1.SProc.List2in1OTAString(orderToActivate.order_id, orderToActivate.crm_login);

        //                if (uni1 != Convert.ToString(0))
        //                {
        //                    Log.Debug("--------------------------------------------------------");

        //                    DB.Common.ErrCodeMsg resCodeMsg = new DB.Common.ErrCodeMsg()
        //                    {
        //                        errcode = 0,
        //                        errsubject = "Activation - Compatible SIM",
        //                        errmsg = "OK"
        //                    };
        //                    // foreach (var item in result)
        //                    {
        //                        // Send using the originator sms

        //                        string[] resSms = sms.SendOTA(orderToActivate.mobileno, first1, first03, 12000);

        //                        Log.Debug("end of first1 first 03");

        //                        string[] resSms1 = sms.SendOTA(orderToActivate.mobileno, first1, first13, 12000);

        //                        Log.Debug("end of first1 first 13");

        //                        string[] resSms2 = sms.SendOTA(orderToActivate.mobileno, first2, first23, 12000);

        //                        Log.Debug("end of first2 first 23");

        //                        //string[] resSms3 = sms.SendOTA(orderToActivate.mobileno, first3, first33, 12000);
        //                        //string[] resSms4 = sms.SendOTA(orderToActivate.mobileno, first4, first43, 12000);

        //                        //1.A. Log
        //                        int status = -1;
        //                        if (resSms.Length > 0)
        //                            int.TryParse(resSms[0], out status);

        //                        if (resCodeMsg.errcode == 0 && status == -1)
        //                        {
        //                            resCodeMsg.errcode = -1;
        //                            resCodeMsg.errmsg = "Failed to send OTA string";

        //                            Log.Debug("failed to send ota string");
        //                        }
        //                        if (resCodeMsg.errmsg != "Failed to send OTA string")
        //                        {

        //                            sms.Send(false, orderToActivate.mobileno, senderSMS, activateTextRestart, 5000);
        //                            Log.Debug(string.Format("{0} SMS Notification #1 {1} {2}", orderToActivate.mobileno, senderSMS, activateTextRestart));
        //                            // Send using the MMI sms
        //                            sms.Send(true, orderToActivate.mobileno, senderSMS, activateTextRestart, 5000);
        //                            Log.Debug(string.Format("{0} SMS Notification #2 {1} {2}", orderToActivate.mobileno, senderSMS, activateTextRestart));
        //                            var Currency3 = CRM_API.DB.Customer2in1.SProc.Get2in1Currency3(orderToActivate.mobileno, Currency1.OTA_Customer_Id);
        //                            Log.Debug("result of currency3");
        //                        }
        //                        Log.Debug("--------------------------------------------------------");
        //                        return Request.CreateResponse(HttpStatusCode.OK, resCodeMsg);
        //                    }
        //                    return Request.CreateResponse(HttpStatusCode.OK, resCodeMsg);
        //                }
        //            }
        //            return Request.CreateResponse(HttpStatusCode.OK, new DB.Common.ErrCodeMsg() { errcode = -1, errmsg = "Empty OTA " });
        //        }

        //        return Request.CreateResponse(HttpStatusCode.OK, new DB.Common.ErrCodeMsg() { errcode = -1, errmsg = "Empty OTA " });
        //    }
        //    catch (Exception ex)
        //    {
        //        return Request.CreateResponse(HttpStatusCode.OK, new DB.Common.ErrCodeMsg() { errcode = -1, errmsg = ex.StackTrace });
        //    }
        //} 
        #endregion

        HttpResponseMessage ActivateOTAVectone2in1(CRM_API.Models.Customer2in1Records orderToActivate)
        {

            string filename = string.Empty;
            //path create 
            string filePath = HostingEnvironment.MapPath("~/App_Data/" + "/" + "Country.csv");
            string file = filePath;
            //check exists file
            if (System.IO.File.Exists(file))
            {
                System.GC.Collect();
                System.GC.WaitForPendingFinalizers();
                System.IO.File.Delete(file);
            }

            // else if (!System.IO.File.Exists(file))
            {

                System.IO.FileStream f = System.IO.File.Create(file);
                f.Close();
                StringBuilder sb = new StringBuilder();
                var Currency2 = CRM_API.DB.Customer2in1.SProc.Get2in1Currency2(orderToActivate.mobileno, orderToActivate.imsi_country);
                var Currency1 = CRM_API.DB.Customer2in1.SProc.Get2in1Currency1(orderToActivate.mobileno, orderToActivate.imsi_country);



                string OTA = Convert.ToString(Currency1.SPI) + " " + Convert.ToString(Currency1.KIC) + " " + Convert.ToString(Currency1.KID) + " " + Convert.ToString(Currency1.TAR) + " " + Convert.ToString(Currency1.Counter);
                if (Currency2.errcode == 0)
                {
                    //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F80A0A40000026F00A0DC030412034672616E6365FFFFFFFFFFFFFFFFFFFFFFFFA0A40000026F02A0DC02040302F803");
                    //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F80A0A40000026F07A0DC010409082940809021000000A0A40000026F07A0DC020409082943108901000000A0A40000026F78A0DC0104020001A0A40000");
                    //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F80A0A40000026F42A0DC010429534D532043656E7472652031FDFFFFFFFFFFFFFFFFFFFFFFFF07911356039900F0FFFFFFFFFFFFFF");
                    //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F20A0A40000026F07A0DC020409082943108901000000A0A40000026F78A0DC0104020001");
                    //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F80A0A40000026F42A0DC010429534D532043656E7472652031FDFFFFFFFFFFFFFFFFFFFFFFFF07913357999999F0FFFFFFFFFFFFFF");
                    //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F80A0A40000026F07A0DC030409082980138901000000A0A40000026F78A0DC0304020001");
                    //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F80A0A40000026F42A0DC030429534D532043656E7472652031FDFFFFFFFFFFFFFFFFFFFFFFFF0791448729199965FFFFFFFFFFFFFF");
                    //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F80A0A40000026F42A0DC030429534D532043656E7472652031FDFFFFFFFFFFFFFFFFFFFFFFFF0791448729199965FFFFFFFFFFFFFF");
                    sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd1));
                    sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd2));
                    sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd3));
                    sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd4));
                    sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd5));
                    sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd6));
                    sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd7));
                    sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd8));
                    byte[] temp = System.Text.Encoding.UTF8.GetBytes(sb.ToString());
                    System.IO.File.WriteAllBytes(filePath, temp);
                    Process notePad = new Process();
                    //   notePad.StartInfo.FileName = @"C:\Users\user\Desktop\CRM\CRM-API\App_Data\OTA.exe";
                    notePad.StartInfo.FileName = HostingEnvironment.MapPath("~/App_Data/" + "/" + "OTA.exe");
                    notePad.StartInfo.Arguments = OTA + " " + file;
                    notePad.StartInfo.RedirectStandardOutput = true;
                    notePad.StartInfo.UseShellExecute = false;
                    notePad.Start();
                    StreamReader s = notePad.StandardOutput;
                    String output1 = s.ReadToEnd();
                    string test = output1;
                    string[] result = test.Replace("\r\n", "\n").Split("\n".ToCharArray());
                    string uni1 = result[0].ToString();
                    string uni2 = result[1].ToString();
                    string uni3 = result[2].ToString();
                    string uni4 = result[3].ToString();
                    string[] uni11 = uni1.Split(',');
                    string uni12 = uni1[0].ToString();
                    string uni13 = uni1[1].ToString();
                    var uni14 = uni1[2];
                    string[] results;
                    string[] results1;
                    string[] results2;
                    string[] results3;
                    string[] results4;
                    string[] results5;
                    string[] results6;
                    string[] results7;
                    results = result[0].Split(',');
                    results1 = result[1].Split(',');
                    results2 = result[2].Split(',');
                    results3 = result[3].Split(',');
                    results4 = result[4].Split(',');
                    results5 = result[5].Split(',');
                    results6 = result[6].Split(',');
                    results7 = result[7].Split(',');
                    string first;
                    string first03;
                    string first1;
                    string first13;
                    string first2;
                    string first23;
                    string first3;
                    string first33;
                    string first4;
                    string first43;
                    string first5;
                    string first53;
                    string first6;
                    string first63;
                    string first7;
                    string first73;
                    first = results[1];
                    first03 = results[2];
                    first1 = results1[1];
                    first13 = results1[2];
                    first2 = results2[1];
                    first23 = results2[2];
                    first3 = results3[1];
                    first33 = results3[2];
                    first4 = results4[1];
                    first43 = results4[2];
                    first5 = results5[1];
                    first53 = results5[2];
                    first6 = results6[1];
                    first63 = results6[2];
                    first7 = results7[1];
                    first73 = results7[2];
                    notePad.WaitForExit();

                    // 1.       Send OTA
                    Helpers.SendSMS sms = new Helpers.SendSMS();
                    //KeyValuePair<int, IEnumerable<CRM_API.Models.OTAString>> listOTA =
                    //    CRM_API.DB.Customer2in1.SProc.List2in1OTAString(orderToActivate.order_id, orderToActivate.crm_login);

                    if (uni1 != Convert.ToString(0))
                    {
                        Log.Debug("--------------------------------------------------------");

                        DB.Common.ErrCodeMsg resCodeMsg = new DB.Common.ErrCodeMsg()
                        {
                            errcode = 0,
                            errsubject = "Activation - Compatible SIM",
                            errmsg = "OK"
                        };


                        // foreach (var item in result)
                        {
                            // Send using the originator sms

                            string[] resSms = sms.SendOTA(orderToActivate.mobileno, first, first03, 12000);
                            string[] resSms1 = sms.SendOTA(orderToActivate.mobileno, first1, first13, 12000);
                            string[] resSms2 = sms.SendOTA(orderToActivate.mobileno, first2, first23, 12000);
                            string[] resSms3 = sms.SendOTA(orderToActivate.mobileno, first3, first33, 12000);
                            string[] resSms4 = sms.SendOTA(orderToActivate.mobileno, first4, first43, 12000);
                            string[] resSms5 = sms.SendOTA(orderToActivate.mobileno, first5, first53, 12000);
                            string[] resSms6 = sms.SendOTA(orderToActivate.mobileno, first6, first63, 12000);
                            string[] resSms7 = sms.SendOTA(orderToActivate.mobileno, first7, first73, 12000);

                            //1.A. Log
                            int status = -1;
                            if (resSms.Length > 0)
                                int.TryParse(resSms[0], out status);

                            if (resCodeMsg.errcode == 0 && status == -1)
                            {
                                resCodeMsg.errcode = -1;
                                resCodeMsg.errmsg = "Failed to send OTA string";
                            }
                            if (resCodeMsg.errmsg != "Failed to send OTA string")
                            {
                                sms.Send(false, orderToActivate.mobileno, senderSMS, activateTextRestart, 5000);
                                Log.Debug(string.Format("{0} SMS Notification #1 {1} {2}", orderToActivate.mobileno, senderSMS, activateTextRestart));
                                // Send using the MMI sms
                                sms.Send(true, orderToActivate.mobileno, senderSMS, activateTextRestart, 5000);
                                Log.Debug(string.Format("{0} SMS Notification #2 {1} {2}", orderToActivate.mobileno, senderSMS, activateTextRestart));
                                var Currency3 = CRM_API.DB.Customer2in1.SProc.Get2in1Currency3(orderToActivate.mobileno, Currency1.OTA_Customer_Id);
                            }
                            Log.Debug("--------------------------------------------------------");
                            return Request.CreateResponse(HttpStatusCode.OK, resCodeMsg);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, resCodeMsg);
                    }
                }
                else
                {
                    sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd1));
                    sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd2));
                    sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd3));
                    //sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd5));
                    byte[] temp = System.Text.Encoding.UTF8.GetBytes(sb.ToString());
                    System.IO.File.WriteAllBytes(filePath, temp);
                    Process notePad = new Process();
                    //   notePad.StartInfo.FileName = @"C:\Users\user\Desktop\CRM\CRM-API\App_Data\OTA.exe";
                    notePad.StartInfo.FileName = HostingEnvironment.MapPath("~/App_Data/" + "/" + "OTA.exe");
                    notePad.StartInfo.Arguments = OTA + " " + file;
                    notePad.StartInfo.RedirectStandardOutput = true;
                    notePad.StartInfo.UseShellExecute = false;
                    notePad.Start();
                    StreamReader s = notePad.StandardOutput;
                    String output1 = s.ReadToEnd();
                    string test = output1;
                    string[] result = test.Replace("\r\n", "\n").Split("\n".ToCharArray());
                    string uni1 = result[0].ToString();
                    string uni2 = result[1].ToString();
                    string uni3 = result[2].ToString();
                    string uni4 = result[3].ToString();
                    string[] uni11 = uni1.Split(',');
                    string uni12 = uni1[0].ToString();
                    string uni13 = uni1[1].ToString();
                    var uni14 = uni1[2];
                    string[] results;
                    string[] results1;
                    string[] results2;
                    string[] results3;
                    string[] results4;
                    results = result[0].Split(',');
                    results1 = result[1].Split(',');
                    results2 = result[2].Split(',');
                    // results3 = result[3].Split(',');
                    // results4 = result[4].Split(',');
                    string first;
                    string first03;
                    string first1;
                    string first13;
                    string first2;
                    string first23;
                    string first3;
                    string first33;
                    string first4;
                    string first43;
                    first = results[1];
                    first03 = results[2];
                    first1 = results1[1];
                    first13 = results1[2];
                    first2 = results2[1];
                    first23 = results2[2];
                    //first3 = results3[1];
                    //first33 = results3[2];
                    //first4 = results4[1];
                    //first43 = results4[2];
                    notePad.WaitForExit();

                    // 1.       Send OTA
                    Helpers.SendSMS sms = new Helpers.SendSMS();
                    //KeyValuePair<int, IEnumerable<CRM_API.Models.OTAString>> listOTA =
                    //    CRM_API.DB.Customer2in1.SProc.List2in1OTAString(orderToActivate.order_id, orderToActivate.crm_login);

                    if (uni1 != Convert.ToString(0))
                    {
                        Log.Debug("--------------------------------------------------------");

                        DB.Common.ErrCodeMsg resCodeMsg = new DB.Common.ErrCodeMsg()
                        {
                            errcode = 0,
                            errsubject = "Activation - Compatible SIM",
                            errmsg = "OK"
                        };
                        // foreach (var item in result)
                        {
                            // Send using the originator sms

                            string[] resSms = sms.SendOTA(orderToActivate.mobileno, first1, first03, 12000);
                            string[] resSms1 = sms.SendOTA(orderToActivate.mobileno, first1, first13, 12000);
                            string[] resSms2 = sms.SendOTA(orderToActivate.mobileno, first2, first23, 12000);
                            //string[] resSms3 = sms.SendOTA(orderToActivate.mobileno, first3, first33, 12000);
                            //string[] resSms4 = sms.SendOTA(orderToActivate.mobileno, first4, first43, 12000);

                            //1.A. Log
                            int status = -1;
                            if (resSms.Length > 0)
                                int.TryParse(resSms[0], out status);

                            if (resCodeMsg.errcode == 0 && status == -1)
                            {
                                resCodeMsg.errcode = -1;
                                resCodeMsg.errmsg = "Failed to send OTA string";
                            }
                            if (resCodeMsg.errmsg != "Failed to send OTA string")
                            {
                                sms.Send(false, orderToActivate.mobileno, senderSMS, activateTextRestart, 5000);
                                Log.Debug(string.Format("{0} SMS Notification #1 {1} {2}", orderToActivate.mobileno, senderSMS, activateTextRestart));
                                // Send using the MMI sms
                                sms.Send(true, orderToActivate.mobileno, senderSMS, activateTextRestart, 5000);
                                Log.Debug(string.Format("{0} SMS Notification #2 {1} {2}", orderToActivate.mobileno, senderSMS, activateTextRestart));
                                var Currency3 = CRM_API.DB.Customer2in1.SProc.Get2in1Currency3(orderToActivate.mobileno, Currency1.OTA_Customer_Id);
                            }
                            Log.Debug("--------------------------------------------------------");
                            return Request.CreateResponse(HttpStatusCode.OK, resCodeMsg);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, resCodeMsg);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, new DB.Common.ErrCodeMsg() { errcode = -1, errmsg = "Empty OTA " });
            }

            return Request.CreateResponse(HttpStatusCode.OK, new DB.Common.ErrCodeMsg() { errcode = -1, errmsg = "Empty OTA " });
        }
        HttpResponseMessage ActivateOTAFree(CRM_API.Models.Customer2in1Records orderToActivate)
        {

            CRM_API.Models.CustomerSearchResult resHistoryValues1 = CRM_API.DB.Customer.SProc.CustomerDetailInfo1(orderToActivate.mobileno);
            string productcode = resHistoryValues1.product;
            var Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(orderToActivate.mobileno, productcode);
            // CRM_API.Models.CustomerSearchResult resHistoryValues = CRM_API.DB.Customer.SProc.CustomerDetailInfo(productcode, orderToActivate.mobileno);
            //resHistoryValues.mimsi_compatible.ToString()==
            if (Customer.mimsi_compatible == 1)
            {
                string filename = string.Empty;

                //path create 
                string filePath = HostingEnvironment.MapPath("~/App_Data/" + "/" + "Country.csv");
                string file = filePath;
                //check exists file
                if (System.IO.File.Exists(file))
                {
                    System.GC.Collect();
                    System.GC.WaitForPendingFinalizers();
                    System.IO.File.Delete(file);
                }

                // else if (!System.IO.File.Exists(file))
                {

                    System.IO.FileStream f = System.IO.File.Create(file);
                    f.Close();
                    StringBuilder sb = new StringBuilder();
                    var Currency2 = 0;
                    var Currency1 = CRM_API.DB.Customer2in1.SProc.Get2in1CurrencySMS(orderToActivate.mobileno, orderToActivate.imsi_country);



                    string OTA = Convert.ToString(Currency1.SPI) + " " + Convert.ToString(Currency1.KIC) + " " + Convert.ToString(Currency1.KID) + " " + Convert.ToString(Currency1.TAR) + " " + Convert.ToString(Currency1.Counter);
                    if (OTA != "    ")
                    {
                        //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F80A0A40000026F00A0DC030412034672616E6365FFFFFFFFFFFFFFFFFFFFFFFFA0A40000026F02A0DC02040302F803");
                        //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F80A0A40000026F07A0DC010409082940809021000000A0A40000026F07A0DC020409082943108901000000A0A40000026F78A0DC0104020001A0A40000");
                        //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F80A0A40000026F42A0DC010429534D532043656E7472652031FDFFFFFFFFFFFFFFFFFFFFFFFF07911356039900F0FFFFFFFFFFFFFF");
                        //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F20A0A40000026F07A0DC020409082943108901000000A0A40000026F78A0DC0104020001");
                        //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F80A0A40000026F42A0DC010429534D532043656E7472652031FDFFFFFFFFFFFFFFFFFFFFFFFF07913357999999F0FFFFFFFFFFFFFF");
                        //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F80A0A40000026F07A0DC030409082980138901000000A0A40000026F78A0DC0304020001");
                        //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F80A0A40000026F42A0DC030429534D532043656E7472652031FDFFFFFFFFFFFFFFFFFFFFFFFF0791448729199965FFFFFFFFFFFFFF");
                        //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F80A0A40000026F42A0DC030429534D532043656E7472652031FDFFFFFFFFFFFFFFFFFFFFFFFF0791448729199965FFFFFFFFFFFFFF");
                        sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd1));
                        sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd2));
                        sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd3));
                        sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd4));
                        sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd5));
                        sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd6));
                        sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd7));
                        sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd8));
                        byte[] temp = System.Text.Encoding.UTF8.GetBytes(sb.ToString());
                        System.IO.File.WriteAllBytes(filePath, temp);
                        Process notePad = new Process();
                        //   notePad.StartInfo.FileName = @"C:\Users\user\Desktop\CRM\CRM-API\App_Data\OTA.exe";
                        notePad.StartInfo.FileName = HostingEnvironment.MapPath("~/App_Data/" + "/" + "OTA.exe");
                        notePad.StartInfo.Arguments = OTA + " " + file;
                        notePad.StartInfo.RedirectStandardOutput = true;
                        notePad.StartInfo.UseShellExecute = false;
                        notePad.Start();
                        StreamReader s = notePad.StandardOutput;
                        Log.Debug("Exe results start");
                        String output1 = s.ReadToEnd();
                        Log.Debug("Exe results", output1);
                        Log.Debug("Exe results end");
                        string test = output1;
                        string[] result = test.Replace("\r\n", "\n").Split("\n".ToCharArray());
                        string uni1 = result[0].ToString();
                        string uni2 = result[1].ToString();
                        string uni3 = result[2].ToString();
                        string uni4 = result[3].ToString();
                        string[] uni11 = uni1.Split(',');
                        string uni12 = uni1[0].ToString();
                        string uni13 = uni1[1].ToString();
                        var uni14 = uni1[2];
                        string[] results;
                        string[] results1;
                        string[] results2;
                        string[] results3;
                        string[] results4;
                        string[] results5;
                        string[] results6;
                        string[] results7;
                        results = result[0].Split(',');
                        results1 = result[1].Split(',');
                        results2 = result[2].Split(',');
                        results3 = result[3].Split(',');
                        results4 = result[4].Split(',');
                        results5 = result[5].Split(',');
                        results6 = result[6].Split(',');
                        results7 = result[7].Split(',');
                        string first;
                        string first03;
                        string first1;
                        string first13;
                        string first2;
                        string first23;
                        string first3;
                        string first33;
                        string first4;
                        string first43;
                        string first5;
                        string first53;
                        string first6;
                        string first63;
                        string first7;
                        string first73;
                        first = results[1];
                        first03 = results[2];
                        first1 = results1[1];
                        first13 = results1[2];
                        first2 = results2[1];
                        first23 = results2[2];
                        first3 = results3[1];
                        first33 = results3[2];
                        first4 = results4[1];
                        first43 = results4[2];
                        first5 = results5[1];
                        first53 = results5[2];
                        first6 = results6[1];
                        first63 = results6[2];
                        first7 = results7[1];
                        first73 = results7[2];
                        notePad.WaitForExit();

                        // 1.       Send OTA
                        Helpers.SendSMS sms = new Helpers.SendSMS();
                        //KeyValuePair<int, IEnumerable<CRM_API.Models.OTAString>> listOTA =
                        //    CRM_API.DB.Customer2in1.SProc.List2in1OTAString(orderToActivate.order_id, orderToActivate.crm_login);

                        if (uni1 != Convert.ToString(0))
                        {
                            Log.Debug("--------------------------------------------------------");

                            DB.Common.ErrCodeMsg resCodeMsg = new DB.Common.ErrCodeMsg()
                            {
                                errcode = 0,
                                errsubject = "Activation - Compatible SIM",
                                errmsg = "OK"
                            };


                            // foreach (var item in result)
                            {
                                // Send using the originator sms

                                string[] resSms = sms.SendOTA(orderToActivate.mobileno, first, first03, 12000 * 4);
                                string[] resSms1 = sms.SendOTA(orderToActivate.mobileno, first1, first13, 12000 * 4);
                                string[] resSms2 = sms.SendOTA(orderToActivate.mobileno, first2, first23, 12000 * 4);
                                string[] resSms3 = sms.SendOTA(orderToActivate.mobileno, first3, first33, 12000 * 4);
                                string[] resSms4 = sms.SendOTA(orderToActivate.mobileno, first4, first43, 12000 * 4);
                                string[] resSms5 = sms.SendOTA(orderToActivate.mobileno, first5, first53, 12000 * 4);
                                string[] resSms6 = sms.SendOTA(orderToActivate.mobileno, first6, first63, 12000 * 4);
                                string[] resSms7 = sms.SendOTA(orderToActivate.mobileno, first7, first73, 12000 * 4);

                                //1.A. Log
                                int status = -1;
                                if (resSms.Length > 0)
                                    int.TryParse(resSms[0], out status);

                                if (resCodeMsg.errcode == 0 && status == -1)
                                {
                                    resCodeMsg.errcode = -1;
                                    resCodeMsg.errmsg = "Failed to send OTA string";
                                }
                                if (resCodeMsg.errmsg != "Failed to send OTA string")
                                {
                                    sms.Send(false, orderToActivate.mobileno, senderSMS, activateTextRestart2, 5000);
                                    Log.Debug(string.Format("{0} SMS Notification #1 {1} {2}", orderToActivate.mobileno, senderSMS, activateTextRestart2));
                                    // Send using the MMI sms
                                    sms.Send(true, orderToActivate.mobileno, senderSMS, activateTextRestart2, 5000);
                                    Log.Debug(string.Format("{0} SMS Notification #2 {1} {2}", orderToActivate.mobileno, senderSMS, activateTextRestart2));
                                    var Currency3 = CRM_API.DB.Customer2in1.SProc.Get2in1Currency4(orderToActivate.mobileno, Currency1.OTA_Customer_Id);
                                }
                                Log.Debug("--------------------------------------------------------");
                                return Request.CreateResponse(HttpStatusCode.OK, resCodeMsg);
                            }
                            return Request.CreateResponse(HttpStatusCode.OK, resCodeMsg);
                        }
                    }
                    else
                    {
                        // 1.       Send OTA
                        Helpers.SendSMS sms = new Helpers.SendSMS();
                        //KeyValuePair<int, IEnumerable<CRM_API.Models.OTAString>> listOTA =
                        //    CRM_API.DB.Customer2in1.SProc.List2in1OTAString(orderToActivate.order_id, orderToActivate.crm_login);

                        //if (uni1 != Convert.ToString(0))
                        {
                            Log.Debug("--------------------------------------------------------");

                            DB.Common.ErrCodeMsg resCodeMsg = new DB.Common.ErrCodeMsg()
                            {
                                errcode = 0,
                                errsubject = "Activation - Compatible SIM",
                                errmsg = "OK"
                            };
                            // foreach (var item in result)
                            {
                                {
                                    resCodeMsg.errcode = -1;
                                    resCodeMsg.errmsg = "Failed to send OTA string";
                                }
                                if (resCodeMsg.errmsg != "Failed to send OTA string")
                                {
                                    sms.Send(false, orderToActivate.mobileno, senderSMS, activateTextRestart, 5000);
                                    Log.Debug(string.Format("{0} SMS Notification #1 {1} {2}", orderToActivate.mobileno, senderSMS, activateTextRestart));
                                    // Send using the MMI sms
                                    sms.Send(true, orderToActivate.mobileno, senderSMS, activateTextRestart, 5000);
                                    Log.Debug(string.Format("{0} SMS Notification #2 {1} {2}", orderToActivate.mobileno, senderSMS, activateTextRestart));
                                    var Currency3 = CRM_API.DB.Customer2in1.SProc.Get2in1Currency3(orderToActivate.mobileno, Currency1.OTA_Customer_Id);
                                }
                                var Currency4 = CRM_API.DB.Customer2in1.SProc.Get2in1CurrencySMS1(orderToActivate.mobileno, orderToActivate.imsi_country);
                                Log.Debug("--------------------------------------------------------");
                                return Request.CreateResponse(HttpStatusCode.OK, resCodeMsg);
                            }
                            return Request.CreateResponse(HttpStatusCode.OK, resCodeMsg);
                        }
                        return Request.CreateResponse(HttpStatusCode.ExpectationFailed);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, new DB.Common.ErrCodeMsg() { errcode = -1, errmsg = "Empty OTA " });
                }
                // return Request.CreateResponse(HttpStatusCode.OK, new DB.Common.ErrCodeMsg() { errcode = -1, errmsg = "Empty OTA " });
            }
            else
            {
                Log.Debug(string.Format(""));
                Helpers.SendSMS sms = new Helpers.SendSMS();
                sms.Send(false, orderToActivate.mobileno, senderSMS, activateTextRestart1, 5000);
                Log.Debug(string.Format("{0} SMS Notification #1 {1} {2}", orderToActivate.mobileno, senderSMS, activateTextRestart1));
                // Send using the MMI sms
                sms.Send(true, orderToActivate.mobileno, senderSMS, activateTextRestart1, 5000);
                Log.Debug(string.Format("{0} SMS Notification #2 {1} {2}", orderToActivate.mobileno, senderSMS, activateTextRestart1));
                Log.Debug(string.Format(""));
                var Currency4 = CRM_API.DB.Customer2in1.SProc.Get2in1CurrencySMS1(orderToActivate.mobileno, orderToActivate.imsi_country);
            }
            DB.Common.ErrCodeMsg resCodeMsg1 = new DB.Common.ErrCodeMsg()
            {
                errcode = -1,
                errsubject = "Activation - NoN Compatible SIM",
                errmsg = "fail"
            };
            return Request.CreateResponse(HttpStatusCode.OK, resCodeMsg1);

        }


        //HttpResponseMessage ActivateOTAFree(CRM_API.Models.Customer2in1Records orderToActivate)
        //{

        //    CRM_API.Models.CustomerSearchResult resHistoryValues1 = CRM_API.DB.Customer.SProc.CustomerDetailInfo1(orderToActivate.mobileno);
        //    string productcode = resHistoryValues1.product;
        //    var Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(orderToActivate.mobileno, productcode);
        //    // CRM_API.Models.CustomerSearchResult resHistoryValues = CRM_API.DB.Customer.SProc.CustomerDetailInfo(productcode, orderToActivate.mobileno);
        //    //resHistoryValues.mimsi_compatible.ToString()==
        //    if (Customer.mimsi_compatible == 1)
        //    {
        //        string filename = string.Empty;

        //        //path create 
        //        string filePath = HostingEnvironment.MapPath("~/App_Data/" + "/" + "Country.csv");
        //        string file = filePath;
        //        //check exists file
        //        if (System.IO.File.Exists(file))
        //        {
        //            System.GC.Collect();
        //            System.GC.WaitForPendingFinalizers();
        //            System.IO.File.Delete(file);
        //        }

        //        // else if (!System.IO.File.Exists(file))
        //        {

        //            System.IO.FileStream f = System.IO.File.Create(file);
        //            f.Close();
        //            StringBuilder sb = new StringBuilder();
        //            var Currency2 = 0;
        //            var Currency1 = CRM_API.DB.Customer2in1.SProc.Get2in1CurrencySMS(orderToActivate.mobileno, orderToActivate.imsi_country);



        //            string OTA = Convert.ToString(Currency1.SPI) + " " + Convert.ToString(Currency1.KIC) + " " + Convert.ToString(Currency1.KID) + " " + Convert.ToString(Currency1.TAR) + " " + Convert.ToString(Currency1.Counter);
        //            if (Currency2 == 0)
        //            {
        //                //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F80A0A40000026F00A0DC030412034672616E6365FFFFFFFFFFFFFFFFFFFFFFFFA0A40000026F02A0DC02040302F803");
        //                //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F80A0A40000026F07A0DC010409082940809021000000A0A40000026F07A0DC020409082943108901000000A0A40000026F78A0DC0104020001A0A40000");
        //                //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F80A0A40000026F42A0DC010429534D532043656E7472652031FDFFFFFFFFFFFFFFFFFFFFFFFF07911356039900F0FFFFFFFFFFFFFF");
        //                //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F20A0A40000026F07A0DC020409082943108901000000A0A40000026F78A0DC0104020001");
        //                //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F80A0A40000026F42A0DC010429534D532043656E7472652031FDFFFFFFFFFFFFFFFFFFFFFFFF07913357999999F0FFFFFFFFFFFFFF");
        //                //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F80A0A40000026F07A0DC030409082980138901000000A0A40000026F78A0DC0304020001");
        //                //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F80A0A40000026F42A0DC030429534D532043656E7472652031FDFFFFFFFFFFFFFFFFFFFFFFFF0791448729199965FFFFFFFFFFFFFF");
        //                //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F80A0A40000026F42A0DC030429534D532043656E7472652031FDFFFFFFFFFFFFFFFFFFFFFFFF0791448729199965FFFFFFFFFFFFFF");
        //                sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd1));
        //                sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd2));
        //                sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd3));
        //                sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd4));
        //                sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd5));
        //                sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd6));
        //                sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd7));
        //                sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd8));
        //                byte[] temp = System.Text.Encoding.UTF8.GetBytes(sb.ToString());
        //                System.IO.File.WriteAllBytes(filePath, temp);
        //                Process notePad = new Process();
        //                //   notePad.StartInfo.FileName = @"C:\Users\user\Desktop\CRM\CRM-API\App_Data\OTA.exe";
        //                notePad.StartInfo.FileName = HostingEnvironment.MapPath("~/App_Data/" + "/" + "OTA.exe");
        //                notePad.StartInfo.Arguments = OTA + " " + file;
        //                notePad.StartInfo.RedirectStandardOutput = true;
        //                notePad.StartInfo.UseShellExecute = false;
        //                notePad.Start();
        //                StreamReader s = notePad.StandardOutput;
        //                Log.Debug("Exe results start");
        //                String output1 = s.ReadToEnd();
        //                Log.Debug("Exe results", output1);
        //                Log.Debug("Exe results end");
        //                string test = output1;
        //                string[] result = test.Replace("\r\n", "\n").Split("\n".ToCharArray());
        //                string uni1 = result[0].ToString();
        //                string uni2 = result[1].ToString();
        //                string uni3 = result[2].ToString();
        //                string uni4 = result[3].ToString();
        //                string[] uni11 = uni1.Split(',');
        //                string uni12 = uni1[0].ToString();
        //                string uni13 = uni1[1].ToString();
        //                var uni14 = uni1[2];
        //                string[] results;
        //                string[] results1;
        //                string[] results2;
        //                string[] results3;
        //                string[] results4;
        //                string[] results5;
        //                string[] results6;
        //                string[] results7;
        //                results = result[0].Split(',');
        //                results1 = result[1].Split(',');
        //                results2 = result[2].Split(',');
        //                results3 = result[3].Split(',');
        //                results4 = result[4].Split(',');
        //                results5 = result[5].Split(',');
        //                results6 = result[6].Split(',');
        //                results7 = result[7].Split(',');
        //                string first;
        //                string first03;
        //                string first1;
        //                string first13;
        //                string first2;
        //                string first23;
        //                string first3;
        //                string first33;
        //                string first4;
        //                string first43;
        //                string first5;
        //                string first53;
        //                string first6;
        //                string first63;
        //                string first7;
        //                string first73;
        //                first = results[1];
        //                first03 = results[2];
        //                first1 = results1[1];
        //                first13 = results1[2];
        //                first2 = results2[1];
        //                first23 = results2[2];
        //                first3 = results3[1];
        //                first33 = results3[2];
        //                first4 = results4[1];
        //                first43 = results4[2];
        //                first5 = results5[1];
        //                first53 = results5[2];
        //                first6 = results6[1];
        //                first63 = results6[2];
        //                first7 = results7[1];
        //                first73 = results7[2];
        //                notePad.WaitForExit();

        //                // 1.       Send OTA
        //                Helpers.SendSMS sms = new Helpers.SendSMS();
        //                //KeyValuePair<int, IEnumerable<CRM_API.Models.OTAString>> listOTA =
        //                //    CRM_API.DB.Customer2in1.SProc.List2in1OTAString(orderToActivate.order_id, orderToActivate.crm_login);

        //                if (uni1 != Convert.ToString(0))
        //                {
        //                    Log.Debug("--------------------------------------------------------");

        //                    DB.Common.ErrCodeMsg resCodeMsg = new DB.Common.ErrCodeMsg()
        //                    {
        //                        errcode = 0,
        //                        errsubject = "Activation - Compatible SIM",
        //                        errmsg = "OK"
        //                    };


        //                    // foreach (var item in result)
        //                    {
        //                        // Send using the originator sms

        //                        string[] resSms = sms.SendOTA(orderToActivate.mobileno, first, first03, 12000 * 4);
        //                        string[] resSms1 = sms.SendOTA(orderToActivate.mobileno, first1, first13, 12000 * 4);
        //                        string[] resSms2 = sms.SendOTA(orderToActivate.mobileno, first2, first23, 12000 * 4);
        //                        string[] resSms3 = sms.SendOTA(orderToActivate.mobileno, first3, first33, 12000 * 4);
        //                        string[] resSms4 = sms.SendOTA(orderToActivate.mobileno, first4, first43, 12000 * 4);
        //                        string[] resSms5 = sms.SendOTA(orderToActivate.mobileno, first5, first53, 12000 * 4);
        //                        string[] resSms6 = sms.SendOTA(orderToActivate.mobileno, first6, first63, 12000 * 4);
        //                        string[] resSms7 = sms.SendOTA(orderToActivate.mobileno, first7, first73, 12000 * 4);

        //                        //1.A. Log
        //                        int status = -1;
        //                        if (resSms.Length > 0)
        //                            int.TryParse(resSms[0], out status);

        //                        if (resCodeMsg.errcode == 0 && status == -1)
        //                        {
        //                            var Currency58 = CRM_API.DB.Customer2in1.SProc.Get2in1CurrencySMS1(orderToActivate.mobileno, orderToActivate.imsi_country);
        //                            resCodeMsg.errcode = -1;
        //                            resCodeMsg.errmsg = "Failed to send OTA string";
        //                        }
        //                        if (resCodeMsg.errmsg != "Failed to send OTA string")
        //                        {
        //                            sms.Send(false, orderToActivate.mobileno, senderSMS, activateTextRestart2, 5000);
        //                            Log.Debug(string.Format("{0} SMS Notification #1 {1} {2}", orderToActivate.mobileno, senderSMS, activateTextRestart2));
        //                            // Send using the MMI sms
        //                            sms.Send(true, orderToActivate.mobileno, senderSMS, activateTextRestart2, 5000);
        //                            Log.Debug(string.Format("{0} SMS Notification #2 {1} {2}", orderToActivate.mobileno, senderSMS, activateTextRestart2));
        //                            var Currency3 = CRM_API.DB.Customer2in1.SProc.Get2in1Currency4(orderToActivate.mobileno, Currency1.OTA_Customer_Id);
        //                        }
        //                        Log.Debug("--------------------------------------------------------");
        //                        var Currency4 = CRM_API.DB.Customer2in1.SProc.Get2in1CurrencySMS1(orderToActivate.mobileno, orderToActivate.imsi_country);
        //                        return Request.CreateResponse(HttpStatusCode.OK, resCodeMsg);
        //                    }
        //                    return Request.CreateResponse(HttpStatusCode.OK, resCodeMsg);
        //                }
        //            }
        //            else
        //            {
        //                sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd1));
        //                sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd2));
        //                sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd3));
        //                //sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd5));
        //                byte[] temp = System.Text.Encoding.UTF8.GetBytes(sb.ToString());
        //                System.IO.File.WriteAllBytes(filePath, temp);
        //                Process notePad = new Process();
        //                //   notePad.StartInfo.FileName = @"C:\Users\user\Desktop\CRM\CRM-API\App_Data\OTA.exe";
        //                notePad.StartInfo.FileName = HostingEnvironment.MapPath("~/App_Data/" + "/" + "OTA.exe");
        //                notePad.StartInfo.Arguments = OTA + " " + file;
        //                notePad.StartInfo.RedirectStandardOutput = true;
        //                notePad.StartInfo.UseShellExecute = false;
        //                notePad.Start();
        //                StreamReader s = notePad.StandardOutput;
        //                String output1 = s.ReadToEnd();
        //                string test = output1;
        //                string[] result = test.Replace("\r\n", "\n").Split("\n".ToCharArray());
        //                string uni1 = result[0].ToString();
        //                string uni2 = result[1].ToString();
        //                string uni3 = result[2].ToString();
        //                string uni4 = result[3].ToString();
        //                string[] uni11 = uni1.Split(',');
        //                string uni12 = uni1[0].ToString();
        //                string uni13 = uni1[1].ToString();
        //                var uni14 = uni1[2];
        //                string[] results;
        //                string[] results1;
        //                string[] results2;
        //                string[] results3;
        //                string[] results4;
        //                results = result[0].Split(',');
        //                results1 = result[1].Split(',');
        //                results2 = result[2].Split(',');
        //                // results3 = result[3].Split(',');
        //                // results4 = result[4].Split(',');
        //                string first;
        //                string first03;
        //                string first1;
        //                string first13;
        //                string first2;
        //                string first23;
        //                string first3;
        //                string first33;
        //                string first4;
        //                string first43;
        //                first = results[1];
        //                first03 = results[2];
        //                first1 = results1[1];
        //                first13 = results1[2];
        //                first2 = results2[1];
        //                first23 = results2[2];
        //                //first3 = results3[1];
        //                //first33 = results3[2];
        //                //first4 = results4[1];
        //                //first43 = results4[2];
        //                notePad.WaitForExit();

        //                // 1.       Send OTA
        //                Helpers.SendSMS sms = new Helpers.SendSMS();
        //                //KeyValuePair<int, IEnumerable<CRM_API.Models.OTAString>> listOTA =
        //                //    CRM_API.DB.Customer2in1.SProc.List2in1OTAString(orderToActivate.order_id, orderToActivate.crm_login);

        //                if (uni1 != Convert.ToString(0))
        //                {
        //                    Log.Debug("--------------------------------------------------------");

        //                    DB.Common.ErrCodeMsg resCodeMsg = new DB.Common.ErrCodeMsg()
        //                    {
        //                        errcode = 0,
        //                        errsubject = "Activation - Compatible SIM",
        //                        errmsg = "OK"
        //                    };
        //                    // foreach (var item in result)
        //                    {
        //                        // Send using the originator sms

        //                        string[] resSms = sms.SendOTA(orderToActivate.mobileno, first1, first03, 12000);
        //                        string[] resSms1 = sms.SendOTA(orderToActivate.mobileno, first1, first13, 12000);
        //                        string[] resSms2 = sms.SendOTA(orderToActivate.mobileno, first2, first23, 12000);
        //                        //string[] resSms3 = sms.SendOTA(orderToActivate.mobileno, first3, first33, 12000);
        //                        //string[] resSms4 = sms.SendOTA(orderToActivate.mobileno, first4, first43, 12000);

        //                        //1.A. Log
        //                        int status = -1;
        //                        if (resSms.Length > 0)
        //                            int.TryParse(resSms[0], out status);

        //                        if (resCodeMsg.errcode == 0 && status == -1)
        //                        {
        //                            var Currency4 = CRM_API.DB.Customer2in1.SProc.Get2in1CurrencySMS1(orderToActivate.mobileno, orderToActivate.imsi_country);
        //                            resCodeMsg.errcode = -1;
        //                            resCodeMsg.errmsg = "Failed to send OTA string";
        //                        }
        //                        if (resCodeMsg.errmsg != "Failed to send OTA string")
        //                        {
        //                            sms.Send(false, orderToActivate.mobileno, senderSMS, activateTextRestart, 5000);
        //                            Log.Debug(string.Format("{0} SMS Notification #1 {1} {2}", orderToActivate.mobileno, senderSMS, activateTextRestart));
        //                            // Send using the MMI sms
        //                            sms.Send(true, orderToActivate.mobileno, senderSMS, activateTextRestart, 5000);
        //                            Log.Debug(string.Format("{0} SMS Notification #2 {1} {2}", orderToActivate.mobileno, senderSMS, activateTextRestart));
        //                            var Currency3 = CRM_API.DB.Customer2in1.SProc.Get2in1Currency3(orderToActivate.mobileno, Currency1.OTA_Customer_Id);
        //                        }
        //                        Log.Debug("--------------------------------------------------------");
        //                        var Currency5 = CRM_API.DB.Customer2in1.SProc.Get2in1CurrencySMS1(orderToActivate.mobileno, orderToActivate.imsi_country);
        //                        return Request.CreateResponse(HttpStatusCode.OK, resCodeMsg);
        //                    }
        //                    return Request.CreateResponse(HttpStatusCode.OK, resCodeMsg);
        //                }
        //                var Currency6 = CRM_API.DB.Customer2in1.SProc.Get2in1CurrencySMS1(orderToActivate.mobileno, orderToActivate.imsi_country);
        //                return Request.CreateResponse(HttpStatusCode.ExpectationFailed);
        //            }
        //            var Currency9 = CRM_API.DB.Customer2in1.SProc.Get2in1CurrencySMS1(orderToActivate.mobileno, orderToActivate.imsi_country);
        //            return Request.CreateResponse(HttpStatusCode.OK, new DB.Common.ErrCodeMsg() { errcode = -1, errmsg = "Empty OTA " });
        //        }
        //        // return Request.CreateResponse(HttpStatusCode.OK, new DB.Common.ErrCodeMsg() { errcode = -1, errmsg = "Empty OTA " });
        //    }
        //    else
        //    {
        //        Log.Debug(string.Format(""));
        //        Helpers.SendSMS sms = new Helpers.SendSMS();
        //        sms.Send(false, orderToActivate.mobileno, senderSMS, activateTextRestart1, 5000);
        //        Log.Debug(string.Format("{0} SMS Notification #1 {1} {2}", orderToActivate.mobileno, senderSMS, activateTextRestart1));
        //        // Send using the MMI sms
        //        sms.Send(true, orderToActivate.mobileno, senderSMS, activateTextRestart1, 5000);
        //        Log.Debug(string.Format("{0} SMS Notification #2 {1} {2}", orderToActivate.mobileno, senderSMS, activateTextRestart1));
        //        Log.Debug(string.Format(""));
        //        var Currency4 = CRM_API.DB.Customer2in1.SProc.Get2in1CurrencySMS1(orderToActivate.mobileno, orderToActivate.imsi_country);
        //    }
        //    return Request.CreateResponse(HttpStatusCode.ExpectationFailed);

        //}
        HttpResponseMessage ActivateOTAVectoneLocal2in1(CRM_API.Models.Customer2in1Records orderToActivate)
        {

            string filename = string.Empty;

            //path create 
            string filePath = HostingEnvironment.MapPath("~/App_Data/" + "/" + "Country.csv");
            string file = filePath;
            //check exists file
            if (System.IO.File.Exists(file))
            {
                System.GC.Collect();
                System.GC.WaitForPendingFinalizers();
                System.IO.File.Delete(file);
            }

            // else if (!System.IO.File.Exists(file))
            {

                System.IO.FileStream f = System.IO.File.Create(file);
                f.Close();
                StringBuilder sb = new StringBuilder();
                var Currency2 = 0;
                var Currency1 = CRM_API.DB.Customer2in1.SProc.Get2in1CurrencyvECTONESMS(orderToActivate.mobileno, orderToActivate.imsi_country);
                string OTA = Convert.ToString(Currency1.SPI) + " " + Convert.ToString(Currency1.KIC) + " " + Convert.ToString(Currency1.KID) + " " + Convert.ToString(Currency1.TAR) + " " + Convert.ToString(Currency1.Counter);
                if (Currency2 == 0)
                {
                    //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F80A0A40000026F00A0DC030412034672616E6365FFFFFFFFFFFFFFFFFFFFFFFFA0A40000026F02A0DC02040302F803");
                    //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F80A0A40000026F07A0DC010409082940809021000000A0A40000026F07A0DC020409082943108901000000A0A40000026F78A0DC0104020001A0A40000");
                    //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F80A0A40000026F42A0DC010429534D532043656E7472652031FDFFFFFFFFFFFFFFFFFFFFFFFF07911356039900F0FFFFFFFFFFFFFF");
                    //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F20A0A40000026F07A0DC020409082943108901000000A0A40000026F78A0DC0104020001");
                    //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F80A0A40000026F42A0DC010429534D532043656E7472652031FDFFFFFFFFFFFFFFFFFFFFFFFF07913357999999F0FFFFFFFFFFFFFF");
                    //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F80A0A40000026F07A0DC030409082980138901000000A0A40000026F78A0DC0304020001");
                    //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F80A0A40000026F42A0DC030429534D532043656E7472652031FDFFFFFFFFFFFFFFFFFFFFFFFF0791448729199965FFFFFFFFFFFFFF");
                    //sb.AppendFormat("{0},{1},{2}\n", "8944010000131900000", "5F34FCE69CEF018FDAC0BACAE575FE1E", "A0A40000023F00A0A40000027F80A0A40000026F42A0DC030429534D532043656E7472652031FDFFFFFFFFFFFFFFFFFFFFFFFF0791448729199965FFFFFFFFFFFFFF");
                    sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd1));
                    sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd2));
                    sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd3));
                    sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd4));
                    sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd5));
                    sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd6));
                    sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd7));
                    sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd8));
                    byte[] temp = System.Text.Encoding.UTF8.GetBytes(sb.ToString());
                    System.IO.File.WriteAllBytes(filePath, temp);
                    Process notePad = new Process();
                    //   notePad.StartInfo.FileName = @"C:\Users\user\Desktop\CRM\CRM-API\App_Data\OTA.exe";
                    notePad.StartInfo.FileName = HostingEnvironment.MapPath("~/App_Data/" + "/" + "OTA.exe");
                    notePad.StartInfo.Arguments = OTA + " " + file;
                    notePad.StartInfo.RedirectStandardOutput = true;
                    notePad.StartInfo.UseShellExecute = false;
                    notePad.Start();
                    StreamReader s = notePad.StandardOutput;
                    Log.Debug("Exe results start");
                    String output1 = s.ReadToEnd();
                    Log.Debug("Exe results", output1);
                    Log.Debug("Exe results end");
                    string test = output1;
                    string[] result = test.Replace("\r\n", "\n").Split("\n".ToCharArray());
                    string uni1 = result[0].ToString();
                    string uni2 = result[1].ToString();
                    string uni3 = result[2].ToString();
                    string uni4 = result[3].ToString();
                    string[] uni11 = uni1.Split(',');
                    string uni12 = uni1[0].ToString();
                    string uni13 = uni1[1].ToString();
                    var uni14 = uni1[2];
                    string[] results;
                    string[] results1;
                    string[] results2;
                    string[] results3;
                    string[] results4;
                    string[] results5;
                    string[] results6;
                    string[] results7;
                    results = result[0].Split(',');
                    results1 = result[1].Split(',');
                    results2 = result[2].Split(',');
                    results3 = result[3].Split(',');
                    results4 = result[4].Split(',');
                    results5 = result[5].Split(',');
                    results6 = result[6].Split(',');
                    results7 = result[7].Split(',');
                    string first;
                    string first03;
                    string first1;
                    string first13;
                    string first2;
                    string first23;
                    string first3;
                    string first33;
                    string first4;
                    string first43;
                    string first5;
                    string first53;
                    string first6;
                    string first63;
                    string first7;
                    string first73;
                    first = results[1];
                    first03 = results[2];
                    first1 = results1[1];
                    first13 = results1[2];
                    first2 = results2[1];
                    first23 = results2[2];
                    first3 = results3[1];
                    first33 = results3[2];
                    first4 = results4[1];
                    first43 = results4[2];
                    first5 = results5[1];
                    first53 = results5[2];
                    first6 = results6[1];
                    first63 = results6[2];
                    first7 = results7[1];
                    first73 = results7[2];
                    notePad.WaitForExit();

                    // 1.       Send OTA
                    Helpers.SendSMS sms = new Helpers.SendSMS();
                    //KeyValuePair<int, IEnumerable<CRM_API.Models.OTAString>> listOTA =
                    //    CRM_API.DB.Customer2in1.SProc.List2in1OTAString(orderToActivate.order_id, orderToActivate.crm_login);

                    if (uni1 != Convert.ToString(0))
                    {
                        Log.Debug("--------------------------------------------------------");

                        DB.Common.ErrCodeMsg resCodeMsg = new DB.Common.ErrCodeMsg()
                        {
                            errcode = 0,
                            errsubject = "Activation - Compatible SIM",
                            errmsg = "OK"
                        };


                        // foreach (var item in result)
                        {
                            // Send using the originator sms

                            string[] resSms = sms.SendOTA(orderToActivate.mobileno, first, first03, 12000);
                            string[] resSms1 = sms.SendOTA(orderToActivate.mobileno, first1, first13, 12000 * 2);
                            string[] resSms2 = sms.SendOTA(orderToActivate.mobileno, first2, first23, 12000 * 2);
                            string[] resSms3 = sms.SendOTA(orderToActivate.mobileno, first3, first33, 12000 * 2);
                            string[] resSms4 = sms.SendOTA(orderToActivate.mobileno, first4, first43, 12000 * 2);
                            string[] resSms5 = sms.SendOTA(orderToActivate.mobileno, first5, first53, 12000 * 2);
                            string[] resSms6 = sms.SendOTA(orderToActivate.mobileno, first6, first63, 12000 * 2);
                            string[] resSms7 = sms.SendOTA(orderToActivate.mobileno, first7, first73, 12000 * 2);

                            //1.A. Log
                            int status = -1;
                            if (resSms.Length > 0)
                                int.TryParse(resSms[0], out status);

                            if (resCodeMsg.errcode == 0 && status == -1)
                            {
                                resCodeMsg.errcode = -1;
                                resCodeMsg.errmsg = "Failed to send OTA string";
                            }
                            if (resCodeMsg.errmsg != "Failed to send OTA string")
                            {
                                sms.Send(false, orderToActivate.mobileno, senderSMS, activateTextRestart2, 5000);
                                Log.Debug(string.Format("{0} SMS Notification #1 {1} {2}", orderToActivate.mobileno, senderSMS, activateTextRestart2));
                                // Send using the MMI sms
                                sms.Send(true, orderToActivate.mobileno, senderSMS, activateTextRestart2, 5000);
                                Log.Debug(string.Format("{0} SMS Notification #2 {1} {2}", orderToActivate.mobileno, senderSMS, activateTextRestart2));
                                var Currency3 = CRM_API.DB.Customer2in1.SProc.Get2in1CurrencyvECTONE4(orderToActivate.mobileno, Currency1.OTA_Customer_Id);
                            }
                            Log.Debug("--------------------------------------------------------");
                            return Request.CreateResponse(HttpStatusCode.OK, resCodeMsg);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, resCodeMsg);
                    }
                }
                else
                {
                    sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd1));
                    sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd2));
                    sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid).Trim(), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd3));
                    //sb.AppendFormat("{0},{1},{2}\n", Convert.ToString(Currency1.Iccid), Convert.ToString(Currency1.kc_Value), Convert.ToString(Currency1.Cmd5));
                    byte[] temp = System.Text.Encoding.UTF8.GetBytes(sb.ToString());
                    System.IO.File.WriteAllBytes(filePath, temp);
                    Process notePad = new Process();
                    //   notePad.StartInfo.FileName = @"C:\Users\user\Desktop\CRM\CRM-API\App_Data\OTA.exe";
                    notePad.StartInfo.FileName = HostingEnvironment.MapPath("~/App_Data/" + "/" + "OTA.exe");
                    notePad.StartInfo.Arguments = OTA + " " + file;
                    notePad.StartInfo.RedirectStandardOutput = true;
                    notePad.StartInfo.UseShellExecute = false;
                    notePad.Start();
                    StreamReader s = notePad.StandardOutput;
                    String output1 = s.ReadToEnd();
                    string test = output1;
                    string[] result = test.Replace("\r\n", "\n").Split("\n".ToCharArray());
                    string uni1 = result[0].ToString();
                    string uni2 = result[1].ToString();
                    string uni3 = result[2].ToString();
                    string uni4 = result[3].ToString();
                    string[] uni11 = uni1.Split(',');
                    string uni12 = uni1[0].ToString();
                    string uni13 = uni1[1].ToString();
                    var uni14 = uni1[2];
                    string[] results;
                    string[] results1;
                    string[] results2;
                    string[] results3;
                    string[] results4;
                    results = result[0].Split(',');
                    results1 = result[1].Split(',');
                    results2 = result[2].Split(',');
                    // results3 = result[3].Split(',');
                    // results4 = result[4].Split(',');
                    string first;
                    string first03;
                    string first1;
                    string first13;
                    string first2;
                    string first23;
                    string first3;
                    string first33;
                    string first4;
                    string first43;
                    first = results[1];
                    first03 = results[2];
                    first1 = results1[1];
                    first13 = results1[2];
                    first2 = results2[1];
                    first23 = results2[2];
                    //first3 = results3[1];
                    //first33 = results3[2];
                    //first4 = results4[1];
                    //first43 = results4[2];
                    notePad.WaitForExit();

                    // 1.       Send OTA
                    Helpers.SendSMS sms = new Helpers.SendSMS();
                    //KeyValuePair<int, IEnumerable<CRM_API.Models.OTAString>> listOTA =
                    //    CRM_API.DB.Customer2in1.SProc.List2in1OTAString(orderToActivate.order_id, orderToActivate.crm_login);

                    if (uni1 != Convert.ToString(0))
                    {
                        Log.Debug("--------------------------------------------------------");

                        DB.Common.ErrCodeMsg resCodeMsg = new DB.Common.ErrCodeMsg()
                        {
                            errcode = 0,
                            errsubject = "Activation - Compatible SIM",
                            errmsg = "OK"
                        };
                        // foreach (var item in result)
                        {
                            // Send using the originator sms

                            string[] resSms = sms.SendOTA(orderToActivate.mobileno, first1, first03, 12000);
                            string[] resSms1 = sms.SendOTA(orderToActivate.mobileno, first1, first13, 12000);
                            string[] resSms2 = sms.SendOTA(orderToActivate.mobileno, first2, first23, 12000);
                            //string[] resSms3 = sms.SendOTA(orderToActivate.mobileno, first3, first33, 12000);
                            //string[] resSms4 = sms.SendOTA(orderToActivate.mobileno, first4, first43, 12000);

                            //1.A. Log
                            int status = -1;
                            if (resSms.Length > 0)
                                int.TryParse(resSms[0], out status);

                            if (resCodeMsg.errcode == 0 && status == -1)
                            {
                                resCodeMsg.errcode = -1;
                                resCodeMsg.errmsg = "Failed to send OTA string";
                            }
                            if (resCodeMsg.errmsg != "Failed to send OTA string")
                            {
                                sms.Send(false, orderToActivate.mobileno, senderSMS, activateTextRestart, 5000);
                                Log.Debug(string.Format("{0} SMS Notification #1 {1} {2}", orderToActivate.mobileno, senderSMS, activateTextRestart));
                                // Send using the MMI sms
                                sms.Send(true, orderToActivate.mobileno, senderSMS, activateTextRestart, 5000);
                                Log.Debug(string.Format("{0} SMS Notification #2 {1} {2}", orderToActivate.mobileno, senderSMS, activateTextRestart));
                                var Currency3 = CRM_API.DB.Customer2in1.SProc.Get2in1Currency3(orderToActivate.mobileno, Currency1.OTA_Customer_Id);
                            }
                            Log.Debug("--------------------------------------------------------");
                            return Request.CreateResponse(HttpStatusCode.OK, resCodeMsg);
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, resCodeMsg);
                    }
                    return Request.CreateResponse(HttpStatusCode.ExpectationFailed);
                }
                return Request.CreateResponse(HttpStatusCode.OK, new DB.Common.ErrCodeMsg() { errcode = -1, errmsg = "Empty OTA " });
            }

            return Request.CreateResponse(HttpStatusCode.ExpectationFailed);

        }
        HttpResponseMessage ActivateSwap(CRM_API.Models.Customer2in1Records orderToSwap)
        {
            if (orderToSwap != null && (orderToSwap.order_id <= 0 || string.IsNullOrEmpty(orderToSwap.swapiccid)))
                return Request.CreateResponse(HttpStatusCode.PreconditionFailed, new DB.Common.ErrCodeMsg() { errcode = -1, errmsg = "Empty variable" });

            Helpers.SendSMS sms = new Helpers.SendSMS();

            CRM_API.Models.SMSMsgReturn resSwapMsg = new Models.SMSMsgReturn()
            {
                errsubject = "Activation - Non Compatible SIM",
                errcode = -1,
                errmsg = "Failed"
            };

#if !DEBUG
            resSwapMsg =
                CRM_API.DB.Customer2in1.SProc.SimSwap(orderToSwap.order_id, orderToSwap.swapiccid, orderToSwap.crm_login);
            if (resSwapMsg != null)
            {
                Log.Debug("--------------------------------------------------------");
                Log.Debug(string.Format("{0} SIM Swap {1}:{2} {3} {4} {5}",
                    orderToSwap.mobileno,
                    resSwapMsg.errcode, resSwapMsg.errmsg,
                    orderToSwap.swapiccid, orderToSwap.crm_login, resSwapMsg.sms_msg));

                if (resSwapMsg.errcode == 0)
                {
                    //4.       Cancel Location Update
                    string[] resUpdate = new Helpers.HLR().CancelLocation(orderToSwap.mobileno);
                    int status = -1;
                    if (resUpdate.Length > 0)
                        int.TryParse(resUpdate[0], out status);
                    Log.Debug(string.Format("{0} Location Update Status {1}", orderToSwap.mobileno, status));
                    /*
                    if (status == -1)
                    {
                        resSwapMsg.errcode = -1;
                        resSwapMsg.errmsg = "Failed to send Location Update";
                    }
                     */
                }

                if (resSwapMsg.errcode == 0)
                {
                    // Update status
                    CRM_API.DB.Customer2in1.SProc.OrderStatusUpdate(orderToSwap.order_id, "ACTIVE", orderToSwap.crm_login);
                    Log.Debug(string.Format("{0} Change Status to Active", orderToSwap.mobileno));
                    // Send using the originator sms
                    sms.Send(false, orderToSwap.mobileno, senderSMS, resSwapMsg.sms_msg, 100);
                    Log.Debug(string.Format("{0} SMS Notification #1 {1} {2}", orderToSwap.mobileno, senderSMS, resSwapMsg.sms_msg));
                    // Send using the MMI sms
                    sms.Send(true, orderToSwap.mobileno, senderSMS, resSwapMsg.sms_msg, 100);
                    Log.Debug(string.Format("{0} SMS Notification #2 {1} {2}", orderToSwap.mobileno, senderSMS, resSwapMsg.sms_msg));
                }
                Log.Debug("--------------------------------------------------------");
            }
#endif

            return Request.CreateResponse(HttpStatusCode.OK, resSwapMsg);
        }

        HttpResponseMessage InsertSIMOrder(CRM_API.Models.Customer2in1Records InsertSimOrder)
        {
            CRM_API.Models.SimOrder errResult = new CRM_API.Models.SimOrder();

#if DEBUG
            InsertSimOrder.date_of_birth = new DateTime(1915, 10, 14);
#endif

            errResult = CRM_API.DB.Customer2in1.SProc.InsertSimOrderRecords(InsertSimOrder, charge2in1);

            if (errResult.errcode == 0)
            {
                string home_country = CRM_API.DB.Country.CountrySProc.GetCountryByMSIDN(InsertSimOrder.mobileno);
                errResult.errsubject = "SIM Ordered Successfully";
                errResult.errmsg = string.Format("You have placed an order for a new sim {0}-{1} successfully.Order ID : {2}", home_country, InsertSimOrder.imsi_country, errResult.order_id);
            }
            else
            {
                errResult.errsubject = "SIM Order Failed";
                if (string.IsNullOrEmpty(InsertSimOrder.payment_ref) || InsertSimOrder.payment_ref == "-1")
                    errResult.errmsg = string.Format("{0}", errResult.errmsg);
                else
                    errResult.errmsg = string.Format("{0}. Order Number {1}", errResult.errmsg, InsertSimOrder.payment_ref);
            }

            return Request.CreateResponse(HttpStatusCode.OK, errResult);
        }

        // Reactivation without payment
        void ReActivateByCardNoPayment(CRM_API.Models.Customer2in1Records order, out DB.Common.ErrCodeMsg errResult)
        {
            Log.Debug("Customer2in1Controller.ReActivateByCard");
            errResult = new DB.Common.ErrCodeMsg();
            if (order.order_id <= 0)
            {
                errResult.errcode = -1;
                errResult.errsubject = "General Error";
                errResult.errmsg = "Empty New Order";
            }
            else
            {
                order = CRM_API.DB.Customer2in1.SProc.GetStatus(order.order_id, order.SubscriptionID, order.mobileno, order.crm_login);
                if (string.IsNullOrEmpty(order.order_status) || order.order_status != "INACTIVE")
                {
                    errResult.errcode = -1;
                    errResult.errsubject = "General Error";
                    errResult.errmsg = "2in1 cannot be re-activate";
                }
                else
                {
                    errResult = DB.Customer2in1.SProc.ReActivate(order.order_id, order.crm_login);

                    if (errResult.errcode == 0)
                    {
                        string home_country = CRM_API.DB.Country.CountrySProc.GetCountryByMSIDN(order.mobileno);
                        errResult.errsubject = "ReActivation";
                        errResult.errmsg = string.Format("You have reactivated 2 in 1 {0}-{1} successfully. ", home_country, order.imsi_country);
                    }
                    else
                    {
                        errResult.errsubject = "ReActivation";
                        errResult.errmsg = "Reactivation Failed";
                    }
                }
            }
        }

        HttpResponseMessage newCountryList(CRM_API.Models.Customer2in1Records product_code)
        {
                IEnumerable<CRM_API.Models.CountryInfo> listResult =
                    CRM_API.DB.Country.CountrySProc.Get2in1CountriesByProductCode(product_code.product_code);
                return Request.CreateResponse(HttpStatusCode.OK, listResult);
        }

        HttpResponseMessage GetCountryCurrency(CRM_API.Models.Customer2in1Records In)
        {
            var Currency = CRM_API.DB.Customer2in1.SProc.Get2in1Currency(In.product_code, In.imsi_country);
            return Request.CreateResponse(HttpStatusCode.OK, Currency);
        }
    }
}
