﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using CRM_API.Models;
using Mandrill;
using Mandrill.Model;
using Newtonsoft.Json;
using NLog;

namespace CRM_API.Controllers
{
    public class MailChimpController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public HttpResponseMessage Post(EmailSettingsModel model)
        {
            Log.Info("MailChimpController : Input : " + JsonConvert.SerializeObject(model));
            if (model != null)
            {
                switch (model.modetype)
                {
                    case Models.EmailSettingsModeType.GETEMAILASSIGNING:
                        return GetEmailTemplates();
                    case Models.EmailSettingsModeType.EMAILQUEUEASSIGNEDREPLY:
                        return EmailQueueSaveReplyText(model.queue_id, model.email_to, model.email_cc, model.email_subject, model.email_body, model.is_closed, model.filename);
                    case Models.EmailSettingsModeType.SENDMAIL:
                        return SendMail(model.email_to, model.email_subject, model.email_body);
                    case Models.EmailSettingsModeType.GENEARATETICKET:
                        return GenerateTicket(model);
                    case Models.EmailSettingsModeType.CHANGEEMAILQUEUESTATUS:
                        return ChangeEmailQueueStatus(model.queue_id, model.status, model.email_to, model.email_cc, model.assigned_user_id, model.description, model.compaint_category);
                    case Models.EmailSettingsModeType.RESETMYACCOUNTPASSWORD:
                        return SendMail(model.email_to, model.email_subject, model.email_body);
                    default:
                        return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.EmailSettingsModel>().ToArray());
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.EmailModel>().ToArray());
            }
        }

        public HttpResponseMessage ChangeEmailQueueStatus(int queue_id, int status, string email_to, string email_cc, int assigned_user_id, string descr, string compaint_category)
        {
            IEnumerable<CRM_API.Models.ChangeEmailQueueStatusOutput> resValues = CRM_API.DB.Emails.ChangeEmailQueueStatus(queue_id, status, assigned_user_id, descr, compaint_category);
            if (resValues != null && resValues.ElementAt(0).errcode == 0 && !String.IsNullOrEmpty(resValues.ElementAt(0).email_sub) && !String.IsNullOrEmpty(resValues.ElementAt(0).email_filename))
            {
                var api = new MandrillApi(ConfigurationManager.AppSettings["MAILCHIMPAPIKEY"]);
                var message = new MandrillMessage();
                message.FromEmail = ConfigurationManager.AppSettings["QUEUEEMAILMAILFROM"];
                message.AddTo(email_to);
                message.AddTo(email_cc);
                message.Subject = resValues.ElementAt(0).email_sub;
                message.AddGlobalMergeVars("BodyText", descr);
                var result = api.Messages.SendTemplateAsync(message, resValues.ElementAt(0).email_filename).Result;
                Log.Info("ChangeEmailQueueStatus Mail Response : " + JsonConvert.SerializeObject(result));
            }
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.ChangeEmailQueueStatusOutput>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        private HttpResponseMessage GenerateTicket(EmailSettingsModel Model)
        {
            IEnumerable<CRM_API.Models.GenerateTicketOutput> resValues = CRM_API.DB.EmailGenerateTicket.GenerateTicket(Model);
            if (resValues != null && resValues.ElementAt(0).errcode == 0)
            {
                var api = new MandrillApi(ConfigurationManager.AppSettings["MAILCHIMPAPIKEY"]);
                var message = new MandrillMessage();
                message.FromEmail = ConfigurationManager.AppSettings["QUEUEEMAILMAILFROM"];
                message.AddTo(Model.emailto);
                message.AddTo(Model.email_cc);
                message.Subject = resValues.ElementAt(0).email_sub;
                message.AddGlobalMergeVars("FNAME", "Customer");
                message.AddGlobalMergeVars("BodyText", Model.email_body);
                var result = api.Messages.SendTemplateAsync(message, resValues.ElementAt(0).email_filename).Result;
                Log.Info("GenerateTicket Mail Response : " + JsonConvert.SerializeObject(result));
                if (!String.IsNullOrEmpty(resValues.ElementAt(0).sms_url))
                    CRM_API.Helpers.SendSMS.SMSSendOurGateway(resValues.ElementAt(0).sms_org_address, Model.MobileNo, resValues.ElementAt(0).sms_text, resValues.ElementAt(0).sms_url);

                resValues.ElementAt(0).userid = Model.userid;
            }
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.GenerateTicketOutput>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        private HttpResponseMessage GetEmailTemplates()
        {
            Log.Info("GetEmailTemplates");
            try
            {
                var api = new MandrillApi(ConfigurationManager.AppSettings["MAILCHIMPAPIKEY"]);
                IList<Mandrill.Model.MandrillTemplateInfo> lstTemplates = api.Templates.ListAsync("CRM-Templates").Result;
                //IList<Mandrill.Model.MandrillTemplateInfo> output = lstTemplates;
                if (lstTemplates == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.EmailAssigningOutput>().ToArray());
                else
                {
                    for (int index = 0; index < lstTemplates.Count; index++)
                    {
                        string fileName = Path.Combine(ConfigurationManager.AppSettings["QUEUEEMAILTEMPLATEPATH"], lstTemplates[index].Name.Replace(" ", "") + ".html");
                        if (!File.Exists(fileName) || (lstTemplates[index].PublishedAt != null && lstTemplates[index].PublishedAt >= DateTime.Now.AddDays(-7)))
                        {
                            try
                            {
                                File.WriteAllText(fileName, lstTemplates[index].Code);
                            }
                            catch { }
                        }
                        lstTemplates[index].PublishName = fileName.Replace(ConfigurationManager.AppSettings["QUEUEEMAILTEMPLATEPATH"], ConfigurationManager.AppSettings["QUEUEEMAILTEMPLATEURL"]);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, lstTemplates.ToArray());
                }
            }
            catch (Exception ex)
            {
                Log.Error("GetEmailTemplates : " + ex.Message);
                Log.Error("GetEmailTemplates : " + ex.StackTrace);
                string name = "CRM Ticket Creation";
                string slug = "crm-ticket-creation";
                string fileName = Path.Combine(ConfigurationManager.AppSettings["QUEUEEMAILTEMPLATEPATH"], name.Replace(" ", "") + ".html");
                return Request.CreateResponse(HttpStatusCode.OK, new List<Mandrill.Model.MandrillTemplateInfo>() { new Mandrill.Model.MandrillTemplateInfo { Name = name, PublishName = fileName.Replace(ConfigurationManager.AppSettings["QUEUEEMAILTEMPLATEPATH"], ConfigurationManager.AppSettings["QUEUEEMAILTEMPLATEURL"]) , Slug=slug} }.ToArray());
            }
        }

        public HttpResponseMessage EmailQueueSaveReplyText(int queue_id, string email_to, string email_cc, string email_subject, string email_body, int is_closed, string filename)
        {
            IEnumerable<CRM_API.Models.EmailQueueSaveReplyTextOutput> resValues = new List<CRM_API.Models.EmailQueueSaveReplyTextOutput>() { new CRM_API.Models.EmailQueueSaveReplyTextOutput { errcode = -1, errmsg = "Failed" } };
            try
            {
                var api = new MandrillApi(ConfigurationManager.AppSettings["MAILCHIMPAPIKEY"]);
                var message = new MandrillMessage();
                message.FromEmail = ConfigurationManager.AppSettings["QUEUEEMAILMAILFROMSUPPORT"];
                message.AddTo(email_to);
                message.Subject = email_subject;
                message.AddGlobalMergeVars("FNAME", "Customer");
                message.AddGlobalMergeVars("BodyText", email_body);
                message.Html = email_body;
                message.AutoHtml = true;
                var result = api.Messages.SendTemplateAsync(message, filename).Result;
                Log.Info("EmailQueueSaveReplyText Mail Response : " + JsonConvert.SerializeObject(result));
                string errMsg = "Email ";
                if (result != null && result.Count > 0)
                {
                    errMsg += result.ElementAt(0).Status.ToString() + ". ";
                    if (result.ElementAt(0).Status == MandrillSendMessageResponseStatus.Sent || result.ElementAt(0).Status == MandrillSendMessageResponseStatus.Queued || result.ElementAt(0).Status == MandrillSendMessageResponseStatus.Scheduled)
                    {
                        resValues = CRM_API.DB.Emails.EmailQueueSaveReplyText(queue_id, email_to, email_cc, email_subject, email_body, is_closed, filename);
                    }
                    if (resValues != null && resValues.Count() > 0)
                        resValues.ElementAt(0).errmsg = errMsg + resValues.ElementAt(0).errmsg;
                }
            }
            catch (Exception ex)
            {
                Log.Error("EmailQueueSaveReplyText : " + ex.Message);
                resValues = new List<CRM_API.Models.EmailQueueSaveReplyTextOutput>() { new CRM_API.Models.EmailQueueSaveReplyTextOutput { errcode = -1, errmsg = ex.Message } };
            }
            return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        private HttpResponseMessage SendMail(string email_to, string email_subject, string email_body)
        {
            var api = new MandrillApi(ConfigurationManager.AppSettings["MAILCHIMPAPIKEY"]);
            string[] emailto = email_to.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            var message = new MandrillMessage();
            message.FromEmail = ConfigurationManager.AppSettings["QUEUEEMAILMAILFROMSUPPORT"];
            message.Subject = email_subject;
            message.AddGlobalMergeVars("FNAME", "Customer");
            message.AddGlobalMergeVars("BodyText", email_body);
            message.Html = email_body;
            message.AutoHtml = true;
            //var message = new MandrillMessage(ConfigurationManager.AppSettings["QUEUEEMAILMAILFROM"], emailto[iCount],
            //                        email_subject, email_body);
            for (int iCount = 0; iCount < emailto.Count(); iCount++)
            {
                message.AddTo(emailto[iCount]);
            }
            var result = api.Messages.SendAsync(message).Result;
            Log.Info("SendMail Mail Response : " + JsonConvert.SerializeObject(result));
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
    }
}
