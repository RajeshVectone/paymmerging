﻿using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using CRM_API.Models;
using CRM_API.DB;
using System.Configuration;

namespace CRM_API.Controllers
{
    public class FreeCreditSubscribeController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        [HttpPost]
        public HttpResponseMessage Post(FreeCreditSubscribeInput input)
        {
            try
            {
                IEnumerable<FreeCreditSubscribeOutput> resultValues = FreeCreditDB.Subscribe(input);
                if (resultValues != null && resultValues.Count() > 0 && resultValues.ElementAt(0).errcode == 0)
                {
                    if (input.sitecode == "MCM")
                    {
                        //SMS Trigger
                        try
                        {
                            string smsURL = CRM_API.DB.SMSCampaign.SProc.GetSMSGatewayInfo(input.sitecode, input.mobileno);
                            var smsResponse = CRM_API.Helpers.SendSMS.SMSSendOurGateway("Vectone", input.mobileno, string.Format(ConfigurationManager.AppSettings["SMSMESSAGEFREECREDITADDED"], input.price), smsURL);
                            CRM_API.DB.SMSCampaign.SProc.InsertSMSNotifyText(input.mobileno, string.Format(ConfigurationManager.AppSettings["SMSMESSAGEFREECREDITADDED"], input.price), input.crm_user, smsResponse == "-1" ? 0 : 1, "-", "-", "Free Credit added to the customer");
                            Log.Info("FreeCreditSubscribe : SMS : Response : " + smsResponse);
                        }
                        catch (Exception ex)
                        {
                            Log.Error("FreeCreditSubscribe SMS Error : " + ex.Message);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, resultValues.ToArray());
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new List<CRM_API.Models.FreeCreditOutput>());
            }
            return Request.CreateResponse(HttpStatusCode.NotFound, new List<FreeCreditOutput>());
        }
    }
}
