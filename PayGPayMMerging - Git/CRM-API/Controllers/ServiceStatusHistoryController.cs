﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CRM_API.Controllers
{
    public class ServiceStatusHistoryController : ApiController
    {
        /// <summary>
        /// /// Add by BSK for CRMT-111 Jira
        /// Get All Service Status History Details
        /// api/ServiceStatusHistory
        /// </summary>
        public HttpResponseMessage Get(string MobileNo, string ProductCode)
        {
            try
            {
                //31-Jul-2015 : Moorthy Added
                System.Web.HttpContext.Current.Session["GlobalProduct"] = ProductCode;
                var result = CRM_API.DB.PAYMBillGeneration.getServiceStatusHistory(MobileNo);
                return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
            }
            catch (WebException ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new List<CRM_API.Models.ServiceStatusHistory>());
            }
        }
    }
}
