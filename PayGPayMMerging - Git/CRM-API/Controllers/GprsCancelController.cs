﻿using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace CRM_API.Controllers
{
    public class GprsCancelController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public async Task<HttpResponseMessage> Get(string ICCID, string Sitecode, string type)
        {
            Log.Debug("GPRS Cancel Location.GET: Started");
            try
            {
                using (var client = new HttpClient())
                {
                    var Location = CRM_API.DB.SIMOrder.SProc.GetFreeSimByFullnameGroupBycancel(ICCID, Sitecode, type);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
                    string mode;
                    mode = "http";
                    string urll;
                    int node_type = 2;
                    string vlr_number;
                    string imsi_active;
                    imsi_active = Location.ElementAt(0).imsi_active;
                    vlr_number = Location.ElementAt(0).vlr_number;
                    urll = Location.ElementAt(0).url;//mode=http&data-vlr-number=447953713433&data-imsi=234010020000519&data-node-type=2
                    string postData = "mode=" + mode + "&data-vlr-number=" + vlr_number + "&data-imsi=" + imsi_active +"&data-node-type="+ node_type;
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(urll);
                    request.Method = "POST";
                    request.ContentType = "application/x-www-form-urlencoded";
                    ASCIIEncoding enc = new ASCIIEncoding();
                    byte[] data = enc.GetBytes(postData);
                    request.ContentLength = data.Length;
                    Stream newStream = request.GetRequestStream();
                    newStream.Write(data, 0, data.Length);
                    newStream.Close();
                    HttpWebResponse smsRes = (HttpWebResponse)request.GetResponse();
                    Stream resStream = smsRes.GetResponseStream();
                    Log.Debug("GPRS Cancel Location. Success");
                    Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                    StreamReader readStream = new StreamReader(resStream, encode);
                    return Request.CreateResponse(HttpStatusCode.OK, Location);
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new Models.Common.ErrCodeMsg() { errcode = -1, errmsg = e.Message });
            }
        }
    }
}
