﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Mandrill.Model;
using Newtonsoft.Json;
using NLog;

namespace CRM_API.Controllers
{
    /// <summary>
    /// Web Api for Sim Swap Process
    /// </summary>
    public class SimSwapController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        private int SimType;
        private CRM_API.Models.ESPAccount _accinfo_Old;
        private CRM_API.Models.ESPAccount _accinfo_New;
        /// <summary>
        /// Uses : api/simswap
        /// Method : Get
        /// Test Access
        /// </summary>
        /// <returns>string[]</returns>
        public HttpResponseMessage Get(string Option, string ProductCode = "")
        {
            switch (Option.ToLower())
            {
                case "reason":
                    return GetReasonOption(ProductCode);
                case "typeofsim":
                    return GetTypeofSimOption(ProductCode);
                default: return GetReasonOption(ProductCode);
            }
        }

        private HttpResponseMessage GetReasonOption(string ProductCode)
        {
            Dictionary<int, string> result = new Dictionary<int, string>();
            if (ProductCode.Contains("VM"))
                result.Add((int)CRM_API.Models.SwapType.PortIn, "Portin Vectone To Delight");
            else
                result.Add((int)CRM_API.Models.SwapType.PortIn, "Portin Delight To Vectone");
            result.Add((int)CRM_API.Models.SwapType.LostNumber, "Lost or Stolen");
            result.Add((int)CRM_API.Models.SwapType.NewSimType, "New Sim Type");
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
        private HttpResponseMessage GetTypeofSimOption(string ProductCode)
        {
            Dictionary<int, string> result = new Dictionary<int, string>();
            result.Add((int)CRM_API.Models.SimType.Micro, "Micro");
            result.Add((int)CRM_API.Models.SimType.Nano, "Nano");
            result.Add((int)CRM_API.Models.SimType.Normal, "Normal");
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
        /// <summary>
        /// POST api/simswap
        /// Send Data for Swap number.
        /// </summary>
        /// <param name="Model" type="string">MsisdnOld</param>
        /// <param name="Model" type="string">MsisdnNew</param>
        /// <param name="Model" type="string">JiraTicketNo</param>
        /// <param name="Model" type="decimal">SimSwapCharge</param>
        /// <param name="Model" type="string">VerificationCode</param>
        /// <param name="Model" type="Enum">Type</param>
        /// <param name="Model" type="string">User</param>
        /// <param name="Model" type="string">Sitecode</param>
        /// <returns></returns>
        public HttpResponseMessage Post(CRM_API.Models.SimSwapSearch Model)
        {

            try
            {

                switch (Model.TypeofSwap)
                {
                    case Models.SwapType.Validate:
                        return Validate(Model);
                    case Models.SwapType.PortIn:
                        if (Model.IsNewSim == 1)
                        {
                            Model.SwapType = "1";
                            return DoSimSwap(Model);
                        }
                        else
                        {
                            Model.SwapType = "1";
                            return DoPlaceOrder(Model, (int)Model.TypeofSim);
                        }

                    case Models.SwapType.LostNumber:
                        if (Model.IsNewSim == 1)
                        {
                            Model.SwapType = "2";
                            return DoSimSwap(Model);
                        }
                        else
                        {
                            Model.SwapType = "2";
                            return DoPlaceOrder(Model, (int)Model.TypeofSim);
                        }
                    case Models.SwapType.NewSimType:
                        if (Model.IsNewSim == 1)
                        {
                            Model.SwapType = "3";
                            return DoSimSwap(Model);
                        }
                        else
                        {
                            Model.SwapType = "3";
                            return DoPlaceOrder(Model, SimType);
                        }
                    case Models.SwapType.FillSIMReports:
                        return FillSIMReports(Model);
                    default: break;
                }
                return Request.CreateResponse(HttpStatusCode.OK, Model);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new CRM_API.Models.Common.ErrCodeMsg()
                {
                    errcode = -1,
                    errmsg = e.Message,
                    errsubject = "General Error"
                });
            }
        }

        private HttpResponseMessage FillSIMReports(Models.SimSwapSearch Model)
        {
            IEnumerable<CRM_API.Models.FillSIMReportsRef> resValues = CRM_API.DB.SimSwap.SProc.FillSIMReports(Model.MsisdnNew, Model.Sitecode);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.OK, new List<CRM_API.Models.FillSIMReportsRef>() { new CRM_API.Models.FillSIMReportsRef { errcode = -1, errmsg = "No Records found." } }.ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        private HttpResponseMessage DoPlaceOrder(CRM_API.Models.SimSwapSearch In, int InSIMType)
        {
            DB.Common.ErrCodeMsg errResult = new DB.Common.ErrCodeMsg()
            {
                errcode = -1,
                errsubject = "General Error",
                errmsg = "Empty New Order"
            };
            var CountryCode = CRM_API.DB.Subscriber.SProc.GetCountryCode(In.Country, In.Sitecode);
            In.CountryCode = CountryCode.countrycode;
            int subscriber = CRM_API.DB.Subscriber.SProc.submitsubscriberinformation(In);
            if (subscriber != 0)
            {
                int freesimid = CRM_API.DB.Subscriber.SProc.insertFreeSIm(subscriber, InSIMType, In.Sitecode);
                if (freesimid != 0)
                {
                    errResult.errcode = 0;
                    errResult.errsubject = "Success";
                    errResult.errmsg = string.Format("Free SIM ID : {0}", freesimid);
                    if (In.Sitecode == "MCM" || In.Sitecode == "BAU")
                    {
                        try
                        {
                            var api = new Mandrill.MandrillApi(ConfigurationManager.AppSettings["MAILCHIMPAPIKEY"]);
                            var message = new MandrillMessage();
                            //message.FromEmail = ConfigurationManager.AppSettings["QUEUEEMAILMAILFROM"];
                            message.AddGlobalMergeVars("FNAME", In.Firstname);
                            message.AddGlobalMergeVars("orderid", freesimid);
                            message.AddTo(In.Email);
                            IList<MandrillSendMessageResponse> response = api.Messages.SendTemplateAsync(message, ConfigurationManager.AppSettings["MAILCHIMPFREESIMORDER_" + In.Sitecode]).Result;
                            Log.Info("SimSwap Mail Response : " + JsonConvert.SerializeObject(response));
                        }
                        catch (Exception ex)
                        {
                            Log.Error("SimSwap Mail Error : " + ex.Message);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, errResult);
                }
                else
                {
                    errResult.errmsg = "Can't Request Free SIM";
                    return Request.CreateResponse(HttpStatusCode.OK, errResult);
                }
            }
            else
            {
                errResult.errmsg = "Can't Save Personal Data";
                ; return Request.CreateResponse(HttpStatusCode.OK, errResult);
            }

        }

        private HttpResponseMessage DoSimSwap(CRM_API.Models.SimSwapSearch In)
        {
            Log.Info("DoSimSwap : Input : " + JsonConvert.SerializeObject(In));
            var resVerify = Verify(In);
            if (resVerify.errcode != 0)
                return Request.CreateResponse(HttpStatusCode.OK, resVerify);

            var result = CRM_API.DB.SimSwap.SProc.PutSimSwap(In.MsisdnOld, _accinfo_New.ICCID, In.Sitecode, In.VerificationCode, In.User, In.JiraTicketNo, float.Parse(In.SimSwapCharge.ToString()), In.SwapType.ToString());
            if (result.errmsg.ToLower().Contains("swap sim success.") && result.errcode == 1)
            {
                result.errcode = 0;
                result.errsubject = "";

                //30-Jul-2019: Moorthy : Added for sending Sim Swap Success mail thru mandril
                if (In.Sitecode == "MCM")
                {
                    var custDetail = CRM_API.DB.Customer.SProc.CustomerDetail("VMUK", In.MsisdnOld).ToArray();
                    {
                        if (custDetail != null && !String.IsNullOrEmpty(custDetail.ElementAt(0).Email) && custDetail.ElementAt(0).Email != "-")
                        {
                            try
                            {
                                string firstName = "Customer";
                                if (!String.IsNullOrEmpty(custDetail.ElementAt(0).first_name) && custDetail.ElementAt(0).first_name != "-")
                                    firstName = custDetail.ElementAt(0).first_name;
                                var api = new Mandrill.MandrillApi(ConfigurationManager.AppSettings["MAILCHIMPAPIKEY"]);
                                var message = new MandrillMessage();
                                message.AddGlobalMergeVars("FNAME", firstName);
                                message.AddTo(custDetail.ElementAt(0).Email);
                                IList<MandrillSendMessageResponse> response = api.Messages.SendTemplateAsync(message, ConfigurationManager.AppSettings["MAILCHIMPSIMSWAPSUCCESSMAIL_" + In.Sitecode]).Result;
                                Log.Info("Sim Swap Mail Response : " + JsonConvert.SerializeObject(response));
                            }
                            catch (Exception ex)
                            {
                                Log.Error("Sim Swap Mail Error : " + ex.Message);
                            }
                        }
                        else
                        {
                            Log.Error("Sim Swap Mail Is is empty for : " + In.MsisdnOld);
                        }
                    }
                }
            }
            else
            {
                result.errsubject = "General Error";
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        private CRM_API.Models.Common.ErrCodeMsg Verify(CRM_API.Models.SimSwapSearch In)
        {
            var result = new CRM_API.Models.Common.ErrCodeMsg() { errcode = 0, errmsg = "", errsubject = "General Info" };
            _accinfo_Old = CRM_API.DB.ESP.SProc.GetESPAccountInfo(In.MsisdnOld, In.Sitecode).FirstOrDefault();
            _accinfo_New = CRM_API.DB.ESP.SProc.GetESPAccountInfo(In.MsisdnNew, In.Sitecode).FirstOrDefault();
            if (String.IsNullOrEmpty(In.MsisdnOld))
            {
                result.errcode = 1;
                result.errmsg += string.Format("{0} Required</br>", "Old Msisdn");
            }
            else if (String.IsNullOrEmpty(In.MsisdnNew))
            {
                result.errcode = 1;
                result.errmsg += string.Format("{0} Required</br>", "New Msisdn");
            }
            else if ((!String.IsNullOrEmpty(In.MsisdnOld) && !String.IsNullOrEmpty(In.MsisdnNew)) ? In.MsisdnNew == In.MsisdnOld : false)
            {
                result.errcode = 1;
                result.errmsg += string.Format("{0} Required Different From Old Msisdn</br>", "New Msisdn");
            }
            if (_accinfo_New == null)
            {
                result.errcode = 1;
                result.errmsg += string.Format("New Mobile Number Account Return Empty</br>");
            }
            else if (string.IsNullOrEmpty(_accinfo_New.ICCID))
            {
                result.errcode = 1;
                result.errmsg += string.Format("New Mobile Number ICCID Not Available</br>");
            }
            return result;
        }

        private HttpResponseMessage Validate(CRM_API.Models.SimSwapSearch In)
        {
            var _resAccount = CRM_API.DB.ESP.SProc.GetESPAccountInfo(In.MsisdnNew, In.Sitecode).FirstOrDefault();
            var result = new Models.Common.ErrCodeMsg() { errcode = 0, errmsg = "Valid", errsubject = "General Info" };
            if (In.NewIccidLastDigit != _resAccount.ICCID.Substring(_resAccount.ICCID.Trim().Length - 4).Trim())
            {
                result.errmsg = "Invalid";
                result.errcode = -1;
                result.errsubject = "General Error";
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
    }
}
