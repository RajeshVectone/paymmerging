﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CRM_API.Models.Payment;


namespace CRM_API.Controllers
{
    public class PaymentController : ApiController
    {
        // POST api/payment
        [HttpPost]
        public HttpResponseMessage Post(PaymentSearch Search)
        {
            DB.Common.ErrCodeMsg errResult = new DB.Common.ErrCodeMsg()
            {
                errcode = -1,
                errsubject = "General Error",
                errmsg = "Empty New History"
            };
            if (Search != null)
            {
                switch (Search.paymentSearchType)
                {
                    case Models.Payment.PaymentSearchType.GetCustomerWisePaymentBreakup:
                        return GetCustomerWisePaymentBreakup(Search);
                    case Models.Payment.PaymentSearchType.GetCustomerWisePaymentDetails:
                        return GetCustomerWisePaymentDetails(Search);
                    default:
                        break;
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.CustomerCSBHistory>().ToArray());
            }
            return Request.CreateResponse(HttpStatusCode.OK, errResult);
        }

        private HttpResponseMessage GetCustomerWisePaymentBreakup(PaymentSearch search)
        {
            IEnumerable<CustomerWisePaymentBreakupOutput> resValues = CRM_API.DB.Payment.GetCustomerWisePaymentBreakup(search.sitecode, search.searchvalue, search.date_from, search.date_to);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CustomerWisePaymentBreakupOutput>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        private HttpResponseMessage GetCustomerWisePaymentDetails(PaymentSearch search)
        {
            IEnumerable<CustomerWisePaymentDetailsOutput> resValues = CRM_API.DB.Payment.GetCustomerWisePaymentDetails(search.sitecode, search.searchby, search.searchvalue, search.date_from, search.date_to);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CustomerWisePaymentDetailsOutput>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }
    }
}
