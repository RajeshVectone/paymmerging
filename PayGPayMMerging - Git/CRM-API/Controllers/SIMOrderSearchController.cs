﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Script.Serialization;
using System.IO;
using System.Net.Mail;

namespace CRM_API.Controllers
{
    public class SIMOrderSearchController : ApiController
    {
        // Post api/simordersearch
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        [HttpGet]
        public dynamic Get([FromUri] CRM_API.Models.RefMisc Id, string Val = "", string Sitecode = "MCM")
        {
            Log.Debug("SIMOrderSearchController.Get: Started");
            Log.Info(new JavaScriptSerializer().Serialize(Id));
            Log.Info(new JavaScriptSerializer().Serialize(Val));
            Log.Info(new JavaScriptSerializer().Serialize(Sitecode));
            try
            {
                switch (Id)
                {
                    case Models.RefMisc.Status:
                        return CRM_API.DB.SIMOrder.SProc.ListOfStatus();

                    case Models.RefMisc.FreesimId:
                        return GetDetailOrder(Val, Sitecode);

                    case Models.RefMisc.SubOrder:
                        return GetSubOrderFreesim(Val, Sitecode);

                    default: throw new InvalidOperationException("Get Request Not Recognized");
                }
            }
            catch (Exception e)
            {
                Log.Error("SIMOrderSearchController.Get:" + e.Message);
                return Request.CreateResponse(HttpStatusCode.OK, new Models.Common.ErrCodeMsg() { errcode = -1, errmsg = e.StackTrace });
            }
        }

        [HttpPost]
        public async Task<HttpResponseMessage> Post(CRM_API.Models.SIMOrderRequest listRequest)
        {
            Log.Debug("SIMOrderSearchController.Post: Started");
            Log.Info(new JavaScriptSerializer().Serialize(listRequest));
            //dynamic result = null;
            try
            {
                if (listRequest != null)
                {
                    switch (listRequest.isDownload)
                    {
                        case true: return GetFile(listRequest);//await GetSimOrder_Downloadable(listRequest);
                        case false: return GetSimOrder(listRequest);
                        default: return GetSimOrder(listRequest);
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.PreconditionFailed, new List<CRM_API.Models.SIMOrderItem>());
                }
                //return Request.CreateResponse(HttpStatusCode.PreconditionFailed, new Models.Common.ErrCodeMsg() { errcode =-1, errmsg="Uncought Process." });
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new Models.Common.ErrCodeMsg() { errcode = -1, errmsg = e.Message });
            }
        }

        [HttpPut]
        public HttpResponseMessage Put(CRM_API.Models.SIMOrderItem Inx)
        {
            Log.Debug("SIMOrderSearchController.Put: Started");
            Log.Info(new JavaScriptSerializer().Serialize(Inx));
            bool isfreesim2in1;
            try
            {
                isfreesim2in1 = CheckIs2in1Order(Inx);
                var resx = CRM_API.DB.SIMOrder.SProc.ListOfStatus().Where(w => Inx.order_status.ToLower().Contains(w.Value.ToLower()));
                CheckInput(Inx, resx);
                var resy = resx.FirstOrDefault();
                int freesim_orderid;
                string freesim = Inx.freesim_order_id;

                if (isfreesim2in1)
                {
                    if (resy.Id == 4)
                        CRM_API.DB.SIMOrder.SProc.ValidateIccidInput(Inx.iccid, Inx.freesim_order_id, "M", Inx.Sitecode);
                    freesim_orderid = Int32.Parse(freesim.Substring(3, freesim.Length - 3));
                    var result = CRM_API.DB.SIMOrder.SProc.UpdateFreeSimOrder2in1(Inx.mimsi_order_id, freesim_orderid, resy.Value, Inx.Sitecode, Inx.logged_user, Inx.iccid, null, Inx.royal_mail_reference);
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }
                else
                {
                    if (resy.Id == 4)
                        CRM_API.DB.SIMOrder.SProc.ValidateIccidInput(Inx.iccid, Inx.freesim_order_id, "", Inx.Sitecode);
                    var M = CRM_API.DB.SIMOrder.SProc.SearchByOrderId(Inx.freesim_order_id.ToString(), "freesimid", Inx.Sitecode);

                    if (resy.Id == 9)
                    {
                        M.sitecode = Inx.Sitecode;
                        //M.iccid = Inx.iccid;
                        CRM_API.DB.Common.ErrCodeMsg errCodeMsg = CRM_API.DB.SIMOrder.SProc.UpdateFreeSimWithReplacement(M.sitecode, Inx.freesim_order_id, M.iccid, Inx.iccid, "CRM");
                        var result = new Models.Common.ErrCodeMsg() { errcode = errCodeMsg.errcode, errmsg = errCodeMsg.errmsg };
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    }
                    else
                    {
                        M.sitecode = Inx.Sitecode;
                        M.iccid = Inx.iccid;

                        if (CRM_API.DB.SIMOrder.SProc.UpdateFreeSimOrder(M, resy.Id, Inx.logged_user, Inx.royal_mail_reference))
                        {
                            /* Email sending sim dispatch  start */
                            string email = Inx.email;
                            string fullname = Inx.first_name;
                            string oredrid = Inx.freesim_order_id;
                            string trackingnumber = Inx.royal_mail_reference;
                            if (!string.IsNullOrEmpty(email))
                            {
                                Simdispatch(email, fullname, trackingnumber, oredrid);
                            }
                            /* Email sending sim dispatch  End */
                            var result = new Models.Common.ErrCodeMsg() { errcode = 0, errmsg = "Updated" };
                            return Request.CreateResponse(HttpStatusCode.OK, result);


                        }
                        else
                        {
                            var result = new Models.Common.ErrCodeMsg() { errcode = -1, errmsg = "Update Failed" };
                            return Request.CreateResponse(HttpStatusCode.OK, result);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log.Error("SIMOrderSearchController.Put:" + e.Message);
                return Request.CreateResponse(HttpStatusCode.OK, new Models.Common.ErrCodeMsg() { errcode = -1, errmsg = e.Message });
            }

            ///* Email sending sim dispatch  test function start */

            //string email = Inx.email;
            //string fullname = Inx.fullname;
            //string oredrid = Inx.freesim_order_id;
            //string trackingnumber = Inx.royal_mail_reference;
            //Simdispatch(email, fullname, trackingnumber, oredrid);
            //return Request.CreateResponse(HttpStatusCode.OK, fullname);

            ///* Email sending sim dispatch test function   End */
        }

        private static void CheckInput(CRM_API.Models.SIMOrderItem Inx, IEnumerable<Models.RefInfo> resx)
        {
            if (resx == null)
                throw new NullReferenceException("Order Status Not Recognized");
            if (resx.Count() > 1)
                throw new Exception("Duplicate Status Retrive Please Contact Your DB Admin");
            if (Inx.logged_user == null)
                throw new Exception("Please provide CS Username");
            if (string.IsNullOrEmpty(Inx.logged_user))
                throw new Exception("Empty Username CS, Please provide first");
        }

        private static bool CheckIs2in1Order(CRM_API.Models.SIMOrderItem Inx)
        {
            bool isfreesim2in1;
            if (Inx.freesim_order_id.ToString().Contains("M"))
                isfreesim2in1 = true;
            else
                isfreesim2in1 = false;

            if (Inx.mimsi_order_id == 0)
                isfreesim2in1 = false;
            else
                isfreesim2in1 = true;

            return isfreesim2in1;
        }

        private static Models.FreesimOrderItem GetDetailOrder(string Val, string Sitecode)
        {
            Models.FreesimOrderItem resx = null;
            if (Val.Contains("M"))
                resx = GetDetailOrder_Freesim2in1(Val.Substring(3), Sitecode);
            else
            {
                var result = CRM_API.DB.SIMOrder.SProc.SearchByOrderId(Val, "freesimid", Sitecode);
                if (Sitecode == "BNL")
                {
                    var resy = result.Fullname.Split(' ');
                    for (int i = 0; i < resy.Count(); i++)
                    {
                        if (i == 0)
                            result.firstname = resy[0];
                        else
                            result.lastname += resy[i] + ' ';
                    }
                }
                var resz = CRM_API.DB.SIMOrder.SProc.GetFreesimByMasterOrder("freesimid", Val, Sitecode);
                result.Quantity = resz != null ? resz.Count() : 0;
                return result;
            }
            return resx;
        }

        private static Models.FreesimOrderItem GetDetailOrder_Freesim2in1(string Val, string Sitecode)
        {
            Models.SIMOrderRequest requestParams = new Models.SIMOrderRequest() { order_id = int.Parse(Val) };
            CRM_API.Models.SIMOrderItem res = CRM_API.DB.SIMOrder.SProc.SearchByOrderId(requestParams);
            if (res.subscriber_id == 0)
                return null;
            else
            {
                Models.FreesimOrderItem resx = new Models.FreesimOrderItem()
                {
                    activatedate = res.activatedate,
                    Address = res.address1 + ' ' + res.address2,
                    Address1 = res.address1,
                    Address2 = res.address2,
                    city = res.town,
                    CrmCs = res.create_by,
                    CyberSourceId = res.cybersource_id,
                    email = res.email,
                    firstname = res.first_name,
                    freesimid = res.freesim_order_id,
                    freesimstatus = res.order_status,
                    FreeSimType = "FREESIM 2 in 1",
                    Fullname = res.first_name + ' ' + res.last_name,
                    iccid = res.iccid,
                    lastname = res.last_name,
                    mimsi_order_id = res.mimsi_order_id,
                    mobilephone = res.contact,
                    msisdn_referrer = "",
                    ordersim_url = res.ordersim_url,
                    postcode = res.postcode,
                    Quantity = 1,
                    registerdate = res.order_date,
                    sentdate = res.sentdate,
                    sitecode = Sitecode,
                    source_address = res.order_source,
                    SourceReg = res.source_reg,
                    stat_id = res.stat_id,
                    Subscriber_Type = res.subsciber_type,
                    subscriberid = res.subscriber_id,
                    telephone = res.mobileno,
                    title = res.title,
                    ipaddress = res.ipaddress
                };
                return resx;
            }
        }

        private static dynamic GetSubOrderFreesim(string Val, string Sitecode)
        {
            try
            {
                return CRM_API.DB.SIMOrder.SProc.GetFreesimByMasterOrder("freesimid", Val, Sitecode);
            }
            catch (Exception e)
            {
                Log.Error("GetSubOrderFreesim: " + e.Message);
                throw new Exception("GetSubOrderFreesim: " + e.Message);
            }
        }
        private HttpResponseMessage GetFile(CRM_API.Models.SIMOrderRequest listRequest)
        {
            try
            {
                var result = GetSimOrder_Downloadable_Generic(listRequest);
                StringBuilder str = new StringBuilder();
                str.Append("<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv=\"content-type\" content=\"text/plain; charset=UTF-8\"/></head><body>");
                str.Append("<table>");
                str.Append("<thead>");
                str.Append("<tr>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>FreeSimId</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Subscriber ID</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Title</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>First Name</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Last Name</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>House No</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Address</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>City</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Post Code</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Email</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Mobile No</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>ICCID</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Freesim Status</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Register Date</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Activate Date</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Sent Date</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Quantity</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Cybersource ID</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Source</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Source Address</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Ordersim URL</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>MSISDN Referrer</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Country you call most</th>");
                str.Append("</tr>");
                str.Append("</thead>");
                str.Append("<tbody>");
                foreach (CRM_API.Models.FreesimOrderItem val in result)
                {
                    str.Append("<tr>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>" + (string.IsNullOrEmpty(val.freesimid as string) ? "" : (val.freesimid as string)) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>" + val.subscriberid + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>" + (string.IsNullOrEmpty(val.title) ? "" : val.title) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>" + (string.IsNullOrEmpty(val.firstname) ? "" : val.firstname) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>" + (string.IsNullOrEmpty(val.lastname) ? "" : val.lastname) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>" + (string.IsNullOrEmpty(val.houseno) ? "" : val.houseno) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>" + (string.IsNullOrEmpty(val.Address) ? "" : val.Address) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>" + (string.IsNullOrEmpty(val.city) ? "" : val.city) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>" + (string.IsNullOrEmpty(val.postcode) ? "" : val.postcode) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>" + (string.IsNullOrEmpty(val.email) ? "" : val.email) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>" + (string.IsNullOrEmpty(val.mobilephone) ? "" : val.mobilephone) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>" + (string.IsNullOrEmpty(val.iccid) ? "" : val.iccid) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>" + (string.IsNullOrEmpty(val.freesimstatus) ? "" : val.freesimstatus) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>" + (string.IsNullOrEmpty(val.registerdate) ? "" : val.registerdate) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>" + (string.IsNullOrEmpty(val.activatedate) ? "" : val.activatedate) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>" + (string.IsNullOrEmpty(val.sentdate) ? "" : val.sentdate) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>" + val.Quantity + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>" + (string.IsNullOrEmpty(val.CyberSourceId) ? "" : val.CyberSourceId) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>" + (string.IsNullOrEmpty(val.SourceReg) ? "" : val.SourceReg) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>" + (string.IsNullOrEmpty(val.source_address) ? "" : val.source_address) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>" + (string.IsNullOrEmpty(val.ordersim_url) ? "" : val.ordersim_url) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>" + (string.IsNullOrEmpty(val.msisdn_referrer) ? "" : val.msisdn_referrer) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.fav_call_country) ? "" : val.fav_call_country) + "</td>");
                    str.Append("</tr>");
                }
                str.Append("</tbody>");
                str.Append("</table>");
                str.Append("</body></html>");
                HttpResponseMessage resx = new HttpResponseMessage(HttpStatusCode.OK);
                resx.Content = new StringContent(str.ToString(), Encoding.UTF8);
                resx.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                resx.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                resx.Content.Headers.ContentDisposition.FileName = "Freesim_" + DateTime.Now.Year.ToString() + ".xls";

                //HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=Freesim_" + DateTime.Now.Year.ToString() + ".xls");
                //this.Response.ContentType = "application/vnd.ms-excel";
                //byte[] temp = System.Text.Encoding.UTF8.GetBytes(str.ToString());
                //var temp = new System.IO.MemoryStream(System.Text.Encoding.UTF8.GetBytes(str.ToString()));
                //StreamWriter writer = new StreamWriter(temp, Encoding.UTF8);
                //return File(temp, "application/vnd.ms-excel");
                //this.Response.ContentType = "application/octet-stream";

                return resx;
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
        }

        private HttpResponseMessage GetSimOrder(CRM_API.Models.SIMOrderRequest listRequest)
        {
            if (string.IsNullOrEmpty(listRequest.searchby))
            {
                var dateFrom = DateTime.Parse(listRequest.fromdate);
                var dateUntil = DateTime.Parse(listRequest.todate);
                //dateUntil = dateUntil.AddDays(1);
                if (listRequest.status.ToLower() == "registerdateunprocessed")
                {
                    //result = CRM_API.DB.SIMOrder.SProc.GetUnprocessedOrder(dateFrom.ToString("yyyy-MM-dd"), dateUntil.ToString("yyyy-MM-dd"), listRequest.sitecode);
                    var resx = CRM_API.DB.SIMOrder.SProc.GetCombinedtUnprocessedFreesimOrder(dateFrom.ToString("yyyy-MM-dd"), dateUntil.ToString("yyyy-MM-dd"), listRequest.sitecode);
                    return Request.CreateResponse(HttpStatusCode.OK, resx as IEnumerable<Models.FreesimOrderItem>);
                }
                else
                {
                    var resy = CRM_API.DB.SIMOrder.SProc.GetCombinedFreesimOrder(listRequest.status, dateFrom.ToString("yyyy-MM-dd"), dateUntil.ToString("yyyy-MM-dd"), listRequest.sitecode);
                    return Request.CreateResponse(HttpStatusCode.OK, resy as IEnumerable<Models.FreesimOrderItem>);
                }
            }
            else
            {
                switch (listRequest.searchby.ToLower())
                {
                    case "freesimid":
                        if (listRequest.searchvalue.Contains("M"))
                        {
                            listRequest.order_id = int.Parse(listRequest.searchvalue.Substring(3));
                            return Request.CreateResponse(HttpStatusCode.OK, CRM_API.DB.SIMOrder.SProc.Search2in1Order(listRequest));
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, CRM_API.DB.SIMOrder.SProc.GetFreeSimByFreesimidGroupBySubscriberid(listRequest.searchvalue, listRequest.sitecode));
                        }
                    //var resx = ;
                    //var resx = CRM_API.DB.SIMOrder.SProc.GetFreeSimByFreesimidGroupBySubscriberid(listRequest.searchvalue, listRequest.sitecode);
                    //var resy = ((IEnumerable<Models.FreesimOrderItem>)resx).Union(CRM_API.DB.SIMOrder.SProc.Search2in1Order(listRequest));
                    //return Request.CreateResponse(HttpStatusCode.OK, resx as IEnumerable<Models.FreesimOrderItem>);
                    case "fullname":
                        listRequest.full_name = listRequest.searchvalue;
                        var resi = CRM_API.DB.SIMOrder.SProc.GetFreeSimByFullnameGroupBySubscriberid(listRequest.searchvalue, listRequest.sitecode);
                        // var resj = resi.Union(CRM_API.DB.SIMOrder.SProc.Search2in1Order(listRequest));
                        return Request.CreateResponse(HttpStatusCode.OK, resi as IEnumerable<Models.FreesimOrderItem>);
                    case "email":
                        listRequest.email = listRequest.searchvalue;
                        var resk = CRM_API.DB.SIMOrder.SProc.GetFreeSimByFullnameGroupBySubscriberidemail(listRequest.searchvalue, listRequest.sitecode);
                        //var resp = resk.Union(CRM_API.DB.SIMOrder.SProc.Search2in1Order(listRequest));
                        return Request.CreateResponse(HttpStatusCode.OK, resk.OrderByDescending(x => x.registerdate) as IEnumerable<Models.FreesimOrderItem>);
                    case "iccid":
                        listRequest.iccid = listRequest.searchvalue;
                        var resm = CRM_API.DB.SIMOrder.SProc.GetFreeSimByFullnameGroupBySubscriberidiccid(listRequest.searchvalue, listRequest.sitecode);
                        //var resn = resm.Union(CRM_API.DB.SIMOrder.SProc.Search2in1Order(listRequest));
                        return Request.CreateResponse(HttpStatusCode.OK, resm as IEnumerable<Models.FreesimOrderItem>);
                    case "cybersourceid":
                        listRequest.cybersourceid = listRequest.searchvalue;
                        var reso = CRM_API.DB.SIMOrder.SProc.GetFreeSimByFullnameGroupBySubscriberidcybersourceid(listRequest.searchvalue, listRequest.sitecode);
                        //var resq = reso.Union(CRM_API.DB.SIMOrder.SProc.Search2in1Order(listRequest));
                        return Request.CreateResponse(HttpStatusCode.OK, reso as IEnumerable<Models.FreesimOrderItem>);
                    case "mobileno":
                        listRequest.cybersourceid = listRequest.searchvalue;
                        var resMM = CRM_API.DB.SIMOrder.SProc.GetFreeSimByFullnameGroupBySubscriberidcybermobileno(listRequest.searchvalue, listRequest.sitecode);
                        //var resq = reso.Union(CRM_API.DB.SIMOrder.SProc.Search2in1Order(listRequest));
                        return Request.CreateResponse(HttpStatusCode.OK, resMM as IEnumerable<Models.FreesimOrderItem>);

                    default: throw new ArgumentNullException("Search Key Undifined");
                }
            }
        }
        private async Task<HttpResponseMessage> GetSimOrder_Downloadable(CRM_API.Models.SIMOrderRequest listRequest)
        {
            if (string.IsNullOrEmpty(listRequest.searchby))
            {
                var dateFrom = DateTime.Parse(listRequest.fromdate);
                var dateUntil = DateTime.Parse(listRequest.todate);
                //dateUntil = dateUntil.AddDays(1);
                //result = CRM_API.DB.SIMOrder.SProc.GetUnprocessedOrder(dateFrom.ToString("yyyy-MM-dd"), dateUntil.ToString("yyyy-MM-dd"), listRequest.sitecode);
                var result = await CRM_API.DB.SIMOrder.SProc.GetFreeSimForDownloadAsync(dateFrom.ToString("yyyy-MM-dd"), dateUntil.ToString("yyyy-MM-dd"), listRequest.status.ToLower(), listRequest.sitecode);
                //var result = CRM_API.DB.SIMOrder.SProc.GetFreeSimForDownload(dateFrom.ToString("yyyy-MM-dd"), dateUntil.ToString("yyyy-MM-dd"), listRequest.status.ToLower(), listRequest.sitecode);
                var resy = CRM_API.DB.SIMOrder.SProc.Get2in1AllOrder(listRequest.status, dateFrom.ToString("yyyy-MM-dd"), dateUntil.ToString("yyyy-MM-dd"), listRequest.sitecode);
                return Request.CreateResponse(HttpStatusCode.OK, result.Union(resy));
            }
            else
            {
                switch (listRequest.searchby.ToLower())
                {
                    case "freesimid":
                        if (listRequest.searchvalue.Contains("M"))
                        {
                            listRequest.order_id = int.Parse(listRequest.searchvalue.Substring(3));
                            return Request.CreateResponse(HttpStatusCode.OK, CRM_API.DB.SIMOrder.SProc.Search2in1Order(listRequest));
                        }
                        else
                            return Request.CreateResponse(HttpStatusCode.OK, CRM_API.DB.SIMOrder.SProc.
                                GetFreeSimForDownloadByFreesimid(listRequest.searchvalue, listRequest.sitecode));

                    //var resx = CRM_API.DB.SIMOrder.SProc.GetFreeSimForDownloadByFreesimid(listRequest.searchvalue, listRequest.sitecode);
                    //var resy = ((IEnumerable<Models.FreesimOrderItem>)resx).Union(CRM_API.DB.SIMOrder.SProc.Search2in1Order(listRequest));
                    //return Request.CreateResponse(HttpStatusCode.OK, resx as IEnumerable<Models.FreesimOrderItem>);
                    case "fullname":
                        listRequest.full_name = listRequest.searchvalue;
                        var resi = CRM_API.DB.SIMOrder.SProc.GetFreeSimForDownloadByFullname(listRequest.searchvalue, listRequest.sitecode);
                        var resj = resi.Union(CRM_API.DB.SIMOrder.SProc.Search2in1Order(listRequest));
                        return Request.CreateResponse(HttpStatusCode.OK, resi as IEnumerable<Models.FreesimOrderItem>);

                    default: throw new ArgumentNullException("Search Key Undifined");
                }
            }
        }

        private IEnumerable<CRM_API.Models.FreesimOrderItem> GetSimOrder_Downloadable_Generic(CRM_API.Models.SIMOrderRequest listRequest)
        {
            if (string.IsNullOrEmpty(listRequest.searchby))
            {
                var dateFrom = DateTime.Parse(listRequest.fromdate);
                var dateUntil = DateTime.Parse(listRequest.todate);
                //dateUntil = dateUntil.AddDays(1);
                //var result = CRM_API.DB.SIMOrder.SProc.GetUnprocessedOrder(dateFrom.ToString("yyyy-MM-dd"), dateUntil.ToString("yyyy-MM-dd"), listRequest.sitecode);
                //var result = await CRM_API.DB.SIMOrder.SProc.GetFreeSimForDownloadAsync(dateFrom.ToString("yyyy-MM-dd"), dateUntil.ToString("yyyy-MM-dd"), listRequest.status.ToLower(), listRequest.sitecode);
                var result = CRM_API.DB.SIMOrder.SProc.GetFreeSimForDownload(dateFrom.ToString("yyyy-MM-dd"), dateUntil.ToString("yyyy-MM-dd"), listRequest.status.ToLower(), listRequest.sitecode);
                var resy = CRM_API.DB.SIMOrder.SProc.Get2in1AllOrder(listRequest.status, dateFrom.ToString("yyyy-MM-dd"), dateUntil.ToString("yyyy-MM-dd"), listRequest.sitecode);
                return result.Union(resy);
            }
            else
            {
                switch (listRequest.searchby.ToLower())
                {
                    case "freesimid":
                        if (listRequest.searchvalue.Contains("M"))
                        {
                            listRequest.order_id = int.Parse(listRequest.searchvalue.Substring(3));
                            //return Request.CreateResponse(HttpStatusCode.OK, CRM_API.DB.SIMOrder.SProc.Search2in1Order(listRequest));
                            return CRM_API.DB.SIMOrder.SProc.Search2in1Order(listRequest);
                        }
                        else
                            //return Request.CreateResponse(HttpStatusCode.OK, CRM_API.DB.SIMOrder.SProc.
                            //    GetFreeSimForDownloadByFreesimid(listRequest.searchvalue, listRequest.sitecode));
                            return CRM_API.DB.SIMOrder.SProc.GetFreeSimForDownloadByFreesimid(listRequest.searchvalue, listRequest.sitecode);

                    //var resx = CRM_API.DB.SIMOrder.SProc.GetFreeSimForDownloadByFreesimid(listRequest.searchvalue, listRequest.sitecode);
                    //var resy = ((IEnumerable<Models.FreesimOrderItem>)resx).Union(CRM_API.DB.SIMOrder.SProc.Search2in1Order(listRequest));
                    //return Request.CreateResponse(HttpStatusCode.OK, resx as IEnumerable<Models.FreesimOrderItem>);
                    case "fullname":
                        listRequest.full_name = listRequest.searchvalue;
                        var resi = CRM_API.DB.SIMOrder.SProc.GetFreeSimForDownloadByFullname(listRequest.searchvalue, listRequest.sitecode);
                        var resj = resi.Union(CRM_API.DB.SIMOrder.SProc.Search2in1Order(listRequest));
                        //return Request.CreateResponse(HttpStatusCode.OK, resi as IEnumerable<Models.FreesimOrderItem>);
                        return resj as IEnumerable<CRM_API.Models.FreesimOrderItem>;

                    default: throw new ArgumentNullException("Search Key Undifined");
                }
            }
        }
        /* Email sim dispatch function start  */
        public bool Simdispatch(string email, string fullname, string trackingnumber, string orderid)
        {
            try
            {
                //email = "k.karthikeyan@mundio.com";
                string mailsubjectstart = "Your order ID ";
                string mailsubjectend = " has been dispatched.";
                string mailsubjectmiddle = orderid;
                string mailSubject = mailsubjectstart + mailsubjectmiddle + mailsubjectend;
                string mailLocation;
                mailLocation = System.Web.HttpContext.Current.Server.MapPath("~/Email_template/SIM_Dispatch.html").TrimEnd('/');
                StreamReader mailContent_file = new StreamReader(mailLocation);
                string mailContent = mailContent_file.ReadToEnd();
                mailContent_file.Close();
                mailContent = mailContent
                .Replace("[FullName]", fullname)
                .Replace("[trackingnumber]", trackingnumber);
                // Send the email
                MailAddress mailFrom = new MailAddress("noreply@vectonemobile.co.uk", "Vectone Mobile");
                MailAddressCollection mailTo = new MailAddressCollection();
                mailTo.Add(new MailAddress(email, string.Format("{0}", fullname)));
                return CRM_API.Helpers.EmailSending.Send(true, mailFrom, mailTo, null, null, mailSubject, mailContent);
            }
            catch (Exception ex)
            {
                Log.Error("SIMOrderSearchController.Simdispatch", ex.Message);
                return false;
            }
        }
    }
}