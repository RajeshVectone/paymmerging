﻿using CRM_API.DB.Common;
using CRM_API.DB.Complaint;
using CRM_API.Models.Issue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NLog;
using System.Net.Mail;
using System.Text;
using System.Net.Mime;
using System.Configuration;
using Mandrill;
using Mandrill.Model;
using Newtonsoft.Json;

namespace CRM_API.Controllers
{
    public class ComplaintsController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public HttpResponseMessage Post(CRM_API.Models.Complaint.ComplaintModels Model)
        {
            try
            {
                switch (Model.SearchType)
                {
                    case Models.Complaint.SearchType.ComplaintListAll:
                        return GetComplaintsByMobile(Model);
                    case Models.Complaint.SearchType.InsertComplaint:
                        return InsertComplaint(Model);
                    default: break;
                }
            }
            catch (Exception)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new CRM_API.Models.Common.ErrCodeMsg());
            }
            return Request.CreateResponse(HttpStatusCode.ExpectationFailed, Model);
        }

        private HttpResponseMessage GetComplaintsByMobile(CRM_API.Models.Complaint.ComplaintModels In)
        {
            var result = CRM_API.DB.Complaint.SProc.GetComplaintList(In.mobileno);
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        private HttpResponseMessage InsertComplaint(CRM_API.Models.Complaint.ComplaintModels In)
        {
            Log.Info("Complaints Input : " + JsonConvert.SerializeObject(In));
            try
            {
                var productcode = GetProductCode(In.sitecode);
                var result = CRM_API.DB.Complaint.SProc.InsertComplaint(In.customer_name, productcode, In.mobileno, In.issue_type, In.Issue_Function, In.Issue_Complaint, In.issue_status, In.descr, In.esclating_user, In.process_type, In.agent_userid, In.Ticket_id, In.priority_name);

                //TODO 
                if (result != null && result.Count() > 0 && result.ElementAt(0).errcode == 0)
                {
                    Log.Info("Complaints result : " + JsonConvert.SerializeObject(result));
                    string subject = "";
                    string MailBody = "";
                    if (In.process_type == 1)
                    {
                        subject = "New CRM Complaint Generated";
                        MailBody = "<html><body><table><tr>New Complaint created for : <tr/><tr><td>Mobile Number:</td><td>" + In.mobileno + "</td></tr><<tr><td> Ticket ID : </td><td>" + result.ElementAt(0).ticket_id + "</td><tr><tr>Refer <I>Reoprts --> Complaints Report</I> for tracking.</tr></table></body></html>";
                    }
                    else
                    {
                        subject = "CRM Complaint Updated";
                        MailBody = "Complaint updated for : " + In.mobileno + " , Ticket ID : " + result.ElementAt(0).ticket_id;
                    }
                    try
                    {
                        var api = new MandrillApi(ConfigurationManager.AppSettings["MAILCHIMPAPIKEY"]);
                        var message = new MandrillMessage();
                        message.FromEmail = ConfigurationManager.AppSettings["QUEUEEMAILMAILFROM"];
                        message.FromName = "CRM Complaints";
                        message.Subject = subject;
                        message.AddGlobalMergeVars("BodyText", MailBody);
                        message.Html = MailBody;
                        message.AutoHtml = true;
                        string[] emailto = In.email.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                        for (int iCount = 0; iCount < emailto.Count(); iCount++)
                        {
                            message.AddTo(emailto[iCount]);
                        }
                        IList<MandrillSendMessageResponse> response = api.Messages.SendAsync(message).Result;
                        Log.Info("Complaints Mail Response : " + JsonConvert.SerializeObject(response));
                    }
                    catch (Exception ex)
                    {
                        Log.Error("Complaints Mail : " + ex.Message);
                        Log.Error("Complaints Mail : " + ex.StackTrace);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new List<Models.Complaint.ComplaintModels>() { new Models.Complaint.ComplaintModels { errcode = -1, errmsg = ex.Message } });
            }
        }

        private string GetProductCode(string siteCode)
        {
            string productcode = "VMUK";
            if (siteCode == "MCM")
                productcode = "VMUK";
            else if (siteCode == "BAU")
                productcode = "VMAT";
            else if (siteCode == "MFR")
                productcode = "VMFR";
            else if (siteCode == "BNL")
                productcode = "VMNL";
            return productcode;
        }
    }
}
