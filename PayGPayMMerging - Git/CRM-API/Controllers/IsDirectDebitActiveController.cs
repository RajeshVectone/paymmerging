﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CRM_API.Controllers
{
    public class IsDirectDebitActiveController : ApiController
    {
        // GET api/isdirectdebitactive
        public HttpResponseMessage Get(string MobileNo, string ProductCode)
        {
            //TODO : Need to comment
            //return Request.CreateResponse(HttpStatusCode.OK, "N");
            try
            {
                //31-Jul-2015 : Moorthy Added
                System.Web.HttpContext.Current.Session["GlobalProduct"] = ProductCode;
                var result = CRM_API.DB.PMBODDOperations.IsDirectDebitActive(MobileNo);
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (WebException ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new List<CRM_API.Models.DirectDebitActiveMesg>());
            }

        }

        // GET api/isdirectdebitactive/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/isdirectdebitactive
        public void Post([FromBody]string value)
        {
        }

        // PUT api/isdirectdebitactive/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/isdirectdebitactive/5
        public void Delete(int id)
        {
        }
    }
}
