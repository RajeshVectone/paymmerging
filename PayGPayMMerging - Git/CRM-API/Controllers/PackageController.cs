﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace CRM_API.Controllers
{
    public class PackageController : ApiController
    {
        //
        // GET api/Package/

        /*public ActionResult Index()
        {
            return View();
        }*/


        // GET api/Package/
        [HttpPost]
        public HttpResponseMessage Post(CRM_API.Models.Package.PackageSearch resBundle)
        {
            var result = new CRM_API.Models.Package.PackageList();
            switch (resBundle.package_type)
            {
                case CRM_API.Models.Package.PackageType.GetPackageList:
                    return PackageList(resBundle);
                default: break;
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        HttpResponseMessage PackageList(CRM_API.Models.Package.PackageSearch resBundle)
        {
            IEnumerable<CRM_API.Models.Package.PackageList> resValues =
                           CRM_API.DB.Package.SProc.PackageList(resBundle.msisdn, resBundle.status,resBundle.sitecode);
            foreach (var item in resValues)
            {
                item.comment = CRM_API.DB.Package.SProc.InfoPackage(item.custcode, item.batchcode, item.serialcode, resBundle.sitecode, item.packageid);
            }
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.Package.PackageList>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

    }
}
