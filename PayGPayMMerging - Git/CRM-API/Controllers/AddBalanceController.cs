﻿using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using CRM_API.Models;
using CRM_API.DB;

namespace CRM_API.Controllers
{
    public class AddBalanceController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public async Task<HttpResponseMessage> Post(AddBalanceInput input)
        {
            try
            {
                AddBalanceOutput resultValues = AddBalance.UpdateBalance(input);
                return Request.CreateResponse(HttpStatusCode.OK, resultValues);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new Models.Common.ErrCodeMsg() { errcode = -1, errmsg = e.Message });
            }
        }
    }
}
