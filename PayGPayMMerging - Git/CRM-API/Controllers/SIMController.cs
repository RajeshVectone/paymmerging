﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CRM_API.Controllers
{
    public class SIMController : ApiController
    {
        //
        // GET api/SIM/
        #region Private Properties
        private string _cfbTo;
        private int _cfbType;
        private string _cfbuTo;
        private int _cfbuType;
        private string _cfnrcTo;
        private int _cfnrcType;
        private int _cfnrTime;
        private string _cfnryTo;
        private int _cfnryType;
        private int _dataType = 0;
        private string _errMsg;
        private string _iccid;
        private string _msisdn;
        private bool _isSms;
        private string _sitecode;
        #endregion

        public HttpResponseMessage Get(string id, string Sitecode)
        {
            IEnumerable<CRM_API.Models.SIM.SIMInfo> resValues =
                CRM_API.DB.SIMInfo.SProc.SIMInfo(id, Sitecode);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.SIM.SIMInfo>().ToArray());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }
        public HttpResponseMessage Post(CRM_API.Models.SIM.SimInfo_Request Model)
        {
            var result = new CRM_API.Models.SIM.SIMInfo();
            switch (Model.InfoType)
            {
                case Models.SIM.SimInfoType.SimInfo:
                    return GetSimInfo(Model);
                case Models.SIM.SimInfoType.CallFoward:
                    return GetCallFowardInfo(Model);
                case Models.SIM.SimInfoType.PinInfo:
                    return GetPinInfo(Model);
                default: break;
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [HttpPut()]
        public HttpResponseMessage Put(Models.SIM.SimInfoType Id, List<Models.SIM.CallFowardInfo> Model)
        {
            //var result = Model;
            var scode = CRM_API.DB.Sitecode.SProc.GetSiteInfo(Model.Select(s => s.Sitecode).FirstOrDefault());
            try
            {
                int Voice = 0;
                foreach (var item in Model)
                {


                    if (item.CFWStatus.ToString().ToUpper() == "Forwarded to Voice Mail")
                    {
                        Voice += 1;
                    }
                    item.errcode = 0;
                    item.errmsg = "Empty Action";
                    item.errsubject = "General Error";
                    _sitecode = item.Sitecode;
                    _iccid = item.Iccid;
                    _msisdn = item.MobileNo;
                    switch (item.eCFWType)
                    {
                        case Models.SIM.CallFowardType.CallForwardBusy:
                            _cfbType = (int)Models.SIM.CallFowardType.CallForwardBusy;
                            if (item.CFWStatus == Models.SIM.CallFowardStatus.EnabledDestNumber)
                                _cfbTo = "01;1;" + scode.CC + item.ForwardToNb;
                            else if (item.CFWStatus == Models.SIM.CallFowardStatus.EnableVoiceMail)
                                _cfbTo = "mail";
                            else
                                _cfbTo = string.Empty;
                            break;
                        case Models.SIM.CallFowardType.CallForwardNoReply:
                            _cfnryType = (int)Models.SIM.CallFowardType.CallForwardNoReply;
                            if (item.CFWStatus == Models.SIM.CallFowardStatus.EnabledDestNumber)
                            {
                                _cfnryTo = "01;1;" + scode.CC + item.ForwardToNb;
                                _cfnrTime = item.NoReplyTimer;
                            }
                            else if (item.CFWStatus == Models.SIM.CallFowardStatus.EnableVoiceMail)
                            {
                                _cfnryTo = "mail";
                                _cfnrTime = item.NoReplyTimer;
                            }
                            else
                                _cfnryTo = string.Empty;
                            break;
                        case Models.SIM.CallFowardType.CallForwardUnreachable:
                            _cfnrcType = (int)Models.SIM.CallFowardType.CallForwardUnreachable;
                            _isSms = item.SmsFlag == 0 ? false : true;
                            if (item.CFWStatus == Models.SIM.CallFowardStatus.EnabledDestNumber)
                                _cfnrcTo = "01;1;" + scode.CC + item.ForwardToNb;
                            else if (item.CFWStatus == Models.SIM.CallFowardStatus.EnableVoiceMail)
                                _cfnrcTo = "mail";
                            else
                                _cfnrcTo = string.Empty;
                            break;
                        case Models.SIM.CallFowardType.CallFowardUnconditional:
                            _cfbuType = (int)Models.SIM.CallFowardType.CallFowardUnconditional;
                            if (item.CFWStatus == Models.SIM.CallFowardStatus.EnabledDestNumber)
                                _cfbuTo = "01;1;" + scode.CC + item.ForwardToNb;
                            else if (item.CFWStatus == Models.SIM.CallFowardStatus.Disabled)
                            {
                                _cfbTo = string.Empty;
                                item.SmsFlag = 0;
                            }
                            else
                            {
                                _cfbuTo = "mail";
                                //_cfbuTo= string.Empty;
                                item.SmsFlag = 0;
                            }
                            var resx = CRM_API.DB.SIMInfo.SProc.ActivateUnconditionalCallFoward(item.Iccid, _cfbuTo, item.Sitecode, item.SmsFlag);
                            break;
                        default: break;
                    }

                }

                if (Voice > 0)
                {
                    string res = CallDisabled("1");
                }
                else
                {
                    string res = CallDisabled("2");
                }

                return DoSetCallFoward();
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new Models.Common.ErrCodeMsg() { errcode = -1, errmsg = ex.Message, errsubject = "General Error" });
            }
           
            //return Request.CreateResponse(HttpStatusCode.OK, result);

        }
        private string CallDisabled(string ind)
        {
            //var resultValues = new Models.Common.ErrCodeMsg();
            string Returnval = "";
            Returnval = CRM_API.DB.SIMInfo.SProc.CallDisabled(_sitecode, _msisdn, ind);

            if (Returnval.ToLower() == "success")
                 {
                     Returnval = "success";
                 }
                 return Returnval;
        }
        private HttpResponseMessage DoSetCallFoward()
        {
            var resultValues = new Models.Common.ErrCodeMsg();
            var resx = CRM_API.DB.SIMInfo.SProc.CallFowardUpdate(_iccid, _cfbType, _cfbTo, _cfnryType, _cfnryTo, _cfnrcType, _cfnrcTo, _cfnrTime, ref _errMsg, ref _dataType, _sitecode);
            if (resx.err_message.ToLower() == "success")
            {
                resultValues.errmsg = resx.err_message;
                resultValues.errsubject = "General Information";
                resultValues.errcode = 0;
                if (_isSms)
                    resx = CRM_API.DB.SIMInfo.SProc.ActivateNotReachableSMS(_iccid, _cfnrcTo, _cfnrcType, _sitecode);
                else
                    resx = CRM_API.DB.SIMInfo.SProc.DeActivateNotReachableSMS(_iccid, _cfnrcType, _sitecode);
                //CRMT-262 Validate mobile number valid or not 
                CRM_API.Models.Common.ErrCodeMsg check = CRM_API.DB.SIMInfo.SProc.Get_Validatemobile(_sitecode, _msisdn);
                if (check.errcode == 0)
                {

                    //var res = CRM_API.DB.SIMInfo.SProc.DeActivateNotReachableSMS(_iccid, _cfnrcType, _sitecode);
                    //                    var Location = CRM_API.DB.SIMInfo.SProc.GetFreeSimByFullnameGroupBycancel(_iccid, _sitecode, _dataType);

                    var Location = CRM_API.DB.SIMInfo.SProc.GetFreeSimByFullnameGroupBycancel(_iccid, _sitecode, 1);

                    if (Helpers.HLR.USSDSend1(Location.imsi_active, Location.vlr_number, Location.url))
                    //if (Helpers.HLR.USSDSend(_msisdn, _dataType.ToString(), _sitecode))  //  Locked as per logu  2016/03/16 
                    {
                        resx.errcode = 0;
                        resx.errmsg = "Call Forward settings have been updated in Our System (HLRDB).<br>";
                        resx.errmsg += "Sending Call Forward Data to Subscriber location: OK for:" + _msisdn;
                    }
                    else
                    {
                        resx.errcode = -1;
                        resx.errmsg = "Failure sending Call Forward Data to Subscriber location, but Call Forward settings have been updated in Our System (HLRDB).<BR>";
                        resx.errmsg += "Please contact NOC to trace down." + Helpers.HLR.SipServerUssd;
                    }
                }
                else
                {
                    resx.errcode = check.errcode;
                    resx.errmsg = check.errmsg;
                    resx.errsubject = check.errsubject;
                }

                resultValues = (Models.Common.ErrCodeMsg)resx;
            }
            return Request.CreateResponse(HttpStatusCode.OK, resultValues);
        }

        private HttpResponseMessage GetCallFowardInfo(CRM_API.Models.SIM.SimInfo_Request Model)
        {
            var resultValues = CRM_API.DB.SIMInfo.SProc.CallFowardInfo(Model.Iccid, Model.Sitecode);
            var countrycode = CRM_API.DB.Sitecode.SProc.GetSiteInfo(Model.Sitecode);
            foreach (var item in resultValues)
            {
                item.CountryCode = countrycode.CC;
            }
            return Request.CreateResponse(HttpStatusCode.OK, resultValues);
        }

        private HttpResponseMessage GetPinInfo(CRM_API.Models.SIM.SimInfo_Request Model)
        {
            var resultValues = CRM_API.DB.SIMInfo.SProc.SIMPinInfo(Model.Iccid, Model.Sitecode);
            return Request.CreateResponse(HttpStatusCode.OK, resultValues);
        }

        private HttpResponseMessage GetSimInfo(CRM_API.Models.SIM.SimInfo_Request Model)
        {
            CRM_API.Models.SIM.SIMInfo resultValues = CRM_API.DB.SIMInfo.SProc.SIMInfo(Model.Msisdn, Model.Sitecode).FirstOrDefault();
            if (resultValues != null)
                resultValues.pinInfo = CRM_API.DB.SIMInfo.SProc.SIMPinInfo(Model.Iccid, Model.Sitecode).FirstOrDefault();
            resultValues.reasoninfo = CRM_API.DB.SIMInfo.SProc.SIMReasoninfo(Model.Msisdn, Model.Sitecode).FirstOrDefault();
            resultValues.SIMVersion = CRM_API.DB.SIMInfo.SProc.SIMVersion(Model.Iccid, Model.Sitecode).FirstOrDefault();
            return Request.CreateResponse(HttpStatusCode.OK, resultValues);
        }
        private HttpResponseMessage UpdateCallFoward(Models.SIM.CallFowardInfo In)
        {
            var resultValues = CRM_API.DB.SIMInfo.SProc.CallFowardUpdate(_iccid, _cfbType, _cfbTo, _cfnryType, _cfnryTo, _cfnrcType, _cfnrcTo, _cfnrTime, ref _errMsg, ref _dataType, _sitecode);
            return Request.CreateResponse(HttpStatusCode.OK, resultValues);
        }
    }

}
