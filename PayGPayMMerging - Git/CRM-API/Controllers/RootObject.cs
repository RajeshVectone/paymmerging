﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CRM_API.DB.Ref.Country;

namespace CRM_API.Controllers
{
    public class RootObject
    {
        public string MobileNo { get; set; }
        public string IccId { get; set; }
        public string Imei { get; set; }
        public int Autotopup { get; set; }
        public int TopupThreshold { get; set; }
        public int PayType { get; set; }
        public int Balance { get; set; }
        public string CurrCode { get; set; }
        public string TrffClass { get; set; }
        public int Status { get; set; }
        public int ActivateId { get; set; }
        public int SerialFr { get; set; }
        public int SerialTo { get; set; }
        public int SellerId { get; set; }
        public object SubscriptionId { get; set; }
        public string ProductCode { get; set; }
        public Countryb Country { get; set; }
    }
}
