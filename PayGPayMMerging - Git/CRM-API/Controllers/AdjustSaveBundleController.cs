﻿using CRM_API.Models; 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CRM_API.Controllers
{
    public class AdjustSaveBundleController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage Post(CRM_API.Models.AdjustBundleSaveModelsInput input)
        {
            CRM_API.Models.AdjustBundleSaveModelsOutput resValues = CRM_API.DB.UpdateAdjustBundle.UpdateBundle(input);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new AdjustBundleSaveModelsOutput());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues);
        }


    }
}
