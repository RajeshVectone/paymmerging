﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CRM_API.Controllers
{
    public class SendCollectFailureSMSController : ApiController
    {
        // GET api/sendcollectfailuresms
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/sendcollectfailuresms/5
        public HttpResponseMessage Get(int UploadId)
        {
            try
            {
                var result = CRM_API.DB.PMBODDOperations.GetCollectionFailureMobileNumbers(UploadId);
                List<CRM_API.Models.SMSMobileNos> MobNoList = result.ToList();
                //LiveOnly
                for (int i = 0; i < MobNoList.Count; i++)
                {
                    SMS_Payment_Failure(MobNoList[i].MobileNo.Trim());
                }
                //SMS_Payment_Failure("447465187105");
                return Request.CreateResponse(HttpStatusCode.OK, "SMS successfully sent to " + MobNoList.Count.ToString() + " customers.");
            }
            catch (WebException ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new List<CRM_API.Models.SMSMobileNos>());
            }
        }

        // POST api/sendcollectfailuresms
        public void Post([FromBody]string value)
        {
        }

        // PUT api/sendcollectfailuresms/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/sendcollectfailuresms/5
        public void Delete(int id)
        {
        }

        public bool SMS_Payment_Failure(string mobileNo)
        {
            try
            {
                string PAYMTestMobileNo = System.Configuration.ConfigurationManager.AppSettings["PAYMTestMobileNo"];
                if (!string.IsNullOrEmpty(PAYMTestMobileNo))
                    mobileNo = PAYMTestMobileNo;

                //string smsText = string.Format("Your last Payment was UnSuccessful on {1:dd-MM-yyyy}", newplan, start_date);
                string smsText = string.Format("Your last Payment was UnSuccessful! Please Pay it immediately!");
                string[] res = new Helpers.SendSMS().Send(false, mobileNo, "111", smsText, 0);
                if (res != null && res.Count() > 0)
                {
                    return !(res[0] == "-1");
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                //Log.Error("PAYMPlanBundlesController.SMSChangePlan", ex.Message);
                return false;
            }
        }
    }
}
