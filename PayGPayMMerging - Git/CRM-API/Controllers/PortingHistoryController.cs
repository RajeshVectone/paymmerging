﻿using CRM_API.Models.PortingModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CRM_API.Controllers
{
    public class PortingHistoryController : ApiController
    {
        public HttpResponseMessage Post(CRM_API.Models.CustomerOverviewPageInput input)
        {
            System.Web.HttpContext.Current.Session["GlobalProduct"] = input.Product_Code;


            IEnumerable<PortingHistoryModelOutput> resValues = CRM_API.DB.Porting.PortingDetails.GetPortingHistoryDetails(input.MobileNo, input.Sitecode);
            if (resValues == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new List<PortingHistoryModelOutput>());
            else
                return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }


    }
}
