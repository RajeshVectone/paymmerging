﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CRM_API.Models;
using NLog;

namespace CRM_API.Controllers
{
    public class DocsUploadController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public HttpResponseMessage Post()
        {
            try
            {
                int category_id = Convert.ToInt32(System.Web.HttpContext.Current.Request.Form["category_id"]);
                string filename = System.Web.HttpContext.Current.Request.Form["filename"];
                string upload_by = System.Web.HttpContext.Current.Request.Form["upload_by"];
                int doc_id = Convert.ToInt32(System.Web.HttpContext.Current.Request.Form["doc_id"]);
                int processtype = Convert.ToInt32(System.Web.HttpContext.Current.Request.Form["processtype"]);
                string product_name = System.Web.HttpContext.Current.Request.Form["product_name"];

                System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
                if (hfc != null && hfc.Count > 0)
                {
                    System.Web.HttpPostedFile hpf = hfc[0];
                    string fullfilename = Path.Combine(ConfigurationManager.AppSettings["UPLOADFILEPATH"], filename + Path.GetExtension(hpf.FileName));
                    if (!File.Exists(fullfilename))
                    {
                        hpf.SaveAs(fullfilename);
                        IEnumerable<CRM_API.Models.Docs.DocModelCommon> resValues = CRM_API.DB.Docs.InsertDocUploadInfo(category_id, filename, Path.GetFileName(fullfilename), upload_by, doc_id, processtype, product_name);
                        if (resValues == null)
                            return Request.CreateResponse(HttpStatusCode.OK, new List<CRM_API.Models.Docs.DocModelCommon>() { new CRM_API.Models.Docs.DocModelCommon { errcode = -1, errmsg = "Failure" } }.ToArray());
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
                        }
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, new List<CRM_API.Models.Docs.DocModelCommon>() { new CRM_API.Models.Docs.DocModelCommon { errcode = -1, errmsg = "File already exists!" } }.ToArray());
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new List<CRM_API.Models.Docs.DocModelCommon>() { new CRM_API.Models.Docs.DocModelCommon { errcode = -1, errmsg = "Failure" } }.ToArray());
                }
            }
            catch (Exception ex)
            {
                Log.Error("DocsUploadController : Error : " + ex.Message);
                return Request.CreateResponse(HttpStatusCode.OK, new List<CRM_API.Models.Docs.DocModelCommon>() { new CRM_API.Models.Docs.DocModelCommon { errcode = -1, errmsg = ex.Message } }.ToArray());
            }
        }
    }
}
