﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CRM_API.Controllers
{
    /// <summary>
    /// PAYM Payment History
    /// api/PAYMPaymentHistory
    /// </summary>
    public class PAYMPaymentHistoryController : ApiController
    {
        /// <summary>
        /// PAYM Get the Pyment History
        /// api/PAYMPaymentHistory
        /// </summary>
        public HttpResponseMessage Get(string MobileNo, string ProductCode)
        {
            try
            {
                //CRM Enhancement 2
                string SiteCode = "MCM";
                if (ProductCode == "VMFR")
                    SiteCode = "MFR";
                else if (ProductCode == "VMAT")
                    SiteCode = "BAU";
                else if (ProductCode == "VMNL")
                    SiteCode = "BNL";
                else if (ProductCode == "VMBE")
                    SiteCode = "MBE";
                //31-Jul-2015 : Moorthy Added
                System.Web.HttpContext.Current.Session["GlobalProduct"] = ProductCode;
                var result = CRM_API.DB.PAYMPaymentHistroy.getPaymentDetails(MobileNo, SiteCode);
                return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
            }
            catch (WebException ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new List<CRM_API.Models.PaymPaymentHistroy>());
            }
        }
    }
}
