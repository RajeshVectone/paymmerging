﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;

namespace CRM_API.Controllers
{
    public class DDViewForProcessFailureController : ApiController
    {

        // GET api/ddviewforprocessfailure
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        public HttpResponseMessage Get(string ddYear, string ddMonth, string ddStatus, int? ddCustID, string ddPaymtStatus, string ddMobileNo, string ddConnStatus, string ddPaymentRef, string ddPayMode, string sitecode)
        {
            //  CRM_API.Models.ManualMarking values = new CRM_API.Models.ManualMarking();
            var result = CRM_API.DB.PMBODDOperations.GetDataForViewNProcessFailure(ddYear, ddMonth, ddStatus, ddCustID, ddPaymtStatus, ddMobileNo, ddConnStatus, ddPaymentRef, ddPayMode, sitecode);
                return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
               // return Request.CreateResponse(HttpStatusCode.OK, result.ToList());
        }
        // GET api/ddviewforprocessfailure/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        // POST api/ddviewforprocessfailure
        //public void Post([FromBody]string value)
        //{
        //}
        public async Task<HttpResponseMessage> Post(CRM_API.Models.DDViewNProcessFailureParam listRequest)
        {
            try
            {
                var resx = CRM_API.DB.PMBODDOperations.GetDataForViewNProcessFailure(listRequest.ddYear, listRequest.ddMonth, listRequest.ddStatus, listRequest.ddCustID, listRequest.ddPaymtStatus, listRequest.ddMobileNo, listRequest.ddConnStatus, listRequest.ddPaymentRef, listRequest.ddPayMode,"");
                return Request.CreateResponse(HttpStatusCode.OK, resx);

            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new Models.Common.ErrCodeMsg() { errcode = -1, errmsg = e.Message });
            }
        }
        
        // PUT api/ddviewforprocessfailure/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/ddviewforprocessfailure/5
        public void Delete(int id)
        {
        }
    }
}
