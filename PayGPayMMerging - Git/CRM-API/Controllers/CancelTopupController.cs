﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Xml;
using CRM_API.DB;
using CRM_API.DB.BreakageUsage;
using CRM_API.Models;
using CRM_API.Models.BreakageUsageModels;
using Mandrill;
using Mandrill.Model;
using Newtonsoft.Json;
using NLog;

namespace CRM_API.Controllers
{
    public class CancelTopupController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public async Task<HttpResponseMessage> Post(CancelTopupInput Model)
        {
            try
            {
                switch (Model.CancelModeType)
                {
                    case CancelTopupReportModeType.INSERTCANCELTOPUP:
                        return await InsertCancelTopup(Model);
                    case CancelTopupReportModeType.GETCANCELTOPUPREPORT:
                        return GetCancelTopupReport(Model);
                    case CancelTopupReportModeType.UPDATECANCELTOPUP:
                        return await UpdateCancelTopup(Model);
                    default:
                        return Request.CreateResponse(HttpStatusCode.OK, new List<CancelTopupOutput> { new CancelTopupOutput() { errcode = -1, errmsg = "Not Found" } });
                }

            }
            catch (WebException ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new List<CancelTopupOutput> { new CancelTopupOutput() { errcode = -1, errmsg = ex.Message } });
            }
        }

        private HttpResponseMessage GetCancelTopupReport(CancelTopupInput Model)
        {
            List<CancelTopupReportOutput> resultValues = CancelTopup.GetCancelTopupReport(Model).ToList();
            return Request.CreateResponse(HttpStatusCode.OK, resultValues.ToArray());
        }

        private async Task<HttpResponseMessage> InsertCancelTopup(CancelTopupInput Model)
        {
            CancelTopupOutput resultValues = CancelTopup.InsertCancelTopup(Model);
            //if(resultValues != null && resultValues.errcode == 0){
            //    var api = new MandrillApi(ConfigurationManager.AppSettings["MAILCHIMPAPIKEY"]);
            //    var message = new MandrillMessage(ConfigurationManager.AppSettings["QUEUEEMAILMAILFROM"], ConfigurationManager.AppSettings["CANCELTOPUPEMAILTO"],
            //                    ConfigurationManager.AppSettings["CANCELTOPUPEMAILSUBJECT"], Convert.ToString(ConfigurationManager.AppSettings["CANCELTOPUPEMAILBODY"]).Replace("{REFERENCEID}", Model.paymentRef).Replace("{MOBILENO}", Model.mobileno).Replace("{REASON}", Model.Reason));

            //    var result = await api.Messages.SendAsync(message);
            //    Log.Info("InsertCancelTopup : Response : " + JsonConvert.SerializeObject(result));
            //    return Request.CreateResponse(HttpStatusCode.OK, resultValues);
            //}
            return Request.CreateResponse(HttpStatusCode.OK, resultValues);
        }

        private async Task<HttpResponseMessage> UpdateCancelTopup(CancelTopupInput Model)
        {
            CancelTopupOutput resultValues = CancelTopup.UpdateCancelTopup(Model);
            if (resultValues != null && resultValues.errcode == 0)
            {
                var api = new MandrillApi(ConfigurationManager.AppSettings["MAILCHIMPAPIKEY"]);
                var message = new MandrillMessage(ConfigurationManager.AppSettings["QUEUEEMAILMAILFROM"], ConfigurationManager.AppSettings["CANCELTOPUPEMAILTO"],
                                ConfigurationManager.AppSettings["CANCELTOPUPEMAILSUBJECT"], Convert.ToString(ConfigurationManager.AppSettings["CANCELTOPUPEMAILBODY"]).Replace("{REFERENCEID}", Model.paymentRef).Replace("{MOBILENO}", Model.mobileno).Replace("{REASON}", Model.Reason));

                var result = await api.Messages.SendAsync(message);
                Log.Info("InsertCancelTopup : Response : " + JsonConvert.SerializeObject(result));
                return Request.CreateResponse(HttpStatusCode.OK, resultValues);
            }
            return Request.CreateResponse(HttpStatusCode.OK, resultValues);
        }

    }
}
