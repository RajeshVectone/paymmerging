﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml;
using CRM_API.DB;
using CRM_API.DB.BreakageUsage;
using CRM_API.Models;
using CRM_API.Models.BreakageUsageModels;

namespace CRM_API.Controllers
{
    public class AddBalancereportController : ApiController
    {
        public HttpResponseMessage Post(AddBalanceReportInput Model)
        {
            try
            {
                List<AddBalanceReportOutput> resultValues = AddBalance.GetAddBalanceReport(Model).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, resultValues.ToArray());
            }
            catch (WebException ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new List<BreakageUsageOutput>());
            }
        }

    }
}
