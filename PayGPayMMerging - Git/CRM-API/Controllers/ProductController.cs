﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Filters;
using System.Web.Mvc;

namespace CRM_API.Controllers
{
   
    public class ProductController : ApiController
    {
        //
        //
        public HttpResponseMessage Get()
        {
            IList<Models.ProductModel> result = new List<Models.ProductModel>();
            try
            {
                result = CRM_API.DB.Product.ProductSProc.GetAllProducts().ToList();
            }
            catch (Exception)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, result);
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

      
       
    }

   

}
