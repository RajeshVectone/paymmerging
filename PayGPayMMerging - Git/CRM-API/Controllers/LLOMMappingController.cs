﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml;

namespace CRM_API.Controllers
{
    public class LLOMMappingController : ApiController
    {
        //
        // GET: /LLOMMapping/

        public HttpResponseMessage get(string llomno, string vectoneno)
        {
            try
            {
                var result = CRM_API.DB.viewtransaction.lottmmaping(llomno, vectoneno);
                return Request.CreateResponse(HttpStatusCode.OK, result.ToArray());
            }
            catch (WebException ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new List<CRM_API.Models.financeviewtrans>());
            }
        }

    }
}
