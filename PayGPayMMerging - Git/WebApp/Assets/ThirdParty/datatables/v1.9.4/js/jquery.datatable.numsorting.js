 jQuery.fn.dataTableExt.oSort['num-html-asc'] = function (a, b) {
        var x = a.replace(/<.*?>/g, "");
        var y = b.replace(/<.*?>/g, "");
        x = parseFloat(x);
        if (isNaN(x)) { x = -1; };
        y = parseFloat(y);
        if (isNaN(y)) { y = -1; };
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    };

    jQuery.fn.dataTableExt.oSort['num-html-desc'] = function (a, b) {
        var x = a.replace(/<.*?>/g, "");
        var y = b.replace(/<.*?>/g, "");
        x = parseFloat(x);
        if (isNaN(x)) { x = -1; };
        y = parseFloat(y);
        if (isNaN(y)) { y = -1; };
        return ((x < y) ? 1 : ((x > y) ? -1 : 0));
    };