﻿function FillCategory(fillType) {
    $("#ajax-screen-masking").show();
    $('#ddlCategory').html('');
    var url = apiServer + '/api/Docs';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 2
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            var actionOption = '';
            if (fillType == 0)
                actionOption = '<option value="0" style=\"padding:3px\">-- All --</option>';
            else
                actionOption = '<option value="-1" style=\"padding:3px\">-- Select --</option>';
            if (feedback != null && feedback.length > 0) {
                for (icount = 0; icount < feedback.length; icount++) {
                    actionOption += '<option style=\"padding:3px\" value="' + feedback[icount].pk_category_id + '">' + feedback[icount].category_name + '</option>';
                }

            }
            $('#ddlCategory').append(actionOption);
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}

function Search(category_id, is_archive_flag, product_name) {
    $("#documentContainerBody").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblDocumentResult\"></table>");
    $("#documentContainerBody").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var url = apiServer + '/api/Docs';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 1,
        category_id: category_id,
        is_archive_flag: is_archive_flag,
        product_name: product_name
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#documentContainerBody").css({ "background-image": "none" });
            $('#tblDocumentResult').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                iDisplayLength: 10,
                bPaginate: true,
                bFilter: true,
                bInfo: false,
                bAutoWidth: false,
                aaSorting: [],
                aoColumns: [
                    //{ mDataProp: "pk_doc_id", sTitle: "ID", sWidth: 50 },
                    { mDataProp: "category_name", sTitle: "Category Name", sWidth: 100 },
                    {
                        mDataProp: "filename", sTitle: "Filename", sWidth: 100,
                        fnRender: function (ob) {
                            var actionURL = "<a href='" + ob.aData.fullpath + "');' target='_blank'>" + ob.aData.filename + "</a>";
                            return actionURL;
                        }
                    },
                    //{ mDataProp: "filepath", sTitle: "Filepath", sWidth: 75 },
                    { mDataProp: "upload_date_str", sTitle: "Upload Date", sWidth: 75 },
                    { mDataProp: "upload_by", sTitle: "Upload By", sWidth: 150 },
                    { mDataProp: "last_update_str", sTitle: "Last Update.", sWidth: 50 },
                    {
                        mDataProp: "Action", sTitle: "Action", sWidth: 50,
                        fnRender: function (ob) {
                            if (is_archive_flag == 0) {
                                //var actionURL = "<a class='btn blue' href='" + ob.aData.fullpath + "');' target='_blank' style='margin-right: 15px;'>View</a>";
                                var actionURL = "<a  id='btnDownload' title='Download' onClick='DownloadDocFile(\"" + ob.aData.filepath + "\");' style='cursor: pointer;'><img src='../Assets/Main/img/download.png'></img></a>";
                                if ($("#hdroleid").val() == "3" || $("#hdroleid").val() == "4" || $("#hdroleid").val() == "5") {
                                    //actionURL += "<a  id='btnDelete' title='Delete' onClick='UpdateDocFile(\"" + ob.aData.pk_doc_id + "\",3);' style='cursor: pointer;margin-left: 20px;'><img src='../Assets/Main/img/delete.png'></img></a>";
                                    actionURL += "<a  id='btnArchive' title='Archive' onClick='UpdateDocFile(\"" + ob.aData.pk_doc_id + "\",4);' style='cursor: pointer;margin-left: 20px;'><img src='../Assets/Main/img/archive.png'></img></a>";
                                }
                                return actionURL;
                            }
                            else {
                                var actionURL = "<a  id='btnDownload' title='Download' onClick='DownloadDocFile(\"" + ob.aData.filepath + "\");' style='cursor: pointer;'><img src='../Assets/Main/img/download.png'></img></a>";
                                if ($("#hdroleid").val() == "3" || $("#hdroleid").val() == "4" || $("#hdroleid").val() == "5") {
                                    actionURL = "<a  id='btnRestore' title='Restore' onClick='UpdateDocFile(\"" + ob.aData.pk_doc_id + "\",5);' style='cursor: pointer;'><img src='../Assets/Main/img/restore.png'></img></a>";
                                }
                                return actionURL;
                            }
                        }
                    },
                    //{
                    //    mDataProp: "Action2", sTitle: "Action2", sWidth: 100,
                    //    fnRender: function (ob) {
                    //        var actionURL = "<button  id='btnDownload' class='btn blue' onClick=DownloadDocFile('" + ob.aData.filepath + "');>Download</button>";
                    //        return actionURL;
                    //    }
                    //},
                ],
                fnDrawCallback: function () {
                    $("#documentContainerBody tbody td").css({ "font-size": "12px" });
                }
            });
        },
        error: function (feedback) {
            $("#documentContainerBody").css({ "background-image": "none" }).html("").append("No Docs found").show();
        }
    });
}

function CreateDocCategory(category_id, category_name, process_type) {
    $("#ajax-screen-masking").show();
    var url = apiServer + '/api/Docs';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 3,
        category_id: category_id,
        category_name: category_name,
        process_type: process_type
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            if (feedback != null && feedback.length > 0) {
                if (feedback[0].errcode == 0) {
                    alert(feedback[0].errmsg);
                    $("#CategoryPopup").modal("hide");
                    FillCategory(1);
                }
                else {
                    $("#lblPopupCategoryError").html(feedback[0].errmsg);
                }
            }
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}

function InsertDocUploadInfo(category_id, file, filename, upload_by, doc_id, processtype, product_name) {
    $("#lblUploadError").html("");
    $("#ajax-screen-masking").show();
    var formData = new FormData();
    formData.append("file", file);
    formData.append("category_id", category_id);
    formData.append("filename", filename);
    formData.append("upload_by", upload_by);
    formData.append("doc_id", doc_id);
    formData.append("processtype", processtype);
    formData.append("product_name", product_name);
    var url = apiServer + '/api/DocsUpload';
    jQuery.support.cors = true;
    $.ajax
    ({
        url: url,
        type: 'post',
        data: formData,
        contentType: false, // Not to set any content header    
        processData: false, // Not to process data 
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            if (feedback != null && feedback.length > 0) {
                if (feedback[0].errcode == 0) {
                    alert(feedback[0].errmsg);
                    $('#ddlCategory').val("-1");
                    $("#txtDocFileName").val("");
                    $('#pdfFile').val("");
                }
                else {
                    $("#lblUploadError").html(feedback[0].errmsg);
                }
            }
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}

function DownloadDocFile(filename) {
    var IFrameData = $('<iframe/>', { id: 'downloadIFrame', src: '/Download/DownloadDocFile?id=' + filename, style: 'display:none' });
    $("#ajax-screen-masking").append(IFrameData);
}

function UpdateDocFile(doc_id, processtype) {
    var title = "delete"
    if (processtype == 3)
        title = "delete";
    else if (processtype == 4)
        title = "archive";
    else
        title = "restore";

    if (confirm("Are you sure you want to " + title + " this document?")) {
        $("#ajax-screen-masking").show();
        var url = apiServer + '/api/Docs';
        jQuery.support.cors = true;
        var JSONSendData = {
            modetype: 5,
            doc_id: doc_id,
            processtype: processtype
        };
        $.ajax
        ({
            url: url,
            type: 'post',
            data: JSON.stringify(JSONSendData),
            dataType: 'json',
            contentType: "application/json;charset=utf-8",
            cache: false,
            success: function (feedback) {
                $("#ajax-screen-masking").hide();
                if (feedback != null && feedback.length > 0) {
                    if (feedback[0].errcode == 0) {
                        alert(feedback[0].errmsg);
                        if (processtype == 5)
                            Search($("select#ddlCategory").val(), 1, $("#hdproduct_name").val());
                        else
                            Search($("select#ddlCategory").val(), 0, $("#hdproduct_name").val());
                    }
                }
            },
            error: function (feedback) {
                $("#ajax-screen-masking").hide();
            }
        });
    }
}