﻿$(document).ready(function () {

   


    $('#SearchVoucherBtn').click(function (e) {

        //alert('');
        $.ajax
        ({
            url: "//",
            type: 'post',
            data: {},
            dataType: 'json',
            contentType: "application/json;charset=utf-8",
            cache: false,
            beforeSend: function () {
                $("#ajax-screen-masking").show();
                $('#tbVoucherList').hide();
                $('#tbVoucherListContainer').hide();
            },
            success: function (feedback) {
                $("#ajax-screen-masking").hide();
                $('#tbVoucherListContainer').show();

                if (convertNulltoEmptyString(feedback.UseDate) == '' &&
                    convertNulltoEmptyString(feedback.Voucher_Type) == '' &&
                    convertNulltoEmptyString(feedback.SerialNumber) == '' &&
                    convertNulltoEmptyString(feedback.TrffClass) == ''
                ) {
                    $("tr#voucherAvailable").hide();
                    $("tr#noVoucher").show();
                } else {
                    $("tr#voucherAvailable").show();
                    $("tr#noVoucher").hide();
                    $('#val_cbs').text(convertNulltoEmptyString(feedback.SerialNumber));
                    $('#val_usageDate').text(feedback.UsageDate_String);
                    $('#val_amount').text(feedback.price_string);
                    $('#val_voucherType').text(feedback.Voucher_Type);
                    $('#val_tarifClass').text(feedback.TrffClass);
                    $('#val_activationDate').text(feedback.ActivationDate_String);
                    $('#val_Status').text(feedback.Voucher_Status);
                }
                $('#tbVoucherList').fadeIn();
            }
        });

    });



});