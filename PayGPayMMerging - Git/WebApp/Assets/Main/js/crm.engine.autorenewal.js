﻿function GetAutoRenewalInfo(mobileno, sitecode) {
    $("#TabAutoRenewalLoader").show();
    $("#tblAutoRenewalContainer").hide();
    var url = apiServer + '/api/autorenewal';
    jQuery.support.cors = true;
    var JSONSendData = {
        mobileno: mobileno,
        sitecode: sitecode,
        ModelType: 1
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#TabAutoRenewalLoader").hide();
            $("#tblAutoRenewalContainer").show();
            if (feedback != null) {
                $("table#tblAutoRenewalInfo td#tdARIMSISDN").html(feedback.msisdn);
                $("table#tblAutoRenewalInfo td#tdARIAutoRenewalStatus").html(feedback.auto_ren_status);
                if (feedback.auto_ren_status != "") {
                    if (feedback.auto_ren_status.toUpperCase() == "ENABLED") {
                        $("#btnEnableDisable").html("Disable");
                    }
                    else {
                        $("#btnEnableDisable").html("Enable");
                    }
                }
                $("table#tblAutoRenewalInfo td#tdARIEnableBy").html(feedback.enable_by);
                $("table#tblAutoRenewalInfo td#tdARIEnableDate").html(feedback.enable_date_str);
                $("table#tblAutoRenewalInfo td#tdARIDisableBy").html(feedback.disbale_by);
                $("table#tblAutoRenewalInfo td#tdARIDisableDate").html(feedback.disable_date_str);

            } else {
                $("table#tblCustomerRoamingDetails td#tdARIMSISDN").html("");
                $("table#tblCustomerRoamingDetails td#tdARIAutoRenewalStatus").html("");
                $("table#tblCustomerRoamingDetails td#tdARIEnableBy").html("");
                $("table#tblCustomerRoamingDetails td#tdARIEnableDate").html("");
                $("table#tblCustomerRoamingDetails td#tdARIDisableBy").html("");
                $("table#tblCustomerRoamingDetails td#tdARIDisableDate").html("");

            }
        },
        error: function (err) {
            $("#TabAutoRenewalLoader").hide();
            $("#tblAutoRenewalContainer").show();
        }
    });
}

function EnableDisableProcess(mobileno, iccid, crm_user, processflag, sitecode, firstname, email) {
    $("#TabAutoRenewalLoader").show();
    $("#tblAutoRenewalContainer").hide();
    var url = apiServer + '/api/autorenewal';
    jQuery.support.cors = true;
    var JSONSendData = {
        mobileno: mobileno,
        iccid: iccid,
        crm_user: crm_user,
        processflag: processflag,
        sitecode: sitecode,
        firstname: firstname,
        email: email,
        ModelType: 2
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#TabAutoRenewalLoader").hide();
            $("#tblAutoRenewalContainer").show();
            if (feedback != null && feedback.errcode == 0) {
                //alert(feedback.errmsg);
                $('.PMSuccess .modal-header').html("Success");
                $('.PMSuccess .modal-body').html(feedback.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
                GetAutoRenewalInfo(mobileno, sitecode);

            } else {
                //alert(feedback.errmsg);
                $('.PMFailed .modal-header').html("Failure");
                $('.PMFailed .modal-body').html(feedback.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function(err){
            $("#TabAutoRenewalLoader").hide();
            $("#tblAutoRenewalContainer").show();
        }
    });
}