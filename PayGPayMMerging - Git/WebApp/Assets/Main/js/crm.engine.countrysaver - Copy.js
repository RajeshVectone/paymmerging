﻿/* ===================================================== 
   *  TAB CountrySaver 
   ----------------------------------------------------- */

/* ----------------------------------------------------------------
 * Function Name    : ViewCountrySaverList
 * Purpose          : to view Country Saver List
 * Added by         : Edi Suryadi
 * Create Date      : January 29th, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function ViewCountrySaverList(MobileNo, Sitecode) {
    
    var url = apiServer + '/api/countrysaver';
    jQuery.support.cors = true;
    var JSONSendData = {
        MobileNo: MobileNo,
        Sitecode: Sitecode
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#TabProductCountrySaverLoader").hide();
            $('#tblProductCountrySaver').dataTable({
                bDestroy: true,
                aaData: feedback,
                iDisplayLength: 10,
                bPaginate: true,
                bFilter: false,
                bProcessing: true,
                bInfo: false,
                aaSorting: [[0, "asc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No package found."
                },               
                aoColumns: [
                    { mDataProp: "PackageID", sTitle: "Bundle ID" },
                    { mDataProp: "Description", sTitle: "Bundle Name" },
                    {
                        mDataProp: "SubscriptionDate", sTitle: "Subscription Date",
                        fnRender: function (ob) {
                            return convertDateISO8601(ob.aData.SubscriptionDate, 3);
                        }
                    },
                    {
                        mDataProp: "RenewalDate", sTitle: "Renewal Date",
                        fnRender: function (ob) {
                            return convertDateISO8601(ob.aData.RenewalDate, 3);
                        }
                    },
                    {
                        mDataProp: "Amount", sTitle: "Amount",
                        fnRender: function (ob) {
                            return getCurrencySymbol(ob.aData.Currency) + " " + ob.aData.Amount.toFixed(2);
                        }
                    },
                    {
                        mDataProp: "DestinationNumber", sTitle: "F&F CLI"
                    },
                    {
                        mDataProp: "MinRemaining", sTitle: "Remaining Minutes", "bUseRendered": false,
                        fnRender: function (ob) {
                            return ob.aData.MinRemaining.toFixed(1);
                        }
                    },
                    { mDataProp: "Status_Description", sTitle: "Status" },
                    { mDataProp: "PaymentType_Description", sTitle: "Pay Mode" },
                    {
                        mDataProp: "RegID", sTitle: "Action", sWidth: "280px", "bUseRendered": false,
                        fnRender: function (ob) {
                            var data_param = ob.aData.RegID + "|" + ob.aData.PackageID + "|" + ob.aData.Description + "|" + ob.aData.DestinationNumber + "|" + ob.aData.PaymentType_Description;
                            var status = ob.aData.Status;
                            if (status == 7) {
                                var actionOption = "<select class=\"selectCountrySaverAction\" data-param=\"" + data_param + "\" >";
                                actionOption += '<option value="0" style=\"color:#c0c0c0\">--- Select an action ---</option>';
                                actionOption += '<option value="2" style=\"padding:3px\">Cancel</option>';
                                actionOption += '</select>';
                            } else if (status == 13) {
                                var actionOption = "<select class=\"selectCountrySaverAction\" data-param=\"" + data_param + "\" >";
                                actionOption += '<option value="0" style=\"color:#c0c0c0\">--- Select an action ---</option>';
                                actionOption += '<option value="1" style=\"padding:3px\">Renew</option>';
                                actionOption += '</select>';
                            } else if (status == 18) {
                                var actionOption = "<select class=\"selectCountrySaverAction\" data-param=\"" + data_param + "\" >";
                                actionOption += '<option value="0" style=\"color:#c0c0c0\">--- Select an action ---</option>';
                                actionOption += '<option value="3" style=\"padding:3px\">Reactivate</option>';
                                actionOption += '</select>';
                            } else {
                                var actionOption = "<select class=\"selectCountrySaverAction\" disabled>";
                                actionOption += '<option value="0" style=\"color:#c0c0c0\">--- No action available ---</option>';
                                actionOption += '</select>';
                            }
                            return actionOption;
                        }
                    }
                ],
                fnDrawCallback: function () {
                    $("#tblProductCountrySaverContainer input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                },
                fnInitComplete: function (ob) {
                    $("#tblProductCountrySaverContainer input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                }
            });
            $("#tblProductCountrySaver_wrapper .row-fluid:eq(0)").remove();
        },
        error: function (feedback) {
            $("#TabProductCountrySaverLoader").hide();
            $("#tblProductCountrySaverContainer").html("").append("No package found").show();
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : RefreshCountrySaverList
 * Purpose          : to refresh Country Saver List
 * Added by         : Edi Suryadi
 * Create Date      : February 04th, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function RefreshCountrySaverList(MobileNo, Sitecode) {

    $("#TabProductCountrySaverLoader").show();
    $("#tblProductCountrySaver_wrapper").remove();
    $("#tblProductCountrySaverContainer").append("<table class=\"table table-bordered table-hover\" id=\"tblProductCountrySaver\"></table>");

    var url = apiServer + '/api/countrysaver';
    jQuery.support.cors = true;
    var JSONSendData = {
        MobileNo: MobileNo,
        Sitecode: Sitecode
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#TabProductCountrySaverLoader").hide();
            $('#tblProductCountrySaver').dataTable({
                bDestroy: true,
                aaData: feedback,
                iDisplayLength: 10,
                bPaginate: true,
                bFilter: false,
                bProcessing: true,
                bInfo: false,
                aaSorting: [[0, "asc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No package found."
                },
                aoColumns: [
                    { mDataProp: "PackageID", sTitle: "Package ID" },
                    { mDataProp: "Description", sTitle: "Description" },
                    {
                        mDataProp: "SubscriptionDate", sTitle: "Subscription Date",
                        fnRender: function (ob) {
                            return convertDateISO8601(ob.aData.SubscriptionDate, 3);
                        }
                    },
                    {
                        mDataProp: "RenewalDate", sTitle: "Renewal Date",
                        fnRender: function (ob) {
                            return convertDateISO8601(ob.aData.RenewalDate, 3);
                        }
                    },
                    {
                        mDataProp: "Amount", sTitle: "Amount",
                        fnRender: function (ob) {
                            return getCurrencySymbol(ob.aData.Currency) + " " + ob.aData.Amount.toFixed(2);
                        }
                    },
                    {
                        mDataProp: "DestinationNumber", sTitle: "Destination & Number"
                    },
                    {
                        mDataProp: "MinRemaining", sTitle: "Minutes Remaining", "bUseRendered": false,
                        fnRender: function (ob) {
                            return ob.aData.MinRemaining.toFixed(1);
                        }
                    },
                    { mDataProp: "Status_Description", sTitle: "Status" },
                    { mDataProp: "PaymentType_Description", sTitle: "Pay Mode" },
                    {
                        mDataProp: "RegID", sTitle: "Action", sWidth: "280px", "bUseRendered": false,
                        fnRender: function (ob) {
                            var data_param = ob.aData.RegID + "|" + ob.aData.PackageID + "|" + ob.aData.Description + "|" + ob.aData.DestinationNumber + "|" + ob.aData.PaymentType_Description;
                            var status = ob.aData.Status;
                            status = 18;
                            if (status == 7) {
                                var actionOption = "<select class=\"selectCountrySaverAction\" data-param=\"" + data_param + "\" >";
                                actionOption += '<option value="0" style=\"color:#c0c0c0\">--- Select an action ---</option>';
                                actionOption += '<option value="2" style=\"padding:3px\">Cancel</option>';
                                actionOption += '</select>';
                            } else if (status == 13) {
                                var actionOption = "<select class=\"selectCountrySaverAction\" data-param=\"" + data_param + "\" >";
                                actionOption += '<option value="0" style=\"color:#c0c0c0\">--- Select an action ---</option>';
                                actionOption += '<option value="1" style=\"padding:3px\">Renew</option>';
                                actionOption += '</select>';
                            } else if (status == 18) {
                                var actionOption = "<select class=\"selectCountrySaverAction\" data-param=\"" + data_param + "\" >";
                                actionOption += '<option value="0" style=\"color:#c0c0c0\">--- Select an action ---</option>';
                                actionOption += '<option value="3" style=\"padding:3px\">Reactivate</option>';
                                actionOption += '</select>';
                            } else {
                                var actionOption = "<select class=\"selectCountrySaverAction\" disabled>";
                                actionOption += '<option value="0" style=\"color:#c0c0c0\">--- No action available ---</option>';
                                actionOption += '</select>';
                            }
                            return actionOption;
                        }
                    }
                ],
                fnDrawCallback: function () {
                    $("#tblProductCountrySaverContainer input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                },
                fnInitComplete: function (ob) {
                    $("#tblProductCountrySaverContainer input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                }
            });
            $("#tblProductCountrySaver_wrapper .row-fluid:eq(0)").remove();
        },
        error: function (feedback) {
            $("#TabProductCountrySaverLoader").hide();
            $("#tblProductCountrySaverContainer").html("").append("No package found").show();
        }
    });
}

function CCRegisteredList(mobileno) {
    $("#tblCSPBCRegisteredCard").html("");
    var url = apiServer + '/api/payment/' + mobileno;
    $.getJSON(url, function (feedback) {
        $("#tblCSPBCRegisteredCard").dataTable({
            aaData: feedback,
            bDestroy: true,
            bRetrieve: true,
            bFilter: false,
            bSort: false,
            bLengthChange: false,
            aaSorting: [[0, "asc"]],
            oLanguage: {
                "sEmptyTable": 'No registered credit card found'
            },
            aoColumns: [
                {
                    mDataProp: "account_id", sTitle: "", "bUseRendered": false,
                    fnRender: function (ob) {
                        var optValue = "<center><input type=\"radio\" name=\"optSelectRegisteredCC\" value=\"" + ob.aData.Last6digitsCC + "\" data-subsid=\"" + ob.aData.SubscriptionID + "\"></center>";
                        return optValue;
                    }
                },
                {
                    mDataProp: "Last6digitsCC", sTitle: "Card Number", "bUseRendered": false,
                    fnRender: function (ob) {
                        var CardNo = "XXXX XXXX XX" + ob.aData.Last6digitsCC.toString().substr(0, 2) + " " + ob.aData.Last6digitsCC.toString().substr(2, 4);
                        return CardNo;
                    }
                },
                {
                    mDataProp: "expirydate", sTitle: "Expire date", "bUseRendered": false,
                    fnRender: function (ob) {
                        return convertDate06(ob.aData.expirydate, 1);
                    }
                },
                {
                    mDataProp: "account_id", sTitle: "CVC2 (Security) Numbers", sWidth: "280px", "bUseRendered": false,
                    fnRender: function (ob) {
                        var iCVCNumber = "<input type=\"text\" class=\"iCVCNumber\" />";
                        return iCVCNumber;
                    }
                }
            ]
        });
        $("#tblCSPBCRegisteredCard_wrapper .row-fluid").remove();
        $("#tblCSPBCRegisteredCard").show();
    });
}

/* ----------------------------------------------------------------
 * Function Name    : ReactivateByBalance
 * Purpose          : to reactivate country saver by balance
 * Added by         : Edi Suryadi
 * Create Date      : February 04th, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function ReactivateByBalance(SiteCode, RegIDX) {
    var url = apiServer + '/api/countrysaver/';

    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    jQuery.support.cors = true;
    var JSONSendData = {
        SiteCode: SiteCode,
        InfoType: 2,
        RegIDX: RegIDX,
        Paymode: 1 // By Balance
    };
    window.console.log(url);
    window.console.log(JSONSendData);
    //$.ajax({
    //    url: url,
    //    type: 'post',
    //    data: JSON.stringify(JSONSendData),
    //    dataType: 'json',
    //    contentType: "application/json;charset=utf-8",
    //    cache: false,
    //    beforeSend: function () {
    //        $("#ajax-screen-masking").show();
    //    },
    //    success: function (data) {
    //        $("#ajax-screen-masking").hide();

    //        $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
    //        $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });

    //        var stat = data.errcode;
    //        if (stat == 0) {
    //            $('.PMSuccess .modal-header').html(data.errsubject);
    //            $('.PMSuccess .modal-body').html(data.errmsg);
    //            $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
    //                'margin-left': function () {
    //                    return window.pageXOffset - ($(this).width() / 2);
    //                }
    //            });
    //        } else {
    //            $('.PMFailed .modal-header').html(data.errsubject);
    //            $('.PMFailed .modal-body').html(data.errmsg);
    //            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
    //                'margin-left': function () {
    //                    return window.pageXOffset - ($(this).width() / 2);
    //                }
    //            });
    //        }
    //    },
    //    error: function (data) {
    //        $("#ajax-screen-masking").hide();
    //        $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
    //        $('.PMFailed .modal-header').html(data.errsubject);
    //        $('.PMFailed .modal-body').html(data.errmsg);
    //        $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
    //            'margin-left': function () {
    //                return window.pageXOffset - ($(this).width() / 2);
    //            }
    //        });
    //    }
    //});
}

/* ----------------------------------------------------------------
 * Function Name    : ReactivateByNewCard
 * Purpose          : to reactivate country saver by New Card
 * Added by         : Edi Suryadi
 * Create Date      : February 04th, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function ReactivateByNewCard(SiteCode, RegIDX) {
    var url = apiServer + '/api/countrysaver/';

    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    jQuery.support.cors = true;
    var JSONSendData = {
        SiteCode: SiteCode,
        InfoType: 2,
        RegIDX: RegIDX,
        Paymode: 1 // By Balance
    };
    window.console.log(url);
    window.console.log(JSONSendData);
    //$.ajax({
    //    url: url,
    //    type: 'post',
    //    data: JSON.stringify(JSONSendData),
    //    dataType: 'json',
    //    contentType: "application/json;charset=utf-8",
    //    cache: false,
    //    beforeSend: function () {
    //        $("#ajax-screen-masking").show();
    //    },
    //    success: function (data) {
    //        $("#ajax-screen-masking").hide();

    //        $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
    //        $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });

    //        var stat = data.errcode;
    //        if (stat == 0) {
    //            $('.PMSuccess .modal-header').html(data.errsubject);
    //            $('.PMSuccess .modal-body').html(data.errmsg);
    //            $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
    //                'margin-left': function () {
    //                    return window.pageXOffset - ($(this).width() / 2);
    //                }
    //            });
    //        } else {
    //            $('.PMFailed .modal-header').html(data.errsubject);
    //            $('.PMFailed .modal-body').html(data.errmsg);
    //            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
    //                'margin-left': function () {
    //                    return window.pageXOffset - ($(this).width() / 2);
    //                }
    //            });
    //        }
    //    },
    //    error: function (data) {
    //        $("#ajax-screen-masking").hide();
    //        $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
    //        $('.PMFailed .modal-header').html(data.errsubject);
    //        $('.PMFailed .modal-body').html(data.errmsg);
    //        $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
    //            'margin-left': function () {
    //                return window.pageXOffset - ($(this).width() / 2);
    //            }
    //        });
    //    }
    //});
}

/* ----------------------------------------------------------------
 * Function Name    : ReactivateByRegisteredCard
 * Purpose          : to reactivate country saver by Registered Card
 * Added by         : Edi Suryadi
 * Create Date      : February 04th, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function ReactivateByRegisteredCard(MobileNo, CCNumber, CVCNumber) {
    var url = apiServer + '/api/payment/';

    var ajaxSpinnerTop = ($(window).outerHeight() / 3) + $(window).scrollTop();
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    
    var url = apiServer + '/api/payment/';
    jQuery.support.cors = true;
    var JSONSendData = {
        order_records: 8,
        account_id: MobileNo,
        card_number: CCNumber,
        card_verf_code: CVCNumber
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            var stat = data.errcode;
            if (stat == 0) {
                window.console.log("PASSED");
            } else {
                $("#ajax-screen-masking").hide();
                $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "" });
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (data) {
            $("#ajax-screen-masking").hide();
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "" });
            $('.PMFailed .modal-header').html(data.errsubject);
            $('.PMFailed .modal-body').html(data.errmsg);
            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });
    
}

/* ----------------------------------------------------------------
 * Function Name    : RenewByBalance
 * Purpose          : to renew country saver by balance
 * Added by         : Edi Suryadi
 * Create Date      : February 06th, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function RenewByBalance(SiteCode, RegIDX) {
    var url = apiServer + '/api/countrysaver/';

    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    jQuery.support.cors = true;
    var JSONSendData = {
        SiteCode: SiteCode,
        InfoType: 2,
        RegIDX: RegIDX,
        Paymode: 1 // By Balance
    };
    window.console.log(url);
    window.console.log(JSONSendData);
    //$.ajax({
    //    url: url,
    //    type: 'post',
    //    data: JSON.stringify(JSONSendData),
    //    dataType: 'json',
    //    contentType: "application/json;charset=utf-8",
    //    cache: false,
    //    beforeSend: function () {
    //        $("#ajax-screen-masking").show();
    //    },
    //    success: function (data) {
    //        $("#ajax-screen-masking").hide();

    //        $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
    //        $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });

    //        var stat = data.errcode;
    //        if (stat == 0) {
    //            $('.PMSuccess .modal-header').html(data.errsubject);
    //            $('.PMSuccess .modal-body').html(data.errmsg);
    //            $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
    //                'margin-left': function () {
    //                    return window.pageXOffset - ($(this).width() / 2);
    //                }
    //            });
    //        } else {
    //            $('.PMFailed .modal-header').html(data.errsubject);
    //            $('.PMFailed .modal-body').html(data.errmsg);
    //            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
    //                'margin-left': function () {
    //                    return window.pageXOffset - ($(this).width() / 2);
    //                }
    //            });
    //        }
    //    },
    //    error: function (data) {
    //        $("#ajax-screen-masking").hide();
    //        $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
    //        $('.PMFailed .modal-header').html(data.errsubject);
    //        $('.PMFailed .modal-body').html(data.errmsg);
    //        $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
    //            'margin-left': function () {
    //                return window.pageXOffset - ($(this).width() / 2);
    //            }
    //        });
    //    }
    //});
}

/* ----------------------------------------------------------------
 * Function Name    : RenewByNewCard
 * Purpose          : to renew country saver by New Card
 * Added by         : Edi Suryadi
 * Create Date      : February 04th, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function RenewByNewCard(SiteCode, RegIDX) {
    var url = apiServer + '/api/countrysaver/';

    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    jQuery.support.cors = true;
    var JSONSendData = {
        SiteCode: SiteCode,
        InfoType: 2,
        RegIDX: RegIDX,
        Paymode: 1 // By Balance
    };
    window.console.log(url);
    window.console.log(JSONSendData);
    //$.ajax({
    //    url: url,
    //    type: 'post',
    //    data: JSON.stringify(JSONSendData),
    //    dataType: 'json',
    //    contentType: "application/json;charset=utf-8",
    //    cache: false,
    //    beforeSend: function () {
    //        $("#ajax-screen-masking").show();
    //    },
    //    success: function (data) {
    //        $("#ajax-screen-masking").hide();

    //        $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
    //        $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });

    //        var stat = data.errcode;
    //        if (stat == 0) {
    //            $('.PMSuccess .modal-header').html(data.errsubject);
    //            $('.PMSuccess .modal-body').html(data.errmsg);
    //            $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
    //                'margin-left': function () {
    //                    return window.pageXOffset - ($(this).width() / 2);
    //                }
    //            });
    //        } else {
    //            $('.PMFailed .modal-header').html(data.errsubject);
    //            $('.PMFailed .modal-body').html(data.errmsg);
    //            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
    //                'margin-left': function () {
    //                    return window.pageXOffset - ($(this).width() / 2);
    //                }
    //            });
    //        }
    //    },
    //    error: function (data) {
    //        $("#ajax-screen-masking").hide();
    //        $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
    //        $('.PMFailed .modal-header').html(data.errsubject);
    //        $('.PMFailed .modal-body').html(data.errmsg);
    //        $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
    //            'margin-left': function () {
    //                return window.pageXOffset - ($(this).width() / 2);
    //            }
    //        });
    //    }
    //});
}

/* ----------------------------------------------------------------
 * Function Name    : RenewByRegisteredCard
 * Purpose          : to renew country saver by Registered Card
 * Added by         : Edi Suryadi
 * Create Date      : February 04th, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function RenewByRegisteredCard(MobileNo, CCNumber, CVCNumber) {
    var url = apiServer + '/api/payment/';

    var ajaxSpinnerTop = ($(window).outerHeight() / 3) + $(window).scrollTop();
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    var url = apiServer + '/api/payment/';
    jQuery.support.cors = true;
    var JSONSendData = {
        order_records: 8,
        account_id: MobileNo,
        card_number: CCNumber,
        card_verf_code: CVCNumber
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            var stat = data.errcode;
            if (stat == 0) {
                window.console.log("PASSED");
            } else {
                $("#ajax-screen-masking").hide();
                $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "" });
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (data) {
            $("#ajax-screen-masking").hide();
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "" });
            $('.PMFailed .modal-header').html(data.errsubject);
            $('.PMFailed .modal-body').html(data.errmsg);
            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });

}

$.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
    return {
        "iStart": oSettings._iDisplayStart,
        "iEnd": oSettings.fnDisplayEnd(),
        "iLength": oSettings._iDisplayLength,
        "iTotal": oSettings.fnRecordsTotal(),
        "iFilteredTotal": oSettings.fnRecordsDisplay(),
        "iPage": oSettings._iDisplayLength === -1 ?
			0 : Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
        "iTotalPages": oSettings._iDisplayLength === -1 ?
			0 : Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
    };
};

/* ----------------------------------------------------- 
   *  eof TAB CountrySaver 
   ===================================================== */


