﻿/* ===================================================== 
   *  Add Balance Script 
   ----------------------------------------------------- */

function AddBalance(sitecode, mobileno, amount, username, bundleid, balance_type, daysvalid) {
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    var url = apiServer + '/api/AddBalance';
    jQuery.support.cors = true;
    var JSONSendData = {
        sitecode: sitecode,
        mobileno: mobileno,
        amount: amount,
        username: username,
        calledby: 'CRM APP',
        balance_type: balance_type,
        bundleid: bundleid,
        daysvalid: daysvalid
    };
    $.ajax
({
    url: url,
    type: 'post',
    data: JSON.stringify(JSONSendData),
    dataType: 'json',
    contentType: "application/json;charset=utf-8",
    cache: false,
    beforeSend: function () {
        $("#ajax-screen-masking").show();
    },
    success: function (feedback) {
        $("#ajax-screen-masking").hide();
        alert(feedback.errmsg);
    },
    error: function (feedback) {
        $("#ajax-screen-masking").hide();
    }
});
}

function AddBalanceLTP(sitecode, mobileno, paymentref, amount, Comments) {
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    var url = apiServer + '/api/AddBalanceLTP';
    jQuery.support.cors = true;
    var JSONSendData = {
        sitecode: sitecode,
        mobileno: mobileno,
        paymentref: paymentref,
        amount: amount,
        calledby: 'CRM APP',
        crm_user: $('.username').html(),
        comments: Comments
    };
    $.ajax
({
    url: url,
    type: 'post',
    data: JSON.stringify(JSONSendData),
    dataType: 'json',
    contentType: "application/json;charset=utf-8",
    cache: false,
    beforeSend: function () {
        $("#ajax-screen-masking").show();
    },
    success: function (feedback) {
        $("#ajax-screen-masking").hide();
       
        $("#tdABLTPOnlinePayRef").val('');
        $("#tdABLTPAmount").val('');
        $("#tdABLTPComments").val('');
        alert(feedback.errmsg);
    },
    error: function (feedback) {
        $("#ajax-screen-masking").hide();
    }
});
}
function AddBalanceManual(sitecode, mobileno, comments, refund_by, amount) {
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    var url = apiServer + '/api/AddBalanceManual';
    jQuery.support.cors = true;
    var JSONSendData = {
        sitecode: sitecode,
        mobileno: mobileno,
        comments: comments,
        refund_by: refund_by,
        amount: amount,
        calledby: 'CRM APP',
        crm_user: $('.username').html()
    };
    $.ajax
({
    url: url,
    type: 'post',
    data: JSON.stringify(JSONSendData),
    dataType: 'json',
    contentType: "application/json;charset=utf-8",
    cache: false,
    beforeSend: function () {
        $("#ajax-screen-masking").show();
    },
    success: function (feedback) {
        $("#ajax-screen-masking").hide();
        $("#tdABManualAmount").val('');
        $("#tdABManualComments").val('');
        alert(feedback.errmsg);
    },
    error: function (feedback) {
        $("#ajax-screen-masking").hide();
        alert(feedback.responseText);
    }
});

}

