﻿function ShowPAYMPlan(type) {
    if (type == 1) {
        $("#divPayMBundles").show();
        $("#divPayMBundleDetails").show();
        $("#divPayMBundleHistory").hide();
        $('#divPayMChangePlanSelect').hide();
        $('#divPayMChangePlanConfirm').hide();
        $('#divPayMChangePlanOrder').hide();
        $('#divPayMCancelPlanSelect').hide();
        $('#divPayMCancelPlanOrder').hide();
        $("#paymentMethodContainer").hide();
        $("#paymentConfirmPlanContainer").hide();
        //Added by Elango for Jira CRMT-216
        $('#divPayMChangeCreditLimitPlanSelect').hide();
        $('#divPayMAcceptCreditlimit').hide();
        $('#divPayMNorequestCreditlimit').hide();

        //Restrict Excess Usage
        $('#divPayMRestrictExcessPlanSelect').hide();
    }
    else if (type == 2) {
        $("#divPayMBundles").hide();
        $("#divPayMBundleDetails").hide();
        $("#divPayMBundleHistory").show();
        $('#divPayMChangePlanSelect').hide();
        $('#divPayMChangePlanConfirm').hide();
        $('#divPayMChangePlanOrder').hide();
        $('#divPayMCancelPlanSelect').hide();
        $('#divPayMCancelPlanOrder').hide();
        $("#paymentMethodContainer").hide();
        $("#paymentConfirmPlanContainer").hide();
        //Added by Elango for Jira CRMT-216
        $('#divPayMChangeCreditLimitPlanSelect').hide();
        $('#divPayMAcceptCreditlimit').hide();
        $('#divPayMNorequestCreditlimit').hide();

        //Restrict Excess Usage
        $('#divPayMRestrictExcessPlanSelect').hide();
    }
    else if (type = 3) {
        $("#divPayMBundles").hide();
        $("#divPayMBundleDetails").hide();
        $("#divPayMBundleHistory").hide();
        $('#divPayMChangePlanSelect').hide();
        $('#divPayMChangePlanConfirm').hide();
        $('#divPayMChangePlanOrder').hide();
        $('#divPayMCancelPlanSelect').hide();
        $('#divPayMCancelPlanOrder').hide();
        $("#paymentMethodContainer").show();
        $("#paymentConfirmPlanContainer").hide();
        //Added by Elango for Jira CRMT-216
        $('#divPayMChangeCreditLimitPlanSelect').hide();
        $('#divPayMAcceptCreditlimit').hide();
        $('#divPayMNorequestCreditlimit').hide();

        //$("button#PaymentMethodBtnCancel").unbind("click").remove();
        //$("button#PaymentMethodBtnNewCardProceed").unbind("click").remove();

        //Restrict Excess Usage
        $('#divPayMRestrictExcessPlanSelect').hide();
    }
    else if (type = 4) {
        $("#divPayMBundles").hide();
        $("#divPayMBundleDetails").hide();
        $("#divPayMBundleHistory").hide();
        $('#divPayMChangePlanSelect').hide();
        $('#divPayMChangePlanConfirm').hide();
        $('#divPayMChangePlanOrder').hide();
        $('#divPayMCancelPlanSelect').hide();
        $('#divPayMCancelPlanOrder').hide();
        $("#paymentMethodContainer").hide();
        $("#paymentConfirmPlanContainer").show();
        //Added by Elango for Jira CRMT-216
        $('#divPayMChangeCreditLimitPlanSelect').hide();
        $('#divPayMAcceptCreditlimit').hide();
        $('#divPayMNorequestCreditlimit').hide();

        //$("button#paymentConfirmPlan_btnCancel").unbind("click").remove();
        //$("button#paymentConfirmPlan_btnConfirmOrder").unbind("click").remove();

        //Restrict Excess Usage
        $('#divPayMRestrictExcessPlanSelect').hide();
    }
    else if (type = 6) {
        $("#divPayMBundles").hide();
        $("#divPayMBundleDetails").hide();
        $("#divPayMBundleHistory").hide();
        $('#divPayMChangePlanSelect').hide();
        $('#divPayMChangePlanConfirm').hide();
        $('#divPayMChangePlanOrder').hide();
        $('#divPayMCancelPlanSelect').hide();
        $('#divPayMCancelPlanOrder').hide();
        $("#paymentMethodContainer").hide();
        $("#paymentConfirmPlanContainer").hide();
        //Added by Elango for Jira CRMT-216
        $('#divPayMChangeCreditLimitPlanSelect').hide();
        $('#divPayMAcceptCreditlimit').hide();
        $('#divPayMNorequestCreditlimit').hide();

        //Restrict Excess Usage
        $('#divPayMRestrictExcessPlanSelect').show();
    }
}

function ViewPAYMPlanListAction(idx) {
    var paymplanaction = $("#paymplanaction" + idx + " option:selected").val();
    //$("input[name='paymplanaction']:selected");
    //alert(paymplanaction);
    var paymplanactionArr = paymplanaction.split('_');
    if (paymplanactionArr.length > 1) {
        if (paymplanactionArr[0] == 1) {
            ViewPAYMPlanHistory(paymplanactionArr[1]);
        }
        else if (paymplanactionArr[0] == 2) {
            //Added : 25-Dec-2018 - Bundle Category
            PopulateBundleCategory(paymplanactionArr[1], paymplanactionArr[2], paymplanactionArr[3]);
            //ViewPayMChangePlanSelect(paymplanactionArr[1]);
        }
        else if (paymplanactionArr[0] == 3) {
            ViewPayMCancelPlanSelect(paymplanactionArr[1], paymplanactionArr[2]);
        }
        else if (paymplanactionArr[0] == 4) {
            ViewPayMChangeCreditLimit(paymplanactionArr[1], paymplanactionArr[2]);
        }
        else if (paymplanactionArr[0] == 5) {
            ViewPayMAcceptCreditLimit(paymplanactionArr[1], paymplanactionArr[2]);
        }
        else if (paymplanactionArr[0] == 6) {
            ViewPayMRestrictExcessUsageSelect(paymplanactionArr[1], paymplanactionArr[2]);
        }
    }
}

function ViewPAYMPlanList(MobileNo, ProductCode, SiteCode) {
    ShowPAYMPlan(1);
    $("#PayMBundles").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblBundlesPlan\"></table>");
    $("#PayMBundles").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var row = 0;
    var url = apiServer + "/api/PAYMPlanBundles";
    var JSONSendData = {
        requestType: 1,
        MobileNo: MobileNo,
        ProductCode: ProductCode,
        SiteCode: SiteCode
    };
    $.ajax
    ({
        url: url,
        type: "post",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            //Added by Elango for Jira CRMT-216 End
            var UserName = $('.username').html();
            $("#PayMBundles").css({ "background-image": "none" });
            $('#tblBundlesPlan').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: false,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No PAYM Plans found."
                },
                aoColumns: [
                    //{ mDataProp: "idx", sTitle: "Plan ID" },
                    //CRM Enhancement 2
                    { mDataProp: "sim_type", sTitle: "Sim&nbsp;Type" },
                    { mDataProp: "bundle_plan_name", sTitle: "Plan&nbsp;Name" },
                    { mDataProp: "plan_start_date_name", sTitle: "Subscription Date" },
                    //Added by Elango for Jira CRMT-216 End

                    //Restrict Excess Usage
                    //{ mDataProp: "credit_limit", sTitle: "Credit Limit (£)" },
                    { mDataProp: "credit_limit", sTitle: "Excess usage Limit (£)" },
                    { mDataProp: "excess_usage_status", sTitle: "Excess usage Status" },
                    { mDataProp: "plan_active_status_name", sTitle: "Status" },
                    //CRM Enhancement 2
                    { mDataProp: "assigned_allowance", sTitle: "Ass.&nbsp;Allowance" },
                    //01-Feb-2019: Moorthy : added for Ava Allowance
                    //{ mDataProp: "avaialable_allowance", sTitle: "Ava.&nbsp;Allowance"},
                    {
                        mDataProp: "avaialable_allowance", sTitle: "Ava.&nbsp;Allowance",
                        fnRender: function (ob) {
                            if (ob.aData.avaialable_allowance != null) {
                                var arr = ob.aData.avaialable_allowance.split('|');
                                var avaAllowance = "";
                                $.each(arr, function (i) {
                                    avaAllowance += "<span style=\"font-size:12px\">" + arr[i] + "</span><br>";
                                });
                                if (ob.aData.international_tariff != "") {
                                    return "<a href=\"javascript:void(0)\" " + "\" ctrlValue1=\"" + ob.aData.plan_start_date_name + "\" ctrlValue2=\"" + ob.aData.international_tariff + "\" style=\"text-decoration: underline;color:blue;\" class=\"orderIDAnchor\" param-data=\"" + ob.aData.config_id + "\" bundle-data=\"" + ob.aData.bundleid + "\">" + avaAllowance + "</a>";
                                }
                                else
                                    return avaAllowance;
                            }
                            else
                                return "";
                        }

                    },
                    { mDataProp: "paym_conversion_str", sTitle: "PayM&nbsp;Conversion" },
                    { mDataProp: "plan_cancel_requestdate_str", sTitle: "Cancel Request&nbsp;Date" },
                    { mDataProp: "plan_cancel_scheduledate_str", sTitle: "Cancel Schedule&nbsp;Date" },
                    //01-Feb-2019 : Moorthy : added for additional output bill_cycle
                    { mDataProp: "bill_period", sTitle: "Billing&nbsp;Cycle" },
                    { mDataProp: "next_billing_date_str", sTitle: "Next Billing&nbsp;Date" },
                    { mDataProp: "Last_billing_date_str", sTitle: "Last Billing&nbsp;Date" },
                    {
                        mDataProp: 'action', "sTitle": "Action", bSortable: false,
                        fnRender: function (oObj) {
                            //Modified for Change & Cancel Plan
                            var strOption = '<select id="paymplanaction' + oObj.aData.idx + '" name="paymplanaction' + oObj.aData.idx + '" onChange="javascript:ViewPAYMPlanListAction(' + oObj.aData.idx + ');">' +
                            '<option value="0_' + oObj.aData.idx + '">Select</option>' +
                            '<option value="1_' + MobileNo + '">View Subscription History</option>' +
                            //Added by Elango for Jira CRMT-216 End
                            //Commented by Moorthy : As per Praveen comment on PayM merging issue no 17 
                            //((UserName == "Product.Team") ? ('<option value="5_' + MobileNo + '">Approve/Reject</option>') : "") +
                            ((feedback[0].plan_active_status_name == "Active") ? ('<option value="2_' + oObj.aData.plan_name + '_' + oObj.aData.bundleid + '_' + oObj.aData.plan_value + '">Change Plan</option>' +
                            '<option value="3_' + oObj.aData.plan_name + '_' + oObj.aData.bundleid + '">Cancel Plan</option>' +
                            ((UserName == "dev") ? ('<option value="4_' + MobileNo + '">Change Credit Limit</option>') : "")) : "") +
                            '<option value="6_' + MobileNo + "_" + oObj.aData.excess_usage_status_flag + '">Restrict Excess Usage</option>' + '</select>';

                            //return '<select id="paymplanaction' + oObj.aData.idx + '" name="paymplanaction' + oObj.aData.idx + '" onChange="javascript:ViewPAYMPlanListAction(' + oObj.aData.idx + ');">' +
                            //    '<option value="0_' + oObj.aData.idx + '"></option>' +
                            //    '<option value="1_' + MobileNo + '">View Subscription History</option>' +
                            //    '<option value="2_' + oObj.aData.plan_name + '">Change Plan</option>' +
                            //    '<option value="3_' + oObj.aData.plan_name + '_' + oObj.aData.bundleid + '">Cancel Plan</option>' + '</select>';
                            return strOption;
                        }
                    }
                ],
                fnDrawCallback: function () {
                    $("#tblBundlesPlan tbody td").css({ "font-size": "12px" });
                }
            });
            $("#tblBundlesPlan tbody td").css({ "font-size": "12px" });
            $("#tblBundlesPlan_wrapper .row-fluid:eq(0)").remove();
            $("#tblBundlesPlan_wrapper .row-fluid .span6:eq(0)").addClass("span4");
            $("#tblBundlesPlan_wrapper .row-fluid .span6:eq(1)").addClass("span8");
            $("#tblBundlesPlan_wrapper .row-fluid .span6").removeClass("span6");
        },
        error: function (feedback) {
            $("#PayMBundles").css({ "background-image": "none" }).html("").append("No PAYM Plans found.").show();
        }
    });
}
// 31-Jul-2015 : Moorthy Modified
function ViewPAYMPlanDetailList(MobileNo, ProductCode) {
    ShowPAYMPlan(1);
    $("#PayMBundleDetails").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblBundlesPlanDetail\"></table>");
    $("#PayMBundleDetails").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var row = 0;
    var url = apiServer + "/api/PAYMPlanBundles";
    var JSONSendData = {
        requestType: 2,
        MobileNo: MobileNo,
        ProductCode: ProductCode
    };
    $.ajax
    ({
        url: url,
        type: "post",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#PayMBundleDetails").css({ "background-image": "none" });
            $('#tblBundlesPlanDetail').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: false,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Bundle Detail found."
                },
                aoColumns: [
                    { mDataProp: "package_id", sTitle: "Package ID" },
                    { mDataProp: "package_name", sTitle: "Package Details", sWidth: "250px" },
                    { mDataProp: "startDate_name", sTitle: "Start Date" },
                    { mDataProp: "expDate_name", sTitle: "End Date" },
                    { mDataProp: "packagetype_name", sTitle: "Package Type" },
                    { mDataProp: "status_name", sTitle: "Status" },
                    { mDataProp: "credit_limit", sTitle: "Allocated<br />(mins/text/data)" },
                    { mDataProp: "current_excess", sTitle: "Used<br />(mins/text/data" },
                    { mDataProp: "available_excess", sTitle: "Balance<br />(mins/text/data" },
                ],
                fnDrawCallback: function () {
                    $("#tblBundlesPlanDetail tbody td").css({ "font-size": "12px" });
                }
            });
            $("#tblBundlesPlanDetail tbody td").css({ "font-size": "12px" });
            $("#tblBundlesPlanDetail_wrapper .row-fluid:eq(0)").remove();
            $("#tblBundlesPlanDetail_wrapper .row-fluid .span6:eq(0)").addClass("span4");
            $("#tblBundlesPlanDetail_wrapper .row-fluid .span6:eq(1)").addClass("span8");
            $("#tblBundlesPlanDetail_wrapper .row-fluid .span6").removeClass("span6");
        },
        error: function (feedback) {
            $("#PayMBundleDetails").css({ "background-image": "none" }).html("").append("No Bundle Detail  found.").show();
        }
    });
}

function ViewPAYMPlanHistory(MobileNo) {
    ShowPAYMPlan(2);
    $("#PayMBundleHistory").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblBundlesPlanHistory\"></table>");
    $("#PayMBundleHistory").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var row = 0;
    var url = apiServer + "/api/PAYMPlanBundles";
    var JSONSendData = {
        requestType: 3,
        MobileNo: MobileNo,
        ProductCode: $('.clsproductcode')[0].innerHTML
    };
    $.ajax
    ({
        url: url,
        type: "post",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#PayMBundleHistory").css({ "background-image": "none" });
            $('#tblBundlesPlanHistory').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: false,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Bundle Detail found."
                },
                aoColumnDefs: [{ "bVisible": false, "aTargets": [0] }],
                aoColumns: [
                    { mDataProp: "log_date", sTitle: "Date" },
                    { mDataProp: "log_date_name", sTitle: "Date" },
                    { mDataProp: "description", sTitle: "Action", sWidth: "200px" },
                    //01-Feb-2019: Moorthy : Added for CRM Enhancement 2
                    { mDataProp: "prev_plan", sTitle: "Prev. Plan" },
					{ mDataProp: "plan_details", sTitle: "Curr. Plan" },
                    { mDataProp: "requested_by", sTitle: "Requested By" },
                    { mDataProp: "payment_amount_name", sTitle: "Amount" },
                    //Added by Elango for Jira CRMT-216 End
                    { mDataProp: "request_status", sTitle: "Request Status" },
                    { mDataProp: "payment_methods", sTitle: "Payment Methods" },
                    { mDataProp: "payment_status_name", sTitle: "Payment Status" },
                ],
                fnDrawCallback: function () {
                    $("#tblBundlesPlanHistory tbody td").css({ "font-size": "12px" });
                }
            });
            $("#tblBundlesPlanHistory tbody td").css({ "font-size": "12px" });
            $("#tblBundlesPlanHistory_wrapper .row-fluid:eq(0)").remove();
            $("#tblBundlesPlanHistory_wrapper .row-fluid .span6:eq(0)").addClass("span4");
            $("#tblBundlesPlanHistory_wrapper .row-fluid .span6:eq(1)").addClass("span8");
            $("#tblBundlesPlanHistory_wrapper .row-fluid .span6").removeClass("span6");
        },
        error: function (feedback) {
            $("#PayMBundleHistory").css({ "background-image": "none" }).html("").append("No Bundle Detail  found.").show();
        }
    });
}
// 31-Jul-2015 : Moorthy Modified
function ClosePAYMPlanHistory(MobileNo, ProductCode) {
    ViewPAYMPlanList(MobileNo, ProductCode);
    ViewPAYMPlanDetailList(MobileNo, ProductCode);
}

//Added : 25-Dec-2018 - Change Plan
function PopulateBundleCategory(planName, planId, planValue) {
    $('#ddlBundleCategory').find('option').remove();
    $("#PayMChangePlanSelect").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblPayMChangePlanSelect\"></table>");
    $("#btnChangePlanConfirm").attr("disabled", "disabled");

    var sitecode = $('#PayMChangePlanSelect_SiteCode').val();
    $("#PayMChangePlanSelect_CurrentPlan").val(planName);
    $("#PayMChangePlanSelect_CurrentPlanId").val(planId);
    $("#PayMChangePlanSelect_CurrentPlanValue").val(planValue);
    var url = apiServer + "/api/PAYMPlanBundles";
    var JSONSendData = {
        requestType: 96,
        SiteCode: sitecode
    };
    $.ajax
    ({
        url: url,
        type: "post",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            var category_id = 0;
            $("#PayMChangePlanSelect").css({ "background-image": "none" });
            $('#divPayMChangePlanSelect').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
            var feedbackarr = jQuery.makeArray(JSON.stringify(feedback).split('}'));
            var icount;
            var actionOption = '';
            for (icount = 0; icount < feedbackarr.length - 1; icount++) {
                if (icount == 0) {
                    category_id = feedback[icount].category_id;
                }
                actionOption += '<option value="' + feedback[icount].category_id + '" style=\"padding:3px\">' + feedback[icount].category_name + '</option>';
            }
            $('#ddlBundleCategory').append(actionOption);
            PopulateBundle(category_id);
        },
        error: function (feedback) {
            $("#PayMChangePlanSelect").css({ "background-image": "none" });
        }
    });
}

//Added : 25-Dec-2018 - Change Plan
function PopulateBundle(category_id) {
    var planName = $("#PayMChangePlanSelect_CurrentPlan").val();
    var planId = $("#PayMChangePlanSelect_CurrentPlanId").val();
    var sitecode = $('#PayMChangePlanSelect_SiteCode').val();
    $("#btnChangePlanConfirm").attr("disabled", "disabled");
    $("#PayMChangePlanSelect").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblPayMChangePlanSelect\"></table>");
    $("#PayMChangePlanSelect").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var row = 0;
    var url = apiServer + "/api/PAYMPlanBundles";
    var JSONSendData = {
        requestType: 97,
        SiteCode: sitecode,
        CategoryId: category_id
    };
    $.ajax
    ({
        url: url,
        type: "post",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            if (feedback.length > 0)
                $("#btnChangePlanConfirm").removeAttr("disabled");
            $("#PayMChangePlanSelect").css({ "background-image": "none" });
            $('#tblPayMChangePlanSelect').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: false,
                bFilter: false,
                bInfo: false,
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Bundle Selection found."
                },
                aoColumns: [
                    {
                        mDataProp: "bundleid", sTitle: "Select", bSortable: false,
                        fnRender: function (oObj) {
                            var checked = "";
                            if (oObj.aData.bundleid == planId)
                                checked = "checked";
                            return '<div class="radio">' +
                                '<label>' +
                                '<input type="radio" id="radioPayMChangePlanSelect" name="radioPayMChangePlanSelect" value="' + oObj.aData.bundleid + '_' + oObj.aData.plan_value + '_' + oObj.aData.plan_name + '" ' + checked + '>' +
                                '<label>' +
                                '</div>';
                        }
                    },
                    //{ mDataProp: "bundleid", sTitle: "Bundle ID", bSortable: false },
                    { mDataProp: "plan_name", sTitle: "Bundle Name", bSortable: false },
                    { mDataProp: "plan_value", sTitle: "Plan Value", bSortable: false },
                    { mDataProp: "bundle_call", sTitle: "Call", bSortable: false },
                    { mDataProp: "bundle_sms", sTitle: "Sms", bSortable: false },
                    { mDataProp: "bundle_data", sTitle: "Data", bSortable: false }
                ],
                fnDrawCallback: function () {
                    $("#tblPayMChangePlanSelect tbody td").css({ "font-size": "12px" });
                }
            });
            $("#tblPayMChangePlanSelect tbody td").css({ "font-size": "12px" });
            $("#tblPayMChangePlanSelect_wrapper .row-fluid:eq(0)").remove();
            $("#tblPayMChangePlanSelect_wrapper .row-fluid .span6:eq(0)").addClass("span4");
            $("#tblPayMChangePlanSelect_wrapper .row-fluid .span6:eq(1)").addClass("span8");
            $("#tblPayMChangePlanSelect_wrapper .row-fluid .span6").removeClass("span6");
        },
        error: function (feedback) {
            $("#PayMChangePlanSelect").css({ "background-image": "none" }).html("").append("No Bundle Selection found.").show();
        }
    });
}

//Not In Use
function ViewPayMChangePlanSelect(planName) {
    $('#divPayMChangePlanSelect').modal({ keyboard: false, backdrop: 'static' }).css({
        'margin-left': function () {
            return window.pageXOffset - ($(this).width() / 2);
        }
    });
    $("#PayMChangePlanSelect_CurrentPlan").val(planName);
    $("#PayMChangePlanSelect").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblPayMChangePlanSelect\"></table>");
    $("#PayMChangePlanSelect").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var row = 0;
    var url = apiServer + "/api/PAYMPlanBundles";
    var JSONSendData = {
        requestType: 4,
        ProductCode: $('.clsproductcode')[0].innerHTML
    };
    $.ajax
    ({
        url: url,
        type: "post",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#PayMChangePlanSelect").css({ "background-image": "none" });
            $('#tblPayMChangePlanSelect').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: false,
                bFilter: false,
                bInfo: false,
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Bundle Selection found."
                },
                aoColumns: [
                    {
                        mDataProp: "ProductSKU", sTitle: "Select", bSortable: false,
                        fnRender: function (oObj) {
                            var checked = "";
                            if (oObj.aData.plan_name == planName)
                                checked = "checked";
                            return '<div class="radio">' +
                                '<label>' +
                                '<input type="radio" id="radioPayMChangePlanSelect" name="radioPayMChangePlanSelect" value="' + oObj.aData.bundleid + '_' + oObj.aData.plan_name + '" ' + checked + '>' +
                                '<label>' +
                                '</div>';
                        }
                    },
                    { mDataProp: "bundleid", sTitle: "Bundle ID", bSortable: false },
                    { mDataProp: "plan_name", sTitle: "Bundle Name", bSortable: false },
                ],
                fnDrawCallback: function () {
                    $("#tblPayMChangePlanSelect tbody td").css({ "font-size": "12px" });
                }
            });
            $("#tblPayMChangePlanSelect tbody td").css({ "font-size": "12px" });
            $("#tblPayMChangePlanSelect_wrapper .row-fluid:eq(0)").remove();
            $("#tblPayMChangePlanSelect_wrapper .row-fluid .span6:eq(0)").addClass("span4");
            $("#tblPayMChangePlanSelect_wrapper .row-fluid .span6:eq(1)").addClass("span8");
            $("#tblPayMChangePlanSelect_wrapper .row-fluid .span6").removeClass("span6");
        },
        error: function (feedback) {
            $("#PayMChangePlanSelect").css({ "background-image": "none" }).html("").append("No Bundle Selection found.").show();
        }
    });
}

//Added : 25-Dec-2018 - Change Plan
function NextPayMChangePlanSelect(MobileNo) {
    if ($("input[name='radioPayMChangePlanSelect']:checked").length == 1) {
        //$('#divPayMChangePlanSelect').modal('hide');
        var oldPlanName = $("#PayMChangePlanSelect_CurrentPlan").val();
        var oldPlanBundleId = $("#PayMChangePlanSelect_CurrentPlanId").val();
        var oldPlanValue = $("#PayMChangePlanSelect_CurrentPlanValue").val();//parseFloat(oldPlanName.substring(1, oldPlanName.length));
        var newPlan = $("input[name='radioPayMChangePlanSelect']:checked").val()
        var newPlanArr = newPlan.split('_');
        if (newPlanArr.length > 1) {
            newPlanBundleId = newPlanArr[0];
            newPlanValue = newPlanArr[1];
            newPlanName = newPlanArr[2];
            //newPlanValue = parseFloat(newPlanName.substring(1, newPlanName.length));
        }
        if (oldPlanBundleId != null && !isNaN(oldPlanBundleId) && newPlanBundleId != null && !isNaN(newPlanBundleId)) {//if (oldPlanValue != null && !isNaN(oldPlanValue) && newPlanValue != null && !isNaN(newPlanValue)) {
            if (oldPlanBundleId == newPlanBundleId) {
                alert("New plan is the same as the old one");
                //$("#divPayMChangePlanSelect").modal('show');
            }
            else {
                $("#PayMChangePlanConfirm_OldPlan").text(oldPlanName);
                $("#PayMChangePlanConfirm_OldPlanValue").val(oldPlanValue);
                $("#PayMChangePlanConfirm_NewPlan").text(newPlanName);
                $("#PayMChangePlanConfirm_NewPlanValue").val(newPlanValue);
                $("#PayMChangePlanConfirm_NewBundle").val(newPlanBundleId);
                $('#divPayMChangePlanSelect').modal('hide');
                $('#divPayMChangePlanConfirm').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        }
        else {
            alert("Bundle Selection caused some general error");
            $('#divPayMChangePlanSelect').modal('hide');
        }
    }
    else {
        alert("Select Bundle!");
        $("#divPayMChangePlanSelect").modal('show');
    }
}

function NextPayMChangePlanConfirm(MobileNo) {debugger
    $('#divPayMChangePlanConfirm').modal('hide');
    var oldPlanName = $("#PayMChangePlanConfirm_OldPlan").text();
    var oldPlanValue = parseFloat($("#PayMChangePlanConfirm_OldPlanValue").val());
    var newPlanName = $("#PayMChangePlanConfirm_NewPlan").text();
    var newPlanValue = parseFloat($("#PayMChangePlanConfirm_NewPlanValue").val());
    var newPlanBundleId = $("#PayMChangePlanConfirm_NewBundle").val();

    //10-Jul-2019 : Commented as discussed with Logu, that there is upgrade, always allow to process the request no need to show the payment page
    //if (oldPlanValue < newPlanValue) {
    //    // Open up the payment confirmation 
    //    $("#PayMChangePlanOrder_OldPlan").val(oldPlanName);
    //    $("#PayMChangePlanOrder_OldPlanValue").val(oldPlanValue);
    //    $("#PayMChangePlanOrder_NewPlan").text(newPlanName);
    //    $("#PayMChangePlanOrder_NewPlanValue").val(newPlanValue);
    //    $("#PayMChangePlanOrder_NewBundle").val(newPlanBundleId);
    //    $("#PayMChangePlanOrder_OrderValue").val(newPlanValue - oldPlanValue);
    //    ApplyPayMChangePlanOrderVoucher();

    //    var url = apiServer + "/api/PAYMPlanBundles";
    //    var JSONSendData = {
    //        requestType: 5,
    //        BundleId: newPlanBundleId,
    //        ProductCode: $('.clsproductcode')[0].innerHTML
    //    };
    //    $.ajax
    //    ({
    //        url: url,
    //        type: "post",
    //        data: JSON.stringify(JSONSendData),
    //        dataType: "json",
    //        async: true,
    //        contentType: "application/json;charset=utf-8",
    //        cache: false,
    //        success: function (feedback) {
    //            if (feedback.length > 0) {
    //                if (feedback[0].errcode == 0) {
    //                    var PlanDescription = feedback[0].PlanDescription.split("|").join("<br />");
    //                    $("#PayMChangePlanOrder_PlanDetails").html(PlanDescription);
    //                    $('#divPayMChangePlanOrder').modal({ keyboard: false, backdrop: 'static' }).css({
    //                        'margin-left': function () {
    //                            return window.pageXOffset - ($(this).width() / 2);
    //                        }
    //                    });
    //                }
    //                else {
    //                    alert(feedback[0].errmsg);
    //                }
    //            }
    //        },
    //        error: function (feedback) {
    //            $("#PayMChangePlanOrder_PlanDetails").html('');
    //        }
    //    }).done(function () {

    //    });
    //} else {
    // Do the request
    //DoChangePayMPlan(MobileNo, newPlanValue, newPlanBundleId, "CRM-API");
    $("#ajax-screen-masking").show();
    var url = apiServer + "/api/PAYMPlanBundles";
    var JSONSendData = {
        requestType: 60,
        MobileNo: MobileNo,
        BundleId: newPlanBundleId,
        newplan: newPlanValue,
        newplantext: newPlanName,
        calledby: "CRM-API",
        ProductCode: $('.clsproductcode')[0].innerHTML
    };
    $.ajax
    ({
        url: url,
        type: "post",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            if (feedback[0].errcode < 0) {
                var ErrMsg = feedback[0].errmsg;
                alert(ErrMsg);
            }
            else {
                alert("Request Processed successfully");
            }
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
            alert("General Error");
        }
    }).done(function () {
        $("#ajax-screen-masking").hide();
        location.reload();
    });
    //}
}

function ApplyPayMChangePlanOrderVoucher() {
    var orderValue = parseFloat($("#PayMChangePlanOrder_OrderValue").val());
    var voucherValue = 0;
    var totalValue = orderValue - voucherValue;
    $("#PayMChangePlanOrder_OrderValueText").html('£' + orderValue);
    $("#PayMChangePlanOrder_VoucherValue").val(voucherValue);
    $("#PayMChangePlanOrder_TotalValue").val(totalValue);
    $("#PayMChangePlanOrder_TotalValueText").html('£' + totalValue);
    if (voucherValue > 0)
        $("#PayMChangePlanOrder_VoucherValueText").html('£' + voucherValue);
    else
        $("#PayMChangePlanOrder_VoucherValueText").html('');
}

function NextPayMChangePlanOrder(MobileNo) {
    $('#divPayMChangePlanOrder').modal('hide');
    CCPayMPlan(MobileNo, 1);
}

function ViewPayMCancelPlanSelect(planName, bundleId) {
    var url = apiServer + "/api/PAYMPlanBundles";
    var JSONSendData = {
        requestType: 6,
        oldplanText: planName,
        BundleId: bundleId,
        ProductCode: $('.clsproductcode')[0].innerHTML
    };
    $.ajax
    ({
        url: url,
        type: "post",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            if (feedback != null && feedback.length > 0) {
                bundleId = feedback[0].bundleid;
                planDescription = feedback[0].PlanDescription;

                $("#PayMCancelPlanSelect_CurrentPlan").val(planName);
                $("#PayMCancelPlanSelect_CurrentBundleId").val(bundleId);
                $("#PayMCancelPlanSelect_PlanDescription").val(planDescription);
                $('#divPayMCancelPlanSelect').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (feedback) {
            alert("Error unknown BundleId");
        }
    });
}

function NextPayMCancelPlanSelect(MobileNo) {
    $('#divPayMCancelPlanSelect').modal('hide');
    var planName = $("#PayMCancelPlanSelect_CurrentPlan").val();
    var planBundleId = $("#PayMCancelPlanSelect_CurrentBundleId").val();
    var PlanDescription = $("#PayMCancelPlanSelect_PlanDescription").val();

    var cancelType = $("input[name='radioPayMCancelPlanSelect']:checked").val();
    if (cancelType == 3) {
        var planValue = parseFloat(planName.substring(1, planName.length))
        var excessValue = 0;
        var outstandingValue = 0;
        var CurrentOutstandingValue = 0;

        $("#PayMCancelPlanOrder_CurrentPlan").text(planName);
        $("#PayMCancelPlanOrder_CurrentBundleId").text(planBundleId);
        $("#PayMCancelPlanOrder_BasePlanValue").val(planValue);
        $("#PayMCancelPlanOrder_BasePlanValueText").text('£' + planValue);
        $("#PayMCancelPlanOrder_PlanDescription").val(PlanDescription);
        $("#PayMCancelPlanOrder_PlanDetails").html(PlanDescription.split("|").join("<br />"));
        $("#PayMCancelPlanOrder_ExcessValue").val(excessValue);
        $("#PayMCancelPlanOrder_ExcessValueText").text('loading...');
        $("#PayMCancelPlanOrder_CurrentOutstandingValue").val(CurrentOutstandingValue);
        $("#PayMCancelPlanOrder_CurrentOutstandingValueText").text('loading...');
        $("#PayMCancelPlanOrder_OutstandingValue").val(outstandingValue);
        $("#PayMCancelPlanOrder_OutstandingValueText").text('loading...');

        var url = apiServer + "/api/PAYMPlanBundles";
        var JSONSendData = {
            requestType: 7,
            MobileNo: MobileNo,
            ProductCode: $('.clsproductcode')[0].innerHTML
        };
        $.ajax
        ({
            url: url,
            type: "post",
            data: JSON.stringify(JSONSendData),
            dataType: "json",
            async: true,
            contentType: "application/json;charset=utf-8",
            cache: false,
            success: function (feedback) {
                if (feedback != null && feedback.length > 0) {
                    excessValue = feedback[0].excessValue;
                    CurrentOutstandingValue = feedback[0].outstandingValue;
                    if (excessValue > 0)
                        excessValue = excessValue / 100;
                }
            },
            error: function (feedback) {
            }
        }).done(function () {
            var outstandingValue = planValue + excessValue + CurrentOutstandingValue;
            //15-Mar-2018 : Roung the outstandingValue
            outstandingValue = Math.round(outstandingValue * 100) / 100;
            $("#PayMCancelPlanOrder_ExcessValue").val(excessValue);
            $("#PayMCancelPlanOrder_ExcessValueText").text('£' + excessValue);
            $("#PayMCancelPlanOrder_CurrentOutstandingValue").val(CurrentOutstandingValue);
            $("#PayMCancelPlanOrder_CurrentOutstandingValueText").text('£' + CurrentOutstandingValue);
            $("#PayMCancelPlanOrder_OutstandingValue").val(outstandingValue);
            $("#PayMCancelPlanOrder_OutstandingValueText").text('£' + outstandingValue);
        });

        $('#divPayMCancelPlanOrder').modal({ keyboard: false, backdrop: 'static' }).css({
            'margin-left': function () {
                return window.pageXOffset - ($(this).width() / 2);
            }
        });
    }
    else {
        var url = apiServer + "/api/PAYMPlanBundles";
        var JSONSendData = {
            requestType: 70,
            MobileNo: MobileNo,
            BundleId: planBundleId,
            ProductCode: $('.clsproductcode')[0].innerHTML
        };
        $.ajax
        ({
            url: url,
            type: "post",
            data: JSON.stringify(JSONSendData),
            dataType: "json",
            async: true,
            contentType: "application/json;charset=utf-8",
            cache: false,
            success: function (feedback) {
                if (feedback[0].errcode < 0) {
                    var ErrMsg = feedback[0].errmsg;
                    alert(ErrMsg);
                }
                else {
                    alert('Request Processed successfully');
                }
            },
            error: function (feedback) {
                alert("General Error");
            }
        }).done(function () {
            location.reload();
        });
    }
}

function NextPayMCancelPlanOrder(MobileNo) {
    $('#divPayMCancelPlanOrder').modal('hide');
    CCPayMPlan(MobileNo, 2);
}

////Added by Elango for Jira CRMT-216 Start
function ViewPayMChangeCreditLimit(planName, bundleId) {
    $('#divPayMChangeCreditLimitPlanSelect').modal({ keyboard: false, backdrop: 'static' }).css({
        'margin-left': function () {
            return window.pageXOffset - ($(this).width() / 2);
        }
    });
    $('#tblPayMCreditlimitChange').show();
    $('#btnSubmit').show();
    $('#btnRCancel').show();
    $('#tblPayMCreditlimitChangeconfirm').hide();
    $('#btnClose').hide();
    $('#Amountext').val("");
}
$('#Amountext').blur(function () {
    var y = 5;
    var x = $(this).val();
    var remainder = x % y;
    if (remainder == 0) {
        //x is a multiple of y
    } else {
        $('#Amountext').val("");
        $('#Amountext').focus();
    }
});

function UpdatePayMChangeCreditLimit(MobileNo, CLAmount, Email) {

    if ($('#Amountext').val().trim() == "") {
        alert("Kindly provide valid credit limit amount");
        $('#Amountext').focus();
        return;
    }
    var url = apiServer + "/api/PAYMPlanBundles";
    var JSONSendData = {
        requestType: 90,
        RequestCreditAmount: CLAmount,
        MobileNo: MobileNo,
        username: Email,
        ProductCode: $('.clsproductcode')[0].innerHTML
    };
    $.ajax
    ({
        url: url,
        type: "post",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            //alert(feedback);
            //if (feedback != null && feedback.length > 0) {
            //    bundleId = feedback[0].bundleid;
            //    planDescription = feedback[0].PlanDescription;

            $('#divPayMChangeCreditLimitPlanSelect').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
            $('#btnSubmit').hide();
            $('#btnRCancel').hide();
            $('#btnRCancel').css("display:none");
            $('#tblPayMCreditlimitChangeconfirm').show();
            $('#btnClose').show();
            // }
        },
        error: function (feedback) {
            alert("Error unknown BundleId");
        }
    });
}
function ViewPayMAcceptCreditLimit(planName, bundleId) {
    var url = apiServer + "/api/PAYMPlanBundles";
    var JSONSendData = {
        requestType: 91,
        MobileNo: planName,
        ProductCode: $('.clsproductcode')[0].innerHTML
    };
    $.ajax
    ({
        url: url,
        type: "POST",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            if (feedback[0].errcode == 1) {
                $('#divPayMNorequestCreditlimit').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
                $('#errMsg').text(feedback[0].errmsg);
            }
            else {
                $('#divPayMAcceptCreditlimit').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });;
                $('#tblPayMAcceptCreditlimit').show();
                $('#btnPreApprove').show();
                $('#btnApprove').hide();
                $('#btnReject').show();
                $('#tblPayMAcceptCreditlimitConfirm').hide();
                $('#tblPayMAcceptCreditlimitPrompt').hide();
                $('#btnAClose').hide();
                $('#txtrequestcredit').hide();
                $('#PayMNorecord').hide();

                var Existingval = feedback[0].existing_credit_limit;
                var Newval = feedback[0].new_credit_limit;
                $('#lblexistingcredit').text(Existingval);
                $('#lblrequestcredit').text(Newval);
                $('#txtrequestcredit').val(Newval);
            }
        },
        error: function (feedback) {
            alert("Error unknown BundleId");
        }
    });
}

$('#btnPreApprove').click(function () {
    $('#tblPayMAcceptCreditlimitPrompt').show();
    $('#tblPayMAcceptCreditlimitConfirm').hide();
    $('#tblPayMAcceptCreditlimit').hide();
    $('#btnApprove').show();
    $('#btnPreApprove').hide();
});

$('#aEdit').click(function () {
    $('#txtrequestcredit').show();
    $('#lblrequestcredit').hide();
    $('#btnAClose').hide();
});
$('#txtrequestcredit').blur(function () {
    var y = 5;
    var x = $(this).val();
    var remainder = x % y;
    if (remainder == 0) {
        //x is a multiple of y
    } else {
        $('#txtrequestcredit').val("");
        $('#txtrequestcredit').focus();
    }
});
function ApprovePayMChangeCreditLimit(MobileNo, CreditAmount) {

    if ($('#txtrequestcredit').val().trim() == "") {
        alert("Kindly provide valid credit limit amount");
        $('#txtrequestcredit').focus();
        return;
    }

    var url = apiServer + "/api/PAYMPlanBundles";
    var JSONSendData = {
        requestType: 92,
        RequestCreditAmount: CreditAmount,
        MobileNo: MobileNo,
        ProductCode: $('.clsproductcode')[0].innerHTML
    };
    $.ajax
    ({
        url: url,
        type: "post",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('#divPayMAcceptCreditlimit').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
            $('#tblPayMAcceptCreditlimit').hide();
            $('#tblPayMAcceptCreditlimitPrompt').hide();
            $('#btnApprove').hide();
            $('#btnReject').hide();
            $('#tblPayMAcceptCreditlimitConfirm').show();
            $('#btnAClose').show();
            $('#txtrequestcredit').hide();
            $('#lblapprovecredit').text(CreditAmount);
        },
        error: function (feedback) {
            alert("Error unknown BundleId");
        }
    });
}
function RejectPayMChangeCreditLimit(MobileNo) {
    var url = apiServer + "/api/PAYMPlanBundles";
    var JSONSendData = {
        requestType: 93,
        MobileNo: MobileNo
    };
    $.ajax
    ({
        url: url,
        type: "post",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            //alert(feedback);
            //if (feedback != null && feedback.length > 0) {
            //    bundleId = feedback[0].bundleid;
            //    planDescription = feedback[0].PlanDescription;

            $('#divPayMAcceptCreditlimit').hide();

            // }
        },
        error: function (feedback) {
            alert("Error unknown BundleId");
        }
    });
};
////Added by Elango for Jira CRMT-216 End

function CCPayMPlan(MobileNo, payType) {
    var url = apiServer + '/api/payment/' + MobileNo
    $.getJSON(url, function (data) {
        $("#CCDetailsSaved").dataTable({
            aaData: data,
            bDestroy: true,
            bRetrieve: true,
            bFilter: false,
            bLengthChange: false,
            iDisplayLength: 10,
            aaSorting: [],
            oLanguage: {
                "sEmptyTable": 'No saved card found'
            },
            aoColumns: [

				{ mDataProp: "Last6digitsCC", sTitle: 'Credit Card Number' },
				{ mDataProp: "expirydate", sTitle: 'Expiry Date' },
				{
				    mDataProp: 'SubscriptionID', sTitle: 'Action', bSortable: false,
				    fnRender: function (ob) {
				        if (payType == 1) {
				            var content = '<button class="btn blue PaymentMethodBtnSavedCardProceed" onclick="CCPlanChangeConfirm(\'' + MobileNo + '\',\'' + ob.aData.SubscriptionID + '\',\'' + ob.aData.Last6digitsCC + '\',' + ob.aData.cvv_number + ')" />Select</button>';
				        }
				        else {
				            var content = '<button class="btn blue PaymentMethodBtnSavedCardProceed" onclick="CCPlanCancelConfirm(\'' + MobileNo + '\',\'' + ob.aData.SubscriptionID + '\',\'' + ob.aData.Last6digitsCC + '\',' + ob.aData.cvv_number + ')" />Select</button>';
				        }
				        return content;
				    }
				}

            ]
        });
        $("#CCDetailsSaved_wrapper .row-fluid").remove();
        $("#tblCCDetailsSavedLoader").hide();
        $("#CCDetailsSaved").show();
    });

    ShowPAYMPlan(3);
    $("button#PaymentMethodBtnCancel").live("click", function () {
        location.reload();
    });
    if (payType == 1) {
        $("#paymentMethod span#titlePaymentMethod").html(" Change Plan ");
        //$("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
        //$("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
        $("button#PaymentMethodBtnNewCardProceed").click(function () {
            if (ValidatePaymentMethodForm()) {
                CCPlanChangeConfirm(MobileNo);
            }
        });
    }
    else {
        $("#paymentMethod span#titlePaymentMethod").html(" Cancel Plan ");
        $("button#PaymentMethodBtnNewCardProceed").click(function () {
            if (ValidatePaymentMethodForm()) {
                CCPlanCancelConfirm(MobileNo);
            }
        });
    }
    $("#PaymentMethodBtnSave").unbind("click").remove();
    $("#PaymentMethodBtnNewCardProceed").show();
    $("#paymentMethod").fadeIn();
    var productCode = $('.clsproductcode')[0].innerHTML;
    if (productCode == 'VMUK' || productCode == 'DMUK') {
    }
    else {
        $('#BtnFindAddress').hide();
    }
}



function CCPlanChangeConfirm(MobileNo, subcriptionid, Last6Digit, CVVNumber) {
    $("#paymentMethod").fadeOut();

    var sitecode = $('#PayMChangePlanSelect_SiteCode').val();
    var typeCard = $("input:radio[name=PaymentMethodCardType]:checked").val();
    if (subcriptionid == null || subcriptionid == '') {
        var cardNo = $('#PaymentMethodCCNumber').val();
        var expMonth = $('#PaymentMethodExpiryMonth').val();
        var expYear = $('#PaymentMethodExpiryYear').val();
        var issueNo = $('#PaymentMethodIssueNumber').val();
        var cardVerf = $('#PaymentMethodCardVerfCode').val();
        var firstName = $('#PaymentMethodFirstName').val();
        var lastName = $('#PaymentMethodLastName').val();
        //change made on 07-07-2015 for post code
        //var postCode = $('#PaymentMethodPostcode').val();
        var postCode = $("div#postcode_lookup input[type=text]").val();
        //end change made
        var houseNo = $('#PaymentMethodHouseNo').val();
        var address = $('#PaymentMethodAddress').val();
        var city = $('#PaymentMethodCity').val();
        var country = $('select[name=PaymentMethodCountry] option:selected').val();
    } else {
        var cardNo = Last6Digit;
        var expMonth = '';
        var expYear = '';
        var issueNo = '';
        var cardVerf = CVVNumber;
        var firstName = '';
        var lastName = '';
        var postCode = '';
        var houseNo = '';
        var address = '';
        var city = '';
        var country = '';
    }

    var oldPlanName = $("#PayMChangePlanOrder_OldPlan").val();
    var oldPlanValue = parseFloat($("#PayMChangePlanOrder_OldPlanValue").val());
    var newPlanName = $("#PayMChangePlanOrder_NewPlan").text();
    var newPlanValue = parseFloat($("#PayMChangePlanOrder_NewPlanValue").val());
    var bundleId = $("#PayMChangePlanOrder_NewBundle").val();
    var voucherValue = $("#PayMChangePlanOrder_VoucherValue").val();
    var orderValue = $("#PayMChangePlanOrder_OrderValue").val();
    var totalValue = $("#PayMChangePlanOrder_TotalValue").val();

    ShowPAYMPlan(4);
    $("td#paymentConfirmPlan_OrderType").html("PAYM Change Plan");
    $("td#paymentConfirmPlan_OrderPrice").html('£' + orderValue);
    $("td#paymentConfirmPlan_OrderDiscount").html('£' + voucherValue);
    $("td#paymentConfirmPlan_OrderTotal").html('£' + totalValue);
    //03-Sep-2015 : Added value for 'You are purchasing'
    newPlanName = 'Plan';
    $("td#paymentConfirmPlan_PDPurchase").html(newPlanName);
    $("td#paymentConfirmPlan_PDMobileNo").html(MobileNo);
    $("td#paymentConfirmPlan_PDCardNumber").html(cardNo);

    $("#paymentConfirmPlanBtnCancel").live("click", function () {
        $("#paymentConfirmPlanContainer").fadeOut();
        ShowPAYMPlan(3);
        $("#paymentMethod").fadeIn();
    });

    $("#paymentConfirmPlanBtnConfirmOrder").live("click", function () {

        //var ajaxSpinnerTop = $(window).outerHeight() / 2;
        //$("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
        //$("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
        //$("#ajax-screen-masking").show();

        ShowPAYMPlan(3);
        $("#PayMBundleHistory").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblBundlesPlanHistory\"></table>");
        $("#PayMBundleHistory").css({ "min-height": "200px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });

        var url = apiServer + "/api/PAYMPlanBundles";
        var JSONSendData = {
            requestType: 61,
            SiteCode: sitecode,
            MobileNo: MobileNo,
            BundleId: bundleId,
            newplan: newPlanValue,
            newplantext: newPlanName,
            calledby: "CRM-API",
            ordervalue: orderValue,
            discount: voucherValue,
            totalvalue: totalValue,
            SubscriptionID: subcriptionid,
            card_number: cardNo,
            card_expiry_month: expMonth,
            card_expiry_year: expYear,
            card_issue_number: issueNo,
            card_verf_code: cardVerf,
            card_first_name: firstName,
            card_last_name: lastName,
            card_post_code: postCode,
            card_house_number: houseNo,
            card_address: address,
            card_city: city,
            card_country: country,
            card_type: typeCard,
            ProductCode: $('.clsproductcode')[0].innerHTML
        };
        $.ajax
        ({
            url: url,
            type: "post",
            data: JSON.stringify(JSONSendData),
            dataType: "json",
            async: true,
            contentType: "application/json;charset=utf-8",
            cache: false,
            success: function (feedback) {
                $("#ajax-screen-masking").hide();
                if (feedback != null && feedback.length > 0 && feedback[0] != null) {
                    if (feedback[0].errcode < 0) {
                        alert(feedback[0].errmsg + " - Request Processed failed");
                    }
                    else {
                        alert(feedback[0].errmsg + " - Request Processed successfully");
                    }
                }
                else {
                    alert("Empty Return");
                }
            },
            error: function (feedback) {
                $("#ajax-screen-masking").hide();
                if (feedback != null && feedback.length > 0 && feedback[0] != null) {
                    if (feedback[0].errcode < 0) {
                        alert(feedback[0].errmsg + " - Request Processed failed");
                    }
                    else {
                        alert(feedback[0].errmsg + " - Request Processed successfully");
                    }
                }
                else {
                    alert("General Error");
                }
            }
        }).done(function () {
            location.reload();
        });
    });

    $("#paymentConfirmPlanContainer").fadeIn();
}


/*Added on 21 Jan 2015 - by Hari - Reactivate LLOM Confirm*/
function CCReactivateLLOMConfirm(MobileNo, subcriptionid, Last6Digit, CVVNumber, RegNo, sitecode, SvcId, UserName, payType, Price) {
    $("#paymentMethod").fadeOut();

    //var sitecode = $('#PayMChangePlanSelect_SiteCode').val();
    var typeCard = $("input:radio[name=PaymentMethodCardType]:checked").val();
    if (subcriptionid == null || subcriptionid == '') {
        var cardNo = $('#PaymentMethodCCNumber').val();
        var expMonth = $('#PaymentMethodExpiryMonth').val();
        var expYear = $('#PaymentMethodExpiryYear').val();
        var issueNo = $('#PaymentMethodIssueNumber').val();
        var cardVerf = $('#PaymentMethodCardVerfCode').val();
        var firstName = $('#PaymentMethodFirstName').val();
        var lastName = $('#PaymentMethodLastName').val();
        //change made on 07-07-2015 for post code
        //var postCode = $('#PaymentMethodPostcode').val();
        var postCode = $("div#postcode_lookup input[type=text]").val();
        //end change made
        var houseNo = $('#PaymentMethodHouseNo').val();
        var address = $('#PaymentMethodAddress').val();
        var city = $('#PaymentMethodCity').val();
        var country = $('select[name=PaymentMethodCountry] option:selected').val();
    } else {
        var cardNo = Last6Digit;
        var expMonth = '';
        var expYear = '';
        var issueNo = '';
        var cardVerf = CVVNumber;
        var firstName = '';
        var lastName = '';
        var postCode = '';
        var houseNo = '';
        var address = '';
        var city = '';
        var country = '';
    }

    var oldPlanName = $("#PayMChangePlanOrder_OldPlan").val();
    var oldPlanValue = parseFloat($("#PayMChangePlanOrder_OldPlanValue").val());
    var newPlanName = $("#PayMChangePlanOrder_NewPlan").text();
    var newPlanValue = parseFloat($("#PayMChangePlanOrder_NewPlanValue").val());
    var bundleId = $("#PayMChangePlanOrder_NewBundle").val();
    var voucherValue = $("#PayMChangePlanOrder_VoucherValue").val();
    var orderValue = Price; //$("#PayMChangePlanOrder_OrderValue").val();
    var totalValue = Price; //$("#PayMChangePlanOrder_TotalValue").val();

    ShowPAYMPlan(4);
    $("td#paymentConfirmPlan_OrderType").html("PAYM Re-Activate LLOM");
    $("td#paymentConfirmPlan_OrderPrice").html('£' + Price);
    $("td#paymentConfirmPlan_OrderDiscount").html('£' + '0');
    $("td#paymentConfirmPlan_OrderTotal").html('£' + Price);
    //03-Sep-2015 : Added value for 'You are purchasing'
    newPlanName = 'LLOM';
    $("td#paymentConfirmPlan_PDPurchase").html(newPlanName);
    $("td#paymentConfirmPlan_PDMobileNo").html(MobileNo);
    $("td#paymentConfirmPlan_PDCardNumber").html(cardNo);

    $("#paymentConfirmPlanBtnCancel").live("click", function () {
        $("#paymentConfirmPlanContainer").fadeOut();
        ShowPAYMPlan(3);
        $("#paymentMethod").fadeIn();
    });

    $("#paymentConfirmPlanBtnConfirmOrder").live("click", function () {


        //var ajaxSpinnerTop = $(window).outerHeight() / 2;
        //$("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
        //$("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
        //$("#ajax-screen-masking").show();

        ShowPAYMPlan(3);
        $("#PayMBundleHistory").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblBundlesPlanHistory\"></table>");
        $("#PayMBundleHistory").css({ "min-height": "200px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });

        var url = apiServer + "/api/PAYMPlanBundles";
        var JSONSendData = {
            requestType: 81,
            SiteCode: sitecode,
            MobileNo: MobileNo,
            BundleId: bundleId,
            newplan: newPlanValue,
            newplantext: newPlanName,
            calledby: "CRM-API",
            ordervalue: orderValue,
            discount: voucherValue,
            totalvalue: totalValue,
            SubscriptionID: subcriptionid,
            card_number: cardNo,
            card_expiry_month: expMonth,
            card_expiry_year: expYear,
            card_issue_number: issueNo,
            card_verf_code: cardVerf,
            card_first_name: firstName,
            card_last_name: lastName,
            card_post_code: postCode,
            card_house_number: houseNo,
            card_address: address,
            card_city: city,
            card_country: country,
            card_type: typeCard,
            Reg_Number: RegNo,
            SvcId: SvcId,
            username: UserName,
            payment_mode: payType,
            ProductCode: $('.clsproductcode')[0].innerHTML,
            mode: $("input[name=ModeReactivateLLOM]").val()
        };
        $.ajax
        ({
            url: url,
            type: "post",
            data: JSON.stringify(JSONSendData),
            dataType: "json",
            async: true,
            contentType: "application/json;charset=utf-8",
            cache: false,
            success: function (feedback) {
                $("#ajax-screen-masking").hide();
                if (feedback != null && feedback.length > 0 && feedback[0] != null) {
                    if (feedback[0].errcode < 0) {
                        alert(feedback[0].errmsg + " - Request Processed failed");
                    }
                    else {
                        alert(feedback[0].errmsg + " - Request Processed successfully");
                    }
                }
                else {
                    alert("Empty Return");
                }
            },
            error: function (feedback) {
                $("#ajax-screen-masking").hide();
                if (feedback != null && feedback.length > 0 && feedback[0] != null) {
                    if (feedback[0].errcode < 0) {
                        alert(feedback[0].errmsg + " - Request Processed failed");
                    }
                    else {
                        alert(feedback[0].errmsg + " - Request Processed successfully");
                    }
                }
                else {
                    alert("General Error");
                }
            }
        }).done(function () {
            location.reload();
        });
    });

    $("#paymentConfirmPlanContainer").fadeIn();
}
/*End added*/


/*Added on 21 Jan 2015 - by Hari - Reactivate LLOM Confirm*/

function CCReactivateBundleConfirm(MobileNo, subcriptionid, Last6Digit, CVVNumber, RegNo, sitecode, BundleId, UserName, payType, price) {
    $("#paymentMethod").fadeOut();

    //var sitecode = $('#PayMChangePlanSelect_SiteCode').val();
    var typeCard = $("input:radio[name=PaymentMethodCardType]:checked").val();
    if (subcriptionid == null || subcriptionid == '') {
        var cardNo = $('#PaymentMethodCCNumber').val();
        var expMonth = $('#PaymentMethodExpiryMonth').val();
        var expYear = $('#PaymentMethodExpiryYear').val();
        var issueNo = $('#PaymentMethodIssueNumber').val();
        var cardVerf = $('#PaymentMethodCardVerfCode').val();
        var firstName = $('#PaymentMethodFirstName').val();
        var lastName = $('#PaymentMethodLastName').val();
        //change made on 07-07-2015 for post code
        //var postCode = $('#PaymentMethodPostcode').val();
        var postCode = $("div#postcode_lookup input[type=text]").val();
        //end changed
        var houseNo = $('#PaymentMethodHouseNo').val();
        var address = $('#PaymentMethodAddress').val();
        var city = $('#PaymentMethodCity').val();
        var country = $('select[name=PaymentMethodCountry] option:selected').val();
    } else {
        var cardNo = Last6Digit;
        var expMonth = '';
        var expYear = '';
        var issueNo = '';
        var cardVerf = CVVNumber;
        var firstName = '';
        var lastName = '';
        var postCode = '';
        var houseNo = '';
        var address = '';
        var city = '';
        var country = '';
    }

    var oldPlanName = $("#PayMChangePlanOrder_OldPlan").val();
    var oldPlanValue = parseFloat($("#PayMChangePlanOrder_OldPlanValue").val());
    var newPlanName = $("#PayMChangePlanOrder_NewPlan").text();
    var newPlanValue = parseFloat($("#PayMChangePlanOrder_NewPlanValue").val());
    var bundleId = $("#PayMChangePlanOrder_NewBundle").val();
    var voucherValue = $("#PayMChangePlanOrder_VoucherValue").val();
    var orderValue = price;//$("#PayMChangePlanOrder_OrderValue").val();
    var totalValue = price;//$("#PayMChangePlanOrder_TotalValue").val();

    ShowPAYMPlan(4);
    $("td#paymentConfirmPlan_OrderType").html("PAYM Re-Activate Bundle");
    $("td#paymentConfirmPlan_OrderPrice").html('£' + price);
    $("td#paymentConfirmPlan_OrderDiscount").html('£' + '0');
    $("td#paymentConfirmPlan_OrderTotal").html('£' + price);
    //03-Sep-2015 : Added value for 'You are purchasing'
    newPlanName = 'Bundle';
    $("td#paymentConfirmPlan_PDPurchase").html(newPlanName);
    $("td#paymentConfirmPlan_PDMobileNo").html(MobileNo);
    $("td#paymentConfirmPlan_PDCardNumber").html(cardNo);

    $("#paymentConfirmPlanBtnCancel").live("click", function () {
        $("#paymentConfirmPlanContainer").fadeOut();
        ShowPAYMPlan(3);
        $("#paymentMethod").fadeIn();
    });

    $("#paymentConfirmPlanBtnConfirmOrder").live("click", function () {

        //var ajaxSpinnerTop = $(window).outerHeight() / 2;
        //$("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
        //$("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
        //$("#ajax-screen-masking").show();

        ShowPAYMPlan(3);
        $("#PayMBundleHistory").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblBundlesPlanHistory\"></table>");
        $("#PayMBundleHistory").css({ "min-height": "200px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });

        var url = apiServer + "/api/PAYMPlanBundles";
        var JSONSendData = {
            requestType: 80,
            SiteCode: sitecode,
            MobileNo: MobileNo,
            BundleId: BundleId,
            newplan: newPlanValue,
            newplantext: newPlanName,
            calledby: "CRM-API",
            ordervalue: orderValue,
            discount: voucherValue,
            totalvalue: totalValue,
            SubscriptionID: subcriptionid,
            card_number: cardNo,
            card_expiry_month: expMonth,
            card_expiry_year: expYear,
            card_issue_number: issueNo,
            card_verf_code: cardVerf,
            card_first_name: firstName,
            card_last_name: lastName,
            card_post_code: postCode,
            card_house_number: houseNo,
            card_address: address,
            card_city: city,
            card_country: country,
            card_type: typeCard,
            Reg_Number: RegNo,
            SvcId: 0,
            username: UserName,
            payment_mode: payType,
            ProductCode: $('.clsproductcode')[0].innerHTML
        };
        $.ajax
        ({
            url: url,
            type: "post",
            data: JSON.stringify(JSONSendData),
            dataType: "json",
            async: true,
            contentType: "application/json;charset=utf-8",
            cache: false,
            success: function (feedback) {
                $("#ajax-screen-masking").hide();
                if (feedback != null && feedback.length > 0 && feedback[0] != null) {
                    if (feedback[0].errcode < 0) {
                        alert(feedback[0].errmsg + " - Request Processed failed");
                    }
                    else {
                        alert(feedback[0].errmsg + " - Request Processed successfully");
                    }
                }
                else {
                    alert("Empty Return");
                }
            },
            error: function (feedback) {
                $("#ajax-screen-masking").hide();
                if (feedback != null && feedback.length > 0 && feedback[0] != null) {
                    if (feedback[0].errcode < 0) {
                        alert(feedback[0].errmsg + " - Request Processed failed");
                    }
                    else {
                        alert(feedback[0].errmsg + " - Request Processed successfully");
                    }
                }
                else {
                    alert("General Error");
                }
            }
        }).done(function () {
            location.reload();
        });
    });

    $("#paymentConfirmPlanContainer").fadeIn();
}
/*End added*/

function CCPlanCancelConfirm(MobileNo, subcriptionid, Last6Digit, CVVNumber) {
    $("#paymentMethod").fadeOut();

    var sitecode = $('#PayMChangePlanSelect_SiteCode').val();
    var typeCard = $("input:radio[name=PaymentMethodCardType]:checked").val();
    if (subcriptionid == null || subcriptionid == '') {
        var cardNo = $('#PaymentMethodCCNumber').val();
        var expMonth = $('#PaymentMethodExpiryMonth').val();
        var expYear = $('#PaymentMethodExpiryYear').val();
        var issueNo = $('#PaymentMethodIssueNumber').val();
        var cardVerf = $('#PaymentMethodCardVerfCode').val();
        var firstName = $('#PaymentMethodFirstName').val();
        var lastName = $('#PaymentMethodLastName').val();
        //change made on 07-07-2015 for post code
        //var postCode = $('#PaymentMethodPostcode').val();
        var postCode = $("div#postcode_lookup input[type=text]").val();
        //end changed
        var houseNo = $('#PaymentMethodHouseNo').val();
        var address = $('#PaymentMethodAddress').val();
        var city = $('#PaymentMethodCity').val();
        var country = $('select[name=PaymentMethodCountry] option:selected').val();
    } else {
        var cardNo = Last6Digit;
        var expMonth = '';
        var expYear = '';
        var issueNo = '';
        var cardVerf = CVVNumber;
        var firstName = '';
        var lastName = '';
        var postCode = '';
        var houseNo = '';
        var address = '';
        var city = '';
        var country = '';
    }

    var planName = $("#PayMCancelPlanSelect_CurrentPlan").val();
    var planBundleId = $("#PayMCancelPlanSelect_CurrentBundleId").val();
    var PlanDescription = $("#PayMCancelPlanSelect_PlanDescription").val();
    var planValue = $("#PayMCancelPlanOrder_BasePlanValue").val();
    var planValueText = $("#PayMCancelPlanOrder_BasePlanValueText").val();
    var excessValue = $("#PayMCancelPlanOrder_ExcessValue").val();
    var excessValueText = $("#PayMCancelPlanOrder_ExcessValueText").val();
    var CurrentOutstandingValue = $("#PayMCancelPlanOrder_CurrentOutstandingValue").val();
    var CurrentOutstandingValueText = $("#PayMCancelPlanOrder_CurrentOutstandingValueText").val();
    var outstandingValue = $("#PayMCancelPlanOrder_OutstandingValue").val();
    var outstandingValueText = $("#PayMCancelPlanOrder_OutstandingValueText").val();
    var voucherValue = 0;
    var totalValue = outstandingValue - voucherValue;

    ShowPAYMPlan(4);
    $("td#paymentConfirmPlan_OrderType").html("PAYM Cancel Plan");
    $("td#paymentConfirmPlan_OrderPrice").html('£' + outstandingValue);
    $("td#paymentConfirmPlan_OrderDiscount").html('£' + voucherValue);
    $("td#paymentConfirmPlan_OrderTotal").html('£' + totalValue);
    $("td#paymentConfirmPlan_PDPurchase").html(planName);
    $("td#paymentConfirmPlan_PDMobileNo").html(MobileNo);
    $("td#paymentConfirmPlan_PDCardNumber").html(cardNo);

    $("#paymentConfirmPlanBtnCancel").live("click", function () {
        $("#paymentConfirmPlanContainer").fadeOut();
        ShowPAYMPlan(3);
        $("#paymentMethod").fadeIn();
    });

    $("#paymentConfirmPlanBtnConfirmOrder").live("click", function () {

        //var ajaxSpinnerTop = $(window).outerHeight() / 2;
        //$("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
        //$("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
        //$("#ajax-screen-masking").show();

        ShowPAYMPlan(3);
        $("#PayMBundleHistory").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblBundlesPlanHistory\"></table>");
        $("#PayMBundleHistory").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });

        var url = apiServer + "/api/PAYMPlanBundles";
        var JSONSendData = {
            requestType: 71,
            SiteCode: sitecode,
            MobileNo: MobileNo,
            BundleId: planBundleId,
            newplan: planValue,
            newplantext: planName,
            calledby: "CRM-API",
            ordervalue: outstandingValue,
            discount: voucherValue,
            totalvalue: totalValue,
            SubscriptionID: subcriptionid,
            card_number: cardNo,
            card_expiry_month: expMonth,
            card_expiry_year: expYear,
            card_issue_number: issueNo,
            card_verf_code: cardVerf,
            card_first_name: firstName,
            card_last_name: lastName,
            card_post_code: postCode,
            card_house_number: houseNo,
            card_address: address,
            card_city: city,
            card_country: country,
            card_type: typeCard,
            ProductCode: $('.clsproductcode')[0].innerHTML
        };
        $.ajax
        ({
            url: url,
            type: "post",
            data: JSON.stringify(JSONSendData),
            dataType: "json",
            async: true,
            contentType: "application/json;charset=utf-8",
            cache: false,
            success: function (feedback) {
                $("#ajax-screen-masking").hide();
                if (feedback != null && feedback.length > 0 && feedback[0] != null) {
                    if (feedback[0].errcode < 0) {
                        alert(feedback[0].errmsg + " - Request Processed failed");
                    }
                    else {
                        alert(feedback[0].errmsg + " - Request Processed successfully");
                    }
                }
                else {
                    alert("Empty Return");
                }
            },
            error: function (feedback) {
                $("#ajax-screen-masking").hide();
                if (feedback != null && feedback.length > 0 && feedback[0] != null) {
                    if (feedback[0].errcode < 0) {
                        alert(feedback[0].errmsg + " - Request Processed failed");
                    }
                    else {
                        alert(feedback[0].errmsg + " - Request Processed successfully");
                    }
                }
                else {
                    alert("General Error");
                }
            }
        }).done(function () {
            location.reload();
        });
    });

    $("#paymentConfirmPlanContainer").fadeIn();
}


function CCReactivateConfirmCountrySaver(MobileNo, subcriptionid, Last6Digit, CVVNumber, payType, sitecode, bundleid, processby) {
    $("#paymentMethod").fadeOut();

    //var sitecode = $('#PayMChangePlanSelect_SiteCode').val();
    var typeCard = $("input:radio[name=PaymentMethodCardType]:checked").val();
    if (subcriptionid == null || subcriptionid == '') {
        var cardNo = $('#PaymentMethodCCNumber').val();
        var expMonth = $('#PaymentMethodExpiryMonth').val();
        var expYear = $('#PaymentMethodExpiryYear').val();
        var issueNo = $('#PaymentMethodIssueNumber').val();
        var cardVerf = $('#PaymentMethodCardVerfCode').val();
        var firstName = $('#PaymentMethodFirstName').val();
        var lastName = $('#PaymentMethodLastName').val();
        //change made on 07-07-2015 for post code
        //var postCode = $('#PaymentMethodPostcode').val();
        var postCode = $("div#postcode_lookup input[type=text]").val();
        //end changed
        var houseNo = $('#PaymentMethodHouseNo').val();
        var address = $('#PaymentMethodAddress').val();
        var city = $('#PaymentMethodCity').val();
        var country = $('select[name=PaymentMethodCountry] option:selected').val();
    } else {
        var cardNo = Last6Digit;
        var expMonth = '';
        var expYear = '';
        var issueNo = '';
        var cardVerf = CVVNumber;
        var firstName = '';
        var lastName = '';
        var postCode = '';
        var houseNo = '';
        var address = '';
        var city = '';
        var country = '';
    }

    var planName = $("#PayMCancelPlanSelect_CurrentPlan").val();
    //var planBundleId = $("#PayMCancelPlanSelect_CurrentBundleId").val();
    var planBundleId = bundleid;
    var PlanDescription = $("#PayMCancelPlanSelect_PlanDescription").val();
    var planValue = $("#PayMCancelPlanOrder_BasePlanValue").val();
    var planValueText = $("#PayMCancelPlanOrder_BasePlanValueText").val();
    var excessValue = $("#PayMCancelPlanOrder_ExcessValue").val();
    var excessValueText = $("#PayMCancelPlanOrder_ExcessValueText").val();
    var CurrentOutstandingValue = $("#PayMCancelPlanOrder_CurrentOutstandingValue").val();
    var CurrentOutstandingValueText = $("#PayMCancelPlanOrder_CurrentOutstandingValueText").val();
    var outstandingValue = $("#PayMCancelPlanOrder_OutstandingValue").val();
    var outstandingValueText = $("#PayMCancelPlanOrder_OutstandingValueText").val();
    var voucherValue = 0;
    var totalValue = outstandingValue - voucherValue;

    ShowPAYMPlan(4);
    $("td#paymentConfirmPlan_OrderType").html("PAYM Cancel Plan");
    $("td#paymentConfirmPlan_OrderPrice").html('£' + outstandingValue);
    $("td#paymentConfirmPlan_OrderDiscount").html('£' + voucherValue);
    $("td#paymentConfirmPlan_OrderTotal").html('£' + totalValue);
    //03-Sep-2015 : Added value for 'You are purchasing'
    planName = 'Country Saver';
    $("td#paymentConfirmPlan_PDPurchase").html(planName);
    $("td#paymentConfirmPlan_PDMobileNo").html(MobileNo);
    $("td#paymentConfirmPlan_PDCardNumber").html(cardNo);

    $("#paymentConfirmPlanBtnCancel").live("click", function () {
        $("#paymentConfirmPlanContainer").fadeOut();
        ShowPAYMPlan(3);
        $("#paymentMethod").fadeIn();
    });

    $("#paymentConfirmPlanBtnConfirmOrder").live("click", function () {

        //var ajaxSpinnerTop = $(window).outerHeight() / 2;
        //$("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
        //$("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
        //$("#ajax-screen-masking").show();

        ShowPAYMPlan(3);
        $("#PayMBundleHistory").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblBundlesPlanHistory\"></table>");
        $("#PayMBundleHistory").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });

        var url = apiServer + "/api/PAYMPlanBundles";
        var JSONSendData = {
            requestType: 82,
            SiteCode: sitecode,
            MobileNo: MobileNo,
            BundleId: planBundleId,
            newplan: planValue,
            newplantext: planName,
            calledby: "CRM-API",
            ordervalue: outstandingValue,
            discount: voucherValue,
            totalvalue: totalValue,
            SubscriptionID: subcriptionid,
            card_number: cardNo,
            card_expiry_month: expMonth,
            card_expiry_year: expYear,
            card_issue_number: issueNo,
            card_verf_code: cardVerf,
            card_first_name: firstName,
            card_last_name: lastName,
            card_post_code: postCode,
            card_house_number: houseNo,
            card_address: address,
            card_city: city,
            card_country: country,
            card_type: typeCard,
            ProductCode: $('.clsproductcode')[0].innerHTML
        };
        $.ajax
        ({
            url: url,
            type: "post",
            data: JSON.stringify(JSONSendData),
            dataType: "json",
            async: true,
            contentType: "application/json;charset=utf-8",
            cache: false,
            success: function (feedback) {
                $("#ajax-screen-masking").hide();
                if (feedback != null && feedback.length > 0 && feedback[0] != null) {
                    if (feedback[0].errcode < 0) {
                        alert(feedback[0].errmsg + " - Request Processed failed");
                    }
                    else {
                        alert(feedback[0].errmsg + " - Request Processed successfully");
                    }
                }
                else {
                    alert("Empty Return");
                }
            },
            error: function (feedback) {
                $("#ajax-screen-masking").hide();
                if (feedback != null && feedback.length > 0 && feedback[0] != null) {
                    if (feedback[0].errcode < 0) {
                        alert(feedback[0].errmsg + " - Request Processed failed");
                    }
                    else {
                        alert(feedback[0].errmsg + " - Request Processed successfully");
                    }
                }
                else {
                    alert("General Error");
                }
            }
        }).done(function () {
            location.reload();
        });
    });

    $("#paymentConfirmPlanContainer").fadeIn();
}


function getCountryList(productcode) {



    var url = apiServer + '/api/customer2in1/';

    jQuery.support.cors = true;
    var JSONSendData = {
        order_type: 4,
        product_code: productcode
    };

    $.ajax
    ({
        url: url,
        type: 'POST',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("[name=imsi_country]").attr("disabled", true);
            $("img#countryListLoader").show();
        },
        success: function (data) {

            var optionData = "";
            if (data.length > 0) {
                for (i = 0; i < data.length; i++) {
                    if (productcode != 'VMUK' && productcode != 'DMUK') {
                        optionData += "<option value=\"" + data[i].newcountry + "\">" + data[i].newcountry + "</option>";
                    } else {
                        if (data[i].newcountry != 'UK') {
                            optionData += "<option value=\"" + data[i].newcountry + "\">" + data[i].newcountry + "</option>";
                        }
                    }
                }
            }
            $("[name=imsi_country]").append(optionData).removeAttr("disabled");
            $("img#countryListLoader").hide();
        }
    });
}

/*Added by Hari - 21-01-2015 -for LLOM Reactivate*/
//function CCPayMPlanLLOM(MobileNo, payType) {
function CCPayMPlanLLOM(MobileNo, RegNo, Sitecode, SvcId, UserName, payType, Price, Mode) {

    var url = apiServer + '/api/payment/' + MobileNo
    $.getJSON(url, function (data) {
        $("#CCDetailsSaved").dataTable({
            aaData: data,
            bDestroy: true,
            bRetrieve: true,
            bFilter: false,
            bLengthChange: false,
            iDisplayLength: 10,
            aaSorting: [],
            oLanguage: {
                "sEmptyTable": 'No saved card found'
            },
            aoColumns: [

				{ mDataProp: "Last6digitsCC", sTitle: 'Credit Card Number' },
				{ mDataProp: "expirydate", sTitle: 'Expiry Date' },
				{
				    mDataProp: 'SubscriptionID', sTitle: 'Action', bSortable: false,
				    fnRender: function (ob) {
				        if (payType == 1) {
				            var content = '<button class="btn blue PaymentMethodBtnSavedCardProceed" onclick="CCReactivateLLOMConfirm(\'' + MobileNo + '\',\'' + ob.aData.SubscriptionID + '\',\'' + ob.aData.Last6digitsCC + '\',\'' + ob.aData.cvv_number + '\',\'' + RegNo + '\',\'' + Sitecode + '\',\'' + SvcId + '\',\'' + UserName + '\',\'' + payType + '\',\'' + Price + '\')" />Select</button>';
				        }
				        else {
				            var content = '<button class="btn blue PaymentMethodBtnSavedCardProceed" onclick="CCReactivateLLOMConfirm(\'' + MobileNo + '\',\'' + ob.aData.SubscriptionID + '\',\'' + ob.aData.Last6digitsCC + '\',\'' + ob.aData.cvv_number + '\',\'' + RegNo + '\',\'' + Sitecode + '\',\'' + SvcId + '\',\'' + UserName + '\',\'' + payType + '\',\'' + Price + '\')" />Select</button>';
				        }
				        return content;
				    }
				}

            ]
        });

        var nowDate = new Date;
        var nowYear = nowDate.getFullYear();
        var strExpireYear = "";
        $("select[name=expiry_year]").html("").append("<option value=\"0\" selected>Select Year</option>");
        for (var i = nowYear; i <= (nowYear + 5) ; i++) {
            strExpireYear += "<option value=\"" + i + "\">" + i + "</option>";
        }
        $("select[name=PaymentMethodExpiryYear]").append(strExpireYear);

        $("#CCDetailsSaved_wrapper .row-fluid").remove();
        $("#tblCCDetailsSavedLoader").hide();
        $("#CCDetailsSaved").show();

        $("#postcode_lookup").children('select').hide();
    });

    ShowPAYMPlan(3);
    $("button#PaymentMethodBtnCancel").live("click", function () {
        location.reload();
    });
    //if (payType == 1) {
    $("#paymentMethod span#titlePaymentMethod").html(" Change Plan ");
    //$("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    //$("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    $("button#PaymentMethodBtnNewCardProceed").click(function () {
        if (ValidatePaymentMethodForm()) {
            //CCPlanChangeConfirm(MobileNo);
            CCReactivateLLOMConfirm(MobileNo, null, null, null, RegNo, Sitecode, SvcId, UserName, payType, Price);
        }
    });
    //}
    /*
    else {
        $("#paymentMethod span#titlePaymentMethod").html(" Cancel Plan ");
        $("button#PaymentMethodBtnNewCardProceed").click(function () {
            if (ValidatePaymentMethodForm()) {
                CCPlanCancelConfirm(MobileNo);
            }
        });
    }
    */
    $("#PaymentMethodBtnSave").unbind("click").remove();
    $("#PaymentMethodBtnNewCardProceed").show();
    $("#paymentMethod").fadeIn();
    //$("#BtnFindAddress").hide();



}


//function CCPayMPlanLLOM(MobileNo, payType) {
//function CCPAYMPAYNOW(MobileNo, RegNo, Sitecode, SvcId, UserName, payType, Price, Mode) {
function CCPAYMPAYNOW(MobileNo, RegNo) {
    //    alert("test");
    var url = apiServer + '/api/payment/' + MobileNo
    $.getJSON(url, function (data) {
        $("#CCDetailsSaved").dataTable({
            aaData: data,
            bDestroy: true,
            bRetrieve: true,
            bFilter: false,
            bLengthChange: false,
            iDisplayLength: 10,
            aaSorting: [],
            oLanguage: {
                "sEmptyTable": 'No saved card found'
            },
            aoColumns: [

				{ mDataProp: "Last6digitsCC", sTitle: 'Credit Card Number' },
				{ mDataProp: "expirydate", sTitle: 'Expiry Date' },
				{
				    mDataProp: 'SubscriptionID', sTitle: 'Action', bSortable: false,
				    fnRender: function (ob) {
				        if (payType == 1) {
				            var content = '<button class="btn blue PaymentMethodBtnSavedCardProceed" onclick="CCReactivateLLOMConfirm(\'' + MobileNo + '\',\'' + ob.aData.SubscriptionID + '\',\'' + ob.aData.Last6digitsCC + '\',\'' + ob.aData.cvv_number + '\',\'' + RegNo + '\',\'' + Sitecode + '\',\'' + SvcId + '\',\'' + UserName + '\',\'' + payType + '\',\'' + Price + '\')" />Select</button>';
				        }
				        else {
				            var content = '<button class="btn blue PaymentMethodBtnSavedCardProceed" onclick="CCReactivateLLOMConfirm(\'' + MobileNo + '\',\'' + ob.aData.SubscriptionID + '\',\'' + ob.aData.Last6digitsCC + '\',\'' + ob.aData.cvv_number + '\',\'' + RegNo + '\',\'' + Sitecode + '\',\'' + SvcId + '\',\'' + UserName + '\',\'' + payType + '\',\'' + Price + '\')" />Select</button>';
				        }
				        return content;
				    }
				}

            ]
        });

        var nowDate = new Date;
        var nowYear = nowDate.getFullYear();
        var strExpireYear = "";
        $("select[name=expiry_year]").html("").append("<option value=\"0\" selected>Select Year</option>");
        for (var i = nowYear; i <= (nowYear + 5) ; i++) {
            strExpireYear += "<option value=\"" + i + "\">" + i + "</option>";
        }
        $("select[name=PaymentMethodExpiryYear]").append(strExpireYear);

        $("#CCDetailsSaved_wrapper .row-fluid").remove();
        $("#tblCCDetailsSavedLoader").hide();
        $("#CCDetailsSaved").show();

        $("#postcode_lookup").children('select').hide();
    });

    ShowPAYMPlan(3);
    $("button#PaymentMethodBtnCancel").live("click", function () {
        location.reload();
    });
    //if (payType == 1) {
    $("#paymentMethod span#titlePaymentMethod").html(" Change Plan ");
    //$("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    //$("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    $("button#PaymentMethodBtnNewCardProceed").click(function () {
        if (ValidatePaymentMethodForm()) {
            //CCPlanChangeConfirm(MobileNo);
            CCReactivateLLOMConfirm(MobileNo, null, null, null, RegNo, Sitecode, SvcId, UserName, payType, Price);
        }
    });
    //}
    /*
    else {
        $("#paymentMethod span#titlePaymentMethod").html(" Cancel Plan ");
        $("button#PaymentMethodBtnNewCardProceed").click(function () {
            if (ValidatePaymentMethodForm()) {
                CCPlanCancelConfirm(MobileNo);
            }
        });
    }
    */
    $("#PaymentMethodBtnSave").unbind("click").remove();
    $("#PaymentMethodBtnNewCardProceed").show();
    $("#paymentMethod").fadeIn();
    //$("#BtnFindAddress").hide();



}

function CCPayMPlanBundle(MobileNo, RegNo, Sitecode, BundleId, UserName, payType, price) {
    var url = apiServer + '/api/payment/' + MobileNo
    $.getJSON(url, function (data) {
        $("#CCDetailsSaved").dataTable({
            aaData: data,
            bDestroy: true,
            bRetrieve: true,
            bFilter: false,
            bLengthChange: false,
            iDisplayLength: 10,
            aaSorting: [],
            oLanguage: {
                "sEmptyTable": 'No saved card found'
            },
            aoColumns: [

				{ mDataProp: "Last6digitsCC", sTitle: 'Credit Card Number' },
				{ mDataProp: "expirydate", sTitle: 'Expiry Date' },
				{
				    mDataProp: 'SubscriptionID', sTitle: 'Action', bSortable: false,
				    fnRender: function (ob) {
				        if (payType == 1) {
				            var content = '<button class="btn blue PaymentMethodBtnSavedCardProceed" onclick="CCReactivateBundleConfirm(\'' + MobileNo + '\',\'' + ob.aData.SubscriptionID + '\',\'' + ob.aData.Last6digitsCC + '\',' + ob.aData.cvv_number + ',' + RegNo + ',\'' + Sitecode + '\',\'' + BundleId + '\',\'' + UserName + '\',\'' + payType + '\',\'' + price + '\')" />Select</button>';
				        }
				        else {
				            var content = '<button class="btn blue PaymentMethodBtnSavedCardProceed" onclick="CCReactivateBundleConfirm(\'' + MobileNo + '\',\'' + ob.aData.SubscriptionID + '\',\'' + ob.aData.Last6digitsCC + '\',' + ob.aData.cvv_number + ',' + RegNo + ',\'' + Sitecode + '\',\'' + BundleId + '\',\'' + UserName + '\',\'' + payType + '\',\'' + price + '\')" />Select</button>';
				        }
				        return content;
				    }
				}

            ]
        });

        var nowDate = new Date;
        var nowYear = nowDate.getFullYear();
        var strExpireYear = "";
        $("select[name=expiry_year]").html("").append("<option value=\"0\" selected>Select Year</option>");
        for (var i = nowYear; i <= (nowYear + 5) ; i++) {
            strExpireYear += "<option value=\"" + i + "\">" + i + "</option>";
        }
        $("select[name=PaymentMethodExpiryYear]").append(strExpireYear);


        $("#CCDetailsSaved_wrapper .row-fluid").remove();
        $("#tblCCDetailsSavedLoader").hide();
        $("#CCDetailsSaved").show();
    });

    ShowPAYMPlan(3);
    $("button#PaymentMethodBtnCancel").live("click", function () {
        location.reload();
    });
    //if (payType == 1) {
    $("#paymentMethod span#titlePaymentMethod").html(" Change Plan ");
    //$("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    //$("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    $("button#PaymentMethodBtnNewCardProceed").click(function () {
        if (ValidatePaymentMethodForm()) {
            CCReactivateBundleConfirm(MobileNo, null, null, null, RegNo, Sitecode, BundleId, UserName, payType, price);
        }
    });
    //}
    /*
    else {
        $("#paymentMethod span#titlePaymentMethod").html(" Cancel Plan ");
        $("button#PaymentMethodBtnNewCardProceed").click(function () {
            if (ValidatePaymentMethodForm()) {
                CCPlanCancelConfirm(MobileNo);
            }
        });
    }
    */
    $("#PaymentMethodBtnSave").unbind("click").remove();
    $("#PaymentMethodBtnNewCardProceed").show();
    $("#paymentMethod").fadeIn();
}


function CCReactivateCountrySaver(MobileNo, payType, sitecode, bundleid, processby) {
    var url = apiServer + '/api/payment/' + MobileNo
    $.getJSON(url, function (data) {
        $("#CCDetailsSaved").dataTable({
            aaData: data,
            bDestroy: true,
            bRetrieve: true,
            bFilter: false,
            bLengthChange: false,
            iDisplayLength: 10,
            aaSorting: [],
            oLanguage: {
                "sEmptyTable": 'No saved card found'
            },
            aoColumns: [

				{ mDataProp: "Last6digitsCC", sTitle: 'Credit Card Number' },
				{ mDataProp: "expirydate", sTitle: 'Expiry Date' },
				{
				    mDataProp: 'SubscriptionID', sTitle: 'Action', bSortable: false,
				    fnRender: function (ob) {
				        if (payType == 1) {
				            var content = '<button class="btn blue PaymentMethodBtnSavedCardProceed" onclick="CCReactivateConfirmCountrySaver(\'' + MobileNo + '\',\'' + ob.aData.SubscriptionID + '\',\'' + ob.aData.Last6digitsCC + '\',\'' + ob.aData.cvv_number + '\',\'' + payType + '\',\'' + sitecode + '\',\'' + bundleid + '\',\'' + processby + '\')" />Select</button>';
				        }
				        else {
				            var content = '<button class="btn blue PaymentMethodBtnSavedCardProceed" onclick="CCReactivateConfirmCountrySaver(\'' + MobileNo + '\',\'' + ob.aData.SubscriptionID + '\',\'' + ob.aData.Last6digitsCC + '\',\'' + ob.aData.cvv_number + '\',\'' + payType + '\',\'' + sitecode + '\',\'' + bundleid + '\',\'' + processby + '\')" />Select</button>';
				        }
				        return content;
				    }
				}

            ]
        });
        $("#CCDetailsSaved_wrapper .row-fluid").remove();
        $("#tblCCDetailsSavedLoader").hide();
        $("#CCDetailsSaved").show();
    });

    ShowPAYMPlan(3);
    $("button#PaymentMethodBtnCancel").live("click", function () {
        location.reload();
    });
    //if (payType == 1) {
    $("#paymentMethod span#titlePaymentMethod").html(" Change Plan ");
    //$("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    //$("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    $("button#PaymentMethodBtnNewCardProceed").click(function () {
        if (ValidatePaymentMethodForm()) {
            CCReactivateConfirmCountrySaver(MobileNo);
        }
    });
    //}
    //else {
    //    $("#paymentMethod span#titlePaymentMethod").html(" Cancel Plan ");
    //    $("button#PaymentMethodBtnNewCardProceed").click(function () {
    //        if (ValidatePaymentMethodForm()) {
    //            CCPlanCancelConfirm(MobileNo);
    //        }
    //    });
    //}
    $("#PaymentMethodBtnSave").unbind("click").remove();
    $("#PaymentMethodBtnNewCardProceed").show();
    $("#paymentMethod").fadeIn();
}

/* ----------------------------------------------------------------
 * Function Name    : ValidatePaymentMethodForm
 * Purpose          : to validate Add 2in1 country form
 * Added by         : Edi Suryadi
 * Create Date      : October 16th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function ValidatePaymentMethodForm() {
    var passed = true;
    $("#PaymentMethodNewCardDetails label.errLabel").hide();

    var typeCard = $("input:radio[name=PaymentMethodCardType]:checked").val();
    if (typeCard == null) {
        $("#PaymentMethodNewCardDetails label.errLabel").eq(0).show();
        passed = false;
    }

    var cardNo = $("#PaymentMethodCCNumber").val();
    if (cardNo == "") {
        $("#PaymentMethodNewCardDetails label.errLabel").eq(1).show();
        passed = false;
    }

    var expMonth = $("#PaymentMethodExpiryMonth").val();
    var expYear = $("#PaymentMethodExpiryYear").val();
    if (expMonth == 0 || expYear == 0) {
        $("#PaymentMethodNewCardDetails label.errLabel").eq(2).show();
        passed = false;
    }

    var cardVerf = $("#PaymentMethodCardVerfCode").val();
    if (cardVerf == "") {
        $("#PaymentMethodNewCardDetails label.errLabel").eq(3).show();
        passed = false;
    }

    var firstName = $("#PaymentMethodFirstName").val();
    if (firstName == "") {
        $("#PaymentMethodNewCardDetails label.errLabel").eq(4).show();
        passed = false;
    }

    var lastName = $("#PaymentMethodLastName").val();
    if (lastName == "") {
        $("#PaymentMethodNewCardDetails label.errLabel").eq(5).show();
        passed = false;
    }

    //var postCode = $("#PaymentMethodPostcode").val();
    //change made on 07-07-2015 for post code
    var postCode = $("div#postcode_lookup input[type=text]").val();
    //end changed
    if (postCode == "") {
        $("#PaymentMethodNewCardDetails label.errLabel").eq(6).show();
        passed = false;
    }

    var houseNo = $("#PaymentMethodHouseNo").val();
    if (houseNo == "") {
        $("#PaymentMethodNewCardDetails label.errLabel").eq(7).show();
        passed = false;
    }

    var address = $("#PaymentMethodAddress").val();
    if (address == "") {
        $("#PaymentMethodNewCardDetails label.errLabel").eq(8).show();
        passed = false;
    }

    var city = $("#PaymentMethodCity").val();
    if (city == "") {
        $("#PaymentMethodNewCardDetails label.errLabel").eq(9).show();
        passed = false;
    }

    var Email = $("input[name=PaymentMethodEmail]").val();
    if (Email != "" && !validateEmail(Email)) {
        $("#PaymentMethodNewCardDetails label.errLabel").eq(10).show();
        passed = false;
    }

    return passed;
}

//PayM Restrict Excess Usage
function ViewPayMRestrictExcessUsageSelect(mobileno, status) {
    $("#hdREUMobileNo").val(mobileno);
    $("#hdREUStatus").val(status);
    $("#radioPayMREUSelect1").removeAttr('checked');
    $("#radioPayMREUSelect2").removeAttr('checked');
    if (status == 1)
        $("#radioPayMREUSelect1").attr('checked', 'checked');
    else
        $("#radioPayMREUSelect2").attr('checked', 'checked');
    $('#divPayMRestrictExcessPlanSelect').modal({ keyboard: false, backdrop: 'static' }).css({
        'margin-left': function () {
            return window.pageXOffset - ($(this).width() / 2);
        }
    });
}

function SavePayMRestrictExcessUsageSelect(sitecode) {
    var mobileno = $("#hdREUMobileNo").val();
    var status = 0;
    if ($("#radioPayMREUSelect1").attr('checked') == "checked")
        status = 1;
    var username = $('.username').html();
    $("#ajax-screen-masking").show();
    var url = apiServer + '/api/PAYMPlanBundles';
    jQuery.support.cors = true;
    var JSONSendData = {
        requestType: 95,
        status: status,
        SiteCode: sitecode,
        MobileNo: mobileno,
        updated_by: username
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            if (feedback != null && feedback.length > 0) {
                alert(feedback[0].errmsg);
                $('#divPayMRestrictExcessPlanSelect').modal('hide');
                location.reload();
            }
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}

//01-Feb-2019: Moorthy : added for Ava Allowance
//CRM Enhancement 2
function AvaAllowanceClick(config_id, sitecode, bundle_name, msisdn, international_tariff, subscription_date) {
    $("#ajax-screen-masking").show();
    $("#pbBundleRates").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblBundleRates\"></table>");
    //$("#pbBundleRates").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    $("#BundleRatesName").html("");
    var url = apiServer + '/api/bundle/';
    jQuery.support.cors = true;
    var JSONSendData = {
        config_id: config_id,
        Bundle_Type: 8,
        Sitecode: sitecode,
        msisdn: msisdn,
        international_tariff: international_tariff,
        subscription_date: subscription_date
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#BundleRatesName").html(feedback[0].bundle_call);
            $("#pbBundleRates").css({ "background-image": "none" });
            $('#tblBundleRates').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: false,
                bFilter: false,
                bInfo: false,
                bSorting: false,
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Data Available."
                },
                aoColumns: [
                    { mDataProp: "country", sTitle: "Country", sWidth: "150px" },
                    { mDataProp: "fixed", sTitle: "Landline", sWidth: "50px" },
                    { mDataProp: "mobile", sTitle: "Mobile", sWidth: "50px" },
                    { mDataProp: "fixed_used_min", sTitle: "Landline Used&nbsp;Min.", sWidth: "50px" },
                    { mDataProp: "mobile_used_min", sTitle: "Mobile Used&nbsp;Min.", sWidth: "50px" },
                    { mDataProp: "country_type", sTitle: "Country&nbsp;Type", sWidth: "50px" }
                ]
            });
            $('#ProductBundlesMessage3').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
            alert(feedback[0].errmsg);
            $("#pbBundleRates").css({ "background-image": "none" }).html("").append("No Data Available.").show();
        }
    });
}