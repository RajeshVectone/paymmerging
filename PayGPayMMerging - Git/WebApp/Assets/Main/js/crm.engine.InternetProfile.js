﻿/* ===================================================== 
   *  TAB BUNDLES 
   ----------------------------------------------------- */

/* ----------------------------------------------------------------
 * Function Name    : Internet Profile
 * Purpose          : to show Intenet Profile  
 * Added by         : karthik
 * Create Date      : December 18th, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function InternetProfile(Msisdn, Iccid, Sitecode, balance, tariffclass) {
    $("#pbInternetProfileBody").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblPBInternetProfile\"></table>");
    $("#pbInternetProfileBody").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    
    jQuery.support.cors = true;
    var url = apiServer + '/api/bundle/';
    
    var JSONSendData = {
        Msisdn: Msisdn,
        Iccid: Iccid,
        Bundle_Type: 5,
        Sitecode: Sitecode,
        balance: balance,
        tariffclass: tariffclass
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#pbInternetProfileBody").css({ "background-image": "none" });
            $('#tblPBInternetProfile').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: false,
                bFilter: false,
                bInfo: false,
                aaSorting: [],
                aoColumns: [
                    { mDataProp: "IMSI", sTitle: "IMSI" },
                    { mDataProp: "IMSI_Status", sTitle: "IMSI Status" },
                    {
                        mDataProp: "Firstautdate_string", sTitle: "First Authentication",
                        fnRender: function (ob) {
                            var content1 = ob.aData.Firstautdate_string.substring(0, 10);
                            if (content1 == "01-01-0001") {
                                return " ";
                            }
                            else {
                                return ob.aData.Firstautdate_string;
                            }
                        }
                    },
                    {
                        mDataProp: "GPRSUpdate_string", sTitle: "First GPRS Update",
                        fnRender: function (ob) {
                            var content = ob.aData.GPRSUpdate_string.substring(0, 10);
                            if (content == "01-01-0001") {
                                return " ";
                            }
                            else {
                                return ob.aData.GPRSUpdate_string;
                            }
                        }
                    },
                    //Modif by karthik Jira:CRMT-196
                    { mDataProp: "profile_value", sTitle: "Profile Value" },
                    { mDataProp: "APN", sTitle: "APN" }
                    /*{ 
                        mDataProp: "ExistOCSI", sTitle: "Action",
                        fnRender: function (ob) {
                            var IMSI = ob.aData.IMSI;
                            var Mobileno = Msisdn;
                            var sitecode = ob.aData.Sitecode;
                            var history  = "10";

                            
                            var Parameter = "\"" + Mobileno + "\",\"" + Sitecode + "\",\"" + history + "\"";
                            // alert(Parameter);
                            var actionURL = "<a class='btn blue btnINPS' onclick='InternetProfileSend(" + Parameter + ");'> SMS </a>";
                            return actionURL;
                            
                        }
                    }*/
                ],
                fnDrawCallback: function () {
                    $("#pbInternetProfileBody tbody td").css({ "font-size": "12px" });
                }
            });
            $("#tblPBInternetProfile_wrapper .row-fluid:eq(0)").remove();
            $("#tblPBInternetProfile_wrapper .row-fluid .span6:eq(0)").addClass("span4");
            $("#tblPBInternetProfile_wrapper .row-fluid .span6:eq(1)").addClass("span8");
            $("#tblPBInternetProfile_wrapper .row-fluid .span6").removeClass("span6");

            //Disable by live changes required
            
            var DownloadAnchor = "<a id=\"downloadAnchor\" style=\"display:none;\"></a>";
            var btnExportHistory = "<button class=\"btn blue\" id=\"btnGPRSSetting\" name=\"btnGPRSSetting\">GPRS Settings</button>";
            $("#tblPBInternetProfile_wrapper .row-fluid .span4").append(DownloadAnchor + btnExportHistory).show();

        },
        error: function (feedback) {
            $("#pbInternetProfileBody").css({ "background-image": "none" }).html("").append("No internet profile found").show();
        }
    });
}
/* ----------------------------------------------------------------
 * Function Name    : GPRS setting sending to customer
 * Purpose          :  GPRS setting sending to customer
 * Added by         : karthik
 * Create Date      : June 4th, 2015
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function VerifyInternetProfile(Msisdn, Sitecode) {
    var url = apiServer + '/api/bundle/';
    jQuery.support.cors = true;
    var JSONSendData = {
        Msisdn: Msisdn,
        Bundle_Type: 6,
        Sitecode: Sitecode
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            var errcode = feedback[0].errcode;
            if (errcode == 0) {
                $('.PMSuccess .modal-header').html(feedback[0].errsubject);
                $('.PMSuccess .modal-body').html(feedback[0].errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
            else {
                $('.PMFailed .modal-header').html(feedback[0].errsubject);
                $('.PMFailed .modal-body').html(feedback[0].errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (feedback) {
            $("#pbInternetProfileBody").css({ "background-image": "none" }).html("").append("No internet profile found").show();
        }
    });
}
/* ----------------------------------------------------- 
   *   Internetprofile Send SMS  
   ===================================================== */

function InternetProfileSend(Msisdn, Sitecode , History) {
    
    var url = apiServer + '/api/bundle/';
    jQuery.support.cors = true;
    var JSONSendData = {
        Msisdn: Msisdn,
        Bundle_Type: 7,
        Sitecode: Sitecode
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            var errcode = feedback.errcode;
            if (errcode == 0) {
                $('.PMSuccess .modal-header').html('SMS Send');
                $('.PMSuccess .modal-body').html('GPRS Setting Send by SMS');
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
            else {
                $('.PMFailed .modal-header').html(feedback.errsubject);
                $('.PMFailed .modal-body').html('GPRS Setting Send by SMS Failed');
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (feedback) {
            $("#pbInternetProfileBody").css({ "background-image": "none" }).html("").append("No internet profile found").show();
        }
    });
}

