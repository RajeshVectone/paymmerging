﻿/* ===================================================== 
   *  NL PORT IN - LIST
   ----------------------------------------------------- */

/* ----------------------------------------------------------------
 * Function Name    : NLPortINList
 * Purpose          : to display porting list
 * Added by         : Edi Suryadi
 * Create Date      : September 24th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function NLPortINList(Msisdn, MsgIdentifier, PortingDateFrom, PortingDateTo, StateID) {

    $("#NLPortINList_TblListContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"NLPortINList_TblList\"></table>");
    $('#NLPortINList').height(190);
    var NLPortINList_btnNew = "<button id=\"NLPortINList_btnNew\" class=\"btn blue\" style=\"width:80px\">New</button>";

    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    var url = apiServer + '/api/nlporting';
    jQuery.support.cors = true;
    var JSONSendData = {
        sitecode: "BNL",
        SubType: 102,
        Msisdn :Msisdn,     
        MsgIdentifier : MsgIdentifier,     
        PortingDateFrom : PortingDateFrom,     
        PortingDateTo: PortingDateTo,
        StateId: StateID
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function (feedback) {
            $("#ajax-screen-masking").show();
            $('#NLPortINList_TblListContainer').hide();
        },
        success: function (feedback) {
            var oTable = $('#NLPortINList_TblList').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: true,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [[0, "desc"]],
                oLanguage: {
                    "sSearch": "Filter: "
                },
                aoColumns: [
                    { mDataProp: "portingId", sTitle: "Porting ID" },
                    { mDataProp: "RequestDate", sTitle: "Request Date" },
                    { mDataProp: "actualdate", sTitle: "Porting Date" },
                    { mDataProp: "networkprovider", sTitle: "DNO" },
                    { mDataProp: "Initial", sTitle: "Initial" },
                    { mDataProp: "Prefix", sTitle: "Prefix" },
                    { mDataProp: "LastName", sTitle: "Lastname" },
                    { mDataProp: "TelpNoSerieStart", sTitle: "Telp No.Start" },
                    { mDataProp: "TelpNoSerieEnd", sTitle: "Telp No.End" },
                    { mDataProp: "StateID", sTitle: "State" },
                    { mDataProp: "statusack", sTitle: "Ack" },
                    {
                        mDataProp: "requestdate", sTitle: "Action", sWidth: "200px",
                        fnRender: function (ob) {
                            var actionURL = "";
                            actionURL = "<button data-mid=\"" + ob.aData.MsgIdentifier + "\" data-ack=\"" + ob.aData.statusack + "\" class=\"btn blue NLPortINList_btnEdit\" style=\"float:left;width:50px\">Edit</button>";
                            actionURL += "<button data-mid=\"" + ob.aData.MsgIdentifier + "\" class=\"btn blue NLPortINList_btnView\" style=\"float:left;width:50px;margin-top:5px\">View</button>";
                            return actionURL;
                        }
                    }
                ]
            });
            
            $("#NLPortINList_TblList_wrapper .row-fluid .span6:eq(0)").css({"font-size" : "16px"}).html("Result : <b>" + oTable.fnSettings().fnRecordsTotal() + "</b>");
            $("#NLPortINList_TblList_wrapper .row-fluid .span6:eq(2)").addClass("span4").html(NLPortINList_btnNew);
            $("#NLPortINList_TblList_wrapper .row-fluid .span6:eq(3)").addClass("span8");
            $("#NLPortINList_TblList_wrapper .row-fluid:eq(1) .span6").removeClass("span6");
            var NLPortINListHeight = $("#NLPortINList").outerHeight();
            var NLPortINListTableHeight = $("#NLPortINList_TblListContainer").outerHeight();
            if (navigator.appVersion.indexOf("Chrome/") != -1) {
                $("#NLPortINList").height(NLPortINListHeight + NLPortINListTableHeight + 60);
            } else {
                $("#NLPortINList").height(NLPortINListHeight + NLPortINListTableHeight + 30);
            }
            $("#ajax-screen-masking").hide();
            $("#NLPortINList_TblListContainer").show();

        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : NLPortINViewDetail
 * Purpose          : to view detail list
 * Added by         : Edi Suryadi
 * Create Date      : September 27th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function NLPortINViewDetail(MsgIdentifier) {
    var url = apiServer + '/api/nlporting/';
    jQuery.support.cors = true;
    var JSONSendData = {
        SubType: 103,
        MsgIdentifier: MsgIdentifier,
        Sitecode: "BNL"
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function(){
            $("#NLPortINListView_Loader").show();
            $("#NLPortINListView_Content").hide();
        },
        success: function (feedback) {
            if (feedback != "" && feedback != null && feedback != "null") {

                $("#NLPortINListView_strRecipientServiceProvider").html(convertEmptyStringtoSpace($.trim(feedback.RecServiceProvider)));
                $("#NLPortINListView_strRecipientNetworkOperator").html(convertEmptyStringtoSpace($.trim(feedback.RecNetworkProvider)));
                $("#NLPortINListView_strDonorServiceProvider").html(convertEmptyStringtoSpace($.trim(feedback.DonServiceProvider)));
                $("#NLPortINListView_strDonorNetworkOperator").html(convertEmptyStringtoSpace($.trim(feedback.DonNetworkProvider)));
                $("#NLPortINListView_strTypeOfNumbers").html(convertEmptyStringtoSpace($.trim(feedback.TypeOfNumbers)));
                $("#NLPortINListView_strRequestDate").html(convertEmptyStringtoSpace($.trim(feedback.RequestDate)));
                $("#NLPortINListView_strInitials").html(convertEmptyStringtoSpace($.trim(feedback.Initial)));
                $("#NLPortINListView_strPrefix").html(convertEmptyStringtoSpace($.trim(feedback.Prefix)));
                $("#NLPortINListView_strLastname").html(convertEmptyStringtoSpace($.trim(feedback.LastName)));
                $("#NLPortINListView_strCompanyName").html(convertEmptyStringtoSpace($.trim(feedback.CompanyName)));
                $("#NLPortINListView_strStreet").html(convertEmptyStringtoSpace($.trim(feedback.Street)));
                $("#NLPortINListView_strHouseNumber").html(convertEmptyStringtoSpace($.trim(feedback.HouseNumber)));
                $("#NLPortINListView_strHouseNumberExt").html(convertEmptyStringtoSpace($.trim(feedback.HouseNumberExt)));
                $("#NLPortINListView_strZipCode").html(convertEmptyStringtoSpace($.trim(feedback.Zipcode)));
                $("#NLPortINListView_strCity").html(convertEmptyStringtoSpace($.trim(feedback.City)));
                $("#NLPortINListView_strNoteOfMessage").html(convertEmptyStringtoSpace($.trim(feedback.NoteOfMsg)));
                $("#NLPortINListView_strMobileNumber").html(convertEmptyStringtoSpace($.trim(feedback.TelpNoSerieStart)));
                $("#NLPortINListView_strDonorICCID").html(convertEmptyStringtoSpace($.trim(feedback.ICC)));
                $("#NLPortINListView_strVectoneNumber").html(convertEmptyStringtoSpace($.trim(feedback.VectoneNo)));
                $("#NLPortINListView_strBlockingCode").html(convertEmptyStringtoSpace($.trim(feedback.BlockingCode + "(" + feedback.BlockingText + ")")));
                $("#NLPortINListView_strNoteOfItem").html(convertEmptyStringtoSpace($.trim(feedback.NoteOfItem)));
                $("#NLPortINListView_strOptions").html(convertEmptyStringtoSpace($.trim(feedback.Options)));

                // Edit Page Buttons
                var btnBack0 = "<button id=\"NLPortINListView_btnBack\" class=\"btn darkgrey\" style=\"width:100px;height:40px\">Back</button>";
                var btnBack1 = "<button id=\"NLPortINListView_btnBack\" class=\"btn darkgrey\" style=\"width:100px;height:40px;margin-left:10px\">Back</button>";
                var btnPortingPerformed = "<button id=\"NLPortINListView_btnPortingPerformed\" class=\"btn blue\" style=\"width:160px;height:40px;margin-left:10px\">Porting Performed</button>";
                var StateID = $.trim(feedback.StateID).toLowerCase();
                var btnData = "";
                switch (StateID) {
                    // Porting request answer
                    case 'pra':
                        btnData = btnPortingPerformed + btnBack1;
                        break;
                    default:
                        btnData = btnBack0;
                        break;
                }
                $("#NLPortINListView_btnContainer").html("").append(btnData);

                $("span#NLPortINList_Title").html("Porting Detail");
                $("#NLPortINList").hide();
                $("#NLPortINListView").fadeIn();
                $("#NLPortINListEdit").hide();
            } else {
                $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "2" });
                $('.PMFailed .modal-header').html("Netherland - PORT IN: Porting List");
                $('.PMFailed .modal-body').html("Can not find the data of this transaction.");
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        complete: function () {
            $("#NLPortINListView_Loader").hide();
            $("#NLPortINListView_Content").show();
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : NLPortINViewDetailPortingPerformed
 * Purpose          : to save edit detail list
 * Added by         : Edi Suryadi
 * Create Date      : October 03rd, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function NLPortINViewDetailPortingPerformed(p1, p2, p3, p4, p5, p6, p7) {
    var url = apiServer + '/api/nlporting';

    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    jQuery.support.cors = true;
    var JSONSendData = {
        SubType: 104,
        Sitecode: "BNL",
        rsp: p1,
        rno: p2,
        dsp: p3,
        dno: p4,
        requestdate: p5,
        NoteOfMsg: p6,
        TelpNo: p7
    };
    //window.console.log(JSONSendData);
    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();

            var stat = data.errcode;
            if (stat == 0) {
                $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
                $('.PMSuccess .modal-header').html(data.errsubject);
                $('.PMSuccess .modal-body').html(data.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            } else {
                $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "2" });
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (data) {
            $("#ajax-screen-masking").hide();
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            $('.PMFailed .modal-header').html(data.errsubject);
            $('.PMFailed .modal-body').html(data.errmsg);
            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : NLPortINEditDetail
 * Purpose          : to edit detail list
 * Added by         : Edi Suryadi
 * Create Date      : September 27th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function NLPortINEditDetail(MsgIdentifier, StatusAck) {
    $("div.iChangeMode").hide();
    var url = apiServer + '/api/nlporting/';
    jQuery.support.cors = true;
    var JSONSendData = {
        SubType: 103,
        MsgIdentifier: MsgIdentifier,
        Sitecode: "BNL"
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#NLPortINListEdit_Loader").show();
            $("#NLPortINListEdit_Content").hide();
        },
        success: function (feedback) {
            if (feedback != "" && feedback != null && feedback != "null") {
                $("select[name=NLPortINListEdit_sRecipientServiceProvider]").val($.trim(feedback.RecServiceProvider));
                $("select[name=NLPortINListEdit_sDonorServiceProvider]").val($.trim(feedback.DonServiceProvider));
                NLPortINGetNetworkOperatorListForEdit("RSP", $.trim(feedback.RecServiceProvider));
                NLPortINGetNetworkOperatorListForEdit("DSP", $.trim(feedback.DonServiceProvider));
                $("input[name=NLPortINListEdit_iRequestDate]").val($.trim(feedback.RequestDate));
                $("input[name=NLPortINListEdit_iInitials]").val($.trim(feedback.Initial));
                $("input[name=NLPortINListEdit_iPrefix]").val($.trim(feedback.Prefix));
                $("input[name=NLPortINListEdit_iLastname]").val($.trim(feedback.LastName));
                $("input[name=NLPortINListEdit_iCompanyName]").val($.trim(feedback.CompanyName));
                $("input[name=NLPortINListEdit_iStreet]").val($.trim(feedback.Street));
                $("input[name=NLPortINListEdit_iHouseNumber]").val($.trim(feedback.HouseNumber));
                $("input[name=NLPortINListEdit_iHouseNumberExt]").val($.trim(feedback.HouseNumberExt));
                $("input[name=NLPortINListEdit_iZipCode]").val($.trim(feedback.Zipcode));
                $("input[name=NLPortINListEdit_iCity]").val($.trim(feedback.City));
                $("input[name=NLPortINListEdit_iEmail]").val("");
                $("input[name=NLPortINListEdit_iMobileNumber]").val($.trim(feedback.TelpNoSerieStart));
                $("input[name=NLPortINListEdit_iDonorICCID]").val($.trim(feedback.ICC));
                $("input[name=NLPortINListEdit_iVectoneNumber]").val($.trim(feedback.VectoneNo));

                $("select[name=NLPortINListEdit_sRecipientNetworkOperator]").val($.trim(feedback.RecNetworkProvider));
                $("select[name=NLPortINListEdit_sDonorNetworkOperator]").val($.trim(feedback.DonNetworkProvider));

                /* Generate buttons depending its state */

                // Edit Page Buttons
                var btnBack0 = "<button id=\"NLPortINListEdit_btnBack\" class=\"btn darkgrey\" style=\"width:100px;height:40px\">Back</button>";
                var btnBack1 = "<button id=\"NLPortINListEdit_btnBack\" class=\"btn darkgrey\" style=\"width:100px;height:40px;margin-left:10px\">Back</button>";
                var btnCancel = "<button id=\"NLPortINListEdit_btnCancel\" data-pid=\"" + $.trim(feedback.portingId) + "\" class=\"btn blue\" style=\"width:100px;height:40px;margin-left:10px\">Cancel</button>";
                var btnChange = "<button id=\"NLPortINListEdit_btnChange\" data-pid=\"" + $.trim(feedback.portingId) + "\" class=\"btn blue\" style=\"width:100px;height:40px\">Change</button>";
                var btnResent = "<button id=\"NLPortINListEdit_btnResent\" data-pid=\"" + $.trim(feedback.portingId) + "\" class=\"btn blue\" style=\"width:100px;height:40px\">Resent</button>";
                var StateID = $.trim(feedback.StateID).toLowerCase();
                var sAck = $.trim(StatusAck).toLowerCase().replace(" ", "_");
                var btnData = "";

                switch (StateID) {
                    // Porting request
                    case 'pr':
                        if (sAck == 'nack') {
                            $("#NLPortINListEdit .isResentMode").attr("disabled", true);
                            $("span#NLPortINListEdit_iRequestDate_IconCalendar").hide();
                            $("span.NLPortINListEdit_Mandatory").hide();
                            $("input[name=NLPortINListEdit_iRequestDate]").css({ "width": "244px" });
                            $("div#NLPortINListEdit").height(840);
                            btnData = btnBack0;
                        } else if (sAck == 'ack') {
                            $("#NLPortINListEdit .isResentMode").attr("disabled", true);
                            $("span#NLPortINListEdit_iRequestDate_IconCalendar").hide();
                            $("span.NLPortINListEdit_Mandatory").hide();
                            $("input[name=NLPortINListEdit_iRequestDate]").css({ "width": "244px" });
                            $("div.iChangeMode").show();
                            $("div#NLPortINListEdit").height(1015);
                            btnData = btnChange + btnCancel + btnBack1;
                        } else if (sAck == 'not_yet') {
                            $("#NLPortINListEdit .isResentMode").attr("disabled", true);
                            $("span#NLPortINListEdit_iRequestDate_IconCalendar").hide();
                            $("span.NLPortINListEdit_Mandatory").hide();
                            $("input[name=NLPortINListEdit_iRequestDate]").css({ "width": "244px" });
                            $("div#NLPortINListEdit").height(840);
                            btnData = btnBack0;
                        }
                        break;
                    // Porting request answer
                    case 'pra':
                        if (sAck == 'ack') {
                            $("#NLPortINListEdit .isResentMode").attr("disabled", true);
                            $("span#NLPortINListEdit_iRequestDate_IconCalendar").hide();
                            $("span.NLPortINListEdit_Mandatory").hide();
                            $("input[name=NLPortINListEdit_iRequestDate]").css({ "width": "244px" });
                            $("div.iChangeMode").show();
                            $("div#NLPortINListEdit").height(1015);
                            btnData = btnChange + btnCancel + btnBack1;
                        }
                        break;
                    // Porting request answer : Blocking
                    case 'prb':
                        if (sAck == 'ack') {
                            $("#NLPortINListEdit .isResentMode").removeAttr("disabled");
                            $("span#NLPortINListEdit_iRequestDate_IconCalendar").show();
                            $("input[name=NLPortINListEdit_iRequestDate]").val(convertNLDateToHuman($.trim(feedback.RequestDate), 1)).change();
                            $("span.NLPortINListEdit_Mandatory").show();
                            $("input[name=NLPortINListEdit_iRequestDate]").css({ "width": "217px" });
                            $("div#NLPortINListEdit").height(840);
                            btnData = btnResent + btnBack1;
                        }
                        break;
                    // Finish
                    case 'fin':
                        if (sAck == 'ack') {
                            $("#NLPortINListEdit .isResentMode").attr("disabled", true);
                            $("span#NLPortINListEdit_iRequestDate_IconCalendar").hide();
                            $("span.NLPortINListEdit_Mandatory").hide();
                            $("input[name=NLPortINListEdit_iRequestDate]").css({ "width": "244px" });
                            $("div#NLPortINListEdit").height(840);
                            btnData = btnBack0;
                        }
                        break;
                        // Cancel message
                    case 'ca':
                        if (sAck == 'ack') {
                            $("#NLPortINListEdit .isResentMode").attr("disabled", true);
                            $("span#NLPortINListEdit_iRequestDate_IconCalendar").hide();
                            $("span.NLPortINListEdit_Mandatory").hide();
                            $("input[name=NLPortINListEdit_iRequestDate]").css({ "width": "244px" });
                            $("div#NLPortINListEdit").height(840);
                            btnData = btnBack0;
                        }
                        break;
                    default:
                        $("#NLPortINListEdit .isResentMode").attr("disabled", true);
                        $("span#NLPortINListEdit_iRequestDate_IconCalendar").hide();
                        $("span.NLPortINListEdit_Mandatory").hide();
                        $("input[name=NLPortINListEdit_iRequestDate]").css({ "width": "244px" });
                        $("div#NLPortINListEdit").height(840);
                        btnData = btnBack0;
                        break;
                }
                $("#NLPortINListEdit_btnContainer").html("").append(btnData);


                $("span#NLPortINList_Title").html("Porting Detail");
                $("#NLPortINList").hide();
                $("#NLPortINListView").hide();
                $("#NLPortINListEdit").fadeIn();
            } else {
                $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "2" });
                $('.PMFailed .modal-header').html("Netherland - PORT IN: Porting List");
                $('.PMFailed .modal-body').html("Can not find the data of this transaction.");
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        complete: function () {
            $("#NLPortINListEdit_Loader").hide();
            $("#NLPortINListEdit_Content").show();
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : NLPortINEditDetailCancel
 * Purpose          : to cancel porting request
 * Added by         : Edi Suryadi
 * Create Date      : October 03rd, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function NLPortINEditDetailCancel(p1, p2, p3, p4, p5, p6, p7) {
    var url = apiServer + '/api/nlporting';

    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    jQuery.support.cors = true;
    var JSONSendData = {
        rsp: p1,
        rno: p2,
        dsp: p3,
        dno: p4,
        requestdate: p5,
        NoteOfMsg: p6,
        TelpNo: p7,
        sitecode: "BNL",
        SubType: 5
    };

    //window.console.log(JSONSendData);

    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();

            var stat = data.errcode;
            if (stat == 0) {
                $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
                $('.PMSuccess .modal-header').html(data.errsubject);
                $('.PMSuccess .modal-body').html(data.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            } else {
                $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "2" });
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (data) {
            $("#ajax-screen-masking").hide();
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            $('.PMFailed .modal-header').html(data.errsubject);
            $('.PMFailed .modal-body').html(data.errmsg);
            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : NLPortINEditDetailChange
 * Purpose          : to change porting request
 * Added by         : Edi Suryadi
 * Create Date      : October 03rd, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function NLPortINEditDetailChange(p1, p2, p3, p4, p5, p6, p7) {
    var url = apiServer + '/api/nlporting';

    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    jQuery.support.cors = true;
    var JSONSendData = {
        rsp: p1,
        rno: p2,
        dsp: p3,
        dno: p4,
        requestdate: p5,
        NoteOfMsg: p6,
        TelpNo: p7,
        sitecode: "BNL",
        SubType: 3
    };

    //window.console.log(JSONSendData);

    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();

            var stat = data.errcode;
            if (stat == 0) {
                $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
                $('.PMSuccess .modal-header').html(data.errsubject);
                $('.PMSuccess .modal-body').html(data.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            } else {
                $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "2" });
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (data) {
            $("#ajax-screen-masking").hide();
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            $('.PMFailed .modal-header').html(data.errsubject);
            $('.PMFailed .modal-body').html(data.errmsg);
            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });
}


/* ----------------------------------------------------------------
 * Function Name    : NLPortINEditDetailResent
 * Purpose          : to resent the porting
 * Added by         : Edi Suryadi
 * Create Date      : September 27th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function NLPortINEditDetailResent(p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19) {
    var url = apiServer + '/api/nlporting';

    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    jQuery.support.cors = true;
    var JSONSendData = {
        rsp: p1,
        rno: p2,
        dsp: p3,
        dno: p4,
        requestdate: p5,
        CustId: p6,
        Initials: p7,
        Prefix: p8,
        ContactName: p9,
        CompanyName: p10,
        Street: p11,
        Houseno: p12,
        HouseNoExt: p13,
        Zipcode: p14,
        City: p15,
        Email: p16,
        TelpNo: p17,
        Iccid: p18,
        Msisdn: p19,
        //NoteOfMsg : "", // Hardcode from backend October 10th, 2013 - 13:01
        Sitecode: "BNL",
        IsResent: "true",
        SubType: 2
    };

    //window.console.log(JSONSendData);

    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();

            var stat = data.errcode;
            if (stat == 0) {
                $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
                $('.PMSuccess .modal-header').html(data.errsubject);
                $('.PMSuccess .modal-body').html(data.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            } else {
                $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "2" });
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (data) {
            $("#ajax-screen-masking").hide();
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            $('.PMFailed .modal-header').html(data.errsubject);
            $('.PMFailed .modal-body').html(data.errmsg);
            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : NLPortINEditDetailValidateInput
 * Purpose          : to validate input
 * Added by         : Edi Suryadi
 * Create Date      : September 24th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */

function NLPortINEditDetailValidateInput() {

    var passed = true;

    /* Validate Street */
    var Street = $("input[name=NLPortINListEdit_iStreet]").val();
    if (Street == "") {
        passed = false;
    }

    /* Validate House No */
    var regexHouseNo = /^[0-9]{1,5}$/;
    var HouseNo = $("input[name=NLPortINListEdit_iHouseNumber]").val();
    if (regexHouseNo.test(HouseNo) == false || HouseNo == "") {
        $("span#err_NLPortINListEdit_iHouseNumber").fadeIn();
        passed = false;
    } else {
        $("span#err_NLPortINListEdit_iHouseNumber").hide();
    }

    /* Validate House No Ext */
    var regexHouseNoExt = /^[0-9]{1,4}$/;
    var HouseNoExt = $("input[name=NLPortINListEdit_iHouseNumberExt]").val();
    if (regexHouseNoExt.test(HouseNoExt) == false && HouseNoExt != "") {
        $("span#err_NLPortINListEdit_iHouseNumberExt").fadeIn();
        passed = false;
    } else {
        $("span#err_NLPortINListEdit_iHouseNumberExt").hide();
    }

    /* Validate Zip Code */
    var regexZipCode = /^[1-9]\d{3}[A-Z]{2}$/;
    var ZipCode = $("input[name=NLPortINListEdit_iZipCode]").val();
    if (regexZipCode.test(ZipCode) == false || ZipCode == "") {
        $("span#err_NLPortINListEdit_iZipCode").fadeIn();
        passed = false;
    } else {
        $("span#err_NLPortINListEdit_iZipCode").hide();
    }

    /* Validate City */
    var City = $("input[name=NLPortINListEdit_iCity]").val();
    if (City == "") {
        passed = false;
    }

    /* Validate Email */
    var Email = $("input[name=NLPortINListEdit_iEmail]").val();
    if (validateEmail(Email) == false && Email != "") {
        $("span#err_NLPortINListEdit_iEmail").fadeIn();
        passed = false;
    } else {
        $("span#err_NLPortINListEdit_iEmail").hide();
    }

    /* Validate Mobile Number */
    var regexMobileNumber = /^[0-9]{1,30}$/;
    var MobileNumber = $("input[name=NLPortINListEdit_iMobileNumber]").val();
    if (regexMobileNumber.test(MobileNumber) == false && MobileNumber != "") {
        $("span#err_NLPortINListEdit_iMobileNumber").fadeIn();
        passed = false;
    } else {
        $("span#err_NLPortINListEdit_iMobileNumber").hide();
    }

    /* Validate Vectone Number */
    var VectoneNumber = $("input[name=NLPortINListEdit_iVectoneNumber]").val();
    if (VectoneNumber == "") {
        passed = false;
    }

    return passed;
}

/* ----------------------------------------------------------------
 * Function Name    : NLPortINGetPortingStateList
 * Purpose          : to load service provider list for combobox
 * Added by         : Edi Suryadi
 * Create Date      : Ocotber 01st, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function NLPortINGetPortingStateList() {
    $("select[name=NLPortINList_sPortingState]").html("");

    var url = apiServer + '/api/nlporting';
    jQuery.support.cors = true;
    var JSONSendData = {
        Sitecode: "BNL",
        SubType: 101
    };
    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (data) {
            var optionContent = "";
            for (i = 0; i < data.length; i++) {
                optionContent += "<option value=\"" + data[i].StateID + "\">" + data[i].Name + "</option>";
            }
            $("select[name=NLPortINList_sPortingState]").append(optionContent);
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : NLPortINGetServiceProviderListForEdit
 * Purpose          : to get service provider list for combobox
 * Added by         : Edi Suryadi
 * Create Date      : Ocotber 01st, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function NLPortINGetServiceProviderListForEdit() {

    var url = apiServer + '/api/nlporting';
    jQuery.support.cors = true;
    var JSONSendData = {
        OptType: "SP", // SP = Service Provider ; NO = Network Operator
        ProviderCode: "", // null to get Service provider list.To get Network Operator,please provide provider code
        sitecode: "BNL",
        SubType: 100
    };
    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (data) {
            var optionContent = "";
            for (i = 0; i < data.length; i++) {
                optionContent += "<option value=\"" + data[i].Name + "\">" + data[i].FullName + "</option>";
            }
            $("select[name=NLPortINListEdit_sRecipientServiceProvider]").html("").append(optionContent);
            $("select[name=NLPortINListEdit_sDonorServiceProvider]").html("").append(optionContent);
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : NLPortINGetNetworkOperatorListForEdit
 * Purpose          : to get network operator list for combobox
 * Added by         : Edi Suryadi
 * Create Date      : Ocotber 01st, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function NLPortINGetNetworkOperatorListForEdit(ServiceProviderType, ProviderCode) {
    var url = apiServer + '/api/nlporting';
    jQuery.support.cors = true;
    var JSONSendData = {
        OptType: "NO", // SP = Service Provider ; NO = Network Operator
        ProviderCode: ProviderCode, // null to get Service provider list.To get Network Operator,please provide provider code
        sitecode: "BNL",
        SubType: 100
    };
    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            if (ServiceProviderType == "RSP") {
                $("select[name=NLPortINListEdit_sRecipientNetworkOperator]").html("").attr("disabled", true);
                $("img#NLPortINListEdit_sRecipientNetworkOperator_Loader").show();
            } else if (ServiceProviderType == "DSP") {
                $("select[name=NLPortINListEdit_sDonorNetworkOperator]").html("").attr("disabled", true);
                $("img#NLPortINListEdit_sDonorNetworkOperator_Loader").show();
            }
        },
        success: function (data) {
            var optionContent = "";

            for (i = 0; i < data.length; i++) {
                optionContent += "<option value=\"" + data[i].Name + "\">" + data[i].FullName + "</option>";
            }

            if (ServiceProviderType == "RSP") {
                $("select[name=NLPortINListEdit_sRecipientNetworkOperator]").append(optionContent);
            } else if (ServiceProviderType == "DSP") {
                $("select[name=NLPortINListEdit_sDonorNetworkOperator]").append(optionContent).removeAttr("disabled");
            }
            $("img#NLPortINListEdit_sRecipientNetworkOperator_Loader").hide();
            $("img#NLPortINListEdit_sDonorNetworkOperator_Loader").hide();
        }
    });
}

/* ----------------------------------------------------- 
   *  eof NL PORT IN - LIST
   ===================================================== */






