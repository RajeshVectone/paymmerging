﻿//Auto Renewal History Report Search
function getAutoRenewalHistory(date_fr, date_to, mobileno, sitecode) {
    $("#tblTabAutoRenewalHistoryContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblTabAutoRenewalHistory\"></table>");
    $("#tblTabAutoRenewalHistoryContainer").css({ "min-height": "100px", "background-repeat": "no-repeat", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var row = 0;
    var url = apiServer + "/api/AutoRenewalHistory";
    var JSONSendData = {
        sitecode: sitecode,
        msisdn: mobileno,
        date_from: date_fr,
        date_to: date_to
    };
    $.ajax
    ({
        url: url,
        type: "POST",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('#tblTabAutoRenewalHistory').html('');
            $("#tblTabAutoRenewalHistoryContainer").css({ "background-image": "none" });
            $('#tblTabAutoRenewalHistory').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                iDisplayLength: 10,
                bPaginate: true,
                bFilter: true,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [[0, "desc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No records found."
                },
                aoColumns: [                       
                       { mDataProp: "mobileno", sTitle: "Mobile Number" },
                       { mDataProp: "iccid", sTitle: "ICCID" },
                       { mDataProp: "bundleid", sTitle: "Bundle ID" },
                       { mDataProp: "bundle_name", sTitle: "Bundle Name" },
                       { mDataProp: "create_date", sTitle: "Created Date" },
                       { mDataProp: "create_by", sTitle: "Created By" },
                       { mDataProp: "comments", sTitle: "Comments" },
                       { mDataProp: "process_name", sTitle: "Process Name" },
                ],
                fnDrawCallback: function () {
                    $("#tblTabAutoRenewalHistory tbody td").css({ "font-size": "12px" });
                }
            });
            $("#tblTabAutoRenewalHistory tbody td").css({ "font-size": "12px" });
        },
        error: function (feedback) {
            $("#tblTabAutoRenewalHistory").css({ "background-image": "none" }).html("").append("No records found.").show();
        }
    });
}

//Auto Renewal History Report Download
function downloadAutoRenewalhistory(date_fr, date_to, mobileno, sitecode) {
    var i = 0;
    jQuery.support.cors = true;
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader1").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    $("#ajax-screen-masking #downloadIFrame").remove();
    var url = '/Download/DownloadAutoRenewalHistory';
    var JSONSendData = {
        sitecode: sitecode,
        msisdn: mobileno,
        date_fr: date_fr,
        date_to: date_to
    };
    $.ajax
    ({
        url: url,
        type: 'get',
        dataType: 'json',
        data: JSONSendData,
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {

            if (feedback.errcode == 0) {
                $("#ajax-screen-masking").hide();
                var IFrameData = $('<iframe/>', { id: 'downloadIFrame', src: '/Download/DownloadRenewalList?id=' + feedback.Text, style: 'display:none' });
                $("#ajax-screen-masking").append(IFrameData);
            }
        }
    });
}