﻿function PaymentHistoryPAYG(MobileNo) {
   
    var dates = new Date();
    $("#PayMContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblPayMResult\"></table>");
    $("#PayMContainer").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var url = apiServer + "/api/PAYMBill";
    var JSONSendData = {
        MobileNo: MobileNo
    };
    $.ajax
    ({
        url: url,
        type: "GET",
        data: JSONSendData,
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#PayMContainer").css({ "background-image": "none" });
            $('#tblPayMResult').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: false,
                bInfo: false,
                iDisplayLength: 5,
                aaSorting: [[0, "desc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No PAYM Bills found."
                },
                aoColumns: [
                    { mDataProp: "BillPeriod", sTitle: "Bill Period" },
                    { mDataProp: "Outstanding", sTitle: "OutStanding(£)" },
                    //CRM Enhancement 2
                    { mDataProp: "Payment", sTitle: "Customer Has Paid(£)" },
                    //{ mDataProp: "Payment", sTitle: "Payment(£)" },
                    { mDataProp: "PlanCharges", sTitle: "Plan Charges(£)" },
                    { mDataProp: "ExcessUsage", sTitle: "Excess Usages(£)" },
                    { mDataProp: "Deduction", sTitle: "Deductions(£)" },
                    { mDataProp: "TotalBill", sTitle: "Total Bill" },
                    { mDataProp: "PaymentStatus", sTitle: "Payment Status " },
                    {
                        sTitle: "Download", mDataProp: "Action", "bSortable": false,
                        fnRender: function (ob) {
                            var datetime = ob.aData.BillPeriod;
                            var month = datetime.substring(4, 6);
                            var year = datetime.substring(0, 4);
                            var parameter = MobileNo + "," + year + "," + month;
                            var DownloadList = "<a class='btn blue' onclick='getdownload(" + parameter + ");'> Download</a>";
                            return DownloadList;
                        }
                    },
                ],
                fnDrawCallback: function () {
                    $("#tblPayMResult tbody td").css({ "font-size": "12px" });
                }
            });
            $("#tblPayMResult tbody td").css({ "font-size": "12px" });
            $("#tblPayMResult_wrapper .row-fluid:eq(0)").remove();
            $("#tblPayMResult_wrapper .row-fluid .span6:eq(0)").addClass("span4");
            $("#tblPayMResult_wrapper .row-fluid .span6:eq(1)").addClass("span8");
            $("#tblPayMResult_wrapper .row-fluid .span6").removeClass("span6");
        },
        error: function (feedback) {
            $("#PayMContainer").css({ "background-image": "none" }).html("").append("No Payment history PAYG found.").show();
        }
    });
    getOutStandingAmount(MobileNo, $('.clsproductcode')[0].innerHTML);
}

function getOutStandingAmount(MobileNo, ProductCode) {
  
    var url = apiServer + '/api/PAYMBill/';
    var JSONSendData = {
        MobileNo: MobileNo,
        ProductCode: ProductCode
    };
    $.ajax
    ({
        url: url,
        type: "Post",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {	
            //if (feedback[0].PaymentStatus == "Payment Failed" || feedback[0].PaymentStatus == "Bill Generated") {
            if (feedback[0].PaymentStatus != "Payment Successful"){ 
                if (feedback[0].PaymentStatus != "Payment Processed") {
                    $('#OutStanding').show();
                    $('#lblOutStandingAmount').text(feedback[0].TotalBill);
                }
                else
                    $('#OutStanding').hide();
            }
            else {
                    $('#OutStanding').hide();
            }
        }
  
    });
}

function getdownload(MobileNo, year, Month) {
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    $("#ajax-screen-masking #downloadIFrames").remove();
    //Modified By BSK for Prev.Month Data download issue
    //var url = '/Download/DownloadPAYMBills/';
    var url = '/Download/DownloadPAYMBillsDetails/';
    var JSONSendData = {
        MobileNo: MobileNo,
        year: year,
        Month: Month,
        productCode : $('.clsproductcode')[0].innerHTML
    };
    $.ajax
    ({
        url: url,
        type: "get",
        data: JSONSendData,
        dataType: "json",
        async: false,
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            if (feedback.err_message.indexOf('.pdf') >= 0) {
                var result = "/Download/" + feedback.err_message;
                window.open(result, "_blank");
            }
            else {
                $('#filesName').html("General Exceptions : " + feedback.err_message);
                $("#ajax-screen-masking").hide();
                $('#FileGenrated').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (feedback) {
            $('#filesName').html("General Exceptions : " + feedback.err_message);
            $("#ajax-screen-masking").hide();
            $('#FileGenrated').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });
}




function ProcessPaymentNow(MobileNo, OutstandingAmt, UserName ,SiteCode)
{
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    $("#ajax-screen-masking #downloadIFrames").remove();
    var url = apiServer + '/api/paynowprocess';
    var JSONSendData = {
        MobileNo: MobileNo,
        OutstandingAmt: OutstandingAmt,
        updatedBy: UserName,
        siteCode:SiteCode,
        productCode: $('.clsproductcode')[0].innerHTML
    };

    $.ajax
    ({
        url: url,
        type: "get",
        data: JSONSendData,
        dataType: "json",
        async: false,
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {
            $("#ajax-screen-masking").hide();

            $('#filesName2').html("Message : " + feedback[0].errmsg);
            $('#FileGenrated2').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        },
        error: function (xhr, status, error) {

            var feedback = eval("(" + xhr.responseText + ")");
            $('#filesName').html("General Exceptions : " + feedback[0].errmsg);
            $("#ajax-screen-masking").hide();
            $('#FileGenrated').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });

}

/// <summary>
/// /// Add by BSK for CRMT-111 Jira
/// Get All Service Status History Details
/// api/ServiceStatusHistory
/// </summary>
function ServiceStatusHistory(MobileNo, ProductCode) {
    
    var dates = new Date();
    $("#ServiceStatusHistoryContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblServiceStatusHistory\"></table>");
    $("#ServiceStatusHistoryContainer").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var url = apiServer + "/api/ServiceStatusHistory";
    var JSONSendData = {
        MobileNo: MobileNo,
        ProductCode: ProductCode
    };
    $.ajax
    ({
        url: url,
        type: "GET",
        data: JSONSendData,
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#ServiceStatusHistoryContainer").css({ "background-image": "none" });
            $('#tblServiceStatusHistory').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: false,
                bInfo: false,
                iDisplayLength: 5,
                aaSorting: [[0, "desc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Service Status History Found."
                },
                aoColumns: [
                    { mDataProp: "updatedate", sTitle: "Update Date" },
                    { mDataProp: "prev_service_status", sTitle: "Previous Service Status" },
                    { mDataProp: "after_service_status", sTitle: "Current Service Status" },
                    { mDataProp: "remark", sTitle: "Remark" },
                    { mDataProp: "updateby", sTitle: "Updated By" },
                   
                ],
                fnDrawCallback: function () {
                    $("#tblServiceStatusHistory tbody td").css({ "font-size": "12px" });
                }
            });
            $("#tblServiceStatusHistory tbody td").css({ "font-size": "12px" });
            $("#tblServiceStatusHistory_wrapper .row-fluid:eq(0)").remove();
            $("#tblServiceStatusHistory_wrapper .row-fluid .span6:eq(0)").addClass("span4");
            $("#tblServiceStatusHistory_wrapper .row-fluid .span6:eq(1)").addClass("span8");
            $("#tblServiceStatusHistory_wrapper .row-fluid .span6").removeClass("span6");
        },
        error: function (feedback) {
            $("#ServiceStatusHistoryContainer").css({ "background-image": "none" }).html("").append("No Service Status History Found.").show();
        }
    });
    //getOutStandingAmount(MobileNo);
}

//CRM Enhancement 2
function PaymentHistoryPAYM(MobileNo, ProductCode) {
  
    var dates = new Date();
    $("#PayMContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblPayMResult\"></table>");
    $("#PayMContainer").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var url = apiServer + "/api/PAYMBill";
    var JSONSendData = {
        MobileNo: MobileNo,
        ProductCode: ProductCode
    };
    $.ajax
    ({
        url: url,
        type: "GET",
        data: JSONSendData,
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#PayMContainer").css({ "background-image": "none" });
            $('#tblPayMResult').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: false,
                bInfo: false,
                iDisplayLength: 5,
                aaSorting: [[0, "desc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No PAYM Bills found."
                },
                aoColumns: [
                    { mDataProp: "BillPeriod", sTitle: "Bill Period" },
                    { mDataProp: "Outstanding", sTitle: "OutStanding(£)" },
                    //CRM Enhancement 2
                    { mDataProp: "Payment", sTitle: "Customer Has Paid(£)" },
                    //{ mDataProp: "Payment", sTitle: "Payment(£)" },
                    //{ mDataProp: "PlanCharges", sTitle: "Plan Charges(£)" },
                    { mDataProp: "PlanCharges", sTitle: "Plan Amount(£)" },
                    { mDataProp: "ExcessUsage", sTitle: "Excess Usages(£)" },
                    { mDataProp: "LLOM", sTitle: "LLOM Subscription(£)" },
                    //{ mDataProp: "Bundle", sTitle: "Bundle(£)" },
                    //{ mDataProp: "Country_Saver", sTitle: "Country Saver(£)" },
                    { mDataProp: "Deduction", sTitle: "Deductions(£)" },
                    { mDataProp: "TotalBill", sTitle: "Total Bill with Payment" },
                    { mDataProp: "PaymentStatus", sTitle: "Payment Status " },
                    { mDataProp: "Pay_Reference", sTitle: "Payment Reference " },
                    { mDataProp: "Pay_Mode", sTitle: "Payment Mode" },
                    { mDataProp: "payment_date_str", sTitle: "Payment Date" },
                    {
                        sTitle: "Download", mDataProp: "Action", "bSortable": false,
                        fnRender: function (ob) {
                            var datetime = ob.aData.BillPeriod;
                            var month = datetime.substring(4, 6);
                            var year = datetime.substring(0, 4);
                            var parameter = MobileNo + "," + year + "," + month;
                            var DownloadList = "<a class='btn blue' onclick='getdownload(" + parameter + ");'> Download</a>";
                            return DownloadList;
                        }
                    },
                ],
                fnDrawCallback: function () {
                    $("#tblPayMResult tbody td").css({ "font-size": "12px" });
                }
            });
            $("#tblPayMResult tbody td").css({ "font-size": "12px" });
            $("#tblPayMResult_wrapper .row-fluid:eq(0)").remove();
            $("#tblPayMResult_wrapper .row-fluid .span6:eq(0)").addClass("span4");
            $("#tblPayMResult_wrapper .row-fluid .span6:eq(1)").addClass("span8");
            $("#tblPayMResult_wrapper .row-fluid .span6").removeClass("span6");
        },
        error: function (feedback) {
            $("#PayMContainer").css({ "background-image": "none" }).html("").append("No Payment history PAYG found.").show();
        }
    });
    getOutStandingAmount(MobileNo, ProductCode);
}


function SMSViewTracking(MobileNo, SiteCode, ProductCode) {
 
    var dates = new Date();
    $("#PayMContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblSMSCampaignTracking\"></table>");
    $("#PayMContainer").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var url = apiServer + "/api/PAYMBill";
    var JSONSendData = {
        MobileNo: MobileNo,
        ProductCode: ProductCode
    };
    $.ajax
    ({
        url: url,
        type: "GET",
        data: JSONSendData,
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#PayMContainer").css({ "background-image": "none" });
            $('#tblPayMResult').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: false,
                bInfo: false,
                iDisplayLength: 5,
                aaSorting: [[0, "desc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No PAYM Bills found."
                },
                aoColumns: [
                    { mDataProp: "BillPeriod", sTitle: "Bill Period" },
                    { mDataProp: "Outstanding", sTitle: "OutStanding(£)" },
                    { mDataProp: "Payment", sTitle: "Payment(£)" },
                    { mDataProp: "PlanCharges", sTitle: "Plan Charges(£)" },
                    { mDataProp: "ExcessUsage", sTitle: "Excess Usages(£)" },
                    { mDataProp: "LLOM", sTitle: "LLOM Subscription(£)" },
                    { mDataProp: "Bundle", sTitle: "Bundle(£)" },
                    { mDataProp: "Country_Saver", sTitle: "Country Saver(£)" },
                    { mDataProp: "Deduction", sTitle: "Deductions(£)" },
                    { mDataProp: "TotalBill", sTitle: "Total Bill" },
                    { mDataProp: "PaymentStatus", sTitle: "Payment Status " },
                    { mDataProp: "Pay_Reference", sTitle: "Payment Reference " },
                    { mDataProp: "Pay_Mode", sTitle: "Payment Mode" },
                    {
                        sTitle: "Download", mDataProp: "Action", "bSortable": false,
                        fnRender: function (ob) {
                            var datetime = ob.aData.BillPeriod;
                            var month = datetime.substring(4, 6);
                            var year = datetime.substring(0, 4);
                            var parameter = MobileNo + "," + year + "," + month;
                            var DownloadList = "<a class='btn blue' onclick='getdownload(" + parameter + ");'> Download</a>";
                            return DownloadList;
                        }
                    },
                ],
                fnDrawCallback: function () {
                    $("#tblPayMResult tbody td").css({ "font-size": "12px" });
                }
            });
            $("#tblPayMResult tbody td").css({ "font-size": "12px" });
            $("#tblPayMResult_wrapper .row-fluid:eq(0)").remove();
            $("#tblPayMResult_wrapper .row-fluid .span6:eq(0)").addClass("span4");
            $("#tblPayMResult_wrapper .row-fluid .span6:eq(1)").addClass("span8");
            $("#tblPayMResult_wrapper .row-fluid .span6").removeClass("span6");
        },
        error: function (feedback) {
            $("#PayMContainer").css({ "background-image": "none" }).html("").append("No Payment history PAYG found.").show();
        }
    });
    getOutStandingAmount(MobileNo, ProductCode);
}

//Emails - Trigger from CRM
function SendMail(firstname, email, sitecode) {
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    $("#ajax-screen-masking #downloadIFrames").remove();
    var url = apiServer + '/api/paynowprocess';
    var JSONSendData = {
        firstname: firstname,
        email: email,
        sitecode: sitecode
    };

    $.ajax
    ({
        url: url,
        type: "get",
        data: JSONSendData,
        dataType: "json",
        async: false,
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            alert(feedback[0].errmsg);
        },
        error: function (xhr, status, error) {

            var feedback = eval("(" + xhr.responseText + ")");
            $('#filesName').html("General Exceptions : " + feedback[0].errmsg);
            $("#ajax-screen-masking").hide();
            $('#FileGenrated').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });
}