﻿/* ===================================================== 
   *  TAB CALL SETTINGS 
   ----------------------------------------------------- */

/* ----------------------------------------------------------------
 * Function Name    : SIMDetailsInfo
 * Purpose          : to show Call Settings Information
 * Added by         : Edi Suryadi
 * Create Date      : September 04th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function CallSettingsInfo(mobileno, ICCID, Sitecode) {
    
    var url = apiServer + '/api/sim';

    jQuery.support.cors = true;
    var JSONSendData = {
        Msisdn: mobileno,
        Iccid: ICCID,
        Sitecode: Sitecode,
        InfoType: 2
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#TabCallSettingsLoader").hide();
            $("#tblCustomerCallSettingsContainer").show();
            $("#btnEditCallSettingsContainer").show();
            var errcode = -1;
            if (feedback != '' && feedback != null) {
                errcode = feedback.errcode;
            }

            $("table#tblCustomerCallSettings td#tdCCSAlwaysForward").html("-");
            $("table#tblCustomerCallSettings td#tdCCSForwardBusy").html("-");
            $("table#tblCustomerCallSettings td#tdCCSForwardUnanswered").html("-");
            $("table#tblCustomerCallSettings td#tdCCSForwardUnreachable").html("-");

            if (feedback != null) {

                var feedback_length = feedback.length;

                var cfu_CFWTypeName = "";
                var cfu_CFWStatus = "";
                var cfu_ForwardToNb = "";
                var cfu_CountryCode = "";

                var cfb_CFWTypeName = "";
                var cfb_CFWStatus = "";
                var cfb_ForwardToNb = "";
                var cfb_CountryCode = "";

                var cfnry_CFWTypeName = "";
                var cfnry_CFWStatus = "";
                var cfnry_ForwardToNb = "";
                var cfnry_CountryCode = "";
                var cfnry_NoReplyTimer = "";

                var cfnrc_CFWTypeName = "";
                var cfnrc_CFWStatus = "";
                var cfnrc_ForwardToNb = "";
                var cfnrc_CountryCode = "";

                for (i = 0; i < feedback_length; i++) {
                    if ($.trim(feedback[i].CFWTypeName.toLowerCase()) == 'cfu') {
                        $("table#tblCustomerCallSettings td#tdCCSAlwaysForward").html(feedback[i].CFWStatusName);
                        cfu_CFWTypeName = feedback[i].CFWTypeName;
                        cfu_CFWStatus = feedback[i].CFWStatus;
                        cfu_ForwardToNb = feedback[i].ForwardToNb;
                        cfu_CountryCode = feedback[i].CountryCode;
                        $("input[name=CSetCountryCode]").val(feedback[i].CountryCode);
                    } else if ($.trim(feedback[i].CFWTypeName.toLowerCase()) == 'cfb') {
                        $("table#tblCustomerCallSettings td#tdCCSForwardBusy").html(feedback[i].CFWStatusName);
                        cfb_CFWTypeName = feedback[i].CFWTypeName;
                        cfb_CFWStatus = feedback[i].CFWStatus;
                        cfb_ForwardToNb = feedback[i].ForwardToNb;
                        cfb_CountryCode = feedback[i].CountryCode;
                        $("input[name=CSetCountryCode]").val(feedback[i].CountryCode);
                    } else if ($.trim(feedback[i].CFWTypeName.toLowerCase()) == 'cfnry') {
                        $("table#tblCustomerCallSettings td#tdCCSForwardUnanswered").html(feedback[i].CFWStatusName);
                        cfnry_CFWTypeName = feedback[i].CFWTypeName;
                        cfnry_CFWStatus = feedback[i].CFWStatus;
                        cfnry_ForwardToNb = feedback[i].ForwardToNb;
                        cfnry_CountryCode = feedback[i].CountryCode;
                        cfnry_NoReplyTimer = feedback[i].NoReplyTimer;
                        $("input[name=CSetCountryCode]").val(feedback[i].CountryCode);
                    } else if ($.trim(feedback[i].CFWTypeName.toLowerCase()) == 'cfnrc') {
                        $("table#tblCustomerCallSettings td#tdCCSForwardUnreachable").html(feedback[i].CFWStatusName);
                        cfnrc_CFWTypeName = feedback[i].CFWTypeName;
                        cfnrc_CFWStatus = feedback[i].CFWStatus;
                        cfnrc_ForwardToNb = feedback[i].ForwardToNb;
                        cfnrc_CountryCode = feedback[i].CountryCode;
                        $("input[name=CSetCountryCode]").val(feedback[i].CountryCode);
                    }
                }
                var arrData = [
                    cfu_CFWTypeName,
                    cfu_CFWStatus,
                    cfu_ForwardToNb,
                    cfu_CountryCode,
                    cfb_CFWTypeName,
                    cfb_CFWStatus,
                    cfb_ForwardToNb,
                    cfb_CountryCode,
                    cfnry_CFWTypeName,
                    cfnry_CFWStatus,
                    cfnry_ForwardToNb,
                    cfnry_CountryCode,
                    cfnry_NoReplyTimer,
                    cfnrc_CFWTypeName,
                    cfnrc_CFWStatus,
                    cfnrc_ForwardToNb,
                    cfnrc_CountryCode
                ];
                defValueEditSettings(arrData);
                
                var defValue = convertNulltoEmptyString(cfu_CFWTypeName) + "|" +
                                convertNulltoEmptyString(cfu_CFWStatus) + "|" +
                                convertNulltoEmptyString(cfu_ForwardToNb) + "|" +
                                convertNulltoEmptyString(cfu_CountryCode) + "|" +
                                convertNulltoEmptyString(cfb_CFWTypeName) + "|" +
                                convertNulltoEmptyString(cfb_CFWStatus) + "|" +
                                convertNulltoEmptyString(cfb_ForwardToNb) + "|" +
                                convertNulltoEmptyString(cfb_CountryCode) + "|" +
                                convertNulltoEmptyString(cfnry_CFWTypeName) + "|" +
                                convertNulltoEmptyString(cfnry_CFWStatus) + "|" +
                                convertNulltoEmptyString(cfnry_ForwardToNb) + "|" +
                                convertNulltoEmptyString(cfnry_CountryCode) + "|" +
                                convertNulltoEmptyString(cfnry_NoReplyTimer) + "|" +
                                convertNulltoEmptyString(cfnrc_CFWTypeName) + "|" +
                                convertNulltoEmptyString(cfnrc_CFWStatus) + "|" +
                                convertNulltoEmptyString(cfnrc_ForwardToNb) + "|" +
                                convertNulltoEmptyString(cfnrc_CountryCode);
                $("[name=defValueCallSettings]").val(defValue);
                
            }
        }
    });
}

function defValueEditSettings(arrData) {

    // cfu
    if (arrData[0] != "") {
        $("select[name=ecfsAFSettingSelect] option[value='" + arrData[1] + "']").attr("selected", true);
        if (arrData[1] == 2) {
            $("input[name=ecfsAFMobileNo]").val(arrData[2]);
        } else {
            $("input[name=ecfsAFMobileNo]").val("");
        }
        $("span#ecfsAFMobileNoHeader").html(arrData[3] + "- ");
    } else {
        $("select[name=ecfsAFSettingSelect] option[value='0']").attr("selected", true);
        $("select[name=ecfsAFSettingSelect]").attr("disabled", true);
    }

    // cfb
    if (arrData[4] != "") {
        $("select[name=ecfsFWBSettingSelect] option[value='" + arrData[5] + "']").attr("selected", true);
        if (arrData[5] == 2) {
            $("input[name=ecfsFWBMobileNo]").val(arrData[6]);
        } else {
            $("input[name=ecfsFWBMobileNo]").val("");
        }
        $("span#ecfsFWBMobileNoHeader").html(arrData[7] + "- ");
    } else {
        $("select[name=ecfsFWBSettingSelect] option[value='0']").attr("selected", true);
        $("select[name=ecfsFWBSettingSelect]").attr("disabled", true);
    }

    //cfnry
    if (arrData[8] != "") {
        $("select[name=ecfsFWUASettingSelect] option[value='" + arrData[9] + "']").attr("selected", true);
        if (arrData[9] == 1) {
            $("input[name=ecfsFWUAMobileNo]").val("");
            $("select[name=ecfsFWUAReplyTime] option[value='" + arrData[12] + "']").attr("selected", true);
        } else if (arrData[9] == 2) {
            $("input[name=ecfsFWUAMobileNo]").val(arrData[10]);
            $("select[name=ecfsFWUAReplyTime] option[value='" + arrData[12] + "']").attr("selected", true);
        } else {
            $("input[name=ecfsFWUAMobileNo]").val("");
        }
        $("span#ecfsFWUAMobileNoHeader").html(arrData[11] + "- ");
    } else {
        $("select[name=ecfsFWUASettingSelect] option[value='0']").attr("selected", true);
        $("select[name=ecfsFWUASettingSelect]").attr("disabled", true);
    }

    // cfnrc
    if (arrData[13] != "") {
        $("select[name=ecfsFWURSettingSelect] option[value='" + arrData[14] + "']").attr("selected", true);
        if (arrData[14] == 2) {
            $("input[name=ecfsFWURMobileNo]").val(arrData[15]);
        } else {
            $("input[name=ecfsFWURMobileNo]").val("");
        }
        $("span#ecfsFWURMobileNoHeader").html(arrData[16] + "- ");
    } else {
        $("select[name=ecfsFWURSettingSelect] option[value='0']").attr("selected", true);
        $("select[name=ecfsFWURSettingSelect]").attr("disabled", true);
    }
    
}

/* ----------------------------------------------------------------
 * Function Name    : SaveEditSettings
 * Purpose          : to save Call Settings Information
 * Added by         : Edi Suryadi
 * Create Date      : September 04th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function SaveEditSettings(arrSettings) {
    
    var url = apiServer + '/api/sim/5';

    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    jQuery.support.cors = true;

    $("#CSetEditCallForwading").hide();
    $(".modal-backdrop").hide();

    $.ajax({
        url: url,
        type: 'put',
        data: JSON.stringify(arrSettings),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();

            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });

            var stat = data.errcode;
            if (stat == 0) {
                $('.PMSuccess .modal-header').html(data.errsubject);
                $('.PMSuccess .modal-body').html(data.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            } else {
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (data) {
            $("#ajax-screen-masking").hide();
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            $('.PMFailed .modal-header').html(data.errsubject);
            $('.PMFailed .modal-body').html(data.errmsg);
            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : getForwardingDestNo
 * Purpose          : to explode phone no in forwarding json feedback
 * Added by         : Edi Suryadi
 * Create Date      : September 10th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function getForwardingDestNo(str) {
    var splitStr = str.split(";");
    return splitStr[2]
}

/* ----------------------------------------------------- 
   *  eof TAB CALL SETTINGS  
   ===================================================== */


