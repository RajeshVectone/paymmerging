﻿/* ===================================================== 
   *  TAB PAYMENT PROFILE 
   ----------------------------------------------------- */

/* ----------------------------------------------------------------
 * Function Name    : ViewPaymentProfiles
 * Purpose          : to view payment profile list
 * Added by         : Edi Suryadi
 * Create Date      : January 21th, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function ViewPaymentProfiles(Msisdn, Sitecode) {
    $("#tblPaymentProfileContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblPaymentProfile\"></table>");

    var url = apiServer + '/api/CustomerPaymentProfile';
    jQuery.support.cors = true;
    var JSONSendData = {
        Msisdn: Msisdn,
        RequestType: 1,
        Sitecode: Sitecode,
        LastCCNumber: ''
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('#PPSettings select#selectCardList').html("");
            $.each(feedback, function (index, val) {
                var CardNumber = val.CardNumber;
                $('#PPSettings select#selectCardList').append('<option style="font-size:12px" value="' + CardNumber + '">XXXX XXXX XX' + CardNumber.toString().substr(0, 2) + " " + CardNumber.toString().substr(2, 4) + '</option>');
            });
            $("#tblPaymentProfileContainer").css({ "background-image": "none" });
            $('#tblPaymentProfile').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                iDisplayLength: 10,
                bPaginate: true,
                bFilter: false,
                bInfo: false,
                aaSorting: [],
                aoColumns: [
                    {
                        mDataProp: "IsDefault", sTitle: "Default Profile",
                        fnRender: function (ob) {
                            var optValue = "";
                            if (ob.aData.IsDefault == true) {
                                optValue = "<center><input type=\"radio\" name=\"optPPDefaultProfile\" value=\"" + ob.aData.CardNumber.toString().replace(/X| /g, '') + "\" checked></center>";
                            } else {
                                optValue = "<center><input type=\"radio\" name=\"optPPDefaultProfile\" value=\"" + ob.aData.CardNumber.toString().replace(/X| /g, '') + "\"></center>";
                            }
                            return optValue;
                        }
                    },
                    {
                        mDataProp: "CardNumber", sTitle: "Card Number",
                        fnRender: function (ob) {
                            var CardNo = "XXXX XXXX XX" + ob.aData.CardNumber.toString().substr(0, 2) + " " + ob.aData.CardNumber.toString().substr(2, 4);
                            return CardNo;
                        }
                    },
                    { mDataProp: "Status", sTitle: "Status" },
                    {
                        mDataProp: "SetupDate", sTitle: "Setup Date",
                        fnRender: function (ob) {
                            return convertDateISOCustom(ob.aData.SetupDate, 1);
                        }
                    },
                    {
                        mDataProp: "LastUsedDate", sTitle: "Last Date of Use",
                        fnRender: function (ob) {
                            return convertDateISOCustom(ob.aData.LastUsedDate, 1);
                        }
                    },
                    {
                        mDataProp: "LastPaymentAmount", sTitle: "Last Payment Amount",
                        fnRender: function (ob) {
                            return getCurrencySymbol(ob.aData.Currency) + " " + ob.aData.LastPaymentAmount.toFixed(2);
                        }
                    },
                    {
                        mDataProp: "blocked_by", sTitle: "Blocked By"
                    },
                    {
                        mDataProp: "block_date", sTitle: "Blocked Date"
                    },
                    {
                        mDataProp: "MoreInfoAction", sTitle: "Action", sWidth: "100px",
                        fnRender: function (ob) {
                            var dataInfo = "";
                            dataInfo += ob.aData.Status + "|";
                            dataInfo += ob.aData.SetupDate + "|";
                            dataInfo += ob.aData.LastUsedDate + "|";
                            dataInfo += ob.aData.LastPaymentAmount;

                            var actionURL = "";
                            actionURL = "<button data-info=\"" + dataInfo + "\" data-card=\"" + ob.aData.CardNumber.toString().replace(/X| /g, '') + "\" class=\"btn blue btnPPMoreInfo\">More Info</button>";
                            return actionURL;
                        }
                    },
                    //22-Feb-2019 : Moorthy : Added for Block Card 
                     {
                         mDataProp: "card_status", sTitle: "CC fraud", sWidth: "100px",
                         fnRender: function (ob) {
                             var actionURL = "";
                             if (ob.aData.card_status == 0)
                                 actionURL = "<button data-info=\"" + ob.aData.card_status + "\" data-card=\"" + ob.aData.CardNumber.toString().replace(/X| /g, '') + "\" class=\"btn blue btnPPBlockCard\">Block Card</button>";
                             else
                                 actionURL = "<button data-info=\"" + ob.aData.card_status + "\" data-card=\"" + ob.aData.CardNumber.toString().replace(/X| /g, '') + "\" class=\"btn blue btnPPBlockCard\">UnBlock Card</button>";
                             return actionURL;
                         }
                     },
                ],
                fnDrawCallback: function () {
                    $("#tblPaymentProfile tbody td").css({ "font-size": "12px" });
                }
            });
            $("#tblPaymentProfile tbody td").css({ "font-size": "12px" });
            $("#tblPaymentProfile_wrapper .row-fluid:eq(0)").remove();
        },
        complete: function (feedback) {
            $("#btnPaymentProfileSettingsContainer").html("").append("<button id=\"btnPaymentProfileSettings\" class=\"btn blue\" style=\"float:right;width:100px;height:35px;\">Settings</button>");
            if ($("input[name=optPPDefaultProfile]:radio").length) {
                $("#PPSetDefaultProfile input[name=PPSDPLastSelectedProfile]").val($("input[name=optPPDefaultProfile]:checked").val());
            }
        },
        error: function (feedback) {
            $("#TabProductPackagesLoader").hide();
            $("#tblProductPackagesContainer").html("").append("No package found").show();
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : ViewDetailInfo
 * Purpose          : to view detail of payment profile
 * Added by         : Edi Suryadi
 * Create Date      : January 21th, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function ViewDetailInfo(Msisdn, Sitecode, LastCCNumber) {
    var url = apiServer + '/api/CustomerPaymentProfile';
    jQuery.support.cors = true;
    var JSONSendData = {
        Msisdn: Msisdn,
        RequestType: 2,
        Sitecode: Sitecode,
        LastCCNumber: LastCCNumber
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('#PPMoreInfo').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
            var CardNumber = "XXXX XXXX XX" + feedback.CardNumber.toString().substr(0, 2) + " " + feedback.CardNumber.toString().substr(2, 4);
            $("#PPMoreInfo td#PPMoreInfo_CardHolder").html(convertNulltoEmptyString(feedback.NameCardholder));
            $("#PPMoreInfo td#PPMoreInfo_CardType").html(convertNulltoEmptyString(feedback.CardType));
            $("#PPMoreInfo td#PPMoreInfo_CardNumber").html(convertNulltoEmptyString(CardNumber));
            $("#PPMoreInfo td#PPMoreInfo_ExpireDate").html(convertNulltoEmptyString(feedback.ExpDate));
            $("#PPMoreInfo td#PPMoreInfo_Street").html(convertNulltoEmptyString(feedback.Street));
            $("#PPMoreInfo td#PPMoreInfo_City").html(convertNulltoEmptyString(feedback.City));
            $("#PPMoreInfo td#PPMoreInfo_Country").html(convertNulltoEmptyString(feedback.Country));
            $("#PPMoreInfo td#PPMoreInfo_PostCode").html(convertNulltoEmptyString(feedback.Postcode));
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : SetDefaultProfile
 * Purpose          : to set the default of payment profile
 * Added by         : Edi Suryadi
 * Create Date      : January 21th, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function SetDefaultProfile(Msisdn, Sitecode, LastCCNumber) {
    var url = apiServer + '/api/CustomerPaymentProfile';

    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    jQuery.support.cors = true;
    var JSONSendData = {
        Msisdn: Msisdn,
        RequestType: 8,
        Sitecode: Sitecode,
        LastCCNumber: LastCCNumber
    };

    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();

            var stat = data.errcode;
            if (stat == 0) {
                $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
                $('.PMSuccess .modal-header').html(data.errsubject);
                $('.PMSuccess .modal-body').html(data.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            } else {
                $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (data) {
            $("#ajax-screen-masking").hide();
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            $('.PMFailed .modal-header').html(data.errsubject);
            $('.PMFailed .modal-body').html(data.errmsg);
            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : SettingUpdate
 * Purpose          : to update the Auto Top-Up settings
 * Added by         : Edi Suryadi
 * Create Date      : January 21th, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function SettingUpdate(Msisdn, Sitecode, LastCCNumber, AxTopupStatus, AxTopupAmount, Currency, PaymentType) {
    var url = apiServer + '/api/CustomerPaymentProfile';

    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    jQuery.support.cors = true;
    var JSONSendData = {
        Msisdn: Msisdn,
        RequestType: 5,
        Sitecode: Sitecode,
        LastCCNumber: LastCCNumber,
        AxTopupStatus: AxTopupStatus,
        AxTopupAmount: AxTopupAmount,
        Currency: Currency,
        PaymentType: PaymentType
    };

    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();

            var stat = data.errcode;
            if (stat == 0) {
                $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
                $('.PMSuccess .modal-header').html(data.errsubject);
                $('.PMSuccess .modal-body').html(data.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            } else {
                $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (data) {
            $("#ajax-screen-masking").hide();
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            $('.PMFailed .modal-header').html(data.errsubject);
            $('.PMFailed .modal-body').html(data.errmsg);
            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });
}

//22-Feb-2019 : Moorthy : Added for Block Card 
function BlockCard(Msisdn, LastCCNumber, card_status, CalledBy) {
    var url = apiServer + '/api/CustomerPaymentProfile';

    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    jQuery.support.cors = true;
    var JSONSendData = {
        RequestType: 9,
        Msisdn: Msisdn,
        LastCCNumber: LastCCNumber,
        card_status: card_status,
        CalledBy: CalledBy
    };

    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();

            var stat = data.errcode;
            if (stat == 0) {
                $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
                $('.PMSuccess .modal-header').html('General');
                $('.PMSuccess .modal-body').html(data.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
                setTimeout(function () { window.location.reload(); }, 5000);
            } else {
                $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
                $('.PMFailed .modal-header').html('General');
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (data) {
            $("#ajax-screen-masking").hide();
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            $('.PMFailed .modal-header').html('Error');
            $('.PMFailed .modal-body').html(data.errmsg);
            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });
}
/* ----------------------------------------------------- 
   *  eof PAYMENT PROFILE  
   ===================================================== */


