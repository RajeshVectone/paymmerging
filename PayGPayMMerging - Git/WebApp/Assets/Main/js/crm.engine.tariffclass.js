﻿/* ===================================================== 
   *  TAB GENERAL 
   ----------------------------------------------------- */

/* ----------------------------------------------------------------
 * Function Name    : TariffClass
 * Purpose          : to show TariffClass customer Information
 * Added by         : Edi Suryadi
 * Create Date      : September 04th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function TariffclassInfo(productCode, searchText, Sitecode) {
    var url = apiServer + '/api/customersearch';

    jQuery.support.cors = true;
    var JSONSendData = {
        Product_Code: productCode,
        SearchBy: 'MobileNo',
        SearchText: searchText,
        Sitecode: Sitecode,
        Search_Type: 4

    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#tblCustomerTeriffClassLoader").hide();
            $("#tblCustomerTeriffClassInfoContainer").show();
            var errcode = -1;
            if (feedback != '' && feedback != null) {
                errcode = feedback.errcode;
            }

            if (errcode == 0) {

                var SubFirstusage_string = feedback.Firstusage_string.substring(0, 10);
                var Substartdate_string = feedback.startdate_string.substring(0, 10);
                var SubEnddate_String = feedback.Enddate_String.substring(0, 10);

                if (SubFirstusage_string == "01-01-0001" || SubFirstusage_string == "01-01-1900") {
                    $("table#tblCustomerTeriffClassInfo td#tdCGIFirstusage").html("");
                }
                else {
                    $("table#tblCustomerTeriffClassInfo td#tdCGIFirstusage").html(feedback.Firstusage_string);
                }
                if (Substartdate_string == "01-01-0001" || Substartdate_string == "01-01-1900") {
                    $("table#tblCustomerTeriffClassInfo td#tdCGIStartdate").html("");
                }
                else {
                    $("table#tblCustomerTeriffClassInfo td#tdCGIStartdate").html(feedback.startdate_string);
                }
                if (SubEnddate_String == "01-01-0001" || SubEnddate_String == "01-01-1900") {
                    $("table#tblCustomerTeriffClassInfo td#tdCGIEnddate").html("");
                }
                else {
                    $("table#tblCustomerTeriffClassInfo td#tdCGIEnddate").html(feedback.Enddate_String);
                }

                var smart_tariffclass = feedback.smart_tariffclass;
                var smart_startdate_String = feedback.smart_startdate_String.substring(0, 10);
                var smart_enddate_String = feedback.smart_enddate_String.substring(0, 10);

                $("table#tblCustomerTeriffClassInfo td#tdsmart_tariffclass").html(feedback.smart_tariffclass);

                if (smart_startdate_String == "01-01-0001" || smart_startdate_String == "01-01-1900") {
                    $("table#tblCustomerTeriffClassInfo td#tdsmart_startdate").html("");
                }
                else {
                    $("table#tblCustomerTeriffClassInfo td#tdsmart_startdate").html(feedback.smart_startdate_String);
                }
                if (smart_enddate_String == "01-01-0001" || smart_enddate_String == "01-01-1900") {
                    $("table#tblCustomerTeriffClassInfo td#tdsmart_enddate").html("");
                }
                else {
                    $("table#tblCustomerTeriffClassInfo td#tdsmart_enddate").html(feedback.smart_enddate_String);
                }

                $("table#tblCustomerTeriffClassInfo td#tdCGISitecode").html(feedback.Site_code);
                $("table#tblCustomerTeriffClassInfo td#tdCGICustomercode").html(feedback.Customer_code);
                $("table#tblCustomerTeriffClassInfo td#tdCGIBatchcode").html(feedback.Batch_code);
                $("table#tblCustomerTeriffClassInfo td#tdCGISerialCode").html(feedback.Serial_code);
                $("table#tblCustomerTeriffClassInfo td#tdCGIAccountstatus").html(feedback.account_status);
                $("table#tblCustomerTeriffClassInfo td#tdCGITariffclass").html(feedback.tariff_class);
                //$("table#tblCustomerTeriffClassInfo td#tdCGIBalanceunit").html(feedback.Balance_unit);
                //$("table#tblCustomerTeriffClassInfo td#tdCGIBalanceCreditBonus").html(feedback.Balance_Credit_Bonus);
                //$("table#tblCustomerTeriffClassInfo td#tdCGITotalBalance").html(feedback.Total_Balance);

            } else {
                $("table#tblCustomerTeriffClassInfo td#tdCGISitecode").html("");
                $("table#tblCustomerTeriffClassInfo td#tdCGIFirstusage").html("");
                $("table#tblCustomerTeriffClassInfo td#tdCGICustomercode").html("");
                $("table#tblCustomerTeriffClassInfo td#tdCGIStartdate").html("");
                $("table#tblCustomerTeriffClassInfo td#tdCGIBatchcode").html("");
                $("table#tblCustomerTeriffClassInfo td#tdCGIEnddate").html("");
                $("table#tblCustomerTeriffClassInfo td#tdCGISerialCode").html("");
                $("table#tblCustomerTeriffClassInfo td#tdsmart_tariffclass").html("");
                $("table#tblCustomerTeriffClassInfo td#tdCGIAccountstatus").html("");
                $("table#tblCustomerTeriffClassInfo td#tdsmart_startdate").html("");
                $("table#tblCustomerTeriffClassInfo td#tdCGITariffclass").html("");
                $("table#tblCustomerTeriffClassInfo td#tdsmart_enddate").html("");
                //$("table#tblCustomerTeriffClassInfo td#tdCGIBalanceunit").html("");
                //$("table#tblCustomerTeriffClassInfo td#tdCGIBalanceCreditBonus").html("");
                //$("table#tblCustomerTeriffClassInfo td#tdCGITotalBalance").html("");
            }
        }
    });
}

