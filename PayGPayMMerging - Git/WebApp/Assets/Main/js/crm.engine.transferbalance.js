﻿function TransferBalance(sitecode, donor_mobileno, receiver_mobileno, amount, username) {
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    var url = apiServer + '/api/TransferBalance';
    jQuery.support.cors = true;
    var JSONSendData = {
        sitecode: sitecode,
        donor_mobileno: donor_mobileno,
        receiver_mobileno: receiver_mobileno,
        amount: amount,
        username: username        
    };
    $.ajax
({
    url: url,
    type: 'post',
    data: JSON.stringify(JSONSendData),
    dataType: 'json',
    contentType: "application/json;charset=utf-8",
    cache: false,
    beforeSend: function () {
        $("#ajax-screen-masking").show();
    },
    success: function (feedback) {
        $("#ajax-screen-masking").hide();
        alert(feedback.errmsg);
        if (feedback.errcode == 0)
            location.reload();
    },
    error: function (feedback) {
        $("#ajax-screen-masking").hide();
    }
});
}