﻿function ViewDDStepUpProcess(siteCode) {
    var i = 0;
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    $("#tblDDOperationContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblDDOperation\"></table>");
    var url = apiServer + '/api/PMBOPayWay';
    jQuery.support.cors = true;
    var JSONSendData = {
        sitecode: siteCode
    };
    $.ajax({
        url: url,
        type: 'get',
        data: JSONSendData,
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#tblDDOperationContainer").hide();
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            $("#tblDDOperationContainer").show();
            $('#tblDDOperation').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                iDisplayLength: 20,
                bPaginate: true,
                bFilter: false,
                bInfo: false,
                aaSorting: [[0, "desc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No call PayAway files found."
                },
                aoColumns: [
                    { mDataProp: "opsid", sTitle: "File ID" },
                    { mDataProp: "filename", sTitle: "File Name", },
                    {
                        mDataProp: "createdate", sTitle: "File Date",
                        fnRender: function (ob) {
                            return convertDate01(ob.aData.createdate);
                        }
                    },
                    { mDataProp: "user_name", sTitle: "Login" },
                    { mDataProp: "operation_name", sTitle: "Operation" },
                    { mDataProp: "total_rows", sTitle: "Nb Of Trans" },
                    {
                        sTitle: "Download", mDataProp: "userid", "bSortable": false,
                        fnRender: function (ob) {
                            i = i + 1;
                            var DownloadList = "<i class='icon-download-alt icon-white' style='font-size:22px'></i>&nbsp;";
                            DownloadList = DownloadList + "<a href='#' onclick='DownloadPaywayFiles(" + ob.aData.opsid + ");'> Download</a>"
                            return DownloadList;
                        }
                    },
                    {
                        sTitle: "Status", mDataProp: "status",
                        fnRender: function (ob) {
                            var news = "";
                            if (ob.aData.status == '-1') {
                                var news = "NO";
                            }
                            else {
                                news = "YES"
                            }
                            return news;
                        }
                    },
                ],
                fnDrawCallback: function () {
                    $("#tblDDOperation tbody").css({ "font-size": "12px" });
                }
            });
        },
        error: function () {
            $("#ajax-screen-masking").hide();
        }
    });
}

function DownloadPaywayFiles(fileId,siteCode) {
    $("#ajax-screen-masking #downloadIFrames").remove();
    var url = '/Download/DownloadPaywayFiles';
    jQuery.support.cors = true;
    var JSONSendData = {
        fileId: fileId,
        sitecode:siteCode
    };
    $.ajax({
        url: url,
        type: 'get',
        data: JSONSendData,
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            if (feedback.err_code == 0) {
                var srcValues = '/Download/DownloadPaywaysList?id=' + feedback.Text + '&filename=' + feedback.err_message;
                var IFrameData = $('<iframe/>', { id: 'downloadIFrames', src: srcValues, style: 'display:none' });
                $("#ajax-screen-masking").append(IFrameData);
                ViewDDStepUpProcess(siteCode);
            }
            else if (feedback.err_code == -1) {
                alert(feedback.err_message);
            }
        }
    });

    
}

function ViewDDgosubmit(siteCode) {
    
   
    var i = 0;
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    $("#tblDDgoSubmitContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblDDgoSubmit\"></table>");
    var url = apiServer + '/api/PMBOPayWay';
    jQuery.support.cors = true;
    var JSONSendData = {
        sitecode: siteCode,
        ind: '1'
    };
    $.ajax({
        url: url,
        type: 'get',
        data: JSONSendData,
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#tblDDgoSubmitContainer").hide();
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {
             
            $("#ajax-screen-masking").hide();
            $("#tblDDgoSubmitContainer").show();
            $('#tblDDgoSubmit').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                iDisplayLength: 20,
                bPaginate: true,
                bFilter: true,
                bInfo: false,
                aaSorting: [[0, "desc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Records in DD Queue."
                },
                aoColumns: [
                    { mDataProp: "pp_customer_id", sTitle: "Customer ID" },
                    { mDataProp: "subscriberid", sTitle: "Subscriber ID" },
                    { mDataProp: "Name", sTitle: "Name" },
                    { mDataProp: "email", sTitle: "Email" },
                    { mDataProp: "AcHldrName", sTitle: "AC Holder Name" },
                    { mDataProp: "reference_id", sTitle: "Reference ID" },
                    { mDataProp: "charge_date", sTitle: "Charge Date" },
                    { mDataProp: "amount", sTitle: "Amount" },
                    //{ mDataProp: "currency", sTitle: "Currency" },
                    { mDataProp: "mandate_id", sTitle: "Mandate Id" },
                    { mDataProp: "status", sTitle: "Status" },
                    { mDataProp: "cust_reference", sTitle: "Period" },
                    { mDataProp: "AccountStatus", sTitle: "Account Status" },
                    //{ mDataProp: "BankName", sTitle: "  " },
                   // { mDataProp: "cust_metadata_reference", sTitle: "Customer Reference" }

                    //{
                    //    sTitle: "Download", mDataProp: "userid", "bSortable": false,
                    //    fnRender: function (ob) {
                    //        i = i + 1;
                    //        var DownloadList = "<i class='icon-download-alt icon-white' style='font-size:22px'></i>&nbsp;";
                    //        DownloadList = DownloadList + "<a href='#' onclick='DownloadPaywayFiles(" + ob.aData.opsid + ");'> Download</a>"
                    //        return DownloadList;
                    //    }
                    //},
                    //{
                    //    sTitle: "Status", mDataProp: "status",
                    //    fnRender: function (ob) {
                    //        var news = "";
                    //        if (ob.aData.status == '-1') {
                    //            var news = "NO";
                    //        }
                    //        else {
                    //            news = "YES"
                    //        }
                    //        return news;
                    //    }
                    //},
                ],
                fnDrawCallback: function () {
                    $("#tblDDgoSubmit tbody").css({ "font-size": "12px" });
                }
            });
        },
        error: function () {
            $("#ajax-screen-masking").hide();
        }
    });
}

function ViewDDgoScuccess(siteCode) {
 
  
    var i = 0;
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    $("#tblDDgoSuccessContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblDDgoSuccess\"></table>");
    var url = apiServer + '/api/PMBOPayWay';
    jQuery.support.cors = true;
    var JSONSendData = {
        sitecode: siteCode,
        ind: '2'
    };
    $.ajax({
        url: url,
        type: 'get',
        data: JSONSendData,
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#tblDDgoSuccessContainer").hide();
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {

            $("#ajax-screen-masking").hide();
            $("#tblDDgoSuccessContainer").show();
            $('#tblDDgoSuccess').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                iDisplayLength: 20,
                bPaginate: true,
                bFilter: true,
                bInfo: false,
                aaSorting: [[0, "desc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Records in Success."
                },
                aoColumns: [
                    { mDataProp: "pp_customer_id", sTitle: "Customer ID" },
                    { mDataProp: "subscriberid", sTitle: "Subscriber ID" },
                    { mDataProp: "Name", sTitle: "Name" },
                    { mDataProp: "email", sTitle: "Email" },
                    { mDataProp: "AcHldrName", sTitle: "AC Holder Name" },
                    { mDataProp: "reference_id", sTitle: "Reference ID" },
                    { mDataProp: "charge_date", sTitle: "Charge Date" },
                    { mDataProp: "amount", sTitle: "Amount" },
                    //{ mDataProp: "currency", sTitle: "Currency" },
                    { mDataProp: "mandate_id", sTitle: "Mandate Id" },
                    { mDataProp: "status", sTitle: "Status" },
                    { mDataProp: "cust_reference", sTitle: "Period" },
                    { mDataProp: "AccountStatus", sTitle: "Account Status" },
                   // { mDataProp: "BankName", sTitle: "Bank Name" },
                   // { mDataProp: "cust_metadata_reference", sTitle: "Customer Reference" }

                ],
                fnDrawCallback: function () {
                    $("#tblDDgoSuccess tbody").css({ "font-size": "12px" });
                }
            });
        },
        error: function () {
            $("#ajax-screen-masking").hide();
        }
    });
}


function ViewDDgoReject(siteCode) {
 

    var i = 0;
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    $("#tblDDgoRejectContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblDDgoReject\"></table>");
    var url = apiServer + '/api/PMBOPayWay';
    jQuery.support.cors = true;
    var JSONSendData = {
        sitecode: siteCode,
        ind: '3'
    };
    $.ajax({
        url: url,
        type: 'get',
        data: JSONSendData,
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#tblDDgoRejectContainer").hide();
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {

            $("#ajax-screen-masking").hide();
            $("#tblDDgoRejectContainer").show();
            $('#tblDDgoReject').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                iDisplayLength: 20,
                bPaginate: true,
                bFilter: true,
                bInfo: false,
                aaSorting: [[0, "desc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Records in DD Reject List."
                },
                aoColumns: [
                    { mDataProp: "pp_customer_id", sTitle: "Customer ID" },
                    { mDataProp: "subscriberid", sTitle: "Subscriber ID" },
                    { mDataProp: "Name", sTitle: "Name" },
                    { mDataProp: "email", sTitle: "Email" },
                    { mDataProp: "AcHldrName", sTitle: "AC Holder Name" },
                    { mDataProp: "reference_id", sTitle: "Reference ID" },
                    { mDataProp: "charge_date", sTitle: "Charge Date" },
                    { mDataProp: "amount", sTitle: "Amount" },
                   // { mDataProp: "currency", sTitle: "Currency" },
                    { mDataProp: "mandate_id", sTitle: "Mandate Id" },
                    { mDataProp: "status", sTitle: "Status" },
                    { mDataProp: "cust_reference", sTitle: "Period" },
                    { mDataProp: "AccountStatus", sTitle: "Account Status" },
                   // { mDataProp: "BankName", sTitle: "Bank Name" },
                   // { mDataProp: "cust_metadata_reference", sTitle: "Customer Reference" }

                ],
                fnDrawCallback: function () {
                    $("#tblDDgoReject tbody").css({ "font-size": "12px" });
                }
            });
        },
        error: function () {
            $("#ajax-screen-masking").hide();
        }
    });
}