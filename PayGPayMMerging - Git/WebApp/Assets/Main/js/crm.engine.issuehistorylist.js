﻿/* ===================================================== 
   *  Issue History Report Script 
   ----------------------------------------------------- */
//Issue History Report Search
function getissuehistorylist(sitecode, date_fr, date_to) {
    $("#tblDDOperationContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblDDOperation\"></table>");
    $("#tblDDOperationContainer").css({ "min-height": "100px", "background-repeat": "no-repeat", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var row = 0;
    var url = apiServer + "/api/IssueHistoryList";
    var JSONSendData = {
        sitecode: sitecode,
        date_from: date_fr,
        date_to: date_to
    };
    $.ajax
    ({
        url: url,
        type: "POST",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('#tblDDOperation').html('');
            $("#tblDDOperationContainer").css({ "background-image": "none" });
            $('#tblDDOperation').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                iDisplayLength: 10,
                bPaginate: true,
                bFilter: true,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [[0, "desc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No records found."
                },
                aoColumns: [
                    { mDataProp: "Customer_Name", sTitle: "Customer Name" },
                    { mDataProp: "Subscriber_ID", sTitle: "Subscriber ID" },
                    { mDataProp: "Mobile_number", sTitle: "Mobile No" },
                    { mDataProp: "contact_type", sTitle: "Contact Type" },
                    { mDataProp: "Call_Type", sTitle: "Call Type" },
                    { mDataProp: "Category", sTitle: "Category" },
                    { mDataProp: "Sub_category", sTitle: "Sub Category" },
                    { mDataProp: "Status", sTitle: "Status" },
                    { mDataProp: "Interval", sTitle: "Interval" },
                    { mDataProp: "esclated_to", sTitle: "Escalated To" },
                    { mDataProp: "assigned_to", sTitle: "Assigned To" },
                    { mDataProp: "resolved_by", sTitle: "Resolved by" },
                    { mDataProp: "resolved_date_String", sTitle: "Resolved date" },
                    //{ mDataProp: "typedesc", sTitle: "Type Desc." },
                    { mDataProp: "submitdate_String", sTitle: "Submit Date" },
                    { mDataProp: "Submitby", sTitle: "Submit By" }
                ],
                fnDrawCallback: function () {
                    $("#tblDDOperation tbody td").css({ "font-size": "12px" });
                }
            });
            $("#tblDDOperation tbody td").css({ "font-size": "12px" });
        },
        error: function (feedback) {
            $("#tblDDOperationContainer").css({ "background-image": "none" }).html("").append("No records found.").show();
        }
    });
}

//Issue History Report Download
function downloadissuehistorylist(sitecode, date_fr, date_to) {
    var i = 0;
    jQuery.support.cors = true;
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader1").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    $("#ajax-screen-masking #downloadIFrame").remove();
    var url = '/Download/DownloadIssueHistoryList';
    var JSONSendData = {
        sitecode: sitecode,
        date_fr: date_fr,
        date_to: date_to
    };
    $.ajax
    ({
        url: url,
        type: 'get',
        dataType: 'json',
        data: JSONSendData,
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {

            if (feedback.errcode == 0) {
                $("#ajax-screen-masking").hide();
                var IFrameData = $('<iframe/>', { id: 'downloadIFrame', src: '/Download/DownloadFinanceIssueHistoryList?id=' + feedback.Text, style: 'display:none' });
                $("#ajax-screen-masking").append(IFrameData);
            }
        }
    });
}