﻿var modeServer = 0;

if (modeServer == 0) {
    /* Local Server */
//    var apiServer = 'http://localhost:804';
     var apiServer = 'http://localhost:1898';
   // var apiServer = 'http://localhost:1899';
    //var apiServer = 'http://192.168.1.230:9877';
    ////31-Jul-2015 : Added Test API server url
    //var apiServer = 'http://192.168.2.102:9691';
    //var apiServer = 'http://192.168.1.230:8695'; //Developer Testing Server
    //var apiServer = 'http://192.168.1.230:6565';//Staging Server
    //var apiServer = 'http://192.168.1.230:5216'; //Live

    //var apiServer = 'http://localhost:38631';

} else {
    /* Real/Staging Server 
    var apiServer = 'http://192.168.41.23:1830';
    var apiServer1 = 'http://192.168.41.23:8033';
    var apiServer2 = 'http://sws.vectone.com';*/
    //var apiServer = 'http://192.168.1.230:6565';
    //var apiServer = 'http://192.168.1.230:8695';

    //var apiServer = 'http://localhost:1830';
    var apiServer = 'http://localhost:804';
    //var apiServer = 'http://localhost:1899';
    ////31-Jul-2015 : Added Test API server url
    //var apiServer = 'http://192.168.2.102:9691';
    //var apiServer = 'http://192.168.1.230:8695'; //Developer Testing Server
    //var apiServer = 'http://192.168.1.230:6565';//Staging Server
    //var apiServer = 'http://192.168.1.230:5216'; //Live

    var apiServer1 = 'http://192.168.41.23:8033';
    var apiServer2 = 'http://sws.vectone.com';

}


/* ----------------------------------------------------------------
 * Function Name    : exportTableToExcel
 * Purpose          : To export html table in to excel format
 * Added by         : Edi Suryadi
 * Create Date      : August 2nd, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
var exportTableToExcel = (function () {
    var uri = 'data:application/vnd.ms-excel;base64,'
      , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
      , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
      , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
    return function (table, name, filename) {
        if (!table.nodeType) table = document.getElementById(table)
        var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
        //window.location.href = uri + base64(format(template, ctx))

        document.getElementById("downloadAnchor").href = uri + base64(format(template, ctx));
        document.getElementById("downloadAnchor").download = filename;
        document.getElementById("downloadAnchor").click();

    }
})()
/* ---------------------------------------------------------------- */

/* ----------------------------------------------------------------
 * Function Name    : CCSubsList
 * Purpose          : List all the credit card subscription list
 * Added by         : Edi Suryadi
 * Create Date      : August 27th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function CCSubsList(mobileno) {
    var url = apiServer + '/api/payment/' + mobileno
    $.getJSON(url, function (data) {

        $('#CCDetailsSaved').dataTable({
            aaData: data,
            bDestroy: true,
            bRetrieve: true,
            bFilter: false,
            bLengthChange: false,
            iDisplayLength: 5,
            aaSorting: [[0, "asc"]],
            oLanguage: {
                "sEmptyTable": 'No saved card found'
            },
            aoColumns: [

				{ mDataProp: "Last6digitsCC", sTitle: 'Credit Card Number' },
				{ mDataProp: "expirydate", sTitle: 'Expiry Date' },
				{
				    mDataProp: 'SubscriptionID', sTitle: 'Action', bSortable: false,
				    fnRender: function (ob) {
				        var content = '<input type="button" class="btn blue selectSavedCard" value="Select" alt="' + ob.aData.SubscriptionID + '|' + ob.aData.Last6digitsCC + '|' + ob.aData.cvv_number + '" />';
				        return content;
				    }
				}

            ],
            fnDrawCallback: function (ob) {
                $('.dataTables_info').hide();
                if (ob.fnRecordsTotal() <= ob._iDisplayLength) {
                    $("#CCDetailsSaved_wrapper .row-fluid").remove();
                    $('.dataTables_paginate').hide();
                }
            }
        });
        $("#CCDetailsSaved_wrapper .row-fluid").eq(0).remove();
        $('#loader').hide();
        $('#CCDetailsSaved').show();
    });
}

/* ----------------------------------------------------------------
 * Function Name    : capitalizeString
 * Purpose          : to capitalize string
 * Added by         : Edi Suryadi
 * Create Date      : September 05th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function capitalizeString(str) {
    return str.replace(/\w\S*/g, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}

/* ----------------------------------------------------------------
 * Function Name    : convertDate01
 * Purpose          : to convert date from 25/09/2013 into 25/Sep/2013 
 * Added by         : Edi Suryadi
 * Create Date      : September 05th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function convertDate01(inputDate) {
    if (convertNulltoEmptyString(inputDate) != "") {
        var arrShortMonthList = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
        var splitDate = inputDate.split('/');
        return splitDate[0] + "/" + arrShortMonthList[parseInt(splitDate[1]) - 1] + "/" + splitDate[2];
    } else {
        return "";
    }
}

/* ----------------------------------------------------------------
 * Function Name    : connvertDate02
 * Purpose          : to convert date from 25/Sep/2013 into 25/09/2013 
 * Added by         : Edi Suryadi
 * Create Date      : September 24th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function convertDate02(inputDate) {
    var arrShortMonthList = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
    var splitDate = inputDate.split("/");
    var result = "";
    switch (splitDate[1].toLowerCase()) {
        case arrShortMonthList[0].toLowerCase():
            result = "01";
            break;
        case arrShortMonthList[1].toLowerCase():
            result = "02";
            break;
        case arrShortMonthList[2].toLowerCase():
            result = "03";
            break;
        case arrShortMonthList[3].toLowerCase():
            result = "04";
            break;
        case arrShortMonthList[4].toLowerCase():
            result = "05";
            break;
        case arrShortMonthList[5].toLowerCase():
            result = "06";
            break;
        case arrShortMonthList[6].toLowerCase():
            result = "07";
            break;
        case arrShortMonthList[7].toLowerCase():
            result = "08";
            break;
        case arrShortMonthList[8].toLowerCase():
            result = "09";
            break;
        case arrShortMonthList[9].toLowerCase():
            result = "10";
            break;
        case arrShortMonthList[10].toLowerCase():
            result = "11";
            break;
        case arrShortMonthList[11].toLowerCase():
            result = "12";
            break;
    }
    return splitDate[0] + "/" + result + "/" + splitDate[2];
}

/* ----------------------------------------------------------------
 * Function Name    : convertDate03
 * Purpose          : to convert date from 2013/09/25 into 25/Sep/2013 
 * Added by         : Edi Suryadi
 * Create Date      : September 27th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function convertDate03(inputDate) {
    if (convertNulltoEmptyString(inputDate) != "") {
        var arrShortMonthList = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
        var splitDate = inputDate.split('/');
        return splitDate[2] + "/" + arrShortMonthList[parseInt(splitDate[1]) - 1] + "/" + splitDate[0];
    } else {
        return "";
    }
}

/* ----------------------------------------------------------------
 * Function Name    : convertDate04
 * Purpose          : to convert date from 2013/09/25 into 25 September 2013 
 * Added by         : Edi Suryadi
 * Create Date      : November 21th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function convertDate04(inputDate) {
    if (convertNulltoEmptyString(inputDate) != "") {
        var arrLongMonthList = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
        var splitDate = inputDate.split('/');
        return splitDate[2] + " " + arrLongMonthList[parseInt(splitDate[1]) - 1] + " " + splitDate[0];
    } else {
        return "";
    }
}

/* ----------------------------------------------------------------
 * Function Name    : convertDate05
 * Purpose          : to convert date from 25/09/2013 into 2013-09-25 
 * Added by         : Edi Suryadi
 * Create Date      : December 12th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function convertDate05(inputDate) {
    if (convertNulltoEmptyString(inputDate) != "") {
        var splitDate = inputDate.split('/');
        return splitDate[2] + "-" + splitDate[1] + "-" + splitDate[0];
    } else {
        return "";
    }
}

/* ----------------------------------------------------------------
 * Function Name    : convertDate06
 * Purpose          : to convert date from 201325 into Sep/September 2013 
 * Added by         : Edi Suryadi
 * Create Date      : February 04th, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function convertDate06(inputDate, OutputType) {
    if (convertNulltoEmptyString(inputDate) != "") {
        var arrShortMonthList = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
        var arrLongMonthList = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
        var iYear = inputDate.substr(0, 4);
        var iMonth = parseInt(inputDate.substr(4, 2)) - 1;
        var result = "";
        switch (OutputType) {
            case 0:
                result = arrShortMonthList[iMonth] + " " + iYear;
                break;
            case 1:
                result = arrLongMonthList[iMonth] + " " + iYear;
                break;
        }
        return result;
    } else {
        return "";
    }
}

/* ----------------------------------------------------------------
 * Function Name    : convertDateISO8601
 * Purpose          : to convert date from 2013-10-21T09:25:13.73 into 21/Oct/2013 09:25
 * Added by         : Edi Suryadi
 * Create Date      : October 21th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function convertDateISO8601(inputDate, type) {
    if (convertNulltoEmptyString(inputDate) != "" && inputDate.indexOf("T") >= 0) {
        var result = "";
        if (inputDate != "0001-01-01T00:00:00") {
            var arrShortMonthList = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
            var splitDate = inputDate.split('T');
            var splitDateLeft = splitDate[0].split('-');
            var aDate = splitDateLeft[2] + "/" + arrShortMonthList[parseInt(splitDateLeft[1]) - 1] + "/" + splitDateLeft[0];
            var aTime = splitDate[1].substr(0, 8);
            if (type == 0) {
                result = aDate + " " + aTime;
            } else if (type == 1) {
                result = aDate;
            } else if (type == 2) {
                // 21-11-2013 09:25
                result = splitDateLeft[2] + "-" + splitDateLeft[1] + "-" + splitDateLeft[0] + " " + aTime;
            } else if (type == 3) {
                // 21-11-2013
                result = splitDateLeft[2] + "-" + splitDateLeft[1] + "-" + splitDateLeft[0];
            }
        }
        return result;
    } else {
        return "";
    }
}

/* ----------------------------------------------------------------
 * Function Name    : convertNLDateToHuman
 * Purpose          : to convert date from 20131009100000 into 2013/10/09 10:00:00 
 * Added by         : Edi Suryadi
 * Create Date      : October 03rd, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function convertNLDateToHuman(inputDate, formatType) {
    if (inputDate.length == 14) {
        var nlYear = inputDate.substring(0, 4);
        var nlMonth = inputDate.substring(4, 6);
        var nlDate = inputDate.substring(6, 8);
        var nlHour = inputDate.substring(8, 10);
        var nlMinute = inputDate.substring(10, 12);
        var nlSecond = inputDate.substring(12, 14);
        if (formatType == 0) {
            var HumanFormat = nlYear + "/" + nlMonth + "/" + nlDate + " " + nlHour + ":" + nlMinute + ":" + nlSecond;
        } else if (formatType == 1) {
            var HumanFormat = nlDate + "/" + nlMonth + "/" + nlYear + " " + nlHour + ":" + nlMinute + ":" + nlSecond;
        }
    } else {
        var HumanFormat = inputDate;
    }
    return HumanFormat;
}

/* ----------------------------------------------------------------
 * Function Name    : convertNulltoEmptyString
 * Purpose          : to convert null to Empty String 
 * Added by         : Edi Suryadi
 * Create Date      : September 10th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function convertNulltoEmptyString(str) {
    if (str == null || str == 'null') {
        return "";
    } else {
        return str;
    }
}

/* ----------------------------------------------------------------
 * Function Name    : convertNulltoZero
 * Purpose          : to convert null to Zero 
 * Added by         : Edi Suryadi
 * Create Date      : September 10th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function convertNulltoZero(str) {
    if (str == null || str == 'null') {
        return 0;
    } else {
        return str;
    }
}

/* ----------------------------------------------------------------
 * Function Name    : convertEmptyStringtoSpace
 * Purpose          : to EmptyString to Space 
 * Added by         : Edi Suryadi
 * Create Date      : October 01st, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function convertEmptyStringtoSpace(str) {
    if (str == "") {
        return "&nbsp;";
    } else {
        return str;
    }
}

/* ----------------------------------------------------------------
 * Function Name    : convertNulltoNA
 * Purpose          : to convert null to N/A 
 * Added by         : Edi Suryadi
 * Create Date      : November 06th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function convertNulltoNA(str) {
    if (str == null || str == 'null' || str == 'undefined') {
        return "N/A";
    } else {
        return str;
    }
}

/* ----------------------------------------------------------------
 * Function Name    : validateEmail
 * Purpose          : to validate email 
 * Added by         : Edi Suryadi
 * Create Date      : September 12th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

/* ----------------------------------------------------------------
 * Function Name    : ActionDetailHeaderButtons
 * Purpose          : to turn off action in detail header button 
 * Added by         : Edi Suryadi
 * Create Date      : September 17th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function ActionDetailHeaderButtons(IDtoTurnOFF, mobileno, simType) {
    //debugger
    //18-Jul-2017 : Moorthy : Added if the simType is undefined
    if (simType == undefined) {
        simType = 'PAYG';
        if (window.location.href.toLowerCase().indexOf('/paym/') > -1) {
            simType = 'PAYM';
        }
    }
    if (IDtoTurnOFF != 1) {
        $("a#detailMoreInfo").live("click", function () {
            window.location = '/Customer/TabGeneral/' + mobileno + "-" + simType;
        });
    }
    if (IDtoTurnOFF != 2) {
        $("a#detailOrder").live("click", function () {
            window.location = '/Customer/OrderManagement/' + mobileno + "-" + simType;
        });
    }
    if (IDtoTurnOFF != 2) {
        $("a#detailPAYMOrder").live("click", function () {
            window.location = '/PAYM/OrderManagement/' + mobileno + "-" + simType;
        });
    }
    if (IDtoTurnOFF != 3) {
        $("a#detailProduct").live("click", function () {
            window.location = '/Customer/TabBundles/' + mobileno + "-" + simType;
        });
    }
    if (IDtoTurnOFF != 4) {
        $("a#detailCallHistory").live("click", function () {
            window.location = '/Customer/TabCallHistory_v1/' + mobileno + "-" + simType;
        });
    }
    if (IDtoTurnOFF != 5) {
        $("a#detailPaymentTopupHistory").live("click", function () {
            window.location = '/Customer/TabTopupHistory/' + mobileno + "-" + simType;
        });
    }
    if (IDtoTurnOFF != 6) {
        $("a#detailSIMTools").live("click", function () {
            window.location = '/Customer/TabSIMSwap/' + mobileno + "-" + simType;
        });
    }
    if (IDtoTurnOFF != 7) {
        $("a#detailIssuesTracking").live("click", function () {
            window.location = '/Customer/TabIssuesTracking/' + mobileno + "-" + simType;
        });
    }
    // 06-Aug-2015 : Moorthy Added For Paym Tabs
    if (IDtoTurnOFF != 4) {
        $("a#detailPAYMCallHistory").live("click", function () {
            window.location = '/PAYM/TabCallHistory/' + mobileno + "-" + simType;
        });
    }
    if (IDtoTurnOFF != 7) {
        $("a#detailPAYMIssuesTracking").live("click", function () {
            window.location = '/PAYM/TabIssuesTracking/' + mobileno + "-" + simType;
        });
    }
    if (IDtoTurnOFF != 8) {
        $("a#detailServiceStatusHistory").live("click", function () {
            window.location = '/PAYM/TabServiceStatusHistory/' + mobileno + "-" + simType;
        });
    }
    if (IDtoTurnOFF != 8) {
        $("a#detailPAYMBills").live("click", function () {
            window.location = '/PAYM/TabBills/' + mobileno + "-" + simType;
        });
    }
    if (IDtoTurnOFF != 9) {
        $("a#detailPAYMPayment").live("click", function () {
            window.location = '/PAYM/TabPaymentHistroy/' + mobileno + "-" + simType;
        });
    }
    if (IDtoTurnOFF != 10) {
        $("a#detailPAYMSIMTools").live("click", function () {
            window.location = '/PAYM/TabSIMSwap/' + mobileno + "-" + simType;
        });
    }

    //Customer Overview Page
    if (IDtoTurnOFF != 11) {
        $("a#detailOverview").live("click", function () {
            window.location = '/Customer/TabCOverview/' + mobileno + "-" + simType;
        });
    }

    //Vectone Xtra App
    if (IDtoTurnOFF != 12) {
        $("a#detailVectoneXtraApp").live("click", function () {
            window.location = '/Customer/TabVectoneXtraApp/' + mobileno + "-" + simType;
        });
    }

    //Rajesh: Porting
    if (IDtoTurnOFF != 13) {
        $("a#detailPAYMPorting").live("click", function () {
            window.location = '/PAYM/TabPorting/' + mobileno + "-" + simType;
        });
    }
    if (IDtoTurnOFF != 13) {
        $("a#detailPorting").live("click", function () {
            window.location = '/Customer/TabPorting/' + mobileno + "-" + simType;
        });
    }
    //26-Dec-2018 : Moorthy : Addded for SMS module
    if (IDtoTurnOFF != 14) {
        $("a#detailSMSCampaignTracking").live("click", function () {
            window.location = '/PAYM/TabSMSCampaignTracking/' + mobileno + "-" + simType;
        });
    }
    if (IDtoTurnOFF != 14) {
        $("a#detailCustSMSCampaignTracking").live("click", function () {
            window.location = '/Customer/TabSMSCampaignTracking/' + mobileno + "-" + simType;
        });
    }
    if (IDtoTurnOFF != 15) {
        $("a#detailPAYMComplaints").live("click", function () {
            window.location = '/PAYM/TabComplaints/' + mobileno + "-" + simType;
        });
    }
    if (IDtoTurnOFF != 15) {
        $("a#detailComplaints").live("click", function () {
            window.location = '/Customer/TabComplaints/' + mobileno + "-" + simType;
        });
    }
    // 06-Aug-2015 : Moorthy Commented : When clicking the TabSMSCampaignTracking the url changed to 'PayM' from 'Çustomer'
    /*Added by Hari - 14 Feb 2014*/
    //if (IDtoTurnOFF != 11) {
    //    /* TAB SMSCampaignTracking"  */
    //    $("a#detailSMSCampaignTracking").live("click", function () {
    //        //05-Aug-2015 : Moorthy Commented
    //        //alert('test2');
    //        window.location = '/PAYM/TabSMSCampaignTracking/' + mobileno;
    //    });
    //}
    /*End - added by Hari*/
}


/* ----------------------------------------------------------------
 * Function Name    : ActivateTopbarMenu
 * Purpose          : to activate menu on topbar 
 * Added by         : Edi Suryadi
 * Create Date      : September 24th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function ActivateTopbarMenu(IDtoTurnOFF) {

    $("ul#topBarMenu li.swidnav").removeClass("active");

    if (IDtoTurnOFF == 1) {
        $("ul#topBarMenu li#MenuHome").addClass("active");
    }

    if (IDtoTurnOFF == 2) {
        $("ul#topBarMenu li#MenuCustomer").addClass("active");
    }

    if (IDtoTurnOFF == 3) {
        $("ul#topBarMenu li#MenuSIMOrder").addClass("active");
    }

    if (IDtoTurnOFF == 4) {
        $("ul#topBarMenu li#MenuVoucher").addClass("active");
    }

    if (IDtoTurnOFF == 5) {
        $("ul#topBarMenu li#MenuPortIN").addClass("active");
    }

    if (IDtoTurnOFF == 6) {
        $("ul#topBarMenu li#MenuPayment").addClass("active");
    }
    if (IDtoTurnOFF == 7) {
        $("ul#topBarMenu li#MenuHome").addClass("active");
    }
    if (IDtoTurnOFF == 8) {
        $("ul#topBarMenu li#MenuFinance").addClass("active");
    }
    if (IDtoTurnOFF == 9) {
        $("ul#topBarMenu li#MenuUsers").addClass("active");
    }
    //Queues
    if (IDtoTurnOFF == 10) {
        $("ul#topBarMenu li#MenuQueue").addClass("active");
    }
    if (IDtoTurnOFF == 11) {
        $("ul#topBarMenu li#MenuLLOTG").addClass("active");
    }
    //31-Jan-2019 : CRM Enhancement 2
    if (IDtoTurnOFF == 12) {
        $("ul#topBarMenu li#MenuCSreports").addClass("active");
    }

    //13-Aug-2019 : Doc Repository
    if (IDtoTurnOFF == 13) {
        $("ul#topBarMenu li#MenuDocRepository").addClass("active");
    }
}

/* ----------------------------------------------------------------
 * Function Name    : AddNowDateWithFiveWorkingDays
 * Purpose          : to now date with 5 working days
 * Added by         : Edi Suryadi
 * Create Date      : October 07th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function AddNowDateWithFiveWorkingDays() {
    var nowDate = new Date();
    var dateAfterAdded = new Date();
    var nowDay = nowDate.getDay();

    var dayAdded = 0;
    switch (nowDay) {
        case 0:
            dayAdded = 5;
            break;
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
            dayAdded = 7;
            break;
        case 6:
            dayAdded = 6;
            break;
    }
    dateAfterAdded.setDate(nowDate.getDate() + dayAdded);
    return dateAfterAdded;
}

/* ----------------------------------------------------------------
 * Function Name    : ReplaceForFindAddress
 * Purpose          : 
 * Added by         : Edi Suryadi
 * Create Date      : October 16th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function ReplaceForFindAddress(str) {
    if (!str || typeof str != 'string')
        return null;
    return str.replace(/^[\s]+/, '').replace(/[\s]+$/, '').replace(/[\s]{2,}/, ' ');
}

/* ----------------------------------------------------------------
 * Function Name    : isValidDate
 * Purpose          : to validate date
 * Added by         : Ed Nutting
 *                    hxxp://www.codeproject.com/Tips/144113/JavaScript-Date-Validation
 * Create Date      : November 29th, 2012
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
//Value parameter - required. All other parameters are optional.
function isValidDate(value, sepVal, dayIdx, monthIdx, yearIdx) {
    try {
        //Change the below values to determine which format of date you wish to check. It is set to dd/mm/yyyy by default.
        var DayIndex = dayIdx !== undefined ? dayIdx : 0;
        var MonthIndex = monthIdx !== undefined ? monthIdx : 1;
        var YearIndex = yearIdx !== undefined ? yearIdx : 2;
        value = value.replace(/-/g, "/").replace(/\./g, "/");
        var SplitValue = value.split(sepVal || "/");
        var OK = true;
        if (!(SplitValue[DayIndex].length == 1 || SplitValue[DayIndex].length == 2)) {
            OK = false;
        }
        if (OK && !(SplitValue[MonthIndex].length == 1 || SplitValue[MonthIndex].length == 2)) {
            OK = false;
        }
        if (OK && SplitValue[YearIndex].length != 4) {
            OK = false;
        }
        if (OK) {
            var Day = parseInt(SplitValue[DayIndex], 10);
            var Month = parseInt(SplitValue[MonthIndex], 10);
            var Year = parseInt(SplitValue[YearIndex], 10);

            if (OK = ((Year > 1900) && (Year < new Date().getFullYear()))) {
                if (OK = (Month <= 12 && Month > 0)) {

                    var LeapYear = (((Year % 4) == 0) && ((Year % 100) != 0) || ((Year % 400) == 0));

                    if (OK = Day > 0) {
                        if (Month == 2) {
                            OK = LeapYear ? Day <= 29 : Day <= 28;
                        }
                        else {
                            if ((Month == 4) || (Month == 6) || (Month == 9) || (Month == 11)) {
                                OK = Day <= 30;
                            }
                            else {
                                OK = Day <= 31;
                            }
                        }
                    }
                }
            }
        }
        return OK;
    }
    catch (e) {
        return false;
    }
}

/* ----------------------------------------------------------------
 * Function Name    : convertNulltoNA
 * Purpose          : to convert null to N/A 
 * Added by         : Edi Suryadi
 * Create Date      : November 06th, 2013
 * Last Update      : January 30th, 2014
 * Update History   : - January 30th, 2014
                        Add some currencies
 * ---------------------------------------------------------------- */
function getCurrencySymbol(str) {
    var CurrencySymbol = '';
    if (convertNulltoEmptyString(str) != '' && convertNulltoEmptyString(str) != 'undefined') {
        switch (str) {
            case 'USD':
            case 'ARS':
            case 'AUD':
            case 'BSD':
            case 'BBD':
            case 'BMD':
            case 'BND':
            case 'CAD':
            case 'KYD':
            case 'CLP':
            case 'COP':
            case 'XCD':
            case 'SVC':
            case 'FJD':
            case 'GYD':
            case 'LRD':
            case 'MXN':
            case 'NAD':
            case 'NZD':
            case 'SGD':
            case 'SBD':
            case 'SRD':
            case 'TVD':
                CurrencySymbol = '&#36;';
                break;
            case 'ALL':
                CurrencySymbol = 'Lek';
                break;
            case 'AFN':
                CurrencySymbol = '&#1547;';
                break;
            case 'AWG':
                CurrencySymbol = '&#402;';
                break;
            case 'AZN':
                CurrencySymbol = 'MaH';
                break;
            case 'BYR':
                CurrencySymbol = 'p.';
                break;
            case 'BZD':
                CurrencySymbol = 'BZ&#36;';
                break;
            case 'BOB':
                CurrencySymbol = '&#36;b';
                break;
            case 'BAM':
                CurrencySymbol = 'KM';
                break;
            case 'BWP':
                CurrencySymbol = 'P';
                break;
            case 'BGN':
                CurrencySymbol = '&Pi;B';
                break;
            case 'BRL':
                CurrencySymbol = 'R&#36;';
                break;
            case 'DOP':
                CurrencySymbol = 'RD&#36;';
                break;
            case 'CZK':
                CurrencySymbol = 'Kč';
                break;
            case 'CUP':
                CurrencySymbol = '₱';
                break;
            case 'HRK':
                CurrencySymbol = 'kn';
                break;
            case 'CRC':
                CurrencySymbol = '₡';
                break;
            case 'CNY':
            case 'HKD':
                CurrencySymbol = '元';
                break;
            case 'LKR':
            case 'INR':
            case 'SCR':
                CurrencySymbol = 'Rs';
                break;
            case 'KHR':
                CurrencySymbol = 'KHR';
                break;
            case 'EGP':
            case 'GBP':
            case 'FKP':
            case 'GIP':
            case 'GGP':
            case 'IMP':
            case 'JEP':
            case 'LBP':
            case 'SYP':
            case 'TRL':
                CurrencySymbol = '&pound;';
                break;
            case 'EUR':
                CurrencySymbol = '&euro;';
                break;
            case 'EEK':
            case 'SEK':
            case 'DKK':
                CurrencySymbol = 'kr';
                break;
            default:
                CurrencySymbol = str;

        }
    }
    return CurrencySymbol;
}


/* ----------------------------------------------------------------
 * Function Name    : convertDateISOCustom
 * Purpose          : to convert date from 2013-10-21T09:25:13.73 into 21-10-2013 09:25
 * Added by         : Harry Ramdhani
 * Create Date      : October 30th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function convertDateISOCustom(inputDate, type) {
    if (convertNulltoEmptyString(inputDate) != "") {
        var splitDate = inputDate.split('T');
        var splitDateLeft = splitDate[0].split('-');
        var aDate = splitDateLeft[2] + "-" + splitDateLeft[1] + "-" + splitDateLeft[0];
        var aTime = splitDate[1].substr(0, 8);
        var result = "";
        if (type == 0) {
            result = aDate + " " + aTime;
        } else if (type == 1) {
            result = aDate;
        }
        return result;
    } else {
        return "";
    }
}

/* ----------------------------------------------------------------
 * Function Name    : inArray
 * Purpose          : to search data in an array
 * Added by         : Edi Suryadi
 * Create Date      : January 07th, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function inArray(needle, haystack) {
    var length = haystack.length;
    for (var i = 0; i < length; i++) {
        if (haystack[i].toLowerCase() == needle.toLowerCase()) return true;
    }
    return false;
}


