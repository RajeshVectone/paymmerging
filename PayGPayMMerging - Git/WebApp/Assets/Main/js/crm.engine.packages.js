﻿/* ===================================================== 
   *  TAB PACKAGES 
   ----------------------------------------------------- */

/* ----------------------------------------------------------------
 * Function Name    : ViewPackages
 * Purpose          : to view packages
 * Added by         : Edi Suryadi
 * Create Date      : September 06th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function ViewPackages(Msisdn, sitecode) {

    $("#tblProductPackagesContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblProductPackages\"></table>");

    var url = apiServer + '/api/package';
    jQuery.support.cors = true;
    var JSONSendData = {
        Msisdn: Msisdn,
        package_type: 1,
        sitecode: sitecode,
        status: ''
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#TabProductPackagesLoader").hide();
            $("#tblProductPackagesContainer").show();
            $('#tblProductPackages').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                iDisplayLength: 10,
                bPaginate: true,
                bFilter: false,
                bInfo: false,
                // aaSorting: [[0, "asc"]],
                aaSorting: [],
                aoColumns: [
                    { mDataProp: "packageid", sTitle: "Package ID" },
                    { mDataProp: "packagename", sTitle: "Package Name" },
                    { mDataProp: "balance", sTitle: "Balance" },
                    { mDataProp: "startdate_string", sTitle: "Start Date" },
                    { mDataProp: "expdate_string", sTitle: "Expired Date" },
                    { mDataProp: "statusname", sTitle: "Status" },
                    { mDataProp: "packageTypeName", sTitle: "Package Type" },
                    {
                        mDataProp: "comment", sTitle: "Comment",
                        fnRender: function (ob) {
                            if (ob.aData.comment != '') {
                                return ob.aData.comment;
                            } else {
                                return "No Info";
                            }
                        }
                    },
                    {
                        mDataProp: "package_type", sTitle: "Action",
                        fnRender: function (ob) {
                            var PackId = ob.aData.tariffClass;
                            var Start_date = ob.aData.startdate_str;
                            var End_date = ob.aData.expdate_str;
                            var PackageType = ob.aData.packageTypeName;
                            var Mobileno = Msisdn;
                            var Sitecode = sitecode;

                            if (PackageType == "Call") {
                                var historyType = 1;
                            }

                            else if (PackageType == "SMS") {
                                var historyType = 2;
                            }

                            else if (PackageType == "Internet") {
                                var historyType = 3;
                            }
                            else if (PackageType == "Call and SMS") {
                                var historyType = 5;
                            }
                            else {
                                var historyType = 1;
                            }
                            var Parameter = "\"" + Mobileno + "\",\"" + Sitecode + "\",\"" + Start_date + "\",\"" + End_date + "\",\"" + PackId + "\",\"" + historyType + "\"";
                            // alert(Parameter);
                            var actionURL = "<a class='btn blue btnPPCDR' onclick='getPopUpCDR(" + Parameter + ");' ctlValue='" + Parameter + "'> CDR</a>";
                            //alert(actionURL);
                            //var actionURL = "<a class='btn blue btnPPCDR' onclick='getPopUpCDR();'> CDR</a>";
                            return actionURL;
                        }
                    }
                ]
            });
            $("#tblProductPackages_wrapper .row-fluid:eq(0)").remove();
        },
        error: function (feedback) {
            $("#TabProductPackagesLoader").hide();
            $("#tblProductPackagesContainer").html("").append("No package found").show();
        }
    });
}

//Added by karthik Jira:CRMT-198
/* ----------------------------------------------------------------
 * Function Name    : getPopUpCDR
 * Purpose          : Pop up show data
 * Added by         : karthik
 * Create Date      : December 27th, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function getPopUpCDR(Mobileno, Sitecode, Start_date, End_date, PackId, historyType) {
    //CDR History Download
    $("#hdCDRHistory").val(Mobileno + "," + Sitecode + "," + Start_date + "," + End_date + "," + PackId + "," + historyType);
    var url = apiServer + '/api/cdr_v1/';
    jQuery.support.cors = true;
    var JSONSendData = {

        mobileno: Mobileno,
        HistoryType: historyType,
        Sitecode: Sitecode,
        Date_From: Start_date,
        Date_To: End_date,
        Packageid: PackId

    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#TabProductCDRLoader").hide();
            $("#chFilterResultBody").css({ "background-image": "none" });
            $('#tblCHFilterResult').dataTable({

                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: false,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No  history found."
                },
                aoColumns: [
                    { mDataProp: "History_Type", sTitle: "History Type" },
                    { mDataProp: "dateTime_String", sTitle: "Call & Time" },
                    { mDataProp: "CLI", sTitle: "CLI" },
                    { mDataProp: "Destination_Number", sTitle: "Destination Number" },
                    { mDataProp: "Type", sTitle: "Type" },
                    { mDataProp: "Destination_Code", sTitle: "Destination Code" },
                     {
                         mDataProp: "DurationMin_string", sTitle: "Duration (Min:sec)",
                         fnRender: function (ob) {
                             return ob.aData.DurationMin_string + " Min";
                         }
                     },
                    {
                        mDataProp: "DataUsage_String", sTitle: "Data Usage(in MB)",
                        fnRender: function (ob) {
                            return ob.aData.DataUsage_String + " MB";
                        }
                    },
                    { mDataProp: "Tariff_Class", sTitle: "Tariff Class" },
                    { mDataProp: "Roaming_Zone", sTitle: "Roaming Zone" },
                    { mDataProp: "Net_Charges", sTitle: "Net Charges" },
                    { mDataProp: "Balance_String", sTitle: "Balance" }
                ]
            });
            $("#tblCHFilterResult_wrapper .row-fluid:eq(0)").remove();
            $("#tblCHFilterResult_wrapper .row-fluid .span6:eq(0)").addClass("span4");
            $("#tblCHFilterResult_wrapper .row-fluid .span6:eq(1)").addClass("span8");
            $("#tblCHFilterResult_wrapper .row-fluid .span6").removeClass("span6");

        },
        error: function (feedback) {
            $("#TabProductCDRLoader").hide();
            $("#chFilterResultBody").css({ "background-image": "none" }).html("").append("No  history found.").show();
        }
    });
}
/* ----------------------------------------------------- 
   *  eof TAB PACKAGES 
   ===================================================== */


//CDR History Download
function downloadcdrhistory(mobileno, siteCode, startDate, endDate, packageId, historyType) {
    var i = 0;
    jQuery.support.cors = true;
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader1").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    $("#ajax-screen-masking #downloadIFrame").remove();
    var url = '/Download/DownloadCDRHistory';
    var JSONSendData = {
        mobileno: mobileno,
        siteCode: siteCode,
        startDate: startDate,
        endDate: endDate,
        packageId: packageId,
        historyType: historyType
    };
    $.ajax
    ({
        url: url,
        type: 'get',
        dataType: 'json',
        data: JSONSendData,
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {

            if (feedback.errcode == 0) {
                $("#ajax-screen-masking").hide();
                var IFrameData = $('<iframe/>', { id: 'downloadIFrame', src: '/Download/DownloadCDRHistoryFile?id=' + feedback.Text, style: 'display:none' });
                $("#ajax-screen-masking").append(IFrameData);
            }
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}