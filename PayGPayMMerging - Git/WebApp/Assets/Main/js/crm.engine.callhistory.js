﻿/* ===================================================== 
   *  TAB CDR HISTORY  
   ----------------------------------------------------- */

$(document).ready(function () {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    if (dd < 10) { dd = '0' + dd } if (mm < 10) { mm = '0' + mm } today = dd + '/' + mm + '/' + yyyy;

    var lastdate1 = $('#lastdate1').val();
    if (lastdate1 == '') {
        $('#dp1').attr('data-date', today);
        $('#dp1').datepicker().on('changeDate', function (e) {
            $('div.datepicker').hide();
        });
        $('#startdate').attr('value', today);
        $('#lastdate1').val(today);
    }
    else {
        $('#dp1').attr('data-date', lastdate1);
        $('#dp1').datepicker().on('changeDate', function (e) {
            $('div.datepicker').hide();
        });
        $('#startdate').attr('value', lastdate1);
    }

    var lastdate2 = $('#lastdate2').val();
    if (lastdate2 == '') {
        $('#dp2').attr('data-date', today);
        $('#dp2').datepicker().on('changeDate', function (e) {
            $('div.datepicker').hide();
        });
        $('#enddate').attr('value', today);
        $('#lastdate2').val(today);
    }
    else {
        $('#dp2').attr('data-date', lastdate2);
        $('#dp2').datepicker().on('changeDate', function (e) {
            $('div.datepicker').hide();
        });
        $('#enddate').attr('value', lastdate2);
    }

    $('#OrderListSearchBtn').click(function () {
        var orderType = $("select#orderType").val();
        var fromDate = $.trim($('#startdate').val());
        var toDate = $.trim($('#enddate').val());
        orderList(orderType, fromDate, toDate);
    });

});

/* ----------------------------------------------------------------
 * Function Name    : AllHistory
 * Purpose          : to show history of all
 * Added by         : Edi Suryadi
 * Create Date      : January 5th, 2016
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function AllHistory(mobileno, Iccid, Sitecode, startDate, endDate, Packageid) {
    $("span#strHistoryType").html("&nbsp;of All History");
    $("#chFilterResultBody").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblCHFilterResult\"></table>");
    $("#chFilterResultBody").css({ "overflow": "auto", "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });

    var url = apiServer + '/api/cdr_v1/';
    jQuery.support.cors = true;
    var JSONSendData = {
        mobileno: mobileno,
        Iccid: Iccid,
        HistoryType: 0,
        Sitecode: Sitecode,
        Date_From: startDate,
        Date_To: endDate,
        //modif by karthik Jira CRMT-198
        Packageid: Packageid
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#chFilterResultBody").css({ "background-image": "none" });
            var oTable = $('#tblCHFilterResult').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: false,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No  history found."
                },
                aoColumns: [
                    { mDataProp: "History_Type", sTitle: "History Type" },

                    //{ mDataProp: "dateTime_String", sTitle: "Call & Time" },

                    {
                        mDataProp: "Date_Time", sTitle: "Call & Time", "bUseRendered": false,
                        fnRender: function (ob) {
                            return convertDateISOCustom(ob.aData.Date_Time, 0);
                        }
                    },


                    { mDataProp: "CLI", sTitle: "CLI" },
                    { mDataProp: "Destination_Number", sTitle: "Destination Number" },
                    { mDataProp: "Package_Name", sTitle: "Package Name" },
                    { mDataProp: "Type", sTitle: "Type" },
                    { mDataProp: "Destination_Code", sTitle: "Destination Code" },
                    {
                        mDataProp: "DurationMin_string", sTitle: "Duration (Min:sec)",
                        fnRender: function (ob) {
                            return ob.aData.DurationMin_string + " Min";
                        }
                    },
                    {
                        mDataProp: "DataUsage_String", sTitle: "Data Usage(in MB)",
                        fnRender: function (ob) {
                            return ob.aData.DataUsage_String + " MB";
                        }
                    },
                    { mDataProp: "Tariff_Class", sTitle: "Tariff Class" },
                    { mDataProp: "Roaming_Zone", sTitle: "Roaming Zone" },
                    { mDataProp: "Net_Charges", sTitle: "Net Charges" },
                    { mDataProp: "Balance_String", sTitle: "Balance" },
                    {
                        mDataProp: "Status", sTitle: "Within Bundle",
                        fnRender: function (ob) {
                            if (ob.aData.Status == 1) {
                                return "Yes";
                            }
                            else {
                                return "No";
                            }
                        }
                    }
                ]
            });
            if (oTable.length > 0) {
                oTable.fnAdjustColumnSizing();
            }
            $("#tblCHFilterResult_wrapper .row-fluid:eq(0)").remove();
            $("#tblCHFilterResult_wrapper .row-fluid .span6:eq(0)").addClass("span4");
            $("#tblCHFilterResult_wrapper .row-fluid .span6:eq(1)").addClass("span8");
            $("#tblCHFilterResult_wrapper .row-fluid .span6").removeClass("span6");

            var DownloadAnchor = "<a id=\"downloadAnchor\" style=\"display:none;\"></a>";
            var btnExportHistory = "<button class=\"btn blue\" id=\"btnExportCDRHistory\" name=\"btnExportCDRHistory\">Download to Excel</button>";
            $("#tblCHFilterResult_wrapper .row-fluid .span4").append(DownloadAnchor + btnExportHistory).show();
        },
        error: function (feedback) {
            $("#chFilterResultBody").css({ "background-image": "none" }).html("").append("No  history found.").show();
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : CallHistory
 * Purpose          : to show history of Call
 * Added by         : Edi Suryadi
 * Create Date      : September 09th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function CallHistory(mobileno, Iccid, Sitecode, startDate, endDate) {
    $("span#strHistoryType").html("&nbsp;of Call History");
    $("#chFilterResultBody").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblCHFilterResult\"></table>");
    $("#chFilterResultBody").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    
    var url = apiServer + '/api/cdr_v1/';
    jQuery.support.cors = true;
    var JSONSendData = {
        mobileno: mobileno,
        Iccid: Iccid,
        HistoryType: 1,
        Sitecode: Sitecode,
        Date_From: startDate,
        Date_To: endDate
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#chFilterResultBody").css({ "background-image": "none" });
            $('#tblCHFilterResult').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: false,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No call history found."
                },
                aoColumns: [
                    { mDataProp: "CallDate", sTitle: "Call Date and Time" },
                    { mDataProp: "Ani", sTitle: "CLI" },
                    { mDataProp: "DialedNum", sTitle: "Destination Number" },
                    //{ mDataProp: "renewal_mode", sTitle: "Call Type" },
                    { mDataProp: "DestCode", sTitle: "Destination Code" },
                    { mDataProp: "TalkTime_Minutes_string", sTitle: "Talk Time in Minutes" },
                    { mDataProp: "TalkCharge", sTitle: "Charge" }
                ]
            });
            $("#tblCHFilterResult_wrapper .row-fluid:eq(0)").remove();
            $("#tblCHFilterResult_wrapper .row-fluid .span6:eq(0)").addClass("span4");
            $("#tblCHFilterResult_wrapper .row-fluid .span6:eq(1)").addClass("span8");
            $("#tblCHFilterResult_wrapper .row-fluid .span6").removeClass("span6");
            
            var DownloadAnchor = "<a id=\"downloadAnchor\" style=\"display:none;\"></a>";
            var btnExportHistory = "<button class=\"btn blue\" id=\"btnExportCDRHistory\" name=\"btnExportCDRHistory\">Download to Excel</button>";
            $("#tblCHFilterResult_wrapper .row-fluid .span4").append(DownloadAnchor + btnExportHistory).show();
        },
        error: function (feedback) {
            $("#chFilterResultBody").css({ "background-image": "none" }).html("").append("No call history found.").show();
        }
    });
}
function CallHistory3(mobileno, Sitecode) {


    $("span#strHistoryType").html("&nbsp;of Call History");
    $("#chFilterResultBody").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblCHFilterResult\"></table>");
    $("#chFilterResultBody").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });

    var url = apiServer + '/api/MobileNoReports/';
    jQuery.support.cors = true;
    var JSONSendData = {
        mobileno: mobileno,
        step: 1,
        sitecode: Sitecode,
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#chFilterResultBody").css({ "background-image": "none" });
            $('#tblCHFilterResult').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: false,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No call history found."
                },
                aoColumns: [
                    { mDataProp: "Submit_Date", sTitle: "Submit Date" },
                    { mDataProp: "Issue_Type", sTitle: "Issue_Type" },
                    { mDataProp: "Complaint", sTitle: "Complaint" },
                   // { mDataProp: "renewal_mode", sTitle: "Call Type" },
                    { mDataProp: "Function", sTitle: "Function" },
                    { mDataProp: "Total_Calls", sTitle: "Total Calls" },
                    { mDataProp: "MobileNo", sTitle: "Mobile No(s)" },


                ]
            });
            $("#tblCHFilterResult_wrapper .row-fluid:eq(0)").remove();
            $("#tblCHFilterResult_wrapper .row-fluid .span6:eq(0)").addClass("span4");
            $("#tblCHFilterResult_wrapper .row-fluid .span6:eq(1)").addClass("span8");
            $("#tblCHFilterResult_wrapper .row-fluid .span6").removeClass("span6");

            var DownloadAnchor = "<a id=\"downloadAnchor\" style=\"display:none;\"></a>";
            var btnExportHistory = "<button class=\"btn blue\" id=\"btnExportCDRHistory\" name=\"btnExportCDRHistory\">Download to Excel</button>";
            $("#tblCHFilterResult_wrapper .row-fluid .span4").append(DownloadAnchor + btnExportHistory).show();
        },
        error: function (feedback) {
            $("#chFilterResultBody").css({ "background-image": "none" }).html("").append("No call history found.").show();
        }
    });
}
function CallHistory2(startdate, enddate, Sitecode) {
    $("span#strHistoryType").html("&nbsp;of Date Range");
    $("#chFilterResultBody").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblCHFilterResult\"></table>");
    $("#chFilterResultBody").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });

    var url = apiServer + '/api/MobileNoReports/';
    jQuery.support.cors = true;
    var JSONSendData = {
        startdate: startdate,
        enddate: enddate,
        step: 2,
        sitecode: Sitecode,
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#chFilterResultBody").css({ "background-image": "none" });
            $('#tblCHFilterResult').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: false,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No call history found."
                },
                aoColumns: [
                    { mDataProp: "Submit_Date", sTitle: "Submit Date", sType: "date-euro" },
                    { mDataProp: "Issue_Type", sTitle: "Issue_Type" },
                    { mDataProp: "Complaint", sTitle: "Complaint" },
                    //{ mDataProp: "renewal_mode", sTitle: "Call Type" },
                    { mDataProp: "Function", sTitle: "Function" },
                   { mDataProp: "Total_Calls", sTitle: "Total_Calls" },
                   { mDataProp: "MobileNo", sTitle: "Mobile No" },

                ]
            });
            $("#tblCHFilterResult_wrapper .row-fluid:eq(0)").remove();
            $("#tblCHFilterResult_wrapper .row-fluid .span6:eq(0)").addClass("span4");
            $("#tblCHFilterResult_wrapper .row-fluid .span6:eq(1)").addClass("span8");
            $("#tblCHFilterResult_wrapper .row-fluid .span6").removeClass("span6");

            var DownloadAnchor = "<a id=\"downloadAnchor\" style=\"display:none;\"></a>";
            var btnExportHistory = "<button class=\"btn blue\" id=\"btnExportCDRHistory\" name=\"btnExportCDRHistory\">Download to Excel</button>";
            $("#tblCHFilterResult_wrapper .row-fluid .span4").append(DownloadAnchor + btnExportHistory).show();
        },
        error: function (feedback) {
            $("#chFilterResultBody").css({ "background-image": "none" }).html("").append("No call history found.").show();
        }
    });
}
function CallHistory1(mobileno, Sitecode) {
    $("span#strHistoryType").html("&nbsp;of Customer Mobile Number");
    $("#chFilterResultBody").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblCHFilterResult\"></table>");
    $("#chFilterResultBody").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });

    var url = apiServer + '/api/MobileNoReports/';
    jQuery.support.cors = true;
    var JSONSendData = {
        step: 1,
        mobileno: mobileno,
        sitecode: Sitecode
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#chFilterResultBody").css({ "background-image": "none" });
            $('#tblCHFilterResult').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: false,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [],
              
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No call history found."
                },
           

               

                aoColumns: [
                    { mDataProp: "Submit_Date", sTitle: "Submit Date", sType: "date-euro" },
                    { mDataProp: "Issue_Type", sTitle: "Issue_Type" },
                    { mDataProp: "Complaint", sTitle: "Complaint" },
                    //{ mDataProp: "renewal_mode", sTitle: "Call Type" },
                    { mDataProp: "Function", sTitle: "Function" },
                     { mDataProp: "Total_Calls", sTitle: "Total_Calls" },
                     { mDataProp: "MobileNo", sTitle: "MobileNo" },

                ]
          
            });
            $("#tblCHFilterResult_wrapper .row-fluid:eq(0)").remove();
            $("#tblCHFilterResult_wrapper .row-fluid .span6:eq(0)").addClass("span4");
            $("#tblCHFilterResult_wrapper .row-fluid .span6:eq(1)").addClass("span8");
            $("#tblCHFilterResult_wrapper .row-fluid .span6").removeClass("span6");

            var DownloadAnchor = "<a id=\"downloadAnchor\"></a>";
            var btnExportHistory = "<button class=\"btn blue\" id=\"btnExportCDRHistory\" name=\"btnExportCDRHistory\">Download to Excel</button>";
            $("#tblCHFilterResult_wrapper .row-fluid .span4").append(DownloadAnchor + btnExportHistory).show();
        },
        error: function (feedback) {
            $("#chFilterResultBody").css({ "background-image": "none" }).html("").append("No call history found.").show();
        }
    });
}
/* ----------------------------------------------------------------
 * Function Name    : TextHistory
 * Purpose          : to show history of Text/SMS
 * Added by         : Edi Suryadi
 * Create Date      : September 09th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function TextHistory(mobileno, Iccid, Sitecode, startDate, endDate) {
    $("span#strHistoryType").html("&nbsp;of Text History");
    $("#chFilterResultBody").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblCHFilterResult\"></table>");
    $("#chFilterResultBody").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });

    var url = apiServer + '/api/cdr_v1/';
    jQuery.support.cors = true;
    var JSONSendData = {
        mobileno: mobileno,
        Iccid: Iccid,
        HistoryType: 2,
        Sitecode: Sitecode,
        Date_From: startDate,
        Date_To: endDate
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#chFilterResultBody").css({ "background-image": "none" });
            $('#tblCHFilterResult').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: false,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No text history found."
                },
                aoColumns: [
                    { mDataProp: "Cnxdate_String", sTitle: "Date and Time" },
                    { mDataProp: "Cli", sTitle: "CLI" },
                    { mDataProp: "DialedNum", sTitle: "Text to" },
                    //{ mDataProp: "errcode", sTitle: "Type" },
                    { mDataProp: "DestCode", sTitle: "Destination Code" },
                    { mDataProp: "TalkCharge_string", sTitle: "Charge" },
                    { mDataProp: "Balance", sTitle: "Balance" }
                ]
            });
            $("#tblCHFilterResult_wrapper .row-fluid:eq(0)").remove();
            $("#tblCHFilterResult_wrapper .row-fluid .span6:eq(0)").addClass("span4");
            $("#tblCHFilterResult_wrapper .row-fluid .span6:eq(1)").addClass("span8");
            $("#tblCHFilterResult_wrapper .row-fluid .span6").removeClass("span6");
            
            var DownloadAnchor = "<a id=\"downloadAnchor\" style=\"display:none;\"></a>";
            var btnExportHistory = "<button class=\"btn blue\" id=\"btnExportCDRHistory\" name=\"btnExportCDRHistory\">Download to Excel</button>";
            $("#tblCHFilterResult_wrapper .row-fluid .span4").append(DownloadAnchor + btnExportHistory).show();
        },
        error: function (feedback) {
            $("#chFilterResultBody").css({ "background-image": "none" }).html("").append("No text history found.").show();
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : DataHistory
 * Purpose          : to show history of Data/GPRS
 * Added by         : Edi Suryadi
 * Create Date      : September 09th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function DataHistory(mobileno, Iccid, Sitecode, startDate, endDate) {
    $("span#strHistoryType").html("&nbsp;of Data History");
    $("#chFilterResultBody").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblCHFilterResult\"></table>");
    $("#chFilterResultBody").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });

    var url = apiServer + '/api/cdr_v1/';
    jQuery.support.cors = true;
    var JSONSendData = {
        mobileno: mobileno,
        Iccid: Iccid,
        HistoryType: 3,
        Sitecode: Sitecode,
        Date_From: startDate,
        Date_To: endDate
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#chFilterResultBody").css({ "background-image": "none" });
            $('#tblCHFilterResult').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: false,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No data history found."
                },
                aoColumns: [
                    { mDataProp: "CnxDate", sTitle: "CNX Date" },
                    { mDataProp: "PackageID", sTitle: "Package ID" },
                    { mDataProp: "GprsAllocation", sTitle: "GPRS Allocation in bytes" },
                    { mDataProp: "GprsCharge", sTitle: "GPRS Charge" },
                    { mDataProp: "TariffClass", sTitle: "Tariff Class" },
                    { mDataProp: "RoamingZone", sTitle: "Roaming Zone" }
                ]
            });
            $("#tblCHFilterResult_wrapper .row-fluid:eq(0)").remove();
            $("#tblCHFilterResult_wrapper .row-fluid .span6:eq(0)").addClass("span4");
            $("#tblCHFilterResult_wrapper .row-fluid .span6:eq(1)").addClass("span8");
            $("#tblCHFilterResult_wrapper .row-fluid .span6").removeClass("span6");

            var DownloadAnchor = "<a id=\"downloadAnchor\" style=\"display:none;\"></a>";
            var btnExportHistory = "<button class=\"btn blue\" id=\"btnExportCDRHistory\" name=\"btnExportCDRHistory\">Download to Excel</button>";
            $("#tblCHFilterResult_wrapper .row-fluid .span4").append(DownloadAnchor + btnExportHistory).show();
        },
        error: function (feedback) {
            $("#chFilterResultBody").css({ "background-image": "none" }).html("").append("No data history found.").show();
        }
    });
}

/* ----------------------------------------------------- 
   *  eof TAB CDR HISTORY 
   ===================================================== */


