﻿/* ===================================================== 
   *  TAB TAB AUTO TOPUP   
   ----------------------------------------------------- */

/* ----------------------------------------------------------------
 * Function Name    : CancelAutoTopup
 * Purpose          : to cancel auto topup
 * Added by         : Edi Suryadi
 * Create Date      : September 12th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function CancelAutoTopup(p1, p2, p3, p4) {
    var url = apiServer + '/api/topup';

    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    jQuery.support.cors = true;
    var JSONSendData = {
        Msisdn: p1,
        Sitecode: p2,
        Iccid: p3,
        submit_by: p4,
        SearchType: 3,
        Product_Code: $('.clsproductcode')[0].innerHTML
    };

    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();

            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });

            var stat = data.errcode;
            if (stat == 0) {
                $('.PMSuccess .modal-header').html(data.errsubject);
                $('.PMSuccess .modal-body').html(data.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            } else {
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (data) {
            $("#ajax-screen-masking").hide();
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            $('.PMFailed .modal-header').html(data.errsubject);
            $('.PMFailed .modal-body').html(data.errmsg);
            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : InfoAutoTopup
 * Purpose          : to view auto topup information
 * Added by         : Edi Suryadi
 * Create Date      : September 12th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function InfoAutoTopup(p1, p2, p3) {
    var url = apiServer + '/api/topup';
    jQuery.support.cors = true;
    var JSONSendData = {
        Msisdn: p1,
        Sitecode: p2,
        Iccid: p3,
        SearchType: 2,
        Product_Code: $('.clsproductcode')[0].innerHTML
    };

    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#TabGeneralLoader").show();
            $("#phAutoTopupContainer").hide();
        },
        success: function (feedback) {
            $("#TabGeneralLoader").hide();
            //20-Feb-2019 : Moorthy : Modifed for New Cancel Topup
            //var stat = feedback.errcode;
            if (feedback.errcode == 0) {
                //var message = feedback.message;
                ////if(message.
                //if (message.length <= 8) { }
                //else if (message.length >= 9) {
                //    message = message.substring(0, 6) + "<br/><br/><b>" + message.substring(12, 21) + "  :  </b>" + message.substring(24, message.length);
                //}
                //$("span#phAutoTopupStatus").html(message);
                //if (feedback.status < 0) {
                //    $("button#btnPHAutoTopupCancel").attr("disabled", true).removeClass("blue").addClass("grey");
                //} else if (feedback.status == 0) {
                //    $("button#btnPHAutoTopupCancel").attr("disabled", true).removeClass("blue").addClass("grey");
                //} else {
                //    $("button#btnPHAutoTopupCancel").removeAttr("disabled").removeClass("grey").addClass("blue");
                //}

                $("span#phAutoTopupStatus").html(feedback.autotopup_status);
                $("span#phAutoTopupCardDetailCardNo").html(feedback.cardnumber);
                $("span#phAutoTopupRecentAutotopupDate").html(feedback.recent_autotopup_date);
                $("span#phAutoTopupCancelDate").html(feedback.aut_cancel_date);
                $("span#phAutoTopupCancelMethod").html(feedback.aut_cancel_by);
                //if (feedback.aut_cancel_date != null && feedback.aut_cancel_by != '') {
                //    $("span#phAutoTopupStatus").html(feedback.autotopup_status);
                //    $("span#phAutoTopupCardDetailCardNo").html(feedback.cardnumber);
                //    $("span#phAutoTopupRecentAutotopupDate").html(feedback.recent_autotopup_date);
                //    $("span#phAutoTopupCancelDate").html(feedback.aut_cancel_date);
                //    $("span#phAutoTopupCancelMethod").html(feedback.aut_cancel_by);
                //}
                //else {
                //    $("span#phAutoTopupStatus").html("-");
                //    $("span#phAutoTopupCardDetailCardNo").html('');
                //    $("span#phAutoTopupRecentAutotopupDate").html('');
                //    $("span#phAutoTopupCancelDate").html('');
                //    $("span#phAutoTopupCancelMethod").html('');
                //}

                if (feedback.autotopup_status.toLowerCase().startsWith("enable")) {
                    $("button#btnPHAutoTopupCancel").removeAttr("disabled").removeClass("grey").addClass("blue"); 
                } else {
                    $("button#btnPHAutoTopupCancel").attr("disabled", true).removeClass("blue").addClass("grey");
                }

            } else {
                $("span#phAutoTopupStatus").html("-");
                $("span#phAutoTopupCardDetailCardNo").html("-");
                $("span#phAutoTopupRecentAutotopupDate").html("-");
                $("span#phAutoTopupCancelDate").html("-");
                $("span#phAutoTopupCancelMethod").html("-");
                $("button#btnPHAutoTopupCancel").attr("disabled", true).removeClass("blue").addClass("grey");
            }
            //if (feedback._Last6digitsCC != '') {
            //    $("span#phAutoTopupCardDetailCardNo").html('********' + feedback._Last6digitsCC);
            //} else {
            //    $("span#phAutoTopupCardDetailCardNo").html("-");
            //}
            $("#phAutoTopupContainer").show();
        },
        error: function (feedback) {
            $("#TabGeneralLoader").hide();
            $("#phAutoTopupContainer").html("No auto top up information found!").show();
        }
    });
}

/* ----------------------------------------------------- 
   *  eof TAB AUTO TOPUP 
   ===================================================== */


