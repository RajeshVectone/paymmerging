﻿/* ===================================================== 
   *  SIM ORDER LIST 
   ----------------------------------------------------- */

function SIMOrderList(mobileno, sitecode) {
    var url = apiServer + '/api/ordermanagement/';
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking2 #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking2").css({ "height": $(document).outerHeight() + "px" });
    jQuery.support.cors = true;
    var JSONSendData = {
        mobileno: mobileno,
        sitecode: sitecode
    };
    $.ajax
    ({
        url: url,
        type: 'get',
        data: JSONSendData,
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#TableSIMOrderListContainer").hide();
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            $("#TableSIMOrderListContainer").show();
            $('#TableSIMOrderList').dataTable({
                bDestroy: true,
                aaData: feedback,
                iDisplayLength: 20,
                bPaginate: true,
                bFilter: false,
                bProcessing: true,
                bInfo: false,
                aaSorting: [[0, "asc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Records found."
                },
                sPaginationType: "bootstrap",
                aoColumns: [
                    {
                        mDataProp: "sitecode", sTitle: 'Free SIM ID',
                        fnRender: function (ob) {
                            return ob.aData.freesimid;
                        }
                    },
                    { mDataProp: "title", sTitle: 'Title' },
                    {
                        mDataProp: "firstname", sTitle: 'First Name',
                        fnRender: function (ob) {
                            return ob.aData.firstname;
                        }
                    },
                    {
                        mDataProp: "lastname", sTitle: 'Last Name',
                        fnRender: function (ob) {
                            return ob.aData.lastname;
                        }
                    },
                    {
                        mDataProp: "registerdate", sTitle: 'Register Date',
                        fnRender: function (ob) {
                            return ob.aData.registerdate;
                        }
                    },
                    { mDataProp: "source_address", sTitle: 'Purchased From' },
                    {
                        mDataProp: "mimsi_order_id", sTitle: "Action", sWidth: "156px",
                        fnRender: function (ob) {
                            return "<center><a href=\"javascript:void(0)\" class=\"orderIDAnchor btn blue\" param-data=\"" + ob.aData.freesimid + "\" mimsi-data=\"" + ob.aData.mimsi_order_id + "\">View</a>";
                        }
                    }
                ],
                fnCreatedRow: function (nRow, aData, iDataIndex) {
                    $(nRow).addClass("rowTableSOL");
                },
                fnDrawCallback: function () {
                    $("#SIMorderListPanel input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                    $("#TableSIMOrderList td a").css({ "text-decoration": "underline" });
                    $("#TableSIMOrderList td").css({ "font-size": "12px" });
                },
                fnInitComplete: function (ob) {

                }
            });
            $("#TableSIMOrderList_wrapper").css({ "overflow": "auto", "overflow-y": "hidden", "padding-bottom": "10px" });
            $("#TableSIMOrderList tbody td").css({ "font-size": "12px" });
        },
        error: function () {
            $("#ajax-screen-masking").hide();
        }



    });
    //Swap Details
    var urls = apiServer + '/api/simtransfer/';
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking2 #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking2").css({ "height": $(document).outerHeight() + "px" });
    jQuery.support.cors = true;
    var JSONSendData = {
        mobileno: mobileno,
        sitecode: sitecode
    };
    $.ajax
    ({
        url: urls,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#TableSIMSwap").hide();
            $("#ajax-screen-masking").show();
        },
        success: function (feedbacks) {
            $("#ajax-screen-masking").hide();
            $("#TableSIMSwap").show();
            $('#TableSimSwap').dataTable({
                bDestroy: true,
                aaData: feedbacks,
                iDisplayLength: 20,
                bPaginate: true,
                bFilter: false,
                bProcessing: true,
                bInfo: false,
                aaSorting: [[0, "asc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Records found."
                },
                sPaginationType: "bootstrap",
                aoColumns: [
                    {
                        mDataProp: "Createdate", sTitle: 'Createdate',
                        fnRender: function (ob) {
                            return ob.aData.Createdate;
                        }
                    },
                    { mDataProp: "msisdn", sTitle: 'Mobile Number' },
                    {
                        mDataProp: "Iccid_Old", sTitle: 'Old Iccid',
                        fnRender: function (ob) {
                            return ob.aData.Iccid_Old;
                        }
                    },
                    { mDataProp: "Balance_Old", sTitle: 'Old Balance' },
                    {
                        mDataProp: "Iccid_New", sTitle: 'New Iccid',
                        fnRender: function (ob) {
                            return ob.aData.lastname;
                        }
                    }
                ],
                fnCreatedRow: function (nRow, aData, iDataIndex) {
                    $(nRow).addClass("rowTableSOL");
                },
                fnDrawCallback: function () {
                    $("#SIMorderListPanel input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                    $("#TableSimSwap td a").css({ "text-decoration": "underline" });
                    $("#TableSimSwap td").css({ "font-size": "12px" });
                },
                fnInitComplete: function (ob) {

                }
            });
            $("#TableSIMOrderList_wrapper").css({ "overflow": "auto", "overflow-y": "hidden", "padding-bottom": "10px" });
            $("#orderDetailSwap tbody td").css({ "font-size": "12px" });
        },
        error: function () {
            $("#ajax-screen-masking").hide();
        }
    });
}

function SIMViewOrderDetail(orderID, Sitecode) {
    var ajaxSpinnerTop = ($(window).outerHeight() / 2) + $(window).scrollTop();
    $("#ajax-screen-masking2 #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking2").css({ "height": $(document).outerHeight() + "px" });
    $("#VieworderDetail #ViewSIMOrderDetailError").html("").hide();
    $("#VieworderDetail input, #VieworderDetail select").val("").removeAttr("disabled");
    var url = apiServer + '/api/SIMOrderSearch/2?val=' + orderID + "&Sitecode=" + Sitecode;
    $.ajax
    ({
        url: url,
        type: 'get',
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            $('table.tblOrderDetail #tdODTitle').html(feedback.title);
            $('table.tblOrderDetail #tdODFirstName').html(feedback.firstname);
            $('table.tblOrderDetail #tdODLastName').html(feedback.lastname);
            if (feedback.mimsi_order_id > 0) {
                $('table.tblOrderDetail #tdODHouseNo').html(feedback.Address1);
                $('table.tblOrderDetail #tdODAddress').html(feedback.Address2);
            } else {
                $('table.tblOrderDetail #tdODHouseNo').html(feedback.houseno);
                $('table.tblOrderDetail #tdODAddress').html(feedback.Address);
            }
            $('table.tblOrderDetail #tdODCity').html(feedback.city);
            $('table.tblOrderDetail #tdODPostcode').html(feedback.postcode);
            $('table.tblOrderDetail #tdODTelp').html(feedback.telephone);
            $('table.tblOrderDetail #tdODMobilePhone').html(feedback.mobilephone);
            $('table.tblOrderDetail #tdODEmail').html(feedback.email);
            $('table.tblOrderDetail #tdODBirthdate').html(convertDateISO8601(feedback.birthdate, 0));
            $('table.tblOrderDetail #tdODStatus').html(feedback.freesimstatus);
            $('table.tblOrderDetail #tdODQty').html(feedback.Quantity);
            $('table.tblOrderDetail #tdODSourceAddress').html(feedback.source_address);
            $('table.tblOrderDetail #tdODOrderSIMURL').html(feedback.ordersim_url);
            $('table.tblOrderDetail #tdODRegDate').html(convertDateISO8601(feedback.registerdate, 0));
            $('table.tblOrderDetail #tdODSentDate').html(convertDateISO8601(feedback.sentdate, 0));
            $('table.tblOrderDetail #tdODActivateDate').html(convertDateISO8601(feedback.activatedate, 0));
            $('table.tblOrderDetail #tdODICCID').html($('#tdCIHICCID').text());
            $('#orderDetail').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });
}
function SIMOrderList1(mobileno, sitecode, sim, user_role) {
    var url = apiServer + '/api/ordermanagement/';
    //var ajaxSpinnerTop = $(window).outerHeight() / 2;
    //$("#ajax-screen-masking2 #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    //$("#ajax-screen-masking2").css({ "height": $(document).outerHeight() + "px" });
    //$("#ajax-screen-masking").show();
    jQuery.support.cors = true;
    var JSONSendData = {
        mobileno: mobileno,
        sitecode: sitecode,
        Sim: sim
    };
    $.ajax
    ({
        url: url,
        type: 'get',
        data: JSONSendData,
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#TableSIMOrderListContainer").hide();
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            $("#TableSIMOrderListContainer").show();
            $('#TableSIMOrderList').dataTable({
                bDestroy: true,
                aaData: feedback,
                iDisplayLength: 20,
                bPaginate: true,
                bFilter: false,
                bProcessing: true,
                bInfo: false,
                aaSorting: [],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Records found."
                },
                sPaginationType: "bootstrap",
                aoColumns: [
                    {
                        mDataProp: "sitecode", sTitle: 'Free SIM ID',
                        fnRender: function (ob) {
                            return ob.aData.freesimid;
                        }
                    },
                    //{ mDataProp: "title", sTitle: 'Title' },
                    //{
                    //    mDataProp: "firstname", sTitle: 'First Name',
                    //    fnRender: function (ob) {
                    //        return ob.aData.firstname;
                    //    }
                    //},
                    //{
                    //    mDataProp: "lastname", sTitle: 'Last Name',
                    //    fnRender: function (ob) {
                    //        return ob.aData.lastname;
                    //    }
                    //},
                    //{
                    //    mDataProp: "registerdate", sTitle: 'Register Date',
                    //    fnRender: function (ob) {
                    //        return ob.aData.registerdate;
                    //    }
                    //},
                    { mDataProp: "Fullname", sTitle: 'Name' },
                    {
                        mDataProp: "registerdate_string", sTitle: 'Register Date',
                        fnRender: function (ob) {
                            return ob.aData.registerdate_string;
                        }
                    },
                    { mDataProp: "CyberSourceId", sTitle: 'Order Ref.' },
                    { mDataProp: "source_address", sTitle: 'Source Address' },
                    { mDataProp: "order_status", sTitle: 'Order Status' },
                    //Added : 24-Dec-2018 - Send a SIM Card
                    {
                        mDataProp: "title", sTitle: "Action", bSortable: false,
                        fnRender: function (ob) {
                            if (ob.aData.order_status != "CANCELED" && user_role == "Admin")
                                return "<button id=\"orderIDCancel\" class=\"orderIDCancel btn blue\" param-data=\"" + ob.aData.freesimid + "\" mimsi-data=\"" + ob.aData.mimsi_order_id + "\" ref-data=\"" + ob.aData.CyberSourceId + "\">Cancel</button>";
                            else
                                return "<button id=\"orderIDCancel\" class=\"orderIDCancel btn lightgrey\" param-data=\"" + ob.aData.freesimid + "\" mimsi-data=\"" + ob.aData.mimsi_order_id + "\" ref-data=\"" + ob.aData.CyberSourceId + "\"  disabled=\"disabled\">Cancel</button>";
                        }
                    },
                    {
                        mDataProp: "sim_brand", sTitle: "Action", bSortable: false,
                        fnRender: function (ob) {
                            if (ob.aData.order_status != "CANCELED" && user_role == "Admin")
                                return "<button id=\"orderIDReplace\" class=\"orderIDReplace btn blue\" param-data=\"" + ob.aData.freesimid + "\" mimsi-data=\"" + ob.aData.mimsi_order_id + "\">Replace</button>";
                            else
                                return "<button id=\"orderIDReplace\" class=\"orderIDReplace btn lightgrey\" param-data=\"" + ob.aData.freesimid + "\" mimsi-data=\"" + ob.aData.mimsi_order_id + "\"  disabled=\"disabled\">Replace</button>";;
                        }
                    },
                    {
                        mDataProp: "mimsi_order_id", sTitle: "Action", bSortable: false,
                        fnRender: function (ob) {
                            return "<button id=\"orderIDAnchor\" class=\"orderIDAnchor btn blue\" param-data=\"" + ob.aData.freesimid + "\" mimsi-data=\"" + ob.aData.mimsi_order_id + "\">View</button>";
                        }
                    }
                ],
                fnCreatedRow: function (nRow, aData, iDataIndex) {
                    $(nRow).addClass("rowTableSOL");
                },
                fnDrawCallback: function () {
                    $("#SIMorderListPanel input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                    $("#TableSIMOrderList td a").css({ "text-decoration": "underline" });
                    $("#TableSIMOrderList td").css({ "font-size": "12px" });
                },
                fnInitComplete: function (ob) {

                }
            });
            $("#TableSIMOrderList_wrapper").css({ "overflow": "auto", "overflow-y": "hidden", "padding-bottom": "10px" });
            $("#TableSIMOrderList tbody td").css({ "font-size": "12px" });
        },
        error: function () {
            $("#ajax-screen-masking").hide();
        }
    });

    //Swap Details
    var urls = apiServer + '/api/simtransfer/';
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking2 #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking2").css({ "height": $(document).outerHeight() + "px" });
    jQuery.support.cors = true;
    var JSONSendData = {
        SubType: 1,
        Msisdn: mobileno,
        sitecode: sitecode
    };
    $.ajax
    ({
        url: urls,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#TableSIMSwap").hide();
            $("#ajax-screen-masking").show();
        },
        success: function (feedbacks) {
            $("#ajax-screen-masking").hide();
            $("#TableSIMSwap").show();
            $('#TableSimSwap').dataTable({
                bDestroy: true,
                aaData: feedbacks,
                iDisplayLength: 20,
                bPaginate: true,
                bFilter: false,
                bProcessing: true,
                bInfo: false,
                aaSorting: [],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Records found."
                },
                sPaginationType: "bootstrap",
                aoColumns: [
                    {
                        mDataProp: "Createdate_string", sTitle: 'Createdate',

                    },
                    { mDataProp: "Msisdn", sTitle: 'Mobile Number' },
                    {
                        mDataProp: "Iccid_Old", sTitle: 'Old Iccid',

                    },
                    { mDataProp: "Balance_String", sTitle: 'Old Balance' },
                    {
                        mDataProp: "Iccid_New", sTitle: 'New Iccid',

                    }
                ],
                fnCreatedRow: function (nRow, aData, iDataIndex) {
                    $(nRow).addClass("rowTableSOL");
                },
                fnDrawCallback: function () {
                    $("#SIMorderListPanel input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                    $("#TableSimSwap td a").css({ "text-decoration": "underline" });
                    $("#TableSimSwap td").css({ "font-size": "12px" });
                },
                fnInitComplete: function (ob) {

                }
            });
            $("#TableSIMOrderList_wrapper").css({ "overflow": "auto", "overflow-y": "hidden", "padding-bottom": "10px" });
            $("#orderDetailSwap tbody td").css({ "font-size": "12px" });
        },
        error: function () {
            $("#ajax-screen-masking").hide();
        }
    });
}

//Added : 24-Dec-2018 - Send a SIM Card
function SIMOrderDetailReplace(orderID, Sitecode) {
    var ajaxSpinnerTop = ($(window).outerHeight() / 2) + $(window).scrollTop();
    $("#ajax-screen-masking3 #ajax-loader2").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking3").css({ "height": $(document).outerHeight() + "px" });
    $("#orderDetailReplace #SIMOrderDetailReplaceError").html("").hide();
    $("#orderDetailReplace input, #orderDetailReplace select").val("").removeAttr("disabled");
    var url = apiServer + '/api/SIMOrderSearch/2?val=' + orderID + "&Sitecode=" + Sitecode;
    $.ajax
    ({
        url: url,
        type: 'get',
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking3").show();
            //$("table.tblOrderDetail:eq(1) tr#trODMailRef").remove();
        },
        success: function (feedback) {
            $("#ajax-screen-masking3").hide();

            $('table.tblOrderDetailReplace #tdODRTitle').html(feedback.title);
            $('table.tblOrderDetailReplace #tdODRFirstName').html(feedback.firstname);
            $('table.tblOrderDetailReplace #tdODRLastName').html(feedback.lastname);
            if (feedback.mimsi_order_id > 0) {
                $('table.tblOrderDetailReplace #tdODRHouseNo').val(feedback.Address1);
                $('table.tblOrderDetailReplace #tdODRAddress').val(feedback.Address2);
                $('table.tblOrderDetailReplace #tdODRAddress1').val(feedback.Address1);
                $('table.tblOrderDetailReplace #tdODRAddress2').val(feedback.Address2);
                $('table.tblOrderDetailReplace #tdODRAddress3').val(feedback.Address3);
            } else {
                $('table.tblOrderDetailReplace #tdODRHouseNo').val(feedback.houseno);
                $('table.tblOrderDetailReplace #tdODRAddress').val(feedback.Address);
                $('table.tblOrderDetailReplace #tdODRAddress1').val(feedback.Address1);
                $('table.tblOrderDetailReplace #tdODRAddress2').val(feedback.Address2);
                $('table.tblOrderDetailReplace #tdODRAddress3').val(feedback.Address3);
            }
            if (Sitecode == 'MCM') {

                $('table.tblOrderDetailReplace #trAddress1').show();
                $('table.tblOrderDetailReplace #trAddress2').show();
                $('table.tblOrderDetailReplace #trAddress3').show();


                $('table.tblOrderDetailReplace #trHouseno').hide();
                $('table.tblOrderDetailReplace #trAddress').hide();

            }
            else {
                $('table.tblOrderDetailReplace #trAddress1').hide();
                $('table.tblOrderDetailReplace #trAddress2').hide();
                $('table.tblOrderDetailReplace #trAddress3').hide();


                $('table.tblOrderDetailReplace #trHouseno').show();
                $('table.tblOrderDetailReplace #trAddress').show();
            }
            $('table.tblOrderDetailReplace #tdODRCity').val(feedback.city);
            $('table.tblOrderDetailReplace #tdODRPostcode').val(feedback.postcode.toUpperCase());
            $('table.tblOrderDetailReplace #tdODRTelp').html(feedback.telephone);
            $('table.tblOrderDetailReplace #tdODRMobilePhone').html(feedback.mobilephone);
            $('table.tblOrderDetailReplace #tdODREmail').html(feedback.email);
            $('table.tblOrderDetailReplace #tdODRBirthdate').html(convertDateISO8601(feedback.birthdate, 0));
            $('table.tblOrderDetailReplace #tdOfavcallcountry').html(feedback.fav_call_country);
            $('table.tblOrderDetailReplace2 #tdODRStatus').html(feedback.freesimstatus);
            $('table.tblOrderDetailReplace2 #tdODRQty').html(feedback.Quantity);
            $('table.tblOrderDetailReplace2 #tdODRSourceAddress').html(feedback.source_address);
            $('table.tblOrderDetailReplace2 #tdODROrderSIMURL').html(feedback.ordersim_url);
            $('table.tblOrderDetailReplace2 #tdODRRegDate').html(convertDateISO8601(feedback.registerdate, 0));
            $('table.tblOrderDetailReplace2 #tdODRSentDate').html(convertDateISO8601(feedback.sentdate, 0));
            //11-Dec-2017 : Added to display Replacement Date
            $('table.tblOrderDetailReplace2 #tdODRReplacementDate').html(convertDateISO8601(feedback.replacement_date, 0));

            $('table.tblOrderDetailReplace2 #tdODRActivateDate').html(convertDateISO8601(feedback.activatedate, 0));

            var RoyalMailRefrence = convertNulltoEmptyString(feedback.royal_mail_reference);
            if (RoyalMailRefrence == '') {
                $('table.tblOrderDetailReplace2 input[name=input_mailrefR]').val('').removeAttr('readonly disabled');
            } else {
                $('table.tblOrderDetailReplace2 input[name=input_mailrefR]').val(RoyalMailRefrence).attr({ 'readonly': true, 'disabled': true });
            }
            var SubscriberType = convertNulltoEmptyString(feedback.Subscriber_Type);

            var QTYOrder = parseInt(feedback.Quantity);
            $("input[name=QTYOrderR]").val(QTYOrder);

            $('#orderDetailReplace').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });

            $("span#prefix_89_1, span#prefix_89_2").hide();
            $("input[name=input_iccidR], input[name=input_iccidR2]").css({ "width": "205px" });

            if (QTYOrder > 1) {
                $("input[name=input_iccidR], input[name=input_iccidR2]").hide();
                $("select[name=optICCIDList]").html("").show();
                var url2 = apiServer + '/api/SIMOrderSearch/3?val=' + orderID + "&Sitecode=" + Sitecode;
                $.getJSON(url2, function (feedback2) {
                    var arrFreeSIMID = "";
                    var arrICCID = "";
                    var arrMIMSIOrderID = "";

                    /******************* Email sending SIM dispatch **************/
                    var firstname = "";
                    //   var lastname = "";
                    var email = "";
                    /******************* Email sending  SIM dispatch **************/
                    for (var idx in feedback2) {
                        if (convertNulltoEmptyString(feedback2[idx].freesimid) != "") {
                            arrFreeSIMID += feedback2[idx].freesimid + "|";
                        }
                        if (convertNulltoEmptyString(feedback2[idx].iccid) != "") {
                            arrICCID += feedback2[idx].iccid + "|";
                        }
                        if (convertNulltoEmptyString(feedback2[idx].mimsi_order_id) != "") {
                            arrMIMSIOrderID += feedback2[idx].mimsi_order_id + "|";
                        }
                        /******************* Email sending  SIMdispatch **************/
                        if (convertNulltoEmptyString(feedback2[idx].firstname) != "") {
                            firstname += feedback2[idx].firstname + "|";
                        }
                        //if (convertNulltoEmptyString(feedback2[idx].lastname) != "") {
                        //    lastname += feedback2[idx].lastname + "|";
                        //}
                        if (convertNulltoEmptyString(feedback2[idx].email) != "") {
                            email += feedback2[idx].email + "|";
                        }
                        /******************* Email sending  SIM dispatch **************/
                    }
                    arrFreeSIMID = arrFreeSIMID.replace(/\|$/, '');
                    arrICCID = arrICCID.replace(/\|$/, '');
                    arrMIMSIOrderID = arrMIMSIOrderID.replace(/\|$/, '');

                    /******************* Email sending  SIM dispatch **************/
                    firstname = firstname.replace(/\|$/, '');
                    // lastname = lastname.replace(/\|$/, '');
                    email = email.replace(/\|$/, '');
                    /******************* Email sending  SIM dispatch **************/
                    $("input[name=arrFreeSIMIDR]").val(arrFreeSIMID);
                    $("input[name=arrICCIDR]").val(arrICCID);
                    $("input[name=arrMIMSIOrderIDR]").val(arrMIMSIOrderID);
                    $("#hdSubscriberTypeR").val(feedback.Subscriber_Type);
                    /******************* Email sending  SIM dispatch **************/
                    $("input[name=arrFirstName]").val(firstname);
                    // $("input[name=arrLasttName]").val(lastname);
                    $("input[name=arrEmailtName]").val(email);
                    /******************* Email sending  SIM dispatch **************/
                    if (arrICCID != "") {
                        $("tr#trODRICCID2, select[name=optICCIDList2]").show();
                        $("button#btnODOpenICCIDList2").remove();
                        $("select[name=optICCIDList]").css({ "width": "260px", "font-size": "14px" });
                        $("#tdODRICCID2").append("<button class=\"btn blue\" id=\"btnODOpenICCIDList2\" style=\"width:105px;margin-left:5px;height:28px;margin-top:-9px;padding:0;\">Add ICCID No</button>");

                        $("table#tblICCIDList1, table#tblICCIDList2").html("");
                        $("table#tblICCIDList1 input").attr("disabled", true);
                        var ICCIDListNo = "";
                        var splitICCID = arrICCID.split("|");
                        if (splitICCID.length) {
                            for (var i = 0; i < splitICCID.length; i++) {
                                ICCIDListNo += "<option value=\"" + splitICCID[i] + "\">" + splitICCID[i] + "</option>";
                            }
                            $("select[name=optICCIDList]").attr("readonly", true).html("").append(ICCIDListNo);
                        }
                        $("table#tblICCIDList2").append("<tr id=\"trErrorICCIDList2\"><td colspan=\"2\" id=\"tdErrorICCIDList2\"></td></tr>");
                        for (var i = 0; i < QTYOrder; i++) {
                            $("table#tblICCIDList2").append("<tr><td style=\"width:60px\">ICCID " + (i + 1) + "&nbsp;:</td><td><span style=\"font-size:14px;color:#505050;border:1px solid #c0c0c0;background-color:#eeeeee;margin-right:2px;padding:5px 2px;\">89</span><input style=\"margin:0;margin-bottom:3px;width:213px;\" class=\"iccid_list_1\" type=\"text\" id=\"input_iccid2_" + (i + 1) + "\" name=\"input_iccid2_" + (i + 1) + "\"/><input class=\"iccid_list_h1\" type=\"hidden\" /></td></tr>");
                            $("table#tblICCIDList2 input").attr("disabled", true);
                        }

                        $("input[type=text].iccid_list_1").eq(0).removeAttr("disabled");

                    } else {
                        $("tr#trODRICCID2").hide();
                        $("button#btnODOpenICCIDList1").remove();
                        $("select[name=optICCIDList]").css({ "width": "180px", "font-size": "12px" }).removeAttr("readonly");
                        $("#tdODRICCID").append("<button class=\"btn blue\" id=\"btnODOpenICCIDList1\" style=\"width:105px;margin-left:5px;height:28px;margin-top:-9px;padding:0;\">Add ICCID No</button>");
                        $("table#tblICCIDList1").html("");
                        $("table#tblICCIDList1").append("<tr id=\"trErrorICCIDList1\"><td colspan=\"2\" id=\"tdErrorICCIDList1\"></td></tr>");
                        for (var i = 0; i < QTYOrder; i++) {
                            if (SubscriberType == 'TELE SALES') {
                                $("table#tblICCIDList1").append("<tr><td style=\"width:60px\">ICCID " + (i + 1) + "&nbsp;:</td><td><input maxlength=\"19\" style=\"margin:0;margin-bottom:3px;margin-top:-1px;width:213px;\" class=\"iccid_list_0\" type=\"text\" id=\"input_iccid1_" + (i + 1) + "\" name=\"input_iccid1_" + (i + 1) + "\"/><input class=\"iccid_list_h0\" type=\"hidden\" /></td></tr>");
                                $("table#tblICCIDList1 input").attr("disabled", true);
                            }
                            else {
                                $("table#tblICCIDList1").append("<tr><td style=\"width:60px\">ICCID " + (i + 1) + "&nbsp;:</td><td><span style=\"font-size:14px;color:#505050;border:1px solid #c0c0c0;background-color:#eeeeee;margin-right:2px;padding:5px 2px;\">89</span><input maxlength=\"17\" style=\"margin:0;margin-bottom:3px;margin-top:-1px;width:213px;\" class=\"iccid_list_0\" type=\"text\" id=\"input_iccid1_" + (i + 1) + "\" name=\"input_iccid1_" + (i + 1) + "\"/><input class=\"iccid_list_h0\" type=\"hidden\" /></td></tr>");
                                $("table#tblICCIDList1 input").attr("disabled", true);
                            }
                        }
                        $("input[type=text].iccid_list_0").eq(0).removeAttr("disabled");
                    }
                });
            } else {

                $("button#btnODOpenICCIDList1, button#btnODOpenICCIDList2").remove();
                $("table#tblICCIDList1, table#tblICCIDList2").html("");
                $("input[name=input_iccidR]").show();
                $("input[name=input_iccidR2], select[name=optICCIDList], select[name=optICCIDList2]").hide();
                var arrFreeSIMID = convertNulltoEmptyString(feedback.freesimid);
                var arrICCID = convertNulltoEmptyString(feedback.iccid);
                var arrMIMSIOrderID = convertNulltoEmptyString(feedback.mimsi_order_id);

                /******************* Email sending  SIM dispatch **************/
                var firstname = convertNulltoEmptyString(feedback.firstname);
                //var lastname = convertNulltoEmptyString(feedback.lastname);
                var email = convertNulltoEmptyString(feedback.email);
                /******************* Email sending  SIM dispatch **************/
                $("input[name=arrFreeSIMIDR]").val(arrFreeSIMID);
                $("input[name=arrICCIDR]").val(arrICCID);
                $("input[name=arrMIMSIOrderIDR]").val(arrMIMSIOrderID);

                /******************* Email sending  SIM dispatch **************/
                $("input[name=arrFirstName]").val(firstname);
                //$("input[name=arrLasttName]").val(lastname);
                $("input[name=arrEmailtName]").val(email);
                /******************* Email sending  SIM dispatch **************/
                if (arrICCID != "") {
                    $("input[name=input_iccidR]").val(arrICCID).attr({ "readonly": true, "maxlength": "19" });
                    $("tr#trODRICCID2").show();
                    $("span#prefix_89_2").show();
                    $("input[name=input_iccidR2]").css({ "width": "180px" }).show().focus();
                } else {
                    $("span#prefix_89_1").show();
                    $("input[name=input_iccidR]").css({ "width": "180px" }).val("").removeAttr("readonly").attr({ "maxlength": "17" });
                    $("input[name=input_iccidR]").focus();
                    $("tr#trODRICCID2").hide();
                }
            }

        },
        error: function () {
            $("#ajax-screen-masking3").hide();
        }
    });
}

//Added : 25-Dec-2018 - Replace Sim Order
function UpdateOrderReplace(arrSettings) {
    var url = apiServer + '/api/OrderManagement';
    jQuery.support.cors = true;
    $.ajax({
        url: url,
        type: 'put',
        data: JSON.stringify(arrSettings),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        async: false,
        timeout: 50000,
        success: function (data) {
            if (data.errcode == "0") {
                $("td.currentDispatchStatusR").html("<span class=\"alert-success\">Success</span>");
            } else {
                $("td.currentDispatchStatusR").html("<span class=\"alert-error\">Failed</span>");
                var errMessage = "";
                errMessage += "&emsp;<span class=\"btnDPViewError\" style=\"cursor:pointer\"><i class=\"icon-search\"></i></span>";
                errMessage += "<div style=\"position: absolute;z-index:9999;display:none;width:0\" class=\"DPDetailError\">";
                errMessage += "<div style=\"float:left;top:-31px;left:-203px;width:250px;position:relative;padding: 10px;background: #f2dede;border-radius: 4px !important;color: #b94a48;border:1px solid #eed3d7;\">";
                errMessage += "<span>" + data.errmsg + "</span>";
                errMessage += "<div style=\"border-left: 5px solid #b94a48;left:271px;top:4px\" class=\"arrow-right\"></div>";
                errMessage += "</div></div>";
                $("td.currentDispatchStatusR").append(errMessage);
            }
            $("td.currentDispatchStatusR").removeClass("currentDispatchStatusR");
        }
    });
}

//Added : 25-Dec-2018 - Cancel Sim Order
function UpdateOrderCancel(arrSettings) {
    $("#ajax-screen-masking").show();
    var url = apiServer + '/api/OrderManagement';
    jQuery.support.cors = true;
    $.ajax({
        url: url,
        type: 'put',
        data: JSON.stringify(arrSettings),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        async: false,
        timeout: 50000,
        success: function (data) {
            $("#ajax-screen-masking").hide();
            if (data.length > 0) {
                alert(data[0].errmsg);
                if (data[0].errcode == 0) {
                    location.reload();
                }
            }
        },
        error: function (data) {
            $("#ajax-screen-masking").hide();
        }
    });
}
/* ----------------------------------------------------- 
   *  eof SIM ORDER LIST 
   ===================================================== */


