﻿/* ===================================================== 
   *  SIM ORDER LIST 
   ----------------------------------------------------- */

function SIMOrderList(siteCode, productCode, orderType, fromdate, todate) {

    var url = apiServer + '/api/simordersearch/';
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    jQuery.support.cors = true;
    var JSONSendData = {
        status: orderType,
        fromdate: fromdate,
        todate: todate,
        sitecode: siteCode,
        product_code: productCode
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#TableSIMOrderListContainer").hide();
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            $("#TableSIMOrderListContainer").show();
            $('#TableSIMOrderList').dataTable({
                bDestroy: true,
                aaData: feedback,
                iDisplayLength: 20,
                bPaginate: true,
                bFilter: false,
                bProcessing: true,
                bInfo: false,
                sPaginationType: "bootstrap",
                aoColumns: [
                    {
                        mDataProp: "sitecode", sTitle: 'Free SIM ID',
                        fnRender: function (ob) {
                            return "<center><a href=\"javascript:void(0)\" class=\"orderIDAnchor\" param-data=\"" + ob.aData.freesimid + "\" mimsi-data=\"" + ob.aData.mimsi_order_id + "\">" + ob.aData.freesimid + "</a></center>";
                        }
                    },
                    { mDataProp: "title", sTitle: 'Title' },
                    {
                        mDataProp: "firstname", sTitle: 'First Name',
                        fnRender: function (ob) {
                            return "<a href=\"javascript:void(0)\" class=\"firstnameAnchor\" param-data=\"" + ob.aData.freesimid + "\" mimsi-data=\"" + ob.aData.mimsi_order_id + "\">" + ob.aData.firstname + "</a>";
                        }
                    },
                    {
                        mDataProp: "lastname", sTitle: 'Last Name',
                        fnRender: function (ob) {
                            return "<a href=\"javascript:void(0)\" class=\"lastnameAnchor\" param-data=\"" + ob.aData.freesimid + "\" mimsi-data=\"" + ob.aData.mimsi_order_id + "\">" + ob.aData.lastname + "</a>";
                        }
                    },
                    { mDataProp: "freesimstatus", sTitle: 'Status' },
                    {
                        mDataProp: "registerdate", sTitle: 'Register Date',
                        fnRender: function (ob) {
                            return convertDate03(ob.aData.registerdate);
                        }
                    },
                    { mDataProp: "FreeSimType", sTitle: 'Free SIM Type' },
                    { mDataProp: "CyberSourceId", sTitle: 'Cybersource ID' },
                    { mDataProp: "Subscriber_Type", sTitle: 'Subscriber Type' },
                    { mDataProp: "Quantity", sTitle: 'Quantity' },
                    { mDataProp: "SourceReg", sTitle: 'Source' },
                    { mDataProp: "source_address", sTitle: 'Source Address', sWidth: 100 },
                    { mDataProp: "ordersim_url", sTitle: 'Ordersim URL' },
                    { mDataProp: "msisdn_referrer", sTitle: 'MSISDN Referrer' },
                    { mDataProp: "fav_call_country", sTitle: 'Country you call most' }
                    //11-Jan-2016 : Moorthy : Added new column IP Address
                    , { mDataProp: "ipaddress", sTitle: 'IP Address' }
                    //21-Jul-2016 : Moorthy : Added for landing & visit page url's
                    , { mDataProp: "landing_page_url", sTitle: 'Landing Page', sWidth: 100 }
                    , { mDataProp: "last_visit_page_url", sTitle: 'Visit Page', sWidth: 100 }
                ],
                fnCreatedRow: function (nRow, aData, iDataIndex) {
                    $(nRow).addClass("rowTableSOL");
                },
                fnDrawCallback: function () {
                    $("#SIMorderListPanel input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                    $("#TableSIMOrderList td a").css({ "text-decoration": "underline" });
                    $("#TableSIMOrderList td").css({ "font-size": "12px" });
                },
                fnInitComplete: function (ob) {
                    $("#TableSIMOrderList .row-fluid:eq(0) .span6").html("");
                    var orderType = $("select[name=orderType] option:selected").text();
                    var countryName = $("input[name=ResultCountry]").val();
                    var gProductCode = $("input[name=gProductCode]").val();
                    var searchResultStr = "Searching result the <b>" + orderType + "</b> in <b>" + countryName + "</b>.<br/>From " + convertDate01($("input#startdate").val()) + " to " + convertDate01($("input#enddate").val()) + ".<br/><b><span id=\"fnRecordsTotal\">" + ob.fnRecordsTotal() + "</span></b> order(s) found.";
                    $("#TableSIMOrderList_wrapper .span6:eq(0)").html("<div style=\"float:left\">" + searchResultStr + "</div>");
                    if (ob.fnRecordsTotal() <= 0) {
                        $("#TableSIMOrderList_wrapper .row-fluid:eq(1)").remove();
                    } else {
                        if (ob.fnRecordsTotal() <= ob._iDisplayLength) {
                            $("#TableSIMOrderList_wrapper .row-fluid:eq(1)").remove();
                        }
                        if ($("button#ExportToExcelBtn").length <= 0) {
                            var ExportToExcelBtn = "<button class=\"btn blue\" id=\"ExportToExcelBtn\" name=\"ExportToExcelBtn\" style=\"float:right;margin-top:20px\">Download</button>";
                            $("#TableSIMOrderList_wrapper .row-fluid:eq(0) .span6:eq(1)").append(ExportToExcelBtn);
                        }
                    }
                    $("#TableSIMOrderList td").css({ "font-size": "12px" });
                    $("#TableSIMOrderList td a").css({ "text-decoration": "underline" });
                    $("#SIMorderListPanel input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                }
            });
            $("#TableSIMOrderList_wrapper").css({ "overflow": "auto", "overflow-y": "hidden", "padding-bottom": "10px" });
            $("#TableSIMOrderList tbody td").css({ "font-size": "12px" });
        },
        error: function () {
            $("#ajax-screen-masking").hide();
        }
    });
}

function SIMOrderSearch(siteCode, searchBy, searchValue, productCode) {
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    var url = apiServer + '/api/simordersearch/';
    jQuery.support.cors = true;
    var JSONSendData = {
        sitecode: siteCode,
        searchby: searchBy,
        searchvalue: searchValue,
        product_code: productCode
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#TableSIMOrderListContainer").hide();
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            $("#TableSIMOrderListContainer").show();
            $('#TableSIMOrderList').dataTable({
                bDestroy: true,
                aaData: feedback,
                iDisplayLength: 20,
                bPaginate: true,
                bFilter: false,
                bInfo: false,
                sPaginationType: "bootstrap",
                aoColumns: [

                    {
                        mDataProp: "sitecode", sTitle: 'Free SIM ID',
                        fnRender: function (ob) {
                            return "<center><a href=\"javascript:void(0)\" class=\"orderIDAnchor\" param-data=\"" + ob.aData.freesimid + "\" mimsi-data=\"" + ob.aData.mimsi_order_id + "\">" + ob.aData.freesimid + "</a></center>";
                        }
                    },
                    { mDataProp: "title", sTitle: 'Title' },
                    {
                        mDataProp: "firstname", sTitle: 'First Name',
                        fnRender: function (ob) {
                            return "<a href=\"javascript:void(0)\" class=\"firstnameAnchor\" param-data=\"" + ob.aData.freesimid + "\" mimsi-data=\"" + ob.aData.mimsi_order_id + "\">" + ob.aData.firstname + "</a>";
                        }
                    },
                    {
                        mDataProp: "lastname", sTitle: 'Last Name',
                        fnRender: function (ob) {
                            return "<a href=\"javascript:void(0)\" class=\"lastnameAnchor\" param-data=\"" + ob.aData.freesimid + "\" mimsi-data=\"" + ob.aData.mimsi_order_id + "\">" + ob.aData.lastname + "</a>";
                        }
                    },
                    { mDataProp: "freesimstatus", sTitle: 'Status' },
                    {
                        mDataProp: "registerdate", sTitle: 'Register Date',
                        fnRender: function (ob) {
                            return convertDate03(ob.aData.registerdate);
                        }
                    },
                    { mDataProp: "FreeSimType", sTitle: 'Free SIM Type	' },
                    { mDataProp: "CyberSourceId", sTitle: 'Cybersource ID' },
                    { mDataProp: "Subscriber_Type", sTitle: 'Subscriber Type' },
                    { mDataProp: "Quantity", sTitle: 'Quantity' },
                    { mDataProp: "SourceReg", sTitle: 'Source' },
                    { mDataProp: "source_address", sTitle: 'Source Address', sWidth: 100 },
                    { mDataProp: "ordersim_url", sTitle: 'Ordersim URL' },
                    { mDataProp: "msisdn_referrer", sTitle: 'MSISDN Referrer' },
                    { mDataProp: "fav_call_country", sTitle: 'Country you call most' },
                    //11-Jan-2016 : Moorthy : Added new column IP Address
                    { mDataProp: "ipaddress", sTitle: 'ÌP Address' }
                     //21-Jul-2016 : Moorthy : Added for landing & visit page url's
                    , { mDataProp: "landing_page_url", sTitle: 'Landing Page', sWidth: 100 }
                    , { mDataProp: "last_visit_page_url", sTitle: 'Visit Page', sWidth: 100 }
                ],
                fnCreatedRow: function (nRow, aData, iDataIndex) {
                    $(nRow).addClass("rowTableSOL");
                },
                fnDrawCallback: function () {
                    $("#SIMorderListPanel input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                    $("#TableSIMOrderList td a").css({ "text-decoration": "underline" });
                    $("#TableSIMOrderList td").css({ "font-size": "12px" });
                },
                fnInitComplete: function (ob) {
                    $("#TableSIMOrderList .row-fluid:eq(0) .span6").html("");
                    var searchResultStr = "Searching result : <b><span id=\"fnRecordsTotal\">" + ob.fnRecordsTotal() + "</span></b> order(s) found.";
                    $("#TableSIMOrderList_wrapper .span6:eq(0)").html("<div style=\"float:left\">" + searchResultStr + "</div>");

                    if (ob.fnRecordsTotal() <= 0) {
                        $("#TableSIMOrderList_wrapper .row-fluid:eq(1)").remove();
                    } else {
                        if (ob.fnRecordsTotal() <= ob._iDisplayLength) {
                            $("#TableSIMOrderList_wrapper .row-fluid:eq(1)").remove();
                        }
                        if ($("button#ExportToExcelBtn").length <= 0) {
                            var ExportToExcelBtn = "<button class=\"btn blue\" id=\"ExportToExcelBtn\" name=\"ExportToExcelBtn\" style=\"float:right;\">Download</button>";
                            $("#TableSIMOrderList_wrapper .row-fluid:eq(0) .span6:eq(1)").append(ExportToExcelBtn);
                        }
                    }
                    $("#TableSIMOrderList td").css({ "font-size": "12px" });
                    $("#TableSIMOrderList td a").css({ "text-decoration": "underline" });
                    $("#SIMorderListPanel input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                }
            }).fadeIn();
            $("#TableSIMOrderList_wrapper").css({ "overflow": "auto", "overflow-y": "hidden", "padding-bottom": "10px" });
            $("#TableSIMOrderList tbody td").css({ "font-size": "12px" });
        },
        error: function () {
            $("#ajax-screen-masking").hide();
        }
    });
}
function RefreshSIMOrderList(productCode) {
    $("table#TableSIMOrderList").html("");
    var siteCode = $("input[name=ExportSitecode]").val();
    var orderType = $("input[name=ExportOrderType]").val();
    var fromDate = $("input[name=ExportFromDate]").val();
    var toDate = $("input[name=ExportToDate]").val();
    var url = apiServer + '/api/simordersearch/';
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    jQuery.support.cors = true;
    var JSONSendData = {
        status: orderType,
        fromdate: fromDate,
        todate: toDate,
        sitecode: siteCode,
        product_code: productCode
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#TableSIMOrderListContainer").hide();
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            $("#TableSIMOrderListContainer").show();
            var lastAccessedPage = parseInt($("#SIMOrderListContainer input[name=accessedPage]").val());
            var oTable = $('#TableSIMOrderList').dataTable({
                bDestroy: true,
                aaData: feedback,
                iDisplayLength: 20,
                bPaginate: true,
                bFilter: false,
                bInfo: false,
                aaSorting: [[0, "asc"]],
                sPaginationType: "bootstrap",
                aoColumns: [

                    {
                        mDataProp: "sitecode", sTitle: 'Free SIM ID',
                        fnRender: function (ob) {
                            return "<center><a href=\"javascript:void(0)\" class=\"orderIDAnchor\" param-data=\"" + ob.aData.freesimid + "\" mimsi-data=\"" + ob.aData.mimsi_order_id + "\">" + ob.aData.freesimid + "</a></center>";
                        }
                    },
                    { mDataProp: "title", sTitle: 'Title' },
                    {
                        mDataProp: "firstname", sTitle: 'First Name',
                        fnRender: function (ob) {
                            return "<a href=\"javascript:void(0)\" class=\"firstnameAnchor\" param-data=\"" + ob.aData.freesimid + "\" mimsi-data=\"" + ob.aData.mimsi_order_id + "\">" + ob.aData.firstname + "</a>";
                        }
                    },
                    {
                        mDataProp: "lastname", sTitle: 'Last Name',
                        fnRender: function (ob) {
                            return "<a href=\"javascript:void(0)\" class=\"lastnameAnchor\" param-data=\"" + ob.aData.freesimid + "\" mimsi-data=\"" + ob.aData.mimsi_order_id + "\">" + ob.aData.lastname + "</a>";
                        }
                    },
                    { mDataProp: "freesimstatus", sTitle: 'Status' },
                    {
                        mDataProp: "registerdate", sTitle: 'Register Date',
                        fnRender: function (ob) {
                            return convertDate03(ob.aData.registerdate);
                        }
                    },
                    { mDataProp: "FreeSimType", sTitle: 'Free SIM Type	' },
                    { mDataProp: "CyberSourceId", sTitle: 'Cybersource ID' },
                    { mDataProp: "Subscriber_Type", sTitle: 'Subscriber Type' },
                    { mDataProp: "Quantity", sTitle: 'Quantity' },
                    { mDataProp: "SourceReg", sTitle: 'Source' },
                    { mDataProp: "source_address", sTitle: 'Source Address' },
                    { mDataProp: "ordersim_url", sTitle: 'Ordersim URL' },
                    { mDataProp: "msisdn_referrer", sTitle: 'MSISDN Referrer' },
                    { mDataProp: "fav_call_country", sTitle: 'Country you call most' }

                ],
                fnCreatedRow: function (nRow, aData, iDataIndex) {
                    $(nRow).addClass("rowTableSOL");
                },
                fnDrawCallback: function () {
                    $("#SIMorderListPanel input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                },
                fnInitComplete: function (ob) {
                    $("#TableSIMOrderList .row-fluid:eq(0) .span6").html("");
                    var orderType = $("select[name=orderType] option:selected").text();
                    var countryName = $("input[name=ResultCountry]").val();
                    var gProductCode = $("input[name=gProductCode]").val();
                    var searchResultStr = "Searching result the <b>" + orderType + "</b> in <b>" + countryName + "</b>.<br/>From " + convertDate01($("input#startdate").val()) + " to " + convertDate01($("input#enddate").val()) + ".<br/><b><span id=\"fnRecordsTotal\">" + ob.fnRecordsTotal() + "</span></b> order(s) found.";
                    $("#TableSIMOrderList_wrapper .span6:eq(0)").html("<div style=\"float:left\">" + searchResultStr + "</div>");

                    if (ob.fnRecordsTotal() <= 0) {
                        $("#TableSIMOrderList_wrapper .row-fluid:eq(1)").remove();
                    } else {
                        if (ob.fnRecordsTotal() <= ob._iDisplayLength) {
                            $("#TableSIMOrderList_wrapper .row-fluid:eq(1)").remove();
                        }
                        if ($("button#ExportToExcelBtn").length <= 0) {
                            var ExportToExcelBtn = "<button class=\"btn blue\" id=\"ExportToExcelBtn\" name=\"ExportToExcelBtn\" style=\"float:right;margin-top:20px\">Download</button>";
                            $("#TableSIMOrderList_wrapper .row-fluid:eq(0) .span6:eq(1)").append(ExportToExcelBtn);
                        }
                    }

                    $("#TableSIMOrderList td").css({ "font-size": "12px" });
                    $("#TableSIMOrderList td a").css({ "text-decoration": "underline" });
                    $("#SIMorderListPanel input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                }
            }).fadeIn();
            $("#TableSIMOrderList tbody td").css({ "font-size": "12px" });
            $("#TableSIMOrderList_wrapper").css({ "overflow": "auto", "overflow-y": "hidden", "padding-bottom": "10px" });
            $("#SIMorderListPanel input[name=accessedPage]").val(lastAccessedPage);
            oTable.fnPageChange(lastAccessedPage);
        },
        error: function () {
            $("#ajax-screen-masking").hide();
        }
    });
}

function RefreshSIMOrderSearch() {
    $("table#TableSIMOrderList").html("");

    var siteCode = $("input[name=ExportSitecode]").val();
    var searchBy = $("input[name=ExportSearchBy]").val();
    var searchValue = $("input[name=ExportSearchValue]").val();
    var productCode = $("input[name=ExportProduct]").val();

    var url = apiServer + '/api/simordersearch/';
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    jQuery.support.cors = true;
    var JSONSendData = {
        sitecode: siteCode,
        searchby: searchBy,
        searchvalue: searchValue,
        product_code: productCode
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#TableSIMOrderListContainer").hide();
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            $("#TableSIMOrderListContainer").show();
            $('#TableSIMOrderList').dataTable({
                bDestroy: true,
                aaData: feedback,
                iDisplayLength: 20,
                bPaginate: true,
                bFilter: false,
                bInfo: false,
                aaSorting: [[0, "asc"]],
                sPaginationType: "bootstrap",
                aoColumns: [

                    {
                        mDataProp: "sitecode", sTitle: 'Free SIM ID',
                        fnRender: function (ob) {
                            return "<center><a href=\"javascript:void(0)\" class=\"orderIDAnchor\" param-data=\"" + ob.aData.freesimid + "\" mimsi-data=\"" + ob.aData.mimsi_order_id + "\">" + ob.aData.freesimid + "</a></center>";
                        }
                    },
                    { mDataProp: "title", sTitle: 'Title' },
                    {
                        mDataProp: "firstname", sTitle: 'First Name',
                        fnRender: function (ob) {
                            return "<a href=\"javascript:void(0)\" class=\"firstnameAnchor\" param-data=\"" + ob.aData.freesimid + "\" mimsi-data=\"" + ob.aData.mimsi_order_id + "\">" + ob.aData.firstname + "</a>";
                        }
                    },
                    {
                        mDataProp: "lastname", sTitle: 'Last Name',
                        fnRender: function (ob) {
                            return "<a href=\"javascript:void(0)\" class=\"lastnameAnchor\" param-data=\"" + ob.aData.freesimid + "\" mimsi-data=\"" + ob.aData.mimsi_order_id + "\">" + ob.aData.lastname + "</a>";
                        }
                    },
                    { mDataProp: "freesimstatus", sTitle: 'Status' },
                    {
                        mDataProp: "registerdate", sTitle: 'Register Date',
                        fnRender: function (ob) {
                            return convertDate03(ob.aData.registerdate);
                        }
                    },
                    { mDataProp: "FreeSimType", sTitle: 'Free SIM Type	' },
                    { mDataProp: "CyberSourceId", sTitle: 'Cybersource ID' },
                    { mDataProp: "Subscriber_Type", sTitle: 'Subscriber Type' },
                    { mDataProp: "Quantity", sTitle: 'Quantity' },
                    { mDataProp: "SourceReg", sTitle: 'Source' },
                    { mDataProp: "source_address", sTitle: 'Source Address' },
                    { mDataProp: "ordersim_url", sTitle: 'Ordersim URL' },
                    { mDataProp: "msisdn_referrer", sTitle: 'MSISDN Referrer' },
                    { mDataProp: "fav_call_country", sTitle: 'Country you call most' }

                ],
                fnCreatedRow: function (nRow, aData, iDataIndex) {
                    $(nRow).addClass("rowTableSOL");
                },
                fnDrawCallback: function () {
                    $("#SIMorderListPanel input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                },
                fnInitComplete: function (ob) {
                    $("#TableSIMOrderList .row-fluid:eq(0) .span6").html("");
                    var searchResultStr = "Searching result : <b><span id=\"fnRecordsTotal\">" + ob.fnRecordsTotal() + "</span></b> order(s) found.";
                    $("#TableSIMOrderList_wrapper .span6:eq(0)").html("<div style=\"float:left\">" + searchResultStr + "</div>");

                    if (ob.fnRecordsTotal() <= 0) {
                        $("#TableSIMOrderList_wrapper .row-fluid:eq(1)").remove();
                    } else {
                        if (ob.fnRecordsTotal() <= ob._iDisplayLength) {
                            $("#TableSIMOrderList_wrapper .row-fluid:eq(1)").remove();
                        }
                        if ($("button#ExportToExcelBtn").length <= 0) {
                            var ExportToExcelBtn = "<button class=\"btn blue\" id=\"ExportToExcelBtn\" name=\"ExportToExcelBtn\" style=\"float:right;\">Download</button>";
                            $("#TableSIMOrderList_wrapper .row-fluid:eq(0) .span6:eq(1)").append(ExportToExcelBtn);
                        }
                    }

                    $("#TableSIMOrderList td").css({ "font-size": "12px" });
                    $("#TableSIMOrderList td a").css({ "text-decoration": "underline" });
                    $("#SIMorderListPanel input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                }
            }).fadeIn();
            $("#TableSIMOrderList_wrapper").css({ "overflow": "auto", "overflow-y": "hidden", "padding-bottom": "10px" });
            $("#TableSIMOrderList tbody td").css({ "font-size": "12px" });
        },
        error: function () {
            $("#ajax-screen-masking").hide();
        }
    });
}

//function SIM2in1Detail(orderID) {
//    var ajaxSpinnerTop = ($(window).outerHeight() / 2) + $(window).scrollTop();

//    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
//    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
//    $("#orderDetail #SIMOrderDetailError").html("").hide();
//    $("#orderDetail input, #orderDetail select").val("").removeAttr("disabled");
//    var url = apiServer + '/api/simorder/';
//    jQuery.support.cors = true;
//    var JSONSendData = {
//        action_type: 17,
//        search_text: "",
//        fromdate: "",
//        todate: "",
//        freesim_order_id: orderID
//    };

//    $.ajax
//    ({
//        url: url,
//        type: 'post',
//        data: JSON.stringify(JSONSendData),
//        dataType: 'json',
//        contentType: "application/json;charset=utf-8",
//        cache: false,
//        beforeSend: function () {
//            $("#ajax-screen-masking").show();
//        },
//        success: function (feedback) {
//            $("#ajax-screen-masking").hide();

//            $('table.tblOrderDetail #tdODTitle').html(feedback.title);
//            $('table.tblOrderDetail #tdODFirstName').html(feedback.first_name);
//            $('table.tblOrderDetail #tdODLastName').html(feedback.last_name);
//            $('table.tblOrderDetail #tdODHouseNo').html(feedback.address1);
//            $('table.tblOrderDetail #tdODAddress').html(feedback.address2);
//            $('table.tblOrderDetail #tdODCity').html(feedback.town);
//            $('table.tblOrderDetail #tdODPostcode').html(feedback.postcode);
//            $('table.tblOrderDetail #tdODMobilePhone').html(feedback.mobileno);
//            $('table.tblOrderDetail #tdODEmail').html(feedback.email);
//            $('table.tblOrderDetail #tdODStatus').html(feedback.order_status);
//            $('table.tblOrderDetail #tdODQty').html(feedback.quantity);
//            $('table.tblOrderDetail #tdODSourceAddress').html(feedback.order_source);
//            $('table.tblOrderDetail #tdODOrderSIMURL').html(feedback.ordersim_url);
//            $('table.tblOrderDetail #tdODBirthdate').html(convertDateISOCustom(feedback.Birthdate, 0).substring(0, 10));
//            $('table.tblOrderDetail #tdODRegDate').html(feedback.order_date_string);
//            $('table.tblOrderDetail #tdODSentDate').html(convertDateISOCustom(feedback.UpdateDate, 0));
//            $('table.tblOrderDetail #tdODActivateDate').html(convertDateISOCustom(feedback.UpdateDate, 0));
//            $('table.tblOrderDetail #input_iccid').val(feedback.iccid);

//            var RoyalMailRefrence = convertNulltoEmptyString(feedback.royal_mail_reference);
//            if (RoyalMailRefrence == '') {
//                $('table.tblOrderDetail input[name=input_mailref]').val('').removeAttr('readonly disabled');
//            } else {
//                $('table.tblOrderDetail input[name=input_mailref]').val(RoyalMailRefrence).attr({ 'readonly': true, 'disabled': true });
//            }

//            var QTYOrder = parseInt(feedback.quantity);
//            $("input[name=QTYOrder]").val(QTYOrder);

//            $('#orderDetail').modal({ keyboard: false, backdrop: 'static' }).css({
//                'margin-left': function () {
//                    return window.pageXOffset - ($(this).width() / 2);
//                }
//            });

//            $("span#prefix_89_1, span#prefix_89_2").hide();
//            $("input[name=input_iccid], input[name=input_iccid2]").css({ "width": "205px" });

//            if (QTYOrder > 1) {
//                $("input[name=input_iccid], input[name=input_iccid2]").hide();
//                $("select[name=optICCIDList]").html("").show();
//                var url2 = apiServer + '/api/SIMOrderSearch/3?val=' + orderID + "&Sitecode=" + Sitecode;
//                $.getJSON(url2, function (feedback2) {
//                    var arrFreeSIMID = "";
//                    var arrICCID = "";
//                    var arrMIMSIOrderID = "";

//                    for (var idx in feedback2) {
//                        if (convertNulltoEmptyString(feedback2[idx].freesim_order_id) != "") {
//                            arrFreeSIMID += feedback2[idx].freesim_order_id + "|";
//                        }
//                        if (convertNulltoEmptyString(feedback2[idx].iccid) != "") {
//                            arrICCID += feedback2[idx].iccid + "|";
//                        }
//                        if (convertNulltoEmptyString(feedback2[idx].mimsi_order_id) != "") {
//                            arrMIMSIOrderID += feedback2[idx].mimsi_order_id + "|";
//                        }
//                    }
//                    arrFreeSIMID = arrFreeSIMID.replace(/\|$/, '');
//                    arrICCID = arrICCID.replace(/\|$/, '');
//                    arrMIMSIOrderID = arrMIMSIOrderID.replace(/\|$/, '');
//                    $("input[name=arrFreeSIMID]").val(arrFreeSIMID);
//                    $("input[name=arrICCID]").val(arrICCID);
//                    $("input[name=arrMIMSIOrderID]").val(arrMIMSIOrderID);

//                    if (arrICCID != "") {
//                        $("tr#trODICCID2, select[name=optICCIDList2]").show();
//                        $("button#btnODOpenICCIDList1").remove();
//                        $("select[name=optICCIDList]").css({ "width": "260px", "font-size": "14px" });

//                        $("button#btnODOpenICCIDList2").remove();
//                        $("#tdODICCID2").append("<button class=\"btn blue\" id=\"btnODOpenICCIDList2\" style=\"width:105px;margin-left:5px;height:28px;margin-top:-9px;padding:0;\">Add ICCID No</button>");

//                        $("table#tblICCIDList1, table#tblICCIDList2").html("");
//                        $("table#tblICCIDList1 input").attr("disabled", true);
//                        var ICCIDListNo = "";
//                        var splitICCID = arrICCID.split("|");
//                        if (splitICCID.length) {
//                            for (var i = 0; i < splitICCID.length; i++) {
//                                ICCIDListNo += "<option value=\"" + splitICCID[i] + "\">" + splitICCID[i] + "</option>";
//                            }
//                            $("select[name=optICCIDList]").attr("readonly", true).html("").append(ICCIDListNo);
//                        }
//                        $("table#tblICCIDList2").append("<tr id=\"trErrorICCIDList2\"><td colspan=\"2\" id=\"tdErrorICCIDList2\"></td></tr>");
//                        for (var i = 0; i < QTYOrder; i++) {
//                            $("table#tblICCIDList2").append("<tr><td style=\"width:60px\">ICCID " + (i + 1) + "&nbsp;:</td><td><span style=\"font-size:14px;color:#505050;border:1px solid #c0c0c0;background-color:#eeeeee;margin-right:2px;padding:5px 2px;\">89</span><input style=\"margin:0;margin-bottom:3px;width:213px;\" class=\"iccid_list_1\" type=\"text\" id=\"input_iccid2_" + (i + 1) + "\" name=\"input_iccid2_" + (i + 1) + "\"/><input class=\"iccid_list_h1\" type=\"hidden\" /></td></tr>");
//                            $("table#tblICCIDList2 input").attr("disabled", true);
//                        }
//                        $("input[type=text].iccid_list_1").eq(0).removeAttr("disabled");

//                    } else {
//                        $("tr#trODICCID2").hide();
//                        $("button#btnODOpenICCIDList1").remove();
//                        $("select[name=optICCIDList]").css({ "width": "180px", "font-size": "12px" }).removeAttr("readonly");
//                        $("#tdODICCID").append("<button class=\"btn blue\" id=\"btnODOpenICCIDList1\" style=\"width:105px;margin-left:5px;height:28px;margin-top:-9px;padding:0;\">Add ICCID No</button>");
//                        $("table#tblICCIDList1").html("");
//                        $("table#tblICCIDList1").append("<tr id=\"trErrorICCIDList1\"><td colspan=\"2\" id=\"tdErrorICCIDList1\"></td></tr>");
//                        for (var i = 0; i < QTYOrder; i++) {
//                            $("table#tblICCIDList1").append("<tr><td style=\"width:60px\">ICCID " + (i + 1) + "&nbsp;:</td><td><span style=\"font-size:14px;color:#505050;border:1px solid #c0c0c0;background-color:#eeeeee;margin-right:2px;padding:5px 2px;\">89</span><input style=\"margin:0;margin-bottom:3px;width:213px;\" class=\"iccid_list_0\" type=\"text\" id=\"input_iccid1_" + (i + 1) + "\" name=\"input_iccid1_" + (i + 1) + "\"/><input class=\"iccid_list_h0\" type=\"hidden\" /></td></tr>");
//                            $("table#tblICCIDList1 input").attr("disabled", true);
//                        }
//                        $("input[type=text].iccid_list_0").eq(0).removeAttr("disabled");
//                    }
//                });
//            } else {
//                $("button#btnODOpenICCIDList1, button#btnODOpenICCIDList2").remove();
//                $("table#tblICCIDList1, table#tblICCIDList2").html("");
//                $("input[name=input_iccid]").show();
//                $("input[name=input_iccid2], select[name=optICCIDList], select[name=optICCIDList2]").hide();

//                var arrFreeSIMID = "";
//                if (feedback.freesim_order_id != "") {
//                    arrFreeSIMID = convertNulltoEmptyString(feedback.freesim_order_id);
//                }
//                var arrICCID = convertNulltoEmptyString(feedback.iccid);
//                var arrMIMSIOrderID = convertNulltoEmptyString(feedback.mimsi_order_id);

//                $("input[name=arrFreeSIMID]").val(arrFreeSIMID);
//                $("input[name=arrICCID]").val(arrICCID);
//                $("input[name=arrMIMSIOrderID]").val(arrMIMSIOrderID);

//                if (arrICCID != "") {
//                    $("input[name=input_iccid]").val(arrICCID).attr("readonly", true);
//                    $("tr#trODICCID2").show();
//                    $("span#prefix_89_2").show();
//                    $("input[name=input_iccid2]").css({ "width": "180px" }).show().focus();
//                } else {
//                    $("span#prefix_89_1").show();
//                    $("input[name=input_iccid]").css({ "width": "180px" }).val("").removeAttr("readonly");
//                    $("input[name=input_iccid]").focus();
//                    $("tr#trODICCID2").hide();
//                }
//            }
//        },
//        error: function () {
//            $("#ajax-screen-masking").hide();
//        }
//    });
//}

function SIMOrderDetail(orderID, Sitecode) {
    var ajaxSpinnerTop = ($(window).outerHeight() / 2) + $(window).scrollTop();
    $("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    $("#orderDetail #SIMOrderDetailError").html("").hide();
    $("#orderDetail input, #orderDetail select").val("").removeAttr("disabled");
    var url = apiServer + '/api/SIMOrderSearch/2?val=' + orderID + "&Sitecode=" + Sitecode;
    $.ajax
    ({
        url: url,
        type: 'get',
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
            //$("table.tblOrderDetail:eq(1) tr#trODMailRef").remove();
        },
        success: function (feedback) {
            $("#ajax-screen-masking").hide();

            $('table.tblOrderDetail #tdODTitle').html(feedback.title);
            $('table.tblOrderDetail #tdODFirstName').html(feedback.firstname);
            $('table.tblOrderDetail #tdODLastName').html(feedback.lastname);
            if (feedback.mimsi_order_id > 0) {
                $('table.tblOrderDetail #tdODHouseNo').html(feedback.Address1);
                $('table.tblOrderDetail #tdODAddress').html(feedback.Address2);
                $('table.tblOrderDetail #tdODAddress1').html(feedback.Address1);
                $('table.tblOrderDetail #tdODAddress2').html(feedback.Address2);
                $('table.tblOrderDetail #tdODAddress3').html(feedback.Address3);
            } else {
                $('table.tblOrderDetail #tdODHouseNo').html(feedback.houseno);
                $('table.tblOrderDetail #tdODAddress').html(feedback.Address);
                $('table.tblOrderDetail #tdODAddress1').html(feedback.Address1);
                $('table.tblOrderDetail #tdODAddress2').html(feedback.Address2);
                $('table.tblOrderDetail #tdODAddress3').html(feedback.Address3);
            }
            if (Sitecode == 'MCM') {

                $('table.tblOrderDetail #trAddress1').show();
                $('table.tblOrderDetail #trAddress2').show();
                $('table.tblOrderDetail #trAddress3').show();


                $('table.tblOrderDetail #trHouseno').hide();
                $('table.tblOrderDetail #trAddress').hide();

            }
            else {
                $('table.tblOrderDetail #trAddress1').hide();
                $('table.tblOrderDetail #trAddress2').hide();
                $('table.tblOrderDetail #trAddress3').hide();


                $('table.tblOrderDetail #trHouseno').show();
                $('table.tblOrderDetail #trAddress').show();
            }
            $('table.tblOrderDetail #tdODCity').html(feedback.city);
            if (feedback.postcode != null)
                $('table.tblOrderDetail #tdODPostcode').html(feedback.postcode.toUpperCase());
            else
                $('table.tblOrderDetail #tdODPostcode').html("");
            $('table.tblOrderDetail #tdODTelp').html(feedback.telephone);
            $('table.tblOrderDetail #tdODMobilePhone').html(feedback.mobilephone);
            $('table.tblOrderDetail #tdODEmail').html(feedback.email);
            $('table.tblOrderDetail #tdODBirthdate').html(convertDateISO8601(feedback.birthdate, 0));
            $('table.tblOrderDetail #tdOfavcallcountry').html(feedback.fav_call_country);
            $('table.tblOrderDetail #tdODStatus').html(feedback.freesimstatus);
            $('table.tblOrderDetail #tdODQty').html(feedback.Quantity);
            $('table.tblOrderDetail #tdODSourceAddress').html(feedback.source_address);
            $('table.tblOrderDetail #tdODOrderSIMURL').html(feedback.ordersim_url);
            $('table.tblOrderDetail #tdODRegDate').html(convertDateISO8601(feedback.registerdate, 0));
            $('table.tblOrderDetail #tdODSentDate').html(convertDateISO8601(feedback.sentdate, 0));
            //11-Dec-2017 : Added to display Replacement Date
            $('table.tblOrderDetail #tdODReplacementDate').html(convertDateISO8601(feedback.replacement_date, 0));

            $('table.tblOrderDetail #tdODActivateDate').html(convertDateISO8601(feedback.activatedate, 0));

            var RoyalMailRefrence = convertNulltoEmptyString(feedback.royal_mail_reference);
            if (RoyalMailRefrence == '') {
                $('table.tblOrderDetail input[name=input_mailref]').val('').removeAttr('readonly disabled');
            } else {
                $('table.tblOrderDetail input[name=input_mailref]').val(RoyalMailRefrence).attr({ 'readonly': true, 'disabled': true });
            }
            var SubscriberType = convertNulltoEmptyString(feedback.Subscriber_Type);
            //var SubscriberType = convertNulltoEmptyString(feedback.Subscriber_Type);
            //var arrayAllowedSubscriberType = new Array("NanoSIM", "POSTPAID RegularSIM", "POSTPAID MicroSIM", "PaidSIM");
            //if (inArray(SubscriberType, arrayAllowedSubscriberType)) {
            //    var mailRef = "";
            //    mailRef += "<tr id=\"trODMailRef\"><td>Mail Reference</td><td id=\"tdODMailRef\" >";
            //    mailRef += "<input type=\"text\" id=\"input_mailref\" name=\"input_mailref\"/>";
            //    mailRef += "</td></tr>";
            //    $("table.tblOrderDetail:eq(1) tbody").append(mailRef);
            //}

            var QTYOrder = parseInt(feedback.Quantity);
            $("input[name=QTYOrder]").val(QTYOrder);

            $('#orderDetail').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });

            $("span#prefix_89_1, span#prefix_89_2").hide();
            $("input[name=input_iccid], input[name=input_iccid2]").css({ "width": "205px" });

            if (QTYOrder > 1) {
                $("input[name=input_iccid], input[name=input_iccid2]").hide();
                $("select[name=optICCIDList]").html("").show();
                var url2 = apiServer + '/api/SIMOrderSearch/3?val=' + orderID + "&Sitecode=" + Sitecode;
                $.getJSON(url2, function (feedback2) {
                    var arrFreeSIMID = "";
                    var arrICCID = "";
                    var arrMIMSIOrderID = "";

                    /******************* Email sending SIM dispatch **************/
                    var firstname = "";
                    //   var lastname = "";
                    var email = "";
                    /******************* Email sending  SIM dispatch **************/
                    for (var idx in feedback2) {
                        if (convertNulltoEmptyString(feedback2[idx].freesimid) != "") {
                            arrFreeSIMID += feedback2[idx].freesimid + "|";
                        }
                        if (convertNulltoEmptyString(feedback2[idx].iccid) != "") {
                            arrICCID += feedback2[idx].iccid + "|";
                        }
                        if (convertNulltoEmptyString(feedback2[idx].mimsi_order_id) != "") {
                            arrMIMSIOrderID += feedback2[idx].mimsi_order_id + "|";
                        }
                        /******************* Email sending  SIMdispatch **************/
                        if (convertNulltoEmptyString(feedback2[idx].firstname) != "") {
                            firstname += feedback2[idx].firstname + "|";
                        }
                        //if (convertNulltoEmptyString(feedback2[idx].lastname) != "") {
                        //    lastname += feedback2[idx].lastname + "|";
                        //}
                        if (convertNulltoEmptyString(feedback2[idx].email) != "") {
                            email += feedback2[idx].email + "|";
                        }
                        /******************* Email sending  SIM dispatch **************/
                    }
                    arrFreeSIMID = arrFreeSIMID.replace(/\|$/, '');
                    arrICCID = arrICCID.replace(/\|$/, '');
                    arrMIMSIOrderID = arrMIMSIOrderID.replace(/\|$/, '');

                    /******************* Email sending  SIM dispatch **************/
                    firstname = firstname.replace(/\|$/, '');
                    // lastname = lastname.replace(/\|$/, '');
                    email = email.replace(/\|$/, '');
                    /******************* Email sending  SIM dispatch **************/
                    $("input[name=arrFreeSIMID]").val(arrFreeSIMID);
                    $("input[name=arrICCID]").val(arrICCID);
                    $("input[name=arrMIMSIOrderID]").val(arrMIMSIOrderID);
                    $("#hdSubscriberType").val(feedback.Subscriber_Type);
                    /******************* Email sending  SIM dispatch **************/
                    $("input[name=arrFirstName]").val(firstname);
                    // $("input[name=arrLasttName]").val(lastname);
                    $("input[name=arrEmailtName]").val(email);
                    /******************* Email sending  SIM dispatch **************/
                    if (arrICCID != "") {
                        $("tr#trODICCID2, select[name=optICCIDList2]").show();
                        $("button#btnODOpenICCIDList2").remove();
                        $("select[name=optICCIDList]").css({ "width": "260px", "font-size": "14px" });
                        $("#tdODICCID2").append("<button class=\"btn blue\" id=\"btnODOpenICCIDList2\" style=\"width:105px;margin-left:5px;height:28px;margin-top:-9px;padding:0;\">Add ICCID No</button>");

                        $("table#tblICCIDList1, table#tblICCIDList2").html("");
                        $("table#tblICCIDList1 input").attr("disabled", true);
                        var ICCIDListNo = "";
                        var splitICCID = arrICCID.split("|");
                        if (splitICCID.length) {
                            for (var i = 0; i < splitICCID.length; i++) {
                                ICCIDListNo += "<option value=\"" + splitICCID[i] + "\">" + splitICCID[i] + "</option>";
                            }
                            $("select[name=optICCIDList]").attr("readonly", true).html("").append(ICCIDListNo);
                        }
                        $("table#tblICCIDList2").append("<tr id=\"trErrorICCIDList2\"><td colspan=\"2\" id=\"tdErrorICCIDList2\"></td></tr>");
                        for (var i = 0; i < QTYOrder; i++) {
                            $("table#tblICCIDList2").append("<tr><td style=\"width:60px\">ICCID " + (i + 1) + "&nbsp;:</td><td><span style=\"font-size:14px;color:#505050;border:1px solid #c0c0c0;background-color:#eeeeee;margin-right:2px;padding:5px 2px;\">89</span><input style=\"margin:0;margin-bottom:3px;width:213px;\" class=\"iccid_list_1\" type=\"text\" id=\"input_iccid2_" + (i + 1) + "\" name=\"input_iccid2_" + (i + 1) + "\"/><input class=\"iccid_list_h1\" type=\"hidden\" /></td></tr>");
                            $("table#tblICCIDList2 input").attr("disabled", true);
                        }

                        $("input[type=text].iccid_list_1").eq(0).removeAttr("disabled");

                    } else {
                        $("tr#trODICCID2").hide();
                        $("button#btnODOpenICCIDList1").remove();
                        $("select[name=optICCIDList]").css({ "width": "180px", "font-size": "12px" }).removeAttr("readonly");
                        $("#tdODICCID").append("<button class=\"btn blue\" id=\"btnODOpenICCIDList1\" style=\"width:105px;margin-left:5px;height:28px;margin-top:-9px;padding:0;\">Add ICCID No</button>");
                        $("table#tblICCIDList1").html("");
                        $("table#tblICCIDList1").append("<tr id=\"trErrorICCIDList1\"><td colspan=\"2\" id=\"tdErrorICCIDList1\"></td></tr>");
                        for (var i = 0; i < QTYOrder; i++) {
                            if (SubscriberType == 'TELE SALES') {
                                $("table#tblICCIDList1").append("<tr><td style=\"width:60px\">ICCID " + (i + 1) + "&nbsp;:</td><td><input maxlength=\"19\" style=\"margin:0;margin-bottom:3px;margin-top:-1px;width:213px;\" class=\"iccid_list_0\" type=\"text\" id=\"input_iccid1_" + (i + 1) + "\" name=\"input_iccid1_" + (i + 1) + "\"/><input class=\"iccid_list_h0\" type=\"hidden\" /></td></tr>");
                                $("table#tblICCIDList1 input").attr("disabled", true);
                            }
                            else {
                                $("table#tblICCIDList1").append("<tr><td style=\"width:60px\">ICCID " + (i + 1) + "&nbsp;:</td><td><span style=\"font-size:14px;color:#505050;border:1px solid #c0c0c0;background-color:#eeeeee;margin-right:2px;padding:5px 2px;\">89</span><input maxlength=\"17\" style=\"margin:0;margin-bottom:3px;margin-top:-1px;width:213px;\" class=\"iccid_list_0\" type=\"text\" id=\"input_iccid1_" + (i + 1) + "\" name=\"input_iccid1_" + (i + 1) + "\"/><input class=\"iccid_list_h0\" type=\"hidden\" /></td></tr>");
                                $("table#tblICCIDList1 input").attr("disabled", true);
                            }
                        }
                        $("input[type=text].iccid_list_0").eq(0).removeAttr("disabled");
                    }
                });
            } else {

                $("button#btnODOpenICCIDList1, button#btnODOpenICCIDList2").remove();
                $("table#tblICCIDList1, table#tblICCIDList2").html("");
                $("input[name=input_iccid]").show();
                $("input[name=input_iccid2], select[name=optICCIDList], select[name=optICCIDList2]").hide();
                var arrFreeSIMID = convertNulltoEmptyString(feedback.freesimid);
                var arrICCID = convertNulltoEmptyString(feedback.iccid);
                var arrMIMSIOrderID = convertNulltoEmptyString(feedback.mimsi_order_id);

                /******************* Email sending  SIM dispatch **************/
                var firstname = convertNulltoEmptyString(feedback.firstname);
                //var lastname = convertNulltoEmptyString(feedback.lastname);
                var email = convertNulltoEmptyString(feedback.email);
                /******************* Email sending  SIM dispatch **************/
                $("input[name=arrFreeSIMID]").val(arrFreeSIMID);
                $("input[name=arrICCID]").val(arrICCID);
                $("input[name=arrMIMSIOrderID]").val(arrMIMSIOrderID);

                /******************* Email sending  SIM dispatch **************/
                $("input[name=arrFirstName]").val(firstname);
                //$("input[name=arrLasttName]").val(lastname);
                $("input[name=arrEmailtName]").val(email);
                /******************* Email sending  SIM dispatch **************/
                if (arrICCID != "") {
                    $("input[name=input_iccid]").val(arrICCID).attr({ "readonly": true, "maxlength": "19" });
                    $("tr#trODICCID2").show();
                    $("span#prefix_89_2").show();
                    $("input[name=input_iccid2]").css({ "width": "180px" }).show().focus();
                } else {
                    $("span#prefix_89_1").show();
                    $("input[name=input_iccid]").css({ "width": "180px" }).val("").removeAttr("readonly").attr({ "maxlength": "17" });
                    $("input[name=input_iccid]").focus();
                    $("tr#trODICCID2").hide();
                }
            }

        },
        error: function () {
            $("#ajax-screen-masking").hide();
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : UpdateOrder
 * Purpose          : to update the free SIM order detail
 * Added by         : Edi Suryadi
 * Create Date      : October 21th, 2013
 * Last Update      : November 28th, 2013
 * Update History   : - Optimizing code for batching order update(28/Oct/2013)
 * ---------------------------------------------------------------- */
function UpdateOrder(arrSettings) {
    var url = apiServer + '/api/SIMOrderSearch';
    jQuery.support.cors = true;
    $.ajax({
        url: url,
        type: 'put',
        data: JSON.stringify(arrSettings),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        async: false,
        timeout: 50000,
        success: function (data) {
            if (data.errcode == "0") {
                $("td.currentDispatchStatus").html("<span class=\"alert-success\">Success</span>");
            } else {
                $("td.currentDispatchStatus").html("<span class=\"alert-error\">Failed</span>");
                var errMessage = "";
                errMessage += "&emsp;<span class=\"btnDPViewError\" style=\"cursor:pointer\"><i class=\"icon-search\"></i></span>";
                errMessage += "<div style=\"position: absolute;z-index:9999;display:none;width:0\" class=\"DPDetailError\">";
                errMessage += "<div style=\"float:left;top:-31px;left:-203px;width:250px;position:relative;padding: 10px;background: #f2dede;border-radius: 4px !important;color: #b94a48;border:1px solid #eed3d7;\">";
                errMessage += "<span>" + data.errmsg + "</span>";
                errMessage += "<div style=\"border-left: 5px solid #b94a48;left:271px;top:4px\" class=\"arrow-right\"></div>";
                errMessage += "</div></div>";
                $("td.currentDispatchStatus").append(errMessage);
            }
            $("td.currentDispatchStatus").removeClass("currentDispatchStatus");
        }
    });

}

$.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
    return {
        "iStart": oSettings._iDisplayStart,
        "iEnd": oSettings.fnDisplayEnd(),
        "iLength": oSettings._iDisplayLength,
        "iTotal": oSettings.fnRecordsTotal(),
        "iFilteredTotal": oSettings.fnRecordsDisplay(),
        "iPage": oSettings._iDisplayLength === -1 ?
			0 : Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
        "iTotalPages": oSettings._iDisplayLength === -1 ?
			0 : Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
    };
};

/* ----------------------------------------------------- 
   *  eof SIM ORDER LIST 
   ===================================================== */



