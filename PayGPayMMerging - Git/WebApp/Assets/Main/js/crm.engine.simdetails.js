﻿/* ===================================================== 
   *  TAB SIM DETAILS 
   ----------------------------------------------------- */

/* ----------------------------------------------------------------
 * Function Name    : SIMDetailsInfo
 * Purpose          : to show SIM Details Information
 * Added by         : Edi Suryadi
 * Create Date      : September 04th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function SIMDetailsInfo(mobileno, ICCID, Sitecode) {
    var url = apiServer + '/api/sim';
    jQuery.support.cors = true;
    var JSONSendData = {
        Msisdn: mobileno,
        Iccid: ICCID,
        Sitecode: Sitecode,
        InfoType: 1
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            var errcode = -1;
            if (feedback != '' && feedback != null) {
                errcode = feedback.errcode;
            }
            if (errcode == 0) {
                $("table#tblCustomerSIMDetails td#tdCSDMobileNo").html(feedback.msisdn);
                $("table#tblCustomerSIMDetails td#tdSimStatus").html(feedback.strstatus);
                $("table#tblCustomerSIMDetails td#tdSimReason").html(feedback.reasoninfo.reason);
                $("table#tblCustomerSIMDetails td#tdSimVersion").html(feedback.SIMVersion.sim_Version);
                $("table#tblCustomerSIMDetails td#tdCSDICCID").html(feedback.iccid);
                $("table#tblCustomerSIMDetails td#tdCSDIMSIActive").html(feedback.activeimsi);
                $("table#tblCustomerSIMDetails td#tdCSDIMSIMaster").html(feedback.masterimsi);
                $("table#tblCustomerSIMDetails td#tdCSDSitecode").html(feedback.sitecode);
                $("table#tblCustomerSIMDetails td#tdCSDMNRF").html(convertNulltoNA(feedback.mnrf) + " / " + convertNulltoNA(feedback.mcef));
                //$("table#tblCustomerSIMDetails td#tdCSDFirstUpdate").html(feedback.firstupd_string);
                //$("table#tblCustomerSIMDetails td#tdCSDLastUpdate").html(feedback.lastupde_string);
                //Added by karthik Jira:197
                var Subfirstupd_string = feedback.firstupd_string.substring(0, 10);
                var SubLastUpd_string = feedback.lastupde_string.substring(0, 10);

                if (Subfirstupd_string == "01-01-0001" || Subfirstupd_string == "01-01-1900") {
                    $("table#tblCustomerSIMDetails td#tdCSDFirstUpdate").html("");
                }
                else {
                    $("table#tblCustomerSIMDetails td#tdCSDFirstUpdate").html(feedback.firstupd_string);
                }

                if (SubLastUpd_string == "01-01-0001" || SubLastUpd_string == "01-01-1900") {
                    $("table#tblCustomerSIMDetails td#tdCSDLastUpdate").html("");
                }
                else {
                    $("table#tblCustomerSIMDetails td#tdCSDLastUpdate").html(feedback.lastupde_string);
                }

                //26Oct2018 : Anees Modified to displaying empty when date is 01-01-0001 or 01-01-1900
                var Suboldfirstupd_string = feedback.oldsim_firstupd_string.substring(0, 10);
                var SuboldLastUpd_string = feedback.oldsim_lastupd_lastupd.substring(0, 10);

                if (Suboldfirstupd_string == "01-01-0001" || Suboldfirstupd_string == "01-01-1900") {
                    $("table#tblCustomerSIMDetails td#tdCSDOSFFirstUpdate").html("");
                }
                else {
                    $("table#tblCustomerSIMDetails td#tdCSDOSFFirstUpdate").html(feedback.oldsim_firstupd_string);
                }

                if (SuboldLastUpd_string == "01-01-0001" || SuboldLastUpd_string == "01-01-1900") {
                    $("table#tblCustomerSIMDetails td#tdCSDOSFLastUpdate").html("");
                }
                else {
                    $("table#tblCustomerSIMDetails td#tdCSDOSFLastUpdate").html(feedback.oldsim_lastupd_lastupd);
                }

                $("table#tblCustomerSIMDetails td#tdCSDCurrentVRL").html(feedback.vlr);
                $("table#tblCustomerSIMDetails td#tdCSDPIN1").html(convertNulltoNA(feedback.pinInfo.pin1));
                $("table#tblCustomerSIMDetails td#tdCSDPUK1").html(convertNulltoNA(feedback.pinInfo.puk1));
                $("table#tblCustomerSIMDetails td#tdCSDCurrentLocation").html(convertNulltoNA(feedback.current_loc));
                $("table#tblCustomerSIMDetails td#tdCSDLastLocation").html(convertNulltoNA(feedback.last_loc));
                $("table#tblCustomerSIMDetails td#tdCSDLastLocation").html(convertNulltoNA(feedback.last_loc));
                //anees 11oct2018
                $("table#tblCustomerSIMDetails td#tdCSDVendor").html(feedback.vendor);
                $("table#tblCustomerSIMDetails td#tdCSDSimType").html(feedback.simtype);

                if (feedback.mnrf != "0") { $("#btnReset").show(); }
                else { $("#btnReset").hide(); }

                //ODB Out
                if (feedback.odbout != undefined) {
                    $("#btnODBOut").show();
                    if (feedback.odbout == 0) {
                        $("table#tblCustomerSIMDetails td#tdCSDODBOut").html("Disable");
                        $("#btnODBOut").html("Enable");
                    }
                    else {
                        $("table#tblCustomerSIMDetails td#tdCSDODBOut").html("Enable");
                        $("#btnODBOut").html("Disable");
                    }
                }
                else {
                    $("#btnODBOut").hide();
                }
            } else {
                $("table#tblCustomerSIMDetails td#tdSimStatus").html("");
                $("table#tblCustomerSIMDetails td#tdCSDMobileNo").html("");
                $("table#tblCustomerSIMDetails td#tdCSDICCID").html("");
                $("table#tblCustomerSIMDetails td#tdCSDIMSIActive").html("");
                $("table#tblCustomerSIMDetails td#tdCSDIMSIMaster").html("");
                $("table#tblCustomerSIMDetails td#tdCSDSitecode").html("");
                $("table#tblCustomerSIMDetails td#tdCSDMNRF").html("");
                $("table#tblCustomerSIMDetails td#tdCSDFirstUpdate").html("");
                $("table#tblCustomerSIMDetails td#tdCSDLastUpdate").html("");
                $("table#tblCustomerSIMDetails td#tdCSDCurrentVRL").html("");
                $("table#tblCustomerSIMDetails td#tdCSDPIN1").html("");
                $("table#tblCustomerSIMDetails td#tdCSDPUK1").html("");
                $("table#tblCustomerSIMDetails td#tdCSDCurrentLocation").html("");
                $("table#tblCustomerSIMDetails td#tdCSDLastLocation").html("");
                //anees 11oct2018
                $("table#tblCustomerSIMDetails td#tdCSDVendor").html("");
                $("table#tblCustomerSIMDetails td#tdCSDSimType").html("");
                //ODB Out
                $("table#tblCustomerSIMDetails td#tdCSDODBOut").html("");
                $("#btnODBOut").hide();
            }
        }
    });
    var url = apiServer + '/api/simtransfer';
    jQuery.support.cors = true;
    var JSONSendData = {
        Msisdn: mobileno,
        Sitecode: Sitecode,
        SubType: 1
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            var errcode = -1;
            if (feedback != '' && feedback != null) {
                errcode = feedback[0].errcode;
            }
            if (errcode == 1) {
                var Createdate = new Date(feedback[0].Createdate);
                var cDate = Createdate.getDate();
                var cMonth = Createdate.getMonth() + 1;
                var Createdate_String = "";
                if (cDate < 10) { cDate = '0' + cDate } if (cMonth < 10) { cMonth = '0' + cMonth } Createdate_String = cDate + '-' + cMonth + '-' + Createdate.getFullYear();
                $("table#tblCustomerLastSwapSIMDetails td#tdCSDLastSwapDate").html(Createdate_String);
                if (feedback[0].Iccid_Old == null || feedback[0].Iccid_Old == "") {
                    $("table#tblCustomerLastSwapSIMDetails td#tdCSDLastSwapOldICCID").html("Not Available");
                } else {
                    $("table#tblCustomerLastSwapSIMDetails td#tdCSDLastSwapOldICCID").html(feedback[0].Iccid_Old);
                }
                if (feedback[0].Iccid_New == null || feedback[0].Iccid_New == "") {
                    $("table#tblCustomerLastSwapSIMDetails td#tdCSDLastSwapNewICCID").html("Not Available");
                } else {
                    $("table#tblCustomerLastSwapSIMDetails td#tdCSDLastSwapNewICCID").html(feedback[0].Iccid_New);
                }
                $("table#tblCustomerLastSwapSIMDetails td#tdCSDLastSwapOldBalance").html(feedback[0].Balance_Old);
                $("table#tblCustomerLastSwapSIMDetails td#tdCSDLastSwapAfterBalance").html(feedback[0].Afterbal);
                $("table#tblCustomerLastSwapSIMDetails td#tdCSDLastSwapType").html(feedback[0].Transfertypedesc);
                $("table#tblCustomerLastSwapSIMDetails td#tdCSDLastSwapMessage").html(feedback[0].errmsg);
                $("table#tblCustomerLastSwapSIMDetails td#tdCSDLastSwapComment").html(feedback[0].Comment);
                $("table#tblCustomerLastSwapSIMDetails td#tdPortOutDate").html(feedback[0].PortOutDate);
                $("table#tblCustomerLastSwapSIMDetails td#tdPortOutNetwork").html(feedback[0].PortOutNetwork);

                if (feedback[0].errmsg == "OTP Process" || feedback[0].errmsg == "Swap SIM success.") {
                    $("table#tblCustomerSIMDetailsAdditional td#tdCSDRoaming_OTA_Process").html(feedback[0].OTA_Status);
                    $("table#tblCustomerSIMDetailsAdditional td#tdCSDRoaming_IMSI_Overseas").html(feedback[0].Roaming_IMSI_Overseas);
                    $("table#tblCustomerSIMDetailsAdditional td#tdCSDHome_IMSI").html(feedback[0].Home_IMSI);
                    $("table#tblCustomerSIMDetailsAdditional td#tdCSDRoaming_IMSI").html(feedback[0].Roaming_IMSI);
                    $("table#tblCustomerSIMDetailsAdditional td#tdCSDVendor_Name").html(feedback[0].Vendor_Name);
                    $("table#tblCustomerSIMDetailsAdditional td#tdCSDRoaming_Country").html(feedback[0].country);
                    var SubCreated_Date_String = feedback[0].Created_Date_String.toString().substring(0, 10);
                    if (SubCreated_Date_String == "01-01-0001" || SubCreated_Date_String == "01-01-1900")
                        $("table#tblCustomerSIMDetailsAdditional td#tdCSDDate_of_Activation").html("");
                    else
                        $("table#tblCustomerSIMDetailsAdditional td#tdCSDDate_of_Activation").html(feedback[0].Created_Date_String);
                    $("#tblCustomerSIMDetailsAdditional").show();
                }

                if (feedback[0].errmsg == "OTP Process") {
                    $("#tblCustomerLastSwapSIMDetails").hide();
                }
                else {
                    $("#tblCustomerLastSwapSIMDetails").show();
                }


            } else {
                $("table#tblCustomerLastSwapSIMDetails td#tdCSDLastSwapDate").html("");
                $("table#tblCustomerLastSwapSIMDetails td#tdCSDLastSwapOldICCID").html("");
                $("table#tblCustomerLastSwapSIMDetails td#tdCSDLastSwapNewICCID").html("");
                $("table#tblCustomerLastSwapSIMDetails td#tdCSDLastSwapOldBalance").html("");
                $("table#tblCustomerLastSwapSIMDetails td#tdCSDLastSwapAfterBalance").html("");
                $("table#tblCustomerLastSwapSIMDetails td#tdCSDLastSwapType").html("");
                $("table#tblCustomerLastSwapSIMDetails td#tdCSDLastSwapMessage").html("");
                $("table#tblCustomerLastSwapSIMDetails td#tdCSDLastSwapComment").html("");
                $("table#tblCustomerLastSwapSIMDetails td#tdPortOutDate").html("");
                $("table#tblCustomerLastSwapSIMDetails td#tdPortOutNetwork").html("");
                if (feedback != null && feedback != '') {
                    if (feedback[0].errmsg == "OTP Process" || feedback[0].errmsg == "Swap SIM success.") {

                        $("table#tblCustomerSIMDetailsAdditional td#tdCSDRoaming_OTA_Process").html(feedback[0].OTA_Status);
                        $("table#tblCustomerSIMDetailsAdditional td#tdCSDRoaming_IMSI_Overseas").html(feedback[0].Roaming_IMSI_Overseas);
                        $("table#tblCustomerSIMDetailsAdditional td#tdCSDHome_IMSI").html(feedback[0].Home_IMSI);
                        $("table#tblCustomerSIMDetailsAdditional td#tdCSDRoaming_IMSI").html(feedback[0].Roaming_IMSI);
                        $("table#tblCustomerSIMDetailsAdditional td#tdCSDVendor_Name").html(feedback[0].Vendor_Name);
                        $("table#tblCustomerSIMDetailsAdditional td#tdCSDRoaming_Country").html(feedback[0].country);
                        var SubCreated_Date_String = feedback[0].Created_Date_String.toString().substring(0, 10);
                        if (SubCreated_Date_String == "01-01-0001" || SubCreated_Date_String == "01-01-1900")
                            $("table#tblCustomerSIMDetailsAdditional td#tdCSDDate_of_Activation").html("");
                        else
                            $("table#tblCustomerSIMDetailsAdditional td#tdCSDDate_of_Activation").html(feedback[0].Created_Date_String);
                        $("#tblCustomerSIMDetailsAdditional").show();
                    }
                }
                $("#tblCustomerLastSwapSIMDetails").hide();
            }
        }
    });
    $("#TabSIMDetailsLoader").hide();
    $("#tblCustomerSIMDetailsContainer").show();
}
function SIMDetailsInfoPAYM(mobileno, ICCID, Sitecode) {
    var url = apiServer + '/api/sim';
    jQuery.support.cors = true;
    var JSONSendData = {
        Msisdn: mobileno,
        Iccid: ICCID,
        Sitecode: Sitecode,
        InfoType: 1
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            var errcode = -1;
            if (feedback != '' && feedback != null) {
                errcode = feedback.errcode;
            }
            if (errcode == 0) {
                $("table#tblCustomerSIMDetails td#tdCSDMobileNo").html(feedback.msisdn);
                $("table#tblCustomerSIMDetails td#tdSimStatus").html(feedback.strstatus);
                $("table#tblCustomerSIMDetails td#tdCSDICCID").html(feedback.iccid);
                $("table#tblCustomerSIMDetails td#tdCSDIMSIActive").html(feedback.activeimsi);
                $("table#tblCustomerSIMDetails td#tdCSDIMSIMaster").html(feedback.masterimsi);
                $("table#tblCustomerSIMDetails td#tdCSDSitecode").html(feedback.sitecode);
                $("table#tblCustomerSIMDetails td#tdCSDMNRF").html(convertNulltoNA(feedback.mnrf) + " / " + convertNulltoNA(feedback.mcef));
                //10-Aug-2015 : Moorthy Modified to displaying empty when date is 01-01-0001 or 01-01-1900
                //$("table#tblCustomerSIMDetails td#tdCSDFirstUpdate").html(feedback.firstupd_string);
                //$("table#tblCustomerSIMDetails td#tdCSDLastUpdate").html(feedback.lastupde_string);
                var Subfirstupd_string = feedback.firstupd_string.substring(0, 10);
                var SubLastUpd_string = feedback.lastupde_string.substring(0, 10);

                if (Subfirstupd_string == "01-01-0001" || Subfirstupd_string == "01-01-1900") {
                    $("table#tblCustomerSIMDetails td#tdCSDFirstUpdate").html("");
                }
                else {
                    $("table#tblCustomerSIMDetails td#tdCSDFirstUpdate").html(feedback.firstupd_string);
                }

                if (SubLastUpd_string == "01-01-0001" || SubLastUpd_string == "01-01-1900") {
                    $("table#tblCustomerSIMDetails td#tdCSDLastUpdate").html("");
                }
                else {
                    $("table#tblCustomerSIMDetails td#tdCSDLastUpdate").html(feedback.lastupde_string);
                }


                //26Oct2018 : Anees Modified to displaying empty when date is 01-01-0001 or 01-01-1900
                var Suboldfirstupd_string = feedback.oldsim_firstupd_string.substring(0, 10);
                var SuboldLastUpd_string = feedback.oldsim_lastupd_lastupd.substring(0, 10);

                if (Suboldfirstupd_string == "01-01-0001" || Suboldfirstupd_string == "01-01-1900") {
                    $("table#tblCustomerSIMDetails td#tdCSDOSFFirstUpdate").html("");
                }
                else {
                    $("table#tblCustomerSIMDetails td#tdCSDOSFFirstUpdate").html(feedback.oldsim_firstupd_string);
                }

                if (SuboldLastUpd_string == "01-01-0001" || SuboldLastUpd_string == "01-01-1900") {
                    $("table#tblCustomerSIMDetails td#tdCSDOSFLastUpdate").html("");
                }
                else {
                    $("table#tblCustomerSIMDetails td#tdCSDOSFLastUpdate").html(feedback.oldsim_lastupd_lastupd);
                }

                $("table#tblCustomerSIMDetails td#tdCSDCurrentVRL").html(feedback.vlr);
                $("table#tblCustomerSIMDetails td#tdCSDPIN1").html(convertNulltoNA(feedback.pinInfo.pin1));
                $("table#tblCustomerSIMDetails td#tdCSDPUK1").html(convertNulltoNA(feedback.pinInfo.puk1));
                //$("table#tblCustomerSIMDetails td#tdCSDCurrentLocation").html(convertNulltoNA(feedback.current_loc));
                //$("table#tblCustomerSIMDetails td#tdCSDLastLocation").html(convertNulltoNA(feedback.last_loc));

                //anees 11oct2018
                $("table#tblCustomerSIMDetails td#tdCSDVendor").html(feedback.vendor);
                $("table#tblCustomerSIMDetails td#tdCSDSimType").html(feedback.simtype);


                if (feedback.mnrf != "0") { $("#btnReset").show(); }
                else { $("#btnReset").hide(); }

                //ODB Out
                if (feedback.odbout != undefined) {
                    $("#btnODBOut").show();
                    if (feedback.odbout == 0) {
                        $("table#tblCustomerSIMDetails td#tdCSDODBOut").html("Disable");
                        $("#btnODBOut").html("Enable");
                    }
                    else {
                        $("table#tblCustomerSIMDetails td#tdCSDODBOut").html("Enable");
                        $("#btnODBOut").html("Disable");
                    }
                }
                else {
                    $("#btnODBOut").hide();
                }

                //Roaming Comments
                $("table#tblCustomerSIMDetails td#tdCSDRoamingComments").html(feedback.Roaming_comments);
            } else {
                $("table#tblCustomerSIMDetails td#tdSimStatus").html("");
                $("table#tblCustomerSIMDetails td#tdCSDMobileNo").html("");
                $("table#tblCustomerSIMDetails td#tdCSDICCID").html("");
                $("table#tblCustomerSIMDetails td#tdCSDIMSIActive").html("");
                $("table#tblCustomerSIMDetails td#tdCSDIMSIMaster").html("");
                $("table#tblCustomerSIMDetails td#tdCSDSitecode").html("");
                $("table#tblCustomerSIMDetails td#tdCSDMNRF").html("");
                $("table#tblCustomerSIMDetails td#tdCSDFirstUpdate").html("");
                $("table#tblCustomerSIMDetails td#tdCSDLastUpdate").html("");
                $("table#tblCustomerSIMDetails td#tdCSDCurrentVRL").html("");
                $("table#tblCustomerSIMDetails td#tdCSDPIN1").html("");
                $("table#tblCustomerSIMDetails td#tdCSDPUK1").html("");
                //$("table#tblCustomerSIMDetails td#tdCSDCurrentLocation").html("");
                //$("table#tblCustomerSIMDetails td#tdCSDLastLocation").html("");
                //anees 11oct2018
                $("table#tblCustomerSIMDetails td#tdCSDVendor").html("");
                $("table#tblCustomerSIMDetails td#tdCSDSimType").html("");
                $("table#tblCustomerSIMDetails td#tdCSDOSFFirstUpdate").html("");
                $("table#tblCustomerSIMDetails td#tdCSDOSFLastUpdate").html("");
                //ODB Out
                $("table#tblCustomerSIMDetails td#tdCSDODBOut").html("");
                //Roaming Comments
                $("table#tblCustomerSIMDetails td#tdCSDRoamingComments").html("");
                $("#btnODBOut").hide();
            }
        }
    });
    var url = apiServer + '/api/simtransfer';
    jQuery.support.cors = true;
    var JSONSendData = {
        Msisdn: mobileno,
        Sitecode: Sitecode,
        SubType: 1
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            var errcode = -1;
            if (feedback != '' && feedback != null) {
                errcode = feedback[0].errcode;
            }
            if (errcode == 1) {
                var Createdate = new Date(feedback[0].Createdate);
                var cDate = Createdate.getDate();
                var cMonth = Createdate.getMonth() + 1;
                var Createdate_String = "";
                if (cDate < 10) { cDate = '0' + cDate } if (cMonth < 10) { cMonth = '0' + cMonth } Createdate_String = cDate + '-' + cMonth + '-' + Createdate.getFullYear();
                $("table#tblCustomerLastSwapSIMDetails td#tdCSDLastSwapDate").html(Createdate_String);
                if (feedback[0].Iccid_Old == null || feedback[0].Iccid_Old == "") {
                    $("table#tblCustomerLastSwapSIMDetails td#tdCSDLastSwapOldICCID").html("Not Available");
                } else {
                    $("table#tblCustomerLastSwapSIMDetails td#tdCSDLastSwapOldICCID").html(feedback[0].Iccid_Old);
                }
                if (feedback[0].Iccid_New == null || feedback[0].Iccid_New == "") {
                    $("table#tblCustomerLastSwapSIMDetails td#tdCSDLastSwapNewICCID").html("Not Available");
                } else {
                    $("table#tblCustomerLastSwapSIMDetails td#tdCSDLastSwapNewICCID").html(feedback[0].Iccid_New);
                }
                $("table#tblCustomerLastSwapSIMDetails td#tdCSDLastSwapOldBalance").html(feedback[0].Balance_Old);
                $("table#tblCustomerLastSwapSIMDetails td#tdCSDLastSwapAfterBalance").html(feedback[0].Afterbal);
                $("table#tblCustomerLastSwapSIMDetails td#tdCSDLastSwapType").html(feedback[0].Transfertypedesc);
                $("table#tblCustomerLastSwapSIMDetails td#tdCSDLastSwapMessage").html(feedback[0].errmsg);
                $("table#tblCustomerLastSwapSIMDetails td#tdCSDLastSwapComment").html(feedback[0].Comment);
                $("table#tblCustomerLastSwapSIMDetails td#tdPortOutDate").html(feedback[0].PortOutDate);
                $("table#tblCustomerLastSwapSIMDetails td#tdPortOutNetwork").html(feedback[0].PortOutNetwork);

                $("#tblCustomerLastSwapSIMDetails").show();
            } else {
                $("table#tblCustomerLastSwapSIMDetails td#tdCSDLastSwapDate").html("");
                $("table#tblCustomerLastSwapSIMDetails td#tdCSDLastSwapOldICCID").html("");
                $("table#tblCustomerLastSwapSIMDetails td#tdCSDLastSwapNewICCID").html("");
                $("table#tblCustomerLastSwapSIMDetails td#tdCSDLastSwapOldBalance").html("");
                $("table#tblCustomerLastSwapSIMDetails td#tdCSDLastSwapAfterBalance").html("");
                $("table#tblCustomerLastSwapSIMDetails td#tdCSDLastSwapType").html("");
                $("table#tblCustomerLastSwapSIMDetails td#tdCSDLastSwapMessage").html("");
                $("table#tblCustomerLastSwapSIMDetails td#tdCSDLastSwapComment").html("");
                $("table#tblCustomerLastSwapSIMDetails td#tdPortOutDate").html("");
                $("table#tblCustomerLastSwapSIMDetails td#tdPortOutNetwork").html("");
                $("#tblCustomerLastSwapSIMDetails").hide();
            }

        }
    });
    $("#TabSIMDetailsLoader").hide();
    $("#tblCustomerSIMDetailsContainer").show();
}

/* ----------------------------------------------------- 
   *  eof TAB GENERAL 
   ===================================================== */


