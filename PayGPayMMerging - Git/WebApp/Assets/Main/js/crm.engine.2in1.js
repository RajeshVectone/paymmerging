/* ===================================================== 
   *  2in1TAB 
   ----------------------------------------------------- */

function TwoInOneTab(mobileNo) {
    $("#TwoInOneTbl2in1ListContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"TwoInOneTblDataList\"></table>");
    $("#TwoInOneTbl2in1ListContainer").css({ "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var url = apiServer + '/api/customer2in1/' + mobileNo;
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1) {
        $.getJSON(url, function (data) {
            var oTable = $('#TwoInOneTblDataList').dataTable({
                aaData: data,
                bDestroy: true,
                bRetrieve: true,
                iDisplayLength: 10,
                aaSorting: [],
                aoColumns: [

					{ mDataProp: "imsi_mobileno", sTitle: 'IMSI Mobile Number' },
					{ mDataProp: "imsi_country", sTitle: 'IMSI Country' },
					{ mDataProp: "process_date_string", sTitle: 'Start Date' },
					{ mDataProp: "renewal_date_string", sTitle: 'Renewal Date' },
					{ mDataProp: "renewal_method_string", sTitle: 'Renewal Method' },
					{ mDataProp: "monthly_charge_string", sTitle: 'Monthly Charge' },
					{ mDataProp: "order_status", sTitle: 'Status' },
					{
					    mDataProp: 'imsi', sTitle: 'Action', bSortable: false,
					    fnRender: function (ob) {
					        var content = '<select class="selectTab2in1Action" style="color:#a0a0a0">';
					        content += '<option value="0">--- No action available ---</option>';
					        content += '</select>';

					        if (ob.aData.order_status.toLowerCase() === 'open') {
					            var content = '<select class="selectTab2in1Action">';
					            content += '<option value="0">--- Select an action ---</option>';
					            content += '<option value="1_' + ob.aData.order_id + '|' + ob.aData.new_iccid_to_swap + '">Activate</option>';
					            content += '</select>';
					        } else if (ob.aData.order_status.toLowerCase() === 'inactive') {
					            var content = '<select class="selectTab2in1Action">';
					            content += '<option value="0">--- Select an action ---</option>';
					            content += '<option value="2_' + ob.aData.order_id + '|' + ob.aData.imsi_country + '">Re-Activate</option>';
					            content += '</select>';
					        } else if (ob.aData.order_status.toLowerCase() === 'active') {
					            var content = '<select class="selectTab2in1Action">';
					            content += '<option value="0">--- Select an action ---</option>';
					            if (parseInt(ob.aData.renewal_method) != 3) {
					                content += '<option value="3_' + ob.aData.order_id + '|0">Cancel</option>';
					            }
					            content += '<option value="4_' + ob.aData.order_id + '|' + ob.aData.imsi_country + '">Change Payment Method</option>';
					            content += '</select>';
					        }

					        return content;
					    }
					}

                ]
            });
            $("#TwoInOneTbl2in1ListContainer").css({ "background-image": "none" });
            $("#TwoInOneTblDataList_wrapper .row-fluid:eq(0)").remove();
            $("#TwoInOneTblDataList_wrapper .row-fluid .span6:eq(0)").addClass("span4").html("");
            $("#TwoInOneTblDataList_wrapper .row-fluid .span6:eq(1)").addClass("span8");
            $("#TwoInOneTblDataList_wrapper .row-fluid .span6").removeClass("span6");
            if (oTable.fnSettings().fnRecordsTotal() <= 0) {
                $("#TwoInOneTblDataList_wrapper .row-fluid .span8").hide();
                $("#TwoInOneBtnAddCountryContainer").html("").append("<button class=\"btn blue\" id=\"TwoInOneBtnAddCountry\">Add Country</button>");
            } else {
                $("#TwoInOneTblDataList_wrapper .row-fluid .span8").show();
                $("#TwoInOneBtnAddCountryContainer").html("");
                $("#tab2in1AddNewCountry").remove();
            }
            $("#TwoInOneTblDataList tbody td").css({ "font-size": "12px" });
        })
    }
    else {
        $.getJSON(url, function (data) {
            var oTable = $('#TwoInOneTblDataList').dataTable({
                aaData: data,
                bDestroy: true,
                bRetrieve: true,
                iDisplayLength: 10,
                aaSorting: [],
                aoColumns: [

							   { mDataProp: "imsi_mobileno", sTitle: 'IMSI Mobile Number' },
							   { mDataProp: "imsi_country", sTitle: 'IMSI Country' },
							   { mDataProp: "process_date_string", sTitle: 'Start Date' },
					//{ mDataProp: "renewal_date_string", sTitle: 'Renewal Date' },
							   //{ mDataProp: "renewal_method_string", sTitle: 'Renewal Method' },
							   //{ mDataProp: "monthly_charge_string", sTitle: 'Monthly Charge' },
							   { mDataProp: "order_status", sTitle: 'Status' },
								{
								    mDataProp: 'imsi', sTitle: 'Action', bSortable: false,
								    fnRender: function (ob) {
								        var content = '<select class="selectTab2in1Action" style="color:#a0a0a0">';
								        content += '<option value="0">--- No action available ---</option>';
								        content += '</select>';

								        if (ob.aData.order_status === 'Not Activated') {
								            var content = '<select class="selectTab2in1Action">';
								            content += '<option value=' + ob.aData.imsi_country + '>--- Select an action ---</option>';

								            content += '<option value="1_' + 0 + '|' + ob.aData.imsi_country + '">Activate</option>';

								            content += '</select>';
								        } else if (ob.aData.order_status === 'Activated') {
								            var content = '<select class="selectTab2in1Action">';
								            content += '<option value="1_' + 0 + '|' + ob.aData.imsi_country + '">Activate</option>';
								            content += '</select>';
								        }

								        return content;
								    }
								}

                ]
            });
            $("#TwoInOneTbl2in1ListContainer").css({ "background-image": "none" });
            $("#TwoInOneTblDataList_wrapper .row-fluid:eq(0)").remove();
            $("#TwoInOneTblDataList_wrapper .row-fluid .span6:eq(0)").addClass("span4").html("");
            $("#TwoInOneTblDataList_wrapper .row-fluid .span6:eq(1)").addClass("span8");
            $("#TwoInOneTblDataList_wrapper .row-fluid .span6").removeClass("span6");
            if (oTable.fnSettings().fnRecordsTotal() <= 0) {
                $("#TwoInOneTblDataList_wrapper .row-fluid .span8").hide();
                $("#TwoInOneBtnAddCountryContainer").html("").append("<button class=\"btn blue\" id=\"TwoInOneBtnAddCountry\">Add Country</button>");
            } else {
                $("#TwoInOneTblDataList_wrapper .row-fluid .span8").hide();
                $("#TwoInOneBtnAddCountryContainer").html("").append("<button class=\"btn blue\" id=\"TwoInOneBtnAddCountry\">Add Country</button>");
            }
            $("#TwoInOneTblDataList tbody td").css({ "font-size": "12px" });
        })
    }
}

/* ----------------------------------------------------------------
* Function Name    : TwoInOneActionActivateCompatible
					: (TwoInOneAction --> No. 1)
* Purpose          : To Activate 2-in-1 Compatible SIM
* Added by         : Harry Ramdani
* Create Date      : August, 2013
* Last Update      : -
* Update History   : -
* ---------------------------------------------------------------- */
function TwoInOneActionActivateCompatible(orderID, imsiCountry, mobileNo, loginID) {
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    //var url = /api/customer2in1/';
    var url = apiServer + "/api/customer2in1/";
    //alert(url);
    jQuery.support.cors = true;
    var JSONSendData = {
        order_type: 11,
        order_id: orderID,
        imsi_country: imsiCountry,
        mobileNo: mobileNo,
        crm_login: loginID
    };


   

    $.ajax
	({
	    url: url,
	    type: 'post',
	    data: JSON.stringify(JSONSendData),
	    dataType: 'json',
	    contentType: "application/json;charset=utf-8",
	    cache: false,	   
	    beforeSend: function () {
	        $(".modal-backdrop, #portlet-config2").hide();
	        $("#ajax-screen-masking").show();
	    },


	    success: function (data) {
	        $("#ajax-screen-masking").hide();

	        $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
	        $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });

	        var stat = data.errcode;
	        if (stat == 0) {
	            $('.PMSuccess .modal-header').html(data.errsubject);
	            $('.PMSuccess .modal-body').html(data.errmsg);
	            $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
	                'margin-left': function () {
	                    return window.pageXOffset - ($(this).width() / 2);
	                }
	            });
	        } else {
	            $('.PMFailed .modal-header').html(data.errsubject);
	            $('.PMFailed .modal-body').html(data.errmsg);
	            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
	                'margin-left': function () {
	                    return window.pageXOffset - ($(this).width() / 2);
	                }
	            });
	        }
	    },
        // commented by bharathiraja
	    //error: function (data) {
	    error: function (data,jqXHR, exception) {
	        alert("failure");
	        var msg = '';
	        if (jqXHR.status === 0) {
	            msg = 'Not connect.\n Verify Network.';
	            
	        } else if (jqXHR.status == 404) {
	            msg = 'Requested page not found. [404]';
	            
	        } else if (jqXHR.status == 500) {
	            msg = 'Internal Server Error [500].';
	           
	        } else if (exception === 'parsererror') {
	            msg = 'Requested JSON parse failed.';
	           
	        } else if (exception === 'timeout') {
	            msg = 'Time out error.';
	            
	        } else if (exception === 'abort') {
	            msg = 'Ajax request aborted.';
	            
	        } else {
	            msg = 'Uncaught Error.\n' + jqXHR.responseText;
	            
	        }





	        $("#ajax-screen-masking").hide();
            // commented by bharathiraja
	        //$('.PMFailed .modal-header').html('General Error');
	        //$('.PMFailed .modal-body').html('Request failed.');

	        $('.PMFailed .modal-header').html('General Error');
	        $('.PMFailed .modal-body').html(msg);

	        $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
	            'margin-left': function () {
	                return window.pageXOffset - ($(this).width() / 2);
	            }
	        });
	    }
	});
}

/* ----------------------------------------------------------------
 * Function Name    : TwoInOneActionActivateNonCompatible (Swap)
					: (TwoInOneAction --> No. 1)
 * Purpose          : To Activate 2-in-1 Non Compatible SIM
 * Added by         : Harry Ramdani
 * Create Date      : August, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function TwoInOneActionActivateNonCompatible(orderID, mobileNo, newICCID, loginID) {

    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    var url = apiServer + '/api/Customer2in1/';

    jQuery.support.cors = true;
    var JSONSendData = {
        order_type: 12,
        order_id: orderID,
        mobileno: mobileNo,
        swapiccid: newICCID,
        crm_login: loginID
    };

    $.ajax
	({
	    url: url,
	    type: 'post',
	    data: JSON.stringify(JSONSendData),
	    dataType: 'json',
	    contentType: "application/json;charset=utf-8",
	    cache: false,
	    beforeSend: function () {
	        $(".modal-backdrop, #portlet-config2").hide();
	        $("#ajax-screen-masking").show();
	    },
	    success: function (data) {
	        $("#ajax-screen-masking").hide();

	        $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
	        $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });

	        var stat = data.errcode;
	        if (stat == 0) {
	            $('.PMSuccess .modal-header').html(data.errsubject);
	            $('.PMSuccess .modal-body').html(data.errmsg);
	            $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
	                'margin-left': function () {
	                    return window.pageXOffset - ($(this).width() / 2);
	                }
	            });
	        } else {
	            $('.PMFailed .modal-header').html(data.errsubject);
	            $('.PMFailed .modal-body').html(data.errmsg);
	            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
	                'margin-left': function () {
	                    return window.pageXOffset - ($(this).width() / 2);
	                }
	            });
	        }
	    },

	    // commented by bharathiraja
	    //error: function (data) {
	    error: function (data, jqXHR, exception) {
	        var msg = '';
	        if (jqXHR.status === 0) {
	            msg = 'Not connect.\n Verify Network.';

	        } else if (jqXHR.status == 404) {
	            msg = 'Requested page not found. [404]';

	        } else if (jqXHR.status == 500) {
	            msg = 'Internal Server Error [500].';

	        } else if (exception === 'parsererror') {
	            msg = 'Requested JSON parse failed.';

	        } else if (exception === 'timeout') {
	            msg = 'Time out error.';

	        } else if (exception === 'abort') {
	            msg = 'Ajax request aborted.';

	        } else {
	            msg = 'Uncaught Error.\n' + jqXHR.responseText;

	        }


	        $("#ajax-screen-masking").hide();
	        $('.PMFailed .modal-header').html('General Error');
	        $('.PMFailed .modal-body').html(msg);

            //commented by bharathiraja
	        //$('.PMFailed .modal-body').html('Request failed.');
	        $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
	            'margin-left': function () {
	                return window.pageXOffset - ($(this).width() / 2);
	            }
	        });
	    }
	    
	});
}

/* ----------------------------------------------------------------
 * Function Name    : TwoInOneActionCancel 
					: (TwoInOneAction --> No. 3)
 * Purpose          : To cancel the 2 in 1 plan if requested 
 * Added by         : Edi Suryadi
 * Create Date      : August 16th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function TwoInOneActionCancel(orderID) {
    var url = apiServer + '/api/customer2in1/';
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    jQuery.support.cors = true;
    var JSONSendData = {
        order_type: 3,
        order_id: orderID
    };

    $("#tab2in1CancelSubscription").hide();
    $(".modal-backdrop").hide();

    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();

            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });

            var stat = data.errcode;
            if (stat == 0) {
                $('.PMSuccess .modal-header').html(data.errsubject);
                $('.PMSuccess .modal-body').html(data.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            } else {
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : TwoInOneActionReactivate
					: (TwoInOneAction --> No. 3)
 * Purpose          : To reactivate SIM
 * Added by         : Edi Suryadi
 * Create Date      : August 19th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */

function TwoInOneActionReactivateByCredit(mobileNo, imsiCountry, LoginID, orderID) {
    var url = apiServer + '/api/customer2in1/';
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    jQuery.support.cors = true;
    var JSONSendData = {
        order_type: 21,
        mobileno: mobileNo,
        imsi_country: imsiCountry,
        crm_login: LoginID,
        order_id: orderID
    };

    $.ajax
	({
	    url: url,
	    type: 'POST',
	    data: JSON.stringify(JSONSendData),
	    dataType: 'json',
	    contentType: "application/json;charset=utf-8",
	    cache: false,
	    beforeSend: function () {
	        $("#ajax-screen-masking").show();
	    },
	    success: function (data) {
	        $("#ajax-screen-masking").hide();

	        $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
	        $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });

	        var stat = data.errcode;
	        if (stat == 0) {
	            $('.PMSuccess .modal-header').html(data.errsubject);
	            $('.PMSuccess .modal-body').html(data.errmsg);
	            $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
	                'margin-left': function () {
	                    return window.pageXOffset - ($(this).width() / 2);
	                }
	            });
	        } else {
	            $('.PMFailed .modal-header').html(data.errsubject);
	            $('.PMFailed .modal-body').html(data.errmsg);
	            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
	                'margin-left': function () {
	                    return window.pageXOffset - ($(this).width() / 2);
	                }
	            });
	        }
	    },
	    error: function () {
	        $("#ajax-screen-masking").hide();
	    }
	});
}

function TwoInOneActionReactivateByCard(p1, p2, p3, p0, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, p21, p22, p23, p24, p25, p26, p27, p28) {
    var url = apiServer + '/api/customer2in1/';
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    jQuery.support.cors = true;
    var JSONSendData = {
        order_type: 22,
        mobileno: p1,
        imsi_country: p2,
        crm_login: p3,
        title: p0,
        first_name: p4,
        last_name: p5,
        address1: p6,
        address2: p7,
        town: p8,
        postcode: p9,
        country: p10,
        email: p11,
        contact: p12,
        card_number: p13,
        card_expiry_month: p14,
        card_expiry_year: p15,
        card_issue_number: p16,
        card_verf_code: p17,
        card_first_name: p18,
        card_last_name: p19,
        card_post_code: p20,
        card_house_number: p21,
        card_address: p22,
        card_city: p23,
        card_country: p24,
        card_type: p25,
        SubscriptionID: p26,
        order_id: p27,
        product_code: p28
    };

    $.ajax
	({
	    url: url,
	    type: 'POST',
	    data: JSON.stringify(JSONSendData),
	    dataType: 'json',
	    contentType: "application/json;charset=utf-8",
	    cache: false,
	    beforeSend: function () {
	        $("#ajax-screen-masking").show();
	    },
	    success: function (data) {
	        $("#ajax-screen-masking").hide();

	        $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
	        $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });

	        var stat = data.errcode;
	        if (stat == 0) {
	            $('.PMSuccess .modal-header').html(data.errsubject);
	            $('.PMSuccess .modal-body').html(data.errmsg);
	            $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
	                'margin-left': function () {
	                    return window.pageXOffset - ($(this).width() / 2);
	                }
	            });
	        } else {
	            $('.PMFailed .modal-header').html(data.errsubject);
	            $('.PMFailed .modal-body').html(data.errmsg);
	            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
	                'margin-left': function () {
	                    return window.pageXOffset - ($(this).width() / 2);
	                }
	            });
	        }

	    },
	    error: function () {
	        $("#ajax-screen-masking").hide();
	    }
	});
}

/* ----------------------------------------------------------------
 * Function Name    : TwoInOneActionChangePaymentMethod 
					: (TwoInOneAction --> No. 4)
 * Purpose          : To change the payment methode 
 * Added by         : Edi Suryadi
 * Create Date      : August 19th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function TwoInOneActionChangePaymentMethodByCredit(mobileNo, imsiCountry, LoginID, orderID) {
    var url = apiServer + '/api/customer2in1/';
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    jQuery.support.cors = true;
    var JSONSendData = {
        order_type: 13,
        mobileno: mobileNo,
        imsi_country: imsiCountry,
        crm_login: LoginID,
        order_id: orderID
    };

    $.ajax
	({
	    url: url,
	    type: 'POST',
	    data: JSON.stringify(JSONSendData),
	    dataType: 'json',
	    contentType: "application/json;charset=utf-8",
	    cache: false,
	    beforeSend: function () {
	        $("#ajax-screen-masking").show();
	    },
	    success: function (data) {
	        $("#ajax-screen-masking").hide();

	        $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
	        $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });

	        var stat = data.errcode;
	        if (stat == 0) {
	            $('.PMSuccess .modal-header').html(data.errsubject);
	            $('.PMSuccess .modal-body').html(data.errmsg);
	            $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
	                'margin-left': function () {
	                    return window.pageXOffset - ($(this).width() / 2);
	                }
	            });
	        } else {
	            $('.PMFailed .modal-header').html(data.errsubject);
	            $('.PMFailed .modal-body').html(data.errmsg);
	            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
	                'margin-left': function () {
	                    return window.pageXOffset - ($(this).width() / 2);
	                }
	            });
	        }
	    },
	    error: function () {
	        $("#ajax-screen-masking").hide();
	    }
	});
}

function TwoInOneActionChangePaymentMethodBySavedCard(mobileNo, imsiCountry, LoginID, orderID, cardVerf, subscriptionID, product_code) {
    var url = apiServer + '/api/customer2in1/';
    jQuery.support.cors = true;
    var JSONSendData = {
        order_type: 14,
        mobileno: mobileNo,
        imsi_country: imsiCountry,
        crm_login: LoginID,
        order_id: orderID,
        card_verf_code: cardVerf,
        SubscriptionID: subscriptionID,
        product_code: product_code
    };

    $.ajax
	({
	    url: url,
	    type: 'POST',
	    data: JSON.stringify(JSONSendData),
	    dataType: 'json',
	    contentType: "application/json;charset=utf-8",
	    cache: false,
	    beforeSend: function () {
	        $("#ajax-screen-masking").show();
	    },
	    success: function (data) {
	        $("#ajax-screen-masking").hide();

	        $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
	        $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });

	        var stat = data.errcode;
	        if (stat == 0) {
	            $('.PMSuccess .modal-header').html(data.errsubject);
	            $('.PMSuccess .modal-body').html(data.errmsg);
	            $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
	                'margin-left': function () {
	                    return window.pageXOffset - ($(this).width() / 2);
	                }
	            });
	        } else {
	            $('.PMFailed .modal-header').html(data.errsubject);
	            $('.PMFailed .modal-body').html(data.errmsg);
	            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
	                'margin-left': function () {
	                    return window.pageXOffset - ($(this).width() / 2);
	                }
	            });
	        }
	    },
	    error: function () {
	        $("#ajax-screen-masking").hide();
	    }
	});
}

function TwoInOneActionChangePaymentMethodByNewCard(p1, p2, p3, orderID, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17) {
    var url = apiServer + '/api/customer2in1/';
    jQuery.support.cors = true;
    var JSONSendData = {
        order_type: 14,
        mobileno: p1,
        imsi_country: p2,
        crm_login: p3,
        order_id: orderID,
        card_number: p4,
        card_expiry_month: p5,
        card_expiry_year: p6,
        card_issue_number: p7,
        card_verf_code: p8,
        card_first_name: p9,
        card_last_name: p10,
        card_post_code: p11,
        card_house_number: p12,
        card_address: p13,
        card_city: p14,
        card_country: p15,
        card_type: p16,
        product_code: p17,
        SubscriptionID: ''

    };

    $.ajax
	({
	    url: url,
	    type: 'POST',
	    data: JSON.stringify(JSONSendData),
	    dataType: 'json',
	    contentType: "application/json;charset=utf-8",
	    cache: false,
	    beforeSend: function () {
	        $("#ajax-screen-masking").show();
	    },
	    success: function (data) {
	        $("#ajax-screen-masking").hide();

	        $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
	        $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });

	        var stat = data.errcode;
	        if (stat == 0) {
	            $('.PMSuccess .modal-header').html(data.errsubject);
	            $('.PMSuccess .modal-body').html(data.errmsg);
	            $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
	                'margin-left': function () {
	                    return window.pageXOffset - ($(this).width() / 2);
	                }
	            });
	        } else {
	            $('.PMFailed .modal-header').html(data.errsubject);
	            $('.PMFailed .modal-body').html(data.errmsg);
	            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
	                'margin-left': function () {
	                    return window.pageXOffset - ($(this).width() / 2);
	                }
	            });
	        }
	    },
	    error: function () {
	        $("#ajax-screen-masking").hide();
	    }
	});
}

function TwoInOneAddNewCountry(p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12) {
    var url = apiServer + '/api/customer2in1/';

    jQuery.support.cors = true;
    var JSONSendData = {
        mobileno: p1,
        imsi_country: p2,
        crm_login: p3,
        first_name: p4,
        last_name: p5,
        address1: p6,
        address2: p7,
        town: p8,
        postcode: p9,
        country: p10,
        email: p11,
        contact: p12
    };

    $.ajax
	({
	    url: url,
	    type: 'POST',
	    data: JSON.stringify(JSONSendData),
	    dataType: 'json',
	    contentType: "application/json;charset=utf-8",
	    cache: false,
	    success: function (data) {
	        var stat = data.errcode;
	        if (stat == 0) {
	            window.location = '/Customer/Tab2in1/' + p1;
	        }
	        else {
	            alert(data.errmsg);
	        }
	    }
	});
}

function TwoInOneAddNewCountryByCredit(p1, p2, p3, p0, p4, p5, p6, p7, p8, p9, p10, p11, p12) {

    var url = apiServer + '/api/customer2in1/';
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    jQuery.support.cors = true;
    var JSONSendData = {
        order_type: 1,
        mobileno: p1,
        imsi_country: p2,
        crm_login: p3,
        title: p0,
        first_name: p4,
        last_name: p5,
        address1: p6,
        address2: p7,
        town: p8,
        postcode: p9,
        country: p10,
        email: p11,
        contact: p12
    };
    $.ajax
	({
	    url: url,
	    type: 'POST',
	    data: JSON.stringify(JSONSendData),
	    dataType: 'json',
	    contentType: "application/json;charset=utf-8",
	    cache: false,
	    beforeSend: function () {
	        $("#ajax-screen-masking").show();
	    },
	    success: function (data) {
	        $("#ajax-screen-masking").hide();
	        $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
	        $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });

	        var stat = data.errcode;
	        if (stat == 0) {
	            $('.PMSuccess .modal-header').html(data.errsubject);
	            $('.PMSuccess .modal-body').html(data.errmsg);
	            $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
	                'margin-left': function () {
	                    return window.pageXOffset - ($(this).width() / 2);
	                }
	            });
	        } else {
	            $('.PMFailed .modal-header').html(data.errsubject);
	            $('.PMFailed .modal-body').html(data.errmsg);
	            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
	                'margin-left': function () {
	                    return window.pageXOffset - ($(this).width() / 2);
	                }
	            });
	        }
	    },
	    error: function (data) {
	        $("#ajax-screen-masking").hide();
	        alert(data.errmsg);
	    }
	});
}

function TwoInOneAddNewCountryByCard(p1, p2, p3, p0, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, p21, p22, p23, p24, p25, p26, p27) {
    var url = apiServer + '/api/customer2in1/';
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    jQuery.support.cors = true;
    var JSONSendData = {
        order_type: 2,
        mobileno: p1,
        imsi_country: p2,
        crm_login: p3,
        title: p0,
        first_name: p4,
        last_name: p5,
        address1: p6,
        address2: p7,
        town: p8,
        postcode: p9,
        country: p10,
        email: p11,
        contact: p12,
        card_number: p13,
        card_expiry_month: p14,
        card_expiry_year: p15,
        card_issue_number: p16,
        card_verf_code: p17,
        card_first_name: p18,
        card_last_name: p19,
        card_post_code: p20,
        card_house_number: p21,
        card_address: p22,
        card_city: p23,
        card_country: p24,
        card_type: p25,
        SubscriptionID: p26,
        product_code: p27
    };

    $.ajax
	({
	    url: url,
	    type: 'POST',
	    data: JSON.stringify(JSONSendData),
	    dataType: 'json',
	    contentType: "application/json;charset=utf-8",
	    cache: false,
	    beforeSend: function () {
	        $("#ajax-screen-masking").show();
	    },
	    success: function (data) {
	        $("#ajax-screen-masking").hide();

	        $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
	        $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });

	        var stat = data.errcode;
	        if (stat == 0) {
	            $('.PMSuccess .modal-header').html(data.errsubject);
	            $('.PMSuccess .modal-body').html(data.errmsg);
	            $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
	                'margin-left': function () {
	                    return window.pageXOffset - ($(this).width() / 2);
	                }
	            });
	        } else {
	            $('.PMFailed .modal-header').html(data.errsubject);
	            $('.PMFailed .modal-body').html(data.errmsg);
	            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
	                'margin-left': function () {
	                    return window.pageXOffset - ($(this).width() / 2);
	                }
	            });
	        }
	    },
	    error: function () {
	        $("#ajax-screen-masking").hide();
	    }
	});
}

/* ----------------------------------------------------------------
 * Function Name    : getCountryList
 * Purpose          : Get all countries for add new country page
 * Added by         : Edi Suryadi
 * Create Date      : September 16th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function getCountryList(productcode) {
    var url = apiServer + '/api/customer2in1/';

    jQuery.support.cors = true;
    var JSONSendData = {
        order_type: 4,
        product_code: productcode
    };

    $.ajax
	({
	    url: url,
	    type: 'POST',
	    data: JSON.stringify(JSONSendData),
	    dataType: 'json',
	    contentType: "application/json;charset=utf-8",
	    cache: false,
	    beforeSend: function () {
	        $("[name=imsi_country]").attr("disabled", true);
	        $("img#countryListLoader").show();
	    },
	    success: function (data) {
            debugger
	        var optionData = "";
	        if (data.length > 0) {
	            for (i = 0; i < data.length; i++) {
	                if (productcode != 'VMUK' && productcode != 'DMUK') {
	                    optionData += "<option value=\"" + data[i].newcountry + "\">" + data[i].newcountry + "</option>";
	                } else {
	                    if (data[i].newcountry != 'UK') {
	                        optionData += "<option value=\"" + data[i].newcountry + "\">" + data[i].newcountry + "</option>";
	                    }
	                }
	            }
	        }
	        $("[name=imsi_country]").append(optionData).removeAttr("disabled");
	        $("img#countryListLoader").hide();
	    }
	});
}

/* ----------------------------------------------------------------
 * Function Name    : ValidateAdd2in1CountryForm
 * Purpose          : to validate Add 2in1 country form
 * Added by         : Edi Suryadi
 * Create Date      : October 16th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function ValidateAdd2in1CountryForm() {
    var passed = true;
    $("#tab2in1AddNewCountry label.errLabel").hide();

    var IMSICountry = $("select[name=imsi_country] option:selected").val();
    if (IMSICountry == "") {
        passed = false;
    }

    var Title = $("select[name=Add2in1CountryTitle] option:selected").val();
    if (Title == "") {
        $("#tab2in1AddNewCountry label.errLabel").eq(0).show();
        passed = false;
    }

    var FirstName = $("input[name=Add2in1CountryFirstName]").val();
    if (FirstName == "") {
        $("#tab2in1AddNewCountry label.errLabel").eq(1).show();
        passed = false;
    }

    var LastName = $("input[name=Add2in1CountryLastName]").val();
    if (LastName == "") {
        $("#tab2in1AddNewCountry label.errLabel").eq(2).show();
        passed = false;
    }

    if (IMSICountry.toLowerCase() == 'france') {
        var DOBDay = $("select[name=Add2in1CountryDOBDay] option:selected").val();
        var DOBMonth = $("select[name=Add2in1CountryDOBMonth] option:selected").val();
        var DOBYear = $("select[name=Add2in1CountryDOBYear] option:selected").val();
        var DOB = DOBDay + "." + DOBMonth + "." + DOBYear;
        if (DOBDay == "" || DOBMonth == "" || DOBYear == "") {
            $("#tab2in1AddNewCountry label.errLabel").eq(3).html("* Please select the date of birth").show();
            passed = false;
        } else if (!isValidDate(DOB)) {
            $("#tab2in1AddNewCountry label.errLabel").eq(3).html("* Please input the correct date").show();
            passed = false;
        }


        var SelectIDForm = $("select[name=Add2in1CountrySelectIDForm] option:selected").val();
        if (SelectIDForm == "") {
            $("#tab2in1AddNewCountry label.errLabel").eq(4).show();
            passed = false;
        } else {
            switch (SelectIDForm) {
                case '1':
                    var PassportNumber = $("input[name=Add2in1CountryPassportNumber]").val();
                    if (PassportNumber == "") {
                        $("#tab2in1AddNewCountry label#Add2in1CountryIDFormInputError").html("* Please enter Passport Number").show();
                        passed = false;
                    }
                    break;
                case '2':
                    var IDCardNumber = $("input[name=Add2in1CountryIDCardNumber]").val();
                    if (IDCardNumber == "") {
                        $("#tab2in1AddNewCountry label#Add2in1CountryIDFormInputError").html("* Please enter ID Card Number").show();
                        passed = false;
                    }
                    break;
                case '3':
                    var DrivingLicenceNumber = $("input[name=Add2in1CountryDrivingLicenceNumber]").val();
                    if (DrivingLicenceNumber == "") {
                        $("#tab2in1AddNewCountry label#Add2in1CountryIDFormInputError").html("* Please enter Driving Licence Number").show();
                        passed = false;
                    }
                    break;
                case '4':
                    var BirthCertificateNumber = $("input[name=Add2in1CountryBirthCertificateNumber]").val();
                    if (BirthCertificateNumber == "") {
                        $("#tab2in1AddNewCountry label#Add2in1CountryIDFormInputError").html("* Please enter Birth Certificate Number").show();
                        passed = false;
                    }
                    break;
                case '5':
                    var VisaNumber = $("input[name=Add2in1CountryVisaNumber]").val();
                    if (VisaNumber == "") {
                        $("#tab2in1AddNewCountry label#Add2in1CountryIDFormInputError").html("* Please enter Visa Number").show();
                        passed = false;
                    }
                    break;
            }
        }
    }

    var Email = $("input[name=Add2in1CountryEmail]").val();
    if (Email == "") {
        $("#tab2in1AddNewCountry label.errLabel").eq(6).html("* Please enter Email Address").show();
        passed = false;
    } else if (!validateEmail(Email)) {
        $("#tab2in1AddNewCountry label.errLabel").eq(6).html("* Please enter a valid Email Address").show();
        passed = false;
    }

    var Postcode = $("input[name=Add2in1CountryPostcode]").val();
    if (Postcode == "") {
        $("#tab2in1AddNewCountry label.errLabel").eq(7).show();
        passed = false;
    }

    var HouseNo = $("input[name=Add2in1CountryHouseNo]").val();
    if (HouseNo == "") {
        $("#tab2in1AddNewCountry label.errLabel").eq(8).show();
        passed = false;
    }

    var Address = $("input[name=Add2in1CountryAddress]").val();
    if (Address == "") {
        $("#tab2in1AddNewCountry label.errLabel").eq(9).show();
        passed = false;
    }

    var City = $("input[name=Add2in1CountryCity]").val();
    if (City == "") {
        $("#tab2in1AddNewCountry label.errLabel").eq(10).show();
        passed = false;
    }

    var Country = $("input[name=Add2in1CountryCountry]").val();
    if (Country == "") {
        $("#tab2in1AddNewCountry label.errLabel").eq(11).show();
        passed = false;
    }

    return passed;
}


/* ----------------------------------------------------------------
 * Function Name    : CCSubsList2in1
 * Purpose          : List all the credit card subscription list
 * Added by         : Edi Suryadi
 * Create Date      : August 27th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function CCSubsList2in1(mobileno) {
    var url = apiServer + '/api/payment/' + mobileno
    $.getJSON(url, function (data) {

        $("#CCDetailsSaved").dataTable({
            aaData: data,
            bDestroy: true,
            bRetrieve: true,
            bFilter: false,
            bLengthChange: false,
            iDisplayLength: 10,
            aaSorting: [],
            oLanguage: {
                "sEmptyTable": 'No saved card found'
            },
            aoColumns: [

				{ mDataProp: "Last6digitsCC", sTitle: 'Credit Card Number' },
				{ mDataProp: "expirydate", sTitle: 'Expiry Date' },
				{
				    mDataProp: 'SubscriptionID', sTitle: 'Action', bSortable: false,
				    fnRender: function (ob) {
				        var content = '<button class="btn blue PaymentMethodBtnSavedCardProceed" alt="' + ob.aData.SubscriptionID + '|' + ob.aData.Last6digitsCC + '|' + ob.aData.cvv_number + '" />Select</button>';
				        return content;
				    }
				}

            ]
        });
        $("#CCDetailsSaved_wrapper .row-fluid").remove();
        $("#tblCCDetailsSavedLoader").hide();
        $("#CCDetailsSaved").show();
    });
}

/* ----------------------------------------------------------------
 * Function Name    : ValidateAdd2in1PaymentMethodForm
 * Purpose          : to validate Add 2in1 country form
 * Added by         : Edi Suryadi
 * Create Date      : October 16th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function Validate2in1PaymentMethodForm() {
    var passed = true;
    $("#PaymentMethodNewCardDetails label.errLabel").hide();

    var typeCard = $("input:radio[name=PaymentMethodCardType]:checked").val();
    if (typeCard == null) {
        $("#PaymentMethodNewCardDetails label.errLabel").eq(0).show();
        passed = false;
    }

    var cardNo = $("#PaymentMethodCCNumber").val();
    if (cardNo == "") {
        $("#PaymentMethodNewCardDetails label.errLabel").eq(1).show();
        passed = false;
    }

    var expMonth = $("#PaymentMethodExpiryMonth").val();
    var expYear = $("#PaymentMethodExpiryYear").val();
    if (expMonth == 0 || expYear == 0) {
        $("#PaymentMethodNewCardDetails label.errLabel").eq(2).show();
        passed = false;
    }

    var cardVerf = $("#PaymentMethodCardVerfCode").val();
    if (cardVerf == "") {
        $("#PaymentMethodNewCardDetails label.errLabel").eq(3).show();
        passed = false;
    }

    var firstName = $("#PaymentMethodFirstName").val();
    if (firstName == "") {
        $("#PaymentMethodNewCardDetails label.errLabel").eq(4).show();
        passed = false;
    }

    var lastName = $("#PaymentMethodLastName").val();
    if (lastName == "") {
        $("#PaymentMethodNewCardDetails label.errLabel").eq(5).show();
        passed = false;
    }

    var postCode = $("#PaymentMethodPostcode").val();
    if (postCode == "") {
        $("#PaymentMethodNewCardDetails label.errLabel").eq(6).show();
        passed = false;
    }

    var houseNo = $("#PaymentMethodHouseNo").val();
    if (houseNo == "") {
        $("#PaymentMethodNewCardDetails label.errLabel").eq(7).show();
        passed = false;
    }

    var address = $("#PaymentMethodAddress").val();
    if (address == "") {
        $("#PaymentMethodNewCardDetails label.errLabel").eq(8).show();
        passed = false;
    }

    var city = $("#PaymentMethodCity").val();
    if (city == "") {
        $("#PaymentMethodNewCardDetails label.errLabel").eq(9).show();
        passed = false;
    }

    var Email = $("input[name=PaymentMethodEmail]").val();
    if (Email != "" && !validateEmail(Email)) {
        $("#PaymentMethodNewCardDetails label.errLabel").eq(10).show();
        passed = false;
    }

    return passed;
}

/* ----------------------------------------------------------------
 * Function Name    : Get2in1ProductName
 * Purpose          : to get 2 in 1 product selected by product code
 * Added by         : Edi Suryadi
 * Create Date      : October 17th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function Get2in1ProductName(ProductCode, IMSICountry, highlight) {
    var CountryList = new Array("Austria", "Belgium", "Denmark", "Finland", "France", "Netherlands", "Poland", "Portugal", "Sweden", "United Kingdom");
    var CountryCode = ProductCode.substr(ProductCode.length - 2);
    var countryName = "";
    switch (CountryCode.toLowerCase()) {
        case 'at':
            countryName = CountryList[0];
            break;
        case 'be':
            countryName = CountryList[1];
            break;
        case 'dk':
            countryName = CountryList[2];
            break;
        case 'fi':
            countryName = CountryList[3];
            break;
        case 'fr':
            countryName = CountryList[4];
            break;
        case 'nl':
            countryName = CountryList[5];
            break;
        case 'pl':
            countryName = CountryList[6];
            break;
        case 'pt':
            countryName = CountryList[7];
            break;
        case 'se':
            countryName = CountryList[8];
            break;
        case 'uk':
            countryName = "UK";
            break;
    }
    var Product2in1Name = "";
    if (highlight) {
        Product2in1Name = "<b>" + countryName + " - <span style=\"color:#e45752\">" + IMSICountry + "</span></b>";
    } else {
        Product2in1Name = "<b>" + countryName + " - " + IMSICountry + "</b>";
    }
    return Product2in1Name;
}

/* ----------------------------------------------------- 
   *  eof 2in1TAB 
   ===================================================== */
