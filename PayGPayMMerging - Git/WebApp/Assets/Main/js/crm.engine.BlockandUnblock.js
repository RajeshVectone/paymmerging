﻿/* ----------------------------------------------------------------
 * Function Name    : Date fillter function
 * Purpose          : Last 7 days data
 * Added by         : karthik
 * Create Date      : May 07th, 2015
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
$(document).ready(function () {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    if (dd < 10) { dd = '0' + dd } if (mm < 10) { mm = '0' + mm } today = dd + '/' + mm + '/' + yyyy;


    //Added by karthik Jira:CRMT-232
    var Stdate = new Date();
    Stdate.setDate(Stdate.getDate() - 6);
    var Stdd = Stdate.getDate();
    var Stmm = Stdate.getMonth() + 1; //January is 0!
    var Styyyy = Stdate.getFullYear();
    if (Stdd < 10) { Stdd = '0' + Stdd } if (Stmm < 10) { Stmm = '0' + Stmm } Stdate = Stdd + '/' + Stmm + '/' + Styyyy;

    var lastdate1 = $('#lastdate1').val();
    if (lastdate1 == '') {
        $('#dp1').attr('data-date', Stdate);
        $('#dp1').datepicker().on('changeDate', function (e) {
            $('div.datepicker').hide();
        });
        $('#startdate').attr('value', Stdate);
        $('#lastdate1').val(Stdate);
    }
    else {
        $('#dp1').attr('data-date', lastdate1);
        $('#dp1').datepicker().on('changeDate', function (e) {
            $('div.datepicker').hide();
        });
        $('#startdate').attr('value', lastdate1);
    }

    var lastdate2 = $('#lastdate2').val();
    if (lastdate2 == '') {
        $('#dp2').attr('data-date', today);
        $('#dp2').datepicker().on('changeDate', function (e) {
            $('div.datepicker').hide();
        });
        $('#enddate').attr('value', today);
        $('#lastdate2').val(today);
    }
    else {
        $('#dp2').attr('data-date', lastdate2);
        $('#dp2').datepicker().on('changeDate', function (e) {
            $('div.datepicker').hide();
        });
        $('#enddate').attr('value', lastdate2);
    }

    $('#OrderListSearchBtn').click(function () {
        var orderType = $("select#orderType").val();
        var fromDate = $.trim($('#startdate').val());
        var toDate = $.trim($('#enddate').val());
        orderList(orderType, fromDate, toDate);
    });

});

/* ----------------------------------------------------------------
 * Function Name    : Verify block and Unblock number
 * Purpose          : verify mobile or ICCID wise
 * Added by         : karthik
 * Create Date      : May 07th, 2015
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function verifyblockandunblock(processby, Product, sitecode, Type, Mode, Reason, Account_info) {
    var url = apiServer + "/api/BlockandUnblock";
    jQuery.support.cors = true;
        var JSONSendData = {          
            Process_By: processby,
            product: Product,
            sitecode: sitecode,
            type: Type,
            mode: Mode,
            reason: Reason,
            account_info: Account_info,
            Block_Type: 1
        };
        var ajaxSpinnerTop = $(window).outerHeight() / 2;
        $("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
        $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
        $.ajax
        ({
            url: url,
            type: 'post',
            data: JSON.stringify(JSONSendData),
            dataType: 'json',
            contentType: "application/json;charset=utf-8",
            cache: false,
            beforeSend: function () {
                $("#tblDDOperationContainer").hide();
                $("#ajax-screen-masking").show();
            },
            success: function (feedback) {
                $("#ajax-screen-masking").hide();
                $("#tblDDOperationContainer").show();
                $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
                $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
                var errcode = feedback[0].errcode;
                if (errcode == 0) {
                    BlockUnblockWithMobileAndICCIDNumber(processby, Product, sitecode, Type, Mode, Reason, Account_info)

                }
                else {
                   
                    $('.PMFailed .modal-header').html(feedback[0].errsubject);
                    $('.PMFailed .modal-body').html(feedback[0].errmsg);
                    $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                        'margin-left': function () {
                            return window.pageXOffset - ($(this).width() / 2);
                        }
                    });
                }
            }
        });

}

/* ----------------------------------------------------------------
 * Function Name    :  block and Unblock number
 * Purpose          :  mobile or ICCID wise blocking
 * Added by         : karthik
 * Create Date      : May 07th, 2015
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function BlockUnblockWithMobileAndICCIDNumber(processby, Product, sitecode, Type, Mode, Reason, Account_info) {
    var url = apiServer + "/api/BlockandUnblock";
    jQuery.support.cors = true;
    var JSONSendData = {          
        Process_By: processby,
        product: Product,
        sitecode: sitecode,
        type: Type,
        mode: Mode,
        reason: Reason,
        account_info: Account_info,
        Block_Type:2
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#tblDDOperationContainer").hide();
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            $("#tblDDOperationContainer").show();
            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            var errcode = feedback[0].errcode;
            if (errcode == 0) {
                
                $('.PMSuccess .modal-header').html(feedback[0].errsubject);
                $('.PMSuccess .modal-body').html(feedback[0].errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
            else {
                
                $('.PMFailed .modal-header').html(feedback[0].errsubject);
                $('.PMFailed .modal-body').html(feedback[0].errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : Reports download block and unblock
 * Purpose          : Reports download block and unblock
 * Added by         : karthik
 * Create Date      : May 07th, 2015
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function BlockandUnblockReportsdownload(Product, mode, startDate, endDate) {
    $("#tblBlockUnBlockReportsContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblBlockUnBlockReports\"></table>");
    $("#tblBlockUnBlockReportsContainer").css({ "overflow": "auto", "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });

    var url = apiServer + "/api/BlockandUnblock";
    jQuery.support.cors = true;
    var JSONSendData = {
        product: Product,       
        mode: mode,
        StartDate: startDate,
        EndDate:endDate,
        Block_Type: 3
    };

    if (mode == 1) {
        $.ajax
        ({
            url: url,
            type: 'post',
            data: JSON.stringify(JSONSendData),
            dataType: 'json',
            contentType: "application/json;charset=utf-8",
            cache: false,
            success: function (feedback) {
                $("#btnBlockUnBlockReportsSettingContainer").css({ "background-image": "none" });
                $("#tblBlockUnBlockReportsContainer").css({ "background-image": "none" });
                $('#tblBlockUnBlockReports').dataTable({
                    bDestroy: true,
                    bRetrieve: true,
                    aaData: feedback,
                    iDisplayLength: 10,
                    bPaginate: true,
                    bFilter: true,
                    bInfo: false,
                    aaSorting: [[5, "desc"]],
                    oLanguage: {
                        "sInfoEmpty": '',
                        "sEmptyTable": "No Block and UnBlock History Found "
                    },
                    aoColumns: [
                        { mDataProp: "Batch_Id", sTitle: "Referance ID" },
                        { mDataProp: "Mobile_No", sTitle: "CLI" },
                        { mDataProp: "Iccid", sTitle: "ICCID" },
                        { mDataProp: "brand", sTitle: "Brand" },
                        { mDataProp: "Process_By", sTitle: "User" },
                        {
                            mDataProp: "Log_Date", sTitle: "Date and Time", "bUseRendered": false,
                            fnRender: function (ob) {
                                return convertDateISOCustom(ob.aData.Log_Date, 0);
                            }
                        },
                        {
                            mDataProp: "master_bal", sTitle: "Master Balance",
                            fnRender: function (ob) {
                                var ss = ob.aData.Currency;
                                return getCurrencySymbol(ob.aData.Currency) + " " + ob.aData.master_bal;
                            }
                        },
                        { mDataProp: "reason", sTitle: "Reason For Blocking" }
                    ],
                    fnDrawCallback: function () {
                        $("#tblBlockUnBlockReports tbody td").css({ "font-size": "12px" });
                    }
                });
                $("#tblDDOperation tbody td").css({ "font-size": "12px" });

                //$("#tblBlockUnBlockReports tbody td").css({ "font-size": "12px" });
                //$("#tblBlockUnBlockReports_wrapper .row-fluid:eq(0)").remove();

                //$("#tblBlockUnBlockReports_wrapper .row-fluid .span6:eq(0)").addClass("span4");
                //$("#tblBlockUnBlockReports_wrapper .row-fluid .span6:eq(1)").addClass("span8");

                var DownloadAnchor = "<a id=\"downloadAnchor\" style=\"display:none;\"></a>";
                var btnExportHistory = "<button class=\"btn blue\" id=\"btnExportBlockUnBlock\" name=\"btnExportBlockUnBlock\">Download to Excel</button>";
               // $("#tblBlockUnBlockReports_wrapper .row-fluid .span6").append(DownloadAnchor + btnExportHistory).show();
                // $("div.row-fluid:nth-child(3) > div:nth-child(1)").append(DownloadAnchor + btnExportHistory).show();
                $("div#tblBlockUnBlockReportsContainer  > div#tblBlockUnBlockReports_wrapper  > div.row-fluid:nth-child(3)  > div:nth-child(1)").append(DownloadAnchor + btnExportHistory).show();
            },
            error: function (feedback) {
                $("#TabProductPackagesLoader").hide();
                $("#tblProductPackagesContainer").html("").append("No data found").show();
            }
        });
    }
    else {
         
            $.ajax
            ({
                url: url,
                type: 'post',
                data: JSON.stringify(JSONSendData),
                dataType: 'json',
                contentType: "application/json;charset=utf-8",
                cache: false,
                success: function (feedback) {
                    $("#btnBlockUnBlockReportsSettingContainer").css({ "background-image": "none" });
                    $("#tblBlockUnBlockReportsContainer").css({ "background-image": "none" });
                    $('#tblBlockUnBlockReports').dataTable({
                        bDestroy: true,
                        bRetrieve: true,
                        aaData: feedback,
                        iDisplayLength: 10,
                        bPaginate: true,
                        bFilter: true,
                        bInfo: false,
                        aaSorting: [[5, "desc"]],
                        oLanguage: {
                            "sInfoEmpty": '',
                            "sEmptyTable": "No Block and UnBlock History Found "
                        },
                        aoColumns: [
                            { mDataProp: "Batch_Id", sTitle: "Referance ID" },
                            { mDataProp: "Mobile_No", sTitle: "CLI" },
                            { mDataProp: "Iccid", sTitle: "ICCID" },
                            { mDataProp: "brand", sTitle: "Brand" },
                            { mDataProp: "Process_By", sTitle: "User" },
                            {
                                mDataProp: "Log_Date", sTitle: "Date and Time", "bUseRendered": false,
                                fnRender: function (ob) {
                                    return convertDateISOCustom(ob.aData.Log_Date, 0);
                                }
                            },
                            {
                                mDataProp: "master_bal", sTitle: "Master Balance",
                                fnRender: function (ob) {
                                    return getCurrencySymbol(ob.aData.Currency) + " " + ob.aData.master_bal;
                                }
                            },
                            { mDataProp: "reason", sTitle: "Reason For UnBlocking" }
                        ],
                        fnDrawCallback: function () {
                            $("#tblBlockUnBlockReports tbody td").css({ "font-size": "12px" });
                        }
                    });
                    $("#tblDDOperation tbody td").css({ "font-size": "12px" });
                    //$("#tblBlockUnBlockReports tbody td").css({ "font-size": "12px" });
                    //$("#tblBlockUnBlockReports_wrapper .row-fluid:eq(0)").remove();

                    //$("#tblBlockUnBlockReports_wrapper .row-fluid .span6:eq(0)").addClass("span4");
                    //$("#tblBlockUnBlockReports_wrapper .row-fluid .span6:eq(1)").addClass("span8");

                    var DownloadAnchor = "<a id=\"downloadAnchor\" style=\"display:none;\"></a>";
                    var btnExportHistory = "<button class=\"btn blue\" id=\"btnExportBlockUnBlock\" name=\"btnExportBlockUnBlock\">Download to Excel</button>";
                    // $("#tblBlockUnBlockReports_wrapper .row-fluid .span4").append(DownloadAnchor + btnExportHistory).show();
                    $("div#tblBlockUnBlockReportsContainer  > div#tblBlockUnBlockReports_wrapper  > div.row-fluid:nth-child(3)  > div:nth-child(1)").append(DownloadAnchor + btnExportHistory).show();
                },
                error: function (feedback) {
                    $("#TabProductPackagesLoader").hide();
                    $("#tblProductPackagesContainer").html("").append("No data found").show();
                }
            });
    }
}
/* ----------------------------------------------------------------
 * Function Name    : Validation for block
 * Purpose          : Validation for block
 * Added by         : karthik
 * Create Date      : May 07th, 2015
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function validateproduct() {
    var isvalid = true;
    if ($('#productListFinance').val() == "0") {
        isvalid = false;
        $('#errmsgProd').show();
    }
    else {
        $('#errmsgProd').hide();
    }

    return isvalid;
}
/* ----------------------------------------------------------------
 * Function Name    : Validation for Unblock
 * Purpose          : Validation for Unblock
 * Added by         : karthik
 * Create Date      : May 07th, 2015
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function validateproduct1() {
    var isvalid = true;
    
    if ($('#productListFinanceUnblock').val() == "0") {
        isvalid = false;
        $('#errmsgProd1').show();
    }
    else {
        $('#errmsgProd1').hide();
    }

    return isvalid;
}

/* ----------------------------------------------------------------
 * Function Name    : Clear  block
 * Purpose          : Clear  block tools
 * Added by         : karthik
 * Create Date      : May 07th, 2015
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function clearblock() {
    $("#Singleblocking").val('');
    $("#SingleblockingMsg").val('');

    $("#Multipleblocking").val('');
    $("#MultipleblockingMsg").val('');

    $("#Bulkblocking").val('');
    $("#BulkblockingMsg").val('');


    $("#SingleblockingMobile").val('');
    $("#SingleblockingMobileMsg").val('');

    $("#Multipleblockingmobile").val('');
    $("#MultipleblockingmobileMsg").val('');

    $("#Bulkblockingmobile").val('');
    $("#BulkblockingmobileMsg").val('');
}


/* ----------------------------------------------------------------
 * Function Name    : Clear  Unblock
 * Purpose          : Clear  Unblock tools
 * Added by         : karthik
 * Create Date      : May 07th, 2015
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function clearunblock() {

    $("#Singleunblocking").val('');
    $("#SingleunblockingMsg").val('');

    $("#Multipleunblocking").val('');
    $("#MultipleunblockingMsg").val('');


    $("#Bulkunblocking").val('');
    $("#BulkunblockingMsg").val('');

    $("#SingleunblockingMobile").val('');
    $("#SingleunblockingMobileMsg").val('');

    $("#Multipleunblockingmobile").val('');
    $("#MultipleunblockingmobileMsg").val('');

    $("#Bulkunblockingmobile").val('');
    $("#BulkunblockingmobileMsg").val('');
}


/* ----------------------------------------------------------------
 * Function Name    :bluk block with iccid
 * Purpose          :bluk block with iccid
 * Added by         : karthik
 * Create Date      : May 07th, 2015
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */

function blukblockwithiccid()
{
    $("#TabProductCDRLoader").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblICCIDHistory\"></table>");
    $("#TabProductCDRLoader").css({ "overflow": "auto", "min-height": "100px" });
    var formData = new FormData($('form')[0]);
    $.ajax({
        url: apiServer + "/api/BulkBlock",
        type: 'POST',
        // Form data
        data: formData,
        //Options to tell JQuery not to process data or worry about content-type
        cache: false,
        contentType: false,
        processData: false,
        success: function (feedback) {
            $('.BlukICCIDPMSuccess .modal-footer .BlukICCIDPMSuccess').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            //$('#TabProductCDRLoader').show();

            var errcode = feedback[0].errcode;
            var test = '';
            $.each(feedback, function (key, value) {
                test = value.ICCID + ',' + test;
            });
            var test1 = test.substring(0, test.length - 1);
            $('#HiBlukblockICCID').val(test1);
            var ss = feedback[0].ICCID;
            if (errcode == 0) {
           
                $('#tblICCIDHistory').dataTable({
                    bDestroy: true,
                    bRetrieve: true,
                    aaData: feedback,
                    bPaginate: true,
                    bFilter: false,
                    bInfo: false,
                    iDisplayLength: 10,                  
                    oLanguage: {
                        "sInfoEmpty": '',
                        "sEmptyTable": "No Data Available."
                    },
                    aoColumns: [
                        { mDataProp: "ICCID", sTitle: 'ICCID' }
                    ],
                   
                });         
                $('.BlukICCIDPMSuccess .modal-header').html(feedback[0].errsubject);
                $('.BlukICCIDPMSuccess .modal-body').html(feedback[0].errmsg);
                $('.BlukICCIDPMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
            else {
                $('.PMFailed .modal-header').html(feedback[0].errsubject);
                $('.PMFailed .modal-body').html(feedback[0].errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        }

    });
}

/* ----------------------------------------------------------------
 * Function Name    :bluk block with mobile
 * Purpose          :bluk block with mobile
 * Added by         : karthik
 * Create Date      : May 07th, 2015
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */

function blukblockwithmobile() {
    $("#TabmobilehistoryLoader").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblmobileHistory\"></table>");
    $("#TabmobilehistoryLoader").css({ "overflow": "auto", "min-height": "100px" });
    var formData = new FormData($('form')[1]);
    $.ajax({
        url: apiServer + "/api/BulkBlockMobile",
        type: 'POST',
        // Form data
        data: formData,
        //Options to tell JQuery not to process data or worry about content-type
        cache: false,
        contentType: false,
        processData: false,
        success: function (feedback) {
            $('.BlukMobilesuccess .modal-footer .BlukICCIDPMSuccess').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            //$('#TabProductCDRLoader').show();

            var errcode = feedback[0].errcode;
            var test = '';
            $.each(feedback, function (key, value) {
                test = value.Mobile + ',' + test;
            });
            var test1 = test.substring(0, test.length - 1);
            $('#HiBlukblockmobile').val(test1);
            var ss = feedback[0].Mobile;
            if (errcode == 0) {

                $('#tblmobileHistory').dataTable({
                    bDestroy: true,
                    bRetrieve: true,
                    aaData: feedback,
                    bPaginate: true,
                    bFilter: false,
                    bInfo: false,
                    iDisplayLength: 10,
                    oLanguage: {
                        "sInfoEmpty": '',
                        "sEmptyTable": "No Data Available."
                    },
                    aoColumns: [
                        { mDataProp: "Mobile", sTitle: 'Mobile' }
                    ],

                });
                $('.BlukMobilesuccess .modal-header').html(feedback[0].errsubject);
                $('.BlukMobilesuccess .modal-body').html(feedback[0].errmsg);
                $('.BlukMobilesuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
            else {
                $('.PMFailed .modal-header').html(feedback[0].errsubject);
                $('.PMFailed .modal-body').html(feedback[0].errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        }

    });
}

/* ----------------------------------------------------------------
 * Function Name    :bluk Unblock with iccid
 * Purpose          :bluk Unblock with iccid
 * Added by         : karthik
 * Create Date      : May 07th, 2015
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */

function Unblukblockwithiccid() {
    $("#TabUnblockICCIDLoader").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblUbblockICCIDHistory\"></table>");
    $("#TabUnblockICCIDLoader").css({ "overflow": "auto", "min-height": "100px" });
    var formData = new FormData($('form')[2]);
    $.ajax({
        url: apiServer + "/api/BulkUnBlockICCID",
        type: 'POST',
        // Form data
        data: formData,
        //Options to tell JQuery not to process data or worry about content-type
        cache: false,
        contentType: false,
        processData: false,
        success: function (feedback) {
            $('.UnBlukICCIDsuccess .modal-footer .BlukICCIDPMSuccess').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            //$('#TabProductCDRLoader').show();

            var errcode = feedback[0].errcode;
            var test = '';
            $.each(feedback, function (key, value) {
                test = value.ICCID + ',' + test;
            });
            var test1 = test.substring(0, test.length - 1);
            $('#HiUnBlukICCID').val(test1);
            var ss = feedback[0].ICCID;
            if (errcode == 0) {

                $('#tblUbblockICCIDHistory').dataTable({
                    bDestroy: true,
                    bRetrieve: true,
                    aaData: feedback,
                    bPaginate: true,
                    bFilter: false,
                    bInfo: false,
                    iDisplayLength: 10,
                    oLanguage: {
                        "sInfoEmpty": '',
                        "sEmptyTable": "No Data Available."
                    },
                    aoColumns: [
                        { mDataProp: "ICCID", sTitle: 'ICCID' }
                    ],

                });
                $('.UnBlukICCIDsuccess .modal-header').html(feedback[0].errsubject);
                $('.UnBlukICCIDsuccess .modal-body').html(feedback[0].errmsg);
                $('.UnBlukICCIDsuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
            else {
                $('.PMFailed .modal-header').html(feedback[0].errsubject);
                $('.PMFailed .modal-body').html(feedback[0].errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        }

    });
}


/* ----------------------------------------------------------------
 * Function Name    :Unbluk block with mobile
 * Purpose          :Unbluk block with mobile
 * Added by         : karthik
 * Create Date      : May 07th, 2015
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */

function Unblukblockwithmobile() {
    $("#TabUnblockMobileLoader").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblUbblockMobileHistory\"></table>");
    $("#TabUnblockMobileLoader").css({ "overflow": "auto", "min-height": "100px" });
    var formData = new FormData($('form')[3]);
    $.ajax({
        url: apiServer + "/api/BulkUnBlockMobile",
        type: 'POST',
        // Form data
        data: formData,
        //Options to tell JQuery not to process data or worry about content-type
        cache: false,
        contentType: false,
        processData: false,
        success: function (feedback) {
            $('.UnBlukMobilesuccess .modal-footer .BlukICCIDPMSuccess').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            //$('#TabProductCDRLoader').show();

            var errcode = feedback[0].errcode;
            var test = '';
            $.each(feedback, function (key, value) {
                test = value.Mobile + ',' + test;
            });
            var test1 = test.substring(0, test.length - 1);
            $('#HiUnBlukmobile').val(test1);
            var ss = feedback[0].Mobile;
            if (errcode == 0) {

                $('#tblUbblockMobileHistory').dataTable({
                    bDestroy: true,
                    bRetrieve: true,
                    aaData: feedback,
                    bPaginate: true,
                    bFilter: false,
                    bInfo: false,
                    iDisplayLength: 10,
                    oLanguage: {
                        "sInfoEmpty": '',
                        "sEmptyTable": "No Data Available."
                    },
                    aoColumns: [
                        { mDataProp: "Mobile", sTitle: 'Mobile' }
                    ],

                });
                $('.UnBlukMobilesuccess .modal-header').html(feedback[0].errsubject);
                $('.UnBlukMobilesuccess .modal-body').html(feedback[0].errmsg);
                $('.UnBlukMobilesuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
            else {
                $('.PMFailed .modal-header').html(feedback[0].errsubject);
                $('.PMFailed .modal-body').html(feedback[0].errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        }

    });
}