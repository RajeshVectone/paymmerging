﻿/* ===================================================== 
   *  TAB BUNDLES 
   ----------------------------------------------------- */


/* ----------------------------------------------------------------
 * Function Name    : Internet Profile
 * Purpose          : to show Intenet Profile  
 * Added by         : karthik
 * Create Date      : December 18th, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */

var TicketItemHtml = '<li id="liTicket" class="liTicket" ctlValue="{enqiry_id}" style="cursor:pointer;">';
TicketItemHtml = TicketItemHtml + '<div class="historylog-left"><h3>Created by</h3><div class="leftbox"><strong>{CREATE_BY}</strong></div> </div>';
TicketItemHtml = TicketItemHtml + '<div class="historylog-right"><h3>Created Data</h3><div class="rightbox "><strong>{CREATE_DATE}</strong></div></div>';
TicketItemHtml = TicketItemHtml + '<div class=" innercommentbox ">{COMMENTS}</div></li>';
function GetUserListData(date_from, date_to, Enquiry_ID) {

    $("#pbInternetProfileBody").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblPBInternetProfile\"></table>");
    $("#pbInternetProfileBody").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });

    var url = "/Enquiries/GetEnquiryList";
    jQuery.support.cors = true;
    var JSONSendData = {
        date_from: date_from,
        date_to: date_to,
        Enquiry_ID: Enquiry_ID
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#pbInternetProfileBody").css({ "background-image": "none" });
            $('#tblPBInternetProfile').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                iDisplayLength: 10,
                bPaginate: true,
                bFilter: true,
                bInfo: false,
                aaSorting: [[4, "desc"]],
                aoColumns: [
                    { mDataProp: "Enquiry_ID", sTitle: "Enquiry ID" },
                    { mDataProp: "Enquirer_Name", sTitle: "Enquirer Name" },
                    { mDataProp: "Mobileno", sTitle: "Mobile Number" },
                    { mDataProp: "agent", sTitle: "Agent" },
                    { mDataProp: "Email", sTitle: "Email" },
                    { mDataProp: "Enquiry_Date_string", sTitle: "Enquiry On" },
                    //{ mDataProp: "Existing_Network", sTitle: "Existing Network" },
                    { mDataProp: "Enquiry_Name", sTitle: "Enquiry Type" },
                //    { mDataProp: "current_status", sTitle: "Status" },
                     
                 
                    {
                        mDataProp: "id", sTitle: "Action",
                        fnRender: function (ob) {
                            //var actionOption = "<select class=\"selectLLOTGAction\" data-country=\"" + ob.aData.Country + "\" data-regno=\"" + ob.aData.Reg_Number + "\" data-pt=\"" + ob.aData.Payment_Type + "\" data-ptd=\"" + ob.aData.PaymentType_Description + "\"data-balance=\"" + ob.aData.TotalAmount + "\"data-svcid=\"" + ob.aData.svc_id + "\"data-Planamount=\"" + ob.aData.Plan_amount + "\"data-planstatus=\"" + ob.aData.Status + "\" >";
                            var actionOption = "<select class=\"selectuserlistAction\" data-Enquiry_ID=\"" + ob.aData.Enquiry_ID + "\" data-Enquirer_Name=\"" + ob.aData.Enquirer_Name + "\" id=\"selectuserlistAction\" >";
                            actionOption += '<option value="0" style=\"color:#c0c0c0\">--- Select an action ---</option>';
                            actionOption += '<option value="1" style=\"padding:3px\">Edit</option>';
                            //29/05/2019
                            //actionOption += '<option value="2" style=\"padding:3px\">Remove</option>';
                            if (ob.aData.Status == "Active") {
                                actionOption += '<option value="3" style=\"padding:3px\">Deactivate</option>';
                            }
                            if (ob.aData.Status == "Inactive") {
                                actionOption += '<option value="4" style=\"padding:3px\">Reactive</option>';
                            }
                            actionOption += '</select>';
                            return actionOption;
                        }
                    }

                ],
                fnDrawCallback: function () {
                    $("#pbInternetProfileBody tbody td").css({ "font-size": "12px" });
                }
            });

            //$("#tblPBInternetProfile_wrapper .row-fluid:eq(0)").remove();
            //$("#tblPBInternetProfile_wrapper .row-fluid .span6:eq(0)").addClass("span4");
            //$("#tblPBInternetProfile_wrapper .row-fluid .span6:eq(1)").addClass("span8");
            //$("#tblPBInternetProfile_wrapper .row-fluid .span6").removeClass("span6");

            var DownloadAnchor = "<a id=\"downloadAnchor\" style=\"display:none;\"></a>";
            var btnExportHistory = "<button class=\"btn blue\" id=\"btnExportHistory\" name=\"btnExportHistory\">Download</button>";
            $("div#pbInternetProfileBody  > div#tblPBInternetProfile_wrapper  > div.row-fluid:nth-child(3)  > div:nth-child(1)").append(DownloadAnchor + btnExportHistory).show();
        },
        error: function (feedback) {
            $("#pbInternetProfileBody").css({ "background-image": "none" }).html("").append("No User Details found").show();
        }
    });
}
function FillTicketItems(Enquiry_ID) {
    var url = "/Enquiries/GetHistoryList";
    jQuery.support.cors = true;
    var JSONSendData = { 
        Enquiry_ID: Enquiry_ID
    };
    $.ajax
   ({
       url: url,
       type: 'post',
       data: JSON.stringify(JSONSendData),
       dataType: 'json',
       contentType: "application/json;charset=utf-8",
       cache: false,
       success: function (feedback) {
           $("#ajax-screen-masking").hide();
           $('#GIAddUser ul.tickets-details').html("");
           if (feedback != null && feedback.length > 0) {
               for (var i = 0; i < feedback.length; i++) {
                   var appString = TicketItemHtml.replace("{CREATE_BY}", feedback[i].create_by).replace("{CREATE_DATE}", feedback[i].create_datee_string).replace("{COMMENTS}", feedback[i].comments);
                   $('#GIAddUser ul.tickets-details').append(appString);
               }
           }
           else {
               $('#GIAddUser ul.tickets-details').append("No history found.");
           }
       },
       error: function (feedback) {
           $("#ajax-screen-masking").hide();
       }
   });
}

/* ----------------------------------------------------------------
added Insert Code
 * ---------------------------------------------------------------- */
function InsertNewUser(Enquirer_Name, Mobileno, Email, Existing_Network, Country, Enquiry_Type, Query, Comments, fk_agent_id,mode,Enquiry_ID) {
    var url = '/Enquiries/NewEnquiryInsert';
    jQuery.support.cors = true;
    var JSONSendData = {
        Enquirer_Name: Enquirer_Name,
        Mobileno: Mobileno,
        Email: Email,
        //LastName: LastName,
        Email: Email,
        Existing_Network: Existing_Network,
        Country: Country,
        Enquiry_Type: Enquiry_Type,
        Query: Query,
        Comments: Comments,
        fk_agent_id: fk_agent_id,
        mode: mode,
        Enquiry_ID:Enquiry_ID
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {

            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });

            // var errcode1 = errcode;
            var errcode = feedback.errcode;
            if (errcode == 0) {
                $("#GIAddUser").hide();
                $('.PMSuccess .modal-header').html(feedback.errsubject);
                $('.PMSuccess .modal-body').html(feedback.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
            else {

                $('#lblCheckusername').show();
                //$('.PMFailed .modal-header').html(feedback.errsubject);
                //$('.PMFailed .modal-body').html(feedback.errmsg);
                //$('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                //    'margin-left': function () {
                //        return window.pageXOffset - ($(this).width() / 2);
                //    }
                //});
            }
        },
        error: function (feedback) {
            $("#pbInternetProfileBody").css({ "background-image": "none" }).html("").append("No internet profile found").show();
        }
    });
}
function InsertComments(Enquiry_ID, comments, create_by) {
    var url = '/Enquiries/InsertCommentsHistory';
    jQuery.support.cors = true;
    var JSONSendData = {
        Enquiry_ID: Enquiry_ID,
        comments: comments,
        create_by: create_by
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {

          //  $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
           // $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });

            // var errcode1 = errcode;
            var errcode = feedback[0].errcode;
            if (errcode == 0) {
                //$("#GIHistoryComment").hide();
                //$('.PMSuccess .modal-header').html("Alert");
                //$('.PMSuccess .modal-body').html(feedback[0].ErrMsg);
                //$('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                //    'margin-left': function () {
                //        return window.pageXOffset - ($(this).width() / 2);
                //    }
                //});
                alert("Saved Successfully");
            }
            else {
                alert("Save Failed");
            }
        },
        error: function (feedback) {
            $("#pbInternetProfileBody").css({ "background-image": "none" }).html("").append("No internet profile found").show();
        }
    });
}
/* ----------------------------------------------------------------
 * Function Name    : Delete User List
 * Purpose          : Delete User List
 * Added by         : karthik
 * Create Date      : June 4th, 2015
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function DeleteUser(Id, Username) {
    var url = '/Users/DeleteUserRecord';;
    jQuery.support.cors = true;
    var JSONSendData = {
        Id: Id,
        Username: Username,
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {

            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            $("#GIAddUser").hide();
            // var errcode1 = errcode;
            var errcode = feedback.errcode;
            if (errcode == 0) {
                $('.PMSuccess .modal-header').html(feedback.errsubject);
                $('.PMSuccess .modal-body').html(feedback.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
            else {
                $('.PMFailed .modal-header').html(feedback.errsubject);
                $('.PMFailed .modal-body').html(feedback.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (feedback) {
            $("#pbInternetProfileBody").css({ "background-image": "none" }).html("").append("No internet profile found").show();
        }
    });
}
/* ----------------------------------------------------------------
 * Function Name    : Deactivate
 * Purpose          : Deactivate
 * Added by         : karthik
 * Create Date      : June 4th, 2015
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function Deactivate(Id, Username) {
    var url = '/Users/Deactivate';;
    jQuery.support.cors = true;
    var JSONSendData = {
        Id: Id,
        Username: Username,
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {

            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            $("#GIAddUser").hide();
            // var errcode1 = errcode;
            var errcode = feedback.errcode;
            if (errcode == 0) {
                $('.PMSuccess .modal-header').html(feedback.errsubject);
                $('.PMSuccess .modal-body').html(feedback.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
            else {
                $('.PMFailed .modal-header').html(feedback.errsubject);
                $('.PMFailed .modal-body').html(feedback.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (feedback) {
            $("#pbInternetProfileBody").css({ "background-image": "none" }).html("").append("No internet profile found").show();
        }
    });
}
/* ----------------------------------------------------------------
 * Function Name    : Reactive
 * Purpose          : Reactive
 * Added by         : karthik
 * Create Date      : June 4th, 2015
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function Reactive(Id, Username) {
    var url = '/Users/Reactive';;
    jQuery.support.cors = true;
    var JSONSendData = {
        Id: Id,
        Username: Username,
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {

            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            $("#GIAddUser").hide();
            // var errcode1 = errcode;
            var errcode = feedback.errcode;
            if (errcode == 0) {
                $('.PMSuccess .modal-header').html(feedback.errsubject);
                $('.PMSuccess .modal-body').html(feedback.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
            else {
                $('.PMFailed .modal-header').html(feedback.errsubject);
                $('.PMFailed .modal-body').html(feedback.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (feedback) {
            $("#pbInternetProfileBody").css({ "background-image": "none" }).html("").append("No internet profile found").show();
        }
    });
}
/* ----------------------------------------------------------------
 * Function Name    : GPRS setting sending to customer
 * Purpose          :  GPRS setting sending to customer
 * Added by         : karthik
 * Create Date      : June 4th, 2015
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function GetUserPermissionlist(User_Id, Enquiry_ID, mode) {
    debugger
    var url = '/Users/GetUserPermissionlist';;
    jQuery.support.cors = true;
    var JSONSendData = {
        Id: User_Id
        //Username: Username,
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {


            BasicinfoEdit(User_Id, Enquiry_ID, mode);

            $(':checkbox[name^=chklistitem]').filter(function (index, val) {
                if (feedback.indexOf(this.id) >= 0)
                    return this.checked = true;
            })

            //$('#InUsername').val(feedback[0].Username);
            //$('#inpassword').val(feedback[0].Password);
            //$('#inretypepassword').val(feedback[0].Password);
            //$('#infirstname').val(feedback[0].FirstName);
            //$('#inlastname').val(feedback[0].LastName);
            //$('#inemail').val(feedback[0].Email);
            //$('#UsernameStr3').text(User_Name);           
        },
        error: function (feedback) {
            $("#pbInternetProfileBody").css({ "background-image": "none" }).html("").append("No internet profile found").show();
        }
    });
}
function BasicinfoEdit(User_Id, Enquiry_ID, mode) {
    var url = '/Enquiries/BasicinfoEdit';
    jQuery.support.cors = true;
    var JSONSendData = {
        Enquiry_ID: Enquiry_ID,
        mode:mode
    };
    $.ajax
    ({

        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('#InEnquirerName').val(feedback[0].Enquirer_Name);
            $('#InMobileNo').val(feedback[0].Mobileno);
            $('#InEmail').val(feedback[0].Email);
            $('#InExistingnetwork').val(feedback[0].Existing_Network);
            $('#inCountry').val(feedback[0].Country);
            $('#inEnquiryType').val(feedback[0].Enquiry_Name);
            //$('#UsernameStr3').text(User_Name);
            $('#iEnquiryDescription').text(feedback[0].Query);
            $("#iVectoneResponse").val(feedback[0].Comments);
            //if (feedback[0].Active)
            //    //$("#inStatus").val('1');
            //    $("#inStatus").attr('selectedIndex', 0);
            //else
            //    $("#inStatus").attr('selectedIndex', 1);
            //$("#inStatus").val('2');
        },
        error: function (feedback) {
            $("#pbInternetProfileBody").css({ "background-image": "none" }).html("").append("No internet profile found").show();
        }
    });
}
function GetPermissionlist(User_Id, mode) {
    debugger;
    var url = '/Users/GetPermissionlist';;
    jQuery.support.cors = true;
    $.ajax
    ({
        url: url,
        type: 'post',

        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            var errcode = feedback[0].errcode;
            var id = feedback[0].Id;
            var Name = feedback[0].Name;
            var table = $('<table  id="DropPermissiion" name="DropPermissiion" width="100%"></table>');
            var counter = 0;
            $('#Inpermissionlist').empty();
            $('#Inpermissionlist').append(table);
            var i = 0;
            $(feedback).each(function () {
                if (i == 0) {
                    table.append($('<tr></tr>').append($('<td  id="permissionlist" width="200px"></td>').append($('<input>').attr({
                        type: 'checkbox', name: 'chklistitem', value: this.Value, id: this.Id
                    })).append(
               $('<label style="margin-top:-16px;margin-left:20px">').attr({
                   //for: 'chklistitem' + this.Id++
               }).text(this.Name))));
                    i = i + 1;
                }
                else {
                    table.find($('tr:last').append($('<td id="permissionlist" width="200px"></td>').append($('<input>').attr({
                        type: 'checkbox', name: 'chklistitem', value: this.Value, id: this.Id
                    })).append(
                $('<label style="margin-top:-16px;margin-left:20px">').attr({
                    // for: 'chklistitem' + this.Id++
                }).text(this.Name))));
                    i = i - 1;
                }
            });
            if (mode == "2") {
                GetUserPermissionlist(User_Id);
            }
        },
        error: function (feedback) {
            $("#pbInternetProfileBody").css({ "background-image": "none" }).html("").append("No internet profile found").show();
        }
    });
}
/* ----------------------------------------------------------------
 * Function Name    : GPRS setting sending to customer
 * Purpose          :  GPRS setting sending to customer
 * Added by         : karthik
 * Create Date      : June 4th, 2015
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function GetAssignPermission(User_Id) {
    var url = '/Users/GetAssignPermission';;
    jQuery.support.cors = true;
    $.ajax
    ({
        url: url,
        type: 'post',

        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {

            var errcode = feedback[0].errcode;
            var id = feedback[0].Id;
            var Name = feedback[0].Name;
            var table = $('<table  id="DropPermissiion" name="DropPermissiion" width="100%"></table>');
            var counter = 0;
            $('#Inpermissionlist').empty();
            $('#Inpermissionlist').append(table);
            var i = 0;
            $(feedback).each(function () {
                if (i == 0) {
                    table.append($('<tr></tr>').append($('<td  width="200px"></td>').append($('<input>').attr({
                        type: 'checkbox', name: 'chklistitem', value: this.Value, id: this.Id
                    })).append(
               $('<label style="margin-top:-16px;margin-left:20px">').attr({
                   //for: 'chklistitem' + this.Id++
               }).text(this.Name))));
                    i = i + 1;
                }
                else {
                    table.find($('tr:last').append($('<td id="kkk" width="200px"></td>').append($('<input>').attr({
                        type: 'checkbox', name: 'chklistitem', value: this.Value, id: this.Id
                    })).append(
                $('<label style="margin-top:-16px;margin-left:20px">').attr({
                    // for: 'chklistitem' + this.Id++
                }).text(this.Name))));
                    i = i - 1;
                }

            });


        },
        error: function (feedback) {
            $("#pbInternetProfileBody").css({ "background-image": "none" }).html("").append("No internet profile found").show();
        }
    });
}

///* ---------------------------------------------------------------- 
// * Function Name    : GPRS setting sending to customer
// * Purpose          :  GPRS setting sending to customer
// * Added by         : karthik
// * Create Date      : June 4th, 2015
// * Last Update      : -
// * Update History   : -
// * ---------------------------------------------------------------- */
function validateNewenquiry() {
    var isvalid = true;

    if ($('#InEnquirerName').val() == "") {
        isvalid = false;
        $('#lblenquirername').show();
    }
    else {
        $('#lblenquirername').hide();
    }

    if ($('#InMobileNo').val() == "") {
        isvalid = false;
        $('#lblmobileno').show();
    }
    else {
        $('#lblmobileno').hide();
    }

    if ($('#iEnquiryDescription').val().trim().length <= 0) {
        isvalid = false;
        $('#lblenquirydescription').show();
    }
    else {
        $('#lblenquirydescription').hide();
    }

    if ($('#iVectoneResponse').val().trim().length <= 0) {
        isvalid = false;
        $('#lblvectoneresponse').show();
    }
    else {
        $('#lblvectoneresponse').hide();
    }

    if ($('#InEmail').val() == "") {
        isvalid = false;
        $('#lblemail').show();
    }
    else {
        $('#lblemail').hide();
    }
    return isvalid;
};

function clearfiled() {
    $('#InEnquirerName').val("");
    $('#InMobileNo').val("");
    $('#iEnquiryDescription').val("");
    $('#iVectoneResponse').val("");
    $('#InEmail').val("");
}

/* ----------------------------------------------------------------
 * Function Name    : validate New user
 * Purpose          : validate New user
 * Added by         : 
 * Create Date      : Dec 22, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function validateNewuser() {

    var isvalid = true;

    if ($('#InUsername').val() == "") {
        isvalid = false;
        $('#lblusername').show();
    }
    else {
        $('#lblusername').hide();
    }

    if ($('#inpassword').val() == "") {
        isvalid = false;
        $('#lblpwd').show();
    }
    else {
        $('#lblpwd').hide();
    }

    if ($('#inretypepassword').val() == "") {
        isvalid = false;
        $('#lblconpwd').show();
    }
    else {
        $('#lblconpwd').hide();
    }

    if ($('#infirstname').val() == "") {
        isvalid = false;
        $('#lblfirname').show();
    }
    else {
        $('#lblfirname').hide();
    }

    if ($('#inemail').val() == "") {
        isvalid = false;
        $('#lblemail').show();
    }
    else {
        $('#lblemail').hide();
    }

    var count = $("input[name='chklistitem']:checked").length;
    if (count > 0) {
        // if ($('#chklistitem input:checked').length > 0) {       
        $('#lblpermission').hide();
    }
    else {
        isvalid = false;
        $('#lblpermission').show();
    }

    if ($('#inpassword').val() === $('#inretypepassword').val()) {
        $('#lblconvalipwd').hide();
        // evt.preventDefault();
    }
    else {
        isvalid = false;
        $('#lblconvalipwd').show();
    }

    return isvalid;
};




function ValidateEmail(email) {
    var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    return expr.test(email);
};
/* ----------------------------------------------------- 
   *  eof TAB BUNDLES 
   ===================================================== */

function BindCountry() {
    var url = "/Enquiries/BindCountry";
    jQuery.support.cors = true;
    $.ajax
    ({
        url: url,
        type: 'post',
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            if (feedback != null && feedback.length > 0)
                var actionOption = '';
            for (icount = 0; icount < feedback.length; icount++) {
                actionOption += '<option value="' + feedback[icount].phonecode + '" style=\"padding:3px\">' + feedback[icount].name + '</option>';
            }
            $('#inCountry').append(actionOption);
        },
        error: function (feedback) {
        }
    });
}
function BindEnquiryType() { 
    var url = "/Enquiries/BindEnquiryType";
    jQuery.support.cors = true;
    $.ajax
    ({
        url: url,
        type: 'post',
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('#inEnquiryType').find('option').remove();
            var actionOption = '';
                for (icount = 0; icount < feedback.length; icount++) {
                    actionOption += '<option value="' + feedback[icount].Enquiry_TYPE_ID + '" style=\"padding:3px\">' + feedback[icount].Enquiry_Name + '</option>';
            }
            $('#inEnquiryType').append(actionOption);
        },
        error: function (feedback) {
        }
    });
}

function BindStatus(userID) {
    var url = "/Enquiries/BindStatus";
    jQuery.support.cors = true;
    var JSONSendData = {
        userID: userID,
    }
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            if (feedback != null && feedback.length > 0)
                var actionOption = '';
            for (icount = 0; icount < feedback.length; icount++) {
                actionOption += '<option value="' + feedback[icount].Enquiry_TYPE_ID + '" style=\"padding:3px\">' + feedback[icount].Enquiry_Name + '</option>';
            }
            $('#inEnquiryType').append(actionOption);
        },
        error: function (feedback) {
        }
    });
}


