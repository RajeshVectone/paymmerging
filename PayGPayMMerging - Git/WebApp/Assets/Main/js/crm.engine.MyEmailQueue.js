﻿function GetMyEmailQueue(userid, role_id, status_id) {
    $("#pbEmailQueueBody").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblPBEmailQueueBody\"></table>");
    $("#pbEmailQueueBody").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var url = apiServer + '/api/Emails';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 5,
        userid: userid,
        role_id: role_id,
        status_id: status_id
    };
    var row = 0;
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            console.log(feedback);
            $("#pbEmailQueueBody").css({ "background-image": "none" });
            $('#tblPBEmailQueueBody').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                iDisplayLength: 10,
                bPaginate: true,
                //bLengthChange: false,
                bFilter: true,
                bInfo: false,
                bAutoWidth: false,
                aaSorting: [[0, "desc"]],
                aoColumns: [
                    { mDataProp: "pk_queue_id", sTitle: "ID", sWidth: 50 },
                    { mDataProp: "customer_email", sTitle: "Customer Email", sWidth: 150 },
                    { mDataProp: "assigned_user", sTitle: "Assigned To", sWidth: 150 },
                    { mDataProp: "queue_status", sTitle: "Status", sWidth: 75 },
                    { mDataProp: "assigned_date_string", sTitle: "Date & Time", sWidth: 150 },
                    { mDataProp: "email_subject_trim", sTitle: "Subject", sWidth: 150 },
                    {
                        mDataProp: "email_body_trim", sTitle: "Body", sWidth: 250,
                        //mDataProp: "email_body_trim", sTitle: "Body", sWidth: 500,
                        //fnRender: function (ob) {
                        //    var actionURL = "";
                        //    actionURL = "<p data-toggle=\"tooltip\" title=\"" + ob.aData.email_body + "\">" + ob.aData.email_body_trim + "</p>";
                        //    return actionURL;
                        //}
                    },
                    {
                        mDataProp: "action", sTitle: "Action", bSortable: false, sWidth: 50,
                        fnRender: function (ob) {

                            var btnTitle = "";
                            if (ob.aData.queue_status.toUpperCase() == "ASSIGNED") {
                                btnTitle = "Open";
                            }
                            else if (ob.aData.queue_status.toUpperCase() == "INPROGRESS") {
                                btnTitle = "InProgress";
                            }
                            else if (ob.aData.queue_status.toUpperCase() == "PENDING") {
                                btnTitle = "Pending";
                            }
                            else if (ob.aData.queue_status.toUpperCase() == "RESOLVED") {
                                btnTitle = "Resolved";
                            }
                            else if (ob.aData.queue_status.toUpperCase() == "CLOSED") {
                                btnTitle = "Closed";
                            }
                            var actionURL = "";
                            actionURL = "<button id=\"open\" class=\"btn blue\" queue_status_id=\"" + ob.aData.queue_status_id + "\" statValue=\"" + ob.aData.queue_status + "\" assValue=\"" + ob.aData.assigned_user + "\" ctlvalue=\"" + ob.aData.pk_queue_id + "\" custEmail=\"" + ob.aData.customer_email + "\" style=\"width:85px\" >" + btnTitle + "</button>";
                            return actionURL;
                        }
                    },
                    {
                        mDataProp: "action2", sTitle: "Ticket", bSortable: false, sWidth: 50,
                        fnRender: function (ob) {
                            var actionURL = "";
                            if (ob.aData.queue_status.toUpperCase() == "INPROGRESS" || ob.aData.queue_status.toUpperCase() == "PENDING") {
                                actionURL = "<button id=\"view\" class=\"btn blue\"  queue_status_id=\"" + ob.aData.queue_status_id + "\" ctlvalue=\"" + ob.aData.pk_queue_id + "\" >View</button>";
                            }
                            else {
                                actionURL = "<button id=\"view\" class=\"btn lightgrey\"  queue_status_id=\"" + ob.aData.queue_status_id + "\" ctlvalue=\"" + ob.aData.pk_queue_id + "\"  disabled=\"disabled\">View</button>";
                            }
                            return actionURL;
                        }
                    },
                    {
                        mDataProp: "action3", sTitle: "Action", bSortable: false,
                        fnRender: function (ob) {
                            var actionURL = "";
                            //var btnTitle = "Remove";
                            //if ((role_id == 3 || role_id == 4 || role_id == 5) && ob.aData.queue_status.toUpperCase() == "ASSIGNED") {
                            //    actionURL = "<button id=\"remove\" class=\"btn blue\" ctlvalue=\"" + ob.aData.request_id + "\" emailvalue=\"" + ob.aData.customer_email + "\">" + btnTitle + "</button>";
                            //}
                            //else {
                            //    actionURL = "<button id=\"remove\" class=\"btn lightgrey\" ctlvalue=\"" + ob.aData.request_id + "\" emailvalue=\"" + ob.aData.customer_email + "\" disabled=\"disabled\">" + btnTitle + "</button>";
                            //}
                            actionURL = "<button id=\"remove\" class=\"btn blue\" ctlvalue=\"" + ob.aData.request_id + "\" emailvalue=\"" + ob.aData.customer_email + "\">Remove</button>";
                            return actionURL;
                        }
                    },
                ],
                fnDrawCallback: function () {
                    //if ($('#tblPBEmailQueueBody tr').length == 2) {
                    //    $('.dataTables_paginate').hide();
                    //}
                    //else {
                    //    $('.dataTables_paginate').show();
                    //}
                }

            });
        },
        error: function (feedback) {
            $("#pbEmailQueueBody").css({ "background-image": "none" });
            $('#pbEmailQueueBody ul').append("No records found.");
        }
    });
}

//My Email Queue Reply
function MyEmailQueueReply(queue_id, email_to, email_cc, email_subject, email_body, is_closed, filename, custemail) {
    $("#ajax-screen-masking").show();
    var url = apiServer + '/api/MailChimp';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 7,
        queue_id: queue_id,
        email_to: email_to,
        email_cc: email_cc,
        email_subject: email_subject,
        email_body: email_body,
        is_closed: is_closed,
        filename: filename,
        email_cc: custemail
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: true,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            if (feedback[0].errcode == 0) {
                $("#editor").val('');
                alert("Email sent successfully!");
                //$('#divMyEmailOpen').modal('hide');
                window.location = "/Emails/EmailList/1";
            }
            else {
                alert(feedback[0].errmsg);
            }
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}

//My Email Queue Generate Ticket
function MyEmailQueueGenerateTicket(CustomerName, MobileNo, TicketType, Category, SubCategory, Status, EscalatingTeam, Description, pkqueueid, product_code, pkqueueid, process_type, userid, emailto, EscalatingTeamName) {
    $("#ajax-screen-masking").show();
    var url = apiServer + '/api/MailChimp';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 9,
        CustomerName: CustomerName,
        MobileNo: MobileNo,
        TicketType: TicketType,
        Category: Category,
        SubCategory: SubCategory,
        Status: Status,
        EscalatingTeam: EscalatingTeam,
        Description: Description,
        pkqueueid: pkqueueid,
        product_code: product_code,
        pkqueueid: pkqueueid,
        process_type: process_type,
        userid: userid,
        emailto: emailto,
        EscalatingTeamName: EscalatingTeamName
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: true,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            if (feedback != null && feedback.length > 0) {
                if (feedback[0].errcode == 0) {
                    alert(feedback[0].errmsg + " : " + feedback[0].ticket_id);
                    window.location = "/Emails/EmailList/1";
                } else {
                    alert(feedback[0].errmsg);
                }
            }
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}
