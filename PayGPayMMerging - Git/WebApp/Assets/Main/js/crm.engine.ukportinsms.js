﻿/* ===================================================== 
   *  UK PORT IN - SMS REQUEST
   ----------------------------------------------------- */

/* ----------------------------------------------------------------
 * Function Name    : UKPortINSMSRequestList
 * Purpose          : to display SMS request
 * Added by         : Edi Suryadi
 * Create Date      : September 24th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function UKPortINSMSRequestList(SMSReportFilter) {

    $("#UKPortINSMSStep1_SMSListContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"UKPortINSMSStep1_SMSListTbl\"></table>");
    $('#UKPortINSMSStep1').height(120);

    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    var url = apiServer + '/api/ukporting/';
    jQuery.support.cors = true;
    var JSONSendData = {
        SubType: 8,
        SMSReportFilter : SMSReportFilter,
        Sitecode: "MCM"
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function (feedback) {
            //$("select[name=UKPortINSMSStep1_FiletrBy]").attr("disabled", true);
            $("#ajax-screen-masking").show();
            $('#UKPortINSMSStep1_SMSListContainer').hide();
        },
        success: function (feedback) {
            //$("#UKPortINSMSStep1_SMSListContainer").css({ "background-image": "none" });
            //$("select[name=UKPortINSMSStep1_FiletrBy]").removeAttr("disabled");

            if (SMSReportFilter == '1') {
                var oTable = $('#UKPortINSMSStep1_SMSListTbl').dataTable({
                    bDestroy: true,
                    bRetrieve: true,
                    aaData: feedback,
                    bPaginate: true,
                    bFilter: true,
                    bInfo: true,
                    iDisplayLength: 10,
                    aaSorting: [[0, "desc"]],
                    oLanguage: {
                        sInfo: "Total <b>_TOTAL_</b> request(s) found."
                    },
                    aoColumns: [
                        { mDataProp: "TimeStamp", sTitle: "TimeStamp", bVisible: false },
                        { mDataProp: "PAC", sTitle: "PAC" },
                        {
                            mDataProp: "Sms_Id", sTitle: "Process",
                            fnRender: function (ob) {
                                var actionURL = "";
                                actionURL = "<a alt=\"" + ob.aData.Sms_Id + "\" class=\"UKPortINSMSStep1_btnProcess\" href=\"javascript:void(0)\">Process SMS</a>";
                                return actionURL;
                            }
                        },
                        { mDataProp: "Msisdn", sTitle: "MSISDN" },
                        { mDataProp: "BB_Msisdn", sTitle: "Vectone Mobile" },
                        { mDataProp: "Err_Message", sTitle: "Status", sWidth: "100px" },
                        { mDataProp: "TimeStamp_String", sTitle: "Request Received Time" },
                        { mDataProp: "Service_Number", sTitle: "Service Number" },
                    ]
                });
            } else if (SMSReportFilter == '0') {
                var oTable = $('#UKPortINSMSStep1_SMSListTbl').dataTable({
                    bDestroy: true,
                    bRetrieve: true,
                    aaData: feedback,
                    bPaginate: true,
                    bFilter: true,
                    bInfo: true,
                    iDisplayLength: 10,
                    aaSorting: [[0, "asc"]],
                    oLanguage: {
                        sInfo: "Total <b>_TOTAL_</b> request(s) found."
                    },
                    aoColumns: [
                        { mDataProp: "TimeStamp", sTitle: "TimeStamp", bVisible: false },
                        { mDataProp: "PAC", sTitle: "PAC" },
                        { mDataProp: "Msisdn", sTitle: "MSISDN" },
                        { mDataProp: "BB_Msisdn", sTitle: "Vectone Mobile" },
                        { mDataProp: "Err_Message", sTitle: "Status", sWidth: "100px" },
                        { mDataProp: "TimeStamp_String", sTitle: "Request Received Time" },
                        { mDataProp: "Service_Number", sTitle: "Service Number" },
                    ]
                });
            }
            $("#UKPortINSMSStep1_SMSListTbl_wrapper .row-fluid .span6:eq(0)").html("");
            $("#UKPortINSMSStep1_SMSListTbl_wrapper .row-fluid .span6:eq(2)").addClass("span4");
            $("#UKPortINSMSStep1_SMSListTbl_wrapper .row-fluid .span6:eq(3)").addClass("span8");
            $("#UKPortINSMSStep1_SMSListTbl_wrapper .row-fluid:eq(1) .span6").removeClass("span6");

            var UKPortINSMSStep1Height = $('#UKPortINSMSStep1').height();
            var UKPortINSMSStep1_SMSListTblHeight = $("#UKPortINSMSStep1_SMSListContainer").height();
            $('#UKPortINSMSStep1').height(UKPortINSMSStep1Height + UKPortINSMSStep1_SMSListTblHeight + 10);
            $("#ajax-screen-masking").hide();
            $("#UKPortINSMSStep1_SMSListContainer").show();

        },
        error: function (feedback) {
            $("select[name=UKPortINSMSStep1_FiletrBy]").removeAttr("disabled");
            $("#UKPortINSMSStep1_SMSListContainer").css({ "background-image": "none" }).html("").append("No SMS request found").show();
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : UKPortINSMSRequestDetail
 * Purpose          : to view detail of SMS request
 * Added by         : Edi Suryadi
 * Create Date      : September 24th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function UKPortINSMSRequestDetail(SmsId) {
    var btnSubmitPortIn = "<button class=\"btn blue\" id=\"UKPortINSMSStep2_btnSubmitPortIn\" style=\"width:150px;height:40px;margin-right:10px;\">Submit Port In</button>";
    var btnDeleteSMS = "<button class=\"btn blue\" id=\"UKPortINSMSStep2_btnDeleteSMS\" style=\"width:150px;height:40px\">Delete SMS</button>";

    var url = apiServer + '/api/ukporting/';

    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    jQuery.support.cors = true;
    var JSONSendData = {
        SmsId: SmsId,
        Sitecode: "MCM",
        SubType: 13
    };

    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();
            var valCode = data.Validate_Code;
            if (valCode == 0) {
                $("#UKPortINSMSStep2_btnContainer").html("").append(btnSubmitPortIn);
                $("#UKPortINSMSStep2_btnContainer").append(btnDeleteSMS);
            } else {
                $("#UKPortINSMSStep2_btnContainer").html("").append(btnDeleteSMS);
            }

            $("input[name=UKPortINSMSStep2_hSMSID]").val(SmsId);
            $("#UKPortINSMSStep2_ValidateMessage").html(data.Validate_Message);
            $("#UKPortINSMSStep2_iBarabluMSISDN").html(data.BB_Msisdn);
            $("#UKPortINSMSStep2_iMSISDN").html(data.Msisdn);
            $("input[name=UKPortINSMSStep2_iPAC]").val(data.PAC);

            $("#UKPortINSMSStep1").hide();
            $("#UKPortINSMSStep2").fadeIn();
        },
        error: function (data) {
            $("#ajax-screen-masking").hide();
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            $('.PMFailed .modal-header').html(data.errsubject);
            $('.PMFailed .modal-body').html(data.errmsg);
            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : UKPortINSMSRequestDelete
 * Purpose          : to cancel SMS request
 * Added by         : Edi Suryadi
 * Create Date      : September 24th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function UKPortINSMSRequestDelete(SmsId) {
    var url = apiServer + '/api/ukporting/';

    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    jQuery.support.cors = true;
    var JSONSendData = {
        SmsId: SmsId,
        Sitecode: "MCM",
        SubType: 34
    };

    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();

            var stat = data.errcode;
            if (stat == 0) {
                $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
                $('.PMSuccess .modal-header').html(data.errsubject);
                $('.PMSuccess .modal-body').html(data.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            } else {
                $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "2" });
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (data) {
            $("#ajax-screen-masking").hide();
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            $('.PMFailed .modal-header').html(data.errsubject);
            $('.PMFailed .modal-body').html(data.errmsg);
            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : UKPortINSMSRequestSubmit
 * Purpose          : to submit SMS request
 * Added by         : Edi Suryadi
 * Create Date      : September 24th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function UKPortINSMSRequestSubmit(Msisdn, PAC, PortinMsisdn, PortDate, LoggedUser) {
    var url = apiServer + '/api/ukporting/';

    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    jQuery.support.cors = true;
    var JSONSendData = {
        Msisdn: Msisdn,
        PAC: PAC,
        PortinMsisdn: PortinMsisdn,
        PortDate: PortDate,
        LoggedUser: LoggedUser,
        Sitecode: "MCM",
        SubType: 21
    };

    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();

            var stat = data.errcode;
            if (stat == 0) {
                $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
                $('.PMSuccess .modal-header').html(data.errsubject);
                $('.PMSuccess .modal-body').html(data.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            } else {
                $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "2" });
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (data) {
            $("#ajax-screen-masking").hide();
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            $('.PMFailed .modal-header').html(data.errsubject);
            $('.PMFailed .modal-body').html(data.errmsg);
            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });
}


/* ----------------------------------------------------- 
   *  eof UK PORT IN - SMS REQUEST
   ===================================================== */






