﻿/* ===================================================== 
   *  CSB TAB 
   ----------------------------------------------------- */

/* ----------------------------------------------------------------
 * Function Name    : SCBTab
 * Purpose          : To display Somalia Country Saver data table
 * Added by         : Edi Suryadi
 * Create Date      : August 1st, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function CSBTab(mobileNo) {
    $("#CSBMainWrapper").append("<table class=\"table table-bordered table-hover\" id=\"tbcsb\"></table>");
    var url = apiServer + '/api/customercsb/' + mobileNo
    $.getJSON(url, function (data) {

        var oTable = $('#tbcsb').dataTable({
            aaData: data,
            bDestroy: true,
            bRetrieve: true,
            bFilter: false,
            bLengthChange: false,
            aaSorting: [[0, "asc"]],
            oLanguage: {
                "sInfo": 'Found <b>_TOTAL_</b> bundle(s).',
                "sInfoEmpty": '',
                "sEmptyTable": "No bundle found."
            },
            aoColumns: [

				{ mDataProp: "account_id", sTitle: 'No' },
				{ mDataProp: "dest_msisdn", sTitle: 'Dest. Number' },
				{ mDataProp: "dest_country", sTitle: 'Country' },
                { mDataProp: "new_sim", sTitle: 'New SIM' },
				{ mDataProp: "subscription_string", sTitle: 'Subscription' },
				{ mDataProp: "expiry_string", sTitle: 'Expiry' },
                { mDataProp: "renewal_method", sTitle: 'Renewal Method' },
                { mDataProp: "status", sTitle: 'Status' },
                { mDataProp: "remaining", sTitle: 'Remain' },
				{
				    mDataProp: "account_id", sTitle: 'Actions', bSortable: false,
				    fnRender: function (ob) {
				        if (ob.aData.new_sim != null) {
				            var isNew = 0;
				            if (ob.aData.new_sim.toLowerCase() == 'yes') {
				                var isNew = 1;
				            }
				        } else {
				            var isNew = 0;
				        }
				        
				        var content = '<select class="selectCSBAction" onchange="CSBAction(this.value)" alt="' + ob.aData.account_id  + '">';
				        content += '<option value="0">--- Select an action ---</option>';
				        content += '<option value="1_' + ob.aData.account_id + '|' + ob.aData.dest_msisdn + '|' + mobileNo + '|' + ob.aData.dest_country + '|' + ob.aData.renewal_method + '|' + ob.aData.order_id + '|' + isNew + '">View History</option>';
				        if (ob.aData.status !== null) {
				            if (ob.aData.status.toLowerCase() === 'active') {
				                if (ob.aData.renewal_method.toLowerCase() !== 'cancelled') {
				                    content += '<option value="2_' + ob.aData.account_id + '|' + ob.aData.dest_msisdn + '|' + mobileNo + '|' + ob.aData.dest_country + '|' + ob.aData.renewal_method + '|' + ob.aData.order_id + '|' + isNew + '">Change Payment Method</option>';
				                    content += '<option value="3_' + ob.aData.account_id + '|' + ob.aData.dest_msisdn + '|' + mobileNo + '|' + ob.aData.dest_country + '|' + ob.aData.renewal_method + '|' + ob.aData.order_id + '|' + isNew + '">Cancel</option>';
				                } 
				            } else if (ob.aData.status.toLowerCase() === 'inactive') {
				                content += '<option value="4_' + ob.aData.account_id + '|' + ob.aData.dest_msisdn + '|' + mobileNo + '|' + ob.aData.dest_country + '|' + ob.aData.renewal_method + '|' + ob.aData.order_id + '|' + isNew + '">Re-Activate</option>';
				            }
				        }
				        content += '</select>';
				        return content;
				    }
				}
            ],
            fnDrawCallback: function (ob) {
                if (ob.bSorted || ob.bFiltered) {
                    for (var i = 0, iLen = ob.aiDisplay.length ; i < iLen ; i++) {
                        $('td:eq(0)', ob.aoData[ob.aiDisplay[i]].nTr).html(i + 1);
                    }
                }
                $("table#tbcsb select.selectCSBAction").css({ "color": "#a0a0a0" });
                $("table#tbcsb select.selectCSBAction option").not(":eq(0)").css({ "color": "#000000" });
                $("table#tbcsb option").css({ "padding": "3px 5px" });
                
            }
        });
        getLastCardUsed(mobileNo);
        $("#CSBMainWrapper").css({ "background-image": "none" });
        $('#tbcsb').show();
    })
}
/* ---------------------------------------------------------------- */

/* ----------------------------------------------------------------
 * Function Name    : CSBAction
 * Purpose          : To handle action in CSB tab
 * Added by         : Edi Suryadi
 * Create Date      : August 2nd, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function CSBAction(data) {
    if (data != '0') {
        var x = data.split('_');
        var actionID = x[0];
        var y = x[1].split('|');
        var account_id = $.trim(y[0]);
        var destno =  $.trim(y[1]);
        var mobileno =  $.trim(y[2]);
        var destcountry =  $.trim(y[3]);
        var renewalmethod =  $.trim(y[4]);
        var orderid =  $.trim(y[5]);
        var isNew =  $.trim(y[6]);

        switch (parseInt(actionID)) {
            case 1:
                /* function CSBActionViewHistory(mobileno, destno) */
                $("input[name=tabCSBMode]").val("viewhist");
                $("#CSBWindowHistory #tblCSBViewHistoryCont").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblCSBViewHistory\"></table>");
                $("#tblCSBViewHistoryCont").css({ "background-image": "url('/img/loader.gif')" });
                $('#CSBWindowHistory').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
                var str_destno = destno;
                if (destno === 'null' || destno === null || destno === '') {
                    str_destno = '-';
                }
                $("#CSBWindowHistory #destno_data").html(str_destno);
                $("input[name=DestNo]").val(destno);
                $("input[name=OrderID]").val(orderid);
                CSBActionViewHistory($.trim(isNew), mobileno, destno);
                break;
            case 2:
                /* function CSBActionChangePaymentMethod(mobileno, destno) */
                $("input[name=tabCSBMode]").val("cpm");
                $('#CSBCPMBundle').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
                if (renewalmethod.toLowerCase().search("card") >= 0) {
                    $("#CSBCPMBundle").css({ "width": "550px" });
                    $("#CSBCPMBundle span.currentPaymentMethod").html("<b>Card</b>");
                    $("#btnCPMPayWithCredit, #btnCPMChangeCard").show();
                    $("#btnCPMPayWithCard").hide();
                    $("#CSBCPMCardDetail").show();
                } else {
                    $("#CSBCPMBundle").css({ "width": "400px" });
                    $("#CSBCPMBundle span.currentPaymentMethod").html("<b>Balance</b>");
                    $("#CSBCPMCardDetail").hide();
                    $("#btnCPMPayWithCredit, #btnCPMChangeCard").hide();
                    $("#btnCPMPayWithCard").show();
                }
                
                $("input[name=DestNo]").val(destno);
                $("input[name=OrderID]").val(orderid);
                break;
            case 3:
                /* function CSBActionCancel(mobileno, destno) */
                $("input[name=tabCSBMode]").val("cancel");
                $('#CSBCancelBundle').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
                $("#CSBCancelBundle span.bundleCountry").html("<b>" + destcountry + "</b>");
                $("input[name=DestNo]").val(destno);
                $("input[name=OrderID]").val(orderid);
                break;
            case 4:
                /* Action Reactivate */
                $("input[name=tabCSBMode]").val("reactivate");
                $("#CSBReactivateBundle #CSBReactivateDestNo").val(destno);
                $("#CSBReactivateBundle #CSBReactivateOrderID").val(orderid);
                $("#CSBReactivateNowBalance #CSBReactivateNowBalanceDestNo").val(destno);
                $("#CSBReactivateNowBalance #CSBReactivateNowBalanceOrderID").val(orderid);
                $("#CSBReactivateNowCard #CSBReactivateNowCardDestNo").val(destno);
                $("#CSBReactivateNowCard #CSBReactivateNowCardOrderID").val(orderid);

                $('#CSBReactivateBundle').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
                if (renewalmethod.toLowerCase().search("card") >= 0) {
                    $("#lastPaymentMethod").html("Current payment mode is pay with <b>Card</b>");
                    $("#btnCSBReactivatePayWithCredit").html("Switch to Pay with Balance").css({ "width": "200px" }).show();
                    $("#btnCSBReactivateBundleByCard").html("Change Card").css({ "width": "130px" }).show();
                    $("#btnCSBReactivateWrapper").css({ "text-align": "center" });
                    $("#CSBReactivateBundle").css({ "width": "500px", "height": "330px" });
                    $("#lastPaymentMethod").css({ "height": "20px" });
                    $("#CSBReactivateWantToChange").show();
                    $("#wrapperBtnCSBReactivateNow").show();
                    $("#btnCSBReactivateNow").attr({ "alt": "1" });

                    $("#CSBReactivateCardDetail").show();
                } else if (renewalmethod.toLowerCase().search("balance") >= 0) {
                    $("#lastPaymentMethod").html("Current payment mode is pay with <b>Balance</b>");
                    $("#btnCSBReactivatePayWithCredit").hide();
                    $("#btnCSBReactivateBundleByCard").html("Switch to Pay with Card").css({ "width": "200px" }).show();
                    $("#btnCSBReactivateWrapper").css({ "text-align": "right" });
                    $("#CSBReactivateBundle").css({ "width": "450px", "height": "240px" });
                    $("#lastPaymentMethod").css({ "height": "20px" });
                    $("#CSBReactivateWantToChange").show();
                    $("#wrapperBtnCSBReactivateNow").show();
                    $("#btnCSBReactivateNow").attr({ "alt": "0" });
                    $("#CSBReactivateCardDetail").hide();
                } else {
                    $("#lastPaymentMethod").html("You have been requested to reactivate bundle <b>" + destcountry + "</b>");
                    $("#btnCSBReactivatePayWithCredit").html("Pay with Balance").css({ "width": "160px" }).show();
                    $("#btnCSBReactivateBundleByCard").html("Pay with Card").css({ "width": "160px" }).show();
                    $("#btnCSBReactivateWrapper").css({ "text-align": "center" });
                    $("#CSBReactivateBundle").css({ "width": "500px", "height": "240px" });
                    $("#lastPaymentMethod").css({ "height": "110px" });
                    $("#CSBReactivateWantToChange").hide();
                    $("#wrapperBtnCSBReactivateNow").hide();
                    $("#CSBReactivateCardDetail").hide();
                }

                $("input[name=DestNo]").val(destno);
                $("input[name=OrderID]").val(orderid);
                break;
        }
        $("table#tbcsb .selectCSBAction").val('0');
    }
}
/* ---------------------------------------------------------------- */

/* ----------------------------------------------------------------
 * Function Name    : CSBActionViewHistory
                    : (CSBAction --> No. 1)
 * Purpose          : To view history
 * Added by         : Edi Suryadi
 * Create Date      : August 5th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function CSBActionViewHistory(isNewSIM, mobileno, destno) {
    var url = apiServer + '/api/customercsb/';
    var orderrec = 1;
    if (isNewSIM == 1) {
        orderrec = 1;
    }
    jQuery.support.cors = true;
    var JSONSendData = {
        order_records: orderrec,
        mobileno: mobileno,
        destno: destno
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            if (isNewSIM == 1) {
                $('#tblCSBViewHistory').dataTable({
                    bDestroy: true,
                    bRetrieve: true,
                    aaData: feedback,
                    bPaginate: true,
                    bFilter: false,
                    bInfo: false,
                    iDisplayLength: 1,
                    aaSorting: [[0, "asc"]],
                    oLanguage: {
                        "sEmptyTable": 'No history found'
                    },
                    aoColumns: [
                        { mDataProp: "logdate", sTitle: 'Date' },
                        {
                            mDataProp: "description", sTitle: 'Notes',
                            fnRender: function (ob) {
                                var desc = ob.aData.description;
                                var descSplit = desc.split('|');
                                var firstName =  descSplit[0];
                                var lastName = descSplit[0];
                                var recipientName = firstName + ' ' + lastName;
                                var recipientContactNo =  descSplit[2];
                                var houseNo =  descSplit[3];
                                var address = descSplit[4];
                                var city = descSplit[5];
                                var country = descSplit[6];
                                
                                var content = '';

                                content += 'New SIM Ordered<br/>';
                                content += '<b>Recipient Name</b><br/>';
                                content += recipientName + '<br/>';
                                content += '<b>Recipient Contact Number</b><br/>';
                                content += recipientContactNo + '<br/>';
                                content += '<b>Recipient Address</b><br/>';
                                content += houseNo + ' ' + address + '<br/>';
                                content += city + '<br/>';
                                content += country;

                                return content;
                            }
                        }
                    ],
                    fnDrawCallback: function (ob) {
                        $("table#tblCSBViewHistory thead tr th").eq(0).css({ "width": "120px" });
                    }
                });
            } else {
                $('#tblCSBViewHistory').dataTable({
                    bDestroy: true,
                    bRetrieve: true,
                    aaData: feedback,
                    bPaginate: true,
                    bFilter: false,
                    bInfo: false,
                    iDisplayLength: 5,
                    aaSorting: [[0, "asc"]],
                    oLanguage: {
                        "sEmptyTable": 'No history found'
                    },
                    aoColumns: [
                        { mDataProp: "logdate", sTitle: 'Date' },
                        { mDataProp: "action", sTitle: 'Status' },
                        { mDataProp: "description", sTitle: 'Details' }
                    ],
                    fnDrawCallback: function (ob) {
                        $("table#tblCSBViewHistory thead tr th").eq(0).css({ "width": "120px" });
                    }
                });

            }
            $("#tblCSBViewHistory_wrapper .row-fluid:eq(0)").remove();
            $("#tblCSBViewHistory_wrapper .row-fluid .span6:eq(0)").remove();
            $("#tblCSBViewHistory_wrapper .row-fluid .span6").addClass("span12").removeClass("span6");
            $("#tblCSBViewHistoryCont").css({"background-image" : "none"});
        }
    });
}
/* ---------------------------------------------------------------- */

/* ----------------------------------------------------------------
 * Function Name    : CSBActionChangePaymentMethodByCredit 
                    : (CSBAction --> No. 2a)
 * Purpose          : To change method payment to balance 
 * Added by         : Edi Suryadi
 * Create Date      : August 26th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function CSBActionChangePaymentMethodByCredit(mobileno, destno) {

    var url = apiServer + '/api/customercsb/';
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    jQuery.support.cors = true;
    var JSONSendData = {
        order_records: 4,
        mobileno: mobileno,
        destno: destno
    };

    $("#orderDetail").hide();
    $(".modal-backdrop").hide();

    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();

            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "0" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "0" });

            var stat = data.errcode;
            if (stat == 0) {
                $('.PMSuccess .modal-header').html(data.errsubject);
                $('.PMSuccess .modal-body').html(data.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            } else {
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : CSBActionChangePaymentMethodByNewCard 
                    : (CSBAction --> No. 2b)
 * Purpose          : To change method payment to new card 
 * Added by         : Edi Suryadi
 * Create Date      : August 27th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function CSBActionChangePaymentMethodByCard(p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19) {
    var url = apiServer + '/api/customercsb/';
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    jQuery.support.cors = true;
    var JSONSendData = {
        order_records: 5,
        mobileno: p1,
        destno: p2,
        crm_login: p3,
        card_number: p4,
        card_expiry_month: p5,
        card_expiry_year: p6,
        card_issue_number: p7,
        card_verf_code: p8,
        card_first_name: p9,
        card_last_name: p10,
        card_post_code: p11,
        card_house_number: p12,
        card_address: p13,
        card_city: p14,
        card_country: p15,
        card_type: p16,
        order_id: p17,
        SubscriptionID: p18,
        site_code : p19
    };
    $.ajax
    ({
        url: url,
        type: 'POST',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();

            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });

            var stat = data.errcode;
            if (stat == 0) {
                $('.PMSuccess .modal-header').html(data.errsubject);
                $('.PMSuccess .modal-body').html(data.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            } else {
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (data) {
            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            $("#ajax-screen-masking").hide();
            $('.PMFailed .modal-header').html(data.errsubject);
            $('.PMFailed .modal-body').html(data.errmsg);
            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : CSBActionCancel 
                    : (CSBAction --> No. 3)
 * Purpose          : To cancel the CSB bundle 
 * Added by         : Edi Suryadi
 * Create Date      : August 22th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function CSBActionCancel(mobileno, destno) {

    var url = apiServer + '/api/customercsb/';
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    jQuery.support.cors = true;
    var JSONSendData = {
        order_records: 2,
        mobileno: mobileno,
        destno: destno
    };
    $("#CSBCancelBundle").hide();
    $(".modal-backdrop").hide();

    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();

            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });

            var stat = data.errcode;
            if (stat == 0) {
                $('.PMSuccess .modal-header').html(data.errsubject);
                $('.PMSuccess .modal-body').html(data.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            } else {
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (data) {
            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            $("#ajax-screen-masking").hide();
            $('.PMFailed .modal-header').html(data.errsubject);
            $('.PMFailed .modal-body').html(data.errmsg);
            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : CSBActionReactivateByCredit 
                    : (CSBAction --> No. 4a)
 * Purpose          : To reactivate bundle with balance 
 * Added by         : Edi Suryadi
 * Create Date      : August 26th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function CSBActionReactivateByCredit(mobileno, destno, orderID) {

    var url = apiServer + '/api/customercsb/';
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    jQuery.support.cors = true;
    var JSONSendData = {
        order_records: 6,
        mobileno: mobileno,
        destno: destno,
        order_id: orderID
    };
    $("#CSBReactivateBundle").hide();
    $(".modal-backdrop").hide();

    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();

            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });

            var stat = data.errcode;
            if (stat == 0) {
                $('.PMSuccess .modal-header').html(data.errsubject);
                $('.PMSuccess .modal-body').html(data.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            } else {
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (data) {
            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            $("#ajax-screen-masking").hide();
            $('.PMFailed .modal-header').html(data.errsubject);
            $('.PMFailed .modal-body').html(data.errmsg);
            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : CSBActionReactivateByCard 
                    : (CSBAction --> No. 4b)
 * Purpose          : To reactivate bundle with card 
 * Added by         : Edi Suryadi
 * Create Date      : August 28th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function CSBActionReactivateByCard(p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19) {
    var url = apiServer + '/api/customercsb/';
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    jQuery.support.cors = true;
    var JSONSendData = {
        order_records: 3,
        mobileno: p1,
        destno: p2,
        crm_login: p3,
        card_number: p4,
        card_expiry_month: p5,
        card_expiry_year: p6,
        card_issue_number: p7,
        card_verf_code: p8,
        card_first_name: p9,
        card_last_name: p10,
        card_post_code: p11,
        card_house_number: p12,
        card_address: p13,
        card_city: p14,
        card_country: p15,
        card_type: p16,
        order_id: p17,
        SubscriptionID: p18,
        site_code: p19
    };
    $.ajax
    ({
        url: url,
        type: 'POST',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();

            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });

            var stat = data.errcode;
            if (stat == 0) {
                $('.PMSuccess .modal-header').html(data.errsubject);
                $('.PMSuccess .modal-body').html(data.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            } else {
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }

        },
        error: function (data) {
            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            $("#ajax-screen-masking").hide();
            $('.PMFailed .modal-header').html(data.errsubject);
            $('.PMFailed .modal-body').html(data.errmsg);
            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : getLastCardUsed
 * Purpose          : get Last Credit Card Used
 * Added by         : Edi Suryadi
 * Create Date      : August 27th, 2013
 * Last Update      : September 2nd, 2013
 * Update History   : -
 * ---------------------------------------------------------------- */

function getLastCardUsed(account_id) {
    var url = apiServer + '/api/payment/';
    jQuery.support.cors = true;
    var JSONSendData = {
        order_records: 7,
        account_id: account_id
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            var cardNo = "<span style=\"color:#b0b0b0\">XXXXXXXXXX</span>" + feedback.Last6digitsCC;
            $("#CSBCPMCCNoStr").html(cardNo);
            $("[name=CSBCPMCCNo]").val(feedback.Last6digitsCC);
            $("[name=CSBCPMSubsID]").val(feedback.SubscriptionID);

            $("#CSBReactivateCCNoStr").html(cardNo);
            $("[name=CSBReactivateCCNo]").val(feedback.Last6digitsCC);
            $("[name=CSBReactivateSubsID]").val(feedback.SubscriptionID);

            $("[name=CSBReactivateNowCardCCNo]").val(feedback.Last6digitsCC);
            $("[name=CSBReactivateNowCardSubsID]").val(feedback.SubscriptionID);
        },
        error: function (feedback) {

        }
    });

}

/* ----------------------------------------------------------------
 * Function Name    : ValidateCSBPaymentMethodForm
 * Purpose          : to validate CSB form
 * Added by         : Edi Suryadi
 * Create Date      : October 16th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function ValidateCSBPaymentMethodForm() {
    var passed = true;
    $("#PaymentMethodNewCardDetails label.errLabel").hide();

    var typeCard = $("input:radio[name=PaymentMethodCardType]:checked").val();
    if (typeCard == null) {
        $("#PaymentMethodNewCardDetails label.errLabel").eq(0).show();
        passed = false;
    }

    var cardNo = $("#PaymentMethodCCNumber").val();
    if (cardNo == "") {
        $("#PaymentMethodNewCardDetails label.errLabel").eq(1).show();
        passed = false;
    }

    var expMonth = $("#PaymentMethodExpiryMonth").val();
    var expYear = $("#PaymentMethodExpiryYear").val();
    if (expMonth == 0 || expYear == 0) {
        $("#PaymentMethodNewCardDetails label.errLabel").eq(2).show();
        passed = false;
    }

    var cardVerf = $("#PaymentMethodCardVerfCode").val();
    if (cardVerf == "") {
        $("#PaymentMethodNewCardDetails label.errLabel").eq(3).show();
        passed = false;
    }

    var firstName = $("#PaymentMethodFirstName").val();
    if (firstName == "") {
        $("#PaymentMethodNewCardDetails label.errLabel").eq(4).show();
        passed = false;
    }

    var lastName = $("#PaymentMethodLastName").val();
    if (lastName == "") {
        $("#PaymentMethodNewCardDetails label.errLabel").eq(5).show();
        passed = false;
    }

    var postCode = $("#PaymentMethodPostcode").val();
    if (postCode == "") {
        $("#PaymentMethodNewCardDetails label.errLabel").eq(6).show();
        passed = false;
    }

    var houseNo = $("#PaymentMethodHouseNo").val();
    if (houseNo == "") {
        $("#PaymentMethodNewCardDetails label.errLabel").eq(7).show();
        passed = false;
    }

    var address = $("#PaymentMethodAddress").val();
    if (address == "") {
        $("#PaymentMethodNewCardDetails label.errLabel").eq(8).show();
        passed = false;
    }

    var city = $("#PaymentMethodCity").val();
    if (city == "") {
        $("#PaymentMethodNewCardDetails label.errLabel").eq(9).show();
        passed = false;
    }

    var Email = $("input[name=PaymentMethodEmail]").val();
    if (Email != "" && !validateEmail(Email)) {
        $("#PaymentMethodNewCardDetails label.errLabel").eq(10).show();
        passed = false;
    }

    return passed;
}

/* ----------------------------------------------------------------
 * Function Name    : addNewNotes
 * Purpose          : To add new notes in account history
 * Added by         : Edi Suryadi
 * Create Date      : August 2nd, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
//function addNewNotes(v1) {
//    var url = apiServer + '/api/accountnoteshistory';
//    jQuery.support.cors = true;
//    var notesData = {
//        newNote: v1
//    };

//    $.ajax
//    ({
//        url: url,
//        type: 'post',
//        data: JSON.stringify(notesData),
//        dataType: 'json',
//        contentType: "application/json;charset=utf-8",
//        cache: false,
//        beforeSend: function () {
//            $("#ajax-screen-masking").show();
//        },
//        success: function (dataBalikan) {
//            $("#ajax-screen-masking").hide();
//            var stat = dataBalikan.errcode;
//            if (stat == 0) {
//                alert('New note has successfully added...');
//            }
//            else {
//                alert(dataBalikan.errmsg);
//            }
//        },
//        error: function () {
//            $("#ajax-screen-masking").hide();
//        }
//    });


//}
/* ---------------------------------------------------------------- */

/* ----------------------------------------------------- 
   *  eof CSB TAB 
   ===================================================== */