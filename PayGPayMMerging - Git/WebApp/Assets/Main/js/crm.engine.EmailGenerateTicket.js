﻿function FillTicketType(sitecode) {
    $("#ajax-screen-masking").show();
    $('#ddlEGTTicketType').find('option').remove();
    var url = apiServer + '/api/EmailGenerateTicket';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 1,
        sitecode: sitecode
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: true,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            var feedbackarr = jQuery.makeArray(JSON.stringify(feedback).split('}'));
            var icount;
            var actionOption = '<option value="0" style=\"color:#c0c0c0\">--- Select ---</option>';
            for (icount = 0; icount < feedbackarr.length - 1; icount++) {
                actionOption += '<option ctlValue="' + feedback[icount].Issue_Id + '"   value="' + feedback[icount].Issue_Id + '" style=\"padding:3px\">' + feedback[icount].Issue_Name + '</option>';
            }
            $('#ddlEGTTicketType').append(actionOption);
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}

function FillStatus(userid) {
    $('#ddlEGTStatus').find('option').remove();
    //var actionOption = '<option value="-1" style=\"color:#c0c0c0\">--- Select ---</option>';
    //actionOption +='<option ctlValue="' + "2" + '"   value="' + "2" + '" style=\"padding:3px\">' + "Inprogress" + '</option>';
    //actionOption +='<option ctlValue="' + "3" + '"   value="' + "3" + '" style=\"padding:3px\">' + "Resolved" + '</option>';
    //actionOption += '<option ctlValue="' + "4" + '"   value="' + "4" + '" style=\"padding:3px\">' + "Pending" + '</option>';
    var actionOption = '<option ctlValue="' + "2" + '"   value="' + "2" + '" style=\"padding:3px\">' + "Inprogress" + '</option>';
    $('#ddlEGTStatus').append(actionOption);

    //var url = apiServer + '/api/EmailGenerateTicket';
    //jQuery.support.cors = true;
    //var JSONSendData = {
    //    modetype: 5,
    //    userid: userid
    //};
    //$.ajax
    //({
    //    url: url,
    //    type: 'post',
    //    data: JSON.stringify(JSONSendData),
    //    dataType: 'json',
    //    contentType: "application/json;charset=utf-8",
    //    cache: true,
    //    success: function (feedback) {
    //        var feedbackarr = jQuery.makeArray(JSON.stringify(feedback).split('}'));
    //        var icount;
    //        var actionOption = '<option value="-1" style=\"color:#c0c0c0\">--- Select ---</option>';
    //        for (icount = 0; icount < feedbackarr.length - 1; icount++) {
    //            actionOption += '<option ctlValue="' + feedback[icount].status_id + '"   value="' + feedback[icount].status_id + '" style=\"padding:3px\">' + feedback[icount].status + '</option>';
    //        }
    //        $('#ddlEGTStatus').append(actionOption);
    //    },
    //    error: function (feedback) {

    //    }
    //});
}

function FillEscalatingTeam() {
    $("#ajax-screen-masking").show();
    $('#ddlEGTEscalatingTeam').find('option').remove();
    var url = apiServer + '/api/EmailGenerateTicket';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 7
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: true,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            var feedbackarr = jQuery.makeArray(JSON.stringify(feedback).split('}'));
            var icount;
            var actionOption = '<option value="0" style=\"color:#c0c0c0\">--- Select ---</option>';
            for (icount = 0; icount < feedbackarr.length - 1; icount++) {
                if ($("#txtEGTEscalatingTeam").val().toUpperCase() == feedback[icount].Username.toUpperCase())
                    actionOption += '<option ctlValue="' + feedback[icount].Email + '"   value="' + feedback[icount].Id + '" style=\"padding:3px\"  selected=\"selected\">' + feedback[icount].Username + '</option>';
                else
                    actionOption += '<option ctlValue="' + feedback[icount].Email + '"   value="' + feedback[icount].Id + '" style=\"padding:3px\">' + feedback[icount].Username + '</option>';
            }
            $('#ddlEGTEscalatingTeam').append(actionOption);
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}

function FillCategory(sitecode, Issue_Id) {
    $("#ajax-screen-masking").show();
    $('#ddlEGTCategory').find('option').remove();

    var url = apiServer + '/api/EmailGenerateTicket';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 2,
        sitecode: sitecode,
        Issue_Id: Issue_Id
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: true,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            var feedbackarr = jQuery.makeArray(JSON.stringify(feedback).split('}'));
            var icount;
            var actionOption = '<option value="0" style=\"color:#c0c0c0\">--- Select ---</option>';
            for (icount = 0; icount < feedbackarr.length - 1; icount++) {
                actionOption += '<option ctlValue="' + feedback[icount].Function_Id + '"   value="' + feedback[icount].Function_Id + '" style=\"padding:3px\">' + feedback[icount].Function_Name + '</option>';
            }
            $('#ddlEGTCategory').append(actionOption);
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}

function FillSubCategory(sitecode, Function_Id) {
    $("#ajax-screen-masking").show();
    $('#ddlEGTSubCategory').find('option').remove();

    var url = apiServer + '/api/EmailGenerateTicket';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 4,
        sitecode: sitecode,
        Function_Id: Function_Id
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: true,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            var feedbackarr = jQuery.makeArray(JSON.stringify(feedback).split('}'));
            var icount;
            var actionOption = '<option value="0" style=\"color:#c0c0c0\">--- Select ---</option>';
            for (icount = 0; icount < feedbackarr.length - 1; icount++) {
                actionOption += '<option ctlValue="' + feedback[icount].Complaint_Id + '"   value="' + feedback[icount].Complaint_Id + '" style=\"padding:3px\">' + feedback[icount].Complaint_Name + '</option>';
            }
            $('#ddlEGTSubCategory').append(actionOption);
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}

//function GenerateTicket(CustomerName, MobileNo, TicketType, Category, SubCategory, Status, EscalatingTeam, Description, pkqueueid, product_code, pkqueueid, process_type, userid, emailto, EscalatingTeamName) {
//    $("#ajax-screen-masking").show();
//    var url = apiServer + '/api/EmailGenerateTicket';
//    jQuery.support.cors = true;
//    var JSONSendData = {
//        modetype: 9,
//        CustomerName: CustomerName,
//        MobileNo: MobileNo,
//        TicketType: TicketType,
//        Category: Category,
//        SubCategory: SubCategory,
//        Status: Status,
//        EscalatingTeam: EscalatingTeam,
//        Description: Description,
//        pkqueueid: pkqueueid,
//        product_code: product_code,
//        pkqueueid: pkqueueid,
//        process_type: process_type,
//        userid: userid,
//        emailto: emailto,
//        EscalatingTeamName: EscalatingTeamName
//    };
//    $.ajax
//    ({
//        url: url,
//        type: 'post',
//        data: JSON.stringify(JSONSendData),
//        dataType: 'json',
//        contentType: "application/json;charset=utf-8",
//        cache: true,
//        success: function (feedback) {
//            $("#ajax-screen-masking").hide();
//            if (feedback != null && feedback.Length > 0) {
//                if (feedback[0].errcode == 0) {
//                    alert(feedback[0].errmsg + " : " + feedback[0].ticket_id);
//                    $('#divEmailGenerateTicket').modal('hide');
//                    window.location = "/Emails/EmailList/2";
//                } else {
//                    alert(feedback[0].errmsg);
//                }
//            }
//        },
//        error: function (feedback) {
//            $("#ajax-screen-masking").hide();
//        }
//    });
//}

function ViewTicket(pkqueueid) {
    $("#ajax-screen-masking").show();
    var url = apiServer + '/api/EmailGenerateTicket';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 10,
        pkqueueid: pkqueueid
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: true,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            if (feedback != null) {
                $("#txtEGTCustomerName").val(feedback[0].customer_name);
                $("#txtEGTCustomerName").attr("disabled", "disabled");
                $("#txtEGTMobileNo").val(feedback[0].mobileno);
                $("#txtEGTMobileNo").attr("disabled", "disabled");

                //$("#ddlEGTStatus").val(feedback[0].queue_status);
                //$("#ddlEGTEscalatingTeam").val(feedback[0].esclating_user);

                $("#txtEGTTicketType").val(feedback[0].Issue_Name);
                //$('#ddlEGTTicketType').prop("disabled", true);
                $("#txtEGTCategory").val(feedback[0].Function_Name);
                //$('#ddlEGTCategory').prop("disabled", true);
                $("#txtEGTSubCategory").val(feedback[0].Complaint_Name);
                //$('#ddlEGTSubCategory').prop("disabled", true);

                $("#txtEGTStatus").val(feedback[0].queue_status_txt);
                //$("#ddlEGTEscalatingTeam").val(feedback[0].esclating_user_txt);
                $("#txtEGTEscalatingTeam").val(feedback[0].esclating_user_txt);
                $("#ddlEGTEscalatingTeam option:contains('" + $("#txtEGTEscalatingTeam").val().toUpperCase() + "')").attr('selected', 'selected');

                $("textarea[name=txtEGTDescription]").val(feedback[0].descr);

                $('#trTicketID').show();
                $('#txtTicketID').val(feedback[0].Ticket_id);
            }
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}
function ViewOnlyTicket(pkqueueid) {
    $("#ajax-screen-masking").show();
    $("#txtEVTCustomerName").val("");
    $("#txtEVTMobileNo").val("");
    $("#txtEVTTicketType").val("");
    $('#txtEVTTicketID').val("");
    $("#txtEVTCategory").val("");
    $("#txtEVTSubCategory").val("");
    $("textarea[name=txtEVTDescription]").val("");
    $("#txtEVTStatus").val("");
    $("#txtEVTEscalatingTeam").val("");
    var url = apiServer + '/api/EmailGenerateTicket';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 10,
        pkqueueid: pkqueueid
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: true,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            if (feedback != null && feedback.length > 0) {
                $("#txtEVTCustomerName").val(feedback[0].customer_name);
                $("#txtEVTMobileNo").val(feedback[0].mobileno);
                $("#txtEVTTicketType").val(feedback[0].Issue_Name);
                $("#txtEVTCategory").val(feedback[0].Function_Name);
                $("#txtEVTSubCategory").val(feedback[0].Complaint_Name);
                $("#txtEVTStatus").val(feedback[0].queue_status_txt);
                $("#txtEVTEscalatingTeam").val(feedback[0].esclating_user_txt);
                $("textarea[name=txtEVTDescription]").val(feedback[0].descr);
                $('#txtEVTTicketID').val(feedback[0].Ticket_id);
            }
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}
