﻿/* ===================================================== 
   *  UK PORT IN - SMS REQUEST
   ----------------------------------------------------- */

/* ----------------------------------------------------------------
 * Function Name    : ATPortINTransactionReportList
 * Purpose          : to display SMS request
 * Added by         : Edi Suryadi
 * Create Date      : September 24th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function ATPortINTransactionReportList(TransactionReportFilter) {

    $("#ATPortINTransactionReport_TransactionListContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"ATPortINTransactionReport_TransactionListTbl\"></table>");

    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    var url = apiServer + '/api/ukporting/';
    jQuery.support.cors = true;
    if (TransactionReportFilter == '1') {
        $('#ATPortINTransactionReport').height(205);
        var JSONSendData = {
            SubType: 8,
            SMSReportFilter: TransactionReportFilter,
            Sitecode: "MCM"
        };
    } else if (TransactionReportFilter == '2') {
        $('#ATPortINTransactionReport').height(205);
        var JSONSendData = {
            SubType: 8,
            SMSReportFilter: TransactionReportFilter,
            Sitecode: "MCM"
        };
    } else if (TransactionReportFilter == '3') {
        $('#ATPortINTransactionReport').height(250);
        var JSONSendData = {
            SubType: 8,
            SMSReportFilter: TransactionReportFilter,
            Sitecode: "MCM"
        };
    }

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function (feedback) {
            $("#ajax-screen-masking").show();
            $('#ATPortINTransactionReport_TransactionListContainer').hide();
        },
        success: function (feedback) {
            var oTable = $('#ATPortINTransactionReport_TransactionListTbl').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: true,
                bInfo: true,
                iDisplayLength: 10,
                aaSorting: [[0, "desc"]],
                oLanguage: {
                    sInfo: "Total <b>_TOTAL_</b> request(s) found."
                },
                aoColumns: [
                    { mDataProp: "Msisdn", sTitle: "Porting Code" },
                    { mDataProp: "PAC", sTitle: "MNP Export" },
                    { mDataProp: "Err_Message", sTitle: "Last TimeStamp" },
                    { mDataProp: "TimeStamp", sTitle: "Porting Status" },
                    {
                        mDataProp: "Service_Number", sTitle: "More Information",
                        fnRender: function (ob) {
                            var actionURL = "";
                            actionURL = "<center><button alt=\"" + ob.aData.PAC + "\" class=\"btn blue ATPortINTransactionReport_btnViewDetails\">View Details</button></center>";
                            return actionURL;
                        }
                    }
                ]
            });
            
            $("#ATPortINTransactionReport_TransactionListTbl_wrapper .row-fluid .span6:eq(0)").html("");
            $("#ATPortINTransactionReport_TransactionListTbl_wrapper .row-fluid .span6:eq(2)").addClass("span4");
            $("#ATPortINTransactionReport_TransactionListTbl_wrapper .row-fluid .span6:eq(3)").addClass("span8");
            $("#ATPortINTransactionReport_TransactionListTbl_wrapper .row-fluid:eq(1) .span6").removeClass("span6");

            var ATPortINTransactionReportHeight = $('#ATPortINTransactionReport').height();
            var ATPortINTransactionReport_TransactionListTblHeight = $("#ATPortINTransactionReport_TransactionListContainer").height();
            $('#ATPortINTransactionReport').height(ATPortINTransactionReportHeight + ATPortINTransactionReport_TransactionListTblHeight + 30);
            $("#ajax-screen-masking").hide();
            $("#ATPortINTransactionReport_TransactionListContainer").show();

        },
        error: function (feedback) {
            $("select[name=ATPortINTransactionReport_sSearchBy]").removeAttr("disabled");
            $("#ATPortINTransactionReport_TransactionListContainer").html("").append("No SMS request found").show();
        }
    });
}


/* ----------------------------------------------------- 
   *  eof UK PORT IN - SMS REQUEST
   ===================================================== */






