﻿var MailItemHtml = '<li><div class="row-11 float-right"><div class="row-12"><div class="row-6 txt-lft float-left"><strong>Subject :</strong> {SUBJECT}</div><div class="row-6 txt-rgt float-right">{DATE}</div></div><div class="row-12" style="margin-top: 10px;">{ATTACHMENTS}</div><p class="mail-msg"><strong>Body :</strong><br/> {BODY}</p></div></li>';

var TicketItemHtml = '<li id="liTicket" class="liTicket" ctlValue="{queue_id}" style="cursor:pointer;"><div class="row-12 bor-bot"><div class="row-6 bor-rgt txt-cen"><strong>{TICKETID}</strong></div><div class="row-6 txt-cen"><strong>{DATE}</strong></div></div><p class="pad10" style="word-wrap: break-word;">{BODY}</p></li>';

function BindMainGrid(user_id, queue_id, role_id, queue_status_id) {
    $("#ajax-screen-masking").show();
    $('#hdqueueid').val('');
    $('#hdemail').val('');
    $('#hdcustemail').val('');
    $('#hdsubject').val('');

    $('#divID').html('');
    $('#divCustomerEmail').html('');
    $('#divSubject').html('');
    $('#divStatus').html('');
    $('#divAgentEmail').html('');
    $('#divBody').html('');
    $('#divAssignedTo').html('');
    $('#divAssignedDate').html('');

    var url = apiServer + '/api/Emails';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 5,
        userid: user_id,
        queue_id: queue_id,
        role_id: role_id,
        status_id: queue_status_id
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            if (feedback != null && feedback.length > 0) {
                $('#hdqueueid').val(feedback[0].pk_queue_id);
                $('#hdagentemail').val(feedback[0].assigned_user_email);
                $('#hdsubject').val(feedback[0].email_subject);
                $('#hdcustemail').val(feedback[0].customer_email);

                $('#divID').html(feedback[0].pk_queue_id);
                $('#divCustomerEmail').html(feedback[0].customer_email);
                $('#divSubject').html(feedback[0].email_subject_trim);
                $('#divStatus').html(feedback[0].queue_status);
                $('#divAgentEmail').html(feedback[0].assigned_user_email);
                $('#divBody').html(feedback[0].email_body_trim);
                $('#divAssignedTo').html(feedback[0].assigned_user);
                $('#divAssignedDate').html(feedback[0].assigned_date_string);
            }
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}

function FillMailItems(queue_id) {
    $('#pbMyEmailOpenBody ul.email-container').html("");
    $("#ajax-screen-masking").show();
    var url = apiServer + '/api/Emails';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 8,
        queue_id: queue_id
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            if (feedback != null && feedback.length > 0) {
                for (var i = 0; i < feedback.length; i++) {
                    var attachments = "";
                    if (feedback[i].lstEmailAttachments != null && feedback[i].lstEmailAttachments.length > 0) {
                        var htmlString = '<a href="{0}" target="_blank" style="cursor:pointer;margin-right: 10px;"><img src="/Assets/Main/img/attachments/{1}.png" style="padding-right: 5px;">{2}</a>';
                        for (j = 0; j < feedback[i].lstEmailAttachments.length; j++) {
                            attachments += htmlString.replace("{0}", feedback[i].lstEmailAttachments[j].fileurl).replace("{1}", feedback[i].lstEmailAttachments[j].extension).replace("{2}", feedback[i].lstEmailAttachments[j].filename);
                        }
                    }
                    var appString = MailItemHtml.replace("{SUBJECT}", feedback[i].email_subject).replace("{DATE}", feedback[i].email_date).replace("{BODY}", feedback[i].email_body_html).replace("{ATTACHMENTS}", attachments);;
                    $('#pbMyEmailOpenBody ul.email-container').append(appString);
                }
            }
            else {
                $('#pbMyEmailOpenBody ul.email-container').append("No records found.");
            }
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}

function FillTicketItems(customer_email_id) {
    $('#pbMyEmailOpenBody ul.tickets-details').html("");
    $("#ajax-screen-masking").show();
    var url = apiServer + '/api/Emails';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 9,
        customer_email_id: customer_email_id
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            if (feedback != null && feedback.length > 0) {
                for (var i = 0; i < feedback.length; i++) {
                    var appString = TicketItemHtml.replace("{TICKETID}", feedback[i].Ticket_id).replace("{DATE}", feedback[i].issue_generate_date_string).replace("{BODY}", feedback[i].email_body_trim).replace("{queue_id}", feedback[i].queue_id);
                    $('#pbMyEmailOpenBody ul.tickets-details').append(appString);
                }
            }
            else {
                $('#pbMyEmailOpenBody ul.tickets-details').append("No history found.");
            }
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}

function BindEmailTemplate() {
    $("#ajax-screen-masking").show();
    $('#ddlEmailTemplate').html('');
    //$('#txtEmailSubject').val('');
    var url = apiServer + '/api/MailChimp';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 5
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            if (feedback != null && feedback.length > 0) {
                var actionOption = '<option value="-1" style=\"padding:3px\">-- Select --</option>';
                for (icount = 0; icount < feedback.length; icount++) {
                    actionOption += '<option style=\"padding:3px\" ctlvalue="' + feedback[icount].PublishName + '" value="' + feedback[icount].Slug + '">' + feedback[icount].Name + '</option>';
                }
                $('#ddlEmailTemplate').append(actionOption);
            }
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}