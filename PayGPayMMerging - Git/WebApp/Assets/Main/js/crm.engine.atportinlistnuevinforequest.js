﻿/* ===================================================== 
   *  UK PORT IN - SMS REQUEST
   ----------------------------------------------------- */

/* ----------------------------------------------------------------
 * Function Name    : ATPortINNuevInfoRequestList
 * Purpose          : to display SMS request
 * Added by         : Edi Suryadi
 * Create Date      : September 24th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function ATPortINNuevInfoRequestList(NuevInfoRequestListFilter) {

    $("#ATPortINListNuevInfoRequest_NuevInfoRequestListContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"ATPortINListNuevInfoRequest_NuevInfoRequestListTbl\"></table>");

    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    var url = apiServer + '/api/ukporting/';
    jQuery.support.cors = true;
    if (NuevInfoRequestListFilter == '1') {
        $('#ATPortINListNuevInfoRequest').height(205);
        var JSONSendData = {
            SubType: 8,
            SMSReportFilter: NuevInfoRequestListFilter,
            Sitecode: "MCM"
        };
    } else if (NuevInfoRequestListFilter == '2') {
        $('#ATPortINListNuevInfoRequest').height(250);
        var JSONSendData = {
            SubType: 8,
            SMSReportFilter: NuevInfoRequestListFilter,
            Sitecode: "MCM"
        };
    }

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function (feedback) {
            $("#ajax-screen-masking").show();
            $('#ATPortINListNuevInfoRequest_NuevInfoRequestListContainer').hide();
        },
        success: function (feedback) {
            var oTable = $('#ATPortINListNuevInfoRequest_NuevInfoRequestListTbl').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: true,
                bInfo: true,
                iDisplayLength: 10,
                aaSorting: [[0, "desc"]],
                oLanguage: {
                    sInfo: "Total <b>_TOTAL_</b> request(s) found."
                },
                aoColumns: [
                    { mDataProp: "TimeStamp", sTitle: "Last Timestamp" },
                    { mDataProp: "PAC", sTitle: "Porting Status" },
                    { mDataProp: "Err_Message", sTitle: "GDesc"},
                    { mDataProp: "Msisdn", sTitle: "MSISDN" },
                    { mDataProp: "Service_Number", sTitle: "LDesc" },
                    { mDataProp: "TimeStamp_String", sTitle: "Donor" },
                    { mDataProp: "PAC", sTitle: "NUV PDF File" }
                ]
            });
            
            $("#ATPortINListNuevInfoRequest_NuevInfoRequestListTbl_wrapper .row-fluid .span6:eq(0)").html("");
            $("#ATPortINListNuevInfoRequest_NuevInfoRequestListTbl_wrapper .row-fluid .span6:eq(2)").addClass("span4");
            $("#ATPortINListNuevInfoRequest_NuevInfoRequestListTbl_wrapper .row-fluid .span6:eq(3)").addClass("span8");
            $("#ATPortINListNuevInfoRequest_NuevInfoRequestListTbl_wrapper .row-fluid:eq(1) .span6").removeClass("span6");

            var ATPortINListNuevInfoRequestHeight = $('#ATPortINListNuevInfoRequest').height();
            var ATPortINListNuevInfoRequest_NuevInfoRequestListTblHeight = $("#ATPortINListNuevInfoRequest_NuevInfoRequestListContainer").height();
            $('#ATPortINListNuevInfoRequest').height(ATPortINListNuevInfoRequestHeight + ATPortINListNuevInfoRequest_NuevInfoRequestListTblHeight + 30);
            $("#ajax-screen-masking").hide();
            $("#ATPortINListNuevInfoRequest_NuevInfoRequestListContainer").show();

        },
        error: function (feedback) {
            $("select[name=ATPortINListNuevInfoRequest_sSearchBy]").removeAttr("disabled");
            $("#ATPortINListNuevInfoRequest_NuevInfoRequestListContainer").html("").append("No SMS request found").show();
        }
    });
}


/* ----------------------------------------------------- 
   *  eof UK PORT IN - SMS REQUEST
   ===================================================== */






