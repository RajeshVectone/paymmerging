﻿function AddressListBeginAdd2in1Country(Key, Postcode) {
    $("#errAdd2in1CountryListAddressIP").html("").hide();

	var strUrl = "";
	// Build the url
	strUrl = "https://services.postcodeanywhere.co.uk/PostcodeAnywhere/Interactive/FindByPostcode/v1.00/json.ws?";
	strUrl += "&Key=" + escape(Key);
	strUrl += "&Postcode=" + escape(Postcode);
	strUrl += "&UserName='SWITC11123'";
	strUrl += "&CallbackFunction=AddressListEndAdd2in1Country";
	// Make the request
	if ($("#pcascript").length) try { $('head').remove("#pcascript") } catch (e) { }
	var scriptTag = "<script type=\"text/javascript\" src=\"" + strUrl + "\" id=\"pcascript\"></script>"
	$('head').append(scriptTag);
}

function AddressListEndAdd2in1Country(response) {
	if (response.length == 1 && typeof (response[0].Error) != 'undefined')
	{
	    $("#trAdd2in1CountryAddressList").show();
	    $("#Add2in1CountryListAddressIP").hide();
	    $("#errAdd2in1CountryListAddressIP").html(response[0].Description).show();
	}
	else {
	    if (response.length == 0) {
	        $("#trAdd2in1CountryAddressList").show();
	        $("#Add2in1CountryListAddressIP").hide();
	        $("#errAdd2in1CountryListAddressIP").html("Sorry, no matching items found").show();
	    }
	    else {
	        
	        $("#errAdd2in1CountryListAddressIP").html("").hide();
	        $("#Add2in1CountryListAddressIP").html("");
	        $("#Add2in1CountryListAddressIP").append("<option value=\"\">- Select Address -</option>");
	        var optAddress = "";
	        for (var i in response) {
	            optAddress += "<option value=\"" + response[i].Id + "\">" + response[i].StreetAddress + "</option>";
	        }
	        $("#Add2in1CountryListAddressIP").append(optAddress);
	        $("#Add2in1CountryListAddressIP").live("click", function () {
	            SelectAddressAdd2in1Country('KG22-JN94-EF64-MW54', $(this).val());
	        });

	        $("#Add2in1CountryListAddressIP").show();
	        $("#trAdd2in1CountryAddressList").show();
	    }
	}
	$("img#Add2in1CountryBtnFindLoader").hide();
	$("input#Add2in1CountryBtnFind").show();
}

function SelectAddressAdd2in1Country(Key, Id) {
	if (!Id) return;

	var strUrl = "";
    // Build the url
	strUrl = "https://services.postcodeanywhere.co.uk/PostcodeAnywhere/Interactive/RetrieveById/v1.00/json.ws?";
	strUrl += "&Key=" + escape(Key);
	strUrl += "&Id=" + escape(Id);
	strUrl += "&UserName='SWITC11123'";
	strUrl += "&CallbackFunction=SelectAddressEndAdd2in1Country";
    // Make the request
	if ($("#pcascript").length) try { $('head').remove("#pcascript") } catch (e) { }
	var scriptTag = "<script type=\"text/javascript\" src=\"" + strUrl + "\" id=\"pcascript\"></script>"
	$('head').append(scriptTag);

    // User interface changed
	$("input#Add2in1CountryBtnFind").val('Change');
	$("input#Add2in1CountryHouseNo").attr("readonly", true);
	$("input#Add2in1CountryHouseNo").attr("readonly", false);
	$("input#Add2in1CountryAddress").attr("readonly", false);
	$("input#Add2in1CountryCity").attr("readonly", false);
}

function SelectAddressEndAdd2in1Country(response) {
	if (response.length == 1 && typeof (response[0].Error) != 'undefined') {
	    $("#trAdd2in1CountryAddressList").show();
	    $("#Add2in1CountryListAddressIP").hide();
	    $("#errAdd2in1CountryListAddressIP").html(response[0].Description).show();
	}
	else {
	    if (response.length == 0) {
	        $("#trAdd2in1CountryAddressList").show();
	        $("#Add2in1CountryListAddressIP").hide();
	        $("#errAdd2in1CountryListAddressIP").html("Sorry, no matching items found").show();
	    }
		else {
		    $("#Add2in1CountryListAddressIP").hide();
		    $("#trAdd2in1CountryAddressList").hide();

			var hno = response[0].Line1.split(" ");
			var street = "";
			$("input#Add2in1CountryHouseNo").val(ReplaceForFindAddress(hno[0]));

			for (i = 1; i < hno.length; i++) {
			    street += hno[i] + " ";
			}
			street = street + " " + response[0].Line2 + " " + response[0].Line3 + response[0].Line4 + " " + response[0].Line5;

			$("input#Add2in1CountryAddress").val(ReplaceForFindAddress(street));
			$("input#Add2in1CountryCity").val(response[0].PostTown);
		}
	}
}