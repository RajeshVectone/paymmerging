﻿/* ===================================================== 
   *  UK PORT IN - SUBMIT
   ----------------------------------------------------- */

/* ----------------------------------------------------------------
 * Function Name    : UKPortINModifyValidate
 * Purpose          : to validate Vectone MSISDN
 * Added by         : Edi Suryadi
 * Create Date      : September 11th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function UKPortINModifyValidate(PAC, Msisdn) {

    var url = apiServer + '/api/ukporting/';

    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    jQuery.support.cors = true;
    var JSONSendData = {
        PAC : PAC,
        Msisdn: Msisdn,
        Sitecode : "MCM",
        SubType: 4
    };

    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();

            var stat = data.errcode;
            if (stat == 0) {
                $("#UKPortINModifyStep2_iPAC").text($("input[name=UKPortINModifyStep1_iPAC]").val());
                $("#UKPortINModifyStep2_iMSISDN").text($("input[name=UKPortINModifyStep1_iMSISDN]").val());
                $("input[name=UKPortINModifyStep2_iContactName]").val(data.contact_name);
                $("input[name=UKPortINModifyStep2_iContactDetails]").val(data.contact_details);
                var UKPortINModifyStep2_ihPortDate = data.port_date_string;
                $("input[name=UKPortINModifyStep2_ihPortDate]").val(UKPortINModifyStep2_ihPortDate);
                $('#UKPortINModifyStep2_iPortDate_Container').attr('data-date', UKPortINModifyStep2_ihPortDate);
                $('#UKPortINModifyStep2_iPortDate').val(UKPortINModifyStep2_ihPortDate);

                $("#UKPortINModifyStep1").hide();
                $("#UKPortINModifyStep2").fadeIn();
            } else {
                $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "2" });
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (data) {
            $("#ajax-screen-masking").hide();
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            $('.PMFailed .modal-header').html(data.errsubject);
            $('.PMFailed .modal-body').html(data.errmsg);
            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : UKPortINModifyDoModify
 * Purpose          : to submit the port in request
 * Added by         : Edi Suryadi
 * Create Date      : September 11th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function UKPortINModifyDoModify(Msisdn, PAC, ContactName, ContactDetail, PortDate, isRevise) {

    var url = apiServer + '/api/ukporting/';

    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    var SubType = 0;
    if (isRevise) {
        // isRevise = true
        SubType = 3;
    } else {
        // isRevise = false
        SubType = 5;
    }

    jQuery.support.cors = true;
    var JSONSendData = {
        SubType: SubType,
        Msisdn: Msisdn,
        PAC: PAC,
        Sitecode: "MCM",
        ContactName: ContactName,
        ContactDetail: ContactDetail,
        PortDate: PortDate
    };

    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();

            var stat = data.errcode;
            if (stat == 0) {
                $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
                $('.PMSuccess .modal-header').html(data.errsubject);
                $('.PMSuccess .modal-body').html(data.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            } else {
                $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "2" });
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (data) {
            $("#ajax-screen-masking").hide();
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            $('.PMFailed .modal-header').html(data.errsubject);
            $('.PMFailed .modal-body').html(data.errmsg);
            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });
}



/* ----------------------------------------------------- 
   *  eof UK PORT IN - SUBMIT
   ===================================================== */






