﻿function AddressListBeginPaymentMethod(Key, Postcode) {
    $("#sCSPBCAddressListError").html("").hide();

	var strUrl = "";
	// Build the url
	strUrl = "https://services.postcodeanywhere.co.uk/PostcodeAnywhere/Interactive/FindByPostcode/v1.00/json.ws?";
	strUrl += "&Key=" + escape(Key);
	strUrl += "&Postcode=" + escape(Postcode);
	strUrl += "&UserName='SWITC11123'";
	strUrl += "&CallbackFunction=AddressListEndPaymentMethod";
	// Make the request
	if ($("#pcascript").length) try { $('head').remove("#pcascript") } catch (e) { }
	var scriptTag = "<script type=\"text/javascript\" src=\"" + strUrl + "\" id=\"pcascript\"></script>"
	$('head').append(scriptTag);
}

function AddressListEndPaymentMethod(response) {
	if (response.length == 1 && typeof (response[0].Error) != 'undefined')
	{
	    $("tr#CSPBCAddressListContainer").show();
	    $("#sCSPBCAddressList").hide();
	    $("#sCSPBCAddressListError").html(response[0].Description).show();
	}
	else {
	    if (response.length == 0) {
	        $("tr#CSPBCAddressListContainer").show();
	        $("#sCSPBCAddressList").hide();
	        $("#sCSPBCAddressListError").html("Sorry, no matching items found").show();
	    }
	    else {
	        
	        $("#sCSPBCAddressListError").html("").hide();
	        $("#sCSPBCAddressList").html("").append("<option value=\"\">- Select Address -</option>");
	        var optAddress = "";
	        for (var i in response) {
	            optAddress += "<option value=\"" + response[i].Id + "\">" + response[i].StreetAddress + "</option>";
	        }
	        $("#sCSPBCAddressList").append(optAddress);
	        $("#sCSPBCAddressList").live("click", function () {
	            SelectAddressPaymentMethod('KG22-JN94-EF64-MW54', $(this).val());
	        });

	        $("#sCSPBCAddressList").show();
	        $("tr#CSPBCAddressListContainer").show();
	    }
	}
	$("img#CSPBCBtnFindAddressLoader").hide();
	$("button#CSPBCBtnFindAddress").show();
}

function SelectAddressPaymentMethod(Key, Id) {
	if (!Id) return;

	var strUrl = "";
    // Build the url
	strUrl = "https://services.postcodeanywhere.co.uk/PostcodeAnywhere/Interactive/RetrieveById/v1.00/json.ws?";
	strUrl += "&Key=" + escape(Key);
	strUrl += "&Id=" + escape(Id);
	strUrl += "&UserName='SWITC11123'";
	strUrl += "&CallbackFunction=SelectAddressEndPaymentMethod";
    // Make the request
	if ($("#pcascript").length) try { $('head').remove("#pcascript") } catch (e) { }
	var scriptTag = "<script type=\"text/javascript\" src=\"" + strUrl + "\" id=\"pcascript\"></script>"
	$('head').append(scriptTag);

    // User interface changed
	$("button#CSPBCBtnFindAddress").html('Change');
	$("input#iCSPBCAddress1").attr("readonly", false);
	$("input#iCSPBCAddress2").attr("readonly", false);
	$("input#iCSPBCTown").attr("readonly", false);
}

function SelectAddressEndPaymentMethod(response) {
	if (response.length == 1 && typeof (response[0].Error) != 'undefined') {
	    $("tr#CSPBCAddressListContainer").show();
	    $("#sCSPBCAddressList").hide();
	    $("#sCSPBCAddressListError").html(response[0].Description).show();
	}
	else {
	    if (response.length == 0) {
	        $("tr#CSPBCAddressListContainer").show();
	        $("#sCSPBCAddressList").hide();
	        $("#sCSPBCAddressListError").html("Sorry, no matching items found").show();
	    }
		else {
	        $("#sCSPBCAddressList").hide();
		    $("tr#CSPBCAddressListContainer").hide();

			var hno = response[0].Line1.split(" ");
			var street = "";
			$("input#iCSPBCAddress1").val(ReplaceForFindAddress(hno[0]));

			for (i = 1; i < hno.length; i++) {
			    street += hno[i] + " ";
			}
			street = street + " " + response[0].Line2 + " " + response[0].Line3 + response[0].Line4 + " " + response[0].Line5;

			$("input#iCSPBCAddress2").val(ReplaceForFindAddress(street));
			$("input#iCSPBCTown").val(response[0].PostTown);
		}
	}
}