﻿/* ===================================================== 
   *  TAB SIM SWAP 
   ----------------------------------------------------- */
/* ----------------------------------------------------------------
 * Function Name    : SIMSwapDoValidate
 * Purpose          : to validate the mobile no and ICCID
 * Added by         : Edi Suryadi
 * Create Date      : September 17th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */

function SIMSwapDoValidate(OldMobileNo, NewMobileNo, LastDigitICCID, JiraTicketNo, SimSwapCharge, VerificationCode, UserLogin, Sitecode, SwapType) {

    $("#ajax-screen-masking2").css({ "height": $(document).outerHeight() + "px" });

    
    var url = apiServer + '/api/simswap';

    jQuery.support.cors = true;
    var JSONSendData = {
        TypeofSwap: 0,
        MsisdnOld: OldMobileNo,
        MsisdnNew: NewMobileNo,
        NewIccidLastDigit: LastDigitICCID,
        JiraTicketNo: JiraTicketNo,
        SimSwapCharge: SimSwapCharge,
        VerificationCode: VerificationCode,
        User: UserLogin,
        Sitecode: Sitecode,
        SwapType: SwapType
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking2").show();
            $("button#btnSIMSwapDoValidate").hide();
            $("img#btnSIMSwapDoValidateLoader").show();
        },
        success: function (data) {
            $("#ajax-screen-masking2").hide();
            $("button#btnSIMSwapDoValidate").show();
            $("img#btnSIMSwapDoValidateLoader").hide();

            if (data.errcode == 0) {
                var btnSwapCancel = "<button id=\"btnSIMSwapDoSwap\" class=\"btn blue\" style=\"width:130px;height:40px\">Swap</button>";
                btnSwapCancel += "<button id=\"btnSIMSwapDoCancel\" class=\"btn darkgrey\" style=\"margin-left:20px;width:130px;height:40px\">Cancel</button>";
                $("#SIMSwapBtnSwapCancelContainer").append(btnSwapCancel);
                $("tr.trSIMSwapBtnSwapCancel").show();
                $("button#btnSIMSwapDoValidate").addClass("grey").removeClass("blue").attr("disabled", true);
                $("input[name=iSIMSwapNewMobileNo], input[name=iSIMSwapICCID]").attr("disabled", true);
            } else {
                $("#SIMSwapBtnSwapCancelContainer").html("");
                $("tr.trSIMSwapBtnSwapCancel").hide();
                $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "2" });
                $('.PMFailed .modal-header').html('SIM Swap');
                $('.PMFailed .modal-body').html('Validation has failed. Please reconfirm and re-enter the details. Thanks You.');
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
            
        }
    });
}

/*

Sim SWAP PortIn  

*/

function SIMSwapDoSwapPortIn(OldMobileNo, NewMobileNo, LastDigitICCID, JiraTicketNo, SimSwapCharge, VerificationCode, UserLogin, Sitecode, HaveSIM) {

    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    var url = apiServer + '/api/simswap';

    jQuery.support.cors = true;
    var JSONSendData = {
        TypeofSwap: 1,
        MsisdnOld: OldMobileNo,
        MsisdnNew: NewMobileNo,
        NewIccidLastDigit: LastDigitICCID,
        JiraTicketNo: JiraTicketNo,
        SimSwapCharge: SimSwapCharge,
        VerificationCode: VerificationCode,
        User: UserLogin,
        Sitecode: Sitecode,
        isNewSIM: HaveSIM,
        SwapType:1
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();

            var stat = data.errcode;
            if (stat == 0) {
                $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
                $('.PMSuccess .modal-header').html(data.errsubject);
                $('.PMSuccess .modal-body').html(data.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            } else {
                $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "0" });
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }

        },
        error: function (data) {
            $("#ajax-screen-masking").hide();
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            $('.PMFailed .modal-header').html(data.errsubject);
            $('.PMFailed .modal-body').html(data.errmsg);
            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : SIMSwapDoSwapReasonLost
 * Purpose          : to swap the mobile number - reason = Lost
 * Added by         : Edi Suryadi
 * Create Date      : September 17th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */

function SIMSwapDoSwapReasonLost(OldMobileNo, NewMobileNo, LastDigitICCID, JiraTicketNo, SimSwapCharge, VerificationCode, UserLogin, Sitecode, HaveSIM, SwapType) {

    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    var url = apiServer + '/api/simswap';

    jQuery.support.cors = true;
    var JSONSendData = {
        TypeofSwap: 2,
        MsisdnOld: OldMobileNo,
        MsisdnNew: NewMobileNo,
        NewIccidLastDigit: LastDigitICCID,
        JiraTicketNo: JiraTicketNo,
        SimSwapCharge: SimSwapCharge,
        VerificationCode: VerificationCode,
        User: UserLogin,
        Sitecode: Sitecode,
        isNewSIM: HaveSIM,
        SwapType: SwapType

    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();

            var stat = data.errcode;
            if (stat == 0) {
                $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
                $('.PMSuccess .modal-header').html(data.errsubject);
                $('.PMSuccess .modal-body').html(data.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            } else {
                $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "0" });
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }

        },
        error: function (data) {
            $("#ajax-screen-masking").hide();
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            $('.PMFailed .modal-header').html(data.errsubject);
            $('.PMFailed .modal-body').html(data.errmsg);
            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : SIMSwapDoSwapReasonNewSIM
 * Purpose          : to swap the mobile number - reason = New SIM
 * Added by         : Edi Suryadi
 * Create Date      : September 17th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */

function SIMSwapDoSwapReasonNewSIM(OldMobileNo, NewMobileNo, LastDigitICCID, JiraTicketNo, SimSwapCharge, VerificationCode, UserLogin, Sitecode, HaveSIM, SwapType ) {

    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    var url = apiServer + '/api/simswap';

    jQuery.support.cors = true;
    var JSONSendData = {
        TypeofSwap: 3,
        MsisdnOld: OldMobileNo,
        MsisdnNew: NewMobileNo,
        NewIccidLastDigit: LastDigitICCID,
        JiraTicketNo: JiraTicketNo,
        SimSwapCharge: SimSwapCharge,
        VerificationCode: VerificationCode,
        User: UserLogin,
        Sitecode: Sitecode,
        isNewSIM: HaveSIM,
        SwapType: SwapType
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();

            var stat = data.errcode;
            if (stat == 0) {
                $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
                $('.PMSuccess .modal-header').html(data.errsubject);
                $('.PMSuccess .modal-body').html(data.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            } else {
                $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "0" });
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }

        },
        error: function (data) {
            $("#ajax-screen-masking").hide();
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            $('.PMFailed .modal-header').html(data.errsubject);
            $('.PMFailed .modal-body').html(data.errmsg);
            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : SIMSwapDoPlaceOrderReasonLost
 * Purpose          : to order the mobile number - reason = Lost
 * Added by         : Edi Suryadi
 * Create Date      : September 17th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */

function SIMSwapDoPlaceOrderReasonLost(
    OldMobileNo,
    NewMobileNo,
    LastDigitICCID,
    JiraTicketNo,
    SimSwapCharge,
    VerificationCode,
    UserLogin,
    Sitecode,
    HaveSIM,
    TypeofSim,
    IsNewAddress,
    Firstname,
    Lastname,
    Address1,
    Address2,
    City,
    Postcode,
    Country) {

    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    var url = apiServer + '/api/simswap';

    jQuery.support.cors = true;
    var JSONSendData = {
        TypeofSwap: 2,
        MsisdnOld: OldMobileNo,
        MsisdnNew: NewMobileNo,
        NewIccidLastDigit: LastDigitICCID,
        JiraTicketNo: JiraTicketNo,
        SimSwapCharge: SimSwapCharge,
        VerificationCode: VerificationCode,
        User: UserLogin,
        Sitecode: Sitecode,
        isNewSIM: HaveSIM,

        // For Dispatch Address
        TypeofSim: TypeofSim,
        IsNewAddress: IsNewAddress,
        Firstname: Firstname,
        Lastname: Lastname,
        Address1: Address1,
        Address2: Address2,
        City: City,
        Postcode: Postcode,
        Country: Country
        // eof For Dispatch Address
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();

            var stat = data.errcode;
            if (stat == 0) {
                $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
                $('.PMSuccess .modal-header').html(data.errsubject);
                $('.PMSuccess .modal-body').html(data.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            } else {
                $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "0" });
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }

        },
        error: function (data) {
            $("#ajax-screen-masking").hide();
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            $('.PMFailed .modal-header').html(data.errsubject);
            $('.PMFailed .modal-body').html(data.errmsg);
            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : SIMSwapDoPlaceOrderReasonNewSIM
 * Purpose          : to order the mobile number - reason = New SIM
 * Added by         : Edi Suryadi
 * Create Date      : September 17th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */

//function SIMSwapDoPlaceOrderReasonNewSIM(
//    OldMobileNo,
//    NewMobileNo,
//    LastDigitICCID,
//    JiraTicketNo,
//    SimSwapCharge,
//    VerificationCode,
//    UserLogin,
//    Sitecode,
//    HaveSIM,
//    TypeofSim,
//    IsNewAddress,
//    Firstname,
//    Lastname,
//    Address1,
//    Address2,
//    City,
//    Postcode,
//    Country) {

//    var ajaxSpinnerTop = $(window).outerHeight() / 2;
//    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
//    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

//    var url = apiServer + '/api/simswap';

//    jQuery.support.cors = true;
//    var JSONSendData = {
//        TypeofSwap: 3,
//        MsisdnOld: OldMobileNo,
//        MsisdnNew: NewMobileNo,
//        NewIccidLastDigit: LastDigitICCID,
//        JiraTicketNo: JiraTicketNo,
//        SimSwapCharge: SimSwapCharge,
//        VerificationCode: VerificationCode,
//        User: UserLogin,
//        Sitecode: Sitecode,
//        isNewSIM: HaveSIM,

//        // For Dispatch Address
//        TypeofSim: TypeofSim,
//        IsNewAddress: IsNewAddress,
//        Firstname: Firstname,
//        Lastname: Lastname,
//        Address1: Address1,
//        Address2: Address2,
//        City: City,
//        Postcode: Postcode,
//        Country: Country
//        // eof For Dispatch Address
//    };

//    $.ajax
//    ({
//        url: url,
//        type: 'post',
//        data: JSON.stringify(JSONSendData),
//        dataType: 'json',
//        contentType: "application/json;charset=utf-8",
//        cache: false,
//        beforeSend: function () {
//            $("#ajax-screen-masking").show();
//        },
//        success: function (data) {
//            $("#ajax-screen-masking").hide();

//            var stat = data.errcode;
//            if (stat == 0) {
//                $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
//                $('.PMSuccess .modal-header').html(data.errsubject);
//                $('.PMSuccess .modal-body').html(data.errmsg);
//                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
//                    'margin-left': function () {
//                        return window.pageXOffset - ($(this).width() / 2);
//                    }
//                });
//            } else {
//                $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "0" });
//                $('.PMFailed .modal-header').html(data.errsubject);
//                $('.PMFailed .modal-body').html(data.errmsg);
//                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
//                    'margin-left': function () {
//                        return window.pageXOffset - ($(this).width() / 2);
//                    }
//                });
//            }

//        },
//        error: function (data) {
//            $("#ajax-screen-masking").hide();
//            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
//            $('.PMFailed .modal-header').html(data.errsubject);
//            $('.PMFailed .modal-body').html(data.errmsg);
//            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
//                'margin-left': function () {
//                    return window.pageXOffset - ($(this).width() / 2);
//                }
//            });
//        }
//    });
//}

//31-Dec-2018 : Moorthy : Added for Sim Order
function SIMSwapDoPlaceOrderReasonNewSIM(sitecode, firstname, lastname, houseno, address1, address2, city, postcode, state, mobilephone, email, subscriberchannel, sourceaddress, ordersimurl, ip_address, nb_of_sim) {

    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    var url = apiServer + '/api/FreeSIMOrder';

    jQuery.support.cors = true;
    var JSONSendData = {
        sitecode: sitecode,
        firstname: firstname,
        lastname: lastname,
        houseno: houseno,
        address1: address1,
        address2: address2,
        city: city,
        postcode: postcode,
        state: state,
        mobilephone: mobilephone,
        email: email,
        subscriberchannel: subscriberchannel,
        sourceaddress: sourceaddress,
        ordersimurl: ordersimurl,
        ip_address: ip_address,
        nb_of_sim: nb_of_sim,
        Type: 1
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();

            var stat = data[0].errcode;
            if (stat == 0) {
                $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
                $('.PMSuccess .modal-header').html("Success");
                $('.PMSuccess .modal-body').html(data[0].errmsg + " <br/> Free Sim Id : " + data[0].freesimid_out);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            } else {
                $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "0" });
                $('.PMFailed .modal-header').html("Failure");
                $('.PMFailed .modal-body').html(data[0].errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }

        },
        error: function (data) {
            $("#ajax-screen-masking").hide();
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            $('.PMFailed .modal-header').html("Failure");
            $('.PMFailed .modal-body').html(data[0].errmsg);
            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });
}

//31-Dec-2018 : Moorthy : Added for SIM Reports
function FillSIMReports(MobileNo, SiteCode) {
    $("#SIMReportsContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblSIMReportsContainer\"></table>");
    $("#SIMReportsContainer").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var url = apiServer + "/api/simordersearch";
    var JSONSendData = {
        isDownload: false,
        searchvalue: MobileNo,
        sitecode: SiteCode,
        searchby: "mobileno"
    };
    $.ajax
    ({
        url: url,
        type: "POST",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#SIMReportsContainer").css({ "background-image": "none" });
            $('#tblSIMReportsContainer').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: true,
                bInfo: false,
                iDisplayLength: 5,
                aaSorting: [[0, "desc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No SIM records found."
                },
                aoColumns: [
                { mDataProp: "freesimid", sTitle: "Free SIM ID" },
                { mDataProp: "firstname", sTitle: "First Name" },
                { mDataProp: "freesimstatus", sTitle: "Status" },
                { mDataProp: "FreeSimType", sTitle: "Free SIM Type" },
                { mDataProp: "Subscriber_Type", sTitle: "Subscriber Type" },
                { mDataProp: "Quantity", sTitle: "Quantity" },
                { mDataProp: "SourceReg", sTitle: "Source" },
                { mDataProp: "source_address", sTitle: 'Source Address', sWidth: 100 },
                { mDataProp: "ordersim_url", sTitle: 'Ordersim URL' }

                ],
                fnDrawCallback: function () {
                    $("#tblSIMReportsContainer tbody td").css({ "font-size": "12px" });
                }
            });
        },
        error: function (feedback) {
            $("#SIMReportsContainer").css({ "background-image": "none" }).html("").append("No SIM records found.").show();
        }
    });
}
/* ----------------------------------------------------- 
   *  eof TAB SIM SWAP  
   ===================================================== */


