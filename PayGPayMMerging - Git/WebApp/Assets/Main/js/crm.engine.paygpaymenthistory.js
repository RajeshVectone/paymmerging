﻿/* ===================================================== 
   *  TAB PAYMENT HISTORY PAYG  
   ----------------------------------------------------- */
$(document).ready(function () {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    if (dd < 10) { dd = '0' + dd } if (mm < 10) { mm = '0' + mm } today = dd + '/' + mm + '/' + yyyy;

    var lastdate1 = $('#lastdate1').val();
    if (lastdate1 == '') {
        $('#dp1').attr('data-date', today);
        $('#dp1').datepicker().on('changeDate', function (e) {
            $('div.datepicker').hide();
        });
        $('#startdate').attr('value', today);
        $('#lastdate1').val(today);
    }
    else {
        $('#dp1').attr('data-date', lastdate1);
        $('#dp1').datepicker().on('changeDate', function (e) {
            $('div.datepicker').hide();
        });
        $('#startdate').attr('value', lastdate1);
    }

    var lastdate2 = $('#lastdate2').val();
    if (lastdate2 == '') {
        $('#dp2').attr('data-date', today);
        $('#dp2').datepicker().on('changeDate', function (e) {
            $('div.datepicker').hide();
        });
        $('#enddate').attr('value', today);
        $('#lastdate2').val(today);
    }
    else {
        $('#dp2').attr('data-date', lastdate2);
        $('#dp2').datepicker().on('changeDate', function (e) {
            $('div.datepicker').hide();
        });
        $('#enddate').attr('value', lastdate2);
    }

    $('#OrderListSearchBtn').click(function () {
        var orderType = $("select#orderType").val();
        var fromDate = $.trim($('#startdate').val());
        var toDate = $.trim($('#enddate').val());
        orderList(orderType, fromDate, toDate);
    });

});
/* ----------------------------------------------------------------
* Function Name    : PaymentHistoryPAYG
* Purpose          : to show history of Payment PAYG
* Added by         : Edi Suryadi
* Create Date      : January 20th, 2014
* Last Update      : -
* Update History   : -
* ---------------------------------------------------------------- */
//function PaymentHistoryPAYG(MobileNo, Sitecode) {
function PaymentHistoryPAYG(MobileNo, Decision, PaymentMethod, PaymentType, startdate, enddate, user_role) {
    $("#phpaygFilterResultBody").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblPHPAYGFilterResult\"></table>");
    $("#phpaygFilterResultBody").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });

    // var url = apiServer + "/api/PAYGPaymentHistory?mobileno=" + MobileNo + "&Sitecode="+Sitecode;
    var url = apiServer + "/api/PAYGPaymentHistory?mobileno=" + MobileNo + "&decision=" + Decision + "&paymethod=" + PaymentMethod + "&paytype=" + PaymentType + "&startdate=" + startdate + "&enddate=" + enddate + "&user_role=" + user_role;

    $.ajax
    ({
        url: url,
        type: "GET",
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#phpaygFilterResultBody").css({ "background-image": "none" });
            var viewRefund = 0;
            if (feedback.length > 0) {
                viewRefund = feedback[0].ViewRefund;
                $('#tblPHPAYGFilterResult').dataTable({
                    bDestroy: true,
                    bRetrieve: true,
                    aaData: feedback,
                    bPaginate: true,
                    bFilter: false,
                    bInfo: false,
                    iDisplayLength: 10,
                    aaSorting: [],
                    oLanguage: {
                        "sInfoEmpty": '',
                        "sEmptyTable": "No Payment history PAYG found."
                    },
                    aoColumns: [
                        {
                            mDataProp: "CreateDate", sTitle: "Payment Date", "bUseRendered": false,
                            fnRender: function (ob) {
                                return convertDateISOCustom(ob.aData.CreateDate, 1);
                            }
                        },
                        { mDataProp: "PaymentType", sTitle: "Payment Type" },
                        { mDataProp: "bundle_name", sTitle: "Bundle Name" },
                        { mDataProp: "PaymentMethod", sTitle: "Payment Method" },
                        { mDataProp: "ReferenceId", sTitle: "Transaction ID" },
                        {
                            mDataProp: "TotalAmount", sTitle: "Payment Amount",
                            fnRender: function (ob) {
                                return getCurrencySymbol(ob.aData.Currency) + " " + ob.aData.TotalAmount.toFixed(2);
                            }
                        },
                        { mDataProp: "Status", sTitle: "Status" },
                        { mDataProp: "User", sTitle: "CRM User" },
                        {
                            mDataProp: "IpClient", sTitle: "Action", sWidth: "100px",
                            fnRender: function (ob) {
                                var dataInfo = "";
                                dataInfo += ob.aData.CreateDate + "|";
                                dataInfo += ob.aData.PaymentType + "|";
                                dataInfo += ob.aData.PaymentMethod + "|";
                                dataInfo += ob.aData.ReferenceId + "|";
                                dataInfo += ob.aData.TotalAmount + "|";
                                dataInfo += ob.aData.Status + "|";
                                dataInfo += ob.aData.User + "|";
                                dataInfo += ob.aData.RecipientNo + "|";
                                dataInfo += ob.aData.Operator + "|";
                                dataInfo += ob.aData.Country;

                                var actionURL = "";
                                actionURL = "<button data-info=\"" + dataInfo + "\" data-pid=\"" + ob.aData.ReferenceId + "\" data-pt=\"" + ob.aData.PaymentMethod + "\" class=\"btn blue btnPHPAYGMoreInfo\">More Info</button>";
                                return actionURL;
                            }
                        },
                        {
                            mDataProp: "ViewRefund", sTitle: "Action", sWidth: "100px",// "bVisible": (viewRefund == 0) ? false : true,
                            fnRender: function (ob) {
                                var actionURL = "";
                                //if (ob.aData.ViewRefund == 1) {
                                //if (($.trim(ob.aData.PaymentMethod).toUpperCase() == "CREDIT CARD" || $.trim(ob.aData.PaymentMethod).toUpperCase() == "PAYPAL" || $.trim(ob.aData.PaymentMethod).toUpperCase() == "AUTO TOP-UP") && $.trim(ob.aData.Status).toUpperCase() == "SUCCESSFUL" && $.trim(ob.aData.ReferenceId) != "") {
                                if (user_role == "1" && ob.aData.is_refunded == 0) {
                                    actionURL = "<button data-mn=\"" + ob.aData.MobileNo + "\" data-pid=\"" + ob.aData.ReferenceId + "\" data-amt=\"" + ob.aData.TotalAmountOrg + "\" class=\"btn blue btnCTRFR\">Request for refund</button>";
                                }
                                else {
                                    if (ob.aData.is_refunded == 1)
                                        actionURL = "<button class=\"btn lightgrey btnCTRFR\" disabled=\"disabled\">Balance Refunded</button>";
                                    else if (ob.aData.is_refunded == 2)
                                        actionURL = "<button class=\"btn lightgrey btnCTRFR\" disabled=\"disabled\">Balance Added</button>";
                                    else
                                        actionURL = "<button class=\"btn lightgrey btnCTRFR\" disabled=\"disabled\">Request for refund</button>";
                                }
                                //}
                                return actionURL;
                            }
                        }
                    ],
                    fnDrawCallback: function () {
                        $("#tblPHPAYGFilterResult tbody td").css({ "font-size": "12px" });
                    }
                });
                $("#tblPHPAYGFilterResult tbody td").css({ "font-size": "12px" });
                $("#tblPHPAYGFilterResult_wrapper .row-fluid:eq(0)").remove();
                $("#tblPHPAYGFilterResult_wrapper .row-fluid .span6:eq(0)").addClass("span4");
                $("#tblPHPAYGFilterResult_wrapper .row-fluid .span6:eq(1)").addClass("span8");
                $("#tblPHPAYGFilterResult_wrapper .row-fluid .span6").removeClass("span6");
            }
            else {
                $("#phpaygFilterResultBody").html("").append("No Payment history PAYG found.").show();
            }
        },
        error: function (feedback) {
            $("#phpaygFilterResultBody").css({ "background-image": "none" }).html("").append("No Payment history PAYG found.").show();
        }
    });
}

/* ----------------------------------------------------- 
   *  eof TAB PAYMENT HISTORY PAYG
   ===================================================== */


function RefreshFunction() {
    document.location.reload(true);
}





function getPaymentID(merchID) {
    $("#phpaygFilterResultBody").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblPHPAYGFilterResult\"></table>");
    $("#phpaygFilterResultBody").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var url = apiServer + '/api/FinViewtransaction/';
    jQuery.support.cors = true;
    var JSONSendData = {
        merchID: merchID
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#phpaygFilterResultBody").css({ "background-image": "none" });
            $("#phpaygFilterResultContainer").show();
            $('#tblPHPAYGFilterResult').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: false,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [[0, "desc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Topup history found."
                },
                aoColumns: [
                    { mDataProp: "REFERENCE_ID", sTitle: 'Reference Id' },
                    { mDataProp: "SITECODE", sTitle: 'Site Code' },
                    { mDataProp: "PAYMENT_AGENT", sTitle: 'Payment Agent' },
                    { mDataProp: "SERVICE_TYPE", sTitle: 'Service Type' },
                    { mDataProp: "PRODUCT_CODE", sTitle: 'Code' },
                    { mDataProp: "PAYMENT_MODE", sTitle: 'Mode' },
                    { mDataProp: "ACCOUNT_ID", sTitle: 'Account Id' },
                    { mDataProp: "CC_NO", sTitle: 'CC No' },
                    { mDataProp: "TOTALAMOUNT", sTitle: 'Amount' },
                    { mDataProp: "CURRENCY", sTitle: 'Cur' },
                    {
                        mDataProp: "CREATED_DATE", sTitle: 'Created Date',
                        fnRender: function (ob) {
                            return convertDate01(ob.aData.CREATED_DATE);
                        }
                    },
                    { mDataProp: "SUBSCRIPTIONID", sTitle: 'Subscription Id' },
                    { mDataProp: "CURRENT_STATUS", sTitle: 'Status' },
                    { mDataProp: "CS_ERROR_CODE", sTitle: 'Code' },
                    { mDataProp: "ECI_Value", sTitle: 'ECI' },
                    { mDataProp: "ReasonforReject", sTitle: 'Reject Reason' },
                    {
                        sTitle: "Action", mDataProp: "Action", "bSortable": false,
                        fnRender: function (ob) {
                            var parameters = "N/A";
                            if (ob.aData.CS_ERROR_CODE == '481') {
                                //parameters = "<center><a href=\"javascript:void(0)\" class=\"FailedID\" param-data=\"" + ob.aData.REFERENCE_ID + "\" mimsi-data=\"" + ob.aData.CS_ERROR_CODE + "\">Accept Transaction</a></center>";

                                parameters = "<center><a href=\"javascript:void(0)\" class=\"FailedID\" param-data=\"" + ob.aData.REFERENCE_ID + ":" + ob.aData.ACCOUNT_ID + ":" + ob.aData.TOTALAMOUNT + "\" mimsi-data=\"" + ob.aData.CS_ERROR_CODE + "\">Accept Transaction</a></center>";
                            }
                            else if (ob.aData.CS_ERROR_CODE == '100') {
                                parameters = "<center><a href=\"javascript:void(0)\" class=\"SucessID\" param-data=\"" + ob.aData.REFERENCE_ID + ":" + ob.aData.ACCOUNT_ID + ":" + ob.aData.TOTALAMOUNT + "\" mimsi-data=\"" + ob.aData.CS_ERROR_CODE + "\">Void Transaction</a></center>";
                                //parameters = "<center><a href=\"javascript:void(0)\" class=\"SucessID\"   param-data=\"" + ob.aData.REFERENCE_ID + "\" mimsi-data=\"" + ob.aData.CS_ERROR_CODE + "\">Void Transaction</a></center>";
                            }
                                ////else if (ob.aData.CS_ERROR_CODE == '998') {
                                ////    parameters = "<center><a href=\"javascript:void(0)\" class=\"FailedIDview\" param-data=\"" + ob.aData.REFERENCE_ID + "\" mimsi-data=\"" + ob.aData.CS_ERROR_CODE + "\">View Details</a></center>";
                                ////}
                                ////else if (ob.aData.CS_ERROR_CODE == '999') {
                                ////    parameters = "<center><a href=\"javascript:void(0)\" class=\"SucessIDview\" param-data=\"" + ob.aData.REFERENCE_ID + "\" mimsi-data=\"" + ob.aData.CS_ERROR_CODE + "\">View Deatils</a></center>";
                                ////}
                            else {
                                parameters;
                            }
                            return parameters;
                        }
                    },
                ]
            });
            $("#tblPHPAYGFilterResult tbody td").css({ "font-size": "12px" });
        },
        error: function (feedback) {
            $("#phpaygFilterResultBody").css({ "background-image": "none" }).html("").append("No Payment history PAYG found.").show();
        }
    });

    //100
    $('a.SucessID').live("click", function () {
        $('#failedMsg').modal({ keyboard: false, backdrop: 'static' }).css({
            'margin-left': function () {
                return window.pageXOffset - ($(this).width() / 2);
            }
        });
        $('#StatusCode').val($(this).attr("mimsi-data"));
        $('#MerchID').val($(this).attr("param-data"));
    });
    //481
    $('a.FailedID').live("click", function () {
        $('#SucessMsg').modal({ keyboard: false, backdrop: 'static' }).css({
            'margin-left': function () {
                return window.pageXOffset - ($(this).width() / 2);
            }
        });
        $('#StatusCode').val($(this).attr("mimsi-data"));
        $('#MerchID').val($(this).attr("param-data"));
    });

    $(document).ready(function () {
        //Decission Manager - 481
        //$('#btnSubmit').click(function () {
        //    $('#SucessMsg').modal("hide");            
        //    var reason = $('#ddlaccept').val();
        //    var MerchId = $('#MerchID').val();
        //    var StatusCode = $('#StatusCode').val();
        //    if (reason != null && reason != '')
        //    {
        //        var url = apiServer + '/api/fingetview/';
        //        jQuery.support.cors = true;
        //        var JSONSendData =
        //            {
        //            MerchID: merchID,
        //            StatusCode: StatusCode,
        //            Reason: reason,
        //            Create_revert: '0'
        //        };
        //        $.ajax
        //        ({
        //            url: url,
        //            type: 'POST',
        //            data: JSON.stringify(JSONSendData),
        //            dataType: 'json',
        //            contentType: "application/json;charset=utf-8",
        //            cache: false,
        //            success: function (feedback) {
        //                if (feedback[0].errcode == "0") {
        //                    var list = "Merchant Id : " + feedback[0].REFERENCE_ID + ",<br/>Reason :" + feedback[0].reason + ",<br/>Update Date :" + feedback[0].update_date + ".";
        //                    $('#divResult').html(list);
        //                    $('#failedConformMsg').modal({ keyboard: false, backdrop: 'static' }).css
        //                        ({
        //                            'margin-left': function () {
        //                                return window.pageXOffset - ($(this).width() / 2);
        //                            }
        //                        });
        //                }
        //                else {
        //                    $('#divResult').html(feedback[0].errmsg);
        //                }
        //            }
        //        });
        //    }
        //    else {
        //        //Validationalert(data);

        //    }


        //});


        $('#btnSubmit').click(function () {
            $('#SucessMsg').modal("hide");
            var reason = $('#ddlaccept').val();
            var MerchId = $('#MerchID').val();
            var StatusCode = $('#StatusCode').val();
            if (reason != null && reason != '') {
                var url = apiServer + '/api/fingetview/';
                jQuery.support.cors = true;
                var JSONSendData =
                    {
                        MerchID: MerchId,
                        StatusCode: StatusCode,
                        Reason: reason,
                        Create_revert: '0'
                    };
                $.ajax
                ({
                    url: url,
                    type: 'POST',
                    data: JSON.stringify(JSONSendData),
                    dataType: 'json',
                    contentType: "application/json;charset=utf-8",
                    cache: false,
                    success: function (feedback) {
                        if (feedback[0].errcode == "0") {
                            //if (feedback[0].prevbal != '' || feedback[0].prevbal == 'undefined') {
                            //    var list = feedback[0].status + ",<br/>" + "Merchant Id : " + feedback[0].REFERENCE_ID + ",<br/>Reason :" + feedback[0].reason + ",<br/>Update Date :" + feedback[0].update_date +".";
                            //}
                            //else {
                            var list = feedback[0].status + ",<br/>" + "Merchant Id : " + feedback[0].REFERENCE_ID + ",<br/>Reason :" + feedback[0].reason + ",<br/>Update Date :" + feedback[0].update_date + ",<br/>Previous balance :" + feedback[0].prevbal + ",<br/>New balance :" + feedback[0].afterbal + ".";

                            $('#divResult').html(list);
                            $('#failedConformMsg').modal({ keyboard: false, backdrop: 'static' }).css
                                ({
                                    'margin-left': function () {
                                        return window.pageXOffset - ($(this).width() / 2);
                                    }
                                });
                        }
                        else {
                            $('#divResult').html(feedback[0].errmsg);
                        }
                    }
                });
            }
            else {
                //Validationalert(data);

            }

        });
        //Sucess Payement
        $('#btnConform').click(function () {
            ddlaccept
            $('#failedMsg').modal("hide");
            var MerchId = $('#MerchID').val();
            var StatusCode = $('#StatusCode').val();
            var rejectionValues = $('#ddlRejection').val();
            var url = apiServer + '/api/fingetview/';
            jQuery.support.cors = true;
            var JSONSendData =
                {
                    MerchID: MerchId,
                    StatusCode: StatusCode,
                    Reason: rejectionValues,
                    Create_revert: '1'
                };
            $.ajax
            ({
                url: url,
                type: 'POST',
                data: JSON.stringify(JSONSendData),
                dataType: 'json',
                contentType: "application/json;charset=utf-8",
                cache: false,
                success: function (feedback) {
                    if (feedback[0].errcode == "0") {

                        var list = feedback[0].status + ",<br/>" + "Merchant Id : " + feedback[0].REFERENCE_ID + ",<br/>Reason :" + feedback[0].reason + ",<br/>Update Date :" + feedback[0].update_date + ",<br/>New balance :" + feedback[0].prevbal + ",<br/>Previous balance :" + feedback[0].afterbal + ".";

                        $('#divResult').html(list);
                        $('#failedConformMsg').modal({ keyboard: false, backdrop: 'static' }).css
                            ({
                                'margin-left': function () {
                                    return window.pageXOffset - ($(this).width() / 2);
                                }
                            });
                    }
                    else {
                        $('#divResult').html(feedback[0].errmsg);
                    }
                }




            });
        });
    });
}



