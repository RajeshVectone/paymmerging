﻿function FreeCredit(sitecode,mobileno,username) {debugger
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" }); 
    $("#tblFreeCreditContainerBody").html("").append("<table class=\"table table-bordered\" id=\"tblFreeCredit\"></table>");
    var url = apiServer + '/api/FreeCredit';
    jQuery.support.cors = true;
    var JSONSendData = {
        sitecode: sitecode 
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
            $("#tblFreeCreditContainer").show();
        },
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            $("#tblFreeCreditLoader").hide();
            $('#tblFreeCredit').dataTable({
                aaData: feedback,
                bDestroy: true,
                bRetrieve: true,
                iDisplayLength: 10,
                aaSorting: [],
                aoColumns: [
                        { mDataProp: "bundleid", sTitle: "Product ID" },
                        { mDataProp: "name", sTitle: "Free Credit Name" },
                        { mDataProp: "price", sTitle: "Price" },
                        { mDataProp: "offer_type", sTitle: "Offer Type" },
                        { mDataProp: "renewal_delay", sTitle: "Renewal Delay" },
                         { mDataProp: "is_promo", sTitle: "Is Promo", "bVisible": false },
                        {
                            mDataProp: "ACTION", sTitle: "Action", sWidth: "150px",
                            fnRender: function (ob) { 
                                var actionURL = "";
                                if (ob.aData.bundleid != 0) {
                                    actionURL += "&nbsp;&nbsp;<button alt=\"" + ob.aData.bundleid + "\" sitecode=\"" + sitecode
                                        + "\" mobileno=\"" + mobileno + "\" paymode=\"" + 3 + "\" iccid=\"" + $('#tdCIHICCID').text() + "\" is_promo=\"" + ob.aData.is_promo + "\" process_by=\"" + "CRM" + "\"  crm_user=\"" + username + "\"  price=\"" + ob.aData.price + "\"  class=\"btn blue btnFreeCreditSubscribe\" style=\"width:100px\">Subscribe</button>";
                                }
                                else {
                                                                        
                                }
                                return actionURL;
                            }
                        }
                ],
                fnDrawCallback: function () {
                    //$("#tblFreeCreditContainer input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                },
                fnInitComplete: function (ob) {
                   // $("#tblFreeCreditContainer input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                },
                error: function (feedback) {
                    $("#tblFreeCreditLoader").hide();
                    $("#tblFreeCreditContainer").html("").append("No free credit found").show();
                }
            });

        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}

function Subscribe(BundleID, sitecode, mobileno, ICICID, is_promo, process_by, crm_user, price) {
    var row = 0;
    var url = apiServer + "/api/FreeCreditSubscribe";
    var JSONSendData = {
        sitecode: sitecode,
        mobileno: mobileno,
        iccid: ICICID,
        bundleid: BundleID,
        paymode: 3,
        is_promo: is_promo,
        process_by: process_by,
        crm_user: crm_user,
        price: price
    };
    $.ajax
    ({
        url: url,
        type: "post",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            var stat = feedback[0].errcode;
            if (stat == 0) {
                alert("Subscription successful");
            }
            else {
                alert("Subscription Failure");
            } 
        },
        error: function (feedback) {
            alert("Failure");
            //$("#tblFreeCreditLoader").hide();
            //$("#tblFreeCreditContainer").html("").append("No free credit found").show();
        }
    });
}