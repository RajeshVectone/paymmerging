﻿$(document).ready(function () {

    ActivateTopbarMenu(2);

    $("#SearchBy, #SearchText").css({ "margin-top": "9px" });
    $("#btnSearch").css({ "margin-top": "3px" });
    $("input#SearchText").focus();
    if ($('#SearchBy').val() == 'MobileNo') {
        $('#hintext').text('Include country code. Example: 447574123456');
    } else {
        $('#hint').hide();
    }

    $("input#SearchText").live("focusin", function () {
        $('#hint').fadeIn();
    });
    $("input#SearchText").live("focusout", function () {
        $('#hint').fadeOut();
    });

    $('.close').click(function () {
        $(this).parent().fadeOut();
    });

    $('#SearchBy').change(function () {
        var x = $(this).val();
        switch (x) {
            case 'MobileNo':
                $('#hintext').text('Include country code. Example: 447574123456');
                break;
            case 'FullName':
                $('#hintext').text('Enter part of the Name');
                break;
            case 'pp_customer_id':
                $('#hintext').text('Enter the Customer ID.');
                break;
            case 'subscriberid':
                $('#hintext').text('Enter the Subscription ID.');
                break;
            case 'freesimid':
                $('#hintext').text('Enter the SIM Order ID.');
                break;
            case 'ICCID':
                $('#hintext').text('89xxxxxx (18 or 19 digits)');
                break;
            case 'Email':
                $('#hintext').text('Enter the Email Address.');
                break;
            case 'SerialNo':
                $('#hintext').text('12 digits. Example: 330012341234.');
                break;
            case 'LoginName':
                $('#hintext').text('Enter part of the Login.');
                break;
            //case 'Firstname':
            //    $('#hintext').text('Enter the First name.');
            //    break;
            //case 'LastName':
            //    $('#hintext').text('Enter the Last Name.');
            //    break;
            //case 'AccountCode':
            //    $('#hintext').text('Enter the Account Access Code.');
            //    break;
        }
        $("input#SearchText").focus();
    });

    $("button.btnSelectCustomer").live("click", function () {
      
        var altData = $.trim($(this).attr("alt")).split('|');
        var mobileno = $.trim(altData[0]);
        var productcode = $.trim(altData[1]);
        var SIM_Type = $.trim(altData[2]);
        mobileno = mobileno + "-" + SIM_Type

        //Customer Overview Page
        //if (SIM_Type == 'PAYM') {
        //    window.location = "/PAYM/Detail1/" + mobileno;
        //}
        //else {
        //    window.location = "/Customer/Detail1/" + mobileno;
        //}
        if (SIM_Type == 'PAYM') {
            window.location = "/PAYM/TabPOverview/" + mobileno;
        }
        else {
            window.location = "/Customer/TabCOverview/" + mobileno;
        }


        //29-Jan-2016 : Moorthy : Modified to load the page using the sim type
        //if (SIM_Type == 'PAYM') {
        //    window.location = "/PAYM/Detail1/" + mobileno;
        //}
        //else {
        //    window.location = "/Customer/Detail1/" + mobileno;
        //}
        //var url = apiServer + '/api/customersearch';
        //jQuery.support.cors = true;
        //var JSONSendData = {
        //    Product_Code: productcode,
        //    SearchBy: 'MobileNo',
        //    SearchText: mobileno,
        //    Sitecode: $('#gSiteCode').val(),
        //    Search_Type: 2,
        //    SIM_Type: SIM_Type
        //};
        //$.ajax
        //({
        //    url: url,
        //    type: 'post',
        //    data: JSON.stringify(JSONSendData),
        //    dataType: 'json',
        //    contentType: "application/json;charset=utf-8",
        //    cache: false,
        //    success: function (feedback) {
        //        if (feedback.bill_type == 'PAYM') {
        //            window.location = "/PAYM/Detail1/" + mobileno;
        //        }
        //        else {
        //            window.location = "/Customer/Detail1/" + mobileno;
        //        }
        //    }
        //});

    });
});