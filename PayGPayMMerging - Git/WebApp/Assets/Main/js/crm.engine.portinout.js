﻿/* ===================================================== 
   *  TAB GENERAL 
   ----------------------------------------------------- */

/* ----------------------------------------------------------------
 * Function Name    : Port in Port out Page
 * Purpose          : to show porting details
 * Added by         : Rajesh
 * Create Date      : Dec 24th, 2018
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function GetPortinDetails(productCode, searchText, Sitecode) {
    $("#PortinLoader").show();
    var SIM_Type = 'PAYG';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1) {
        SIM_Type = 'PAYM';
    }

    var url = apiServer + '/api/Porting';

    jQuery.support.cors = true;
    var JSONSendData = { 
        MobileNo: searchText,
        Sitecode: Sitecode
    };

    $.ajax
   ({
       url: url,
       type: 'post',
       data: JSON.stringify(JSONSendData),
       dataType: 'json',
       contentType: "application/json;charset=utf-8",
       cache: false,
       success: function (feedback) {
           $("#PortinLoader").hide();
           if (feedback.portin_operator != null && feedback.portin_operator != "") {
               //$("#tblPortinContainer").show();
               $("table#tblPortinOverview td#tdPortInFromOperator").html(capitalizeString(feedback.portin_operator));
           }
           else {
               $("table#tblPortinOverview td#tdPortInFromOperator").html("");
           }

           if (feedback.potin_date_str != null && feedback.potin_date_str != "01-01-0001 00:00:00")
               $("table#tblPortinOverview td#tdPortInDate").html(capitalizeString(feedback.potin_date_str));
           else
               $("table#tblPortinOverview td#tdPortInDate").html("");

           if (feedback.portin_bonus != null)
               $("table#tblPortinOverview td#tdPortinjoiningbonus ").html(feedback.portin_bonus);
           else
               $("table#tblPortinOverview td#tdPortinjoiningbonus").html("");
           
           if (feedback.portout_date_str != null && feedback.portout_date_str != "01-01-0001 00:00:00")
               $("table#tblPortinOverview td#tdDateofportout").html(feedback.portout_date_str);
           else
               $("table#tblPortinOverview td#tdDateofportout").html("");

           if (feedback.portout_operator != null )
               $("table#tblPortinOverview td#PortoutoperatorName").html(capitalizeString(feedback.portout_operator));
           else
               $("table#tblPortinOverview td#PortoutoperatorName").html("");
            
           if (feedback.pack_code_request_date != null && feedback.pack_code_request_date != "01-01-0001 00:00:00")
               $("table#tblPortinOverview td#tdPACCodeRequestDate").html(feedback.pack_code_request_date_str);
           else
               $("table#tblPortinOverview td#tdPACCodeRequestDate").html("");
           
           if (feedback.paccode != null )
               $("table#tblPortinOverview td#tdPaccode").html(capitalizeString(feedback.paccode));
           else
               $("table#tblPortinOverview td#tdPaccode").html("");          
                 
           if (feedback.portout_operator != null)
               $("table#tblPortinOverview td#tdPortoutoperatorName ").html(capitalizeString(feedback.portout_operator));
           else
               $("table#tblPortinOverview td#tdPortoutoperatorName ").html("");

                      
           if (feedback.portout_date_str != null && feedback.portout_date_str != "01-01-0001 00:00:00")
               $("table#tblPortinOverview td#tdRequestdate").html(feedback.portout_date_str);
           else
               $("table#tblPortinOverview td#tdRequestdate").html("");

        
           if (feedback.charging_fee != null)
               $("table#tblPortinOverview td#tdAmount").html(feedback.charging_fee);
           else
               $("table#tblPortinOverview td#tdAmount").html("");
       },
       error: function (feedback) {
           $("#PortinLoader").hide();
       }
   });

}