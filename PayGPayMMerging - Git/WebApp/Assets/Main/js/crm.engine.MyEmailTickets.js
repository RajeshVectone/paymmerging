﻿function GetMyEmailTickets(userid, role_id, status_id) {
    $("#pbMyEmailTicketsBody").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblPBEmailTickets\"></table>");
    $("#pbMyEmailTicketsBody").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var url = apiServer + '/api/Emails';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 10,
        userid: userid,
        roleid: role_id,
        status_id: status_id
    };
    var row = 0;
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#pbMyEmailTicketsBody").css({ "background-image": "none" });
            $('#tblPBEmailTickets').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                iDisplayLength: 10,
                bPaginate: true,
                //bLengthChange: false,
                bFilter: true,
                bInfo: false,
                bAutoWidth: false,
                aaSorting: [[0, "desc"]],
                aoColumns: [
                    { mDataProp: "queue_id", sTitle: "ID", sWidth: 50 },
                    { mDataProp: "Ticket_id", sTitle: "Ticket ID", sWidth: 150 },
                    { mDataProp: "mobileno", sTitle: "Mobile No", sWidth: 150 },
                    { mDataProp: "customer_name", sTitle: "Customer Name", sWidth: 150 },
                    { mDataProp: "assigned_from", sTitle: "Assigned By", sWidth: 150 },
                    { mDataProp: "esclating_user_txt", sTitle: "Escalated To", sWidth: 150 },
                    { mDataProp: "queue_status_txt", sTitle: "Status", sWidth: 75 },
                    { mDataProp: "compaint_category", sTitle: "Category", sWidth: 75 },
                    { mDataProp: "issue_generate_date_string", sTitle: "Date & Time", sWidth: 150 },
                    {
                        mDataProp: "descr_trim", sTitle: "Desc", sWidth: 250,
                        //mDataProp: "descr_trim", sTitle: "Desc", sWidth: 500,
                        //fnRender: function (ob) {
                        //    var actionURL = "";
                        //    actionURL = "<p data-toggle=\"tooltip\" title=\"" + ob.aData.descr + "\">" + ob.aData.descr_trim + "</p>";
                        //    return actionURL;
                        //}
                    },
                    {
                        mDataProp: "Issue_Name", sTitle: "Action", bSortable: false, sWidth: 50,
                        fnRender: function (ob) {
                            var actionURL = "";
                            var btnTitle = "";
                            //if (ob.aData.queue_status_txt.toUpperCase() == "ASSIGNED") {
                            //    btnTitle = "Generate";
                            //}
                            //else 
                            if (ob.aData.queue_status_txt.toUpperCase() == "OPEN") {
                                btnTitle = "Open";
                            }
                            if (ob.aData.queue_status_txt.toUpperCase() == "ASSIGNED") {
                                btnTitle = "Assigned";
                            }
                            if (ob.aData.queue_status_txt.toUpperCase() == "INPROGRESS") {
                                btnTitle = "Inprogress";
                            }
                            else if (ob.aData.queue_status_txt.toUpperCase() == "PENDING") {
                                btnTitle = "Pending";
                            }
                            else if (ob.aData.queue_status_txt.toUpperCase() == "RESOLVED") {
                                btnTitle = "Resolved";
                            }
                            else if (ob.aData.queue_status_txt.toUpperCase() == "CLOSED") {
                                btnTitle = "Closed";
                            }
                            
                            actionURL = "<button id=\"generateticket\" class=\"btn blue\" Ticket_id=\"" + ob.aData.Ticket_id + "\" Issue_Name=\"" + ob.aData.Issue_Name + "\" Function_Name=\"" + ob.aData.Function_Name + "\" Complaint_Name=\"" + ob.aData.Complaint_Name + "\" desc=\"" + ob.aData.descr + "\" issue_type=\"" + ob.aData.issue_type + "\" Issue_Function=\"" + ob.aData.Issue_Function + "\" Issue_Complaint=\"" + ob.aData.Issue_Complaint + "\" customer_name=\"" + ob.aData.customer_name + "\" queue_status=\"" + ob.aData.queue_status + "\" queue_status_txt=\"" + ob.aData.queue_status_txt + "\" ctlValue=\"" + ob.aData.queue_id + "\" esclating_user_txt=\"" + ob.aData.esclating_user_txt + "\" agent_email=\"" + ob.aData.agent_email + "\"  cust_email=\"" + ob.aData.cust_email + "\" mobileno=\"" + ob.aData.mobileno + "\" category=\"" + ob.aData.compaint_category + "\">" + btnTitle + "</button>";
                            return actionURL;
                        }
                    },
                ],
                //fnDrawCallback: function () {
                //    $("#pbMyEmailTicketsBody tbody td").css({ "font-size": "12px" });
                //}
                fnDrawCallback: function () { 
                     
                //    if ($('#tblPBEmailTickets tr').length == 2) {
                //    $('.dataTables_paginate').hide();
                //}
                //    else {
                //        $("#pbMyEmailTicketsBody tbody td").css({ "font-size": "12px" });
                //    $('.dataTables_paginate').show();
                //}
            }
            });
        },
        error: function (feedback) {
            $("#pbMyEmailTicketsBody").css({ "background-image": "none" });
            $('#pbMyEmailTicketsBody ul').append("No records found.");
        }
    });
}

function MyEmailTicketsFillStatus() {
    $('#ddlEGTStatus').find('option').remove();
    //var actionOption = '<option value="-1" style=\"color:#c0c0c0\">--- Select ---</option>';
    var actionOption = '<option ctlValue="' + "2" + '"   value="' + "2" + '" style=\"padding:3px\">' + "Inprogress" + '</option>';
    actionOption += '<option ctlValue="' + "3" + '"   value="' + "3" + '" style=\"padding:3px\">' + "Resolved" + '</option>';
    $('#ddlEGTStatus').append(actionOption);
}


function ChangeEmailQueueStatus(queue_id, queue_status, agent_email, cust_email, assigned_user_id, descr, compaint_category) {
    $("#ajax-screen-masking").show();
    var url = apiServer + '/api/MailChimp';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 10,
        queue_id: queue_id,
        status: queue_status,
        email_to: agent_email,
        email_cc: cust_email,
        assigned_user_id: assigned_user_id,
        description: descr,
        compaint_category: compaint_category
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: true,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            if (feedback != null && feedback.length > 0) {
                alert(feedback[0].errmsg);
                if (feedback[0].errcode == 0) {
                    $('#divEmailGenerateTicket').modal('hide');
                    window.location = '/Emails/EmailList/2';
                }
            }
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
} 

//Reply
//function MyEmailTicketsReply(queue_id, email_to, email_cc, email_subject, email_body) {
//    var url = apiServer + '/api/Emails';
//    jQuery.support.cors = true;
//    var JSONSendData = {
//        modetype: 7,
//        queue_id: queue_id,
//        email_to: email_to,
//        email_cc: email_cc,
//        email_subject: email_subject,
//        email_body: email_body
//    };
//    $.ajax
//    ({
//        url: url,
//        type: 'post',
//        data: JSON.stringify(JSONSendData),
//        dataType: 'json',
//        contentType: "application/json;charset=utf-8",
//        cache: true,
//        success: function (feedback) {
//            if (feedback[0].errcode == 0) {
//                alert("Email sent successfully!");
//                $('#divEmailReply').modal('hide');
//            }
//            else {
//                alert(feedback[0].errmsg);
//            }
//        },
//        error: function (feedback) {

//        }
//    });
//}

function FillEscalatingTeam() {
    $("#ajax-screen-masking").show();
    $('#ddlEGTEscalatingTeam').find('option').remove();
    var url = apiServer + '/api/EmailGenerateTicket';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 7
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: true,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            var feedbackarr = jQuery.makeArray(JSON.stringify(feedback).split('}'));
            var icount;
            var actionOption = '<option value="0" style=\"color:#c0c0c0\">--- Select ---</option>';
            for (icount = 0; icount < feedbackarr.length - 1; icount++) {
                actionOption += '<option ctlValue="' + feedback[icount].Email + '"   value="' + feedback[icount].Id + '" style=\"padding:3px\">' + feedback[icount].Username + '</option>';
            }
            $('#ddlEGTEscalatingTeam').append(actionOption);
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}