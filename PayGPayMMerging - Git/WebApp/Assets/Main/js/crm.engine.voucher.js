﻿$(document).ready(function () {

    ActivateTopbarMenu(4);

    var URLCountry = apiServer + '/api/countryinfo';
    $.getJSON(URLCountry, function (data) {
        $.each(data, function (index, val) {
            var Country = val.Country,
                Sitecode = val.Sitecode;
            if (Country != 'MMI' && Sitecode != 'MMI') {
                $('#Country').append('<option value="' + Sitecode + '">' + Country + '</option>');
            }
        });
        $('#CountryLoader').hide();
        $('#Country,#SearchVoucherBtn').removeAttr('disabled');
    });

    $('#SearchVoucherBtn').click(function (e) {
        
        var type = $('#searchType').val(),
            voucherCode = $('#codeVoucher').val(),
            siteCode = $('#Country').val();

        var url = apiServer + '/api/voucherinfo';
        var ajaxSpinnerTop = $(window).outerHeight() / 2;
        $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
        $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

        jQuery.support.cors = true;
        var JSONSendData = {
            SearchType: type,
            SearchText: voucherCode,
            SiteCode: siteCode
        };

        $.ajax
        ({
            url: url,
            type: 'post',
            data: JSON.stringify(JSONSendData),
            dataType: 'json',
            contentType: "application/json;charset=utf-8",
            cache: false,
            beforeSend: function () {
                $("#ajax-screen-masking").show();
                $('#tbVoucherList').hide();
                $('#tbVoucherListContainer').hide();
            },
            success: function (feedback) {
                $("#ajax-screen-masking").hide();
                $('#tbVoucherListContainer').show();

                if (convertNulltoEmptyString(feedback.UseDate) == '' && 
                    convertNulltoEmptyString(feedback.Voucher_Type) == '' && 
                    convertNulltoEmptyString(feedback.SerialNumber) == '' && 
                    convertNulltoEmptyString(feedback.TrffClass) == ''
                ) {
                    $("tr#voucherAvailable").hide();
                    $("tr#noVoucher").show();
                } else {
                    $("tr#voucherAvailable").show();
                    $("tr#noVoucher").hide();
                    $('#val_cbs').text(convertNulltoEmptyString(feedback.SerialNumber));
                    $('#val_usageDate').text(feedback.UsageDate_String);
                    $('#val_amount').text(feedback.price_string);
                    $('#val_voucherType').text(feedback.Voucher_Type);
                    $('#val_tarifClass').text(feedback.TrffClass);
                    $('#val_upload_date').text(feedback.upload_date_String);
                    $('#val_activationDate').text(feedback.ActivationDate_String);
                    $('#val_expDate').text(feedback.ExpiryDate_String);
                    $('#val_Status').text(feedback.Voucher_Status);
                }
                $('#tbVoucherList').fadeIn();
            }
        });
       
    });
});