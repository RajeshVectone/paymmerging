﻿//ChargebackSuspension Screen
$(document).ready(function () {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    if (dd < 10) { dd = '0' + dd } if (mm < 10) { mm = '0' + mm } today = dd + '/' + mm + '/' + yyyy;

    var Stdate = new Date();
    Stdate.setDate(Stdate.getDate());
    var Stdd = Stdate.getDate();
    var Stmm = Stdate.getMonth() + 1; //January is 0!
    var Styyyy = Stdate.getFullYear();
    if (Stdd < 10) { Stdd = '0' + Stdd } if (Stmm < 10) { Stmm = '0' + Stmm } Stdate = Stdd + '/' + Stmm + '/' + Styyyy;

    var date = "28/02/2019";
    $('#ttStartDateContainer').attr('data-date', date);
    $('#ttStartDateContainer').datepicker().on('changeDate', function (e) {
        $('div.datepicker').hide();
    });
    $('#ttStartDate').attr('value', date);
    $('#ttStartDateH').val(date);

    $('#ttAStartDateContainer').attr('data-date', date);
    $('#ttAStartDateContainer').datepicker().on('changeDate', function (e) {
        $('div.datepicker').hide();
    });
    $('#ttAStartDate').attr('value', date);
    $('#ttAStartDateH').val(date);

    $('#ttEndDateContainer').attr('data-date', today);
    $('#ttEndDateContainer').datepicker().on('changeDate', function (e) {
        $('div.datepicker').hide();
    });
    $('#ttEndDate').attr('value', today);
    $('#ttEndDateH').val(today);

    $('#ttAEndDateContainer').attr('data-date', today);
    $('#ttAEndDateContainer').datepicker().on('changeDate', function (e) {
        $('div.datepicker').hide();
    });
    $('#ttAEndDate').attr('value', today);
    $('#ttAEndDateH').val(today);

    SetDefault();
});

function SetDefault() {
    $('#productListFinance').val("0");
    $("#txtPayReference").val("");
    $("#txtMSISDN").val("");
    $("#txtReason").val("");
    $("#txtCustomerName").val("");
    $("#txtTransAmount").val("");
    $("#txtChargebackFee").val("");
    $("#ddlStatus").val("-1");

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    if (dd < 10) { dd = '0' + dd } if (mm < 10) { mm = '0' + mm } today = dd + '/' + mm + '/' + yyyy;

    var Stdate = new Date();
    Stdate.setDate(Stdate.getDate());
    var Stdd = Stdate.getDate();
    var Stmm = Stdate.getMonth() + 1; //January is 0!
    var Styyyy = Stdate.getFullYear();
    if (Stdd < 10) { Stdd = '0' + Stdd } if (Stmm < 10) { Stmm = '0' + Stmm } Stdate = Stdd + '/' + Stmm + '/' + Styyyy;

    $('#dp1').attr('data-date', Stdate);
    $('#dp1').datepicker().on('changeDate', function (e) {
        $('div.datepicker').hide();
    });
    $('#startdate').attr('value', Stdate);
    $('#lastdate1').val(Stdate);

    $('#dp2').attr('data-date', today);
    $('#dp2').datepicker().on('changeDate', function (e) {
        $('div.datepicker').hide();
    });
    $('#enddate').attr('value', today);
    $('#lastdate2').val(today);
}

function DoInsertChargebackTransaction(product_code, sitecode, pay_reference, transaction_date, mobileno, reason, chargeback_claim_date, entered_by, proces_type, req_type, customer_name, trans_amount, chargeback_fee) {
    var url = apiServer + "/api/ChargebackSuspension";
    jQuery.support.cors = true;
    var JSONSendData = {
        product_code: product_code,
        sitecode: sitecode,
        pay_reference: pay_reference,
        transaction_date: transaction_date,
        mobileno: mobileno,
        reason: reason,
        chargeback_claim_date: chargeback_claim_date,
        entered_by: entered_by,
        proces_type: proces_type,
        req_type: req_type,
        customer_name: customer_name,
        trans_amount: trans_amount,
        chargeback_fee: chargeback_fee,
        Mode_Type: 1
    };
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            var errcode = feedback[0].errcode;
            if (errcode == 0) {
                SetDefault();
                $('.PMSuccess .modal-header').html("Chargeback Suspension");
                $('.PMSuccess .modal-body').html(feedback[0].errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
            else {
                $('.PMFailed .modal-header').html("Chargeback Suspension");
                $('.PMFailed .modal-body').html(feedback[0].errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (err) {
            $("#ajax-screen-masking").hide();
        }
    });
}

function GetRequestedChargeback(userid, user_role, role_id, status, date_from, date_to) {
    $("#tblBlockUnBlockReportsContainer2").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblBlockUnBlockReports2\"></table>");
    $("#tblBlockUnBlockReportsContainer2").css({ "overflow": "auto", "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });

    var url = apiServer + "/api/ChargebackSuspension";
    jQuery.support.cors = true;
    var JSONSendData = {
        userid: userid,
        role_id: role_id,
        status: status,
        date_from: date_from,
        date_to: date_to,
        Mode_Type: 2
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#btnBlockUnBlockReportsSettingContainer2").css({ "background-image": "none" });
            $("#tblBlockUnBlockReportsContainer2").css({ "background-image": "none" });
            $('#tblBlockUnBlockReports2').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                iDisplayLength: 10,
                bPaginate: true,
                bFilter: true,
                bInfo: false,
                aaSorting: [],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Records Found "
                },
                aoColumns: [
                    { mDataProp: "product_code", sTitle: "Product Code" },
                    { mDataProp: "req_type_name", sTitle: "Request Type" },
                    { mDataProp: "mobileno", sTitle: "Mobile No" },
                    { mDataProp: "pay_reference", sTitle: "Payment Reference" },
                    { mDataProp: "customer_name", sTitle: "Customer Name" },
                    { mDataProp: "chargeback_fee", sTitle: "Chargeback Fee" },
                    { mDataProp: "trans_amount", sTitle: "Transaction Amount" },
                    { mDataProp: "transaction_date_string", sTitle: "Transaction Date" },
                    { mDataProp: "chargeback_claim_date_string", sTitle: "Chargeback Claim Date" },
					{ mDataProp: "entered_by", sTitle: "Entered By" },
                    { mDataProp: "block_date_string", sTitle: "Entered Date" },
                    { mDataProp: "reason", sTitle: "Reason", sWidth: 250 },
                    {
                        mDataProp: "req_type", sTitle: "Action", bSortable: false, //"bVisible": user_role= 'Admin' ? true : false,
                        fnRender: function (ob) {
                            var actionURL = "";
                            if ((user_role == 'Admin' || userid == '1205') && ob.aData.status == 0) {
                                actionURL = "<button class=\"btn blue btnApprove\" ctlValue=\"" + ob.aData.pay_reference + "\" ctlValue2=\"" + ob.aData.mobileno + "\" ctlValue3=\"" + ob.aData.req_type + "\" ctlValue4=\"" + ob.aData.sitecode + "\">Approve</button>";
                            }
                            else
                                actionURL = "<button class=\"btn lightgrey btnApprove\" disabled=\"disabled\" >Approved</button>";
                            return actionURL;
                        }
                    },
                ],
                fnDrawCallback: function () {
                    $("#tblBlockUnBlockReports2 tbody td").css({ "font-size": "12px" });
                }
            });
        },
        error: function (feedback) {
            $("#btnBlockUnBlockReportsSettingContainer2").css({ "background-image": "none" });
            $("#tblBlockUnBlockReportsContainer2").css({ "background-image": "none" });
            $("#tblProductPackagesContainer2").html("").append("No data found").show();
        }
    });
}

function GetRequestedChargebackForUnBlock(userid, user_role, role_id, status, date_from, date_to, search_flag) {
    $("#tblBlockUnBlockReportsContainer1").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblBlockUnBlockReports1\"></table>");
    $("#tblBlockUnBlockReportsContainer1").css({ "overflow": "auto", "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });

    var url = apiServer + "/api/ChargebackSuspension";
    jQuery.support.cors = true;
    var JSONSendData = {
        userid: userid,
        role_id: role_id,
        status: status,
        date_from: date_from,
        date_to: date_to,
        search_flag: search_flag,
        Mode_Type: 2
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#btnBlockUnBlockReportsSettingContainer1").css({ "background-image": "none" });
            $("#tblBlockUnBlockReportsContainer1").css({ "background-image": "none" });
            $('#tblBlockUnBlockReports1').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                iDisplayLength: 10,
                bPaginate: true,
                bFilter: true,
                bInfo: false,
                aaSorting: [],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Records Found "
                },
                aoColumns: [
                    { mDataProp: "product_code", sTitle: "Product Code" },
                    { mDataProp: "req_type_name", sTitle: "Request Type" },
                    { mDataProp: "mobileno", sTitle: "Mobile No" },
                    { mDataProp: "pay_reference", sTitle: "Payment Reference" },
                    { mDataProp: "customer_name", sTitle: "Customer Name" },
                    { mDataProp: "chargeback_fee", sTitle: "Chargeback Fee" },
                    { mDataProp: "trans_amount", sTitle: "Transaction Amount" },
                    { mDataProp: "transaction_date_string", sTitle: "Transaction Date" },
                    { mDataProp: "chargeback_claim_date_string", sTitle: "Chargeback Claim Date" },
					{ mDataProp: "entered_by", sTitle: "Entered By" },
                    { mDataProp: "block_date_string", sTitle: "Entered Date" },
					{ mDataProp: "approved_by", sTitle: "Approved By" },
					{ mDataProp: "approved_date_string", sTitle: "Approved Date" },
                    { mDataProp: "reason", sTitle: "Reason", sWidth: 250 },
                    {
                        mDataProp: "req_type", sTitle: "Action", bSortable: false,  //"bVisible": user_role= 'Admin' ? true : false,
                        fnRender: function (ob) {
                            var actionURL = "";
                            if (ob.aData.req_type != 2 && (user_role == 'Admin' || userid == '1205')) {
                                actionURL = "<button class=\"btn blue btnUnBlock\" ctlValue=\"" + ob.aData.pay_reference + "\" ctlValue2=\"" + ob.aData.mobileno + "\">UnBlock</button>";
                            }
                            else
                                actionURL = "<button class=\"btn lightgrey btnUnBlock\" disabled=\"disabled\" >Unblocked</button>";
                            return actionURL;
                        }
                    },
                ],
                fnDrawCallback: function () {
                    $("#tblBlockUnBlockReports1 tbody td").css({ "font-size": "12px" });
                }
            });
        },
        error: function (feedback) {
            $("#btnBlockUnBlockReportsSettingContainer1").css({ "background-image": "none" });
            $("#tblBlockUnBlockReportsContainer1").css({ "background-image": "none" });
            $("#tblProductPackagesContainer1").html("").append("No data found").show();
        }
    });
}

function GetRequestedChargebackCount(date_from, date_to) {
    var url = apiServer + "/api/ChargebackSuspension";
    jQuery.support.cors = true;
    var JSONSendData = {
        date_from: date_from,
        date_to: date_to,
        Mode_Type: 7
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            var msg = JSON.stringify(feedback);
            var data = JSON.parse(JSON.stringify(feedback));
            $('#tdSuspendedAccounts').html(data[0].suspend_count);
            $('#tdActivatedAccounts').html(data[0].activate_count);

        }, error: function (feedback) {

        }
    });
}

function DoApproveRequestedChargeback(userid, role_id, sitecode, pay_reference, mobileno, req_type, approved_user, user_role) {
    var url = apiServer + "/api/ChargebackSuspension";
    jQuery.support.cors = true;
    var JSONSendData = {
        userid: userid,
        role_id: role_id,
        sitecode: sitecode,
        pay_reference: pay_reference,
        mobileno: mobileno,
        req_type: req_type,
        approved_user: approved_user,
        Mode_Type: 3
    };
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            var errcode = feedback[0].errcode;
            if (errcode == 0) {
                $('.PMSuccess .modal-header').html("Chargeback Suspension");
                $('.PMSuccess .modal-body').html(feedback[0].errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
                var date_from = convertDate01($("input#ttAStartDateH").val());
                var date_to = convertDate01($("input#ttAEndDateH").val());
                setTimeout(function () { GetRequestedChargeback(userid, user_role, role_id, $("#ddlStatus").val(), date_from, date_to); }, 100);
            }
            else {
                $('.PMFailed .modal-header').html("Chargeback Suspension");
                $('.PMFailed .modal-body').html(feedback[0].errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (err) {
            $("#ajax-screen-masking").hide();
        }
    });
}

function DoUnblockRequest(loginuser_id, role_id, pay_reference, mobileno, user_role) {
    var url = apiServer + "/api/ChargebackSuspension";
    jQuery.support.cors = true;
    var JSONSendData = {
        pay_reference: pay_reference,
        mobileno: mobileno,
        Mode_Type: 4
    };
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            var errcode = feedback[0].errcode;
            if (errcode == 0) {
                $('.PMSuccess .modal-header').html("UnBlock");
                $('.PMSuccess .modal-body').html(feedback[0].errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
                var date_from = convertDate01($("input#ttStartDateH").val());
                var date_to = convertDate01($("input#ttEndDateH").val());
                setTimeout(function () { GetRequestedChargebackForUnBlock(loginuser_id, user_role, role_id, 1, date_from, date_to); }, 100);
                GetRequestedChargebackCount(date_from, date_to);
            }
            else {
                $('.PMFailed .modal-header').html("UnBlock");
                $('.PMFailed .modal-body').html(feedback[0].errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (err) {
            $("#ajax-screen-masking").hide();
        }
    });
}

function convertDate01(inputDate) {
    if (convertNulltoEmptyString(inputDate) != "") {
        var arrShortMonthList = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
        var splitDate = inputDate.split('/');
        return splitDate[0] + "/" + arrShortMonthList[parseInt(splitDate[1]) - 1] + "/" + splitDate[2];
    } else {
        return "";
    }
}
//01-Mar-2019 : Moorthy : CRMIMP-21 - Chargeback Tab
function GetChargebackByAccountId(mobileno, user_role) {
    var url = apiServer + "/api/ChargebackSuspension";
    jQuery.support.cors = true;
    var JSONSendData = {
        mobileno: mobileno,
        Mode_Type: 5
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#TabGeneralLoader").show();
            $("#phCSContainer").hide();
        },
        success: function (feedback) {
            $("#TabGeneralLoader").hide();
            if (feedback.length > 0) {
                if (feedback[0].req_type == 1 && (user_role == 'Admin' || userid == '1205')) {
                    $("button#btnCSCancel").removeAttr("disabled").removeClass("grey").addClass("blue");
                    $("button#btnCSSendMail").removeAttr("disabled").removeClass("grey").addClass("blue");
                }
                else {
                    $("button#btnCSCancel").attr("disabled", true).removeClass("blue").addClass("grey");
                }
                $("#phCSStatus").html(feedback[0].req_type_name);
                $("#phCSMobileNo").html(feedback[0].mobileno);
                $("#phCSCustomerName").html(feedback[0].customer_name);
                $("#phCSPaymentReference").html(feedback[0].pay_reference);
                $("#phCSTransactionDate").html(feedback[0].transaction_date_string);
                $("#phCSTransactionAmount").html(feedback[0].trans_amount);
                $("#phCSChargebackClaimDate").html(feedback[0].chargeback_claim_date_string);
                $("#phCSProductCode").html(feedback[0].product_code);
                $("#phCSReason").html(feedback[0].reason);
                $("#phCSApprovedBy").html(feedback[0].approved_by);
                $("#phCSApprovedDate").html(feedback[0].approved_date);
                $("#phCSContainer").show();
            }
            else
                $("#phCSContainer").html("No chargeback suspension information found!").show();
        },
        error: function (feedback) {
            $("#TabGeneralLoader").hide();
            $("#phCSContainer").html("No chargeback suspension information found!").show();
        }
    });
}

function DoUnblockRequestFromChargebackTab(loginuser_id, role_id, pay_reference, mobileno, user_role) {
    var url = apiServer + "/api/ChargebackSuspension";
    jQuery.support.cors = true;
    var JSONSendData = {
        pay_reference: pay_reference,
        mobileno: mobileno,
        Mode_Type: 4
    };
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            var errcode = feedback[0].errcode;
            if (errcode == 0) {
                alert(feedback[0].errmsg);
                GetChargebackByAccountId(mobileno, user_role);
            }
            else {
                alert(feedback[0].errmsg);
            }
        },
        error: function (err) {
            $("#ajax-screen-masking").hide();
        }
    });
}

function GetChargebackCollectionPayment(mobileno) {
    var url = apiServer + "/api/ChargebackSuspension";
    jQuery.support.cors = true;
    var JSONSendData = {
        mobileno: mobileno,
        Mode_Type: 6
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#TabGeneralLoader").show();
            $("#phCSContainer").hide();
        },
        success: function (feedback) {
            $("#TabGeneralLoader").hide();
            if (feedback.length > 0) {
                $("#phCSPayment").html(feedback[0].paid_status);
                $("#phCSPS").html(feedback[0].paid_amount);
                $("#phCSPR").html(feedback[0].paid_reference);
                $("#phCSPD").html(feedback[0].paid_date_string);
                $("#phCSPaymentReference").html(feedback[0].paid_date_string);
            }
            else
                $("#phCSContainer").html("No payment information found!").show();
        },
        error: function (feedback) {
            $("#TabGeneralLoader").hide();
            $("#phCSContainer").html("No payment information found!").show();
        }
    });
}

//Emails - Trigger from CRM
function SendMail(firstname, email, sitecode) {
    $("#ajax-screen-masking").show();
    var url = apiServer + '/api/ChargebackSuspension';;
    jQuery.support.cors = true;
    var JSONSendData = {
        firstname: firstname,
        email: email,
        sitecode: sitecode,
        Mode_Type: 8
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            if (feedback[0].errcode == 0) {
                alert(feedback[0].errmsg)
            }
            else {
                alert(feedback[0].errmsg);
            }
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}