﻿/* ===================================================== 
   *  TAB ISSUES TRACKING   
   ----------------------------------------------------- */

/* ----------------------------------------------------------------
 * Function Name    : ITViewAllIssues
 * Purpose          : to show all issues list based on customer
 * Added by         : Edi Suryadi
 * Create Date      : September 18th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function ITViewAllIssues(Subscriberid, Sitecode, mobileno) {
    var SIM_Type = 'PAYG';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1) {
        SIM_Type = 'PAYM';
    }
    $("#itIssuesListContainer").html("").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });

    if (Subscriberid != '' && Sitecode != '') {
        var url = apiServer + '/api/issuetracker/';
        jQuery.support.cors = true;
        var JSONSendData = {
            SearchType: 1,
            Subscriberid: Subscriberid,
            Sitecode: Sitecode,
            SIM_Type: SIM_Type,
            mobileno: mobileno
        };

        $.ajax
        ({
            url: url,
            type: 'post',
            data: JSON.stringify(JSONSendData),
            dataType: 'json',
            contentType: "application/json;charset=utf-8",
            cache: false,
            success: function (data) {
                $("#itIssuesListContainer").css({ "background-image": "none" });
                $("#itIssuesListContainer").append("<button class=\"btn blue\" id=\"btnITNewIssue\" style=\"width:140px;height:35px\">New Call</button>");
                $("#itIssuesListContainer").append("<button class=\"btn blue\" id=\"btnITDeadIssue\" style=\"width:140px;height:35px;margin-left:50px\">Blank Call</button>");
                $("#itIssuesListContainer").append("<div style=\"width:100%;margin-top:15px\"><table style=\"width:100%\" class=\"table table-bordered\" id=\"tblITIssuesList\"></table></div>");

                $('#tblITIssuesList').dataTable({
                    bDestroy: true,
                    bRetrieve: true,
                    aaData: data,
                    bPaginate: true,
                    bFilter: false,
                    bInfo: false,
                    iDisplayLength: 15,
                    aaSorting: [],
                    oLanguage: {
                        "sInfoEmpty": '',
                        "sEmptyTable": "No issues found."
                    },
                    aoColumns: [
                       { mDataProp: "ProblemID", sTitle: "ProblemID", bVisible: false },
                       //{ mDataProp: "SubmitDate_String", sTitle: "Submit Date", sType: "date-euro", sWidth: "150px" },

                        {
                            mDataProp: "SubmitDate", sTitle: "Submit Date", sWidth: "150px",
                            fnRender: function (ob) {
                                return convertDateISOCustom(ob.aData.SubmitDate, 0);
                            }
                        },

                        //{
                        //    mDataProp: "SubmitDate", sTitle: "Submit Date", sWidth: "150px",
                        //    fnRender: function (ob) {
                        //        return convertDateISOCustom(ob.aData.SubmitDate, 0);
                        //    }
                        //},
                        { mDataProp: "SubmitBy", sTitle: "Submit By" },
                        { mDataProp: "TypeDesc", sTitle: "Call Type" },
                        //CRM Enhancement 2
                        { mDataProp: "contact_type", sTitle: "Contact Type" },
                        { mDataProp: "esclated_to", sTitle: "Escalated To" },
                        { mDataProp: "Assigned_To", sTitle: "Assigned To" },
                        { mDataProp: "Category", sTitle: "Category" },
                        { mDataProp: "Sub_category", sTitle: "Sub Category" },
                        { mDataProp: "Status", sTitle: "Status" },
                        { mDataProp: "Interval", sTitle: "Interval" },
                        { mDataProp: "ProblemDesc", sTitle: "Description" },
                        {
                            mDataProp: "Action", sTitle: "Action", sWidth: "150px",
                            fnRender: function (ob) {
                                var actionURL = "";
                                if (ob.aData.Dead_Call_Status == 0) {
                                    actionURL = "<button alt=\"" + ob.aData.ProblemID + "\" esclated_to=\"" + ob.aData.esclated_to + "\" class=\"btn blue btnITDetailIssue\" style=\"width:60px\">Detail</button>";
                                    actionURL += "&nbsp;&nbsp;<button alt=\"" + ob.aData.ProblemID + "\" Issue_Id=\"" + ob.aData.Issue_Id + "\" Function_Id=\"" + ob.aData.Function_Id + "\"  Complaint_Id=\"" + ob.aData.Complaint_Id + "\" esclated_to=\"" + ob.aData.esclated_to + "\" class=\"btn blue btnITUpdateIssue\" style=\"width:60px\">Edit</button>";
                                }
                                else {
                                    actionURL = "<button alt=\"" + ob.aData.ProblemID + "\" class=\"btn lightgrey btnITDetailIssue\" disabled style=\"width:60px\">Detail</button>";
                                    actionURL += "&nbsp;&nbsp;<button alt=\"" + ob.aData.ProblemID + "\" Issue_Id=\"" + ob.aData.Issue_Id + "\" Function_Id=\"" + ob.aData.Function_Id + "\"  Complaint_Id=\"" + ob.aData.Complaint_Id + "\"  class=\"btn lightgrey btnITUpdateIssue\" disabled style=\"width:60px\">Edit</button>";
                                }
                                return actionURL;
                            }
                        }
                    ]
                });
                $("#tblITIssuesList_wrapper .row-fluid:eq(0)").remove();
                $("#tblITIssuesList_wrapper .row-fluid .span6:eq(0)").addClass("span4");
                $("#tblITIssuesList_wrapper .row-fluid .span6:eq(1)").addClass("span8");
                $("#tblITIssuesList_wrapper .row-fluid .span6").removeClass("span6");
            },
            error: function (data) {
                $("#itIssuesListContainer").css({ "background-image": "none" });
                $("#itIssuesListContainer").append("<button class=\"btn blue\" id=\"btnITNewIssue\" style=\"width:140px;height:35px\">New Issue</button><br/>");
                $("#itIssuesListContainer").append("<div style=\"width:98%;padding:10px;border:1px solid #e5e5e5;background-color:#f5f5f5;margin-top:20px\">No issues found.</div>");
            }
        });
    } else {
        $("#itIssuesListContainer").css({ "background-image": "none" });
        $("#itIssuesListContainer").html("<div style=\"height:40px;font-size:14px;margin-top:15px;color:#555555\">Can\'t find Subscriber ID of this customer.</div>");
    }
}

$(document).ready(function () {
    function trim(str) {
        str = str.replace(/^\s+/, '');
        for (var i = str.length - 1; i >= 0; i--) {
            if (/\S/.test(str.charAt(i))) {
                str = str.substring(0, i + 1);
                break;
            }
        }
        return str;
    }

    function dateHeight(dateStr) {
        if (trim(dateStr) != '') {
            var frDate = trim(dateStr).split(' ');
            var frTime;
            if (frDate.length > 1) {
                frTime = frDate[1].split(':');
            }
            var frDateParts = frDate[0].split('-');
            var day = frDateParts[0] * 60 * 24;
            var month = frDateParts[1] * 60 * 24 * 31;
            var year = frDateParts[2] * 60 * 24 * 366;
            var x;
            if (frDate.length > 1) {
                var hour = frTime[0] * 60;
                var minutes = frTime[1];
                x = day + month + year + hour + minutes;
            }
            else {
                x = day + month + year;
            }

        } else {
            var x = 99999999999999999; //GoHorse!
        }
        return x;
    }


    jQuery.fn.dataTableExt.oSort['date-euro-asc'] = function (a, b) {
        var x = dateHeight(a);
        var y = dateHeight(b);
        var z = ((x < y) ? -1 : ((x > y) ? 1 : 0));
        return z;
    };

    jQuery.fn.dataTableExt.oSort['date-euro-desc'] = function (a, b) {
        var x = dateHeight(a);
        var y = dateHeight(b);
        var z = ((x < y) ? 1 : ((x > y) ? -1 : 0));
        return z;
    };

});

/* ----------------------------------------------------------------
 * Function Name    : SaveNewIssue
 * Purpose          : to save new issue data
 * Added by         : Edi Suryadi
 * Create Date      : September 18th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
//CRM Enhancement 2
function SaveNewIssue(sitecode, mobileno, problemcategorytype, problemtype, problemtype1, subscriberid, problemdesc, solutiondesc, submitby, contact_type, Comp_Status, intreval, JIRA_Tickets, Esclated_To, agent_email_id, interval_comment, priority, AssignedTo, AssignPriority) {
    var SIM_Type = 'PAYG';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1) {
        SIM_Type = 'PAYM';
    }
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    var url = apiServer + '/api/issuetracker/';
    jQuery.support.cors = true;
    var JSONSendData = {
        SearchType: 12,
        sitecode: sitecode,
        Mobile_No: mobileno,
        Issue_Id: problemtype1,
        Function_Id: problemtype,
        Complaint_Id: problemcategorytype,
        Subscriber_ID: subscriberid,
        Issue_Desc: problemdesc,
        Solution_Desc: solutiondesc,
        Created_By: submitby,
        problemstatus: 0,
        contact_type: contact_type,
        Comp_Status: Comp_Status,
        intreval: intreval,
        JIRA_Tickets: JIRA_Tickets,
        SIM_Type: SIM_Type,
        Esclated_To: Esclated_To,
        agent_email_id: agent_email_id,
        interval_comment: interval_comment, //CRM Enhancement 2
        priority: priority,
        AssignPriority: AssignPriority,
        AssignedTo: AssignedTo
    };

    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();
            $('#itPWNewIssue').modal('hide');
            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "2" });

            var stat = data.errcode;
            if (stat == 0) {
                //Mail To Escalating Team
                if (Esclated_To != null) {
                    EscalateSendEmail(mobileno, problemdesc, 0);
                }
              
                alert("Issue added Successfully");
                location.reload();
                //$('.PMSuccess .modal-header').html(data.errsubject);
                //$('.PMSuccess .modal-body').html(data.errmsg);
                //$('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                //    'margin-left': function () {
                //        return window.pageXOffset - ($(this).width() / 2);
                //    }
                //});
            } else {
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (data) {
            $("#ajax-screen-masking").hide();
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            $('.PMFailed .modal-header').html(data.errsubject);
            $('.PMFailed .modal-body').html(data.errmsg);
            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });
}

function EscalateSendEmail(mobileno, problemdesc, alertFlag) {
    if (alertFlag == 1)
        $("#ajax-screen-masking").show();
    var url = apiServer + '/api/MailChimp';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 8,
        email_to: $("#itPWNIEscalatingTeam option:selected").attr("ctlvalue"),
        email_subject: "Ticket has been generated in CRM : " + mobileno,
        email_body: problemdesc + " - " + mobileno
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: true,
        success: function (feedback) {
            if (alertFlag == 1) {
                $("#ajax-screen-masking").hide();
                alert("Mail sent successfully!");
            }
        },
        error: function (feedback) {
            if (alertFlag == 1) {
                $("#ajax-screen-masking").hide();
            }
        }
    });
}

function SaveNewIssuePAYM(sitecode, mobileno, problemcategorytype, problemtype, subscriberid, problemdesc, solutiondesc, submitby) {
    var SIM_Type = 'PAYG';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1) {
        SIM_Type = 'PAYM';
    }
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    var url = apiServer + '/api/issuetracker/';
    jQuery.support.cors = true;
    var JSONSendData = {
        SearchType: 5,
        sitecode: sitecode,
        mobileno: mobileno,
        problemcategorytype: problemcategorytype,
        problemtype: problemtype,
        subscriberid: subscriberid,
        problemdesc: problemdesc,
        solutiondesc: solutiondesc,
        submitby: submitby,
        problemstatus: 0,
        SIM_Type: SIM_Type
    };

    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();

            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "2" });

            var stat = data.errcode;
            if (stat == 0) {
                $('.PMSuccess .modal-header').html(data.errsubject);
                $('.PMSuccess .modal-body').html(data.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            } else {
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (data) {
            $("#ajax-screen-masking").hide();
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            $('.PMFailed .modal-header').html(data.errsubject);
            $('.PMFailed .modal-body').html(data.errmsg);
            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });
}

function SaveDeadIssue(subscriberid, mobileno, submitby, sitecode) {
    var SIM_Type = 'PAYG';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1) {
        SIM_Type = 'PAYM';
    }
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    var url = apiServer + '/api/issuetracker/';
    jQuery.support.cors = true;
    var JSONSendData = {
        SearchType: 13,
        Subscriber_ID: subscriberid,
        Mobile_No: mobileno,
        Created_By: submitby,
        sitecode: sitecode,
        SIM_Type: SIM_Type
    };

    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();
            $('#itPWNewIssue').modal('hide');
            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "2" });

            var stat = data.errcode;
            if (stat == 0) {
                $('.PMSuccess .modal-header ').html(data.errsubject);
                $('.PMSuccess .modal-body').html(data.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });

            } else {
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (data) {
            $("#ajax-screen-masking").hide();
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            $('.PMFailed .modal-header').html(data.errsubject);
            $('.PMFailed .modal-body').html(data.errmsg);
            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });
}

function ITViewAllIssuesPAYM(mobileno, Subscriberid, Sitecode) {
    var SIM_Type = 'PAYG';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1) {
        SIM_Type = 'PAYM';
    }
    $("#itIssuesListContainer").html("").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });

    if ((Subscriberid != '' || mobileno != '') && Sitecode != '') {
        var url = apiServer + '/api/issuetracker/';
        jQuery.support.cors = true;
        var JSONSendData = {
            Mobileno: mobileno,
            SearchType: 1,
            Subscriberid: Subscriberid,
            Sitecode: Sitecode,
            SIM_Type: SIM_Type
        };
        $.ajax
        ({
            url: url,
            type: 'post',
            data: JSON.stringify(JSONSendData),
            dataType: 'json',
            contentType: "application/json;charset=utf-8",
            cache: false,
            success: function (data) {
                $("#itIssuesListContainer").css({ "background-image": "none" });
                $("#itIssuesListContainer").append("<button class=\"btn blue\" id=\"btnITNewIssue\" style=\"width:140px;height:35px\">New Issue</button>");
                $("#itIssuesListContainer").append("<div style=\"width:100%;margin-top:15px\"><table style=\"width:100%\" class=\"table table-bordered\" id=\"tblITIssuesList\"></table></div>");

                $('#tblITIssuesList').dataTable({
                    bDestroy: true,
                    bRetrieve: true,
                    aaData: data,
                    bPaginate: true,
                    bFilter: false,
                    bInfo: false,
                    iDisplayLength: 15,
                    aaSorting: [[0, "asc"]],
                    oLanguage: {
                        "sInfoEmpty": '',
                        "sEmptyTable": "No issues found."
                    },
                    aoColumns: [
                        { mDataProp: "ProblemID", sTitle: "ProblemID", bVisible: false },
                        { mDataProp: "SubmitDate_String", sTitle: "Submit Date", sWidth: "150px" },
                        { mDataProp: "SubmitBy", sTitle: "Submit By" },
                        { mDataProp: "TypeDesc", sTitle: "Issue Type" },
                        { mDataProp: "ProblemDesc", sTitle: "Description Issue" },
                        {
                            mDataProp: "Action", sTitle: "Action", sWidth: "130px",
                            fnRender: function (ob) {
                                var actionURL = "";
                                actionURL = "<button alt=\"" + ob.aData.ProblemID + "\" class=\"btn blue btnITDetailIssue\" style=\"width:60px\">Detail</button>";
                                actionURL += "&nbsp;&nbsp;<button alt=\"" + ob.aData.ProblemID + "\" class=\"btn blue btnITUpdateIssue\" style=\"width:60px\">Edit</button>";
                                return actionURL;
                            }
                        }
                    ]
                });
                $("#tblITIssuesList_wrapper .row-fluid:eq(0)").remove();
                $("#tblITIssuesList_wrapper .row-fluid .span6:eq(0)").addClass("span4");
                $("#tblITIssuesList_wrapper .row-fluid .span6:eq(1)").addClass("span8");
                $("#tblITIssuesList_wrapper .row-fluid .span6").removeClass("span6");
            },
            error: function (data) {
                $("#itIssuesListContainer").css({ "background-image": "none" });
                $("#itIssuesListContainer").append("<button class=\"btn blue\" id=\"btnITNewIssue\" style=\"width:140px;height:35px\">New Issue</button><br/>");
                $("#itIssuesListContainer").append("<div style=\"width:98%;padding:10px;border:1px solid #e5e5e5;background-color:#f5f5f5;margin-top:20px\">No issues found.</div>");
            }
        });
    } else {
        $("#itIssuesListContainer").css({ "background-image": "none" });
        $("#itIssuesListContainer").html("<div style=\"height:40px;font-size:14px;margin-top:15px;color:#555555\">Can\'t find Subscriber ID of this customer.</div>");
    }
}

/* ----------------------------------------------------------------
 * Function Name    : ITDetailIssue
 * Purpose          : to get detail of issue by issue/problem id
 * Added by         : Edi Suryadi
 * Create Date      : September 18th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function ITDetailIssue(ProblemId, Sitecode, Esclated_To) {

    var SIM_Type = 'PAYG';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1) {
        SIM_Type = 'PAYM';
    }
    var ajaxSpinnerTop = ($(window).outerHeight() / 2) + $(window).scrollTop();
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    var url = apiServer + '/api/issuetracker/';
    jQuery.support.cors = true;
    var JSONSendData = {
        SearchType: 2,
        ProblemId: ProblemId,
        Sitecode: Sitecode,
        SIM_Type: SIM_Type
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();

            var stat = data.errcode;
            if (stat == 0) {
                $("#itPWDIIssueID").text(convertEmptytoStrip(data.ProblemID));
                $("#itPWDIIssueType").text(convertEmptytoStrip(data.TypeName));
                $("#itPWDISubscriberID").text(convertEmptytoStrip(data.SubscriberID));
                $("#itPWDISubscriberName").text(convertEmptytoStrip(data.FirstName + " " + data.LastName));
                $("#itPWDIIssueDesc").text(convertEmptytoStrip(data.ProblemDesc));
                $("#itPWDISolutionDesc").text(convertEmptytoStrip(data.SolutionDesc));
                $("#itPWDISubmitBy").text(convertEmptytoStrip(data.SubmitBy));
                $("#itPWDISubmitDate").text(convertEmptytoStrip(data.SubmitDate_String));

                $("#itPWDIContacttype").text(convertEmptytoStrip(data.contact_type));
                $("#itPWDIStatus").text(convertEmptytoStrip(data.Comp_Status));
                $("#itPWDIEscalatedTo").text(convertEmptytoStrip(Esclated_To));
                $("#itPWDAssignedTo").text(convertEmptytoStrip(data.Assigned_To));
                

                $("#itPWDIIntreval").text(convertEmptytoStrip(data.intreval));
                //CRM Enhancement 2
                $("#itPWDIIntervalDesc").text(convertEmptytoStrip(data.interval_comment));
                $("#itPWDICategory").text(convertEmptytoStrip(data.Function));
                $("#itPWDISubCategory").text(convertEmptytoStrip(data.Complaint));
            } else {
                $("#itPWDIIssueID").text("-");
                $("#itPWDIIssueType").text("-");
                $("#itPWDISubscriberID").text("-");
                $("#itPWDISubscriberName").text("-");
                $("#itPWDIIssueDesc").text("-");
                $("#itPWDISolutionDesc").text("-");
                $("#itPWDISubmitBy").text("-");
                $("#itPWDISubmitDate").text("-");

                $("#itPWDIContacttype").text("-");
                $("#itPWDIStatus").text("-");
                $("#itPWDIEscalatedTo").text("-");
                $("#itPWDIIntreval").text("-");
                //CRM Enhancement 2
                $("#itPWDIIntervalDesc").text("-");
                $("#itPWDICategory").text("-");
                $("#itPWDISubCategory").text("-");
            }
            $('#itPWDetailIssue').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        },
        error: function (data) {
            $("#ajax-screen-masking").hide();
            $("#itPWDIIssueID").text("-");
            $("#itPWDIIssueType").text("-");
            $("#itPWDISubscriberID").text("-");
            $("#itPWDISubscriberName").text("-");
            $("#itPWDIIssueDesc").text("-");
            $("#itPWDISolutionDesc").text("-");
            $("#itPWDISubmitBy").text("-");
            $("#itPWDISubmitDate").text("-");
            $("#itPWDIIntervalDesc").text("-");
            $("#itPWDICategory").text("-");
            $("#itPWDISubCategory").text("-");
        }
    });
}

function ITDetailIssuePAYM(ProblemId, Sitecode) {
    var SIM_Type = 'PAYG';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1) {
        SIM_Type = 'PAYM';
    }
    var ajaxSpinnerTop = ($(window).outerHeight() / 2) + $(window).scrollTop();
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    var url = apiServer + '/api/issuetracker/';
    jQuery.support.cors = true;
    var JSONSendData = {
        SearchType: 2,
        ProblemId: ProblemId,
        Sitecode: Sitecode,
        SIM_Type: SIM_Type
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();

            var stat = data.errcode;
            if (stat == 0) {
                $("#itPWDIIssueID").text(convertEmptytoStrip(data.ProblemID));
                $("#itPWDIIssueType").text(convertEmptytoStrip(data.TypeDesc));
                $("#itPWDISubscriberID").text(convertEmptytoStrip(data.SubscriberID));
                $("#itPWDISubscriberName").text(convertEmptytoStrip(data.FirstName + " " + data.LastName));
                $("#itPWDIIssueDesc").text(convertEmptytoStrip(data.ProblemDesc));
                $("#itPWDISolutionDesc").text(convertEmptytoStrip(data.SolutionDesc));
                $("#itPWDISubmitBy").text(convertEmptytoStrip(data.SubmitBy));
                $("#itPWDISubmitDate").text(convertEmptytoStrip(data.SubmitDate_String));
            } else {
                $("#itPWDIIssueID").text("-");
                $("#itPWDIIssueType").text("-");
                $("#itPWDISubscriberID").text("-");
                $("#itPWDISubscriberName").text("-");
                $("#itPWDIIssueDesc").text("-");
                $("#itPWDISolutionDesc").text("-");
                $("#itPWDISubmitBy").text("-");
                $("#itPWDISubmitDate").text("-");
            }
            $('#itPWDetailIssue').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        },
        error: function (data) {
            $("#ajax-screen-masking").hide();
            $("#itPWDIIssueID").text("-");
            $("#itPWDIIssueType").text("-");
            $("#itPWDISubscriberID").text("-");
            $("#itPWDISubscriberName").text("-");
            $("#itPWDIIssueDesc").text("-");
            $("#itPWDISolutionDesc").text("-");
            $("#itPWDISubmitBy").text("-");
            $("#itPWDISubmitDate").text("-");
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : ITUpdateIssueGet
 * Purpose          : to get detail of issue by id for update
 * Added by         : Edi Suryadi
 * Create Date      : September 18th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function ITUpdateIssueGet(ProblemId, Sitecode, issue_id, Function_Id, Complaint_Id, Esclated_To) {
    var SIM_Type = 'PAYG';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1) {
        SIM_Type = 'PAYM';
    }
    var ajaxSpinnerTop = ($(window).outerHeight() / 2) + $(window).scrollTop();
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    var url = apiServer + '/api/issuetracker/';
    jQuery.support.cors = true;
    var JSONSendData = {
        SearchType: 2,
        ProblemId: ProblemId,
        Sitecode: Sitecode,
        SIM_Type: SIM_Type
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();

            var stat = data.errcode;
            if (stat == 0) {
                $("#itPWUIIssueID").text(convertEmptytoStrip(data.ProblemID));
                $("input[name=IssueID]").val(convertEmptytoStrip(data.ProblemID));
                $("#itPWUISelectIssueType1").val(data.CategoryType);
                var categorytypename = $("select[name=itPWUISelectIssueType1] option:selected").text();
                var jiraticket = data.JIRA_Tickets;
                $("#HiJiraticket").val(jiraticket);

                //Contact type
                Getcontacttype(ProblemId, Sitecode)

                Getcompliantype(ProblemId, Sitecode)

                Getintervaltype(ProblemId, Sitecode)

                ITGetDetailProblem1(false, issue_id, Sitecode);

                ITGetProblemCategory1(false, Sitecode, issue_id, Function_Id, Complaint_Id);

                FillEscalationTeamUI(Esclated_To);

                $("#itPWUISubscriberID").text(convertEmptytoStrip(data.SubscriberID));
                $("#itPWUISubscriberName").text(convertEmptytoStrip(data.FirstName + " " + data.LastName));
                $("textarea[name=itPWUITAIssueDesc]").text(convertEmptytoStrip(data.ProblemDesc));
                $("textarea[name=itPWUITASolutionDesc]").text(convertEmptytoStrip(data.SolutionDesc));
                $("#itPWUISubmitBy").text(convertEmptytoStrip(data.SubmitBy));
                $("#itPWUISubmitDate").text(convertEmptytoStrip(data.SubmitDate_String));
                //CRM Enhancement 2
                $("textarea[name=itPWUITAIntervalDesc]").text(convertEmptytoStrip(data.interval_comment));

                GetAssignedTo(ProblemId, Sitecode)

                GetPriority(ProblemId, Sitecode)

            } else {
                $("#itPWUIIssueID").text("-");
                $("#itPWUISelectIssueType1").val("1");
                var categorytypename = $("select[name=itPWUISelectIssueType1] option:selected").text();
                ITGetDetailProblem(false, "1", categorytypename, Sitecode, "");
                $("#itPWUISubscriberID").text("-");
                $("#itPWUISubscriberName").text("-");
                $("#itPWUIIssueDesc").text("-");
                $("#itPWUISolutionDesc").text("-");
                $("#itPWUISubmitBy").text("-");
                $("#itPWUISubmitDate").text("-");
                //CRM Enhancement 2
                $("#itPWUITAIntervalDesc").text("-");
            }
            $('#itPWUpdateIssue').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        },
        error: function (data) {
            $("#itPWUIIssueID").text("-");
            $("#itPWUISelectIssueType1").val("1");
            var categorytypename = $("select[name=itPWUISelectIssueType1] option:selected").text();
            ITGetDetailProblem(false, "1", categorytypename, Sitecode, "");
            $("#itPWUISubscriberID").text("-");
            $("#itPWUISubscriberName").text("-");
            $("#itPWUIIssueDesc").text("-");
            $("#itPWUISolutionDesc").text("-");
            $("#itPWUISubmitBy").text("-");
            $("#itPWUISubmitDate").text("-");
        }
    });
}

function ITUpdateIssueGetPAYM(ProblemId, Sitecode) {
    var SIM_Type = 'PAYG';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1) {
        SIM_Type = 'PAYM';
    }
    var ajaxSpinnerTop = ($(window).outerHeight() / 2) + $(window).scrollTop();
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    var url = apiServer + '/api/issuetracker/';
    jQuery.support.cors = true;
    var JSONSendData = {
        SearchType: 2,
        ProblemId: ProblemId,
        Sitecode: Sitecode,
        SIM_Type: SIM_Type
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();

            var stat = data.errcode;
            if (stat == 0) {
                $("#itPWUIIssueID").text(convertEmptytoStrip(data.ProblemID));
                $("input[name=IssueID]").val(convertEmptytoStrip(data.ProblemID));
                $("#itPWUISelectIssueType1").val(data.CategoryType);
                var categorytypename = $("select[name=itPWUISelectIssueType1] option:selected").text();
                ITGetDetailProblem(false, data.CategoryType, categorytypename, Sitecode, data.ProblemType);
                $("#itPWUISubscriberID").text(convertEmptytoStrip(data.SubscriberID));
                $("#itPWUISubscriberName").text(convertEmptytoStrip(data.FirstName + " " + data.LastName));
                $("textarea[name=itPWUITAIssueDesc]").text(convertEmptytoStrip(data.ProblemDesc));
                $("textarea[name=itPWUITASolutionDesc]").text(convertEmptytoStrip(data.SolutionDesc));
                $("#itPWUISubmitBy").text(convertEmptytoStrip(data.SubmitBy));
                $("#itPWUISubmitDate").text(convertEmptytoStrip(data.SubmitDate_String));

            } else {
                $("#itPWUIIssueID").text("-");
                $("#itPWUISelectIssueType1").val("1");
                var categorytypename = $("select[name=itPWUISelectIssueType1] option:selected").text();
                ITGetDetailProblem(false, "1", categorytypename, Sitecode, "");
                $("#itPWUISubscriberID").text("-");
                $("#itPWUISubscriberName").text("-");
                $("#itPWUIIssueDesc").text("-");
                $("#itPWUISolutionDesc").text("-");
                $("#itPWUISubmitBy").text("-");
                $("#itPWUISubmitDate").text("-");
            }
            $('#itPWUpdateIssue').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        },
        error: function (data) {
            $("#itPWUIIssueID").text("-");
            $("#itPWUISelectIssueType1").val("1");
            var categorytypename = $("select[name=itPWUISelectIssueType1] option:selected").text();
            ITGetDetailProblem(false, "1", categorytypename, Sitecode, "");
            $("#itPWUISubscriberID").text("-");
            $("#itPWUISubscriberName").text("-");
            $("#itPWUIIssueDesc").text("-");
            $("#itPWUISolutionDesc").text("-");
            $("#itPWUISubmitBy").text("-");
            $("#itPWUISubmitDate").text("-");
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : ITUpdateIssueSave
 * Purpose          : to update existing issue
 * Added by         : Edi Suryadi
 * Create Date      : September 19th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
//CRM Enhancement 2
function ITUpdateIssueSave(sitecode, mobileno, problemcategorytype, problemtype, problemtype1, subscriberid, problemdesc, solutiondesc, submitby, ProblemID, contact_type, Comp_Status, intreval, JIRA_Tickets, mode, Esclated_To, interval_comment,
    Assigned_To, AssignPriority) {
    var SIM_Type = 'PAYG';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1) {
        SIM_Type = 'PAYM';
    }
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    var url = apiServer + '/api/issuetracker/';
    jQuery.support.cors = true;
    var JSONSendData = {
        SearchType: 6,
        sitecode: sitecode,
        Mobile_No: mobileno,
        Issue_Id: problemcategorytype,
        Function_Id: problemtype,
        Complaint_Id: problemtype1,
        Subscriber_ID: subscriberid,
        Issue_Desc: problemdesc,
        Solution_Desc: solutiondesc,
        Created_By: submitby,
        problemstatus: 0,
        ProblemID: ProblemID,
        contact_type: contact_type,
        Comp_Status: Comp_Status,
        intreval: intreval,
        JIRA_Tickets: JIRA_Tickets,
        mode: mode,
        SIM_Type: SIM_Type,
        Esclated_To: Esclated_To,
        interval_comment: interval_comment, //CRM Enhancement 2,
        Assigned_To:Assigned_To, 
        AssignPriority: AssignPriority
    };

    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();
            $('#itPWUpdateIssue').modal('hide');
            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "2" });

            var stat = data.errcode;
            if (stat == 0) {
                $('.PMSuccess .modal-header').html(data.errsubject);
                $('.PMSuccess .modal-body').html(data.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            } else {
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (data) {
            $("#ajax-screen-masking").hide();
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            $('.PMFailed .modal-header').html(data.errsubject);
            $('.PMFailed .modal-body').html(data.errmsg);
            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });
}

function ITUpdateIssueSavePAYM(sitecode, problemid, problemcategorytype, problemtype, problemdesc, solutiondesc) {
    var SIM_Type = 'PAYG';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1) {
        SIM_Type = 'PAYM';
    }
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    var url = apiServer + '/api/issuetracker/';
    jQuery.support.cors = true;
    var JSONSendData = {
        SearchType: 6,
        sitecode: sitecode,
        problemid: problemid,
        problemcategorytype: problemcategorytype,
        problemtype: problemtype,
        problemdesc: problemdesc,
        solutiondesc: solutiondesc,
        problemstatus: 0,
        SIM_Type: SIM_Type
    };

    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();

            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "2" });

            var stat = data.errcode;
            if (stat == 0) {
                $('.PMSuccess .modal-header').html(data.errsubject);
                $('.PMSuccess .modal-body').html(data.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            } else {
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (data) {
            $("#ajax-screen-masking").hide();
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            $('.PMFailed .modal-header').html(data.errsubject);
            $('.PMFailed .modal-body').html(data.errmsg);
            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : ITGetProblemCategory
 * Purpose          : to get category of problem for issue type combo box
 * Added by         : Edi Suryadi
 * Create Date      : September 18th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function ITGetProblemCategory1(isNewIssue, Sitecode, Issue_Id, Function_Id, Complaint_Id) {
    var SIM_Type = 'PAYG';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1) {
        SIM_Type = 'PAYM';
    }
    var url = apiServer + '/api/issuetracker/';
    jQuery.support.cors = true;
    var JSONSendData = {
        SearchType: 8,
        Sitecode: Sitecode,
        SIM_Type: SIM_Type
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (data) {
            var IssueType2Option = "";
            $.each(data, function (idx) {
                var selected = "";

                var selectedOptionValue = "";
                if (Issue_Id == "" || Issue_Id == "null") {
                    if (idx == 0) {
                        Issue_Id = data[idx].Issue_Id;
                        selected = "selected=\"selected\"";
                    }
                } else {
                    if (data[idx].Issue_Id == Issue_Id) {

                        selected = "selected=\"selected\"";
                    }
                }
                IssueType2Option += "<option value=\"" + data[idx].Issue_Id + "\" " + selected + ">" + data[idx].Issue + "</option>";
            });
            ITGetDetailProblem1(isNewIssue, Sitecode, Issue_Id, Function_Id, Complaint_Id);
            if (isNewIssue == true) {
                $('#itPWNISelectIssueType3').empty()
                $("select[name=itPWNISelectIssueType3]").append(IssueType2Option).show();
                $("img#itPWNISelectIssueType2Loader").hide();
            } else {
                $('#itPWUISelectIssueType3').empty();
                $("select[name=itPWUISelectIssueType3]").append(IssueType2Option).show();
                $("img#itPWUISelectIssueType2Loader").hide();
            }

        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : ITGetProblemCategory
 * Purpose          : to get category of problem for issue type combo box
 * Added by         : Edi Suryadi
 * Create Date      : September 18th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function ITGetProblemCategory(Sitecode) {
    var SIM_Type = 'PAYG';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1) {
        SIM_Type = 'PAYM';
    }
    var url = apiServer + '/api/issuetracker/';
    jQuery.support.cors = true;
    var JSONSendData = {
        SearchType: 3,
        Sitecode: Sitecode,
        SIM_Type: SIM_Type
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (data) {
            $.each(data, function (idx) {
                var selected = "";
                if (idx == 0) {
                    selected = "selected=\"selected\"";
                }
                $("select[name=itPWNISelectIssueType1]").append("<option value=\"" + data[idx].problemcategorytype + "\" " + selected + ">" + data[idx].categorytypename + "</option>");
                $("select[name=itPWUISelectIssueType1]").append("<option value=\"" + data[idx].problemcategorytype + "\" " + selected + ">" + data[idx].categorytypename + "</option>");
            });
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : ITGetDetailProblem
 * Purpose          : to get detail of problem
 * Added by         : Edi Suryadi
 * Create Date      : September 18th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function ITGetDetailProblem(isNewIssue, problemcategorytype, categorytypename, Sitecode, selectedOptionValue) {

    var url = apiServer + '/api/issuetracker/';
    jQuery.support.cors = true;
    var JSONSendData = {
        SearchType: 4,
        problemcategorytype: problemcategorytype,
        categorytypename: categorytypename,
        Sitecode: Sitecode
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            if (isNewIssue == true) {
                $("select[name=itPWNISelectIssueType2]").html("").hide();
                $("img#itPWNISelectIssueType2Loader").show();
            } else {
                $("select[name=itPWUISelectIssueType2]").html("").hide();
                $("img#itPWUISelectIssueType2Loader").show();
            }
        },
        success: function (data) {
            var IssueType2Option = "";
            $.each(data, function (idx) {
                var selected = "";
                if (selectedOptionValue == "") {
                    if (idx == 0) {
                        selected = "selected=\"selected\"";
                    }
                } else {
                    if (data[idx].problemtype == selectedOptionValue) {
                        selected = "selected=\"selected\"";
                    }
                }
                IssueType2Option += "<option value=\"" + data[idx].problemtype + "\" " + selected + ">" + data[idx].typename + "</option>";
            });

            if (isNewIssue == true) {
                $("select[name=itPWNISelectIssueType2]").append(IssueType2Option).show();
                $("img#itPWNISelectIssueType2Loader").hide();
            } else {
                $("select[name=itPWUISelectIssueType2]").append(IssueType2Option).show();
                $("img#itPWUISelectIssueType2Loader").hide();
            }
        }
    });
}

function ITGetDetailProblem1(isNewIssue, Sitecode, Issue_Id, Function_Id, Complaint_Id) {
    var SIM_Type = 'PAYG';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1) {
        SIM_Type = 'PAYM';
    }
    var url = apiServer + '/api/issuetracker/';
    jQuery.support.cors = true;
    var JSONSendData = {
        SearchType: 9,
        Issue_Id: Issue_Id,
        Sitecode: Sitecode,
        SIM_Type: SIM_Type
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            if (isNewIssue == true) {
                $("select[name=itPWNISelectIssueType2]").html("").hide();
                $("img#itPWNISelectIssueType2Loader").show();
            } else {
                $("select[name=itPWUISelectIssueType2]").html("").hide();
                $("img#itPWUISelectIssueType2Loader").show();
            }
        },
        success: function (data) {

            var IssueType2Option = "";
            // var Function_Id = "";
            $.each(data, function (idx) {
                var selected = "";

                if (Function_Id == "" || Function_Id == "null") {
                    if (idx == 0) {
                        selected = "selected=\"selected\"";
                        Function_Id = data[idx].Function_Id;
                        // selected = "selected=\"selected\"";
                        //   alert(selected);
                    }
                } else {
                    if (data[idx].Function_Id == Function_Id) {
                        //Function_Id = data[idx].Function_Id;
                        selected = "selected=\"selected\"";
                    }
                }
                IssueType2Option += "<option value=\"" + data[idx].Function_Id + "\" " + selected + ">" + data[idx].Function + "</option>";
            });
            ITGetDetailProblem2(isNewIssue, Function_Id, Sitecode, Complaint_Id);
            if (isNewIssue == true) {
                $("select[name=itPWNISelectIssueType2]").append(IssueType2Option).show();
                $("img#itPWNISelectIssueType2Loader").hide();
            } else {
                $("select[name=itPWUISelectIssueType2]").append(IssueType2Option).show();
                $("img#itPWUISelectIssueType2Loader").hide();
            }
        }

    });
}

function ITGetDetailProblem2(isNewIssue, Function_Id, Sitecode, Complaint_Id) {
    var SIM_Type = 'PAYG';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1) {
        SIM_Type = 'PAYM';
    }
    var url = apiServer + '/api/issuetracker/';
    jQuery.support.cors = true;
    var JSONSendData = {
        SearchType: 11,
        Function_Id: Function_Id,
        Sitecode: Sitecode,
        SIM_Type: SIM_Type
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            if (isNewIssue == true) {
                $("select[name=itPWNISelectIssueType1]").html("").hide();

                $("img#itPWNISelectIssueType2Loader").show();
            } else {
                $("select[name=itPWUISelectIssueType1]").html("").hide();
                $("img#itPWUISelectIssueType2Loader").show();
            }
        },
        success: function (data) {
            var IssueType2Option = "";
            $.each(data, function (idx) {
                var selected = "";
                // var Function_Id = "";
                if (Complaint_Id == "") {
                    if (idx == 0) {
                        selected = "selected=\"selected\"";
                        selected = "selected=\"selected\"";
                        // alert(selected);
                    }
                } else {
                    if (data[idx].Complaint_Id == Complaint_Id) {
                        selected = "selected=\"selected\"";
                    }
                }
                IssueType2Option += "<option value=\"" + data[idx].Complaint_Id + "\" " + selected + ">" + data[idx].Complaint + "</option>";
            });

            if (isNewIssue == true) {
                $("select[name=itPWNISelectIssueType1]").append(IssueType2Option).show();
                $("img#itPWNISelectIssueType2Loader").hide();
            } else {
                $("select[name=itPWUISelectIssueType1]").append(IssueType2Option).show();
                $("img#itPWUISelectIssueType2Loader").hide();
            }

        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : ITClearAllFields
 * Purpose          : to clear all texts in new issue form
 * Added by         : Edi Suryadi
 * Create Date      : September 18th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function ITClearAllFields() {
    //$("select[name=itPWNISelectIssueType3]").val("1");
    //$("select[name=itPWNISelectIssueType2]").val("1");
    //$("select[name=itPWNISelectIssueType1]").val("1");

    $("select[name=itPWNIStatus]").val("-1");
    //$("select[name=itPWNIcontacttype]").val("-1");

    //$("#JIRATickets").attr("disabled", true).val();
    //$("#JIRATickets").val('');
    $("#itPWNIEscalatingTeam").attr("disabled", true);
    $("select[name=itPWNIInterval]").val("-1");
    $("#itPWNIInterval").attr("disabled", true);
    $("#itPWNIEscalatingTeam").val('-1');

    $("textarea[name=itPWNITAIssueDesc]").val("");
    //$("textarea[name=itPWNITASolutionDesc]").val("");

    $("#errmsgdescription").hide();
    $("#errmsgStatus").hide();
    $("#errmsgescalated").hide();
    $("#errmsginterval").hide();
}
function ITClearAllFieldsPAYM() {
    $("select[name=itPWNISelectIssueType1]").val("1");
    //13-Jan-2016 : Moorthy : Modified to display the sub category
    $('#itPWNISelectIssueType1').trigger('change');
    //$("select[name=itPWNISelectIssueType2]").val("11");
    $("textarea[name=itPWNITAIssueDesc]").val("");
    $("textarea[name=itPWNITASolutionDesc]").val("");
}

/* ----------------------------------------------------------------
 * Function Name    : convertEmptytoStrip
 * Purpose          : to convert empty string to -
 * Added by         : Edi Suryadi
 * Create Date      : September 18th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function convertEmptytoStrip(str) {
    if (str == '') {
        return '-';
    } else {
        return str;
    }
}

/* ----------------------------------------------------------------
 * Function Name    : Validation
 * Purpose          : Validation 
 * Added by         : 
 * Create Date      : May 07th, 2015
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function validateproduct() {
    var isvalid = true;
    if ($('#itPWNIcontacttype').val() == "-1") {
        isvalid = false;
        $('#errmsgcontacttype').show();
    }
    else {
        $('#errmsgcontacttype').hide();
    }

    if ($('#itPWNISelectIssueType3').val() == "-1") {
        isvalid = false;
        $('#errmsgcalltype').show();
    }
    else {
        $('#errmsgcalltype').hide();
    }

    if ($('#itPWNISelectIssueType2').val() == "-1") {
        isvalid = false;
        $('#errmsgsubtype').show();
    }
    else {
        $('#errmsgsubtype').hide();
    }

    if ($('#itPWNISelectIssueType1').val() == "-1") {
        isvalid = false;
        $('#errmsgcategory').show();
    }
    else {
        $('#errmsgcategory').hide();
    }

    if ($('#itPWNITAIssueDesc').val().trim() == "") {
        isvalid = false;
        $('#errmsgdescription').show();
    }
    else {
        $('#errmsgdescription').hide();
    }

    $("#errmsgescalated").hide();
    $("#errmsginterval").hide();

    if ($('#itPWNIStatus').val() == "-1") {
        isvalid = false;
        $('#errmsgStatus').show();
    }
    else {
        $('#errmsgStatus').hide();
        if ($('#itPWNIStatus').val() == "1") {
            if ($('#itPWNIEscalatingTeam').val() == "-1") {
                isvalid = false;
                $("#errmsgescalated").show();
            }
            else
                $("#errmsgescalated").hide();
        }
        else if ($('#itPWNIStatus').val() == "2") {
            if ($('#itPWNIInterval').val() == "-1") {
                isvalid = false;
                $("#errmsginterval").show();
            }
            else
                $("#errmsginterval").hide();
        }
    }
    if ($('#itPWNIPriority').val() == "-1") {
        isvalid = false;
        $('#errmsgPWNIPriority').show();
    }
    else {
        $('#errmsgPWNIPriority').hide();
    }

    return isvalid;
}

function validateproduct1() {
    var isvalid = true;
    if ($('#itPWUIcontacttype').val() == "-1") {
        isvalid = false;
        $('#errmsgUcontacttype').show();
    }
    else {
        $('#errmsgUcontacttype').hide();
    }

    if ($('#itPWUIStatus').val() == "-1") {
        isvalid = false;
        $('#errmsgUStatus').show();
    }
    else {
        $('#errmsgUStatus').hide();
        if ($('#itPWUIStatus').val() == "1") {
            if ($('#itPWUIEscalatingTeam').val() == "-1") {
                isvalid = false;
                $("#errmsgUescalated").show();
            }
            else
                $("#errmsgUescalated").hide();
        }
        else if ($('#itPWUIStatus').val() == "2") {
            if ($('#itPWUIInterval').val() == "-1") {
                isvalid = false;
                $("#errmsgUinterval").show();
            }
            else
                $("#errmsgUinterval").hide();
        }
    }
    return isvalid;
}

//GetAssignedTo(ProblemId, Sitecode)
function GetAssignedTo(ProblemID, Sitecode) {
    var SIM_Type = 'PAYG';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1) {
        SIM_Type = 'PAYM';
    }
    var url = apiServer + '/api/issuetracker/';
    jQuery.support.cors = true;
    var JSONSendData = {
        SearchType: 18,
        ProblemID: ProblemID,
        Sitecode: Sitecode,
        SIM_Type: SIM_Type
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (data) {
            $('#itPWUIssignedTo').empty();
            var strContact = "";
            var IssueType2Option2 = "";
            $.each(data, function (idx) {
                if (data.length > 0) {
                    IssueType2Option2 += "<option value=\"" + data[idx].Email + "\" Text=\"" + data[idx].id + "\"  >" + data[idx].Username + "</option>";
                    
                }
                else {
                    FillAssignedTo();
                }
                //   var selected = "";            
                //IssueType2Option2 += "<option Text=\"" + data[idx].contact_type + ">" + data[idx].contact_type + "</option>";
                

                
            });
            if (data.length == 0) {
                FillAssignedTo();
            }
            else {
                $("select[name=itPWUIssignedTo]").append(IssueType2Option2);
            }
           
        }
    });
}
//GetPriority(ProblemId, Sitecode)
function GetPriority(ProblemID, Sitecode) {
    var SIM_Type = 'PAYG';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1) {
        SIM_Type = 'PAYM';
    }
    var url = apiServer + '/api/issuetracker/';
    jQuery.support.cors = true;
    var JSONSendData = {
        SearchType: 17,
        ProblemID: ProblemID,
        Sitecode: Sitecode,
        SIM_Type: SIM_Type
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (data) {
            $('#itUPriority').empty();
            var strContact = "";
            var IssueType2Option2 = "";
            $.each(data, function (idx) {
                //   var selected = "";            
                //IssueType2Option2 += "<option Text=\"" + data[idx].contact_type + ">" + data[idx].contact_type + "</option>";
                IssueType2Option2 += "<option value=\"" + data[idx].fk_priority_id + "\" Text=\"" + data[idx].priority + "\"  >" + data[idx].priority + "</option>";
            });
            $("select[name=itUPriority]").append(IssueType2Option2);
        }
    });
}

//Contact type
function Getcontacttype(ProblemID, Sitecode) {
    var SIM_Type = 'PAYG';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1) {
        SIM_Type = 'PAYM';
    }
    var url = apiServer + '/api/issuetracker/';
    jQuery.support.cors = true;
    var JSONSendData = {
        SearchType: 14,
        ProblemID: ProblemID,
        Sitecode: Sitecode,
        SIM_Type: SIM_Type
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (data) {
            $('#itPWUIcontacttype').empty();
            var strContact = "";
            var IssueType2Option2 = "";
            $.each(data, function (idx) {
                //   var selected = "";            
                //IssueType2Option2 += "<option Text=\"" + data[idx].contact_type + ">" + data[idx].contact_type + "</option>";
                IssueType2Option2 += "<option value=\"" + data[idx].status + "\" Text=\"" + data[idx].contact_type + "\"  >" + data[idx].contact_type + "</option>";
            });
            $("select[name=itPWUIcontacttype]").append(IssueType2Option2);
        }
    });
}
function Getcompliantype(ProblemID, Sitecode) {
    var StatusList = new Array("Resolved", "Escalate to Team", "Call Back Arranged ", "Ongoing Issue");
    var Status = "";
    $("select[name=itPWUIStatus]").html("");
    $.each(StatusList, function (key, value) {
        Status += "<option value=\"" + key + "\" Text=\"" + value + "\">" + value + "</option>";
    });
    $("select[name=itPWUIStatus]").append(Status);
    //var SIM_Type = 'PAYG';
    //if (window.location.href.toLowerCase().indexOf('/paym/') > -1) {
    //    SIM_Type = 'PAYM';
    //}
    //var url = apiServer + '/api/issuetracker/';
    //jQuery.support.cors = true;
    //var JSONSendData = {
    //    SearchType: 15,
    //    ProblemID: ProblemID,
    //    Sitecode: Sitecode,
    //    SIM_Type: SIM_Type
    //};

    //$.ajax
    //({
    //    url: url,
    //    type: 'post',
    //    data: JSON.stringify(JSONSendData),
    //    dataType: 'json',
    //    contentType: "application/json;charset=utf-8",
    //    cache: false,
    //    success: function (data) {
    //        $('#itPWUIStatus').empty();
    //        var strContact = "";

    //        var IssueType2Option2 = "";
    //        $.each(data, function (idx) {
    //            //   var selected = "";            
    //            //IssueType2Option2 += "<option Text=\"" + data[idx].contact_type + ">" + data[idx].contact_type + "</option>";
    //            IssueType2Option2 += "<option value=\"" + data[idx].status + "\" Text=\"" + data[idx].Complaint_Status + "\"  >" + data[idx].Complaint_Status + "</option>";
    //        });


    //        $("select[name=itPWUIStatus]").append(IssueType2Option2);

    //        if ($("#itPWUIStatus option:selected").text() == "JIRA Created") {
    //            $("#UJIRATickets").attr("disabled", false).val();
    //            var HiJiraticket = $("#HiJiraticket").val();
    //            $("#UJIRATickets").val(HiJiraticket);
    //            $("#itPWUIInterval").attr("disabled", true).val('');

    //        }
    //        else if ($("#itPWUIStatus option:selected").text() == "Call Back Arranged") {
    //            $("#itPWUIInterval").attr("disabled", false).val();
    //            $("#UJIRATickets").attr("disabled", true).val('');
    //        }
    //        else {
    //            $("#UJIRATickets").attr("disabled", true).val('');
    //            $("#itPWUIInterval").attr("disabled", true).val('');
    //        }


    //    }
    //});
}


function Getintervaltype(ProblemID, Sitecode) {
    var SIM_Type = 'PAYG';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1) {
        SIM_Type = 'PAYM';
    }
    var url = apiServer + '/api/issuetracker/';
    jQuery.support.cors = true;
    var JSONSendData = {
        SearchType: 16,
        ProblemID: ProblemID,
        Sitecode: Sitecode,
        SIM_Type: SIM_Type
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (data) {
            $('#itPWUIInterval').empty();
            var strContact = "";

            var IssueType2Option2 = "";
            $.each(data, function (idx) {
                //   var selected = "";            
                //IssueType2Option2 += "<option Text=\"" + data[idx].contact_type + ">" + data[idx].contact_type + "</option>";
                IssueType2Option2 += "<option value=\"" + data[idx].status + "\" Text=\"" + data[idx].time_interval + "\" >" + data[idx].time_interval + "</option>";
            });


            $("select[name=itPWUIInterval]").append(IssueType2Option2);



        }
    });
}

//Fill Escalation Team New Issue
function FillEscalationTeam() {
    $('#itPWNIEscalatingTeam').find('option').remove();
    var url = apiServer + '/api/EmailGenerateTicket';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 7
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: true,
        success: function (feedback) {
            var feedbackarr = jQuery.makeArray(JSON.stringify(feedback).split('}'));
            var icount;
            var actionOption = '<option value="-1" style=\"color:#c0c0c0\">--- Select ---</option>';
            for (icount = 0; icount < feedbackarr.length - 1; icount++) {
                actionOption += '<option ctlValue="' + feedback[icount].Email + '"   value="' + feedback[icount].Id + '" style=\"padding:3px\">' + feedback[icount].Username + '</option>';
            }
            $('#itPWNIEscalatingTeam').append(actionOption);
        },
        error: function (feedback) {
        }
    });
}
function FillAssignedTo() {
    $('#itPWUIssignedTo').find('option').remove();
    var url = apiServer + '/api/EmailGenerateTicket';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 7
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: true,
        success: function (feedback) {
            var feedbackarr = jQuery.makeArray(JSON.stringify(feedback).split('}'));
            var icount;
            var actionOption = '<option value="-1" style=\"color:#c0c0c0\">--- Select ---</option>';
            for (icount = 0; icount < feedbackarr.length - 1; icount++) {
                actionOption += '<option ctlValue="' + feedback[icount].Email + '"   value="' + feedback[icount].Id + '" style=\"padding:3px\">' + feedback[icount].Username + '</option>';
            }
            $('#itPWUIssignedTo').append(actionOption);
        },
        error: function (feedback) {
        }
    });
}

function FillAssignedToUpdate() {
    $('#itPWUIssignedTo').find('option').remove();
    var url = apiServer + '/api/EmailGenerateTicket';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 7
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: true,
        success: function (feedback) {
            var feedbackarr = jQuery.makeArray(JSON.stringify(feedback).split('}'));
            var icount;
            var actionOption = '<option value="-1" style=\"color:#c0c0c0\">--- Select ---</option>';
            for (icount = 0; icount < feedbackarr.length - 1; icount++) {
                actionOption += '<option ctlValue="' + feedback[icount].Email + '"   value="' + feedback[icount].Id + '" style=\"padding:3px\">' + feedback[icount].Username + '</option>';
            }
            $('#itPWUIssignedTo').append(actionOption);
        },
        error: function (feedback) {
        }
    });
}

//function FillPriority(sitecode) {
//    $('#itPriority').find('option').remove();
//    var url = apiServer + '/api/Priority';
//    jQuery.support.cors = true;
//    var JSONSendData = {
//        sitecode: sitecode
//    };
//    $.ajax
//    ({
//        url: url,
//        type: 'Post',
//        data: JSON.stringify(JSONSendData),
//        dataType: 'json',
//        contentType: "application/json;charset=utf-8",
//        cache: true,
//        success: function (feedback) {
//            var feedbackarr = jQuery.makeArray(JSON.stringify(feedback).split('}'));
//            var icount;
//            var actionOption = '<option value="-1" style=\"color:#c0c0c0\">--- Select ---</option>';
//            for (icount = 0; icount < feedbackarr.length - 1; icount++) {
//                actionOption += '<option ctlValue="' + feedback[icount].priority_id + '"   value="' + feedback[icount].priority + '" style=\"padding:3px\">' + feedback[icount].priority + '</option>';
//            }
//            $('#itPriority').append(actionOption);
//        },
//        error: function (feedback) {
//        }
//    });
//}
function FillPriority(sitecode) {
    var icount;
    var actionOption = '<option value="-1" style=\"color:#c0c0c0\">--- Select ---</option>';
    // for (icount = 0; icount < feedbackarr.length - 1; icount++) {
    actionOption += '<option ctlValue="' + 1 + '"   value="\' + Critical - 8 hours + \' style=\"padding:3px\">Critical - 8 hours</option>';
    actionOption += '<option ctlValue="' + 2 + '"   value="\' + High - 24 hours + \' style=\"padding:3px\">High - 24 hours</option>';
    actionOption += '<option ctlValue="' + 3 + '"   value="\' + Medium - 32 hours + \' style=\"padding:3px\">Medium - 32 hours</option>';
    actionOption += '<option ctlValue="' + 4 + '"   value="\' + Low - 38 hours + \' style=\"padding:3px\">Low - 38 hours</option>';
    $('#itPriority').append(actionOption);
}

function FillPriorityUpdate(sitecode) {
    var icount;
    var actionOption = '<option value="-1" style=\"color:#c0c0c0\">--- Select ---</option>';
    // for (icount = 0; icount < feedbackarr.length - 1; icount++) {
    actionOption += '<option ctlValue="' + 1 + '"   value="\' + Critical - 8 hours + \' style=\"padding:3px\">Critical - 8 hours</option>';
    actionOption += '<option ctlValue="' + 2 + '"   value="\' + High - 24 hours + \' style=\"padding:3px\">High - 24 hours</option>';
    actionOption += '<option ctlValue="' + 3 + '"   value="\' + Medium - 32 hours + \' style=\"padding:3px\">Medium - 32 hours</option>';
    actionOption += '<option ctlValue="' + 4 + '"   value="\' + Low - 38 hours + \' style=\"padding:3px\">Low - 38 hours</option>';
    $('#itUPriority').append(actionOption);
}


//Fill Escalation Team Update Issue
function FillEscalationTeamUI(Esclated_To) {
    $('#itPWUIEscalatingTeam').find('option').remove();
    var url = apiServer + '/api/EmailGenerateTicket';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 7
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: true,
        success: function (feedback) {
            var feedbackarr = jQuery.makeArray(JSON.stringify(feedback).split('}'));
            var icount;
            var actionOption = '<option value="-1" style=\"color:#c0c0c0\">--- Select ---</option>';
            for (icount = 0; icount < feedbackarr.length - 1; icount++) {
                if (feedback[icount].Username == Esclated_To)
                    actionOption += '<option ctlValue="' + feedback[icount].Email + '"   value="' + feedback[icount].Id + '" style=\"padding:3px\" selected="\selected"\>' + feedback[icount].Username + '</option>';
                else
                    actionOption += '<option ctlValue="' + feedback[icount].Email + '"   value="' + feedback[icount].Id + '" style=\"padding:3px\">' + feedback[icount].Username + '</option>';
            }
            $('#itPWUIEscalatingTeam').append(actionOption);
        },
        error: function (feedback) {
        }
    });
}
/* ----------------------------------------------------- 
   *  eof TAB ISSUES TRACKING 
   ===================================================== */


