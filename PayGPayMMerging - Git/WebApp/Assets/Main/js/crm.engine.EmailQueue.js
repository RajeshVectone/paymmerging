﻿function GetEmailQueue() {
    $("#pbEmailQueueBody").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblPBEmailQueue\"></table>");
    $("#pbEmailQueueBody").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var url = apiServer + '/api/Emails';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 1
    };
    var row = 0;
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#pbEmailQueueBody").css({ "background-image": "none" });
            $('#tblPBEmailQueue').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                iDisplayLength: 10,
                bPaginate: true,
                //bLengthChange: false,
                bFilter: true,
                bInfo: false,
                bAutoWidth:false,
                aaSorting: [[3, "asc"]],
                aoColumns: [
                    { mDataProp: "request_id", sTitle: "ID", sWidth: 50 },
                    { mDataProp: "email_from", sTitle: "Email ID", sWidth: 100 },
                    { mDataProp: "Assiged_agent", sTitle: "Assigned To", sWidth: 100 },
                    { mDataProp: "status", sTitle: "Status", sWidth: 75 },
                    { mDataProp: "pk_queue_id", sTitle: "Queue ID", sWidth: 75 },
                    { mDataProp: "Ticket_id", sTitle: "Ticket ID", sWidth: 150 },
                    { mDataProp: "no_of_attachaments", sTitle: "Att.", sWidth: 50 },
                    { mDataProp: "createDate_string", sTitle: "Date & Time", sWidth: 150 },
                    {
                        mDataProp: "email_subject_trim", sTitle: "Subject", sWidth: 250
                        //mDataProp: "email_subject_trim", sTitle: "Subject", sWidth: 250,
                        //fnRender: function (ob) {
                        //    var actionURL = "";
                        //    actionURL = "<p data-toggle='tooltip' title='" + ob.aData.email_subject + "'>" + ob.aData.email_subject_trim + "</p>";
                        //    return actionURL;
                        //}
                    },
                    {
                        mDataProp: "email_body_trim", sTitle: "Body", sWidth: 250
                        //mDataProp: "email_body_trim", sTitle: "Body", sWidth: 500,
                        //fnRender: function (ob) {
                        //    var actionURL = "";
                        //    //actionURL = "<p data-toggle='tooltip' title='" + ob.aData.email_body + "'>" + ob.aData.email_body_trim + "</p>";
                        //    actionURL = "<p data-toggle='tooltip'>" + ob.aData.email_body_trim + "</p>";
                        //    return actionURL;
                        //}
                    },
                    {
                        mDataProp: "email_body", sTitle: "View", sWidth: 75,
                        fnRender: function (ob) {
                            var actionURL = "";
                            //actionURL = '<img src="/Assets/Main/img/Info.png" style="cursor: pointer"title="View" class="infoEmail" id="infoEmail" ctlvalue="' + ob.aData.request_id + '"/>';
                            actionURL = "<button id=\"infoEmail\" class=\"btn blue\" class=\"infoEmail\" id=\"infoEmail\" ctlvalue='" + ob.aData.request_id + "'>View</button>";
                            //<input type=\"hidden\" id=\"hdsubject_' + ob.aData.request_id + '"\" value=\"' + ob.aData.email_subject + '"\" /><input type=\"hidden\" id=\"hdbody_' + ob.aData.request_id + '"\" value=\"' + ob.aData.email_body + '"\" />';
                            //actionURL = '<button id="infoEmail" class="btn blue" ctlvalue="' + ob.aData.request_id + '">View</button><input type="hidden" id="hdsubject_' + ob.aData.request_id + '"\" value=\"' + ob.aData.email_subject + '"\" /><input type=\"hidden\" id=\"hdbody_' + ob.aData.request_id + '"\" value=\"' + ob.aData.email_body + '"\" />';
                            return actionURL;
                        }
                    },
                ],
                fnDrawCallback: function () {
                    $("#pbEmailQueueBody tbody td").css({ "font-size": "12px" });
                }
            });
        },
        error: function (feedback) {
            $("#pbEmailQueueBody").css({ "background-image": "none" }).html("").append("No Emails found").show();
        }
    });
}

//function CheckAllEmailQueue(value) {
//    var rows, checked;
//    rows = $('#tblPBEmailQueue').find('tbody tr');
//    checked = $(value).prop('checked');
//    $.each(rows, function () {
//        var checkbox = $($(this).find('td').eq(0)).find('input').prop('checked', checked);
//    });
//}

//function EmailQueueAssignToUser(request_id, assigned_user, agent_name, email_to, userid, queue_status, process_type) {
//    var url = apiServer + '/api/Emails';
//    jQuery.support.cors = true;
//    var JSONSendData = {
//        request_id_selected: request_id,
//        assigned_user: assigned_user,
//        agent_name: agent_name,
//        email_to: email_to,
//        userid: userid,
//        queue_status: queue_status,
//        process_type: process_type,
//        modetype: 2,
//    };
//    $.ajax
//    ({
//        url: url,
//        type: 'post',
//        data: JSON.stringify(JSONSendData),
//        dataType: 'json',
//        contentType: "application/json;charset=utf-8",
//        cache: true,
//        success: function (feedback) {
//            if (feedback != null) {
//                alert(feedback[0].errmsg);
//                if (feedback[0].errcode == 0)
//                    GetEmailQueue();
//            }
//        },
//        error: function (feedback) {

//        }
//    });
//}

function GetEmailQueueByID(request_id) {
    $("#pbEmailQueueBody").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    $("#divEmailAttachments").html("");
    var url = apiServer + '/api/Emails';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 1,
        request_id: request_id
    };
    var row = 0;
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#pbEmailQueueBody").css({ "background-image": "none" });
            if (feedback != null && feedback.length > 0) {
                $('#txtSubject').val(feedback[0].email_subject);
                $('#txtBody').val(feedback[0].email_body);
                if (feedback[0].lstEmailAttachments != null && feedback[0].lstEmailAttachments.length > 0) {
                    var htmlString = '<a href="{0}" target="_blank" style="cursor:pointer;margin-right: 10px;"><img src="/Assets/Main/img/attachments/{1}.png" style="padding-right: 5px;">{2}</a>';
                    for (i = 0; i < feedback[0].lstEmailAttachments.length; i++) {
                        $("#divEmailAttachments").append(htmlString.replace("{0}", feedback[0].lstEmailAttachments[i].fileurl).replace("{1}", feedback[0].lstEmailAttachments[i].extension).replace("{2}", feedback[0].lstEmailAttachments[i].filename));
                    }
                }

                $('#divEmailView').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (feedback) {
            $("#pbEmailQueueBody").css({ "background-image": "none" });
        }
    });
}