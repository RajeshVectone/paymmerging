﻿/* ===================================================== 
   *  CMK Cancel Location Script 
   ----------------------------------------------------- */

function cancellocation() {


    
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
         

        var tr1 = $('#tdCSDICCID').text();
        var tr3 = $('#tdCSDSitecode').text();
        var url = apiServer + '/api/Cancellocation';

        jQuery.support.cors = true;
        var JSONSendData = {
            ICCID: tr1,
            Sitecode: tr3,
            type:1
        };
        $("#Nothing").hide();
        $(".modal-backdrop").hide();
        $.ajax
        ({
            url: url,
            type: 'get',
            data: JSONSendData,
            dataType: 'json',
            contentType: "application/json;charset=utf-8",
            cache: false,
            beforeSend: function () {
                $("#ajax-screen-masking").show();
            },
            success: function (data) {

               
                $("#ajax-screen-masking").hide();

                $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
                $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });


                
                var stat = data[0].vlr_number;
              
                if (stat != null) {
                   
                    $('.PMSuccess .modal-header').html("Cancel Location");
                    $('.PMSuccess .modal-body').html("Cancel Location Successful");
                    $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                        'margin-left': function () {
                            return window.pageXOffset - ($(this).width() / 2);
                        }
                    });
                } else {
                    
                    $('.PMFailed .modal-header').html("Cancel Location");
                    $('.PMFailed .modal-body').html("vlr number null so Cancel Location Failed ");
                    $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                        'margin-left': function () {
                            return window.pageXOffset - ($(this).width() / 2);
                        }
                    });
                }
            },
            error: function (data, jqXHR, exception) {
                $("#ajax-screen-masking").hide();
                alert($.parseJSON(data.responseText).errmsg);
            }
            
           
        });
        
    }
/* ----------------------------------------------------- 
   *  CMK Cancel Location Script
   ===================================================== */


