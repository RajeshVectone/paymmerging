﻿/* ===================================================== 
   *  TAB PAYMENT HISTORY PAYG  
   ----------------------------------------------------- */

/* ----------------------------------------------------------------
 * Function Name    : PaymentHistoryPAYG
 * Purpose          : to show history of Payment PAYG
 * Added by         : Edi Suryadi
 * Create Date      : January 20th, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function PaymentHistoryPAYG(MobileNo, Decision, PaymentMethod, PaymentType) {
    $("#phpaygFilterResultBody").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblPHPAYGFilterResult\"></table>");
    $("#phpaygFilterResultBody").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    
    var url = apiServer1 + "/api/Payment?mobileno=" + MobileNo + "&decision=" + Decision + "&paymethod=" + PaymentMethod + "&paytype=" + PaymentType;
    $.ajax
    ({
        url: url,
        type: "GET",
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#phpaygFilterResultBody").css({ "background-image": "none" });
            $('#tblPHPAYGFilterResult').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: false,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [[0, "desc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Payment history PAYG found."
                },
                aoColumns: [
                    {
                        mDataProp: "CreateDate", sTitle: "Payment Date", "bUseRendered": false,
                        fnRender: function (ob) {
                            return convertDateISOCustom(ob.aData.CreateDate, 1);
                        }
                    },
                    { mDataProp: "PaymentType", sTitle: "Payment Type" },
                    { mDataProp: "PaymentMethod", sTitle: "Payment Method" },
                    { mDataProp: "ReferenceId", sTitle: "Transaction ID" },
                    {
                        mDataProp: "TotalAmount", sTitle: "Payment Amount",
                        fnRender: function (ob) {
                            return getCurrencySymbol(ob.aData.Currency) + " " + ob.aData.TotalAmount.toFixed(2);
                        }
                    },
                    { mDataProp: "Status", sTitle: "Status" },
                    { mDataProp: "User", sTitle: "CRM User" },
                    {
                        mDataProp: "IpClient", sTitle: "Action", sWidth: "100px",
                        fnRender: function (ob) {
                            var dataInfo = "";
                            dataInfo += ob.aData.CreateDate + "|";
                            dataInfo += ob.aData.PaymentType + "|";
                            dataInfo += ob.aData.PaymentMethod + "|";
                            dataInfo += ob.aData.ReferenceId + "|";
                            dataInfo += ob.aData.TotalAmount + "|";
                            dataInfo += ob.aData.Status + "|";
                            dataInfo += ob.aData.User + "|";
                            dataInfo += ob.aData.RecipientNo + "|";
                            dataInfo += ob.aData.Operator + "|";
                            dataInfo += ob.aData.Country;

                            var actionURL = "";
                            actionURL = "<button data-info=\"" + dataInfo + "\" data-pid=\"" + ob.aData.ReferenceId + "\" data-pt=\"" + ob.aData.PaymentMethod + "\" class=\"btn blue btnPHPAYGMoreInfo\">More Info</button>";
                            return actionURL;
                        }
                    }
                ],
                fnDrawCallback: function () {
                    $("#tblPHPAYGFilterResult tbody td").css({ "font-size": "12px" });
                }
            });
            $("#tblPHPAYGFilterResult tbody td").css({ "font-size": "12px" });
            $("#tblPHPAYGFilterResult_wrapper .row-fluid:eq(0)").remove();
            $("#tblPHPAYGFilterResult_wrapper .row-fluid .span6:eq(0)").addClass("span4");
            $("#tblPHPAYGFilterResult_wrapper .row-fluid .span6:eq(1)").addClass("span8");
            $("#tblPHPAYGFilterResult_wrapper .row-fluid .span6").removeClass("span6");
        },
        error: function (feedback) {
            $("#phpaygFilterResultBody").css({ "background-image": "none" }).html("").append("No Payment history PAYG found.").show();
        }
    });
}

/* ----------------------------------------------------- 
   *  eof TAB PAYMENT HISTORY PAYG
   ===================================================== */


function RefreshFunction() {
    document.location.reload(true);
}





function getPaymentID(merchID) {
    $("#phpaygFilterResultBody").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblPHPAYGFilterResult\"></table>");
    $("#phpaygFilterResultBody").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var url = apiServer + '/api/FinViewtransaction/';
    jQuery.support.cors = true;
    var JSONSendData = {
        merchID: merchID
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#phpaygFilterResultBody").css({ "background-image": "none" });
            $("#phpaygFilterResultContainer").show();
            $('#tblPHPAYGFilterResult').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: false,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [[0, "desc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Topup history found."
                },
                aoColumns: [
                    { mDataProp: "REFERENCE_ID", sTitle: 'Reference Id' },
                    { mDataProp: "SITECODE", sTitle: 'Site Code' },
                    { mDataProp: "PAYMENT_AGENT", sTitle: 'Payment Agent' },
                    { mDataProp: "SERVICE_TYPE", sTitle: 'Service Type' },
                    { mDataProp: "PRODUCT_CODE", sTitle: 'Code' },
                    { mDataProp: "PAYMENT_MODE", sTitle: 'Mode' },
                    { mDataProp: "ACCOUNT_ID", sTitle: 'Account Id' },
                    { mDataProp: "CC_NO", sTitle: 'CC No' },
                    { mDataProp: "TOTALAMOUNT", sTitle: 'Amount' },
                    { mDataProp: "CURRENCY", sTitle: 'Cur' },
                    {
                        mDataProp: "CREATED_DATE", sTitle: 'Created Date',
                        fnRender: function (ob) {
                            return convertDate01(ob.aData.CREATED_DATE);
                        }
                    },
                    { mDataProp: "SUBSCRIPTIONID", sTitle: 'Subscription Id' },
                    { mDataProp: "CURRENT_STATUS", sTitle: 'Status' },
                    { mDataProp: "CS_ERROR_CODE", sTitle: 'Code' },
                    { mDataProp: "ECI_Value", sTitle: 'ECI' },
                    { mDataProp: "ReasonforReject", sTitle: 'Reject Reason' },
                    {
                        sTitle: "Action", mDataProp: "Action", "bSortable": false,
                        fnRender: function (ob) {
                            var parameters = "N/A";
                            if (ob.aData.CS_ERROR_CODE == '481') {
                                //parameters = "<center><a href=\"javascript:void(0)\" class=\"FailedID\" param-data=\"" + ob.aData.REFERENCE_ID + "\" mimsi-data=\"" + ob.aData.CS_ERROR_CODE + "\">Accept Transaction</a></center>";

                                parameters = "<center><a href=\"javascript:void(0)\" class=\"FailedID\" param-data=\"" + ob.aData.REFERENCE_ID + ":" + ob.aData.ACCOUNT_ID + ":" + ob.aData.TOTALAMOUNT + "\" mimsi-data=\"" + ob.aData.CS_ERROR_CODE + "\">Accept Transaction</a></center>";
                            }
                            else if (ob.aData.CS_ERROR_CODE == '100') {
                                parameters = "<center><a href=\"javascript:void(0)\" class=\"SucessID\" param-data=\"" + ob.aData.REFERENCE_ID + ":" + ob.aData.ACCOUNT_ID + ":" + ob.aData.TOTALAMOUNT + "\" mimsi-data=\"" + ob.aData.CS_ERROR_CODE + "\">Void Transaction</a></center>";
                                //parameters = "<center><a href=\"javascript:void(0)\" class=\"SucessID\"   param-data=\"" + ob.aData.REFERENCE_ID + "\" mimsi-data=\"" + ob.aData.CS_ERROR_CODE + "\">Void Transaction</a></center>";
                            }
                                ////else if (ob.aData.CS_ERROR_CODE == '998') {
                                ////    parameters = "<center><a href=\"javascript:void(0)\" class=\"FailedIDview\" param-data=\"" + ob.aData.REFERENCE_ID + "\" mimsi-data=\"" + ob.aData.CS_ERROR_CODE + "\">View Details</a></center>";
                                ////}
                                ////else if (ob.aData.CS_ERROR_CODE == '999') {
                                ////    parameters = "<center><a href=\"javascript:void(0)\" class=\"SucessIDview\" param-data=\"" + ob.aData.REFERENCE_ID + "\" mimsi-data=\"" + ob.aData.CS_ERROR_CODE + "\">View Deatils</a></center>";
                                ////}
                            else {
                                parameters;
                            }
                            return parameters;
                        }
                    },
                ]
            });
            $("#tblPHPAYGFilterResult tbody td").css({ "font-size": "12px" });
        },
        error: function (feedback) {
            $("#phpaygFilterResultBody").css({ "background-image": "none" }).html("").append("No Payment history PAYG found.").show();
        }
    });

    //100
    $('a.SucessID').live("click", function () {
        $('#failedMsg').modal({ keyboard: false, backdrop: 'static' }).css({
            'margin-left': function () {
                return window.pageXOffset - ($(this).width() / 2);
            }
        });
        $('#StatusCode').val($(this).attr("mimsi-data"));
        $('#MerchID').val($(this).attr("param-data"));
    });
    //481
    $('a.FailedID').live("click", function () {
        $('#SucessMsg').modal({ keyboard: false, backdrop: 'static' }).css({
            'margin-left': function () {
                return window.pageXOffset - ($(this).width() / 2);
            }
        });
        $('#StatusCode').val($(this).attr("mimsi-data"));
        $('#MerchID').val($(this).attr("param-data"));
    });

    $(document).ready(function () {
        //Decission Manager - 481
        //$('#btnSubmit').click(function () {
        //    $('#SucessMsg').modal("hide");            
        //    var reason = $('#ddlaccept').val();
        //    var MerchId = $('#MerchID').val();
        //    var StatusCode = $('#StatusCode').val();
        //    if (reason != null && reason != '')
        //    {
        //        var url = apiServer + '/api/fingetview/';
        //        jQuery.support.cors = true;
        //        var JSONSendData =
        //            {
        //            MerchID: merchID,
        //            StatusCode: StatusCode,
        //            Reason: reason,
        //            Create_revert: '0'
        //        };
        //        $.ajax
        //        ({
        //            url: url,
        //            type: 'POST',
        //            data: JSON.stringify(JSONSendData),
        //            dataType: 'json',
        //            contentType: "application/json;charset=utf-8",
        //            cache: false,
        //            success: function (feedback) {
        //                if (feedback[0].errcode == "0") {
        //                    var list = "Merchant Id : " + feedback[0].REFERENCE_ID + ",<br/>Reason :" + feedback[0].reason + ",<br/>Update Date :" + feedback[0].update_date + ".";
        //                    $('#divResult').html(list);
        //                    $('#failedConformMsg').modal({ keyboard: false, backdrop: 'static' }).css
        //                        ({
        //                            'margin-left': function () {
        //                                return window.pageXOffset - ($(this).width() / 2);
        //                            }
        //                        });
        //                }
        //                else {
        //                    $('#divResult').html(feedback[0].errmsg);
        //                }
        //            }
        //        });
        //    }
        //    else {
        //        //Validationalert(data);

        //    }


        //});


        $('#btnSubmit').click(function () {
            $('#SucessMsg').modal("hide");
            var reason = $('#ddlaccept').val();
            var MerchId = $('#MerchID').val();
            var StatusCode = $('#StatusCode').val();
            if (reason != null && reason != '') {
                var url = apiServer + '/api/fingetview/';
                jQuery.support.cors = true;
                var JSONSendData =
                    {
                        MerchID: MerchId,
                        StatusCode: StatusCode,
                        Reason: reason,
                        Create_revert: '0'
                    };
                $.ajax
                ({
                    url: url,
                    type: 'POST',
                    data: JSON.stringify(JSONSendData),
                    dataType: 'json',
                    contentType: "application/json;charset=utf-8",
                    cache: false,
                    success: function (feedback) {
                        if (feedback[0].errcode == "0") 
                        {
                            //if (feedback[0].prevbal != '' || feedback[0].prevbal == 'undefined') {
                            //    var list = feedback[0].status + ",<br/>" + "Merchant Id : " + feedback[0].REFERENCE_ID + ",<br/>Reason :" + feedback[0].reason + ",<br/>Update Date :" + feedback[0].update_date +".";
                            //}
                            //else {
                                var list = feedback[0].status + ",<br/>" + "Merchant Id : " + feedback[0].REFERENCE_ID + ",<br/>Reason :" + feedback[0].reason + ",<br/>Update Date :" + feedback[0].update_date + ",<br/>Previous balance :" + feedback[0].prevbal + ",<br/>New balance :" + feedback[0].afterbal + ".";
                            
                            $('#divResult').html(list);
                            $('#failedConformMsg').modal({ keyboard: false, backdrop: 'static' }).css
                                ({
                                    'margin-left': function () {
                                        return window.pageXOffset - ($(this).width() / 2);
                                    }
                                });
                        }
                        else {
                            $('#divResult').html(feedback[0].errmsg);
                        }
                    }
                });
            }
            else {
                //Validationalert(data);

            }


        });
        //Sucess Payement
        $('#btnConform').click(function () {
            ddlaccept
            $('#failedMsg').modal("hide");
            var MerchId = $('#MerchID').val();
            var StatusCode = $('#StatusCode').val();
            var rejectionValues = $('#ddlRejection').val();
            var url = apiServer + '/api/fingetview/';
            jQuery.support.cors = true;
            var JSONSendData =
                {
                    MerchID: MerchId,
                    StatusCode: StatusCode,
                    Reason: rejectionValues,
                    Create_revert: '1'
                };
            $.ajax
            ({
                url: url,
                type: 'POST',
                data: JSON.stringify(JSONSendData),
                dataType: 'json',
                contentType: "application/json;charset=utf-8",
                cache: false,
                success: function (feedback) {
                    if (feedback[0].errcode == "0")
                    {
                        
                      var list = feedback[0].status + ",<br/>" + "Merchant Id : " + feedback[0].REFERENCE_ID + ",<br/>Reason :" + feedback[0].reason + ",<br/>Update Date :" + feedback[0].update_date + ",<br/>New balance :" + feedback[0].prevbal + ",<br/>Previous balance :" + feedback[0].afterbal + ".";
                        
                        $('#divResult').html(list);
                        $('#failedConformMsg').modal({ keyboard: false, backdrop: 'static' }).css
                            ({
                                'margin-left': function () {
                                    return window.pageXOffset - ($(this).width() / 2);
                                }
                            });
                    }
                    else {
                        $('#divResult').html(feedback[0].errmsg);
                    }
                }




            });
        });
    });
}
