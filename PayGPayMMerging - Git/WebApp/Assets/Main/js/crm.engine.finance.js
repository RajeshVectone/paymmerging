﻿function getpaymentprofile(paymentMode) {
    if (paymentMode == "0") {
        paymentMode = '';
    }
    $("#tblDDOperationContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblDDOperation\"></table>");
    $("#tblDDOperationContainer").css({ "min-height": "100px", "background-repeat": "no-repeat", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var row = 0;
    var url = apiServer + "/api/FinViewtransaction";
    var JSONSendData = {
        paymentMode: paymentMode
    };
    $.ajax
    ({
        url: url,
        type: "GET",
        data: JSONSendData,
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('#tblDDOperation').html('');
            $("#tblDDOperationContainer").css({ "background-image": "none" });
            $('#tblDDOperation').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: true,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [[10, "desc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Payment Profile PAYM found."
                },
                aoColumns: [
                    { mDataProp: "REFERENCE_ID", sTitle: 'Reference Id' },
                    { mDataProp: "ACCOUNT_ID", sTitle: 'Account Id' },
                    { mDataProp: "CC_NO", sTitle: 'CC No' },
                    { mDataProp: "TOTALAMOUNT", sTitle: 'Amount' },
                    { mDataProp: "CURRENCY", sTitle: 'Cur' },
                    { mDataProp: "ECI_Value", sTitle: 'ECI' },
                    { mDataProp: "fraud_score", sTitle: 'Fraud Score' },
                    {
                        mDataProp: "CREATED_DATE", sTitle: 'Created Date',
                        fnRender: function (ob) {
                            return convertDate01(ob.aData.CREATED_DATE);
                        }
                    },
                    { mDataProp: "CURRENT_STATUS", sTitle: 'Status' },
                     { mDataProp: "Current_Balance", sTitle: 'Balance' },
                      { mDataProp: "IP_Address", sTitle: 'Ipaddress' },
                       { mDataProp: "email", sTitle: 'Email' },
                    { mDataProp: "CS_ERROR_CODE", sTitle: 'Code' },
                    { mDataProp: "ReasonforReject", sTitle: 'Reject Reason' },
                    { mDataProp: "Days_from_1st_topup", sTitle: '1st_topup' },
                    { mDataProp: "days_from_Lasttopup", sTitle: 'Lasttopup' },
                    { mDataProp: "total_spend", sTitle: 'spend' },
                    { mDataProp: "total_spend_consum", sTitle: 'spend_consum' },
                    { mDataProp: "spend_last3months", sTitle: 'spend_last3' },
                    { mDataProp: "spend_curr_month", sTitle: 'spendcurmth' },
                    { mDataProp: "Noof_cardtrans", sTitle: 'Noofcardtrs' },
                    { mDataProp: "Noof_voucher_trans", sTitle: 'vouchtrs' },
                    { mDataProp: "Noof_failed_cardtrans", sTitle: 'Nooffailcardtrs' },
                    { mDataProp: "Noof_success_cardtrans_30Days", sTitle: 'Noofsuscardtrs30Days' },
                    { mDataProp: "Noof_Fail_cardtrans_30Days", sTitle: 'Nooffailcardtrs30Days' },
                    //{ mDataProp: "requestid", sTitle: 'Request Id' },
                    // { mDataProp: "fraud_score", sTitle: 'Farud Score' },
                    //26-Aug-2019 : Moorthy added for retrieving SRD value
                    { mDataProp: "srd", sTitle: 'Scheme Reference Data' },
                    {
                        sTitle: "Action", mDataProp: "Action", "bSortable": false,
                        fnRender: function (ob) {
                            var parameters = ob.aData.ACCOUNT_ID + "-PAYG?merid=" + ob.aData.REFERENCE_ID;
                            var DownloadList = "<a class='btn blue' href='/Customer/TabPaymentHistoryPAYG/" + parameters + "'> Select Customer</a>";
                            return DownloadList;
                        }
                    },
                ],
                fnDrawCallback: function () {
                    $("#tblPaymentProfile tbody td").css({ "font-size": "12px" });
                }

            });
            $("#tblDDOperation tbody td").css({ "font-size": "12px" });
        },
        error: function (feedback) {
            $("#tblDDOperationContainer").css({ "background-image": "none" }).html("").append("Data's Not Available.").show();
        }
    });
}

///* Auto topup  Transaction Details*/
//function getpaymentprofileAutotopup(paymentMode) {
//    if (paymentMode == "0") {
//        paymentMode = '';
//    }
//    $("#tblDDOperationContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblDDOperation\"></table>");
//    $("#tblDDOperationContainer").css({ "min-height": "100px", "background-repeat": "no-repeat", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
//    var row = 0;
//    var url = apiServer + "/api/FinViewtransaction";
//    var JSONSendData = {
//        paymentMode: paymentMode
//    };
//    $.ajax
//    ({
//        url: url,
//        type: "GET",
//        data: JSONSendData,
//        dataType: "json",
//        async: true,
//        contentType: "application/json;charset=utf-8",
//        cache: false,
//        success: function (feedback) {
//            $('#tblDDOperation').html('');
//            $("#tblDDOperationContainer").css({ "background-image": "none" });
//            $('#tblDDOperation').dataTable({
//                bDestroy: true,
//                bRetrieve: true,
//                aaData: feedback,
//                bPaginate: true,
//                bFilter: true,
//                bInfo: false,
//                iDisplayLength: 10,
//                aaSorting: [[10, "desc"]],
//                oLanguage: {
//                    "sInfoEmpty": '',
//                    "sEmptyTable": "No Payment Profile PAYM found."
//                },
//                aoColumns: [
//                    { mDataProp: "REFERENCE_ID", sTitle: 'Reference Id' },
//                    { mDataProp: "ACCOUNT_ID", sTitle: 'Account Id' },
//                    { mDataProp: "CC_NO", sTitle: 'CC No' },
//                    { mDataProp: "TOTALAMOUNT", sTitle: 'Amount' },
//                    { mDataProp: "CURRENCY", sTitle: 'Cur' },
//                    { mDataProp: "ECI_Value", sTitle: 'ECI' },
//                    {
//                        mDataProp: "CREATED_DATE", sTitle: 'Created Date',
//                        fnRender: function (ob) {
//                            return convertDate01(ob.aData.CREATED_DATE);
//                        }
//                    },
//                    { mDataProp: "CURRENT_STATUS", sTitle: 'Status' },
//                     { mDataProp: "Current_Balance", sTitle: 'Balance' },
//                      { mDataProp: "IP_Address", sTitle: 'Ipaddress' },
//                       { mDataProp: "email", sTitle: 'Email' },
//                    { mDataProp: "CS_ERROR_CODE", sTitle: 'Code' },
//                    { mDataProp: "ReasonforReject", sTitle: 'Reject Reason' },
//                    { mDataProp: "Days_from_1st_topup", sTitle: '1st_topup' },
//                    { mDataProp: "days_from_Lasttopup", sTitle: 'Lasttopup' },
//                    { mDataProp: "total_spend", sTitle: 'spend' },
//                    { mDataProp: "total_spend_consum", sTitle: 'spend_consum' },
//                    { mDataProp: "spend_last3months", sTitle: 'spend_last3' },
//                    { mDataProp: "spend_curr_month", sTitle: 'spendcurmth' },
//                    { mDataProp: "Noof_cardtrans", sTitle: 'Noofcardtrs' },
//                    { mDataProp: "Noof_voucher_trans", sTitle: 'vouchtrs' },
//                    { mDataProp: "Noof_failed_cardtrans", sTitle: 'Nooffailcardtrs' },
//                    { mDataProp: "Noof_success_cardtrans_30Days", sTitle: 'Noofsuscardtrs30Days' },
//                    { mDataProp: "Noof_Fail_cardtrans_30Days", sTitle: 'Nooffailcardtrs30Days' },
//                    //{ mDataProp: "requestid", sTitle: 'Request Id' },
//                   // { mDataProp: "fraud_score", sTitle: 'Farud Score' },
//                    {
//                        sTitle: "Action", mDataProp: "Action", "bSortable": false,
//                        fnRender: function (ob) {
//                            var parameters = ob.aData.ACCOUNT_ID + "?merid=" + ob.aData.REFERENCE_ID;
//                            var DownloadList = "<a class='btn blue' href='/Customer/TabPaymentHistoryPAYG/" + parameters + "'> Select Customer</a>";
//                            return DownloadList;
//                        }
//                    },
//                ],
//                fnDrawCallback: function () {
//                    $("#tblPaymentProfile tbody td").css({ "font-size": "12px" });
//                }



//            });
//            $("#tblDDOperation tbody td").css({ "font-size": "12px" });
//        },
//        error: function (feedback) {
//            $("#tblDDOperationContainer").css({ "background-image": "none" }).html("").append("Data's Not Available.").show();
//        }
//    });
//}


/**/

//function getpaymentsprofile(Transation_Status, Rejection_Status, Product_Code, Customer_Name, payment_Ref, mobile_Number,EmailAddress, ICCID, startdate, enddate) {
function getpaymentsprofile(paymentMode, value) {
    $("#tblDDOperationContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblDDOperation\"></table>");
    $("#tblDDOperationContainer").css({ "min-height": "100px", "background-repeat": "no-repeat", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var row = 0;
    var url = apiServer + "/api/FinViewstransaction";
    //var JSONSendData = {
    //    Transation_Status: Transation_Status,
    //    Rejection_Status: Rejection_Status,
    //    Product_Code: Product_Code,
    //    Customer_Name: Customer_Name,
    //    payment_Ref: payment_Ref,
    //    mobile_Number: mobile_Number,
    //    EmailAddress: EmailAddress,
    //    ICCID:ICCID,
    //    startdate: startdate,
    //    enddate: enddate
    //};
    var JSONSendData = {
        paymentMode: paymentMode,
        date: value
    };
    $.ajax
    ({
        url: url,
        type: "GET",
        data: JSONSendData,
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('#tblDDOperation').html('');                       
            $("#tblDDOperationContainer").css({ "background-image": "none" });
            $('#tblDDOperation').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: true,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [[10, "desc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Payment Profile PAYM found."
                },
                aoColumns: [
                    { mDataProp: "REFERENCE_ID", sTitle: 'Reference Id' },
                    { mDataProp: "ACCOUNT_ID", sTitle: 'Account Id' },
                    { mDataProp: "CC_NO", sTitle: 'CC No' },
                    { mDataProp: "TOTALAMOUNT", sTitle: 'Amount' },
                    { mDataProp: "CURRENCY", sTitle: 'Cur' },
                    { mDataProp: "ECI_Value", sTitle: 'ECI' },
                    {
                        mDataProp: "CREATED_DATE", sTitle: 'Created Date',
                        fnRender: function (ob) {
                            return convertDate01(ob.aData.CREATED_DATE);
                        }
                    },
                    { mDataProp: "CURRENT_STATUS", sTitle: 'Status' },
                     { mDataProp: "Current_Balance", sTitle: 'Balance' },
                      { mDataProp: "IP_Address", sTitle: 'Ipaddress' },
                       { mDataProp: "email", sTitle: 'Email' },
                    { mDataProp: "CS_ERROR_CODE", sTitle: 'Code' },
                    { mDataProp: "ReasonforReject", sTitle: 'Reject Reason' },
                    { mDataProp: "Days_from_1st_topup", sTitle: '1st_topup' },
                    { mDataProp: "days_from_Lasttopup", sTitle: 'Lasttopup' },
                    { mDataProp: "total_spend", sTitle: 'spend' },
                    { mDataProp: "total_spend_consum", sTitle: 'spend_consum' },
                    { mDataProp: "spend_last3months", sTitle: 'spend_last3' },
                    { mDataProp: "spend_curr_month", sTitle: 'spendcurmth' },
                    { mDataProp: "Noof_cardtrans", sTitle: 'Noofcardtrs' },
                    { mDataProp: "Noof_voucher_trans", sTitle: 'vouchtrs' },
                    { mDataProp: "Noof_failed_cardtrans", sTitle: 'Nooffailcardtrs' },
                    { mDataProp: "Noof_success_cardtrans_30Days", sTitle: 'Noofsuscardtrs30Days' },
                    { mDataProp: "Noof_Fail_cardtrans_30Days", sTitle: 'Nooffailcardtrs30Days' },
                    //26-Aug-2019 : Moorthy added for retrieving SRD value
                    { mDataProp: "srd", sTitle: 'Scheme Reference Data' },
                    {
                        sTitle: "Action", mDataProp: "Action", "bSortable": false,
                        fnRender: function (ob) {
                            var parameters = ob.aData.ACCOUNT_ID + "-PAYG?merid=" + ob.aData.REFERENCE_ID;
                            var DownloadList = "<a class='btn blue' href='/Customer/TabPaymentHistoryPAYG/" + parameters + "'> Select Customer</a>";
                            return DownloadList;
                        }
                    },
                ],
                fnDrawCallback: function () {
                    $("#tblPaymentProfile tbody td").css({ "font-size": "12px" });
                }



            });
           // $("#tblDDOperation tbody td").css({ "font-size": "12px" });

            //$("#tblDDOperation_wrapper .row-fluid:eq(0)").remove();
            $("#tblDDOperation_wrapper .row-fluid .span6:eq(0)").addClass("span4");
            $("#tblDDOperation_wrapper .row-fluid .span6:eq(1)").addClass("span8");
            $("#tblDDOperation_wrapper .row-fluid .span6").removeClass("span6");

            var DownloadAnchor = "<a id=\"downloadAnchor\" style=\"display:none;\"></a>";
            var btnExportHistory = "<button class=\"btn blue\" id=\"btnUpload\" name=\"btnUpload\">Download to Excel</button>";
            $("#tblDDOperation_wrapper .row-fluid .span4").append(DownloadAnchor + btnExportHistory).show();
        },
        error: function (feedback) {
            $("#tblDDOperationContainer").css({ "background-image": "none" }).html("").append("Data's Not Available.").show();
        }
    });
}







function getllotgpprofile(paymentMode, value) {
    if (paymentMode == "0") {
        paymentMode = '';
    }
    $("#tblDDOperationContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblDDOperation\"></table>");
    $("#tblDDOperationContainer").css({ "min-height": "100px", "background-repeat": "no-repeat", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var row = 0;
    var url = apiServer + "/api/LOTGsget";
    var JSONSendData = {
        id: paymentMode,
        value: value
    };
    $.ajax
    ({
        url: url,
        type: "GET",
        data: JSONSendData,
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('#tblDDOperation').html('');
            $("#tblDDOperationContainer").css({ "background-image": "none" });
            $('#tblDDOperation').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: true,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [[20, "desc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Data Available."
                },
                aoColumns: [
                    { mDataProp: "LLOM_Number", sTitle: 'LLOM Number' },
                    { mDataProp: "Vectone_Number", sTitle: 'Vectone Number' },
                    { mDataProp: "Country", sTitle: 'Country' },
                    { mDataProp: "city", sTitle: 'City' },
                    { mDataProp: "plan", sTitle: 'Plan' },
                    { mDataProp: "Start_Date", sTitle: 'Start Date' },
                    { mDataProp: "End_Date", sTitle: 'End Date' },
                    { mDataProp: "Status", sTitle: 'Status' },
                    { mDataProp: "Duration", sTitle: 'Duration(Inactive Days)' },


                ],
                fnDrawCallback: function () {
                    $("#tblPaymentProfile tbody td").css({ "font-size": "12px" });

                }

            });
            $("#tblDDOperation tbody td").css({ "font-size": "12px" });
        },
        error: function (feedback) {
            $("#tblDDOperationContainer").css({ "background-image": "none" }).html("").append("Data's Not Available.").show();
        }
    });
}










function getllotgpprofile_v2(paymentMode, value) {
    if (paymentMode == "0") {
        paymentMode = '';
    }
    $("#tblDDOperationContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblDDOperation\"></table>");
    $("#tblDDOperationContainer").css({ "min-height": "100px", "background-repeat": "no-repeat", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var row = 0;
    var url = apiServer + "/api/LLOMprcitygetv2";
    var JSONSendData = {
        product: paymentMode,
        city: value
    };
    $.ajax
    ({
        url: url,
        type: "GET",
        data: JSONSendData,
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('#tblDDOperation').html('');
            $("#tblDDOperationContainer").css({ "background-image": "none" });
            $('#tblDDOperation').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: true,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [[20, "desc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Data Available."
                },
                aoColumns: [
                    { mDataProp: "LLOMNumber", sTitle: 'LLOM Number' },
                    { mDataProp: "VectoneNumber", sTitle: 'Vectone Number' },
                    { mDataProp: "Country", sTitle: 'Country' },
                    { mDataProp: "area_name", sTitle: 'Area' },
                    { mDataProp: "Plan", sTitle: 'Plan' },
                    { mDataProp: "StartDate", sTitle: 'Start Date' },
                    { mDataProp: "EndDate", sTitle: 'End Date' },
                    { mDataProp: "status", sTitle: 'Status' },
                    { mDataProp: "Duration", sTitle: 'Duration(Inactive Days)' },
                    { mDataProp: "SubscriptionDate", sTitle: 'Subscription Date' },

                ],
                fnDrawCallback: function () {
                    $("#tblPaymentProfile tbody td").css({ "font-size": "12px" });
                }
            });
            $("#tblDDOperation tbody td").css({ "font-size": "12px" });
        },
        error: function (feedback) {
            $("#tblDDOperationContainer").css({ "background-image": "none" }).html("").append("No Data Available.").show();
        }
    });
}



function getlotgctop(value) {
    if (paymentMode == "0") {
        paymentMode = '';
    }
    $("#tblDDOperationContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblDDOperation\"></table>");
    //$("#tblDDOperationContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblDDOperation\"></table>");
    $("#tblDDOperationContainer").css({ "min-height": "100px", "background-repeat": "no-repeat", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var row = 0;
    var url = apiServer + "/api/LLOMCountoproduct";
    var JSONSendData = {
        country: value
    };
    $.ajax
    ({
        url: url,
        type: "GET",
        data: JSONSendData,
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('#tblDDOperation').html('');
            $("#tblDDOperationContainer").css({ "background-image": "none" });
            $('#tblDDOperation').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: true,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [[20, "asc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Data Available."
                },
                aoColumns: [
                    { mDataProp: "Product", sTitle: 'Product' },
                    { mDataProp: "City", sTitle: 'City' },
                    { mDataProp: "Active_Numbers", sTitle: 'Active_Numbers' },
                    { mDataProp: "InActive_Numbers", sTitle: 'Inactive_Numbers' },
                    { mDataProp: "Reserved_Numbers", sTitle: 'Reserved_Numbers' },
                    {
                        sTitle: "Action", mDataProp: "Action", "bSortable": false,
                        fnRender: function (ob) {
                            var parameters = "?pro=" + ob.aData.Product;
                            var parameters2 = "&city=" + ob.aData.City;
                            var parameters3 = "&Id=" + 5;

                            var DownloadList = "<a class='btn blue' href='/LLOTGSearch/TabLsearchcustomer" + parameters + "" + parameters2 + "" + parameters3 + "'>View Details</a>";
                            return DownloadList;
                        }
                    },

                ],
                fnDrawCallback: function () {
                    $("#tblPaymentProfile tbody td").css({ "font-size": "12px" });

                }
            });
            $("#tblDDOperation tbody td").css({ "font-size": "12px" });
        },
        error: function (feedback) {
            $("#tblDDOperationContainer").css({ "background-image": "none" }).html("").append("No Payment Profile PAYM found.").show();
        }
    });


}


function getlotgctoc(value) {

    //give breadcrumb for country
    var crumbUrl;
    var crumbTitle;
    crumbUrl = "./TabLsearchcustomer?id=3&value=" + value;
    crumbTitle = value;
    var crumburl = "<a href='" + crumbUrl + "'>" + crumbTitle + "</a>";
    //" &nbsp;|&nbsp; "

    if (paymentMode == "0") {
        paymentMode = '';
    }
    $("#tblDDOperationContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblDDOperation\"></table>");
    $("#tblDDOperationContainer").css({ "min-height": "100px", "background-repeat": "no-repeat", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var row = 0;
    var url = apiServer + "/api/LLOMCountocity";
    var JSONSendData = {
        country: value
    };
    $.ajax
    ({
        url: url,
        type: "GET",
        data: JSONSendData,
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('#tblDDOperation').html('');
            $("#tblDDOperationContainer").css({ "background-image": "none" });
            $('#tblDDOperation').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: true,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [[20, "asc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Data Available."
                },
                aoColumns: [
                    //{ mDataProp: "City", sTitle: 'City' },
                    {
                        sTitle: "City", mDataProp: "City", "bSortable": true,
                        fnRender: function (ob) {
                            var Action = "3"
                            var parameters3 = "?city=" + ob.aData.City;
                            var parameters4 = "&areaid=" + ob.aData.Area_Code;


                            var DownloadList1 = "<a href='./TabLsearchcustomer" + parameters3 + "" + parameters4 + "'>" + ob.aData.City + "</a>";


                            return DownloadList1;
                        }
                    },
                    { mDataProp: "Area_Code", sTitle: 'Area Code' },
                    { mDataProp: "AllocateNumbers", sTitle: 'Allocate Numbers' },
                    { mDataProp: "Available_Number", sTitle: 'Available Number' },
                    { mDataProp: "Active_Number", sTitle: 'Active Number' },
                    { mDataProp: "InActive_Numbers", sTitle: 'InActive Numbers' },
                    { mDataProp: "Reserved_Numbers", sTitle: 'Reserved Numbers' }//,                   
                    //{
                    //    sTitle: "Action", mDataProp: "Action", "bSortable": false,
                    //    fnRender: function (ob) {
                    //        //var parameters = "?pro=" + ob.aData.Product;
                   //         //var parameters1 = "?city=" + ob.aData.City;
                   //         var parameters2 = "&areaid=" + ob.aData.Area_Code;
                     //       var DownloadList = "<a class='btn blue' href='/LLOTGSearch/TabLsearchcustomer" + parameters3 + "" + parameters2 + "'>View Details</a>";
                      //      return DownloadList;
                      //  }
                   // },
                ],
                fnDrawCallback: function () {
                    $("#tblPaymentProfile tbody td").css({ "font-size": "12px" });
                    $("div#tblDDOperation_wrapper > div.row-fluid > div.span6 > div#dataTablesBreadCrumb").remove();
                    $("<div class=dataTables_filter id=dataTablesBreadCrumb style='float:left'><label><a href='./TabLsearchcustomer'>Home</a>&nbsp;|&nbsp;" + crumburl + "</label></div>").insertAfter($("div#tblDDOperation_wrapper > div.row-fluid > div.span6 > div#tblDDOperation_length"));
                }
            });
            $("#tblDDOperation tbody td").css({ "font-size": "12px" });
        },
        error: function (feedback) {
            $("#tblDDOperationContainer").css({ "background-image": "none" }).html("").append("Data's Not Available.").show();
        }
    });
}



function getlotgproductprofileinput(Cityy, areaid, CityCountry) {
    //give breadcrumb
    //give breadcrumb for country
    var crumbUrl;
    var crumbTitle;
    crumbUrl = "./TabLsearchcustomer?id=3&value=" + CityCountry;
    crumbTitle = CityCountry;
    var crumburl = "<a href='" + crumbUrl + "'>" + crumbTitle + "</a>";
    //" &nbsp;|&nbsp; "
    //give breadcrumb

    //give breadcrumb
    //give breadcrumb for country
    var crumbUrl1;
    var crumbTitle1;
    crumbUrl1 = "./TabLsearchcustomer?city=" + Cityy + "&areaid=" + areaid;
    crumbTitle1 = Cityy;
    var crumburl1 = "<a href='" + crumbUrl1 + "'>" + crumbTitle1 + "</a>";
    //" &nbsp;|&nbsp; "
    //give breadcrumb


    $("#tblDDOperationContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblDDOperation\"></table>");
    $("#tblDDOperationContainer").css({ "min-height": "100px", "background-repeat": "no-repeat", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var row = 0;
    //var url = apiServer + "/api/getlotgproductprofileinput_v3";
    var url = apiServer + "/api/LLOTGAREAWITHID";
    //alert(url);
    var JSONSendData = {
        area: Cityy,
        id: areaid


    };
    $.ajax
    ({
        url: url,
        type: "GET",
        data: JSONSendData,
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('#tblDDOperation').html('');
            $("#tblDDOperationContainer").css({ "background-image": "none" });
            $('#tblDDOperation').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: true,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [[20, "desc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "Data's Not Available."
                },
                aoColumns: [
                    {
                        sTitle: "<input id='chkSelectAll' class='checkAll' type='checkbox' onclick='CheckGridList();'></input>", sDefaultContent: "", "bSortable": false,
                        fnRender: function (ob) {
                            row = row + 1;
                            if (ob.aData.status != 'Active') {
                                //if (ob.aData.status != 'InActive') { //commented on 12-11-2014
                                if (ob.aData.status != 'ReservedNumbers') {
                                    //return "<input type='checkbox' class='checkAll' onclick='CheckGridList(" + ob.aData.LLOMNumber + ");' id='chkChild_" + row + "' ></input>";
                                    //return "<input type='checkbox' class='checkAll' onclick='var x=\"" + ob.aData.LLOMNumber + "-" + ob.aData.status + "\";CheckGridListStatus(x);' id='chkChild_" + row + "' ></input>";
                                    return "<input type='checkbox' class='checkAll' onclick='CheckGridListStatus(\"" + ob.aData.LLOMNumber + "\",\"" + ob.aData.status + "\");' id='chkChild_" + row + "' ></input>";
                                }
                                //}
                            }
                        }
                    },
                    { mDataProp: "LLOMNumber", sTitle: 'LLOM Number' },
                    { mDataProp: "VectoneNumber", sTitle: 'Vectone Number' },
                    { mDataProp: "Country", sTitle: 'Country' },
                    { mDataProp: "area_name", sTitle: 'Area' },
                    { mDataProp: "Plan", sTitle: 'Plan' },
                    { mDataProp: "StartDate", sTitle: 'Start Date' },
                    { mDataProp: "SubscriptionDate", sTitle: 'Subscription Date' },
                    { mDataProp: "EndDate", sTitle: 'End Date' },
                    { mDataProp: "Duration", sTitle: 'Duration(Inactive Days)' },
                    { mDataProp: "status", sTitle: 'status' },
                ],
                fnDrawCallback: function () {
                    $("#tblPaymentProfile tbody td").css({ "font-size": "12px" });
                    $("div#tblDDOperation_wrapper > div.row-fluid > div.span6:eq(1)").css({ "width": "50%", "margin": "0 auto" });
                    $("div#tblDDOperation_wrapper > div.row-fluid > div.span6 > div#tblDDOperation_filter").css({ "float": "left", "padding-left": "0", "margin-left": "0", "margin-right": "auto" });
                    $("div#tblDDOperation_wrapper > div.row-fluid > div.span6 > div#dataTablesBreadCrumb").remove();
                    $("<div class=dataTables_filter id=dataTablesBreadCrumb style='float:left'><label><a href='./TabLsearchcustomer'>Home</a>&nbsp;|&nbsp;" + crumburl + "&nbsp;|&nbsp;" + crumburl1 + "</label></div>").insertAfter($("div#tblDDOperation_wrapper > div.row-fluid > div.span6 > div#tblDDOperation_length"));
                    if ($("button#ExportToExcelBtnCountry").length <= 0) {
                        var ExportToExcelBtn = "<button class=\"btn blue\" id=\"ExportToExcelBtnCountry\" name=\"ExportToExcelBtnCountry\" style=\"float:left;margin-left:0\">Download</button>";
                        $("#tblDDOperation_wrapper .row-fluid:eq(1) .span6:eq(0)").append(ExportToExcelBtn);
                    }

                }
            });
            $("#tblDDOperation tbody td").css({ "font-size": "12px" });
        },
        error: function (feedback) {
            $("#tblDDOperationContainer").css({ "background-image": "none" }).html("").append("Data's Not Available.").show();
        }
    });
}


function CheckGridList(value) {
    var $checkboxes = $("input[type='checkbox']");
    //$('#btnmapt').removeClass('None');

    $("#hdnllomnumber").val(value);
    if ($("input[type='checkbox']:checked").length > 1) {
        $("input[type='checkbox']").removeAttr("checked");
        alert("Please select only one for mapping.");
    }
    else if ($("input[type='checkbox']:checked").length == 1) {
        $('#btnmapt').removeAttr('disabled');
        $('#btnmapt').addClass('blue');
    }
    else {
        $('#btnmapt').attr('disabled', 'disabled');
        $('#btnmapt').removeClass('blue');
    }
};

function CheckGridListStatus(value, status) {
    var $checkboxes = $("input[type='checkbox']");
    //$('#btnmapt').removeClass('None');

    $("#hdnllomnumber").val(value);
    if ($("input[type='checkbox']:checked").length > 1) {
        $("input[type='checkbox']").removeAttr("checked");

        $('#btnunmap').attr('disabled', 'disabled');
        $('#btnunmap').removeClass('blue');

        $('#btnmapt').attr('disabled', 'disabled');
        $('#btnmapt').removeClass('blue');

        alert("Please select only one for mapping.");
    }
    else if ($("input[type='checkbox']:checked").length == 1) {
        if (status.toUpperCase() == 'Inactive'.toUpperCase()) {
            $('#btnunmap').removeAttr('disabled');
            $('#btnunmap').addClass('blue');
        }
        else {
            $('#btnmapt').removeAttr('disabled');
            $('#btnmapt').addClass('blue');
        }
    }
    else {
        //if (status.toUpperCase() == 'Inactive'.toUpperCase()) {
        $('#btnunmap').attr('disabled', 'disabled');
        $('#btnunmap').removeClass('blue');
        //}
        //else {
        $('#btnmapt').attr('disabled', 'disabled');
        $('#btnmapt').removeClass('blue');
        //}
    }
};

function CheckGridListunmap(value1, value2) {
    var $checkboxes = $("input[type='checkbox']");
    //$('#btnunmap').removeClass('None');    
    $("#hdnllomnumberunmap1").val(value1);
    $("#hdnllomnumberunmap2").val(value2);
    if ($("input[type='checkbox']:checked").length > 1) {
        $("input[type='checkbox']").removeAttr("checked");
        alert("Please select only one for Unmapping.");
    }
    else if ($("input[type='checkbox']:checked").length == 1) {
        $('#btnunmap').removeAttr('disabled');
        $('#btnunmap').addClass('blue');
    }
    else {
        $('#btnunmap').attr('disabled', 'disabled');
        $('#btnunmap').removeClass('blue');
    }
};



$(".checkAll").change(function () {
    if ($(".case").length == $(".case:checked").length) {
        $("#selectall").attr("checked", "checked");
    } else {
        $("#selectall").removeAttr("checked");
    }

});


function chekedValues() {

    var rows = $("#tblDDOperation").dataTable().fnGetNodes();
    var checked1 = $('#chkSelectAll').prop('checked');
    //$('.value').prop('checked', true);
    for (var i = 0; i < rows.length; i++) {
        $('#chkChild_' + (i + 1)).prop('checked', checked1);
    }
    if (checked1 == true)
        DownloadAll = 'Y';
    else
        DownloadAll = 'N';
}


function getlotgproductprofileinput_v1(paymentMode) {
    if (paymentMode == "0") {
        paymentMode = '';
    }

    //give breadcrumb
    //give breadcrumb for country
    var crumbUrl;
    var crumbTitle;
    crumbTitle = "Product";

    //" &nbsp;|&nbsp; "
    //give breadcrumb
    /*
    var script = 
        "$(''#btnunmap'').addClass(''None'');" +
        "getlotgproductprofile();" +
        //$(this).addClass('selectedtb');
        "$(''#countryload'').removeClass(''selectedtb'');" +
        "$(''#cityload'').removeClass(''selectedtb'');" +
        "$(this).addClass(''selectedtb'');"
        */
    //var script = "alert(''hi'')";
    var crumburl = "<a id='productloadcrumb' onclick=executeProductLinkCrumb();>" + crumbTitle + "</a>";




    //give breadcrumb
    //give breadcrumb for country
    var crumbUrl1;
    var crumbTitle1;
    crumbUrl1 = "./TabLsearchcustomer?pro=" + paymentMode;
    crumbTitle1 = paymentMode;
    var crumburl1 = "<a href=" + crumbUrl1 + ">" + crumbTitle1 + "</a>";
    //" &nbsp;|&nbsp; "
    //give breadcrumb


    $("#tblDDOperationContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblDDOperation\"></table>");
    $("#tblDDOperationContainer").css({ "min-height": "100px", "background-repeat": "no-repeat", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var row = 0;
    var url = apiServer + "/api/LLOMproductwiseres";
    //alert(url);
    var JSONSendData = {
        product: paymentMode
    };
    $.ajax
    ({
        url: url,
        type: "GET",
        data: JSONSendData,
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('#tblDDOperation').html('');
            $("#tblDDOperationContainer").css({ "background-image": "none" });
            $('#tblDDOperation').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: true,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [[20, "desc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Data Available."
                },
                aoColumns: [
                     {
                         sTitle: "<input id='chkSelectpro3' class='checkAllpro' type='checkbox' onclick='CheckGridListpro();'></input>", sDefaultContent: "", "bSortable": false,
                         fnRender: function (ob) {
                             row = row + 1;
                             if (ob.aData.status != 'Active') {
                                 //return "<input type='checkbox' id='chkSelectpro' class='checkAllpro' onclick='CheckGridListpro(" + ob.aData.LLOMNumber + ");' id='chkChild_" + row + "' ></input>";
                                 return "<input type='checkbox' id='chkSelectpro' class='checkAllpro' onclick='CheckGridListunmap(" + ob.aData.LLOMNumber + "," + ob.aData.VectoneNumber + ");' id='chkChild_" + row + "' ></input>";

                             }
                         }
                     },
                    { mDataProp: "LLOMNumber", sTitle: 'LLOM Number' },
                    { mDataProp: "VectoneNumber", sTitle: 'Vectone Number' },
                    { mDataProp: "Country", sTitle: 'Country' },
                    { mDataProp: "area_name", sTitle: 'Area' },
                    { mDataProp: "Plan", sTitle: 'Plan' },
                    { mDataProp: "StartDate", sTitle: 'Start Date' },
                    { mDataProp: "EndDate", sTitle: 'End Date' },
                    { mDataProp: "Duration", sTitle: 'Duration(Inactive Days)' },
                    { mDataProp: "status", sTitle: 'status' },
                ],
                fnDrawCallback: function () {
                    $("#tblPaymentProfile tbody td").css({ "font-size": "12px" });
                    $("div#tblDDOperation_wrapper > div.row-fluid > div.span6:eq(1)").css({ "width": "50%", "margin": "0 auto" });
                    $("div#tblDDOperation_wrapper > div.row-fluid > div.span6 > div#tblDDOperation_filter").css({ "float": "left", "padding-left": "0", "margin-left": "0", "margin-right": "auto" });
                    $("div#tblDDOperation_wrapper > div.row-fluid > div.span6 > div#dataTablesBreadCrumb").remove();
                    $("<div class=dataTables_filter id=dataTablesBreadCrumb style='float:left'><label>" + "<a href='./TabLsearchcustomer'>Home</a>&nbsp;|&nbsp;" + crumburl + "&nbsp;|&nbsp;" + crumburl1 + "</a></label></div>").insertAfter($("div#tblDDOperation_wrapper > div.row-fluid > div.span6 > div#tblDDOperation_length"));

                    if ($("button#ExportToExcelBtnProduct").length <= 0) {
                        var ExportToExcelBtn = "<button class=\"btn blue\" id=\"ExportToExcelBtnProduct\" name=\"ExportToExcelBtnProduct\" style=\"float:left;margin-left:0\">Download</button>";
                        $("#tblDDOperation_wrapper .row-fluid:eq(1) .span6:eq(0)").append(ExportToExcelBtn);
                    }
                }

            });
            $("#tblDDOperation tbody td").css({ "font-size": "12px" });
        },
        error: function (feedback) {
            $("#tblDDOperationContainer").css({ "background-image": "none" }).html("").append("No Payment Profile PAYM found.").show();
        }
    });
}


function CheckGridListpro(value) {
    var chkArray = [];
    var $checkboxes = $("input[type='checkbox']");
    $('#btnunmap').removeClass('None');
    $("#hdnllomnumberlist").val(value);
    $(".checkAllpro:checked").each(function () {
        //alert('siva');

    });
    var selected;
    selected = chkArray.join(',') + ",";
    if (selected.length > 1) {
        alert("You have selected " + selected);
    } else {
        alert("Please at least one of the checkbox");
    }
};



function GetAllChecked() {
    var chkedshid = new Array();
    var rows = new Array();
    rows = document.getElementById("tblDDOperation").getElementsByTagName("tr");
    trcount = rows.length;

    var totlchk = new Array();
    for (var i = 0; i < rows.length; i++) {
        trid = rows[i].id;


        if (inputList = document.getElementById(trid).getElementsByTagName("input")) {
            for (var n = 0; n < inputList.length; n++) {
                if (inputList[n].type == "checkbox") {
                    if (inputList[n].checked == true) {
                        chkSelectpro[n] = inputList[n].id;
                    }
                }
            }
            totlchk = totlchk.concat(chkedshid.join());
            alert(totlchk);
        }

    }

    document.getElementById('myHiddenfield').value = totlchk.join();
    document.getElementById("BtnSav2Cart").click();
}



function getlotgprofile() {
 
    $("#tblDDOperationContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblDDOperation\"></table>");
    $("#tblDDOperationContainer").css({ "min-height": "150px", "background-repeat": "no-repeat", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var row = 0;
    var url = apiServer + "/api/LLOTGNUM";

    var JSONSendData = {
        //timestamp: new Date().getTime()
    };
    $.ajax
    ({
        url: url,
        type: "GET",
        data: JSONSendData,
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",

        success: function (feedback) {
            $('#tblDDOperation').html('');
            $("#tblDDOperationContainer").css({ "background-image": "none" });
            $('#tblDDOperation').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: false,
                bFilter: true,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [[20, "asc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Data Available."
                },
                aoColumns: [
                    //{ mDataProp: "Country", sTitle: 'Country' },
                    {
                        sTitle: "Country", mDataProp: "Country", "bSortable": true,
                        fnRender: function (ob) {
                            var Action = "3"
                            var parameters = ob.aData.Country;
                            // var DataParam = $(this).attr("data-param").split("|");
                            var DataParam = parameters;//.split("|");
                            //window.location.href = "/LLOTGSearch/TabLsearchcustomer?id=" + Action + "&value=" + DataParam + "";
                            //actionOption = "<a href='/Edit?id="+ oObj.aData[0] + "'>Edit</a>";

                            actionOption = "<a href='/LLOTGSearch/TabLsearchcustomer?id=" + Action + "&value=" + DataParam + "'>" + DataParam + "</a>";

                            ////fnRender: function (ob) {                           
                            ////    var DownloadList = "<a class='btn blue' href='" +  + "'> Select</a>";
                            return actionOption;
                        }
                    },
                    { mDataProp: "Allocated_Numbers", sTitle: 'Allocated Numbers' },
                    { mDataProp: "Active_Numbers", sTitle: 'Active Numbers' },
                    { mDataProp: "Available_Numbers", sTitle: 'Available Numbers' },
                    { mDataProp: "Reserved_Numbers", sTitle: 'Reserved Numbers' }
                    /*,{
                        sTitle: "Action", mDataProp: "Action", "bSortable": false,
                        fnRender: function (ob) {
                            var parameters = ob.aData.Country;
                        //    //var content = '<select class="Select" style="color:#a0a0a0">';
                        //    //content += '<option value="0">--- Select ---</option>';
                        //    //content += '<option value="/LLOTGSearch/TabLsearchcustomer ' + parameters + '" onClick="select();">View By Product</option>';
                        //    //content += '<option value="http://www.google.com/">View By City</option>';
                        //    //content += '</select>';
                            var actionOption = "<select class=\"selectCountrySaverAction\" data-param=\"" + parameters + "\" >";
                            actionOption += '<option value="0" style=\"color:#c0c0c0\">--- Select an action ---</option>';
                            actionOption += '<option value="2" style=\"padding:3px\">View By Product</option>';
                            actionOption += '<option value="3" style=\"padding:3px\">View By City</option>';
                            actionOption += '</select>';
                      
                        ////fnRender: function (ob) {                           
                        ////    var DownloadList = "<a class='btn blue' href='" +  + "'> Select</a>";
                            return actionOption;
                        }
                    },*/
                ],

                fnDrawCallback: function () {
                    $("#tblPaymentProfile tbody td").css({ "font-size": "12px" });
                    $("div#tblDDOperation_wrapper > div.row-fluid > div.span6:eq(0)").css({ "width": "0px" });
                    $("div#tblDDOperation_wrapper > div.row-fluid > div.span6 > div#tblDDOperation_filter").css({ "float": "left", "padding-left": "0", "margin-left": "0", "margin-right": "auto" });


                }

            });
            $("#tblDDOperation tbody td").css({ "font-size": "12px" });
        },
        error: function (feedback) {
            $("#tblDDOperationContainer").css({ "background-image": "none" }).html("").append("No Payment Profile PAYM found.").show();
        }
    });

}


//$(function select () {
//    // bind change event to select
//    $('#dynamic_select').bind('change', function () {
//        var url = $(this).val(); // get selected value
//        if (url) { // require a URL
//            window.location = url; // redirect
//        }
//        return false;
//    });
//});






function getlotgproductprofile() {

    //give breadcrumb
    //give breadcrumb for country
    var crumbUrl;
    var crumbTitle;
    crumbTitle = "Product";

    //" &nbsp;|&nbsp; "
    //give breadcrumb
    /*
    var script = 
        "$(''#btnunmap'').addClass(''None'');" +
        "getlotgproductprofile();" +
        //$(this).addClass('selectedtb');
        "$(''#countryload'').removeClass(''selectedtb'');" +
        "$(''#cityload'').removeClass(''selectedtb'');" +
        "$(this).addClass(''selectedtb'');"
        */
    //var script = "alert(''hi'')";
    var crumburl = "<a id='productloadcrumb' onclick=executeProductLinkCrumb();>" + crumbTitle + "</a>";

    $("#tblDDOperationContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblDDOperation\"></table>");
    $("#tblDDOperationContainer").css({ "min-height": "150px", "background-repeat": "no-repeat", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var row = 0;
    var url = apiServer + "/api/Lottmproductview";
    var JSONSendData = {

    };
    $.ajax
    ({
        url: url,
        type: "GET",
        data: JSONSendData,
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('#tblDDOperation').html('');
            $("#tblDDOperationContainer").css({ "background-image": "none" });
            $('#tblDDOperation').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: true,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [[20, "desc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Data Available."
                },
                aoColumns: [
                    //{ mDataProp: "Product", sTitle: 'Product' },
                    {
                        sTitle: "Product", mDataProp: "Product", "bSortable": true,
                        fnRender: function (ob) {
                            var parameters = "?pro=" + ob.aData.Product;
                            var DownloadList = "<a href='/LLOTGSearch/TabLsearchcustomer" + parameters + "'>" + ob.aData.Product + "</a>";
                            return DownloadList;
                        }
                    },
                    { mDataProp: "Active_Numbers", sTitle: 'Active Numbers' },
                    { mDataProp: "InActive_Numbers", sTitle: 'Inactive Numbers' },
                    { mDataProp: "Reserved_Numbers", sTitle: 'Reserved Numbers' }
                    /*,{
                        sTitle: "Action", mDataProp: "Action", "bSortable": false,
                        fnRender: function (ob) {
                            var parameters = "?pro=" + ob.aData.Product;
                            var DownloadList = "<a class='btn blue' href='/LLOTGSearch/TabLsearchcustomer" + parameters + "'>View Details</a>";
                            return DownloadList;
                        }
                    },*/
                ],

                fnDrawCallback: function () {
                    $("#tblPaymentProfile tbody td").css({ "font-size": "12px" });
                    $("div#tblDDOperation_wrapper > div.row-fluid > div.span6 > div#dataTablesBreadCrumb").remove();
                    $("<div class=dataTables_filter id=dataTablesBreadCrumb style='float:left'><label>" + "<a href='./TabLsearchcustomer'>Home</a>&nbsp;|&nbsp;" + crumburl + "</a></label></div>").insertAfter($("div#tblDDOperation_wrapper > div.row-fluid > div.span6 > div#tblDDOperation_length"));


                }

            });
            $("#tblDDOperation tbody td").css({ "font-size": "12px" });
        },
        error: function (feedback) {
            $("#tblDDOperationContainer").css({ "background-image": "none" }).html("").append("Data's not Available.").show();
        }
    });

}




function getlotgcityprofile() {
    $("#tblDDOperationContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblDDOperation\"></table>");
    $("#tblDDOperationContainer").css({ "min-height": "150px", "background-repeat": "no-repeat", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var row = 0;
    var url = apiServer + "/api/LLOMcityview";
    var JSONSendData = {
    };
    $.ajax
    ({
        url: url,
        type: "GET",
        data: JSONSendData,
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('#tblDDOperation').html('');
            $("#tblDDOperationContainer").css({ "background-image": "none" });
            $('#tblDDOperation').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: true,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [[20, "asc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Data Available."
                },
                aoColumns: [
                    { mDataProp: "City", sTitle: 'City' },
                    { mDataProp: "Area_Code", sTitle: 'Area Code' },
                    { mDataProp: "AllocateNumbers", sTitle: 'Allocate Numbers' },
                    { mDataProp: "Available_Number", sTitle: 'Available Number' },
                    { mDataProp: "Active_Numbers", sTitle: 'Active Numbers' },
                    { mDataProp: "InActive_Numbers", sTitle: 'Inactive Numbers' },
                    { mDataProp: "Reserved_Numbers", sTitle: 'Reserved Numbers' },

                    {
                        sTitle: "Action", mDataProp: "Action", "bSortable": false,
                        fnRender: function (ob) {
                            //var parameters = "?pro=" + ob.aData.Product;
                            var parameters1 = "?city=" + ob.aData.City;
                            var parameters2 = "&areaid=" + ob.aData.Area_Code;
                            var DownloadList = "<a class='btn blue' href='/LLOTGSearch/TabLsearchcustomer" + parameters1 + "" + parameters2 + "'>View Details</a>";
                            return DownloadList;
                        }
                    },
                ],

                fnDrawCallback: function () {
                    $("#tblPaymentProfile tbody td").css({ "font-size": "12px" });

                }

            });
            $("#tblDDOperation tbody td").css({ "font-size": "12px" });
        },
        error: function (feedback) {
            $("#tblDDOperationContainer").css({ "background-image": "none" }).html("").append("Data's not Available.").show();
        }
    });
}

function getpaymentprofiledownload(paymentMode) {
    if (paymentMode == "0") {
        paymentMode = '';
    }
    var i = 0;
    jQuery.support.cors = true;
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader1").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    $("#ajax-screen-masking #downloadIFrame").remove();
    //var url = apiServer + '/Download/DownloadCreateTransaction';
    var url = '/Download/DownloadCreateTransactionNew';
    var JSONSendData = {
        paymentMode: paymentMode
    };
    $.ajax
    ({
        url: url,
        type: 'get',
        dataType: 'json',
        data: JSONSendData,
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {
            
            if (feedback.errcode == 0) {
                $("#ajax-screen-masking").hide();
                var IFrameData = $('<iframe/>', { id: 'downloadIFrame', src: '/Download/DownloadFinanceTransaction?id=' + feedback.Text, style: 'display:none' });
                $("#ajax-screen-masking").append(IFrameData);
            }
        }
    });
}

///* Autotopup download */
//function getpaymentprofiledownloadAutoTopup(paymentMode) {
//    if (paymentMode == "0") {
//        paymentMode = '';
//    }
//    var i = 0;
//    var ajaxSpinnerTop = $(window).outerHeight() / 2;
//    $("#ajax-screen-masking #ajax-loader1").css({ "margin-top": ajaxSpinnerTop + "px" });
//    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
//    $("#ajax-screen-masking #downloadIFrame").remove();
//    var url = '/Download/DownloadCreateTransaction'
//    var JSONSendData = {
//        paymentMode: paymentMode
//    };
//    $.ajax
//    ({
//        url: url,
//        type: 'get',
//        dataType: 'json',
//        data: JSONSendData,
//        contentType: "application/json;charset=utf-8",
//        cache: false,
//        beforeSend: function () {
//            $("#ajax-screen-masking").show();
//        },
//        success: function (feedback) {
//            if (feedback.errcode == 0) {
//                $("#ajax-screen-masking").hide();
//                var IFrameData = $('<iframe/>', { id: 'downloadIFrame', src: '/Download/DownloadViewTransaction?id=' + feedback.Text, style: 'display:none' });
//                $("#ajax-screen-masking").append(IFrameData);
//            }
//        }
//    });
//}


/**/



//function getpaymentsprofiledownload(Transation_Status, Rejection_Status, Product_Code, Customer_Name, payment_Ref, mobile_Number, EmailAddress, ICCID, startdate, enddate) {
function getpaymentsprofiledownload(paymentMode, value) {
    if (paymentMode == "0") {
        paymentMode = '';
    }
    var i = 0;
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    var url = '/Download/DownloadsCreateTransaction'
    //var JSONSendData = {
    //    Transation_Status: Transation_Status,
    //    Rejection_Status: Rejection_Status,
    //    Product_Code: Product_Code,
    //    Customer_Name: Customer_Name,
    //    payment_Ref: payment_Ref,
    //    mobile_Number: mobile_Number,
    //    EmailAddress: EmailAddress,
    //    ICCID: ICCID,
    //    startdate: startdate,
    //    enddate: enddate
    //};
    var JSONSendData = {
        paymentMode: paymentMode,
        date: value
    };
    $.ajax
    ({
        url: url,
        type: 'get',
        dataType: 'json',
        data: JSONSendData,
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#tblDDOperationContainer").show();
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {
            if (feedback.errcode == 0) {
                $("#ajax-screen-masking").hide();
                $("#tblDDOperationContainer").css({ "background-image": "none" });
                var IFrameData = $('<iframe/>', { id: 'downloadIFrame', src: '/Download/DownloadSearchTransaction?id=' + feedback.Text, style: 'display:none' });
                $("#ajax-screen-masking").append(IFrameData);
            }
        }
    });
}



function getllotgmapnumber(llomno, vectoneno) {
    var i = 0;
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking1 #ajax-loader1").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking1").css({ "height": $(document).outerHeight() + "px" });
    $("#ajax-screen-masking #downloadIFrame").remove();
    var url = apiServer + "/api/LLOMMapping";
    var JSONSendData = {
        llomno: llomno,
        vectoneno: vectoneno
    };
    $.ajax
    ({
        url: url,
        type: 'get',
        dataType: 'json',
        data: JSONSendData,
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking1").show();
        },
        success: function (feedback) {
            $("#ajax-screen-masking").append(IFrameData);
        }
    });
}




function getllotgunmapnumber(llomno, vectoneno) {
    var i = 0;
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking1 #ajax-loader1").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking1").css({ "height": $(document).outerHeight() + "px" });
    $("#ajax-screen-masking #downloadIFrame").remove();
    var url = apiServer + "/api/LLOMUnmap";
    var JSONSendData = {
        llomno: llomno,
        vectoneno: vectoneno
    };
    $.ajax
    ({
        url: url,
        type: 'get',
        dataType: 'json',
        data: JSONSendData,
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking1").show();
        },
        success: function (feedback) {
            $("#ajax-screen-masking").append(IFrameData);
        }
    });
}



function DownloadCountryDetails(Cityy, areaid, CityCountry) {
    var url = '/Download/DownloadCountryDetails';
    var ajaxSpinnerTop = ($(window).outerHeight() / 2) + $(window).scrollTop();
    $("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    $("#ajax-screen-masking #downloadIFrame").remove();

    /*var siteCode = $("input[name=ExportSitecode]").val();
    var CountryName = $("input[name=ExportCountry]").val();
    var productCode = $.trim('@ViewBag._productcode');
    */
    var IFrameURL = "";

    //var searchBy = $.trim($("input[name=ExportSearchBy]").val());
    //var searchValue = $.trim($("input[name=ExportSearchValue]").val());
    var searchValue = $("div#tblDDOperation_wrapper > div.row-fluid > div.span6 > div#tblDDOperation_filter > label > input").val();

    var apilink = apiServer + "/api/LLOTGAREAWITHID";

    var JSONSendData = {
        Area: Cityy,
        AreaId: areaid,
        CountryName: CityCountry,
        searchvalue: searchValue,
        isDownload: true,
        api: apilink
    };


    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {
            if (feedback.errcode == 0) {
                var IFrameData = $('<iframe/>', { id: 'downloadIFrame', src: '/Download/GoDownloadCountryDetails?id=' + feedback.Text, style: 'display:none' });
                $("#ajax-screen-masking").append(IFrameData);
            }
            $("#ajax-screen-masking").hide();
        }
    });

}


function DownloadProductDetails(paymentMode) {
    var url = '/Download/DownloadProductDetails';
    var ajaxSpinnerTop = ($(window).outerHeight() / 2) + $(window).scrollTop();
    $("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    $("#ajax-screen-masking #downloadIFrame").remove();

    /*
    var siteCode = $("input[name=ExportSitecode]").val();
    var CountryName = $("input[name=ExportCountry]").val();
    var productCode = $.trim('@ViewBag._productcode');
    */

    var IFrameURL = "";

    //var searchBy = $.trim($("input[name=ExportSearchBy]").val());
    //var searchValue = $.trim($("input[name=ExportSearchValue]").val());
    var searchValue = $("div#tblDDOperation_wrapper > div.row-fluid > div.span6 > div#tblDDOperation_filter > label > input").val();

    var apilink = apiServer + "/api/LLOMproductwiseres";

    var JSONSendData = {
        paymentMode: paymentMode,
        searchvalue: searchValue,
        isDownload: true,
        api: apilink
    };


    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {
            if (feedback.errcode == 0) {
                var IFrameData = $('<iframe/>', { id: 'downloadIFrame', src: '/Download/GoDownloadProductDetails?id=' + feedback.Text, style: 'display:none' });
                $("#ajax-screen-masking").append(IFrameData);
            }
            $("#ajax-screen-masking").hide();
        }
    });
}

//BreakageReport Search
function getbreakagereport(sitecode, date_fr, date_to) {
    $("#tblDDOperationContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblDDOperation\"></table>");
    $("#tblDDOperationContainer").css({ "min-height": "100px", "background-repeat": "no-repeat", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var row = 0;
    var url = apiServer + "/api/FinBreakagereport";
    var JSONSendData = {
        mobileno: "All",
        sitecode: sitecode,
        date_fr: date_fr,
        date_to: date_to
    };
    $.ajax
    ({
        url: url,
        type: "POST",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('#tblDDOperation').html('');
            $("#tblDDOperationContainer").css({ "background-image": "none" });
            $('#tblDDOperation').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: true,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [[0, "desc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No records found."
                },
                aoColumns: [
                    { mDataProp: "logdate", sTitle: "Date", sWidth: "200px" },
                    { mDataProp: "mobileno", sTitle: "Mobile no" },
                    { mDataProp: "package", sTitle: "Package", sWidth: "280px" },
                    { mDataProp: "GPRS_CHARGE", sTitle: "GPRS Usage MB" },
                    { mDataProp: "PACKAGE_ID", sTitle: "Package ID" },
                    { mDataProp: "INITIAL_BALANCE_MB", sTitle: "Initial Balance MB" },
                    { mDataProp: "ASSIGNED_BALANCE_MB", sTitle: "Assigned Balance MB" },
                ],
                fnDrawCallback: function () {
                    $("#tblDDOperation tbody td").css({ "font-size": "12px" });
                }
            });
            $("#tblDDOperation tbody td").css({ "font-size": "12px" });
        },
        error: function (feedback) {
            $("#tblDDOperationContainer").css({ "background-image": "none" }).html("").append("No records found.").show();
        }
    });
}

//BreakageReport Download
function downloadbreakagereport(sitecode, date_fr, date_to) {
    var i = 0;
    jQuery.support.cors = true;
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader1").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    $("#ajax-screen-masking #downloadIFrame").remove();
    var url = '/Download/DownloadBreakageReport';
    var JSONSendData = {
        mobileno: "All",
        sitecode: sitecode,
        date_fr: date_fr,
        date_to: date_to
    };
    $.ajax
    ({
        url: url,
        type: 'get',
        dataType: 'json',
        data: JSONSendData,
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {

            if (feedback.errcode == 0) {
                $("#ajax-screen-masking").hide();
                var IFrameData = $('<iframe/>', { id: 'downloadIFrame', src: '/Download/DownloadFinanceBreakageReport?id=' + feedback.Text, style: 'display:none' });
                $("#ajax-screen-masking").append(IFrameData);
            }
        }
    });
}

//AddBalanceReport Search
function getaddbalancereport(sitecode, date_fr, date_to) {
    $("#tblDDOperationContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblDDOperation\"></table>");
    $("#tblDDOperationContainer").css({ "min-height": "100px", "background-repeat": "no-repeat", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var row = 0;
    var url = apiServer + "/api/AddBalancereport";
    var JSONSendData = {
        calledby: "CRM",
        sitecode: sitecode,
        date_from: date_fr,
        date_to: date_to
    };
    $.ajax
    ({
        url: url,
        type: "POST",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('#tblDDOperation').html('');
            $("#tblDDOperationContainer").css({ "background-image": "none" });
            $('#tblDDOperation').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: true,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [[0, "desc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No records found."
                },
                aoColumns: [
                    { mDataProp: "mobileno", sTitle: "Mobile no", sWidth: "200px" },
                    { mDataProp: "referenceID", sTitle: "Reference ID" },
                    { mDataProp: "credit_date", sTitle: "Credit Date" },
                    { mDataProp: "username", sTitle: "Username", sWidth: "280px" },
                    { mDataProp: "amount", sTitle: "Amount" },
                    { mDataProp: "prevbalance", sTitle: "Prev Balance" },
                    { mDataProp: "afterbalance", sTitle: "After Balance" },
                ],
                fnDrawCallback: function () {
                    $("#tblDDOperation tbody td").css({ "font-size": "12px" });
                }
            });
            $("#tblDDOperation tbody td").css({ "font-size": "12px" });
        },
        error: function (feedback) {
            $("#tblDDOperationContainer").css({ "background-image": "none" }).html("").append("No records found.").show();
        }
    });
}

//AddBalanceReport Download
function downloadaddbalancereport(sitecode, date_fr, date_to) {
    var i = 0;
    jQuery.support.cors = true;
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader1").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    $("#ajax-screen-masking #downloadIFrame").remove();
    var url = '/Download/DownloadTransferBalanceReport';
    var JSONSendData = {
        calledby: "CRM",
        sitecode: sitecode,
        date_fr: date_fr,
        date_to: date_to
    };
    $.ajax
    ({
        url: url,
        type: 'get',
        dataType: 'json',
        data: JSONSendData,
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {

            if (feedback.errcode == 0) {
                $("#ajax-screen-masking").hide();
                var IFrameData = $('<iframe/>', { id: 'downloadIFrame', src: '/Download/DownloadFinanceTransferBalanceReport?id=' + feedback.Text, style: 'display:none' });
                $("#ajax-screen-masking").append(IFrameData);
            }
        }
    });
}

//TransferBalanceReport Search
function gettransferbalancereport(sitecode, date_fr, date_to) {
    $("#tblDDOperationContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblDDOperation\"></table>");
    $("#tblDDOperationContainer").css({ "min-height": "100px", "background-repeat": "no-repeat", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var row = 0;
    var url = apiServer + "/api/TransferBalancereport";
    var JSONSendData = {
        calledby: "CRM",
        sitecode: sitecode,
        date_from: date_fr,
        date_to: date_to
    };
    $.ajax
    ({
        url: url,
        type: "POST",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('#tblDDOperation').html('');
            $("#tblDDOperationContainer").css({ "background-image": "none" });
            $('#tblDDOperation').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: true,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [[0, "desc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No records found."
                },
                aoColumns: [
                    { mDataProp: "referenceid", sTitle: "Reference ID"},
                    { mDataProp: "debit_date_string", sTitle: "Debit Date" },
                    { mDataProp: "donor_mobileno", sTitle: "Donor Mobile No" },
                    { mDataProp: "debit_amount", sTitle: "Debit Amount" },
                    { mDataProp: "credit_date_string", sTitle: "Credit Date" },
                    { mDataProp: "receiver_mobileno", sTitle: "Receiver Mobile No" },
                    { mDataProp: "credit_amount", sTitle: "Credit Amount" },
                    { mDataProp: "process_by", sTitle: "Process By" },
                ],
                fnDrawCallback: function () {
                    $("#tblDDOperation tbody td").css({ "font-size": "12px" });
                }
            });
            $("#tblDDOperation tbody td").css({ "font-size": "12px" });
        },
        error: function (feedback) {
            $("#tblDDOperationContainer").css({ "background-image": "none" }).html("").append("No records found.").show();
        }
    });
}

function downloadtransferbalancereport(sitecode, date_fr, date_to) {
    var i = 0;
    jQuery.support.cors = true;
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader1").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    $("#ajax-screen-masking #downloadIFrame").remove();
    var url = '/Download/DownloadTransferBalanceReport';
    var JSONSendData = {
        calledby: "CRM",
        sitecode: sitecode,
        date_fr: date_fr,
        date_to: date_to
    };
    $.ajax
    ({
        url: url,
        type: 'get',
        dataType: 'json',
        data: JSONSendData,
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {

            if (feedback.errcode == 0) {
                $("#ajax-screen-masking").hide();
                var IFrameData = $('<iframe/>', { id: 'downloadIFrame', src: '/Download/DownloadFinanceTransferBalanceReport?id=' + feedback.Text, style: 'display:none' });
                $("#ajax-screen-masking").append(IFrameData);
            }
        }
    });
}

//05-Feb-2019 : Moorthy : Added for auto Topup Report
function GetAutoTopupReport(value) {
    $("#tblDDOperationContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblDDOperation\"></table>");
    $("#tblDDOperationContainer").css({ "min-height": "100px", "background-repeat": "no-repeat", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var row = 0;
    var url = apiServer + "/api/FinViewstransaction";
    var JSONSendData = {
        paymentMode: "202",
        date: value
    };
    $.ajax
    ({
        url: url,
        type: "GET",
        data: JSONSendData,
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('#tblDDOperation').html('');
            $("#tblDDOperationContainer").css({ "background-image": "none" });
            $('#tblDDOperation').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: true,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Payment Profile PAYM found."
                },
                aoColumns: [
                    { mDataProp: "sorderid", sTitle: 'Reference Id' },
                    { mDataProp: "mobileno", sTitle: 'Account Id' },
                    { mDataProp: "subscription_id", sTitle: 'Subscription Id' },
                    { mDataProp: "amount", sTitle: 'Amount' },
                    { mDataProp: "currency", sTitle: 'Currency' },
                    { mDataProp: "reg_date", sTitle: 'Created Date'},
                    { mDataProp: "decision", sTitle: 'Status' },
                    { mDataProp: "payment_status", sTitle: 'Code' },
                    //26-Aug-2019 : Moorthy added for retrieving SRD value
                    { mDataProp: "srd", sTitle: 'Scheme Reference Data' }
                ],
                fnDrawCallback: function () {
                    $("#tblPaymentProfile tbody td").css({ "font-size": "12px" });
                }
            });
            $("#tblDDOperation_wrapper .row-fluid .span6:eq(0)").addClass("span4");
            $("#tblDDOperation_wrapper .row-fluid .span6:eq(1)").addClass("span8");
            $("#tblDDOperation_wrapper .row-fluid .span6").removeClass("span6");

            var DownloadAnchor = "<a id=\"downloadAnchor\" style=\"display:none;\"></a>";
            var btnExportHistory = "<button class=\"btn blue\" id=\"btnUpload\" name=\"btnDownload\">Download to Excel</button>";
            $("#tblDDOperation_wrapper .row-fluid .span4").append(DownloadAnchor + btnExportHistory).show();
        },
        error: function (feedback) {
            $("#tblDDOperationContainer").css({ "background-image": "none" }).html("").append("Data's Not Available.").show();
        }
    });
}

//05-Feb-2019 : Moorthy : Added for auto Topup Report
function DownloadAutoTopupReport(value){
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    var url = '/Download/DownloadAutoTopupReport'
    var JSONSendData = {
        date: value
    };
    $.ajax
    ({
        url: url,
        type: 'get',
        dataType: 'json',
        data: JSONSendData,
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#tblDDOperationContainer").show();
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {
            if (feedback.errcode == 0) {
                $("#ajax-screen-masking").hide();
                $("#tblDDOperationContainer").css({ "background-image": "none" });
                var IFrameData = $('<iframe/>', { id: 'downloadIFrame', src: '/Download/DownloadFinanceAutoTopupReport?id=' + feedback.Text, style: 'display:none' });
                $("#ajax-screen-masking").append(IFrameData);
            }
        }
    });
}

//12-Feb-2019 : Moorthy : Added for auto Topup Report
function GetGeneralReport(value) {
    $("#tblDDOperationContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblDDOperation\"></table>");
    $("#tblDDOperationContainer").css({ "min-height": "100px", "background-repeat": "no-repeat", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var row = 0;
    var url = apiServer + "/api/FinViewstransaction";
    var JSONSendData = {
        paymentMode: "203",
        date: value
    };
    $.ajax
    ({
        url: url,
        type: "GET",
        data: JSONSendData,
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('#tblDDOperation').html('');
            $("#tblDDOperationContainer").css({ "background-image": "none" });
            $('#tblDDOperation').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: true,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Payment Profile PAYM found."
                },
                aoColumns: [
                    { mDataProp: "sitecode", sTitle: 'Site Code' },
                    { mDataProp: "created_date", sTitle: 'Created Date' },
                    { mDataProp: "account_id", sTitle: 'Account Id' },
                    { mDataProp: "reference_id", sTitle: 'Reference Id' },
                    { mDataProp: "totalamount", sTitle: 'Amount' },
                    { mDataProp: "payment_mode", sTitle: 'Payment Mode' },
                    { mDataProp: "payment_agent", sTitle: 'Payment Agent' },
                    { mDataProp: "product_code", sTitle: 'Product Code' },
                    { mDataProp: "service_type", sTitle: 'Service Type' },
                    { mDataProp: "currency", sTitle: 'Currency' },
                    { mDataProp: "subscriptionid", sTitle: 'Subscription Id' },
                    { mDataProp: "cc_no", sTitle: 'CC No' },
                    { mDataProp: "ipclient", sTitle: 'Client IP' },
                    { mDataProp: "email", sTitle: 'Email' },
                    { mDataProp: "status", sTitle: 'Status' },
                    { mDataProp: "error_code", sTitle: 'Err Code' },
                    { mDataProp: "error_msg", sTitle: 'Err Msg' },
                    { mDataProp: "sim_first_login", sTitle: 'First Login' },
                    { mDataProp: "ECI_Value", sTitle: 'ECI Value' },
                    //26-Aug-2019 : Moorthy added for retrieving SRD value
                    { mDataProp: "srd", sTitle: 'Scheme Reference Data' }
                ],
                fnDrawCallback: function () {
                    $("#tblPaymentProfile tbody td").css({ "font-size": "12px" });
                }
            });
            $("#tblDDOperation_wrapper .row-fluid .span6:eq(0)").addClass("span4");
            $("#tblDDOperation_wrapper .row-fluid .span6:eq(1)").addClass("span8");
            $("#tblDDOperation_wrapper .row-fluid .span6").removeClass("span6");

            var DownloadAnchor = "<a id=\"downloadAnchor\" style=\"display:none;\"></a>";
            var btnExportHistory = "<button class=\"btn blue\" id=\"btnUpload\" name=\"btnDownload\">Download to Excel</button>";
            $("#tblDDOperation_wrapper .row-fluid .span4").append(DownloadAnchor + btnExportHistory).show();
        },
        error: function (feedback) {
            $("#tblDDOperationContainer").css({ "background-image": "none" }).html("").append("Data's Not Available.").show();
        }
    });
}

//12-Feb-2019 : Moorthy : Added for General Report
function DownloadGeneralReport(value) {
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    var url = '/Download/DownloadGeneralReport'
    var JSONSendData = {
        date: value
    };
    $.ajax
    ({
        url: url,
        type: 'get',
        dataType: 'json',
        data: JSONSendData,
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#tblDDOperationContainer").show();
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {
            if (feedback.errcode == 0) {
                $("#ajax-screen-masking").hide();
                $("#tblDDOperationContainer").css({ "background-image": "none" });
                var IFrameData = $('<iframe/>', { id: 'downloadIFrame', src: '/Download/DownloadFinanceGeneralReport?id=' + feedback.Text, style: 'display:none' });
                $("#ajax-screen-masking").append(IFrameData);
            }
        }
    });
}

//Tabffreedatabundlereport Search
function getfreedatabundlereport(sitecode, from_date, to_date) {
    $("#tblDDOperationContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblDDOperation\"></table>");
    $("#tblDDOperationContainer").css({ "min-height": "100px", "background-repeat": "no-repeat", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var row = 0;
    var url = apiServer + "/api/FreeDataBundle";
    var JSONSendData = {
        Type: 4,
        sitecode: sitecode,
        from_date: from_date,
        to_date: to_date
    };
    $.ajax
    ({
        url: url,
        type: "POST",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('#tblDDOperation').html('');
            $("#tblDDOperationContainer").css({ "background-image": "none" });
            $('#tblDDOperation').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: true,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [[0, "desc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No records found."
                },
                aoColumns: [
                    { mDataProp: "mobileno", sTitle: "Mobile No" },
                    { mDataProp: "bundle_category", sTitle: "Bundle Category" },
                    { mDataProp: "bundle_name", sTitle: "Bundle Name" },
                    { mDataProp: "bundle_price", sTitle: "Bundle Price" },
                    { mDataProp: "logdate", sTitle: "Log Date" },
                    { mDataProp: "subs_user", sTitle: "Subscribed User" }
                ],
                fnDrawCallback: function () {
                    $("#tblDDOperation tbody td").css({ "font-size": "12px" });
                }
            });
            $("#tblDDOperation tbody td").css({ "font-size": "12px" });
        },
        error: function (feedback) {
            $("#tblDDOperationContainer").css({ "background-image": "none" }).html("").append("No records found.").show();
        }
    });
}

function downloadfreedatabundlereport(sitecode, from_date, to_date) {
    var i = 0;
    jQuery.support.cors = true;
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader1").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    $("#ajax-screen-masking #downloadIFrame").remove();
    var url = '/Download/DownloadFreeDataBundleReport';
    var JSONSendData = {
        calledby: "CRM",
        sitecode: sitecode,
        from_date: from_date,
        to_date: to_date
    };
    $.ajax
    ({
        url: url,
        type: 'get',
        dataType: 'json',
        data: JSONSendData,
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {

            if (feedback.errcode == 0) {
                $("#ajax-screen-masking").hide();
                var IFrameData = $('<iframe/>', { id: 'downloadIFrame', src: '/Download/DownloadFinanceFreeDataBundleReport?id=' + feedback.Text, style: 'display:none' });
                $("#ajax-screen-masking").append(IFrameData);
            }
        }
    });
}