﻿/* ===================================================== 
   *  Issue History Report Script 
   ----------------------------------------------------- */
//Issue History Report Search
function getComplaintshistory(date_fr, date_to, curuser) {
    $("#tblComplaintsHistoryContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblComplaintsHistory\"></table>");
    $("#tblComplaintsHistoryContainer").css({ "min-height": "100px", "background-repeat": "no-repeat", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var row = 0;
    var url = apiServer + "/api/ComplaintsHistory";
    var JSONSendData = {
        date_from: date_fr,
        date_to: date_to,
        cur_user: curuser
    };
    $.ajax
    ({
        url: url,
        type: "POST",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('#tblComplaintsHistory').html('');
            $("#tblComplaintsHistoryContainer").css({ "background-image": "none" });
            $('#tblComplaintsHistory').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                iDisplayLength: 10,
                bPaginate: true,
                bFilter: true,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [[0, "desc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No records found."
                },
                aoColumns: [
                     { mDataProp: "Ticket_id", sTitle: "TicketId" },
                       { mDataProp: "mobileno", sTitle: "Mobile Number" },
                       { mDataProp: "issue_generate_date_string", sTitle: "Issue Generated Date" },
                       { mDataProp: "Issue_Name", sTitle: "Ticket Type" },
                       { mDataProp: "Function_Name", sTitle: "Category" },
                       { mDataProp: "sub_category", sTitle: "Sub category" },
                       { mDataProp: "agent_name", sTitle: "Agent Name" },
                       { mDataProp: "eslated_user", sTitle: "Esclated User" },
                       { mDataProp: "descr", sTitle: "Description" },
                       { mDataProp: "issue_complete_date_string", sTitle: "Issue Completed Date" },
                       { mDataProp: "aging", sTitle: "Aging" },
                       //{ mDataProp: "status", sTitle: "Status" }
                       {
                           mDataProp: "Action", sTitle: "Action", bSortable: false, sWidth: 50,
                           fnRender: function (ob) {

                               var btnTitle = "Edit";
                               var actionURL = "";
                               actionURL = "<button id=\"EditComplaints\" class=\"btn blue\" Ticket_id=\"" + ob.aData.Ticket_id + "\" mobileno=\"" + ob.aData.mobileno + "\" issue_type=\"" + ob.aData.issue_type + "\" Issue_Function=\"" + ob.aData.Issue_Function + "\" Issue_Complaint=\"" + ob.aData.Issue_Complaint + "\" esclating_user_id=\"" + ob.aData.esclating_user_id + "\" descr=\"" + ob.aData.descr + "\" status_id=\"" + ob.aData.status_id + "\" fk_priority_id=\"" + ob.aData.fk_priority_id + "\" customer_name=\"" + ob.aData.customer_name + "\" product_code=\"" + ob.aData.product_code + "\" agent_userid=\"" + ob.aData.agent_userid + "\"  style=\"width:85px\" >" + ob.aData.status + "</button>";
                               return actionURL;
                           }
                       },
                       {
                           mDataProp: "Action2", sTitle: "View", bSortable: false, sWidth: 50,
                           fnRender: function (ob) {

                               var btnTitle = "View";
                               var actionURL = "";
                               actionURL = "<button id=\"ViewComplaints\" class=\"btn blue\" Ticket_id=\"" + ob.aData.Ticket_id + "\" mobileno=\"" + ob.aData.mobileno + "\" Issue_Name=\"" + ob.aData.Issue_Name + "\" Function_Name=\"" + ob.aData.Function_Name + "\" eslated_user=\"" + ob.aData.eslated_user + "\" descr=\"" + ob.aData.descr + "\" status=\"" + ob.aData.status + "\" sub_category=\"" + ob.aData.sub_category + "\" priority_name=\"" + ob.aData.priority_name + "\" customer_name=\"" + ob.aData.customer_name + "\" product_code=\"" + ob.aData.product_code + "\" agent_userid=\"" + ob.aData.agent_userid + "\"  style=\"width:85px\" >" + btnTitle + "</button>";
                               return actionURL;
                           }
                       }
                ],
                fnDrawCallback: function () {
                    $("#tblComplaintsHistory tbody td").css({ "font-size": "12px" });
                }
            });
            $("#tblComplaintsHistory tbody td").css({ "font-size": "12px" });
        },
        error: function (feedback) {
            $("#tblComplaintsHistoryContainer").css({ "background-image": "none" }).html("").append("No records found.").show();
        }
    });
}

//Issue History Report Download
function downloadComplaintshistory(date_fr, date_to) {
    var i = 0;
    jQuery.support.cors = true;
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader1").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    $("#ajax-screen-masking #downloadIFrame").remove();
    var url = '/Download/DownloadComplaintsHistory';
    var JSONSendData = {
        date_fr: date_fr,
        date_to: date_to
    };
    $.ajax
    ({
        url: url,
        type: 'get',
        dataType: 'json',
        data: JSONSendData,
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {

            if (feedback.errcode == 0) {
                $("#ajax-screen-masking").hide();
                var IFrameData = $('<iframe/>', { id: 'downloadIFrame', src: '/Download/DownloadComplaintsHistoryList?id=' + feedback.Text, style: 'display:none' });
                $("#ajax-screen-masking").append(IFrameData);
            }
        }
    });
}