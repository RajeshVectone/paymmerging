﻿//Fruad Block \ Unblock Screen

function BlockMsisdnIccid(Product, SiteCode, USERTYPE, USERINFO, REQTYPE, REASON_ID, FK_REQ_USERID) {
    var url = apiServer + "/api/FruadBlockandUnblock";
    jQuery.support.cors = true;
    var JSONSendData = {
        Product: Product,
        SiteCode: SiteCode,
        USERTYPE: USERTYPE,
        USERINFO: USERINFO,
        REQTYPE: REQTYPE,
        REASON_ID: REASON_ID,
        FK_REQ_USERID: FK_REQ_USERID,
        Block_Type: 2
    };
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            var errcode = feedback[0].errcode;
            if (errcode == 0) {
                if (REQTYPE == 1) {
                    $('#productListFinance').val("0");
                    $("#Singleblocking").val("");
                    $("#ddlBlockReason").val("0");
                }
                else {
                    $('#productListFinanceUnblock').val("0");
                    $("#Singleunblocking").val("");
                    $("#ddlUnBlockReason").val("0");
                }
                $('.PMSuccess .modal-header').html("Fruad Block \ UnBlock");
                $('.PMSuccess .modal-body').html(feedback[0].errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
            else {
                $('.PMFailed .modal-header').html("Fruad Block \ UnBlock");
                $('.PMFailed .modal-body').html(feedback[0].errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (err) {
            $("#ajax-screen-masking").hide();
        }
    });
}

function GetBlockUnblockRequest(FK_REQ_USERID, user_role, role_id) {
    $("#tblBlockUnBlockReportsContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblBlockUnBlockReports\"></table>");
    $("#tblBlockUnBlockReportsContainer").css({ "overflow": "auto", "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });

    var url = apiServer + "/api/FruadBlockandUnblock";
    jQuery.support.cors = true;
    var JSONSendData = {
        FK_REQ_USERID: FK_REQ_USERID,
        role_id: role_id,
        Block_Type: 3
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#btnBlockUnBlockReportsSettingContainer").css({ "background-image": "none" });
            $("#tblBlockUnBlockReportsContainer").css({ "background-image": "none" });
            $('#tblBlockUnBlockReports').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                iDisplayLength: 10,
                bPaginate: true,
                bFilter: true,
                bInfo: false,
                aaSorting: [],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Block and UnBlock Records Found "
                },
                aoColumns: [
                    { mDataProp: "USERINFO", sTitle: "User Info" },
                    { mDataProp: "REQ_TYE", sTitle: "Request Type" },
                    { mDataProp: "REQ_DATE_string", sTitle: "Request Date" },
                    { mDataProp: "PRODUCT", sTitle: "Product" },
                    { mDataProp: "REASON", sTitle: "Reason" },
                    { mDataProp: "REQ_STATUS", sTitle: "Request Status" },
                    { mDataProp: "REQ_BY", sTitle: "Request By" },
                    { mDataProp: "APPROVED_BY", sTitle: "Approved By" },
                    { mDataProp: "APPROVE_DATE_string", sTitle: "Approved Date" },
                    {
                        mDataProp: "REQID", sTitle: "Action", bSortable: false, //"bVisible": user_role= 'Admin' ? true : false,
                        fnRender: function (ob) {
                            var actionURL = "";
                            if (user_role == 'Admin') {
                                if (ob.aData.APPROVED_BY != undefined && ob.aData.APPROVED_BY.trim() != "" && user_role == 'Admin') {
                                    actionURL = "<button class=\"btn lightgrey btnApprove\" disabled=\"disabled\" >Approve</button>";
                                }
                                else {
                                    actionURL = "<button class=\"btn blue btnApprove\" ctlValue=\"" + ob.aData.REQID + "\">Approve</button>";
                                }
                            }
                            else
                                actionURL = "<button class=\"btn lightgrey btnApprove\" disabled=\"disabled\" >Approve</button>";
                             return actionURL;
                         }
                     },
                ],
                fnDrawCallback: function () {
                    $("#tblBlockUnBlockReports tbody td").css({ "font-size": "12px" });
                }
            });
            $("#tblDDOperation tbody td").css({ "font-size": "12px" });
        },
        error: function (feedback) {
            $("#TabProductPackagesLoader").hide();
            $("#tblProductPackagesContainer").html("").append("No data found").show();
        }
    });
}

function DoBlockConfirmation(loginuser_id, role_id, REQID, loginuser_name) {
    var url = apiServer + "/api/FruadBlockandUnblock";
    jQuery.support.cors = true;
    var JSONSendData = {
        FK_REQ_USERID: loginuser_id,
        role_id: role_id,
        REQID: REQID,
        loginuser_name: loginuser_name,
        Block_Type: 4
    };
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            var errcode = feedback[0].errcode;
            if (errcode == 0) {
                $('.PMSuccess .modal-header').html("Block \ UnBlock");
                $('.PMSuccess .modal-body').html(feedback[0].errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
                setTimeout(function () { GetBlockUnblockRequest(loginuser_id, role_id); }, 100);
            }
            else {
                $('.PMFailed .modal-header').html("Block \ UnBlock");
                $('.PMFailed .modal-body').html(feedback[0].errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (err) {
            $("#ajax-screen-masking").hide();
        }
    });
}
