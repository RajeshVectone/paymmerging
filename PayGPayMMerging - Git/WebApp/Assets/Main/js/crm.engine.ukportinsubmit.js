﻿/* ===================================================== 
   *  UK PORT IN - SUBMIT
   ----------------------------------------------------- */

/* ----------------------------------------------------------------
 * Function Name    : UKPortINSubmitValidate
 * Purpose          : to validate Vectone MSISDN
 * Added by         : Edi Suryadi
 * Create Date      : September 11th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function UKPortINSubmitValidate(Msisdn, LastDigitIccid) {

    var url = apiServer + '/api/ukporting/';

    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    jQuery.support.cors = true;
    var JSONSendData = {
        Msisdn: Msisdn,
        LastDigitIccid: LastDigitIccid,
        Sitecode : "MCM",
        SubType: 1
    };

    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();

            var stat = data.errcode;
            if (stat == 0) {
                $("#UKPortINSubmitStep2_iVectoneMSISDN").text($("input[name=UKPortINSubmitStep1_iMSISDN]").val());
                $("#UKPortINSubmitStep1").hide();
                $("#UKPortINSubmitStep2").fadeIn();
            } else {
                $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "2" });
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (data) {
            $("#ajax-screen-masking").hide();
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            $('.PMFailed .modal-header').html(data.errsubject);
            $('.PMFailed .modal-body').html(data.errmsg);
            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : UKPortINSubmitDoSubmit
 * Purpose          : to submit the port in request
 * Added by         : Edi Suryadi
 * Create Date      : September 11th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function UKPortINSubmitDoSubmit(Msisdn, LastDigitIccid, ContactName, ContactDetail, PAC, PortinMsisdn, PortDate, LoggedUser) {

    var url = apiServer + '/api/ukporting/';

    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    jQuery.support.cors = true;
    var JSONSendData = {
        Msisdn: Msisdn,
        LastDigitIccid: LastDigitIccid,
        ContactName: ContactName,
        ContactDetail: ContactDetail,
        PAC: PAC,
        PortinMsisdn: PortinMsisdn,
        PortDate: PortDate,
        LoggedUser : LoggedUser,
        sitecode: "MCM",
        SubType: 2
    };

    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();

            var stat = data.errcode;
            if (stat == 0) {
                $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
                $('.PMSuccess .modal-header').html(data.errsubject);
                $('.PMSuccess .modal-body').html(data.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            } else {
                $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "2" });
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (data) {
            $("#ajax-screen-masking").hide();
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            $('.PMFailed .modal-header').html(data.errsubject);
            $('.PMFailed .modal-body').html(data.errmsg);
            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });
}



/* ----------------------------------------------------- 
   *  eof UK PORT IN - SUBMIT
   ===================================================== */






