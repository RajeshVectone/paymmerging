﻿$(document).ready(function () {

    $("[name=LoginName]").attr("placeholder", "Username");
    $("[name=Password]").attr("placeholder", "Password");

    /* LOGIN BUTTON AT LOGIN PAGE */
    $("#btnLogin").live("click", function () {
        /* Get the input data */
        var LoginName = $("[name=LoginName]").val();
        var Password = $("[name=Password]").val();
        /* Set type of message and message title */
        var btnAlert = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&nbsp;</button>";
        titleAlert = "<h4><strong>Login Failure</strong></h4>";

        /* Remove inner div of message if exists */
        if ($("#lmsg").length > 0) {
            $("#lmsg").remove();
        }

        /* Check if username and password empty */
        if ($.trim(LoginName) == "" || $.trim(Password) == "") {
            /* Create inner div of message */
            $(".lmessage").append("<div id=\"lmsg\"></div>");
            /* Write a message */
            $("#lmsg").addClass("alert alert-error").append(btnAlert + titleAlert + "Username or password can not be empty...").delay(7000).fadeOut(500);
            /* Set the focus of cursor to the username */
            $("[name=LoginName]").val("").focus();
        } else {
            $("#flogin").submit();
        }
    });
});