﻿/* ----------------------------------------------------------------
 * Function Name    : UploadFile
 * Purpose          : upload file into server
 * Added by         : Harry Ramdhani
 * Create Date      : December 3rd, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function UploadFile(p1, p2, p3) {

    if ($('[name="somefile"]').val() != '') {

        var bar = $('.bar');
        var percent = $('.percent');
        var status = $('#status');

        $("#uploadForm").ajaxSubmit({
            url: apiServer + '/api/exupload/upload',
            type: 'post',
            dataType: 'json',
            data: {
                requesttype: 1,
                sitecode: p1,
                uploadProfileName: p2,
                webuser: p3
            },
            beforeSend: function () {
                status.empty();
                var percentVal = '0%';
                bar.width(percentVal);
                percent.html(percentVal);
            },
            uploadProgress: function (event, position, total, percentComplete) {
                var percentVal = percentComplete + '%';
                bar.width(percentVal);
                percent.html(percentVal);
            },
            complete: function (xhr) {
                status.html(xhr.responseText);
                status.hide();
            },
            success: function (msg) {
                var stat = msg[0].errcode;
                if (stat == 0) {
                    $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
                    $('.PMSuccess .modal-header').html(msg[0].errsubject);
                    $('.PMSuccess .modal-body').html(msg[0].errmsg);
                    $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                        'margin-left': function () {
                            return window.pageXOffset - ($(this).width() / 2);
                        }
                    });
                    // console.log(msg);
                }
                else {
                    $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
                    $('.PMFailed .modal-header').html(msg[0].errsubject);
                    $('.PMFailed .modal-body').html(msg[0].errmsg);
                    $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                        'margin-left': function () {
                            return window.pageXOffset - ($(this).width() / 2);
                        }
                    });
                    // console.log(msg);
                    //$("#tblFormUpload").hide(function () {
                    //    var errcode = msg[0].errcode,
                    //        errsubject = msg[0].errsubject,
                    //        errmsg = msg[0].errmsg,
                    //        prefix = msg[0].prefix;
                    //    $('#lbErrSubject').html(errsubject);
                    //    $('#lbErrCode').html(errcode);
                    //    $('#lbErrMessage').html(errmsg);
                    //    $('#lbPrefix').html(prefix);
                    //    $("#tblFormResult").show();
                    //});                    
                }
            }
        });
    }
    else {
        alert('can\'t empty');
    }
}
