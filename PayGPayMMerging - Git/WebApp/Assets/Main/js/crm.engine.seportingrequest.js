﻿/* ===================================================== 
   *  SE PORTING REQUEST
   ----------------------------------------------------- */
/* ----------------------------------------------------------------
 * Function Name    : SEPortINNewRequestValidateInput
 * Purpose          : to validate input
 * Added by         : Edi Suryadi
 * Create Date      : January 16th, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */

function SEPortINNewRequestValidateInput() {

    var passed = true;

    /* Validate Recipient Network Operator */
    var RecipientNetworkOperator = $("input[name=SEPortINRequest_sRecipientNetworkOperator]").val();
    if (RecipientNetworkOperator == "") {
        passed = false;
    }

    /* Validate Recipient Service Provider */
    var RecipientServiceProvider = $("input[name=SEPortINRequest_sRecipientServiceProvider]").val();
    if (RecipientServiceProvider == "") {
        passed = false;
    }

    /* Validate Email */
    var Email = $("input[name=SEPortINRequest_iEmail]").val();
    if (validateEmail(Email) == false && Email != "") {
        //$("span#err_SEPortINRequest_iEmail").fadeIn();
        passed = false;
    } else {
        //$("span#err_SEPortINRequest_iEmail").hide();
    }

    return passed;
}

/* ----------------------------------------------------------------
 * Function Name    : SEPortINNewRequestSubmit
 * Purpose          : to submit the port in request
 * Added by         : Edi Suryadi
 * Create Date      : January 16th, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function SEPortINNewRequestSubmit(p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17) {
    var url = apiServer + '/api/nlporting';

    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    jQuery.support.cors = true;
    var JSONSendData = {
        rsp: p1,
        rno: p2,
        dsp: p3,
        dno: p4,
        requestdate: p5,
        CustId: p6,
        Initials: p7,
        Prefix: p8,
        ContactName: p9,
        CompanyName: p10,
        Street: p11,
        Houseno: p12,
        HouseNoExt: p13, 
        Zipcode: p14,
        City: p15,
        Email: p16, 
        TelpNo: p17,
        //NoteOfMsg : "", // Hardcode from backend October 10th, 2013 - 13:01
        IsResent : "false",
        sitecode: "BNL",
        SubType: 2
    };

    //window.console.log(JSONSendData);

    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();

            var stat = data.errcode;
            if (stat == 0) {
                $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
                $('.PMSuccess .modal-header').html(data.errsubject);
                $('.PMSuccess .modal-body').html(data.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            } else {
                $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "2" });
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (data) {
            $("#ajax-screen-masking").hide();
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            $('.PMFailed .modal-header').html(data.errsubject);
            $('.PMFailed .modal-body').html(data.errmsg);
            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : NLPortINGetDonorServiceProviderList
 * Purpose          : to get donor service provider list for combobox
 * Added by         : Edi Suryadi
 * Create Date      : January 15th, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function NLPortINGetDonorServiceProviderList() {
    $("select[name=SEPortINRequest_sDonorServiceProvider]").html("").attr("disabled", true);

    var url = apiServer + '/api/nlporting';
    jQuery.support.cors = true;
    var JSONSendData = {
        OptType: "SP", // SP = Service Provider ; NO = Network Operator
        ProviderCode: "", // null to get Service provider list.To get Network Operator,please provide provider code
        sitecode: "BNL",
        SubType: 100
    };
    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (data) {
            var optionContent = "";
            for (i = 0; i < data.length; i++) {
                optionContent += "<option value=\"" + data[i].Name + "\">" + data[i].FullName + "</option>";
            }
            
            $("select[name=SEPortINRequest_sDonorServiceProvider]").append(optionContent);
            $("select[name=SEPortINRequest_sDonorServiceProvider] option[value=ACHT]").attr("selected", true);
        },
        complete: function (data) {
            $("select[name=SEPortINRequest_sDonorServiceProvider]").removeAttr("disabled");
            $("img#SEPortINRequest_sDonorServiceProvider_Loader").hide();
        }
    });
}

/* ----------------------------------------------------- 
   *  eof SE PORTING REQUEST
   ===================================================== */






