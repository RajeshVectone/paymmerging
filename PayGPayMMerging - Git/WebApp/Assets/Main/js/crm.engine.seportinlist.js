﻿/* ===================================================== 
   *  SE PORT IN - LIST
   ----------------------------------------------------- */

/* ----------------------------------------------------------------
 * Function Name    : SEPortINList
 * Purpose          : to display porting list
 * Added by         : Edi Suryadi
 * Create Date      : January 16th, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function SEPortINList(Msisdn, MsgIdentifier, PortingDateFrom, PortingDateTo, StateID) {

    $("#SEPortINList_TblListContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"SEPortINList_TblList\"></table>");
    $('#SEPortINList').height(220);

    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    var url = apiServer + '/api/nlporting';
    jQuery.support.cors = true;
    var JSONSendData = {
        sitecode: "BNL",
        SubType: 102,
        Msisdn :"",     
        MsgIdentifier : "",     
        PortingDateFrom: "01/Jan/2014",
        PortingDateTo: "16/Jan/2014",
        StateId: ""
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function (feedback) {
            $("#ajax-screen-masking").show();
            $('#SEPortINList_TblListContainer').hide();
        },
        success: function (feedback) {
            var oTable = $('#SEPortINList_TblList').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: true,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [[0, "desc"]],
                oLanguage: {
                    "sSearch": "Filter: "
                },
                aoColumns: [
                    { mDataProp: "portingId", sTitle: "Porting ID" },
                    { mDataProp: "RequestDate", sTitle: "MSISDN" },
                    { mDataProp: "RequestDate", sTitle: "Vectone MSISDN" },
                    { mDataProp: "actualdate", sTitle: "Requested Date" },
                    { mDataProp: "actualdate", sTitle: "Porting Date" },
                    { mDataProp: "networkprovider", sTitle: "Subscriber Name" },
                    { mDataProp: "Initial", sTitle: "Contact Phone" },
                    { mDataProp: "Prefix", sTitle: "Email" },
                    { mDataProp: "LastName", sTitle: "Port Status Description" },
                    { mDataProp: "TelpNoSerieStart", sTitle: "Last Update" },
                    {
                        mDataProp: "requestdate", sTitle: "Action", sWidth: "200px",
                        fnRender: function (ob) {
                            var actionURL = "";
                            actionURL = "<button data-mid=\"" + ob.aData.MsgIdentifier + "\" data-ack=\"" + ob.aData.statusack + "\" class=\"btn blue SEPortINList_btnViewEdit\" style=\"float:left;width:80px\">View/Edit</button>";
                            return actionURL;
                        }
                    }
                ]
            });
            
            $("#SEPortINList_TblList_wrapper .row-fluid .span6:eq(0)").css({"font-size" : "16px"}).html("Result : <b>" + oTable.fnSettings().fnRecordsTotal() + "</b>");
            $("#SEPortINList_TblList_wrapper .row-fluid .span6:eq(2)").addClass("span1");
            $("#SEPortINList_TblList_wrapper .row-fluid .span6:eq(3)").addClass("span11");
            $("#SEPortINList_TblList_wrapper .row-fluid:eq(1) .span6").removeClass("span6");
            var SEPortINListHeight = $("#SEPortINList").outerHeight();
            var SEPortINListTableHeight = $("#SEPortINList_TblListContainer").outerHeight();
            if (navigator.appVersion.indexOf("Chrome/") != -1) {
                $("#SEPortINList").height(SEPortINListHeight + SEPortINListTableHeight + 60);
            } else {
                $("#SEPortINList").height(SEPortINListHeight + SEPortINListTableHeight + 30);
            }
            $("#SEPortINList tbody td").css({"font-size" : "12px"});
            $("#ajax-screen-masking").hide();
            $("#SEPortINList_TblListContainer").show();

        }
    });
}

/* ----------------------------------------------------- 
   *  eof SE PORT IN - LIST
   ===================================================== */






