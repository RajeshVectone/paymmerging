﻿//26-Dec-2018 : Moorthy : Addded for SMS module
function ViewCustomerSMS(MobileNo, SiteCode, ProductCode) {
    $("#SMSHistoryContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblSMSHistoryContainer\"></table>");
    var url = apiServer + "/api/SMSCampaign";
    var JSONSendData = {
        Action: 1,
        MobileNo: MobileNo,
        SiteCode: SiteCode,
        ProductCode: ProductCode
    };
    $.ajax
    ({
        url: url,
        type: "POST",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('#tblSMSHistoryContainer').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: true,
                bInfo: false,
                iDisplayLength: 5,
                aaSorting: [],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No SMS history found."
                },
                aoColumns: [
                { mDataProp: "sms_sent_date_string", sTitle: "SMS Date" },
                {mDataProp: "sms_text", sTitle: "Text"},
                { mDataProp: "sms_status", sTitle: "Status" },
                { mDataProp: "crm_user", sTitle: "User" },

                ],
                fnDrawCallback: function () {
                    $("#tblSMSHistoryContainer tbody td").css({ "font-size": "12px" });
                }
            });
        },
        error: function (feedback) {
            $("#SMSHistoryContainer").html("").append("No SMS history found.").show();
        }
    });
}

function SendSMS(MobileNo, SiteCode, ProductCode, SMSText, LoggedUser, templateID, customer_name) {
    $("#ajax-screen-masking").show();
    var url = apiServer + "/api/SMSCampaign";
    var JSONSendData = {
        Action: 2,
        MobileNo: MobileNo,
        SiteCode: SiteCode,
        ProductCode: ProductCode,
        SMSText: SMSText,
        LoggedUser: LoggedUser,
        smsType: templateID,
        customer_name: customer_name
    };
    $.ajax
    ({
        url: url,
        type: "POST",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            alert(feedback[0].errmsg);
            if (feedback[0].errcode == 0)
                location.reload();
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}

//Fill SMS Template
function FillSMSTemplate() {
    $("#SendSMSLoader").show();
    $("#ddlSMSTemplate").find('option').remove();
    var url = apiServer + '/api/EmailSettings';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 3
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#SendSMSLoader").hide();
            if (feedback != null && feedback.length > 0) {
                var actionOption = '<option ctlValue="" value="-1" style=\"padding:3px\">-- Select --</option>';
                for (icount = 0; icount < feedback.length; icount++) {
                    actionOption += '<option ctlValue="' + feedback[icount].sms_text + '"   value="' + feedback[icount].template_id + '"  Templatename="' + feedback[icount].template_name + '"  style=\"padding:3px\">' + feedback[icount].template_name + '</option>';
                }
                $("#ddlSMSTemplate").append(actionOption);
            }
        },
        error: function (feedback) {
            $("#SendSMSLoader").hide();
        }
    });
}