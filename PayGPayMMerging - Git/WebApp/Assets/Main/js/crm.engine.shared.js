﻿/* ----------------------------------------------------------------
 * Function Name    : CustomerInfoHeader
 * Purpose          : to show Customer Info
 * Added by         : Edi Suryadi
 * Create Date      : September 04th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function CustomerInfoHeader(productCode, searchText, Sitecode, CustIDs, ConnStatus) {
    var SIM_Type = 'PAYG';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1) {
        SIM_Type = 'PAYM';
    }
    //02-Jan-2019 : Moorthy : Modified to avoid hitting API  
    //var url = apiServer + '/api/customersearch';
    var url = '/Customer/CustomerDetailInfo';
    var JSONSendData = {
        Product_Code: productCode,
        SearchBy: 'MobileNo',
        SearchText: searchText,
        Sitecode: Sitecode,
        Search_Type: 2,
        SIM_Type: SIM_Type
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#tblCustomerInfoHeaderLoader").hide();
            $("#tblCustomerInfoHeader").show();
            var errcode = -1;
            if (feedback != '' && feedback != null) {
                errcode = feedback.errcode;
            }
            if (errcode == 0) {
                $("table#tblCustomerInfoHeader td#tdCIHBrand").html(feedback.product);
                $("table#tblCustomerInfoHeader td#tdCIHMobileNo").html(feedback.MobileNo);

                $("table#tblCustomerInfoHeader td#tdCIHSubscriberId").html(feedback.SubscriberID);

                //29-Jan-2016 : Moorthy : modified to show the customer id
                //$("table#tblCustomerInfoHeader td#tdCIHCustomerId").html(CustIDs);
                if (feedback.CustomerID == 0) {
                    feedback.CustomerID = "0";
                }

                $("table#tblCustomerInfoHeader td#tdCIHCustomerId").html(feedback.CustomerID);
                $("table#tblCustomerInfoHeader td#tdCIHCustomerName").html(feedback.first_name + " " + feedback.last_name);
                $("table#tblCustomerInfoHeader td#tdCIHSIMStatus").html(feedback.Status);
                $("table#tblCustomerInfoHeader td#tdCIHICCID").html(feedback.ICCID);

                //06-Feb-2016 : Moorthy : added for connection status issue
                //feedback.ConnectionStatus = ConnStatus;
                if (feedback.ConnectionStatus == null) {
                    feedback.ConnectionStatus = "";
                }
                //29-Jan-2016 : Moorthy : balance should be zero for non active connection status
                if (feedback.ConnectionStatus.toLowerCase() == "not active" || feedback.ConnectionStatus.toLowerCase() == "inactive")
                    feedback.balance_string = "0.00";
                $("table#tblCustomerInfoHeader td#tdCIHBalance").html(getCurrencySymbol(feedback.currency) + " " + feedback.balance_string);
                
                //$("table#tblCustomerInfoHeader td#tdCIHActivateDate").html(feedback.active_date_string);
                $("table#tblCustomerInfoHeader td#tdCIHSIMType").html(convertNulltoNA(feedback.SimType));

                //29-Jan-2016 : Moorthy : modified to show the connection status
                //$("table#tblCustomerInfoHeader td#tdCIHConnectionStatus").html(ConnStatus);
                $("table#tblCustomerInfoHeader td#tdCIHConnectionStatus").html(feedback.ConnectionStatus);

                if ($("input[name=iSIMSwapCurrentAddress]").length) {
                    var currentAddress = feedback.Houseno + " " + feedback.address1 + " " + feedback.postcode;
                    $("input[name=iSIMSwapCurrentAddress]").val(currentAddress);

                    $("input[name=iSIMSwapCurrentAddressFirstName]").val(feedback.first_name);
                    $("input[name=iSIMSwapCurrentAddressLastName]").val(feedback.last_name);
                    $("input[name=iSIMSwapCurrentAddressAddress1]").val(feedback.address1);
                    $("input[name=iSIMSwapCurrentAddressAddress2]").val(feedback.address2);
                    $("input[name=iSIMSwapCurrentAddressCity]").val(feedback.city);
                    $("input[name=iSIMSwapCurrentAddressPostcode]").val(feedback.postcode);

                    //31-Dec-2018 : Moorthy : Added for Sim Order
                    $("input[name=iSIMSwapCurrentAddressHouseNo]").val(feedback.Houseno);
                    $("input[name=iSIMSwapCurrentAddressEmail]").val(feedback.Email);
                }
                //Added : 22-Dec-2018 : CRM Enhancement
                $("table#tblCustomerInfoHeader td#tdCIHBonus").html(getCurrencySymbol(feedback.currency) + " " + feedback.bonus_balance);
                if (feedback.first_name == "" || feedback.last_name == "" || feedback.Email == "" || feedback.BirthDate_String == "" || feedback.Houseno == "" || feedback.address1 == "" || feedback.city == "" || feedback.postcode == "" || feedback.securityanswer == "" || feedback.Telephone == "") {
                    $("#detailMoreInfo").html("More Info<span style='color:red;font-weight:bold'> !</span>");
                    $("#detailPAYMMoreInfo").html("More Info<span style='color:red;font-weight:bold'> !</span>");
                }
                //Added : 22-Dec-2018 : CRM Enhancement
                if (feedback.vectone_app == 1) {
                    $("#tdCIHVapp").show();
                    $("#thCIHVapp").show();
                }
                else {
                    $("#tdCIHVapp").hide();
                    $("#thCIHVapp").hide();
                }
            } else {
                //Added : 22-Dec-2018 : CRM Enhancement
                $("#detailMoreInfo").html("More Info<span style='color:red;font-weight:bold'> !</span>");
                $("#detailPAYMMoreInfo").html("More Info<span style='color:red;font-weight:bold'> !</span>");

                $("table#tblCustomerInfoHeader td#tdCIHBrand").html("");
                $("table#tblCustomerInfoHeader td#tdCIHMobileNo").html("");
                $("table#tblCustomerInfoHeader td#tdCIHSubscriberId").html("");
                $("table#tblCustomerInfoHeader td#tdCIHCustomerId").html("");
                $("table#tblCustomerInfoHeader td#tdCIHCustomerName").html("");
                $("table#tblCustomerInfoHeader td#tdCIHSIMStatus").html("");
                $("table#tblCustomerInfoHeader td#tdCIHICCID").html("");
                $("table#tblCustomerInfoHeader td#tdCIHBalance").html("");
                //$("table#tblCustomerInfoHeader td#tdCIHActivateDate").html("");
                $("table#tblCustomerInfoHeader td#tdCIHSIMType").html("");
                $("table#tblCustomerInfoHeader td#tdCIHConnectionStatus").html("");
                //Added : 22-Dec-2018 : CRM Enhancement
                $("table#tblCustomerInfoHeader td#tdCIHBonus").html("");
                $("#tdCIHVapp").hide();
                $("#thCIHVapp").hide();
            }
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : RefreshCustomerInfoHeader
 * Purpose          : to refresh Customer Info
 * Added by         : Edi Suryadi
 * Create Date      : December 31th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function RefreshCustomerInfoHeader(productCode, searchText, Sitecode, CustIDs, ConnStatus) {
    var SIM_Type = 'PAYG';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1) {
        SIM_Type = 'PAYM';
    }
    $("#tblCustomerInfoHeaderLoader").show();
    $("#tblCustomerInfoHeader").hide();
    $("#tblCustomerInfoHeader tbody td").html("");

    //02-Jan-2019 : Moorthy : Modified to avoid hitting API  
    //var url = apiServer + '/api/customersearch';
    var url = '/Customer/CustomerDetailInfo';

    jQuery.support.cors = true;
    var JSONSendData = {
        Product_Code: productCode,
        SearchBy: 'MobileNo',
        SearchText: searchText,
        Sitecode: Sitecode,
        Search_Type: 2,
        SIM_Type: SIM_Type
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#tblCustomerInfoHeaderLoader").hide();
            $("#tblCustomerInfoHeader").show();
            var errcode = -1;
            if (feedback != '' && feedback != null) {
                errcode = feedback.errcode;
            }
            if (errcode == 0) {
                $("table#tblCustomerInfoHeader td#tdCIHBrand").html(feedback.product);
                $("table#tblCustomerInfoHeader td#tdCIHMobileNo").html(feedback.MobileNo);
                $("table#tblCustomerInfoHeader td#tdCIHCustomerId").html(CustIDs);
                $("table#tblCustomerInfoHeader td#tdCIHCustomerName").html(feedback.first_name + " " + feedback.last_name);
                $("table#tblCustomerInfoHeader td#tdCIHSIMStatus").html(feedback.Status);
                $("table#tblCustomerInfoHeader td#tdCIHICCID").html(feedback.ICCID);
                $("table#tblCustomerInfoHeader td#tdCIHBalance").html(getCurrencySymbol(feedback.currency) + " " + feedback.balance_string);
                //$("table#tblCustomerInfoHeader td#tdCIHActivateDate").html(feedback.active_date_string);
                $("table#tblCustomerInfoHeader td#tdCIHSIMType").html(convertNulltoNA(feedback.SimType));
                $("table#tblCustomerInfoHeader td#tdCIHConnectionStatus").html(ConnStatus);
                feedback.ConnectionStatus

                if ($("input[name=iSIMSwapCurrentAddress]").length) {
                    var currentAddress = feedback.Houseno + " " + feedback.address1 + " " + feedback.postcode;
                    $("input[name=iSIMSwapCurrentAddress]").val(currentAddress);

                    $("input[name=iSIMSwapCurrentAddressFirstName]").val(feedback.first_name);
                    $("input[name=iSIMSwapCurrentAddressLastName]").val(feedback.last_name);
                    $("input[name=iSIMSwapCurrentAddressAddress1]").val(feedback.Houseno + " " + feedback.address1);
                    $("input[name=iSIMSwapCurrentAddressAddress2]").val(feedback.address2);
                    $("input[name=iSIMSwapCurrentAddressCity]").val(feedback.city);
                    $("input[name=iSIMSwapCurrentAddressPostcode]").val(feedback.postcode);
                }

                //Added : 22-Dec-2018 : CRM Enhancement
                $("table#tblCustomerInfoHeader td#tdCIHBonus").html(getCurrencySymbol(feedback.currency) + " " + feedback.bonus_balance);
                if (feedback.first_name == "" || feedback.last_name == "" || feedback.Email == "" || feedback.BirthDate_String == "" || feedback.Houseno == "" || feedback.address1 == "" || feedback.city == "" || feedback.postcode == "" || feedback.securityanswer == "" || feedback.Telephone == "") {
                    $("#detailMoreInfo").html("More Info<span style='color:red;font-weight:bold'> !</span>");
                    $("#detailPAYMMoreInfo").html("More Info<span style='color:red;font-weight:bold'> !</span>");
                }
                //Added : 22-Dec-2018 : CRM Enhancement
                if (feedback.vectone_app == 1) {
                    $("#tdCIHVapp").show();
                    $("#thCIHVapp").show();
                }
                else {
                    $("#tdCIHVapp").hide();
                    $("#thCIHVapp").hide();
                }

            } else {
                //Added : 22-Dec-2018 : CRM Enhancement
                $("#detailMoreInfo").html("More Info<span style='color:red;font-weight:bold'> !</span>");
                $("#detailPAYMMoreInfo").html("More Info<span style='color:red;font-weight:bold'> !</span>");

                $("table#tblCustomerInfoHeader td#tdCIHBrand").html("");
                $("table#tblCustomerInfoHeader td#tdCIHMobileNo").html("");
                $("table#tblCustomerInfoHeader td#tdCIHCustomerId").html("");
                $("table#tblCustomerInfoHeader td#tdCIHCustomerName").html("");
                $("table#tblCustomerInfoHeader td#tdCIHSIMStatus").html("");
                $("table#tblCustomerInfoHeader td#tdCIHICCID").html("");
                $("table#tblCustomerInfoHeader td#tdCIHBalance").html("");
                //$("table#tblCustomerInfoHeader td#tdCIHActivateDate").html("");
                $("table#tblCustomerInfoHeader td#tdCIHSIMType").html("");
                $("table#tblCustomerInfoHeader td#tdCIHConnectionStatus").html("");

                //Added : 22-Dec-2018 : CRM Enhancement
                $("table#tblCustomerInfoHeader td#tdCIHBonus").html("");

                //Added : 22-Dec-2018 : CRM Enhancement
                $("#tdCIHVapp").hide();
                $("#thCIHVapp").hide();
            }
        }
    });
}
