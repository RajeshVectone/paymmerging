﻿/* ===================================================== 
   *  TAB GENERAL 
   ----------------------------------------------------- */

/* ----------------------------------------------------------------
 * Function Name    : GeneralInfo
 * Purpose          : to show general customer Information
 * Added by         : Edi Suryadi
 * Create Date      : September 04th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function GeneralInfo(productCode, searchText, Sitecode) {
    //Customer KYC details should be mandatory - error sign if not filled in
    $("#aTabGeneral").html("General")
    $("#aTabGeneral").prop('title', '');
    var SIM_Type = 'PAYG';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1) {
        SIM_Type = 'PAYM';
    }

    var url = apiServer + '/api/customersearch';

    jQuery.support.cors = true;
    var JSONSendData = {
        Product_Code: productCode,
        SearchBy: 'MobileNo',
        SearchText: searchText,
        Sitecode: Sitecode,
        Search_Type: 2,
        SIM_Type: SIM_Type
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#TabGeneralLoader").hide();
            $("#tblCustomerGeneralInfoContainer").show();
            var errcode = -1;
            if (feedback != '' && feedback != null) {
                errcode = feedback.errcode;
            }

            if (errcode == 0) {
                if (feedback.first_name != null)
                    $("table#tblCustomerGeneralInfo td#tdCGIFirstName").html(capitalizeString(feedback.first_name));
                else
                    $("table#tblCustomerGeneralInfo td#tdCGIFirstName").html("");
                if (feedback.last_name != null)
                    $("table#tblCustomerGeneralInfo td#tdCGILastName").html(capitalizeString(feedback.last_name));
                else
                    $("table#tblCustomerGeneralInfo td#tdCGILastName").html("");
                if (feedback.Email != null)
                    $("table#tblCustomerGeneralInfo td#tdCGIEmail").html(feedback.Email);
                else
                    $("table#tblCustomerGeneralInfo td#tdCGIEmail").html("");
                if (feedback.BirthDate_String != null)
                    $("table#tblCustomerGeneralInfo td#tdCGIBirthDate").html(feedback.BirthDate_String);
                else
                    $("table#tblCustomerGeneralInfo td#tdCGIBirthDate").html("");
                if (feedback.Houseno != null)
                    $("table#tblCustomerGeneralInfo td#tdCGIHouseNo").html(capitalizeString(feedback.Houseno));
                else
                    $("table#tblCustomerGeneralInfo td#tdCGIHouseNo").html("");
                if (feedback.address1 != null)
                    $("table#tblCustomerGeneralInfo td#tdCGIAddress1").html(capitalizeString(feedback.address1));
                else
                    $("table#tblCustomerGeneralInfo td#tdCGIAddress1").html("");
                if (feedback.address2 != null)
                    $("table#tblCustomerGeneralInfo td#tdCGIAddress2").html(capitalizeString(feedback.address2));
                else
                    $("table#tblCustomerGeneralInfo td#tdCGIAddress2").html("");
                if (feedback.city != null)
                    $("table#tblCustomerGeneralInfo td#tdCGICity").html(capitalizeString(feedback.city));
                else
                    $("table#tblCustomerGeneralInfo td#tdCGICity").html("");
                if (feedback.postcode != null)
                    $("table#tblCustomerGeneralInfo td#tdCGIPostcode").html(feedback.postcode.toUpperCase());
                else
                    $("table#tblCustomerGeneralInfo td#tdCGIPostcode").html("");

                //13-Jan-2017 : Moorthy Added for ID File Download Func
                if (feedback.proof_file_name != null && feedback.proof_file_name != "") {
                    $("table#tblCustomerGeneralInfo .clscgiidprooffile").show();
                    $("table#tblCustomerGeneralInfo td#tdCGIIDProofFile").attr("ctrlvalue", feedback.proof_file_name);
                }
                else {
                    $("table#tblCustomerGeneralInfo .clscgiidprooffile").hide();
                }

                if (feedback.SubscriberID != null)
                    $("input[name=iGIECSubscriberID]").val(feedback.SubscriberID);
                else
                    $("input[name=iGIECSubscriberID]").val("");

                if (feedback.first_name != null)
                    $("input[name=ihGIECFirstName]").val(capitalizeString(feedback.first_name));
                else
                    $("input[name=ihGIECFirstName]").val("");
                if (feedback.last_name != null)
                    $("input[name=ihGIECLastName]").val(capitalizeString(feedback.last_name));
                else
                    $("input[name=ihGIECLastName]").val("");
                if (feedback.Email != null)
                    $("input[name=ihGIECEmail]").val(feedback.Email);
                else
                    $("input[name=ihGIECEmail]").val("");

                if (feedback.BirthDate_String != null && feedback.BirthDate_String != "") {
                    var arrBOD = feedback.BirthDate_String.split("-");
                    $("input[name=ihGIECDOB]").val(arrBOD[0] + "/" + arrBOD[1] + "/" + arrBOD[2]);
                } else {
                    $("input[name=ihGIECDOB]").val("");
                }
                if (feedback.Houseno != null)
                    $("input[name=ihGIECHouseNo]").val(capitalizeString(feedback.Houseno));
                else
                    $("input[name=ihGIECHouseNo]").val("");
                if (feedback.address1 != null)
                    $("input[name=ihGIECAddress1]").val(capitalizeString(feedback.address1));
                else
                    $("input[name=ihGIECAddress1]").val("");
                if (feedback.address2 != null)
                    $("input[name=ihGIECAddress2]").val(capitalizeString(feedback.address2));
                else
                    $("input[name=ihGIECAddress2]").val("");
                if (feedback.city != null)
                    $("input[name=ihGIECCity]").val(capitalizeString(feedback.city));
                else
                    $("input[name=ihGIECCity]").val("");
                if (feedback.postcode != null)
                    $("input[name=ihGIECPostCode]").val(feedback.postcode.toUpperCase());
                else
                    $("input[name=ihGIECPostCode]").val("");
                if (feedback.country != null)
                    $("input[name=ihGIECCountry]").val(feedback.country.toUpperCase());
                else
                    $("input[name=ihGIECCountry]").val("");

                LoadDefaultCustomerInfo();

                var countryName = "";
                if (feedback.country == null)
                    feedback.country = "";
                var countryCode = feedback.country.toLowerCase();
                switch (countryCode) {
                    case 'gb':
                        countryName = 'United Kingdom';
                        break;
                    case 'dk':
                        countryName = 'Denmark';
                        break;
                    case 'nl':
                        countryName = 'Netherlands';
                        break;
                    case 'se':
                        countryName = 'Sweden';
                        break;
                    case 'pt':
                        countryName = 'Portugal';
                        break;
                    case 'fr':
                        countryName = 'France';
                        break;
                    case 'fi':
                        countryName = 'Finland';
                        break;
                    case 'at':
                        countryName = 'Austria';
                        break;
                    case 'au':
                        countryName = 'Australia';
                        break;
                    case 'be':
                        countryName = 'Belgium';
                        break;
                    case 'pl':
                        countryName = 'Poland';
                        break;
                    default:
                        countryName = '';
                }
                $("table#tblCustomerGeneralInfo td#tdCGICountry").html(capitalizeString(countryName));

                //14-Jun-2017 : Moorthy Added for Security Answer
                if (feedback.securityanswer != null)
                    $("table#tblCustomerGeneralInfo td#tdSecurityAnswer").html(feedback.securityanswer);
                else
                    $("table#tblCustomerGeneralInfo td#tdSecurityAnswer").html("");

                if (feedback.securityanswer != null)
                    $("input[name=ihGIECSecurityAnswer]").val(feedback.securityanswer);
                else
                    $("input[name=ihGIECSecurityAnswer]").val("");

                //27-Nov-2018 : Moorthy Added for Alternate Number
                if (feedback.Telephone != null)
                    $("table#tblCustomerGeneralInfo td#tdAlternateNumber").html(feedback.Telephone);
                else
                    $("table#tblCustomerGeneralInfo td#tdAlternateNumber").html("");

                if (feedback.Telephone != null)
                    $("input[name=ihGIECAlternateNumber]").val(feedback.Telephone);
                else
                    $("input[name=ihGIECAlternateNumber]").val("");


                // Formatting Calendar
                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1; //January is 0!
                var yyyy = today.getFullYear();
                if (dd < 10) { dd = '0' + dd } if (mm < 10) { mm = '0' + mm } today = dd + '/' + mm + '/' + yyyy;

                var _iGIECDOB = $('#_iGIECDOB').val();
                if (_iGIECDOB == '') {
                    $('#iGIECDOBContainer').attr('data-date', today);
                    $('#iGIECDOBContainer').datepicker().on('changeDate', function (e) {
                        $('div.datepicker').hide();
                    });
                    $('#iGIECDOB').val(today);
                    $('#ihGIECDOB').val(today);
                    $('#_iGIECDOB').val(today);
                }
                else {
                    $('#iGIECDOBContainer').attr('data-date', _iGIECDOB);
                    $('#iGIECDOBContainer').datepicker().on('changeDate', function (e) {
                        $('div.datepicker').hide();
                    });
                    $('#iGIECDOB').val(_iGIECDOB);
                    $('#ihGIECDOB').val(_iGIECDOB);

                }

                //Customer KYC details should be mandatory - error sign if not filled in
                if ($("table#tblCustomerGeneralInfo td#tdCGIFirstName").html() == "" || $("table#tblCustomerGeneralInfo td#tdCGILastName").html() == "" || $("table#tblCustomerGeneralInfo td#tdCGIEmail").html() == "" || $("table#tblCustomerGeneralInfo td#tdCGIBirthDate").html() == "" || $("table#tblCustomerGeneralInfo td#tdCGIHouseNo").html() == "" || $("table#tblCustomerGeneralInfo td#tdCGIAddress1").html() == "" || $("table#tblCustomerGeneralInfo td#tdCGICity").html() == "" || $("table#tblCustomerGeneralInfo td#tdCGIPostcode").html() == "" || $("table#tblCustomerGeneralInfo td#tdSecurityAnswer").html() == "" || $("table#tblCustomerGeneralInfo td#tdAlternateNumber").html() == "") {
                    $("#aTabGeneral").html("General <span style='color:red;font-weight:bold'> !</span>");
                    $("#aTabGeneral").prop('title', 'KYC details are mandatory!');
                    $("#aPAYMTabGeneral").html("General <span style='color:red;font-weight:bold'> !</span>");
                    $("#aPAYMTabGeneral").prop('title', 'KYC details are mandatory!');
                }

                //Added : 22-Dec-2018 : CRM Enhancement
                if (feedback.passwd != null && feedback.passwd != "") {
                    $("table#tblCustomerGeneralInfo td#tdCGIRegisteredToMyAccount").html("Yes");
                    $("#btnResetMyAccountPassword").show();
                    $("#btnResetMyAccountPasswordSendMail").show();
                }
                else {
                    $("table#tblCustomerGeneralInfo td#tdCGIRegisteredToMyAccount").html("No");
                    $("#btnResetMyAccountPassword").hide()
                    $("#btnResetMyAccountPasswordSendMail").hide();
                }
                $("table#tblCustomerGeneralInfo td#tdCGIMyAccountPassword").html(feedback.passwd);

                //Added : 11-Jan-2019 : SIM Activation details for AT
                // if (SIM_Type == 'PAYG') {
                $("table#tblCustomerGeneralInfo td#tdCGIPrepaidSimActivated").html(feedback.act_flag);
                $("table#tblCustomerGeneralInfo td#tdCGIPrepaidSimActivatedBy").html(feedback.act_by);
                if (feedback.act_flag != null && feedback.act_flag.trim().toUpperCase() == "YES") {
                    $("table#tblCustomerGeneralInfo td#tdCGIPrepaidSimActivatedOn").html(feedback.act_on_string);

                    //07-Sep-2019: Moorthy : Added for AT Sim Activation
                    if ($("table#tblCustomerInfoHeader td#tdCIHConnectionStatus").html() == "Active") {
                        $("#btnActivateCustomerSimAT").hide();
                    }
                }
                else
                    $("table#tblCustomerGeneralInfo td#tdCGIPrepaidSimActivatedOn").html("");
                //  }
            } else {
                //Customer KYC details should be mandatory - error sign if not filled in
                $("#aTabGeneral").html("General <span style='color:red;font-weight:bold'> !</span>")
                $("#aTabGeneral").prop('title', 'KYC details are mandatory!');
                $("#aPAYMTabGeneral").html("General <span style='color:red;font-weight:bold'> !</span>");
                $("#aPAYMTabGeneral").prop('title', 'KYC details are mandatory!');

                $("table#tblCustomerGeneralInfo td#tdCGIFirstName").html("");
                $("table#tblCustomerGeneralInfo td#tdCGILastName").html("");
                $("table#tblCustomerGeneralInfo td#tdCGIEmail").html("");
                $("table#tblCustomerGeneralInfo td#tdCGIBirthDate").html("");
                $("table#tblCustomerGeneralInfo td#tdCGIAddress1").html("");
                $("table#tblCustomerGeneralInfo td#tdCGIAddress2").html("");
                $("table#tblCustomerGeneralInfo td#tdCGICity").html("");
                $("table#tblCustomerGeneralInfo td#tdCGIPostcode").html("");
                $("table#tblCustomerGeneralInfo td#tdCGICountry").html("");
                //14-Jun-2017 : Moorthy Added for Security Answer
                $("table#tblCustomerGeneralInfo td#tdSecurityAnswer").html("");
                //13-Jan-2017 : Moorthy Added for ID File Download Func
                $("table#tblCustomerGeneralInfo .clscgiidprooffile").hide();

                //Added : 22-Dec-2018 : CRM Enhancement
                $("table#tblCustomerGeneralInfo td#tdCGIRegisteredToMyAccount").html("");
                $("table#tblCustomerGeneralInfo td#tdCGIMyAccountPassword").html("");
                $("#btnResetMyAccountPassword").hide();

                //Added : 11-Jan-2019 : SIM Activation details for AT
                //if (SIM_Type == 'PAYG') {
                $("table#tblCustomerGeneralInfo td#tdCGIPrepaidSimActivated").html("");
                $("table#tblCustomerGeneralInfo td#tdCGIPrepaidSimActivatedBy").html("");
                $("table#tblCustomerGeneralInfo td#tdCGIPrepaidSimActivatedOn").html("");
                // }
            }
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : SaveEditCustomerInfo
 * Purpose          : to save customer information
 * Added by         : Edi Suryadi
 * Create Date      : September 11th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function SaveEditCustomerInfo(p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15) {

    var url = apiServer + '/api/CustomerInfo';

    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    jQuery.support.cors = true;
    var JSONSendData = {
        FirstName: p1,
        LastName: p2,
        Email: p3,
        BirthDate: p4,
        Houseno: p5,
        address1: p6,
        address2: p7,
        city: p8,
        postcode: p9,
        country: p10,
        sitecode: p11,
        SubscriberID: p12,
        Title: p13,
        Telephone: p14,
        SecurityAnswer: p15
    };

    $("#GIEditCustomer").hide();
    $(".modal-backdrop").hide();

    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();

            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });

            var stat = data.errcode;
            if (stat == 0) {
                $('.PMSuccess .modal-header').html(data.errsubject);
                $('.PMSuccess .modal-body').html(data.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            } else {
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (data) {
            $("#ajax-screen-masking").hide();
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            $('.PMFailed .modal-header').html(data.errsubject);
            $('.PMFailed .modal-body').html(data.errmsg);
            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : LoadDefaultCustomerInfo
 * Purpose          : to load the default of customer Information
 * Added by         : Edi Suryadi
 * Create Date      : September 12th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function LoadDefaultCustomerInfo() {
    $("input[name=iGIECFirstName]").val($("input[name=ihGIECFirstName]").val());
    $("input[name=iGIECLastName]").val($("input[name=ihGIECLastName]").val());
    $("input[name=iGIECEmail]").val($("input[name=ihGIECEmail]").val());
    $("input[name=_iGIECDOB]").val($("input[name=ihGIECDOB]").val());
    $("input[name=iGIECHouseNo]").val($("input[name=ihGIECHouseNo]").val());
    $("input[name=iGIECAddress1]").val($("input[name=ihGIECAddress1]").val());
    $("input[name=iGIECAddress2]").val($("input[name=ihGIECAddress2]").val());
    $("input[name=iGIECCity]").val($("input[name=ihGIECCity]").val());
    $("input[name=iGIECPostCode]").val($("input[name=ihGIECPostCode]").val());
    $("select[name=iGIECCountry] option[value='" + $("input[name=ihGIECCountry]").val() + "']").attr("selected", true);
    $("input[name=iGIECAlternateNumber]").val($("input[name=ihGIECAlternateNumber]").val());
    $("input[name=iGIECSecurityAnswer]").val($("input[name=ihGIECSecurityAnswer]").val());
}

/* ----------------------------------------------------- 
   *  eof TAB GENERAL 
   ===================================================== */

//13-Jan-2017 : Moorthy Added for ID File Download Func
function DownloadIDProof() {
    var filename = $("table#tblCustomerGeneralInfo td#tdCGIIDProofFile").attr("ctrlvalue");
    var i = 0;
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    var url = '/Download/DownloadIDProof'
    var JSONSendData = {
        filename: filename
    };
    $.ajax
    ({
        url: url,
        type: 'get',
        dataType: 'json',
        data: JSONSendData,
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            if (feedback != null) {
                if (feedback.errcode == 0) {
                    var IFrameData = $('<iframe/>', { id: 'downloadIFrame', src: '/Download/GoDownloadIDProof?id=' + feedback.Text + '&ext=' + feedback.data_type + '&type=' + feedback.errmsg, style: 'display:none' });
                    $("#ajax-screen-masking").append(IFrameData);
                }
                else {
                    alert(feedback.errmsg);
                }
            }
        },
        error: function (msg) {
            $("#ajax-screen-masking").hide();
        }

    });
}

// Reset MyAccount Password
function ResetMyAccountPassword(sitecode, msisdn, password, firstname, email, crm_user) {
    $("#ajax-screen-masking").show();
    var url = apiServer + '/api/ResetMyAccountPwd';
    jQuery.support.cors = true;
    var JSONSendData = {
        sitecode: sitecode,
        msisdn: msisdn,
        password: password,
        firstname: firstname,
        email: email,
        crm_user: crm_user,
        process_type: 0
    };
    $.ajax
    ({
        url: url,
        type: 'get',
        data: JSONSendData,
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            $('#ResetPasswordPopup').modal('hide');
            //var firstName = $("#tdCGIFirstName").html().trim() != "" ? $("#tdCGIFirstName").html().trim() : "Sir/Madam";
            //var url = "https://www.vectonemobile.co.uk/myaccount/login";
            //var supportMail = "support@vectonemobile.co.uk";
            //if (sitecode == "MCM") {
            //    url = "https://www.vectonemobile.co.uk/myaccount/login";
            //    supportMail = "support@vectonemobile.co.uk";
            //}
            //else if (sitecode == "BAU") {
            //    url = "https://www.vectonemobile.at/en/myaccount/login";
            //    supportMail = "support@vectonemobile.at";
            //}
            //else if (sitecode == "MFR") {
            //    url = "https://www.vectonemobile.fr/en/myaccount/login";
            //    supportMail = "support@vectonemobile.fr";
            //}
            //else if (sitecode == "MBE") {
            //    url = "https://www.vectonemobile.be/en/myaccount/login";
            //    supportMail = "support@vectonemobile.be";
            //}
            //else if (sitecode == "BNL") {
            //    url = "https://www.vectonemobile.nl/en/myaccount/login";
            //    supportMail = "support@vectonemobile.nl";
            //}

            //var body = "Dear " + firstName + ",<br/><br/>Your <a href='" + url + "'>My Vectone</a> account password is " + password + " .<br/>Thank you for choosing Vectone Mobile. Kindly contact us in case of further queries, our Customer Support Team will be glad to assist you.<br/><br/>Should you need any further support, please contact us on - 020 7179 0134 (from other phones) or dial 322 from your Vectone Mobile.  Alternatively, you can send us an email to " + supportMail + "<br/><br/>Kind regards,<br/><br/>Customer Service Team<br/>   Vectone Mobile UK";
            if (feedback[0].errcode == 0) {
                //SendMail(feedback[0].email, "Vectone MyAccount Password has been Reset : " + msisdn, body);
                alert(feedback[0].errmsg);
                location.reload();
            }
            else {
                alert(feedback[0].errmsg);
            }
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}

function SendMail(email_to, email_subject, email_body) {
    $("#ajax-screen-masking").show();
    var url = apiServer + '/api/MailChimp';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 14,
        email_to: email_to,
        email_subject: email_subject,
        email_body: email_body
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: true,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}

// Reset MyAccount Password Send Mail
function ResetMyAccountPasswordSendMail(sitecode, msisdn, password, firstname, email, crm_user) {
    $("#ajax-screen-masking").show();
    var url = apiServer + '/api/ResetMyAccountPwd';
    jQuery.support.cors = true;
    var JSONSendData = {
        sitecode: sitecode,
        msisdn: msisdn,
        password: password,
        firstname: firstname,
        email: email,
        crm_user: crm_user,
        process_type: 1
    };
    $.ajax
    ({
        url: url,
        type: 'get',
        data: JSONSendData,
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            $('#ResetPasswordPopup').modal('hide');
            //var firstName = $("#tdCGIFirstName").html().trim() != "" ? $("#tdCGIFirstName").html().trim() : "Sir/Madam";
            //var url = "https://www.vectonemobile.co.uk/myaccount/login";
            //var supportMail = "support@vectonemobile.co.uk";
            //if (sitecode == "MCM") {
            //    url = "https://www.vectonemobile.co.uk/myaccount/login";
            //    supportMail = "support@vectonemobile.co.uk";
            //}
            //else if (sitecode == "BAU") {
            //    url = "https://www.vectonemobile.at/en/myaccount/login";
            //    supportMail = "support@vectonemobile.at";
            //}
            //else if (sitecode == "MFR") {
            //    url = "https://www.vectonemobile.fr/en/myaccount/login";
            //    supportMail = "support@vectonemobile.fr";
            //}
            //else if (sitecode == "MBE") {
            //    url = "https://www.vectonemobile.be/en/myaccount/login";
            //    supportMail = "support@vectonemobile.be";
            //}
            //else if (sitecode == "BNL") {
            //    url = "https://www.vectonemobile.nl/en/myaccount/login";
            //    supportMail = "support@vectonemobile.nl";
            //}

            //var body = "Dear " + firstName + ",<br/><br/>Your <a href='" + url + "'>My Vectone</a> account password is " + password + " .<br/>Thank you for choosing Vectone Mobile. Kindly contact us in case of further queries, our Customer Support Team will be glad to assist you.<br/><br/>Should you need any further support, please contact us on - 020 7179 0134 (from other phones) or dial 322 from your Vectone Mobile.  Alternatively, you can send us an email to " + supportMail + "<br/><br/>Kind regards,<br/><br/>Customer Service Team<br/>   Vectone Mobile UK";
            if (feedback[0].errcode == 0) {
                //SendMail(feedback[0].email, "Vectone MyAccount Password has been Reset : " + msisdn, body);
                alert(feedback[0].errmsg);
                location.reload();
            }
            else {
                alert(feedback[0].errmsg);
            }
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}

// 07-Sep-2019: Moorthy : Added for AT Sim Activation
function ActivateCustomerSimAT(sitecode, iccid, mobileno, email, birthdate, first_name, last_name, postcode, address, called_by) {
    debugger;
    $("#ajax-screen-masking").show();
    var url = apiServer + '/api/ActivateCustomerSimAT';
    jQuery.support.cors = true;
    var JSONSendData = {
        sitecode: sitecode,
        iccid: iccid,
        mobileno: mobileno,
        email: email,
        birthdate: birthdate,
        first_name: first_name,
        last_name: last_name,
        postcode: postcode,
        address: address,
        called_by: called_by
    };
    $.ajax
    ({
        url: url,
        type: 'POST',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            debugger;
            $("#ajax-screen-masking").hide();
            if (feedback[0].errcode == 0) {
                //SendMail(feedback[0].email, "Vectone MyAccount Password has been Reset : " + msisdn, body);
                alert(feedback[0].errmsg);
                location.reload();
            }
            else {
                alert(feedback[0].errmsg);
            }
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}