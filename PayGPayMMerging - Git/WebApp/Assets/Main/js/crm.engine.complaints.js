﻿function ITViewAllComplaints(Subscriberid, Site_code, mobileno) {
    var SIM_Type = 'PAYG';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1) {
        SIM_Type = 'PAYM';
    }
    $("#itComplaintsContainer").html("").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });

    if (Subscriberid != '' && Site_code != '') {
        var url = apiServer + '/api/Complaints/';

        jQuery.support.cors = true;
        var JSONSendData = {
            SearchType: 1,
            Subscriberid: Subscriberid,
            Site_code: Site_code,
            SIM_Type: SIM_Type,
            mobileno: mobileno
        };

        $.ajax
        ({
            url: url,
            type: 'post',
            data: JSON.stringify(JSONSendData),
            dataType: 'json',
            contentType: "application/json;charset=utf-8",
            cache: false,
            success: function (data) {
                $("#itComplaintsContainer").css({ "background-image": "none" });
                $("#itComplaintsContainer").append("<button class=\"btn blue\" id=\"btnITNewComplaint\" style=\"width:140px;height:35px\">New Complaint</button>");
                //$("#itComplaintsContainer").append("<button class=\"btn blue\" id=\"btnITDeadIssue\" style=\"width:140px;height:35px;margin-left:50px\">Blank Call</button>");
                $("#itComplaintsContainer").append("<div style=\"width:100%;margin-top:15px\"><table style=\"width:100%\" class=\"table table-bordered\" id=\"tblITComplaintsList\"></table></div>");

                $('#tblITComplaintsList').dataTable({
                    bDestroy: true,
                    bRetrieve: true,
                    aaData: data,
                    bPaginate: true,
                    bFilter: true,
                    bInfo: false,
                    iDisplayLength: 15,
                    aaSorting: [],
                    oLanguage: {
                        "sInfoEmpty": '',
                        "sEmptyTable": "No Complaints found."
                    },
                    aoColumns: [
                       { mDataProp: "Ticket_id", sTitle: "TicketId" },
                       { mDataProp: "mobileno", sTitle: "Mobile Number" },
                       { mDataProp: "issue_generate_date_string", sTitle: "Issue Generated Date" },
                       { mDataProp: "Issue_Name", sTitle: "Issue Name" },
                       { mDataProp: "Function_Name", sTitle: "Function Name" },
                       { mDataProp: "agent_name", sTitle: "Agent Name" },
                       { mDataProp: "eslated_user", sTitle: "Esclated User" },
                       { mDataProp: "descr", sTitle: "Description" },
                       { mDataProp: "issue_complete_date_string", sTitle: "Issue Completed Date" },
                       { mDataProp: "sub_category", sTitle: "sub_category" },
                       {
                           mDataProp: "Action", sTitle: "Action", bSortable: false, sWidth: 50,
                           fnRender: function (ob) {

                               var btnTitle = "Edit";
                               var actionURL = "";
                               actionURL = "<button id=\"EditComplaints\" class=\"btn blue\" Ticket_id=\"" + ob.aData.Ticket_id + "\" mobileno=\"" + ob.aData.mobileno + "\" issue_type=\"" + ob.aData.issue_type + "\" Issue_Function=\"" + ob.aData.Issue_Function + "\" Issue_Complaint=\"" + ob.aData.Issue_Complaint + "\" esclating_user_id=\"" + ob.aData.esclating_user_id + "\" descr=\"" + ob.aData.descr + "\" status_id=\"" + ob.aData.status_id + "\" fk_priority_id=\"" + ob.aData.fk_priority_id + "\" style=\"width:85px\" >" + ob.aData.status + "</button>";
                               return actionURL;
                           }
                       },
                       {
                           mDataProp: "Action2", sTitle: "View", bSortable: false, sWidth: 50,
                           fnRender: function (ob) {

                               var btnTitle = "View";
                               var actionURL = "";
                               actionURL = "<button id=\"ViewComplaints\" class=\"btn blue\" Ticket_id=\"" + ob.aData.Ticket_id + "\" mobileno=\"" + ob.aData.mobileno + "\" Issue_Name=\"" + ob.aData.Issue_Name + "\" Function_Name=\"" + ob.aData.Function_Name + "\" eslated_user=\"" + ob.aData.eslated_user + "\" descr=\"" + ob.aData.descr + "\" status=\"" + ob.aData.status + "\" sub_category=\"" + ob.aData.sub_category + "\" priority_name=\"" + ob.aData.priority_name + "\" style=\"width:85px\" >" + btnTitle + "</button>";
                               return actionURL;
                           }
                       }
                    ]
                });
                $("#tblITComplaintsList_wrapper .row-fluid:eq(0)").remove();
                $("#tblITComplaintsList_wrapper .row-fluid .span6:eq(0)").addClass("span4");
                $("#tblITComplaintsList_wrapper .row-fluid .span6:eq(1)").addClass("span8");
                $("#tblITComplaintsList_wrapper .row-fluid .span6").removeClass("span6");
            },
            error: function (data) {
                $("#itComplaintsContainer").css({ "background-image": "none" });
                $("#itComplaintsContainer").append("<button class=\"btn blue\" id=\"btnITNewComplaint\" style=\"width:140px;height:35px\">New Issue</button><br/>");
                $("#itComplaintsContainer").append("<div style=\"width:98%;padding:10px;border:1px solid #e5e5e5;background-color:#f5f5f5;margin-top:20px\">No issues found.</div>");
            }
        });
    } else {
        $("#itComplaintsContainer").css({ "background-image": "none" });
        $("#itComplaintsContainer").html("<div style=\"height:40px;font-size:14px;margin-top:15px;color:#555555\">Can\'t find Subscriber ID of this customer.</div>");
    }
}

$(document).ready(function () {
    function trim(str) {
        str = str.replace(/^\s+/, '');
        for (var i = str.length - 1; i >= 0; i--) {
            if (/\S/.test(str.charAt(i))) {
                str = str.substring(0, i + 1);
                break;
            }
        }
        return str;
    }

    function dateHeight(dateStr) {
        if (trim(dateStr) != '') {
            var frDate = trim(dateStr).split(' ');
            var frTime;
            if (frDate.length > 1) {
                frTime = frDate[1].split(':');
            }
            var frDateParts = frDate[0].split('-');
            var day = frDateParts[0] * 60 * 24;
            var month = frDateParts[1] * 60 * 24 * 31;
            var year = frDateParts[2] * 60 * 24 * 366;
            var x;
            if (frDate.length > 1) {
                var hour = frTime[0] * 60;
                var minutes = frTime[1];
                x = day + month + year + hour + minutes;
            }
            else {
                x = day + month + year;
            }

        } else {
            var x = 99999999999999999; //GoHorse!
        }
        return x;
    }


    jQuery.fn.dataTableExt.oSort['date-euro-asc'] = function (a, b) {
        var x = dateHeight(a);
        var y = dateHeight(b);
        var z = ((x < y) ? -1 : ((x > y) ? 1 : 0));
        return z;
    };

    jQuery.fn.dataTableExt.oSort['date-euro-desc'] = function (a, b) {
        var x = dateHeight(a);
        var y = dateHeight(b);
        var z = ((x < y) ? 1 : ((x > y) ? -1 : 0));
        return z;
    };

});

//function FillTicketType(sitecode) {
//    $("#ajax-screen-masking").show();
//    $('#ddlComplaintTicketType').find('option').remove();
//    var url = apiServer + '/api/EmailGenerateTicket';
//    jQuery.support.cors = true;
//    var JSONSendData = {
//        modetype: 1,
//        sitecode: sitecode
//    };
//    $.ajax
//    ({
//        url: url,
//        type: 'post',
//        data: JSON.stringify(JSONSendData),
//        dataType: 'json',
//        contentType: "application/json;charset=utf-8",
//        cache: true,
//        success: function (feedback) {
//            $("#ajax-screen-masking").hide();
//            var feedbackarr = jQuery.makeArray(JSON.stringify(feedback).split('}'));
//            var icount;
//            var actionOption = '<option value="0" style=\"color:#c0c0c0\">--- Select ---</option>';
//            for (icount = 0; icount < feedbackarr.length - 1; icount++) {
//                actionOption += '<option ctlValue="' + feedback[icount].Issue_Id + '"   value="' + feedback[icount].Issue_Id + '" style=\"padding:3px\">' + feedback[icount].Issue_Name + '</option>';
//            }
//            $('#ddlComplaintTicketType').append(actionOption);
//        },
//        error: function (feedback) {
//            $("#ajax-screen-masking").hide();
//        }
//    });
//}

function FillNewStatus() {
    $('#ddlComplaintStatus').find('option').remove();
    var actionOption = '<option value="-1" style=\"color:#c0c0c0\">--- Select ---</option>';
    var actionOption = '<option ctlValue="' + "0" + '"   value="' + "0" + '" style=\"padding:3px\">' + "Open" + '</option>';
    $('#ddlComplaintStatus').append(actionOption);
}

function FillEscalatingTeam() {
    $("#ajax-screen-masking").show();
    $('#ddlComplaintEscalatingTeam').find('option').remove();
    var url = apiServer + '/api/EmailGenerateTicket';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 7
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: true,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            var feedbackarr = jQuery.makeArray(JSON.stringify(feedback).split('}'));
            var icount;
            var actionOption = '<option value="0" style=\"color:#c0c0c0\">--- Select ---</option>';
            for (icount = 0; icount < feedbackarr.length - 1; icount++) {
                if ($("#txtComplaintEscalatingTeam").val().toUpperCase() == feedback[icount].Username.toUpperCase())
                    actionOption += '<option ctlValue="' + feedback[icount].Email + '"   value="' + feedback[icount].Id + '" style=\"padding:3px\"  selected=\"selected\">' + feedback[icount].Username + '</option>';
                else
                    actionOption += '<option ctlValue="' + feedback[icount].Email + '"   value="' + feedback[icount].Id + '" style=\"padding:3px\">' + feedback[icount].Username + '</option>';
            }
            $('#ddlComplaintEscalatingTeam').append(actionOption);
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}

function FillCategory(sitecode, Issue_Id, Function_Id) {
    $("#ajax-screen-masking").show();
    $('#ddlComplaintCategory').find('option').remove();
    $('#ddlComplaintSubCategory').find('option').remove();

    var url = apiServer + '/api/EmailGenerateTicket';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 2,
        sitecode: sitecode,
        Issue_Id: Issue_Id
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: true,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            if (feedback.length > 0) {
                var feedbackarr = jQuery.makeArray(JSON.stringify(feedback).split('}'));
                var icount;
                var actionOption = '<option value="0" style=\"color:#c0c0c0\" selected>--- Select ---</option>';
                for (icount = 0; icount < feedbackarr.length - 1; icount++) {
                    if (Function_Id == feedback[icount].Function_Id)
                        actionOption += '<option ctlValue="' + feedback[icount].Function_Id + '"   value="' + feedback[icount].Function_Id + '" style=\"padding:3px\" selected>' + feedback[icount].Function_Name + '</option>';
                    else
                        actionOption += '<option ctlValue="' + feedback[icount].Function_Id + '"   value="' + feedback[icount].Function_Id + '" style=\"padding:3px\">' + feedback[icount].Function_Name + '</option>';
                }
                $('#ddlComplaintCategory').append(actionOption);
            }
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}

function FillSubCategory(sitecode, Function_Id, Complaint_Id) {
    $("#ajax-screen-masking").show();
    $('#ddlComplaintSubCategory').find('option').remove();
    var url = apiServer + '/api/EmailGenerateTicket';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 4,
        sitecode: sitecode,
        Function_Id: Function_Id
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: true,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            if (feedback.length > 0) {
                var feedbackarr = jQuery.makeArray(JSON.stringify(feedback).split('}'));
                var icount;
                var actionOption = '<option value="0" style=\"color:#c0c0c0\" selected>--- Select ---</option>';
                for (icount = 0; icount < feedbackarr.length - 1; icount++) {
                    if (Complaint_Id == feedback[icount].Complaint_Id)
                        actionOption += '<option ctlValue="' + feedback[icount].Complaint_Id + '"   value="' + feedback[icount].Complaint_Id + '" style=\"padding:3px\" selected>' + feedback[icount].Complaint_Name + '</option>';
                    else
                        actionOption += '<option ctlValue="' + feedback[icount].Complaint_Id + '"   value="' + feedback[icount].Complaint_Id + '" style=\"padding:3px\">' + feedback[icount].Complaint_Name + '</option>';
                }
                $('#ddlComplaintSubCategory').append(actionOption);
            }
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}

function FillPriority(sitecode) {
    var actionOption = '<option value="-1" style=\"color:#c0c0c0\">--- Select ---</option>';
    actionOption += '<option value=1 style=\"padding:3px\">Critical - 4 hours</option>';
    actionOption += '<option value=2 style=\"padding:3px\">High - 8 hours</option>';
    actionOption += '<option value=3 style=\"padding:3px\">Medium - 12 hours</option>';
    actionOption += '<option value=4 style=\"padding:3px\">Low - 24 hours</option>';
    $('#ddlitComplaintInterval').append(actionOption);
}

function FillUpdateStatus() {
    var actionOption = '<option value="-1" style=\"color:#c0c0c0\">--- Select ---</option>';
    actionOption += '<option value=1 style=\"padding:3px\">In progress</option>';
    actionOption += '<option value=2 style=\"padding:3px\">Open</option>';
    actionOption += '<option value=3 style=\"padding:3px\">Assigned</option>';
    actionOption += '<option value=4 style=\"padding:3px\">Resolved</option>';
    actionOption += '<option value=5 style=\"padding:3px\">Closed</option>';
    actionOption += '<option value=6 style=\"padding:3px\">Reopened</option>';
    $('#ddlComplaintStatus').append(actionOption);
}

function FillAgentStatus() {
    var actionOption = '';
    actionOption += '<option value=0 style=\"padding:3px\">Open</option>';
    actionOption += '<option value=5 style=\"padding:3px\">Closed</option>';
    actionOption += '<option value=6 style=\"padding:3px\">Reopened</option>';
    $('#ddlComplaintStatus').append(actionOption);
}

function FillOtherStatus() {
    var actionOption = '';
    actionOption += '<option value=1 style=\"padding:3px\">Assigned</option>';
    actionOption += '<option value=2 style=\"padding:3px\">In progress</option>';
    actionOption += '<option value=3 style=\"padding:3px\">Resolved</option>';
    $('#ddlComplaintStatus').append(actionOption);
}

function insertcomplaint(customername, sitecode, mobile_no, issuetype, issuefunction, issuecomplaint, issuestatus, description, esclatinguser, processtype, agentuserid, subscriberid, email, ticket_id, priority_name) {
    var SIM_Type = 'PAYG';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1) {
        SIM_Type = 'PAYM';
    }
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    var url = apiServer + '/api/Complaints/';
    jQuery.support.cors = true;
    var JSONSendData = {
        SearchType: 2,

        customer_name: customername,
        sitecode: sitecode,
        mobileno: mobile_no,
        issue_type: issuetype,
        Issue_Function: issuefunction,
        Issue_Complaint: issuecomplaint,
        issue_status: issuestatus,
        descr: description,
        esclating_user: esclatinguser,
        process_type: processtype,
        agent_userid: agentuserid,
        email: email,
        Ticket_id: ticket_id,
        priority_name: priority_name
    };

    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            if (data.length > 0) {
                $("#ajax-screen-masking").hide();
                if (data[0].errcode == 0) {
                    $('#txtComplaintTicketID').val(data[0].ticket_id);
                    //$('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
                    $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
                    $('.PMSuccess').attr({ "alt": "1" });
                    $('.PMSuccess .modal-header').html('Success');
                    $('.PMSuccess .modal-body').html(data[0].errmsg + ' ' + data[0].ticket_id);
                    $('#itPWNewComplaint').modal('hide');
                    ITViewAllComplaints(subscriberid, sitecode, mobile_no);
                    $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                        'margin-left': function () {
                            return window.pageXOffset - ($(this).width() / 2);
                        }
                    });
                }
                else {
                    $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
                    $('.PMFailed .modal-header').html('Failed');
                    $('.PMFailed .modal-body').html(data[0].errmsg);
                    $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                        'margin-left': function () {
                            return window.pageXOffset - ($(this).width() / 2);
                        }
                    });
                }
            }
        },
        error: function (data) {
            $("#ajax-screen-masking").hide();
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            $('.PMFailed .modal-header').html('Failed');
            $('.PMFailed .modal-body').html(data[0].ErrMsg);
            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });
}
/* ----------------------------------------------------------------
 * Function Name    : ITDetailComplaint
 * Purpose          : to get detail of complaint by issue/problem id
 * Added by         : yogesh
 * Create Date      : June 3rd, 2019
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
