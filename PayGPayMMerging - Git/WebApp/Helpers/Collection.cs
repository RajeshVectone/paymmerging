﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.Collections.Specialized;

namespace CRM3.WebApp.Helpers
{
    public class Collection
    {
        public static SelectListItem CreateListItem(string text, string value)
        {
            SelectListItem item = new SelectListItem();

            item.Text = text;
            item.Value = value;

            return item;
        }

        public static IEnumerable<SelectListItem> CustomerSearchOptions()
        {
            List<SelectListItem> resValues = new List<SelectListItem>();

            resValues.Add(CreateListItem("Mobile Number", "MobileNo"));
            resValues.Add(CreateListItem("Customer Name", "FullName"));
            resValues.Add(CreateListItem("Customer ID", "pp_customer_id"));
            resValues.Add(CreateListItem("Subscription  ID", "subscriberid"));
            resValues.Add(CreateListItem("SIM Order ID", "freesimid"));
            resValues.Add(CreateListItem("ICCID", "ICCID"));
            resValues.Add(CreateListItem("E-mail", "Email"));
            resValues.Add(CreateListItem("Serial Number", "SerialNo"));
            resValues.Add(CreateListItem("Login Name", "LoginName"));

            return resValues;
        }

        public static IEnumerable<SelectListItem> ReportType()
        {
            List<SelectListItem> repValues = new List<SelectListItem>();
            repValues.Add(CreateListItem("DD Failures", "DDFailure"));
            repValues.Add(CreateListItem("Account Status", "AccountStatus"));
            repValues.Add(CreateListItem("New PayM Subscriptions", "PayMSub"));
            repValues.Add(CreateListItem("Change of Plan", "PlanChange"));
            repValues.Add(CreateListItem("Revenue Summary Report", "RevenueSummary"));
            repValues.Add(CreateListItem("Payment Failed Report", "PaymentFailed"));
            repValues.Add(CreateListItem("Payment Collected via Credit Card", "PaymentCollected"));
            repValues.Add(CreateListItem("Utilization - Threshold Report", "Utlilization"));
            return repValues;
        }
        public static IEnumerable<SelectListItem> ReportLayout(string rptType)
        {
            List<SelectListItem> repValues = new List<SelectListItem>();
            if (rptType == "DDFailure")
            {
                repValues.Add(CreateListItem("Not Applicable", "0"));
                repValues.Add(CreateListItem("Suspended", "1"));
                repValues.Add(CreateListItem("Cancelled", "2"));
                repValues.Add(CreateListItem("Active", "3"));
                repValues.Add(CreateListItem("Blocked", "4"));
                repValues.Add(CreateListItem("Excess Blocked", "5"));
                repValues.Add(CreateListItem("New Customer", "6"));
                repValues.Add(CreateListItem("PayG to PayM Conversion", "7"));
                repValues.Add(CreateListItem("Upgrade Count", "8"));
                repValues.Add(CreateListItem("Downgrade Count", "9"));
                repValues.Add(CreateListItem("Direct Debit", "10"));
                repValues.Add(CreateListItem("Credit Card", "11"));
                repValues.Add(CreateListItem("Interim Payment Details", "12"));
                repValues.Add(CreateListItem("Payment Attempt by CC post DD-Failures-Success", "13"));
                repValues.Add(CreateListItem("Payment Attempt by CC post DD-Failures-Failures", "14"));
                repValues.Add(CreateListItem("80% Credit Limit Threshold Usage Customer Report", "15"));
                repValues.Add(CreateListItem("100% Credit Limit Threshold Usage Customer Report", "16"));
            }
            else if (rptType == "AccountStatus")
            {
                repValues.Add(CreateListItem("Not Applicable", "0"));
                repValues.Add(CreateListItem("Suspended", "1"));
            }
            else
            {
                repValues.Add(CreateListItem("Suspended", "1"));
            }
            return repValues;
        }
        //Chillitalk-Talk
        public static IEnumerable<SelectListItem> CustomerChillitalk()
        {
            List<SelectListItem> resValues = new List<SelectListItem>();
            resValues.Add(CreateListItem("First name", "FullName"));
            resValues.Add(CreateListItem("Last name", "FullName"));
            resValues.Add(CreateListItem("Account access code", "AccountCode"));
            resValues.Add(CreateListItem("Email address", "Email"));
            resValues.Add(CreateListItem("Mobile number", "MobileNo"));
            return resValues;
        }

        public static IEnumerable<SelectListItem> Customer2in1Action()
        {
            List<SelectListItem> resValues = new List<SelectListItem>();

            resValues.Add(CreateListItem("Select action", ""));
            resValues.Add(CreateListItem("Add number", "AddNumber"));

            return resValues;
        }

        public static IEnumerable<SelectListItem> Customer2in1OrderCountry()
        {
            List<SelectListItem> resValues = new List<SelectListItem>();

            resValues.Add(CreateListItem("Portugal", "PT"));

            return resValues;
        }
    }
}