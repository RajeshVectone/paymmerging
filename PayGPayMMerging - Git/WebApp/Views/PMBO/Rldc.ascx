﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Rldc.ascx.cs" Inherits="CRM3.WebApp.Views.PMBO.Rldc" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<form id="form1" runat="server">
<div style="Height:100%;Width:100%">
    <asp:ScriptManager ID="scriptManager" runat="server" ScriptMode="Release" EnablePartialRendering="false"/>

        <rsweb:ReportViewer ID="reportViewer" runat="server" Width="100%" Height="100%" 
            ShowPrintButton="true"  KeepSessionAlive="true" AsyncRendering="false" ProcessingMode="Remote" SizeToReportContent="true" >
        </rsweb:ReportViewer>

      <input type="hidden"  id="hdnFlag" />

         
</div>
</form>