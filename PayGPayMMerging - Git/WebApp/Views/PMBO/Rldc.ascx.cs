﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.ReportingServices;
using Microsoft.Reporting.WebForms;
using CRM3.WebApp.Views.PMBO;
using System.Web.Mvc;
using CRM_API.Models;
using System.IO;


namespace CRM3.WebApp.Views.PMBO
{
    public partial class Rldc : System.Web.Mvc.ViewUserControl
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            Context.Handler = Page;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["test"] != null)
            {
                string s = Request.QueryString["test"].ToString();
            }
            ViewBag.CheckReport = null;
                
                string str = "";
                string strEmpty = " ";
                if (Session["ddYear"] != null)
                {
                    //string EnterText = Session["EnterText"].ToString();
                    string ddreportType = Session["ddreportType"].ToString();
                    string ddyear = Session["ddYear"].ToString();
                    string ddMonth = Session["ddMonth"].ToString();
                    string ddPlanType = Session["ddPlanType"].ToString();
                    string ddlApplyLayout = Session["ddlApplyLayout"].ToString();
                    str = "/PMBO/" + Session["rptName"].ToString();

                    if (Request.RawUrl.Contains("ReportsViewer"))
                    {
                        Response.Redirect(Request.RawUrl.Replace("ReportsViewer", "Reports"), false);
                        Session["hdnFlag"] = "select";

                    }
                    else if (Request.RawUrl.Contains("Reports"))
                    {
                         ReportingServicesReportViewModel model1 = new ReportingServicesReportViewModel();
                        CRM_API.Models.ReportingServicesReportViewModel model = new CRM_API.Models.ReportingServicesReportViewModel(str, new List<Microsoft.Reporting.WebForms.ReportParameter>()
                        { 
                            new Microsoft.Reporting.WebForms.ReportParameter("ReportType", ddreportType,false),
                            new Microsoft.Reporting.WebForms.ReportParameter("year", ddyear,false),
                            new Microsoft.Reporting.WebForms.ReportParameter("Month", ddMonth,false),
                            new Microsoft.Reporting.WebForms.ReportParameter("PlanType",ddPlanType,false),
                            new Microsoft.Reporting.WebForms.ReportParameter("Layout", ddlApplyLayout,false),
                            new Microsoft.Reporting.WebForms.ReportParameter("Title", strEmpty,false)
            
                        });
                        reportViewer.ServerReport.ReportServerCredentials = model.ServerCredentials;
                        ReportParameter[] RptParameters = model.parameters;

                        reportViewer.ServerReport.ReportPath = model.ReportPath;
                        reportViewer.ServerReport.ReportServerUrl = model.ReportServerURL;

                        if (RptParameters.Count() > 0)
                            this.reportViewer.ServerReport.SetParameters(RptParameters);

                        this.reportViewer.ServerReport.Refresh();

                   
                        Session["ddYear"] = null;

                        ViewBag.CheckReport = "true";
                    }
                }
               
               else
                {
                    ViewBag.hdnFlag = null;
                }
            
        }


    }
}