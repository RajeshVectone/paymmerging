﻿$(document).ready(function () {    
    var i= 1;
    $('#tPagingPaymentInitiate').dataTable({
        "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "sPaginationType": "bootstrap",
        "iDisplayLength": 5,
        "aLengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
        "oLanguage": {
            "sLengthMenu": "_MENU_ per page",
            "oPaginate": {
                "sPrevious": "Prev",
                "sNext": "Next"
            }
        }/*
        "aoColumns": [
           { "sType": "num-html" },
           null, null, null, null, null, null, null, null
        ],
        "aoColumnDefs": [{
            'bSortable': false,
            'aTargets': [11]    //disabled sortring
        }]    */
    });

    $("#searchPayment").click(function () {
        //$("#tPagingPaymentInitiate_wrapper").remove();
        //$("table#tPagingPaymentInitiate").attr("id", "tPagingPaymentServer");   //change id name for new dataTable initiation
        var params = {
            MobileNumber    : $.trim($("#detail-mobileno").text()),
            TransStatus     : $("#filter-status option:selected").val(),
            PaymentProfile  : $("#filter-paymentprofile option:selected").val()
        };

        $.ajax({
            type: "GET",
            url: "../Payment/PaymentHistorySearch",
            cache: true,
            data: params,
            dataType: 'json',
            success: function (data, status) {
                //alert(data);                
                $(data).each(function (index) {	
                    var sOut = '<tr id=' + i + '>';
                    sOut += '<td>' + data[index].convertPaymentDate + '</td>';
                    sOut += '<td>' + data[index].billid +'</td>';
                    sOut += '<td>' + data[index].amount +'</td>';
                    sOut += '<td>' + data[index].transstatus +'</td>';
                    sOut += '<td>' + data[index].paymentprofile +'</td>';
                    sOut += '<td>' + data[index].crmuser +'</td>';
                    sOut += '<td><a class="btn payhistoryPaymMore" href="#" title="' + data[index].billid + '">More Info</a></td>';
                    sOut += '</tr>';
                    i = i+1;
                    jQuery("#tPagingPaymentInitiate > tbody:first").append(sOut);
                });
            },
            error: function (data, status) {
                alert(status, data);
            },
            complete: function () {
                initiateSearchDataTable();
            }
        });
    });
    
    function initiateSearchDataTable() {
        $('#tPagingPaymentInitiate').dataTable({
            "bDestroy": true,
            "bProcessing": true,
            "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
            "sPaginationType": "bootstrap",
            "iDisplayLength": 5,
            "aLengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
            "oLanguage": {
                "sLengthMenu": "_MENU_ per page",
                "oPaginate": {
                    "sPrevious": "Prev",
                    "sNext": "Next"
                }
            }
        });
    }
});