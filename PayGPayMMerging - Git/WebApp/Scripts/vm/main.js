﻿$(document).ready(function () {    
    var DUNI = $('#lara').data('DUNI');
    var orderIdChosen = $('#modalPaygFreeSIM').data('orderid');
    var ispayg = $('#modalProvision').data('ispayg');
    var simIICID = $('#modalPaygFreeSIM').data('iccid');
    var arrChkDispatchOrder = [];
   
    $('#tPagingVMFreeSIM').dataTable({
        "bDestroy": true,
        "bProcessing": true,
        "bAutoWidth": false,
        "sDom": "<'row-fluid'<'span6'l><'span3'><'span3'T>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "sPaginationType": "bootstrap",
        "iDisplayLength": 10,
        "aLengthMenu": [[10, 20, 50, 100, -1], [10, 20, 50, 100, "All"]],
        "oTableTools": {
            "sSwfPath": "../../Content/swf/copy_csv_xls_pdf.swf",
            "sRowSelect": "multi",
            "aButtons": [
            {
                "sExtends": "csv",
                "sButtonText": "Download Excel",
                "bSelectedOnly": true,
                "sFileName": "PAYG FreeSIM Order.csv",
                "sTitle": "PAYG FreeSIM Order",
                "mColumns": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]
            }]
        },
        "oLanguage": {
            "sLengthMenu": "_MENU_ per page",
            "oPaginate": {
                "sPrevious": "Prev",
                "sNext": "Next"
            }
        },
        "aoColumns": [
              null,
              { "bVisible": false }, null, null, null,
              { "bVisible": false }, { "bVisible": false },
              { "bVisible": false }, { "bVisible": false },
              { "bVisible": false }, null, null, null, { "bVisible": false },
              null,null,null,null,null,null
        ]
    });

    $('#tPagingFinance').dataTable({
        "bDestroy": true,
        "bProcessing": true,
        "bAutoWidth": false,
        "sDom": "<'row-fluid'<'span6'l><'span3'><'span3'T>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "sPaginationType": "bootstrap",
        "iDisplayLength": 10,
        "aLengthMenu": [[10, 20, 50, 100, -1], [10, 20, 50, 100, "All"]],
        "oTableTools": {
            "sSwfPath": "../../Content/swf/copy_csv_xls_pdf.swf",
            "sRowSelect": "multi",
            "aButtons": [
            {
                "sExtends": "csv",
                "sButtonText": "Download Excel",
                "bSelectedOnly": true,
                "sFileName": "RecommendBonus.csv",
                "sTitle": "Recommend Bonus",
                "mColumns": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
            }]
        },

        "oLanguage": {
            "sLengthMenu": "_MENU_ per page",
            "oPaginate": {
                "sPrevious": "Prev",
                "sNext": "Next"
            }
        }
    });

    $('#tPagingVMUKOrders').dataTable({
        "bDestroy": true,
        "bProcessing": true,
        "bAutoWidth": false,
        "sDom": "<'row-fluid'<'span6'l><'span3'><'span3'T>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "sPaginationType": "bootstrap",
        "iDisplayLength": 10,
        "aLengthMenu": [[10, 20, 50, 100, -1], [10, 20, 50, 100, "All"]],
        "oTableTools": {
            "sSwfPath": "../../Content/swf/copy_csv_xls_pdf.swf",
            //"sRowSelect": "multi",
            "aButtons": [
            {
                "sExtends": "csv",
                "sButtonText": "Download Excel",
                "bSelectedOnly": true,
                "sFileName": "PAYG FreeSIM Order.csv",
                "sTitle": "PAYG FreeSIM Order",
                "mColumns": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
            }]
        },
        "oLanguage": {
            "sLengthMenu": "_MENU_ per page",
            "oPaginate": {
                "sPrevious": "Prev",
                "sNext": "Next"
            }
        }
    });
    

    $('.paygfreesimorder').live('click', function (ev) {
        var idcard = "";
        var idmodal = $(this).attr("title");    //order_id located at title attribute
        $('#modalPaygFreeSIM').data('orderid', idmodal);     //keep it as global var at modalPaygFreeSIM div
        $('#modalPaygFreeSIM').modal('show');
        getItemOrder(idmodal);
        return false;
    });

    $('#modalPaygFreeSIM').on('show', function () {      //when this show, make datepicker on top
        $(".datepicker").addClass("ontop");
        generateICCIDInput();
    });

    function generateICCIDInput() {
        var id = $('#modalPaygFreeSIM').data('orderid');
        var qty = $("#order-qty-" + id).text();
        $(".trIccid").remove();
        //alert("qty = "+qty);
        var trIcc = '';
        for (var i = 1; i <= qty; i++) {
            trIcc += '<tr class="trIccid">';
            trIcc += '<td>ICCID ' + i + ':</td>';
            trIcc += '<td><input type="text" class="m-wrap medium" id="paygfreesim-iccid' + i + '" /></td>';
            trIcc += '</tr>';
        }
        $('.div-paygfreesimstatus').after(trIcc);
    }
    
    $('#submitPaygFreeSIM').click(function (event) {
        return false;
    });
   
    $('#filterDateTo, #filterDateFrom, #divDtDispatch').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });

    $('#filter-datefrom, #filter-dateto, #date-outofstockdelay, #filter-dispatchdate').inputmask("99/99/9999");

    $('#blogout').click(function () {
        $('#logoutForm').submit();
    });

    /* when modal tracking and provision is hidden, show again the grid order items */
    //$('#modalTracking, #modalProvision').on('hidden', function () {        
    $('#modalTracking').on('hidden', function () {
        $('#modalPaygFreeSIM').modal('show');
        getItemOrder($('#modalPaygFreeSIM').data('orderid'));    //recreate table item order list
        //insertOrderDetail($('#modalPaygFreeSIM').data('orderid'));
    });

    $('#modalProvision').on('hidden', function () {
        $('#modalPaygFreeSIM').modal('hide');
    });

    $('.actionOrdStatusDetail').live('click', function (ev) {
        var idmodal = $(this).attr("title");    //order_id located at title attribute
        $('#modalPaygFreeSIM').data('orderid', idmodal);     //keep it as global var at modalPaygFreeSIM div
        $('#modalPaygFreeSIM').modal('show');
        getItemOrder(idmodal);
        //insertOrderDetail(idmodal);
        return false;
    });

    $('#cancelItemOrder').click(function (event) {        
        var arrChkItemOrder = [];
        jQuery("input[type=checkbox][name=check-itemorder]").map(function () {
            if (jQuery(this).is(':checked'))
                arrChkItemOrder.push($(this).attr("id"));
        });        
        cancelOrderAjax(arrChkItemOrder.toString());
        return false;
    });

    /*----------------------------------------------DISPATCH------------------------------------------------*/

    $('.actionOrdStatusDispatch').live('click', function (ev) {
        var idOrder = $(this).attr("title");                 //get the order id from attr titl
        var idItemOrder = $("#itemorder-" + idOrder + "").val();    //get the itemId 
        $('#modalPaygFreeSIM').data('orderid', idOrder);     //keep it as global var at modalPaygFreeSIM div
        arrChkDispatchOrder.push(idItemOrder);
        $('#modalTracking').modal('show');
        return false;
    });

    $('#dispatchItemOrder').click(function (event) {        
        jQuery("input[type=checkbox][name=check-itemorder]").map(function () {
            if (jQuery(this).is(':checked'))
                arrChkDispatchOrder.push($(this).attr("id"));
        });
        $('#modalPaygFreeSIM').modal('hide');
        $('#modalTracking').modal('show');
        return false;
    });    

    $("#submitDispatch").click(function (event) {
        var trackingcode = $("#tracking-code").val();
        var listitems = arrChkDispatchOrder.toString();        
        var dispatchData = {
            TrackingCode: trackingcode,
            ListItems: listitems,
            UserLogin: $("#VCCRM2").data("username")
        };        
        $.ajax({
            type: "GET",
            url: "/Order/SubmitDispatch/",
            data: dispatchData,
            cache: true,
            dataType: "json",
            success: function (data, status) {
                alert(data[0].errmsg);
                if (parseInt(data[0].errcode) == 0) {    //success
                    $('#modalTracking').modal('hide');
                    sendEmail($('#modalPaygFreeSIM').data('orderid'), "dispatch");
                    //customerChangeStatus($('#modalPaygFreeSIM').data('orderid'));
                }
                
            },
            error: function (data, status) {
                alert(status, data);
            },
            complete: function () {
                $("#tracking-code").val("");
                arrChkDispatchOrder.length = 0; //clear array
            }
        });
        return false;
    });

    function setDispatchAction() {
        $('.dispatchAction').click(function (event) {
            var altID = $(this).attr("alt");            
            //arrChkDispatchOrder.push(altID);
            $('#modalPaygFreeSIM').modal('hide');
            $('#modalTracking').modal('show');
            return false;
        });
    }

    /*----------------------------------------------END DISPATCH------------------------------------------------*/

    /*-------------------------------------------------PRINT ------------------------------------------*/
    $('.actionOrdStatusPrint').live('click', function (ev) {
        var idOrder = $(this).attr("title");                 //get the order id from attr titl
        var idItemOrder = $("#itemorder-" + idOrder + "").val();    //get the itemId (this input hidden type)
        $('#modalPaygFreeSIM').data('orderid', idOrder);     //keep it as global var at modalPaygFreeSIM div
        $("#printOrder").trigger("click");      //execute printOrder
        return false;
    });

    $('#printOrder').click(function (event) {        
        var x = screen.width / 2 - 500 / 2;
        var y = screen.height / 2 - 600 / 2;
        var myWindow = null;
        if ((myWindow == null) || (myWindow.closed)) {
            myWindow = window.open('/Order/PrintOrder?id=' + $('#modalPaygFreeSIM').data('orderid') + '', 'Print The Order', 'height=600,width=500,resizable=yes,left=' + x + ',top=' + y);
            myWindow.focus();
        } else {
            myWindow.location.href = url;
            myWindow.focus();
        }
        return false;
    });

    
    /* -------------------------------------------- PROVISION AREA --------------------------------------------*/
    $('.actionOrdStatusProvision').live('click', function (ev) {
        var idOrder = $(this).attr("title");                 //get the order id from attr title
        var idItemOrder = $("#itemorder-" + idOrder + "").val();    //get the itemId 
        $('#modalPaygFreeSIM').data('orderid', idOrder);             //keep it as global var at modalPaygFreeSIM div (the idOrder that active)
        $('#modalPaygFreeSIM').modal('hide');
        $('#modalProvision').modal('show');
        $("#provision-id").text(idItemOrder);
        getProvisionToDisplay(idItemOrder);       //temporary, enable all field provisioning
        return false;
    });
    /*
    $("#submitProvision").click(function (event) {
        var handsetSerial = $("#provision-handset").val();
        var iccid = $("#provision-simiccid").val();
        var voucher = $("#provision-voucher").val();
        var id = $("#provision-id").text();
        var ispayg = $('#modalProvision').data('ispayg');   //only paym product will execute DD & payment package
        //alert("ispayg = " + ispayg);
        $('#modalPaygFreeSIM').data('iccid', iccid); //put iicid input by user to var global
        var provisionData = {
            OrderItem: id,
            HandsetSerial: handsetSerial,
            IccID: iccid,
            VoucherSerial: voucher,
            UserLogin: $("#VCCRM2").data("username")
        };

        $.ajax({
            type: "GET",
            url: "../Order/SubmitProvision/",
            data: provisionData,
            cache: true,
            dataType: "json",
            success: function (data, status) {
                alert(data[0].errmsg);
                if (parseInt(data[0].errcode) == 0) {    //success
                    $('#modalProvision').modal('hide');
                    if (ispayg == 0) {  //only paym will execute DD & PaymentPackage
                        //sendEmail($('#modalPaygFreeSIM').data('orderid'), "provision");        //no need to sent mail 
                        webPaymentInsertDirectDebitOrder($('#modalPaygFreeSIM').data('orderid'));
                    } else {
                        //alert("pay as u go product");
                    }
                }
            },
            error: function (data, status) {
                alert(status, data);
            },
            complete: function () {
                resetFieldProvision();
            }
        });
        return false;
    });
    */

    $("#provisionClose").click(function (event) {
        $('#modalProvision').modal('hide');
    });

    function resetFieldProvision() {
        $('#provision-simiccid, #provision-voucher, #provision-handset').val("");        
    }

    function setProvisionAction() {
        $('.provisionAction').click(function (event) {
            var altID = $(this).attr("alt");
            $('#modalPaygFreeSIM').modal('hide');
            $('#modalProvision').modal('show');
            $("#provision-id").text($("#row-id-" + altID).text());
            //$("#provision-desc").text($("#row-desc-" + altID).text());
            getProvisionToDisplay(altID);
            return false;
        });
    }

    function getProvisionToDisplay(itemId) {
        $.ajax({
            type: "GET",
            url: "/Order/GetProvisionDisplay/" + itemId,
            cache: true,
            dataType: "json",
            success: function (data, status) {
                //setAttributeProvisionForm(data);
            },
            error: function (data, status) {
                alert(status, data);
            },
            complete: function () {
                //
            }
        });
    }

    function setAttributeProvisionForm(data) {
        var showhandset = data[0].show_handset;
        var showvoucher = data[0].show_voucher;
        var showsim = data[0].show_SIM;
        var ispayg = data[0].is_payg;
        var idOrder = $('#modalPaygFreeSIM').data('orderid');
        var descOrder = $("#order-desc-" + idOrder).text(); //get desc order from front screen order
        $('#modalProvision').data('ispayg', ispayg);
        $("#provision-desc").html(descOrder.replace(/-/g, '<br/>'));
        //commented below so all field enable
        /*if (parseInt(showhandset) == 1) $("#provision-handset").removeAttr("disabled");
        else $("#provision-handset").attr("disabled",true);
        if (parseInt(showvoucher) == 1) {
            $("#provision-voucher").removeAttr("disabled");
        } else $("#provision-voucher").attr("disabled",true);
        if (parseInt(showsim) == 1) {
            $("#provision-simiccid").removeAttr("disabled");
        } else $("#provision-simiccid").attr("disabled", true);*/
        
    }   

    function cancelOrderAjax(arrItemId) {
        $.ajax({
            type: "GET",
            url: "/Order/CancelOrder/" + arrItemId,
            cache: true,
            dataType: "json",
            success: function (data, status) {
                alert(data[0].errmsg);
                getItemOrder($('#modalPaygFreeSIM').data('orderid'));    //recreate table item order list
            },
            error: function (data, status) {
                alert(status, data);
            },
            complete: function () {
                //
            }
        });
    }
   
    function resetFieldPaygDetail() {
        $('#paygfreesim-title, #paygfreesim-firstname, paygfreesim-lastname').text("");
        $('#paygfreesim-address, #paygfreesim-city, paygfreesim-postcode').text("");
        $('#paygfreesim-telephone, #paygfreesim-mobilephone, paygfreesim-email').text("");
    }
   
    function getItemOrder(id) {
        $.ajax({
            type: "GET",
            url: "/FreeSIM/ItemOrder/" + id,
            cache: true,
            dataType: "json",
            beforeSend : function() {
                resetFieldPaygDetail();
            },
            success: function (data, status) {
                if (data.length > 0) {
                    insertItem(data);
                    setProvisionAction();   //activate provisionaction button                    
                    setDispatchAction();   //activate dispatchaction button                    
                } else {

                }
            },
            error: function (data, status) {
                alert(status, data);
            },
            complete: function () {
                jQuery("#ajax-loader", "#tItemOrder > tbody").remove();
            }
        });
    }
   
    function insertItem(data) {
        $(data).each(function (i) {
            $("#paygfreesim-title").html(data[i].title);
            $("#paygfreesim-firstname").html(data[i].first_name);
            $("#paygfreesim-lastname").html(data[i].last_name);
            $("#paygfreesim-email").html(data[i].email);
            $("#paygfreesim-address").html(data[i].address);
            $("#paygfreesim-city").html(data[i].city);
            $("#paygfreesim-postcode").html(data[i].postcode);
            $("#paygfreesim-telephone").html(data[i].telephone);
            $("#paygfreesim-mobilephone").html(data[i].mobile_phone);
        });
    }

    function insertOrderDetail(id) {
        resetFieldDetail();
        $.ajax({
            type: "GET",
            url: "../Customer/GetCustomerByOrderId/" + id,
            cache: true,
            dataType: "json",
            success: function (data, status) {                
                if (data.length > 0) {
                    $('#detail-orderref').text(id);
                    $('#detail-orderdate').text($("#order-date-" + id + "").text());
                    $('#detail-customer').text(data[0].first_name + " " + data[0].last_name);
                    $('#detail-source').text(data[0].order_source);
                    $('#detail-address').text(data[0].address_1 + ", " + data[0].address_2 + ", " + data[0].post_code);
                    $('#detail-paymentref').text(data[0].payment_ref);
                    $('#detail-email').text(data[0].email_address);
                    $('#detail-contacttelp').text(data[0].contact_number);
                }
            },
            error: function (data, status) {
                alert(status, data);
            },
            complete: function () {
                //
            }
        });
    }    

    function setTextAction(data, i) {
        var txtAction = "";        
        if (parseInt(data[i].is_to_dispatch) == 1) txtAction = "Dispatch";
        else if (parseInt(data[i].is_RMA) == 1) txtAction = "Return";
        else if (parseInt(data[i].is_to_provision) == 1) txtAction = "Provision";            
        return txtAction;
    }  

    function isProvisionAction(i, data) {
        var sOut = '';
        switch (data[i].order_status) {
            case "Fail":
                sOut = '<td id="row-textaction-' + i + '"></td>';
                break;
            case "Awaiting Dispatch":
                sOut = '<td id="row-textaction-' + i + '" valign="top"><a class="btn blue dispatchAction" alt="' + data[i].order_id + '" href="#">Dispatch</a></td>';
                break;
            default: sOut = '<td id="row-textaction-' + i + '"></td>'; break;
        }
        return sOut;
    }

    function checkBoxItem() {
        $('#item-checkall').click(function (event) {            
            if ($(this).is(':checked')) {                
                jQuery("input[type=checkbox][name=check-itemorder]").map(function () {
                    jQuery(this).attr("checked", true);	//all checkbox checked
                });
            } else {
                jQuery("input[type=checkbox][name=check-itemorder]").map(function () {
                    jQuery(this).attr("checked", false);	//all checkbox unchecked
                });
            }            
        });      
    }

    $("#select-orderdetail").change(function () {
        var selectedVal = $("option:selected", this).val();
        $(".searchfilter-input").show();
        $("#input-filterorderdetail").attr("name", selectedVal);  //give name attribute 
    });

    $("#proceedOutOfStock").click(function (event) {
        $('#modalProvision').modal('hide');
        $('#myodal').modal('hide');
        $('#modalOutOfStock').modal('show');
    });

    $('#modalOutOfStock').on('show', function () {      //when this show, make datepicker on top
        $(".datepicker").addClass("ontop");
    });
    

    $("#cancelOutOfStock").click(function (event) {
        $('#modalOutOfStock').modal('hide');
    });

    /*
    $('input.date').datepicker({
        format: 'dd/mm/yyyy'
    }); 
    */
});