﻿$(document).ready(function () {
    // Tooltips
    $('[title]').tooltip({
        placement: 'top'
    });

    $('#username').focus();
    $('#username').css('text-transform', 'uppercase');
});