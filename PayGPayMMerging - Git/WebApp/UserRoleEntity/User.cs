//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CRM3.WebApp.UserRoleEntity
{
    using System;
    using System.Collections.Generic;
    
    public partial class User
    {
        public User()
        {
            this.PermissionRecords = new HashSet<PermissionRecord>();
        }
    
        public int Id { get; set; }
        public System.Guid UserGuid { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int PasswordFormatId { get; set; }
        public string PasswordSalt { get; set; }
        public string AdminComment { get; set; }
        public bool Active { get; set; }
        public bool Deleted { get; set; }
        public bool IsSystemAccount { get; set; }
        public string SystemName { get; set; }
        public string LastIpAddress { get; set; }
        public System.DateTime CreatedOnUtc { get; set; }
        public Nullable<System.DateTime> LastLoginDateUtc { get; set; }
        public System.DateTime LastActivityDateUtc { get; set; }
        public string sitecode { get; set; }
        public Nullable<int> role_id { get; set; }
        public Nullable<System.DateTime> queue_assigned { get; set; }
        public int is_logoff { get; set; }
        public int assign_email { get; set; }
    
        public virtual ICollection<PermissionRecord> PermissionRecords { get; set; }
    }
}
