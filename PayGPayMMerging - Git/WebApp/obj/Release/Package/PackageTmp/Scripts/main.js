﻿$(document).ready(function () {    
    var DUNI = $('#lara').data('DUNI');
    var orderIdChosen = $('#myModal').data('orderid');
    var ispayg = $('#modalProvision').data('ispayg');
    var simIICID = $('#myModal').data('iccid');
    var arrChkDispatchOrder = [];

    /*$('#filter-dateto').datepicker()
    .on('changeDate', function (ev) {
        if (ev.date.valueOf('#filter-datefrom') > date.valueOf('#filter-dateto')) {
            alert("start time may not be greater than end time");
        }
    });*/
    /*
    var startDate = new Date(2012, 1, 20);
    var endDate = new Date(2000, 1, 2);

    $('#filter-dateto')
        .datepicker({
            format: 'dd/mm/yyyy'
        })
        .on('changeDate', function (ev) {
            if (ev.date.valueOf() > endDate.valueOf()) {
                alert('The date from must be before the end date (date to).');
                $('#alert').show().find('strong').text('The start date must be before the end date.');
            } else {
                $('#alert').hide();
                startDate = new Date(ev.date);
                alert($('#filter-dateto').data('date'));
            }
            $('#filter-dateto').datepicker('hide');
        });
        */

    $('#tPaging').dataTable({
        "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "sPaginationType": "bootstrap",
        "aaSorting": [],
        "bFilter": false,
        "iDisplayLength": 10,
        "aLengthMenu": [[10, 20, 50, 150, -1], [10, 20, 50, 150, "All"]],
        "oLanguage": {
            "sLengthMenu": "_MENU_ per page",
            "oPaginate": {
                "sPrevious": "Prev",
                "sNext": "Next"
            }
        },
        /*
        "aoColumns": [
           { "sType": "num-html" },
           null, null, null, null, null, null, null, null, null, null, null
        ],
        "aaSorting":[],
        "aoColumnDefs": [{
            'bSortable': false,
            'aTargets': [11]    //disabled sortring
        }],*/
        /*"bAutoWidth": false,
        "aoColumns": [
            { sWidth: '2%' },
            { sWidth: '9%' },
            { sWidth: '9%' },
            { sWidth: '9%' },
            { sWidth: '10%' },
            { sWidth: '10%' },
            { sWidth: '9%' },
            { sWidth: '9%' },
            { sWidth: '9%' },
            { sWidth: '9%' },
            { sWidth: '10%' }],
        "sScrollX": "100%",
        "sScrollXInner": "140%",
        "bScrollCollapse": true*/
    });

    jQuery('#tPaging_wrapper .dataTables_filter input').addClass("m-wrap small"); // modify table search input
    jQuery('#tPaging_wrapper .dataTables_length select').addClass("m-wrap xsmall"); // modify table per page dropdown

    $('#filter-datefrom, #filter-dateto, #divDtOutStock, #input-filterorderdatefrom, #input-filterorderdateto').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });

    $('#filter-datefrom, #filter-dateto, #date-outofstockdelay, #input-filterorderdatefrom, #input-filterorderdateto').inputmask("99/99/9999");

    $('#blogout').click(function () {
        $('#logoutForm').submit();
    });
      
    

    /*
    $('input.date').datepicker({
        format: 'dd/mm/yyyy'
    }); 
    */
});