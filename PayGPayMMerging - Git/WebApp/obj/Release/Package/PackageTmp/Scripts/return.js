﻿$(document).ready(function () {    
    var DUNI = $('#lara').data('DUNI');
    var orderIdChosen = $('#myModal').data('refundcode');
    var simIICID = $('#myModal').data('iccid');
    var itemOrderId = $('#modalReject').data('itemid');
    var arrChkDispatchOrder = [];


    $('#tPaging').dataTable({
        "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "sPaginationType": "bootstrap",
        "iDisplayLength": 5,
        "aLengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
        "oLanguage": {
            "sLengthMenu": "_MENU_ per page",
            "oPaginate": {
                "sPrevious": "Prev",
                "sNext": "Next"
            }
        },
        /*
        "aoColumns": [
           { "sType": "num-html" },
           null, null, null, null, null, null, null, null, null, null, null
        ],*/
        "aaSorting":[]
        /*
        "aoColumnDefs": [{
            'bSortable': false,
            'aTargets': [11]    //disabled sortring
        }],*/
        /*"bAutoWidth": false,
        "aoColumns": [
            { sWidth: '2%' },
            { sWidth: '9%' },
            { sWidth: '9%' },
            { sWidth: '9%' },
            { sWidth: '10%' },
            { sWidth: '10%' },
            { sWidth: '9%' },
            { sWidth: '9%' },
            { sWidth: '9%' },
            { sWidth: '9%' },
            { sWidth: '10%' }],
        "sScrollX": "100%",
        "sScrollXInner": "140%",
        "bScrollCollapse": true*/
    });

    jQuery('#tPaging_wrapper .dataTables_filter input').addClass("m-wrap small"); // modify table search input
    jQuery('#tPaging_wrapper .dataTables_length select').addClass("m-wrap xsmall"); // modify table per page dropdown

    $('#filter-datefrom, #filter-dateto').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });

    $('#filter-datefrom, #filter-dateto').inputmask("99/99/9999");

    $('#blogout').click(function () {
        $('#logoutForm').submit();
        return false;
    });

    $('#modalReject').on('hidden', function () {
        $('#myModal').modal('show');
        getRefundItem($('#myModal').data('refundcode'));    //recreate table item order list   
        return false;
    })
    
    /* orderlist */

    $('.actionViewDetail').live('click', function (ev) {
        var idmodal = $(this).attr("title");    //order_id located at title attribute
        $('#myModal').data('refundcode', idmodal);     //keep it as global var at myModal div
        $('#myModal').modal('show');
        getRefundItem(idmodal);
        return false;
    });

    $('.btmodal').live('click', function (ev) {
        var idmodal = $(this).text();       //the order id
        $('#myModal').data('refundcode', idmodal);     //keep it as global var at myModal div
        $('#myModal').modal('show');
        getRefundItem(idmodal);        
        return false;
    });


    /*----------------------------------------------REJECT------------------------------------------------*/

    function resetFieldReject() {
        $('#reject-datepostage, #reject-totalrefundamount, #reject-comment').val("");
    }

    function setRejectAction() {
        var itemId = $('#modalReject').data('itemid');
        $('#reject-datepostage').inputmask("99/99/9999");
        $('#reject-totalrefundamount').inputmask({ "mask": "9", "repeat": 9, "placeholder": "" });

       

        $('.actionReject').click(function (event) {
            var orderItem = $(this).attr("title");
            
            $('#modalReject').data('itemid', orderItem);
            $('#myModal').modal('hide');
            $('#modalReject').modal('show');
            return false;
        });
    }

    function rejectReturns(id) {
        var rejectComment = $("#reject-comment").val();
        var refundAmount = $("#reject-totalrefundamount").val();
        var paramReject = {
            item_order_id: id,
            comment: rejectComment,
            refund_amount: refundAmount
        };
        $.ajax({
            type: "GET",
            url: "/Return/RejectReturns/",
            cache: true,
            dataType: "json",
            data:paramReject, 
            success: function (data, status) {
                alert(data[0].errmsg);
            },
            error: function (data, status) {
                alert(status, data);
            },
            complete: function () {

            }
        });
    }

    $('#submitReject').click(function (event) {
        rejectReturns($('#modalReject').data('itemid'));
    });


    $('#reject-datepostage').change(function (event) {
        var itemId = $('#modalReject').data('itemid');

        var datefrom = $("#reject-datepostage").val();
        var daterequest = $("#refund-requestdate-" + itemId + "").text();
        var datePar = {
            dateFrom: datefrom,
            dateReq: daterequest
        };
        $.ajax({
            type: "GET",
            url: "/Return/ValidatePostage/",
            cache: true,
            dataType: "json",
            data: datePar,
            success: function (data, status) {
                if (data.errcode==1) alert(data.errmsg);
            },
            error: function (data, status) {
                //alert(status, data);
            },
            complete: function () {

            }
        });
    });
    
    /* -------------------------------------------- ACCEPT AREA --------------------------------------------*/
    function setAcceptItem() {
        $('.actionAccept').click(function (event) {
            var itemNo = $(this).attr("title");
            acceptReturns(itemNo);           
            return false;
        });
    }

    function acceptReturns(id) {
        $.ajax({
            type: "GET",
            url: "/Return/AcceptReturns/" + id,
            cache: true,
            dataType: "json",
            success: function (data, status) {
                alert(data[0].errmsg);
            },
            error: function (data, status) {
                alert(status, data);
            },
            complete: function () {

            }
        });
    }

    function getRefundItem(id) {
        $.ajax({
            type: "GET",
            url: "/Return/GetItemRefund/" + id,
            cache: true,
            dataType: "json",
            beforeSend : function() {
                $("#tItemRefund > tbody").html("");	//empty the body table first
                $("#ajax-loader").clone(true).appendTo("#tItemRefund > tbody").show();
            },
            success: function (data, status) {
                if (data.length > 0) {
                    createTableItem(data);
                    resetFieldReject();
                    setAcceptItem();      //activate acceptaction function
                    setRejectAction();  //activate reject function
                } else {
                    var sOut = '<tr><td colspan="5">no item</td></tr>';
                    jQuery("#tItemRefund > tbody:first").append(sOut);
                }
            },
            error: function (data, status) {
                alert(status, data);
            },
            complete: function () {
                jQuery("#ajax-loader", "#tItemRefund > tbody").remove();
            }
        });
    }


    function createTableItem(data) {
        var refundCode = $('#myModal').data('refundcode');        
        $(data).each(function (i) {
            var sOut = '<tr id=' + i + '>';
            sOut += '<td id="row-itemno-' + data[i].returned_code + '">' + data[i].order_item_id + '</td>';
            sOut += '<td id="row-itemtype-' + data[i].returned_code + '">' + data[i].order_source + '</td>';
            sOut += '<td id="row-desc-' + data[i].returned_code + '">' + data[i].description.replace(/-/g, '<br />'); + '</td>';
            sOut += '<td id="row-refundamount-' + data[i].returned_code + '">' + data[i].refund_amount + '</td>';
            sOut += '<td>';
            sOut += '<div class="btn-group"><a class="btn dropdown-toggle" data-toggle="dropdown" href="#">';
            sOut += 'Action <i class="icon-angle-down"></i></a>';
            sOut += '<ul class="dropdown-menu">';
            sOut += '<li><a class="actionAccept" title="' + data[i].order_item_id + '" href="#">Accept</a></li>';
            sOut += '<li><a class="actionReject" title="' + data[i].order_item_id + '" href="#">Reject</a></li>';
            sOut += '</ul></div></td>';
            sOut += '</tr>';
            i = i + 1;
            jQuery("#tItemRefund > tbody:first").append(sOut);
        });
    }   
});