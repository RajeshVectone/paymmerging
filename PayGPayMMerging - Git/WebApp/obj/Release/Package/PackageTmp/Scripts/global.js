﻿$(document).ready(function () {
    //$("ul li.has-sub a").click(function () {
    $("ul.sub li a").click(function () {
        initLoader();
    });
});

function maskInput() {
    var key_code = window.event.keyCode;
    var oElement = window.event.srcElement;
    if (!window.event.shiftKey && !window.event.ctrlKey && !window.event.altKey) {
        if ((key_code > 47 && key_code < 58) ||
            (key_code > 95 && key_code < 106)) {

            if (key_code > 95)
                key_code -= (95 - 47);
            oElement.value = oElement.value;
        } else if (key_code == 8) {
            oElement.value = oElement.value;
        } else if (key_code != 9) {
            event.returnValue = false;
        }
    }
}

function initLoader() {
    closeLoader();
    $('#basic-simpmodal-content').simpmodal();
    $('#myloader').show();
}

function closeLoader() {
    $.simpmodal.close();
    $('#myloader').hide();
}

function getCustomerByOrderId(orderId) {
    $.ajax({
        type: "GET",
        url: "/Customer/GetCustomerByOrderId/" + orderId,
        cache: true,
        dataType: "json",
        success: function (data, status) {
            res = data;
        },
        error: function (data, status) {
            alert(status, data);
        },
        complete: function () {
            //
        }
    });   
}

function sendEmail(id, type) {
    var urlSer = "";
    
    switch (type) {
        case "provision": urlSer = "/Report/SendEmailProvision/"; break;
        case "dispatch": urlSer = "/Report/SendEmailDispatch/"; break;
        case "cancelOrder": urlSer = "../Report/SendEmailCancelOrder/"; break;
        case "rejectRefund": urlSer = "../Report/SendEmailRejectRefund/"; break;
        case "approveRefund": urlSer = "../Report/SendEmailApproveRefund/"; break;
    }
    //alert("id = " + id + "urlser = " + urlSer);
    $.ajax({
        type: "POST",
        url: urlSer + id,
        beforeSend: function () {
            $("#loader-bottom").show();
            //initLoader();
        },
        cache: true,
        dataType: "json",
        success: function (data, status) {
            alert(data.Msg);
            location.reload();
        },
        error: function (data, status) {
            alert(status+", failed to sent email notification");
        },
        complete: function () {
            $("#loader-bottom").hide();
        }
    });    
}

function webPaymentInsertDirectDebitOrder(orderId) {
    var customerName = $("#order-customer-" + orderId + "").text();        //get customer name from front screen
    var paymentRef = $("#order-paymentref-" + orderId + "").text();        //get paymentref from front screen
    //alert("paymentRef = " + paymentRef);
    var provisionData = {
        orderid: orderId,
        originatorID: "crm2",
        buildingsociety: "crm2",
        accountname : customerName,
        branchsortcode: "crm2",
        accountnumber: "crm2",
        ddref: paymentRef
    };

    $.ajax({
        type: "GET",
        url: "/Provision/InsertDirectDebitOrder/",
        cache: true,
        data: provisionData,
        dataType: "json",
        success: function (data, status) {
            alert(data.Msg);
            insertCustomerCRM();
        },
        error: function (data, status) {
            alert("failed to insert direct debit order");
        },
        complete: function () {
            
        }
    });
}

function insertCustomerCRM() {
    var idOrder = $('#myModal').data('orderid');
    //var simiiccd = $("#provision-simiccid").val();
    var simiiccd = $('#myModal').data('iccid');
    var customerName = $("#order-customer-" + idOrder + "").text();        //get customer name from front screen
    var paymentRef = $("#order-paymentref-" + idOrder + "").text();        //get paymentref from front screen    
    var planPrice = $("#order-planprice-" + idOrder + "").val();        //get paymentref from front screen    
    //alert("planPrice = " + planPrice);
    var provisionData = {
        subscriberid: 1019246,
        postpaidinfo: 2,
        crm2id: idOrder,
        crm2orderid: idOrder,
        cybersourceid : paymentRef,
        iccid: simiiccd.toString(),
        //planprice: parseFloat(planPrice),        
        planprice: planPrice,
        loginname: $("#VCCRM2").data("username"),
        fullname : customerName
    };

    $.ajax({
        type: "GET",
        url: "/Provision/InsertFreeSimCustomer/",
        cache: true,
        data: provisionData,
        dataType: "json",
        success: function (data, status) {
            alert(data.Msg);
            insertDirectDebitAccount();
            posPaidSubscribe();
        },
        error: function (data, status) {
            alert("failed to insert free sim customer");
        },
        complete: function () {

        }
    });

}

function insertDirectDebitAccount() {
    var idOrder = $('#myModal').data('orderid');
    //var simiiccd = $("#provision-simiccid").val();
    var simiiccd = $('#myModal').data('iccid');
    var customerName = $("#order-customer-" + idOrder + "").text();        //get customer name from front screen
    var paymentRef = $("#order-paymentref-" + idOrder + "").text();        //get paymentref from front screen    
    var accountName = $("#order-accountname-" + idOrder + "").val();
    var accountNumber = $("#order-accountnumber-" + idOrder + "").val();
    var sortCode = $("#order-sortcode-" + idOrder + "").val();
    var planPrice = $("#order-planprice-" + idOrder + "").val();    
    var provisionData = {
        subscriberid: 1019246,
        iccid: simiiccd.toString(),
        paymentref: paymentRef,
        sort_code: sortCode,   //dari sort_code
        account_num: accountNumber,
        account_name: accountName,
        fullname: customerName,
        amount: parseFloat(planPrice)
    };

    $.ajax({
        type: "GET",
        url: "/Provision/InsertDirectDebitAccount/",
        cache: true,
        data: provisionData,
        dataType: "json",
        success: function (data, status) {
            alert("direct debit account ok");
        },
        error: function (data, status) {
            //alert("failed to postpaidsubscribe");
        },
        complete: function () {

        }
    });
}

function posPaidSubscribe() {
    var idOrder = $('#myModal').data('orderid');    
    var simiiccd = $('#myModal').data('iccid');
    var customerName = $("#order-customer-" + idOrder + "").text();        //get customer name from front screen
    var paymentRef = $("#order-paymentref-" + idOrder + "").text();        //get paymentref from front screen        
    var bundleId = $("#order-bundleid-" + idOrder + "").val();
    var planprice = $("#order-planprice-" + idOrder + "").val();    
    /*
    exec WEB_postpaid_subscribe_admin
                  'MCM', 2, @mobileno, 0, @creditinit, @creditinit, 'VPMS',
                  30, 1, @renew_datemode, @renew_chosendate, @bundleid, @processby,
                  @errcode output, @errmsg output

    */

    var provisionData = {
        sitecode: "MCM",
        usertype: 2,
        iccid: simiiccd.toString(),
        is_postpaid: 0,
        creditinit: parseFloat(planprice) * 100,   /*plan price * 100*/
        new_trffclass: "PSIM",
        renew_delay:30,
        renew_mode:1,
        renew_datemode:5,        
        bundle_id: bundleId, /* bundle id*/
        processby : "VCCRM2"
    };

    $.ajax({
        type: "GET",
        url: "/Provision/PostpaidSubscribe/",
        cache: true,
        data: provisionData,
        dataType: "json",
        success: function (data, status) {
            //alert(data[0].errmsg);
        },
        error: function (data, status) {
            //alert("failed to postpaidsubscribe");
        },
        complete: function () {

        }
    });
}

function customerChangeStatus(idOrder) {
    //var ppCustomer = $("#pp-customer").val();        
    var provisionData = {
       //pp_customer: ppCustomer,
       status : "Active",
       pmbo_status : 2,
       createby : $("#VCCRM2").data("username"),  
       remark : "-"
    };
    $.ajax({
        type: "GET",
        url: "/Dispatch/CustomerChangeStatus/",
        cache: true,
        data: provisionData,
        dataType: "json",
        success: function (data, status) {
            alert(data.Msg);
        },
        error: function (data, status) {
            alert("failed, service status customers in table pp_customers is not found");
        },
        complete: function () {

        }
    });

}

