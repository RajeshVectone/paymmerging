﻿$(document).ready(function () {
    //getMoreInfoSubscriber();

    /*  how to parseDate format JSON
        http://techneerajnandwana.blogspot.com/2011/10/javascript-serialize-json-object-to.html
    */
    function rxFn(str, m1) {
        var d = new Date(parseInt(m1, 10));
        return d.format('dd-mm-yyyy')
    }

    $('#chistDateFrom, #chistDateTo, #sCustActDate').datepicker({
        //format: 'dd/mm/yyyy',
        //autoclose: true
    });

    $('#chist-datefrom, #chist-dateto, #sCustActDate-dt').inputmask("99/99/9999");

    function parseJsonDate(d) {
        /*var str = Ext.encode(d);
        var result = str.replace(/\/Date\(([-+]?\d+)\)\//gi, rxFn);
        */       
        var date = new Date(parseInt(d.substr(6)));
        //var date = d.replace(/[\\/]/g, "");
        //date = eval("new " + d.replace(/[\\/]/g, ""))
        return date;
    }

    function getMoreInfoSubscriber() {
        var mobileno = $("#detail-mobileno").text() == "" ? '0' : jQuery.trim($("#detail-mobileno").text())
        //alert("mobileno = "+mobileno);
        var params = {
            MobileNumber : mobileno
        };
        /*
        result : 
       [{"activeimsi":"234010012858812","masterimsi":"234010012858812","sitecode":"MCM",
       "strstatus":"Active","mnrf":"0","mcef":"0","vlr":"","firstupd":"\/Date(-62135596800000)\/",
       "lastupd":"\/Date(-62135596800000)\/"}]
        */
        $.ajax({
            type: "GET",
            url: "../Customer/MoreInfoSubscriber/",
            cache: true,
            data: params,
            dataType: 'json',
            success: function (data, status) {
                //alert(data);
                var datas = {
                    activeimsi: data[0].activeimsi,
                    masterimsi: data[0].masterimsi,
                    sitecode: data[0].sitecode,
                    strstatus: data[0].strstatus,
                    mnrf: data[0].mnrf,
                    mcef: data[0].mcef,
                    vlr: data[0].vlr == "" ? "-" : data[0].vlr,
                    firstupdate: parseJsonDate(data[0].firstupd),
                    lastupdate: parseJsonDate(data[0].lastupd)
                };
                updateDataMoreInfo(datas);
            },
            error: function (data, status) {
                alert(status, data);
            },
            complete: function () {
                getMoreAccountStatus(mobileno);
            }
        });
    }

    function updateDataMoreInfo(data) {
        $("#imsiactive").text(data.activeimsi);
        $("#imsimaster").text(data.masterimsi);
        $("#firstupdate").text(data.firstupdate);
        $("#lastupdate").text(data.lastupdate);
        $("#currentvlr").text(data.vlr);
        $("#mnrf").text(data.mnrf);
    }

    function getMoreAccountStatus(mobileno) {
        var params = {
            MobileNumber: mobileno
        };
        $.ajax({
            type: "GET",
            url: "../Customer/MoreInfoAccountStatus/",
            cache: true,
            data: params,
            dataType: 'json',
            success: function (data, status) {
                if (data.length > 0) {
                    var datas = {
                        autotopup: data[0].autotopup,
                        topupthreshold: data[0].topupthreshold,
                        status: data[0].textstatus,
                        balance: data[0].balance,
                        currcode: data[0].currcode,
                        trffclass: data[0].trffclass
                    };
                    updateDataAccountStatus(datas);
                }
            },
            error: function (data, status) {
                alert(status, data);
            },
            complete: function () {
                getSimType();
            }
        });
    }

    function updateDataAccountStatus(data) {
        $("#currcode").text(data.currcode);
        $("#general-status").text(data.status);
        $("#balance").text(data.balance);
    }

    function getSimType() {
        var iccid = jQuery.trim($("#detail-iccid").text())
        var params = {
            ICCID: iccid
        };
        $.ajax({
            type: "GET",
            url: "../Customer/MoreInfoSimType/",
            cache: true,
            data: params,
            dataType: 'json',
            success: function (data, status) {
                if (data.length > 0) {
                    var datas = {
                        freesimstatus: data[0].freesimstatus,
                        freesimstatusname: data[0].freesimstatusname,
                        freesimstatusdesc: data[0].freesimstatusdesc
                    };
                    $("#simtype").text(datas.freesimstatusname);
                }
            },
            error: function (data, status) {
                alert(status, data);
            },
            complete: function () {
                //
            }
        });
    }

    /* -------------------------------------------BEGIN Payment history Tabs Customers -----------------------------*/
    $('.payhistoryPaygMore').live('click', function (ev) {
        var idmodal = "0";
        $('#modalCreditCard').data('customerid', idmodal);     //keep it as global var at myModal div
        $('#modalCreditCard').modal('show');
        getPaymentWs();
        //getItemOrder(idmodal);
        //insertOrderDetail(idmodal);
        return false;
    });

    $('#payhistoryPaymMore').live('click', function (ev) {
        var idmodal = "0"       //the order id
        $('#modalDirectDebit').data('customerid', idmodal);     //keep it as global var at myModal div
        $('#modalDirectDebit').modal('show');
        //getItemOrder(idmodal);
        //insertOrderDetail(idmodal);
        return false;
    });    

    function getPaymentWs() {
        var paramS = {
            sitecode: "SE1",
            productcode: "CTPSE1",
            paymentagent: "CTPSE",
            servicetype: "MVNO",
            paymentmode: "DEF",
            accountid: "447514719202",
            ccno: "4000000000000002",
            expdate: "201310",
            cccvv: "123",
            firstname: "Test",
            lastname: "Account",
            streetname: "5255 Satellite Drive",
            postcode: "L4W5E3",
            city: "Ontario",
            country: "CA",
            state: "Ontario",
            email: "h.binduni@switchlab.com",
            amount: "2",
            currency: "SEK",
            checkenroll: "0",
            capture: "1"
        };

        $.ajax({
            type: "POST",
            url: "../payment/authorize",
            data: paramS,
            cache: true,
            //dataType: "json",
            success: function (data, status) {
                alert(data);
            },
            error: function (data, status) {
                alert(status, data);
            },
            complete: function () {
                $("#tracking-code").val("");
                arrChkDispatchOrder.length = 0; //clear array
            }
        });
    }

    /* --------------------------END payment history tabs ------------------------------------*/

    /*------------------------------ SIM DETAIL -----------------------------*/

    $('#simbrand').change(function (event) {
        var selectedVal = $("option:selected", this).val();
        $(".reasonsim").remove();   //remove first before create object below
        //$(".reasonsim").hide();
        var tdDetail = '<tr class="reasonsim">';
        tdDetail += '<td>Does the customer have a new SIM</td>';
        tdDetail +='<td>:</td>';
        tdDetail += '<td id="customerhassimtd"><select id="selHaveSim">';
        tdDetail += '<option value="">Please Select One</option>';
        tdDetail += '<option value="Yes">Yes</option>';
        tdDetail += '<option value="No">No</option>';
        tdDetail +='</select></td>';
        tdDetail +='</tr>';
        
        if (selectedVal != "Portin" && selectedVal!="") {
            $(this).closest('tr').after(tdDetail);             
        }
        
        return false;
    });

    $('#proceedSimSwap').click(function (event) {
        var selsimbrand = jQuery("#simbrand option:selected").val();
        if (selsimbrand == "") {
            //action here defined later 
        }else {
            $("#simbrandtd").html($("#simbrand option:selected").text());
            $("#trProceedSwap").remove();       //no more proceed button after this
            if (selsimbrand == "Portin") {
                $(".trProceedPortin").show();
            }
            else {
                var selHaveSim = jQuery("#selHaveSim option:selected").val();
                if (selHaveSim == "") alert("please choose customers SIM option");
                else {
                    if (selHaveSim == "Yes") {
                        $("#customerhassimtd").html("Yes");
                        $("#trValidationSimSwap").show();
                    } else
                        $("#customerhassimtd").html("No");
                    $(".trValidationSimSwapNo").show();
                }
            }
        }
    });

    $('#validateSimSwap').click(function (event) {
        var mobileno = $("#detail-mobileno").text();
        var newmobileno = $("#new-mobilenumber").val();
        //alert(mobileno +"  :: "+newmobileno);
        if (mobileno == "" || newmobileno == "") alert("older mobile number / new mobile number is null");
        else {
            var paramS = {
                MobileNumber: mobileno,
                NewMobileNumber: newmobileno
            };
            $.ajax({
                type: "GET",
                url: "../SIM/ValidateMobileNo",
                data: paramS,
                cache: true,
                dataType: "json",
                success: function (data, status) {
                    //alert(data[0].result);
                    var res = data[0].result;
                    if (res == "valid") { generateActionSwap(); }
                    else alert("Validation has failed. Please reconfirm and re-enter the details. Thank you");
                },
                error: function (data, status) {
                    alert(status, data);
                },
                complete: function () {

                }
            });
        }
        return false;
    });

    $('#submitPortin').click(function (event) {
        var mobileno = $("#detail-mobileno").text();
        var newICCID = $("#new-iccid").val();
       // alert(mobileno + "  :: " + newICCID);
        if (mobileno == "" || newICCID == "") alert("older mobile number / iccid is null");
        else {
            
            var paramS = {
                MobileNumber: mobileno,
                ICCID: newICCID
            };
            $.ajax({
                type: "GET",
                url: "../SIM/SubmitPortin",
                data: paramS,
                cache: true,
                dataType: "json",
                success: function (data, status) {
                    alert(data[0].errmsg);
                },
                error: function (data, status) {
                    alert(status, data);
                },
                complete: function () {

                }
            });
        }
        return false;
    });
    
    function generateActionSwap() {
        //alert("ok");
        $("#trActionSwap").show();
    }

    jQuery(".optdispatch").click(function () {
        var val = $(this).attr("value");
        if (val == "curr") {
            $('#modalCurrentAddress').modal('show');
            getDispatchAdd();
        }
        else {
            $('#modalDispatchAddress').modal('show');
        }
        return false;
    });
      
    function getDispatchAdd() {
        var idCust = $("#cust-id").val();
        $.ajax({
            type: "GET",
            url: "../Customer/GetCustomerInfoDetail/" + idCust,
            cache: true,
            dataType: "json",
            beforeSend: function () {
                //$("#current-address").val("");
                $('#cur-address-1, #cur-address-2, #cur-town, #cur-postcode').val("");
            },
            success: function (data, status) {
                if (data.length > 0) {
                    var curr = data[0].address_1 + " " + data[0].address_2 + " " + data[0].post_code;
                    $("#cur-address-1").val(data[0].address_1);
                    $("#cur-address-2").val(data[0].address_2);
                    $("#cur-town").val(data[0].town);
                    $("#cur-postcode").val(data[0].post_code);
                } else {
                    //$("#current-address").val("can't find address customers");
                }
            },
            error: function (data, status) {
                alert(status, data);
            }
        });
        return false;
    }

    $('#placeOrderCurrent').click(function (event) {

    });
    
    $('#placeOrderNew').click(function (event) {

    });
    /*--------------END SIM DETAIL----------------------------*/



    /* --------------------------------BEGIN Payment profile Tabs Customers -------------------*/

    $('#tPagingPaymentProfile').dataTable({
        //"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i>>",
        "sPaginationType": "bootstrap",
        "bFilter": false,
        "bPaginate": false,
        "bInfo": false,
        "iDisplayLength": 5,
        "aLengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
        "oLanguage": {
            "sLengthMenu": "_MENU_ per page",
            "oPaginate": {
                "sPrevious": "Prev",
                "sNext": "Next"
            }
        }
    });

    $("#topupnew-expirydate").datepicker({
        format: "mm-yyyy",
        viewMode: "months",
        minViewMode: "months"
    });

    $('#modalTopUpNewCard').on('show', function () {      //when this show, make datepicker on top
        $(".datepicker").addClass("ontop");
        getCurrency();
    });

    $('#modalSavedTopUp').on('show', function () {
        getCurrency();
    });

    $('.paymentPaygMore').live('click', function (ev) {
        var idmodal = "0";
        $('#modalMoreInfoPayment').data('customerid', idmodal);     //keep it as global var at myModal div
        $('#modalMoreInfoPayment').modal('show');

        return false;
    });

    $('.paymentprofileMore').live('click', function (ev) {
        var idcard = "";
        var idcust = $("#cust-id").val();
        $('#modalMoreInfoPayment').data('creditcard', idcard);     //keep it as global var at myModal div
        $('#modalMoreInfoPayment').modal('show');
        getPaymentDetail(idcust);
        return false;
    });

    $('.savedTopUpMore').live('click', function (ev) {
        var idcard = "";
        var idcust = $("#cust-id").val();
        $('#modalSavedTopUp').data('creditcard', idcard);     //keep it as global var at myModal div
        $('#modalSavedTopUp').modal('show');
        getPaymentDetail(idcust);
        return false;
    });

    $('#payp-autotopup').live('click', function (ev) {
        var customerid = $("#cust-id").val();
        //alert("customerid = " + customerid);
        getAutoTopup(customerid);
        return false;
    });

    $('#cancelTopUpSCard').click(function (event) {
        $("#modalSavedTopUp").modal("hide");
        return false;
    });

    $('#confirmTopUpNewCard').click(function (event) {
        var loginname = $("#detail-username").text();
        var account = $(".topupnew-amount option:selected").val();
        var param = {
            UserName: loginname,
            MobileNo: mobileno
        };

        $.ajax({
            type: "POST",
            url: "/Payment/SubmitTopUp/",
            cache: true,
            data: param,
            dataType: "json",
            success: function (data, status) {
                if (data.length > 0) {  //only if data exist
                    alert(data[0].errmsg);
                }
            },
            error: function (data, status) {
                alert(status, data);
            },
            complete: function () {
                location.reload();
            }
        });

        return false;
    }); 

    $('#confirmUpSCard').click(function (event) {
        var customerid = $("#cust-id").val();
        var mobileno = $("#detail-vcloudnumber").text();
        var accountid = $("detail-username").text();
        var cardno = $("#topup-cardnumber").val();
        var expdate = $("#topup-expirydate").text();
        var amount = $(".topupnew-amount option:selected").val();  //amount topup
        var securityNumber = $("#topup-securityno").val();

        var param = {
            CustomerID  :customerid,
            CardNumber  :cardno,
            ExpDate     :expdate,
            Amount      : amount,
            SecurityNumber :securityNumber
        };

        $.ajax({
            type: "POST",
            url: "/Payment/SendPaymentServices/",
            cache: true,
            data:param,
            dataType: "json",
            success: function (data, status) {
                if (data.length > 0) {  //only if data exist
                    alert(data[0].errmsg);
                }
            },
            error: function (data, status) {
                alert(status, data);
            },
            complete: function () {
                location.reload();
            }
        });

        return false;
    });
    
    
    $('#confirmAutoTopUp').click(function (event) {
        var customerid = $("#cust-id").val();
        var statusSel = jQuery("#payp-autotopupstatus option:selected").val();
        //alert(statusSel);
        var param = {
            CustomerID: customerid,
            Status: statusSel
        };
        $.ajax({
            type: "GET",
            url: "/Payment/SetAutotopUp/" + customerid,
            cache: true,
            dataType: "json",
            success: function (data, status) {
                if (data.length > 0) {  //only if data exist
                    alert(data[0].errmsg);
                }
            },
            error: function (data, status) {
                alert(status, data);
            },
            complete: function () {
                //closeLoader();
                location.reload();
            }
        });

        return false;
    });

    $('#cancelAutoTopUp').click(function (event) {
        $('#modalAutoTopUp').modal('hide');
        return false;
    });

    function getCurrency() {
        var idcust = $("#cust-id").val();
        $.ajax({
            type: "GET",
            url: "/Payment/GetCurrency/" + idcust,
            cache: true,
            dataType: "json",
            beforeSend: function () {
                $(".ajax-loader").show();
                $('.topupnew-amount').html("");
            },
            success: function (data, status) {
                if (data.length > 0) {  //only if data exist
                    var option = '<option value="10">10' + data[0].symbolforaccount + '</option>';
                    option += '<option value="20">20' + data[0].symbolforaccount + '</option>';
                    option += '<option value="30">30' + data[0].symbolforaccount + '</option>';
                    $('.topupnew-amount').append(option);
                }
            },
            error: function (data, status) {
                alert(status, data);
            },
            complete: function () {
                $(".ajax-loader").hide();
            }
        });
    }

    function getAutoTopup(id) {
        $.ajax({
            type: "GET",
            url: "/Payment/GetAutoTopUp/" + id,
            cache: true,
            dataType: "json",          
            beforeSend: function () {
                $(".ajax-loader").show();
            },
            success: function (data, status) {
                if (data.length > 0) {  //only if data exist
                    $("#payp-autotopupstatus option[value=" + data[0].status + "]").attr("selected", "selected");	//select the dropdown option by payp-autotopupstatus value

                    if (data[0].status == 1) {  //active
                        $("#payp-autotopupstatus").removeAttr("disabled");
                        $("#payp-autotopupamount").removeAttr("disabled");
                        $("#payp-triggeramount").removeAttr("disabled");
                    }
                }               
            },
            error: function (data, status) {
                alert(status, data);
            },
            complete: function () {
                $(".ajax-loader").hide();
            }
        });
    }


    function getPaymentDetail(id) {
        $.ajax({
            type: "GET",
            url: "/Payment/GetDetail/" + id,
            cache: true,
            dataType: "json",
            beforeSend: function () {
                $("#tItemPayment tbody").html("");	//empty the body table first
                $("#payp-cardnumber").text("");
                $("#payp-expiredate").text("");

            },
            success: function (data, status) {
                if (data.length > 0) {
                    createTableItemPayp(data);
                    insertDetailPayment(data);
                } else {
                    var sOut = '<tr><td colspan="5">no item order</td></tr>';
                    jQuery("#tItemPayment > tbody:first").append(sOut);
                }
            },
            error: function (data, status) {
                //alert(status, data);
            },
            complete: function () {
                //closeLoader();
            }
        });
    }

    function createTableItemPayp(data) {
        $(data).each(function (i) {
            var sOut = '<tr id=' + i + '>';
            sOut += '<td id="row-creditcard-' + i + '">' + data[i].payment_type + '</td>';
            sOut += '<td id="row-status-' + i + '">' + data[i].status + '</td>';
            sOut += '<td id="row-setupdate-' + i + '">' + data[i].setup_date + '</td>';
            sOut += '<td id="row-lastdateofuse-' + i + '">' + data[i].lastdateofuse_string + '</td>';
            sOut += '<td id="row-lastpaymentamount-' + i + '">' + data[i].lastpaymentamount + '</td>';
            sOut += '</tr>';
            i = i + 1;
            $("#tItemPayment > tbody:first").append(sOut);
        });
    }

    function insertDetailPayment(data) {
        $(data).each(function (i) {
            /*$("#payp-carddetail").text(data[i].card_details);
            $("#payp-addressinfo").text(data[i].address_info);
            $("#payp-nameofcardholder").text(data[i].name_cardholder);
            $("#payp-street").text(data[i].street);
            $("#payp-cardtype").text(data[i].card_type);
            $("#payp-city").text(data[i].city);*/
            $('#payp-cardnumber, #topup-cardnumber').text(data[i].card_number);
            //$("#payp-country").text(data[i].country);
            $('#payp-expiredate, #topup-expirydate').text(data[i].expiry_date);
            //$("#payp-postcode").text(data[i].post_code);
        });
    }

    /* --------------------------END payment profile tabs ------------------------------------*/

    /* --------------------------BEGIN CALL HISTORY--------------------------*/
   
    var i = 1;

    $("#searchCallHistory").click(function () {
        var params = {
            CustomerID : $.trim($("#cust-id").val()),
            DateFrom : $("#chist-datefrom").val(),
            DateTo: $("#chist-dateto").val()
    };

        $.ajax({
            type: "GET",
            url: "../Customer/CallHistorySearch",
            cache: true,
            data: params,
            dataType: 'json',
            beforeSend : function () {
                jQuery("#tPagingCallHistoryInitiate tbody").html("");
               // $(".ajax-loader").show();
            },
            success: function (data, status) {
                                
                $(data).each(function (index) {
                    var sOut = '<tr id=' + i + '>';
                    sOut += '<td>' + data[index].datecallStr + '</td>';
                    sOut += '<td>' + data[index].dialednumparse + '</td>';
                    sOut += '<td>' + data[index].destination + '</td>';
                    sOut += '<td>' + data[index].time + '</td>';
                    sOut += '<td>' + data[index].duration + '</td>';
                    sOut += '<td>' + data[index].cost + '</td>';
                    sOut += '<td>' + data[index].balance + '</td>';
                    sOut += '</tr>';
                    i = i + 1;
                    jQuery("#tPagingCallHistoryInitiate > tbody:first").append(sOut);
                });
            },
            error: function (data, status) {
                alert("connection timeout, query takes to long");
            },
            complete: function () {
                initiateCallHistorySearchDataTable();
                //$(".ajax-loader").hide();
                closeLoader();
            }
        });
        return false;
    });

    function initiateCallHistorySearchDataTable() {
        $('#tPagingCallHistoryInitiate').dataTable({
            "bDestroy": true,
            "bProcessing": true,
            "bAutoWidth": false,
            "bFilter": true,
            //"sDom": "<'row-fluid'<'span6'l><'span3'><'span3'T>r>t<'row-fluid'<'span6'i><'span6'p>>",
            "sDom": "<'row-fluid'<'span3'l><'span6'f><'span3'T>r>t<'row-fluid'<'span6'i><'span6'p>>",
            "sPaginationType": "bootstrap",
            "iDisplayLength": 50,
            "aLengthMenu": [[50, 100, 250, 500, -1], [50, 100, 250, 500, "All"]],
            "oTableTools": {
                "sSwfPath": "../../Content/swf/copy_csv_xls_pdf.swf",
                "aButtons": [
				{
				    "sExtends": "pdf",
				    "sButtonText": "Download PDF",
				    "sTitle": "Customer Selected - Call History"
				    //"sPdfMessage": "Call History",
				}]
            },
            "oLanguage": {
                "sLengthMenu": "_MENU_ per page",
                "oPaginate": {
                    "sPrevious": "Prev",
                    "sNext": "Next"
                }
            }
        });
    }

    /*-----------------------PRODUCT TABS--------------------*/
        
    $("#modalCancelBundles").click(function () {
        var r = confirm("Are you sure you want to cancel bundles? ")
        if (r == true) {
            alert("Cancel bundles confirmed, action implemented later ")
        }
        else {
            //alert("You pressed Cancel!")
        }
        return false;
    });

    $('#tPagingCustVCBundles, #tPagingProductVCPlan, #tPagingCustVCTopupTransfer, #tPagingCustVCHardware').dataTable({
        "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "sPaginationType": "bootstrap",
        "bFilter": false,
        "bPaginate": false,
        "bInfo": false,
        "iDisplayLength": 5,
        "aLengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
        "oLanguage": {
            "sLengthMenu": "_MENU_ per page",
            "oPaginate": {
                "sPrevious": "Prev",
                "sNext": "Next"
            }
        }
    });

    /*------------------------------------TRANSACTION HISTORY-------------------------------*/
    $('#tPagingTransactionHistory').dataTable({
        "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "sPaginationType": "bootstrap",
        "bFilter": false,
        "iDisplayLength": 5,
        "aLengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
        "oLanguage": {
            "sLengthMenu": "_MENU_ per page",
            "oPaginate": {
                "sPrevious": "Prev",
                "sNext": "Next"
            }
        }
    });

    /*------------------------------------CONNECTIVITY STATUS -------------------------------*/
    $('#tPagingConnectivityStatus').dataTable({
        "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "sPaginationType": "bootstrap",
        "bFilter": false,
        "bPaginate": false,
        "bInfo": false,
        "iDisplayLength": 5,
        "aLengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
        "oLanguage": {
            "sLengthMenu": "_MENU_ per page",
            "oPaginate": {
                "sPrevious": "Prev",
                "sNext": "Next"
            }
        }
    });

    /*------------------------------------RETURNS REQUEST OF CUSTOMERS TAB -------------------------------*/
    $('#tPagingReturnsRequest').dataTable({
        "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "sPaginationType": "bootstrap",
        "bFilter": false,
        "bPaginate": false,
        "bInfo": false,
        "iDisplayLength": 5,
        "aLengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
        "oLanguage": {
            "sLengthMenu": "_MENU_ per page",
            "oPaginate": {
                "sPrevious": "Prev",
                "sNext": "Next"
            }
        }
    });

    $('.actionViewDetailReturn').live('click', function (ev) {
        var idmodal = $(this).attr("title");    //order_id located at title attribute
        $('#modalReject').data('refundcode', idmodal);     //keep it as global var at myModal div
        $('#modalReject').modal('show');
        //getRefundItem(idmodal);
        return false;
    });

    /*----------------------------------------------REJECT------------------------------------------------*/

    function resetFieldReject() {
        $('#reject-datepostage, #reject-totalrefundamount, #reject-comment').val("");
    }

    function setRejectAction() {
        var itemId = $('#modalReject').data('itemid');
        $('#reject-datepostage').inputmask("99/99/9999");
        $('#reject-totalrefundamount').inputmask({ "mask": "9", "repeat": 9, "placeholder": "" });



        $('.actionReject').click(function (event) {
            var orderItem = $(this).attr("title");

            $('#modalReject').data('itemid', orderItem);
            $('#myModal').modal('hide');
            $('#modalReject').modal('show');
            return false;
        });
    }

    function rejectReturns(id) {
        var rejectComment = $("#reject-comment").val();
        var refundAmount = $("#reject-totalrefundamount").val();
        var paramReject = {
            item_order_id: id,
            comment: rejectComment,
            refund_amount: refundAmount
        };
        $.ajax({
            type: "GET",
            url: "/Return/RejectReturns/",
            cache: true,
            dataType: "json",
            data: paramReject,
            success: function (data, status) {
                alert(data[0].errmsg);
            },
            error: function (data, status) {
                alert(status, data);
            },
            complete: function () {

            }
        });
    }   

});