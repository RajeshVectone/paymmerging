﻿$(document).ready(function () {    
    var DUNI = $('#lara').data('DUNI');
    var orderIdChosen = $('#myModal').data('orderid');
    var custIdChosen = $('#modalChangeAddress').data('custid');
    var arrChkDispatchOrder = [];
    var arrChkItemOrder = [];

    $('#tPagingCustOrder').dataTable({
        "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "sPaginationType": "bootstrap",
        "bFilter": false,
        "bPaginate": false,
        "bInfo": false,
        "iDisplayLength": 5,
        "aLengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
        "oLanguage": {
            "sLengthMenu": "_MENU_ per page",
            "oPaginate": {
                "sPrevious": "Prev",
                "sNext": "Next"
            }
        },/*
        "aoColumns": [
           { "sType": "num-html" },
           null, null, null, null, null, null, null, null
        ],
        "aoColumnDefs": [{
            'bSortable': false,
            'aTargets': [11]    //disabled sortring
        }]    */
    });

    $('#tPagingCust').dataTable({
        "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "sPaginationType": "bootstrap",
        "bFilter":false,
        "iDisplayLength": 5,
        "aLengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
        "oLanguage": {
            "sLengthMenu": "_MENU_ per page",
            "oPaginate": {
                "sPrevious": "Prev",
                "sNext": "Next"
            }
        },
    });

    jQuery('#tPaging_wrapper .dataTables_filter input').addClass("m-wrap small"); // modify table search input
    jQuery('#tPaging_wrapper .dataTables_length select').addClass("m-wrap xsmall"); // modify table per page dropdown

    $('#filter-datefrom, #filter-dateto').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });

    $('#filter-datefrom, #filter-dateto').inputmask("99/99/9999");

    $('#blogout').click(function () {
        $('#logoutForm').submit();
    });
    /*
    $(".viewCustAccount").click(function () {
        var id = $(this).attr("id");    //get the id of its class
        var paramSearchCust = {
            CustomerID  : $("#search-custid-" + id + "").text(),
            CustomerName: $("#search-custname-" + id + "").text(),
            AccountNo   : $("#search-custaccno-" + id + "").text(),
            MobileNumber: $("#search-custmobilno-" + id + "").text(),
            ICCID       : $("#search-custiccid-" + id + "").text(),
            Product     : $("#search-prod-" + id + "").text(),
            Status      : $("#search-status-" + id + "").text(),
            ActivationDate: $("#search-activation-" + id + "").text(),
            MyAccount   : $("#search-myaccount-" + id + "").text()
        }; //sent all param above ajax method to Detail Controller, 
        
        $.ajax({
            type: "GET",
            url: "/Customer/GetDetail/",
            cache: true,            
            data: paramSearchCust,
            dataType: 'html',
            success: function (data, status) {
                //$(document).replaceAll(data);
                
            },
            error: function (data, status) {
                alert(status, data);
            },
            complete: function () {
                //
            }
        });

    });
    */

    function getCustomerMoreInfo() {
        var customerid = $("#filter-custid").val();
        var paramCust = {
            CustomerID : customerid
        };
        $.ajax({
            type: "GET",
            url: "/Customer/MoreInfo/",
            cache: true,
            dataType: "json",
            data : paramCust,
            beforeSend: function () {
                
            },
            success: function (data, status) {
                if (data.errcode == 0) alert(data.errmsg);
                else {
                    $("#refund-code").val(data.errmsg);
                    ReturnItem(id, data.errmsg);
                }
            },
            error: function (data, status) {
                alert(status, data);
            },
            complete: function () {

            }
        });
    }

    /* --------------------------ACTION CUSTOMERS ORDERS */

    
    /* -----------------------------end action customers orders------------------*/    

    /* ---------------------------------begin MORE INFO CUSTOMERS -------------------------------*/

    $(".mi-edit").click(function () {
        var id = $(this).attr("id");    //get the id of button action
        $("#tdedit-" + id + "").hide();
        $("#tddone-" + id + "").show();
        $("#tdcancel-" + id + "").show();
        var valu = $("." + id).text();  //get value of td element
        $("." + id).html('<input type="text" class="m-wrap small" id="mi-' + id + '" required value="' + valu + '"/>'); //replace td element with input type        
        return false;
    });
    
    $(".mi-edit-notavailable").click(function () {
        var id = $(this).attr("id");    //get the id of button action
        //$("#tdedit-" + id + "").hide();
        //$("#tddone-" + id + "").show();
        alert("temporary not available to edit this column");
        $("#tdcancel-" + id + "").show();
        var valu = $("." + id).text();  //get value of td element
        $("." + id).html('<input type="text" class="m-wrap small" id="mi-' + id + '" required value="' + valu + '"/>'); //replace td element with input type        
        return false;
    });
    
    $(".mi-done").click(function () {
        var id = $(this).attr("id");    //get the id of button action
        var newValue = $("#mi-" + id + "").val();
        updateDataMoreInfo(id, newValue);
        return false;
    });

    $(".mi-cancel").click(function () {
        var id = $(this).attr("id");    //get the id of button action
        //alert("id = "+id);
        var val = $("#mi-" + id + "").val();
        $("."+ id +"").text(val);
        //updateDataMoreInfo(id, newValue);
        $("#tddone-" + id + "").hide();
        $("#tdcancel-" + id + "").hide();
        $("#tdedit-" + id + "").show();
        return false;
    });


    function updateDataMoreInfo(field, newValue) {
        /* the field is the id created in MoreInfo.cshtml when click edited. It must be the same with the field
        that going to be updated with the VCCRM2 customers field
        */
        var param = {
            id: $("#cust-id").val(), //hidden object input at MoreInfo.cshtml
            field: field,
            valu: newValue
        }        
        $.ajax({
            type: "POST",
            url: "../Customer/EditFieldVC",
            cache: true,
            data : param,
            dataType: "json",
            success: function (data, status) {
                alert(data[0].errmsg);
            },
            error: function (data, status) {
                alert(status, data);
            },
            complete: function () {
                location.reload();
            }
        });
        return false;
    }

    $(".mi-editaddress").click(function () {
        $("#modalEditAddress").modal('show');
        var id = $(this).attr("id");    //get the id of button action
        var valu = $("." + id).text();
        getCustomerInfo();
        return false;
    });

    $("#submitEditAddress").click(function () {
        var custId = $("#cust-id").val();        
        var custData = {
            customer_id: custId,
            address_1: $("#address-1").val(),
            address_2: $("#address-2").val(),
            town: $("#town").val(),
            post_code: $("#post_code").val(),
        };
        $.ajax({
            type: "GET",
            url: "../Customer/SubmitEditAddress/",
            data: custData,
            cache: true,
            dataType: "json",
            success: function (data, status) {
                alert(data[0].errmsg);
                if (data[0].errcode == 0) { $('#modalEditAddress').modal('hide'); }
            },
            error: function (data, status) {
                alert(status, data);
            },
            complete: function () {
                location.reload();
            }
        });        
        return false;
    });

    function getCustomerInfo() {
        var idCust = $("#cust-id").val();
        //alert(idCust);
        $.ajax({
            type: "GET",
            url: "../Customer/GetCustomerInfoDetail/" + idCust,
            cache: true,
            dataType: "json",
            beforeSend: function () {
                $('#address-1, #address-2, #town, #post_code').val("");
            },
            success: function (data, status) {               
                if (data.length > 0) {
                    $("#address-1").val(data[0].address_1);
                    $("#address-2").val(data[0].address_2);
                    $("#town").val(data[0].town);
                    $("#post_code").val(data[0].post_code);
                } else { alert("can't find customers data"); }
            },
            error: function (data, status) {
                alert(status, data);
            },
            complete: function () {

            }
        });
    }

    /* MORE INFO FORWARD SETTING/CALL SETTING */
    
    $('.forwardSetting').change(function (event) {
        //alert(this.);
        //var selectedText = $(".forwardSetting option:selected").text();
        var selectedVal = $("option:selected", this).val();
        //alert("selectedVal = " + selectedVal);
        var inputToNumber = '<td>ToNumber : <input type="text" class="m-wrap small" /></td>';
        if (selectedVal=="To Number")
            $(this).closest('tr').append(inputToNumber);
        return false;
    });

    /* ----------------------------------END More Info Customers -------------------------------*/
    
    /*---------------------------------------------PRODUCT CRITERIA, SEARCH CUSTMERS ---------------------*/
    
    function activateDatepickerActDate() {
        $('#sCustActDate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true
        });
        $('#sCustActDate-dt').inputmask("99/99/9999");
    }
    
    /*
    $('#sCustActDate').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    $('#sCustActDate-dt').inputmask("99/99/9999");*/

    $("#select-criteria").change(function () {
        $("#dynamic-search").html("");
        //$("#divActDate").hide();
        var val = $("option:selected", this).val();
        var search = '';
        if (val == "ActivationDate") {
            //$("#divActDate").show();  //in this div contains datepicker bootstrap     
            search = ' <div class="input-append date" data-date-format="dd/mm/yyyy" data-date="18/04/2013" id="sCustActDate">';
            search += '<input type="text" id="sCustActDate-dt" value="dd/mm/yyyy" class="m-wrap small" name="' + val + '">';
            search += '<span class="add-on"><i class="icon-calendar"></i></span>';
            search += '</div>';
        } else {
            if (val == "Status") {
                search = ' <select name="' + val + '" id="status-criteria">';
                search += '<option value="Active">Active Account</option>';
                search += '<option value="Passive">Passive</option>';
                search += '<option value="New">New account</option>';
                search += '<option value="Pending">Unverified</option>';
                search += '<option value="Blocked">Blocked</option>';
                search += '<option value="Deactivated">Deactivated</option>';
                search += '</select>';
            } else {
                search = '<div class="input-icon left">';
                if (val == "CustomerID")
                    search += '<input type="text" class="m-wrap medium" id="input-criteria" data-inputmask="mask: 99-9999999" name="' + val + '"  data-val-required="field is required" data-val="true" placeholder="' + val + '"/>';
                else
                    search += '<input type="text" class="m-wrap medium" id="input-criteria" name="' + val + '"  data-val-required="field is required" data-val="true" placeholder="' + val + '"/>';
                search += '</div>';
            } 
        } if (val == "CustomerID") {
            //alert("val = " + val);
            //$("#input-criteria").inputmask({ "mask": "9", "repeat": 10 });  // ~ mask "9999999999"
        }

        $("#dynamic-search").append(search);
        $("#button-search").show();
        activateDatepickerActDate();
        //return false;
    });

    /*$("#select-product").change(function () {
    
        $('#select-criteria').empty();
        
        var initOption = '<option value="">-Select-</option>';
        $('#select-criteria').append(initOption);
        var val = $("option:selected", this).val();
        
        if (val != "") {
            $.ajax({
                type: "GET",
                url: "../Product/GetSearchCriteria?productid=" + val + "",
                cache: true,
                dataType: 'json',
                beforeSend : function() {
                    $("#button-search").hide();
                    //$("#divActDate").hide();  //in this div contains datepicker bootstrap
                    $(".ajax-loader").show();
                },
                success: function (data, status) {
                    $.each(data, function (key, value) {
                        $('#select-criteria')
                            .append($("<option></option>")
                            .attr("value", data[key].criteria_id)
                            .text(data[key].criteria_name));
                    });
                },
                error: function (data, status) {
                    alert(status, data);
                },
                complete: function () {
                    $(".ajax-loader").hide();
                    
                }
            });
        }
    }); */
});