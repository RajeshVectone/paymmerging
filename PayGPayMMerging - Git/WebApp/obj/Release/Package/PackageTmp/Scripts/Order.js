﻿$(document).ready(function () {

    /* when modal tracking and provision is hidden, show again the grid order items */
    //$('#modalTracking, #modalProvision').on('hidden', function () {        

    /* orderlist */
    $('.btmodal').live('click', function (ev) {
        var idmodal = $(this).text();       //the order id
        $('#myModal').data('orderid', idmodal);     //keep it as global var at myModal div
        $('#myModal').modal('show');
        getItemOrder(idmodal);
        //insertOrderDetail(idmodal);
        return false;
    });

    $('#modalTracking').on('hidden', function () {
        $('#myModal').modal('show');
        getItemOrder($('#myModal').data('orderid'));    //recreate table item order list
        //insertOrderDetail($('#myModal').data('orderid'));
    });

    $('#myModal').on('hidden', function () {
        //initLoader();
        //location.reload();
    });

    $('#modalProvision').on('hidden', function () {
        $('#myModal').modal('hide');
        //initLoader(); location.reload();
    });

    $('.actionOrdStatusDetail').live('click', function (ev) {
        var idmodal = $(this).attr("title");    //order_id located at title attribute
        $('#myModal').data('orderid', idmodal);     //keep it as global var at myModal div
        $('#myModal').modal('show');
        getItemOrder(idmodal);
        //insertOrderDetail(idmodal);
        return false;
    });

    $('#cancelItemOrder').click(function (event) {
        var arrChkItemOrder = [];
        jQuery("input[type=checkbox][name=check-itemorder]").map(function () {
            if (jQuery(this).is(':checked'))
                arrChkItemOrder.push($(this).attr("id"));
        });
        cancelOrderAjax(arrChkItemOrder.toString());
        return false;
    });

    /*----------------------------------------------DISPATCH------------------------------------------------*/

    $('.actionOrdStatusDispatch').live('click', function (ev) {
        var idOrder = $(this).attr("title");                 //get the order id from attr titl
        var idItemOrder = $("#itemorder-" + idOrder + "").val();    //get the itemId 
        $('#myModal').data('orderid', idOrder);     //keep it as global var at myModal div
        //arrChkDispatchOrder.push(idItemOrder);
        $('#modalTracking').modal('show');
        return false;
    });

    $('#dispatchItemOrder').click(function (event) {
        jQuery("input[type=checkbox][name=check-itemorder]").map(function () {
            if (jQuery(this).is(':checked'))
                arrChkDispatchOrder.push($(this).attr("id"));
        });
        $('#myModal').modal('hide');
        $('#modalTracking').modal('show');
        return false;
    });

    $("#submitDispatch").click(function (event) {
        var trackingcode = $("#tracking-code").val();
        var listitems = $('#myModal').data('orderid');
        //alert("id = " + listitems);
        //var listitems = arrChkDispatchOrder.toString();
        var dispatchData = {
            TrackingCode: trackingcode,
            ListItems: listitems,
            UserLogin: $("#VCCRM2").data("username")
        };
        $.ajax({
            type: "GET",
            url: "../Order/SubmitDispatch/",
            data: dispatchData,
            cache: true,
            dataType: "json",
            success: function (data, status) {
                alert(data[0].errmsg);
                if (parseInt(data[0].errcode) == 0) {    //success
                    $('#modalTracking').modal('hide');
                    initLoader();
                    sendEmail(listitems, "dispatch");
                    //customerChangeStatus($('#myModal').data('orderid'));
                }

            },
            error: function (data, status) {
                alert(status, data);
            },
            complete: function () {
                $("#tracking-code").val("");
                //arrChkDispatchOrder.length = 0; //clear array
                //location.reload();
            }
        });
        return false;
    });

    function setDispatchAction() {
        $('.dispatchAction').click(function (event) {
            var altID = $(this).attr("alt");
            //arrChkDispatchOrder.push(altID);
            $('#myModal').modal('hide');
            $('#modalTracking').modal('show');
            return false;
        });
    }

    /*----------------------------------------------END DISPATCH------------------------------------------------*/

    /*----------------------------------------------REJEECT REFUND -----------------------------------------*/

    $('.actionOrdStatusRejectRefund').live('click', function (ev) {
        var idOrder = $(this).attr("title");                 //get the order id from attr titl
        $('#myModal').data('orderid', idOrder);     //keep it as global var at myModal div
        $('#modalCommentRefund').modal('show');
        return false;
    });   

    $("#submitRejectRefund").click(function (event) {
        var comment = $("#refund-rejected-comment").val();
        var listitems = $('#myModal').data('orderid');
        var Data = {
            Comment: comment,
            ListItems: listitems,
            UserLogin: $("#VCCRM2").data("username")
        };
        $.ajax({
            type: "GET",
            url: "../Finance/SubmitRejectRefund/",
            data: Data,
            cache: true,
            dataType: "json",
            success: function (data, status) {
                alert(data[0].errmsg);
                if (parseInt(data[0].errcode) == 0) {    //success
                    $('#modalCommentRefund').modal('hide');
                    initLoader();
                    sendEmail(listitems, "rejectRefund");
                    //location.reload();
                }
            },
            error: function (data, status) {
                alert(status, data);
            },
            complete: function () {
                $("#refund-rejected-comment").val("");
            }
        });
        return false;
    });

    function setRejectAction() {
        $('.rejectRefundAction').click(function (event) {
            var altID = $(this).attr("alt");
            $('#myModal').modal('hide');
            $('#modalCommentRefund').modal('show');
            return false;
        });
    }   
    /*---------------------------------------END REJECT REFUND--------------------------------------------*/


    /*----------------------------------------------ACCEPT REFUND -----------------------------------------*/

    $('.actionOrdStatusAcceptRefund').live('click', function (ev) {
        var idOrder = $(this).attr("title");                 //get the order id from attr titl
        $('#myModal').data('orderid', idOrder);     //keep it as global var at myModal div
        $('#modalApprovalNumber').modal('show');
        return false;
    });

    $("#submitApprovalRefund").click(function (event) {
        var number = $("#refund-ApprovalNumber").val();
        var listitems = $('#myModal').data('orderid');
        //alert("id = " + listitems);
        var Data = {
            Comment: number,
            ListItems: listitems,
            UserLogin: $("#VCCRM2").data("username")
        };
        $.ajax({
            type: "GET",
            url: "../Finance/SubmitAcceptRefund/",
            data: Data,
            cache: true,
            dataType: "json",
            success: function (data, status) {
                alert(data[0].errmsg);
                if (parseInt(data[0].errcode) == 0) {    //success
                    $('#modalApprovalNumber').modal('hide');
                    initLoader();
                    sendEmail(listitems, "approveRefund");
                }
            },
            error: function (data, status) {
                alert(status, data);
            },
            complete: function () {
                $("#refund-ApprovalNumber").val("");
            }
        });
        return false;
    });

    function setAcceptRefundAction() {
        $('.acceptRefundAction').click(function (event) {
            var altID = $(this).attr("alt");
            $('#myModal').modal('hide');
            $('#modalApprovalNumber').modal('show');
            return false;
        });
    }
    /*--------------------------------------------ACCEPT REFUND---------------------------------------*/

    /*-------------------------------------------------PRINT ------------------------------------------*/
    $('.actionOrdStatusPrint').live('click', function (ev) {
        var idOrder = $(this).attr("title");                 //get the order id from attr titl
        var idItemOrder = $("#itemorder-" + idOrder + "").val();    //get the itemId (this input hidden type)
        $('#myModal').data('orderid', idOrder);     //keep it as global var at myModal div
        $("#printOrder").trigger("click");      //execute printOrder
        return false;
    });

    $('#printOrder').click(function (event) {
        var x = screen.width / 2 - 500 / 2;
        var y = screen.height / 2 - 600 / 2;
        var myWindow = null;
        if ((myWindow == null) || (myWindow.closed)) {
            myWindow = window.open('/Order/PrintOrder?id=' + $('#myModal').data('orderid') + '', 'Print The Order', 'height=600,width=500,resizable=yes,left=' + x + ',top=' + y);
            myWindow.focus();
        } else {
            myWindow.location.href = url;
            myWindow.focus();
        }
        return false;
    });


    /* -------------------------------------------- PROVISION AREA --------------------------------------------*/

    $('.actionOrdStatusCancel').live('click', function (ev) {
        var idOrder = $(this).attr("title");                        //get the order id from attr title
        var idItemOrder = $("#itemorder-" + idOrder + "").val();    //get the itemId 
        $('#myModal').data('orderid', idOrder);                     //keep it as global var at myModal div (the idOrder that active)
        var r = confirm("Are you sure cancel the Order ? ")
        if (r == true) submitCancelItem(idOrder);
        return false;
    });

    function submitCancelItem(idOrder) {
        var provisionData = {
            OrderItem: idOrder,
            UserLogin: $("#VCCRM2").data("username")
        };

        $.ajax({
            type: "GET",
            url: "../Order/CancelItem/" + idOrder,
            cache: true,
            dataType: "json",
            success: function (data, status) {
                alert(data[0].errmsg);
                if (parseInt(data[0].errcode) == 0) {    //success
                    initLoader();
                    sendEmail(idOrder, "cancelOrder");
                    //location.reload();
                }
            },
            error: function (data, status) {
                alert(status, data);
            },
            complete: function () {
                //resetFieldProvision();
            }
        });
    }

    $('.actionOrdStatusProvision').live('click', function (ev) {
        var idOrder = $(this).attr("title");                        //get the order id from attr title
        var idItemOrder = $("#itemorder-" + idOrder + "").val();    //get the itemId 
        $('#myModal').data('orderid', idOrder);                     //keep it as global var at myModal div (the idOrder that active)
        $('#myModal').modal('hide');
        $('#modalProvision').modal('show');
        $("#provision-id").text(idItemOrder);
        //getProvisionToDisplay(idItemOrder);               //temporary, enable all field provisioning
        getItemProvision(idItemOrder);
        return false;
    });


    function getItemProvision(itemId) {
        $.ajax({
            type: "GET",
            url: "/Order/ItemOrder/" + itemId,
            cache: true,
            dataType: "json",
            beforeSend: function () {
                $("#provision-cloudusername, #provision-cloudpassword").text("");
            },
            success: function (data, status) {
                if (data.length > 0) {
                    $("#provision-desc").text(data[0].description);
                    $("#provision-cloudusername").text(data[0].login_name);
                    $("#provision-cloudpassword").text(data[0].login_password);
                }
            },
            error: function (data, status) {
                alert(status, data);
            },
            complete: function () {
                //jQuery("#ajax-loader", "#tItemOrder > tbody").remove();
            }
        });
    }


    $("#submitProvision").click(function (event) {
        var desc = $("#provision-desc").text();
        var id = $("#provision-id").text();
        var hardwaresn = $("#provision-hardwaresn").val();
        //alert($("#provision-hardwaresn").val());
        $('#modalProvision').data('orderid', id); //put id input by user to var global at modalprovision
        var provisionData = {
            OrderItem: id,
            HardwareName: desc,
            HardwareSN: hardwaresn,
            UserLogin: $("#VCCRM2").data("username")
        };

        $.ajax({
            type: "GET",
            url: "../Order/SubmitProvision/",
            data: provisionData,
            cache: true,
            dataType: "json",
            success: function (data, status) {
                alert(data[0].errmsg);
                if (parseInt(data[0].errcode) == 0) {    //success
                    $('#modalProvision').modal('hide');
                    initLoader();
                    location.reload();
                }
            },
            error: function (data, status) {
                alert(status, data);
            },
            complete: function () {
                //resetFieldProvision();
            }
        });
        return false;
    });


    $("#provisionClose").click(function (event) {
        $('#modalProvision').modal('hide');
    });

    function resetFieldProvision() {
        $('#provision-simiccid, #provision-voucher, #provision-handset').val("");
    }

    function setProvisionAction() {
        $('.provisionAction').click(function (event) {
            var altID = $(this).attr("alt");
            $('#myModal').modal('hide');
            $('#modalProvision').modal('show');
            $("#provision-id").text($("#row-id-" + altID).text());
            getItemProvision(altID);
            return false;
        });
    }

    function getProvisionToDisplay(itemId) {
        $.ajax({
            type: "GET",
            url: "/Order/GetProvisionDisplay/" + itemId,
            cache: true,
            dataType: "json",
            success: function (data, status) {
                //setAttributeProvisionForm(data);
            },
            error: function (data, status) {
                alert(status, data);
            },
            complete: function () {
                //
            }
        });
    }

    function setAttributeProvisionForm(data) {
        var showhandset = data[0].show_handset;
        var showvoucher = data[0].show_voucher;
        var showsim = data[0].show_SIM;
        var ispayg = data[0].is_payg;
        var idOrder = $('#myModal').data('orderid');
        var descOrder = $("#order-desc-" + idOrder).text(); //get desc order from front screen order
        $('#modalProvision').data('ispayg', ispayg);
        $("#provision-desc").html(descOrder.replace(/-/g, '<br/>'));
        //commented below so all field enable
        /*if (parseInt(showhandset) == 1) $("#provision-handset").removeAttr("disabled");
        else $("#provision-handset").attr("disabled",true);
        if (parseInt(showvoucher) == 1) {
            $("#provision-voucher").removeAttr("disabled");
        } else $("#provision-voucher").attr("disabled",true);
        if (parseInt(showsim) == 1) {
            $("#provision-simiccid").removeAttr("disabled");
        } else $("#provision-simiccid").attr("disabled", true);*/

    }

    function cancelOrderAjax(arrItemId) {
        $.ajax({
            type: "GET",
            url: "../Order/CancelOrder/" + arrItemId,
            cache: true,
            dataType: "json",
            success: function (data, status) {
                alert(data[0].errmsg);
                //getItemOrder($('#myModal').data('orderid'));    //recreate table item order list
            },
            error: function (data, status) {
                alert(status, data);
            },
            complete: function () {
                //
            }
        });
    }


    function getItemOrder(id) {
        $.ajax({
            type: "GET",
            url: "/Order/ItemOrder/" + id,
            cache: true,
            dataType: "json",
            beforeSend: function () {
                $("#tItemOrder > tbody").html("");	//empty the body table first
                $("#ajax-loader").clone(true).appendTo("#tItemOrder > tbody").show();
                //$("#item-checkall").attr("checked",false);  //make sure unchecked for initialization
            },
            success: function (data, status) {
                if (data.length > 0) {
                    createTableItem(data);
                    insertItemHeader(data);
                    setProvisionAction();   //activate provisionaction button                    
                    setDispatchAction();   //activate dispatchaction button    
                    setRejectAction();
                    setAcceptRefundAction();
                } else {
                    var sOut = '<tr><td colspan="6">no item order</td></tr>';
                    jQuery("#tItemOrder > tbody:first").append(sOut);
                }
            },
            error: function (data, status) {
                alert(status, data);
            },
            complete: function () {
                jQuery("#ajax-loader", "#tItemOrder > tbody").remove();
            }
        });
    }

    function insertItemHeader(data) {
        $(data).each(function (i) {
            $("#detail-orderref").html(data[i].order_id);
            $("#detail-customer").html(data[i].customer_name);
            $("#detail-address").html(data[i].dispatch_address);
            $("#detail-email").html(data[i].email);
            $("#detail-orderdate").html(data[i].orderdate_str);
            $("#detail-paymentref").html(data[i].payment_ref);
            $("#detail-contacttelp").html(data[i].contact_number);
        });
    }

    function insertOrderDetail(id) {
        resetFieldDetail();
        $.ajax({
            type: "GET",
            url: "../Customer/GetCustomerByOrderId/" + id,
            cache: true,
            dataType: "json",
            success: function (data, status) {
                if (data.length > 0) {
                    $('#detail-orderref').text(id);
                    $('#detail-orderdate').text($("#order-date-" + id + "").text());
                    $('#detail-customer').text(data[0].first_name + " " + data[0].last_name);
                    $('#detail-source').text(data[0].order_source);
                    $('#detail-address').text(data[0].dispatch_address);
                    $('#detail-paymentref').text(data[0].payment_ref);
                    $('#detail-email').text(data[0].email_address);
                    $('#detail-contacttelp').text(data[0].contact_number);
                }
            },
            error: function (data, status) {
                alert(status, data);
            },
            complete: function () {
                //
            }
        });
    }

    function resetFieldDetail() {
        $('#detail-orderref, #detail-address').text("");
        $('#detail-orderdate, #detail-paymentref').text("");
        $('#detail-customer, #detail-email').text("");
        $('#detail-source, #detail-contacttelp').text("");
    }

    function setTextAction(data, i) {
        var txtAction = "";
        if (parseInt(data[i].is_to_dispatch) == 1) txtAction = "Dispatch";
        else if (parseInt(data[i].is_RMA) == 1) txtAction = "Return";
        else if (parseInt(data[i].is_to_provision) == 1) txtAction = "Provision";
        return txtAction;
    }

    /*
    [{"customer_name":"Sergiu Maris","dispatch_address":"1 Micholm/Wembley/London/HA99 ","email":"s.maris@vectone.com",
    "contact_number":"445600167601","description":"ATA Adapter Cisco","order_id":1,"order_item_id":0,
    "order_type":null,"topup_amount":0,"amount":20,"order_date":"\/Date(1367197144223)\/",
    "statuschange_date":"\/Date(1367197144223)\/","order_source":null,"order_products":null,
    "order_status":"Awaiting Dispatch","item_status":null,"payment_ref":"VCWEB.12345","tracking_code":"",
    "device_type":null,"device_name":null,"orderdate_str":"29/04/2013 07:59","statuschangedate_str":"29/04/2013 07:59"}]
    */
    function createTableItem(data) {
        var idOrder = $('#myModal').data('orderid');
        var descOrder = $("#order-desc-" + idOrder).text(); //get desc order from front screen order
        $(data).each(function (i) {
            //var textAction = setTextAction(data, i);
            var sOut = '<tr id=' + i + '>';
            sOut += '<td id="row-id-' + data[i].order_id + '">' + data[i].order_id + '</td>';
            sOut += '<td id="row-status-' + i + '">' + data[i].order_status + '</td>';
            sOut += '<td id="row-desc-' + data[i].ID + '">' + data[i].description; + '</td>';
            sOut += '<td id="row-price-' + i + '">' + data[i].symbol + ' ' + data[i].amount + '</td>';
            sOut += '<td id="row-trackcode-' + i + '">' + data[i].tracking_code + '</td>';
            sOut += isProvisionAction(i, data);
            sOut += '</tr>';
            i = i + 1;
            jQuery("#tItemOrder > tbody:first").append(sOut);
        });
    }

    function isProvisionAction(i, data) {
        var sOut = '';
        switch (data[i].order_status) {
            case "Fail":
                sOut = '<td id="row-textaction-' + i + '"></td>';
                break;
            case "Awaiting Dispatch":
                sOut = '<td id="row-textaction-' + i + '" valign="top"><a class="btn blue dispatchAction" alt="' + data[i].order_id + '" href="#">Dispatch</a></td>';
                break;
            case "Awaiting Provisioning":
            case "Awaiting provisioning":
            case "Out Of Stock":
            case "Awaiting Provision": sOut = '<td id="row-textaction-' + i + '" valign="top"><a class="btn blue provisionAction" alt="' + data[i].order_id + '" href="#">Provision</a></td>'; break;
            case "Awaiting Refund Approval":
                sOut = '<td id="row-textaction-' + i + '" valign="top"><a class="btn blue acceptRefundAction" alt="' + data[i].order_id + '" href="#">Accept</a></td>';
                sOut += '<td id="row-textaction-' + i + '" valign="top"><a class="btn blue rejectRefundAction" alt="' + data[i].order_id + '" href="#">Reject</a></td>';
                
                break;
            default: sOut = '<td id="row-textaction-' + i + '"></td>'; break;
        }
        return sOut;
    }

    function checkBoxItem() {
        $('#item-checkall').click(function (event) {
            if ($(this).is(':checked')) {
                jQuery("input[type=checkbox][name=check-itemorder]").map(function () {
                    jQuery(this).attr("checked", true);	//all checkbox checked
                });
            } else {
                jQuery("input[type=checkbox][name=check-itemorder]").map(function () {
                    jQuery(this).attr("checked", false);	//all checkbox unchecked
                });
            }
        });
    }

    $("#select-filter").change(function () {
        var selectedVal = $("option:selected", this).val();
        $(".searchfilter-input-date").hide();
        $("#input-filterorder").removeAttr("name");
        $(".searchfilter-input").show();
        $("#input-filterorder").val("");
        if (selectedVal == "OrderDate") {
            $(".searchfilter-input").hide();
            $(".searchfilter-input-date").show();
        }
        $("#input-filterorder").attr("name", selectedVal);  //give name attribute 
    });

    /*------------------------------OUT OF STOCK--------------------------------------*/
    $("#confirmOutOfStock").click(function (event) {
        var dateDelay = $("#date-outofstockdelay").val();
        var id = $('#myModal').data('orderid');
        var hardwaresn = $("#provision-hardwaresn").val();
        alert(id + " date delay" + dateDelay);
        var Data = {
            OrderItem: id,
            UserLogin: $("#VCCRM2").data("username")
        };

        $.ajax({
            type: "GET",
            url: "../Order/SubmitOutOfStock/",
            data: Data,
            cache: true,
            dataType: "json",
            success: function (data, status) {
                alert(data[0].errmsg);
                if (parseInt(data[0].errcode) == 0) {    //success
                    $('#modalOutOfStock').modal('hide');
                    initLoader();
                    location.reload();
                }
            },
            error: function (data, status) {
                alert(status, data);
            },
            complete: function () {
                //resetFieldProvision();
            }
        });
        return false;
    });
    $("#proceedOutOfStock").click(function (event) {
        $('#modalProvision').modal('hide');
        $('#myodal').modal('hide');
        $('#modalOutOfStock').modal('show');
    });

    $('#modalOutOfStock').on('show', function () {      //when this show, make datepicker on top
        $(".datepicker").addClass("ontop");
    });


    $("#cancelOutOfStock").click(function (event) {
        $('#modalOutOfStock').modal('hide');
    });


    /*--------------------RETURNS ------------------------------*/
    $('.actionReturnRequest').live('click', function (ev) {
        var idmodal = $(this).attr("title");    //order_id located at title attribute
        $('#myModal').data('orderid', idmodal);     //keep it as global var at myModal div
        $('#modalRefundCode').modal('show');
        getRefundCode(idmodal);
        //insertOrderDetail(idmodal);
        return false;
    });

    $('.actionChangeAddress').live('click', function (ev) {
        var idCust = $("#cust-id").val();
        $('#modalChangeAddress').data('custid', idCust);
        $('#modalChangeAddress').modal('show');
        getCustomerInfoDetail();
        return false;
    });

    $("#submitChangeAddress").click(function (event) {
        var custId = $('#modalChangeAddress').data('custid');
        var custData = {

            customer_id: custId,
            first_name: $("#cust-firstname").val(),
            last_name: $("#cust-lastname").val(),
            address_1: $("#cust-address").val(),
            address_1: $("#cust-dispatchaddress").val(),
            address_2: $("#cust-billingaddress").val(),
            //dob       : $("#cust-country").val(data[0].town);
            //$("#cust-dob").val(data[0].dob);
            email_address: $("#cust-email").val()
        };
        /*
        $.ajax({
            type: "GET",
            url: "../Customer/SubmitChangeAddress/",
            data: custData,
            cache: true,
            dataType: "json",
            success: function (data, status) {
                alert(data[0].errmsg);
                if (data[0].errcode == 0) { $('#modalChangeAddress').modal('hide'); }
            },
            error: function (data, status) {
                alert(status, data);
            },
            complete: function () {
                // $("#tracking-code").val("");
                //arrChkDispatchOrder.length = 0; //clear array
            }
        });*/
        return false;
    });


    function getCustomerInfoDetail() {
        var idCust = $('#modalChangeAddress').data('custid');
        $.ajax({
            type: "GET",
            url: "/Customer/GetCustomerInfoDetail/" + idCust,
            cache: true,
            dataType: "json",
            beforeSend: function () {
                $('#cust-firstname, #cust-lastname, #cust-email, #cust-dob, #cust-address, #cust-dispatchaddress, #cust-billingaddress, #cust-country').val("");
            },
            success: function (data, status) {
                //var jsonDate = data[0].dob;
                /*var d = eval(jsonDate.slice(1, -1));
                d.date = d.date.replace('/Date(', '');
                d.date = d.date.replace(')/', '');
                var expDate = new Date(parseInt(d.date));
                var re = /-?\d+/;
                var m = re.exec(jsonDate); 
                //var d = new Date(parseInt(m[0]));
                //var dt = d.setTime(currentRow.YearMonth.substring(currentRow.YearMonth.indexOf('(') + 1, currentRow.YearMonth.indexOf(')')));
                alert(expDate);*/
                if (data.length > 0) {
                    $("#cust-firstname").val(data[0].first_name);
                    $("#cust-lastname").val(data[0].last_name);
                    $("#cust-address").val(data[0].address_1);
                    $("#cust-dispatchaddress").val(data[0].address_2);
                    $("#cust-billingaddress").val(data[0].address_2);
                    $("#cust-country").val(data[0].town);
                    $("#cust-dob").val(dt);
                    $("#cust-email").val(data[0].email_address);
                } else { alert("can't find customers data"); }
            },
            error: function (data, status) {
                alert(status, data);
            },
            complete: function () {

            }
        });
    }

    function getRefundCode(id) {
        $.ajax({
            type: "GET",
            url: "/Return/GetRefundCode/",
            cache: true,
            dataType: "json",
            beforeSend: function () {
                $("#refund-code").val("");  //empty first
            },
            success: function (data, status) {
                if (data.errcode == 0) alert(data.errmsg);
                else {
                    $("#refund-code").val(data.errmsg);
                    //ReturnItem(id, data.errmsg);
                }
            },
            error: function (data, status) {
                alert(status, data);
            },
            complete: function () {

            }
        });
    }

    function ReturnItem(id, rfCode) {
        var returnData = {
            OrderId: id,
            RefundCode: rfCode,
            UserLogin: $("#VCCRM2").data("username")
        };

        $.ajax({
            type: "GET",
            url: "/Return/ReturnItem/",
            cache: true,
            data: returnData,
            dataType: "json",
            success: function (data, status) {
                alert(data[0].errmsg);
            },
            error: function (data, status) {
                alert(status, data);
            },
            complete: function () {

            }
        });
    }
});