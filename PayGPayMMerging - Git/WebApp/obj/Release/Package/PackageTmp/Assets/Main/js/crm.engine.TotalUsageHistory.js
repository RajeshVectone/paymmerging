﻿/* ===================================================== 
   *  TAB PAYMENT PROFILE 
   ----------------------------------------------------- */
$(document).ready(function () {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    if (dd < 10) { dd = '0' + dd } if (mm < 10) { mm = '0' + mm } today = dd + '/' + mm + '/' + yyyy;


    //Added by karthik Jira:CRMT-232
    var Stdate = new Date();
    Stdate.setDate(Stdate.getDate() - 9);
    var Stdd = Stdate.getDate();
    var Stmm = Stdate.getMonth() + 1; //January is 0!
    var Styyyy = Stdate.getFullYear();
    if (Stdd < 10) { Stdd = '0' + Stdd } if (Stmm < 10) { Stmm = '0' + Stmm } Stdate = Stdd + '/' + Stmm + '/' + Styyyy;

    var lastdate1 = $('#lastdate1').val();
    if (lastdate1 == '') {
        $('#dp1').attr('data-date', Stdate);
        $('#dp1').datepicker().on('changeDate', function (e) {
            $('div.datepicker').hide();
        });
        $('#startdate').attr('value', Stdate);
        $('#lastdate1').val(Stdate);
    }
    else {
        $('#dp1').attr('data-date', lastdate1);
        $('#dp1').datepicker().on('changeDate', function (e) {
            $('div.datepicker').hide();
        });
        $('#startdate').attr('value', lastdate1);
    }

    var lastdate2 = $('#lastdate2').val();
    if (lastdate2 == '') {
        $('#dp2').attr('data-date', today);
        $('#dp2').datepicker().on('changeDate', function (e) {
            $('div.datepicker').hide();
        });
        $('#enddate').attr('value', today);
        $('#lastdate2').val(today);
    }
    else {
        $('#dp2').attr('data-date', lastdate2);
        $('#dp2').datepicker().on('changeDate', function (e) {
            $('div.datepicker').hide();
        });
        $('#enddate').attr('value', lastdate2);
    }

    $('#OrderListSearchBtn').click(function () {
        var orderType = $("select#orderType").val();
        var fromDate = $.trim($('#startdate').val());
        var toDate = $.trim($('#enddate').val());
        orderList(orderType, fromDate, toDate);
    });





    //function trim(str) {
    //    str = str.replace(/^\s+/, '');
    //    for (var i = str.length - 1; i >= 0; i--) {
    //        if (/\S/.test(str.charAt(i))) {
    //            str = str.substring(0, i + 1);
    //            break;
    //        }
    //    }
    //    return str;
    //}

    //function dateHeight(dateStr) {
    //    if (trim(dateStr) != '') {
    //        var frDate = trim(dateStr).split(' ');
    //        var frTime;
    //        if (frDate.length > 1) {
    //            frTime = frDate[1].split(':');
    //        }
    //        var frDateParts = frDate[0].split('-');
    //        var day = frDateParts[0] * 60 * 24;
    //        var month = frDateParts[1] * 60 * 24 * 31;
    //        var year = frDateParts[2] * 60 * 24 * 366;
    //        var x;
    //        if (frDate.length > 1) {
    //            var hour = frTime[0] * 60;
    //            var minutes = frTime[1];
    //            x = day + month + year + hour + minutes;
    //        }
    //        else {
    //            x = day + month + year;
    //        }

    //    } else {
    //        var x = 99999999999999999; //GoHorse!
    //    }
    //    return x;
    //}


    //jQuery.fn.dataTableExt.oSort['date-euro-asc'] = function (a, b) {
    //    var x = dateHeight(a);
    //    var y = dateHeight(b);
    //    var z = ((x < y) ? -1 : ((x > y) ? 1 : 0));
    //    return z;
    //};

    //jQuery.fn.dataTableExt.oSort['date-euro-desc'] = function (a, b) {
    //    var x = dateHeight(a);
    //    var y = dateHeight(b);
    //    var z = ((x < y) ? 1 : ((x > y) ? -1 : 0));
    //    return z;
    //};



});


/* ----------------------------------------------------------------
 * Function Name    : ViewPaymentProfiles
 * Purpose          : to view payment profile list
 * Added by         : Edi Suryadi
 * Create Date      : January 21th, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function ViewTotalUsageHistory(Msisdn, Sitecode, date_from, date_to) {
    $("#divTUHTitle").html("Usage History");
    $("#divPaymentProfileSettingsContainer").show();
    $("#tblPaymentProfileContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblPaymentProfile\"></table>");
    $("#tblPaymentProfileContainer").css({ "overflow": "auto", "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });

    var url = apiServer + '/api/CustomerTotalUsageHistory';
    jQuery.support.cors = true;
    var JSONSendData = {
        Msisdn: Msisdn,
        RequestType: 1,
        Sitecode: Sitecode,
        date_from: date_from,
        date_to: date_to,
        date_from1: date_from,
        date_to1: date_to
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            // $('#PPSettings select#selectCardList').html("");          
            $("#tblPaymentProfileContainer").css({ "background-image": "none" });
            $('#tblPaymentProfile').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                iDisplayLength: 10,
                bPaginate: true,
                bFilter: false,
                bInfo: false,
                aaSorting: [],
                aoColumns: [

                  //  { mDataProp: "paydate_String", sTitle: "Date" },

                    {
                        mDataProp: "paydate", sTitle: "Date", "bUseRendered": false,
                        fnRender: function (ob) {
                            return convertDateISOCustom(ob.aData.paydate, 0);
                        }
                    },

                    { mDataProp: "type", sTitle: "Reason Of Spend" },
                    { mDataProp: "totalusage_String", sTitle: "Value" },
                    { mDataProp: "beforebal_String", sTitle: "Balance Before Usage" },
                    { mDataProp: "afterbal_String", sTitle: "Balance After Usage" }
                ],
                fnDrawCallback: function () {
                    $("#tblPaymentProfile tbody td").css({ "font-size": "12px" });
                }
            });
            $("#tblPaymentProfile tbody td").css({ "font-size": "12px" });
            $("#tblPaymentProfile_wrapper .row-fluid:eq(0)").remove();

            $("#tblPaymentProfile_wrapper .row-fluid .span6:eq(0)").addClass("span4");
            $("#tblPaymentProfile_wrapper .row-fluid .span6:eq(1)").addClass("span8");

            var DownloadAnchor = "<a id=\"downloadAnchor\" style=\"display:none;\"></a>";
            var btnExportHistory = "<button class=\"btn blue\" id=\"btnExportusageHistory\" name=\"btnExportusageHistory\">Download to Excel</button>";
            $("#tblPaymentProfile_wrapper .row-fluid .span4").append(DownloadAnchor + btnExportHistory).show();
        },
        error: function (feedback) {
            $("#TabProductPackagesLoader").hide();
            $("#tblProductPackagesContainer").html("").append("No package found").show();
        }
    });
}

function GetCallHistorySearchCBS(Msisdn, Sitecode, date_from, date_to, search_type, PackageID, BundleName) {
    $("#divTUHTitle").html("Bundle Breakdown"); 
    $("#divPaymentProfileSettingsContainer").show();
    $("#tblPaymentProfileContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblPaymentProfile\"></table>");
    $("#tblPaymentProfileContainer").css({ "overflow": "auto", "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });

    var url = apiServer + '/api/CustomerTotalUsageHistory';
    jQuery.support.cors = true;
    var JSONSendData = {
        Msisdn: Msisdn,
        RequestType: 3,
        Sitecode: Sitecode,
        date_from: date_from,
        date_to: date_to,
        date_from1: date_from,
        date_to1: date_to,
        search_type: search_type,
        PackageID: PackageID,
        BundleName: BundleName
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#tblPaymentProfileContainer").css({ "background-image": "none" });
            $('#tblPaymentProfile').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                iDisplayLength: 10,
                bPaginate: true,
                bFilter: false,
                bInfo: false,
                aaSorting: [],
                aoColumns: [
                    { mDataProp: "Package_Name", sTitle: "Bundle Name" },
                    { mDataProp: "History_Type", sTitle: "History Type" },
                    { mDataProp: "Date_Time", sTitle: "Call & Time" },
                    { mDataProp: "CLI", sTitle: "CLI" },
                    { mDataProp: "Destination_Number", sTitle: "Destination Number" },
                    { mDataProp: "Type", sTitle: "Type" },
                    { mDataProp: "Destination_Code", sTitle: "Destination Code" },
                    { mDataProp: "Duration_Min", sTitle: "Duration (Min:sec)" },
                    { mDataProp: "Data_Usage", sTitle: "Data Usage(in MB)" },
                    { mDataProp: "Tariff_Class", sTitle: "Tariff Class" },
                    { mDataProp: "Net_Charges", sTitle: "Net Charges" },
                    //{ mDataProp: "Roaming_Zone", sTitle: "Roaming Zone" },
                    { mDataProp: "Balance", sTitle: "Balance" },
                    //{ mDataProp: "custcode", sTitle: "Cust Code" },
                    //{ mDataProp: "batchcode", sTitle: "Batch Code" },
                    //{ mDataProp: "serialcode", sTitle: "Serial Code" },
                    { mDataProp: "roaming_status", sTitle: "Roaming" },
                ],
                fnDrawCallback: function () {
                    $("#tblPaymentProfile tbody td").css({ "font-size": "12px" });
                }
            });
            $("#tblPaymentProfile tbody td").css({ "font-size": "12px" });
            $("#tblPaymentProfile_wrapper .row-fluid:eq(0)").remove();

            $("#tblPaymentProfile_wrapper .row-fluid .span6:eq(0)").addClass("span4");
            $("#tblPaymentProfile_wrapper .row-fluid .span6:eq(1)").addClass("span8");

            var DownloadAnchor = "<a id=\"downloadAnchor\" style=\"display:none;\"></a>";
            var btnExportHistory = "<button class=\"btn blue\" id=\"btnExportBundleBreakDown\" name=\"btnExportBundleBreakDown\">Download to Excel</button>";
            $("#tblPaymentProfile_wrapper .row-fluid .span4").append(DownloadAnchor + btnExportHistory).show();
        },
        error: function (feedback) {
            $("#TabProductPackagesLoader").hide();
            $("#tblProductPackagesContainer").html("").append("No package found").show();
        }
    });
}

//28-Dec-2018 : Moorthy : Added for Bundle breakdown
function GetSubscribedBundlePackagesCDR(Msisdn, Sitecode, search_type) {
    $("#ajax-screen-masking").show();
    $('#ddlSubBreakDown').find('option').remove();
    var url = apiServer + '/api/CustomerTotalUsageHistory';
    jQuery.support.cors = true;
    var JSONSendData = {
        RequestType: 2,
        Msisdn: Msisdn,
        Sitecode: Sitecode,
        search_type: search_type
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            //$('#ddlSubBreakDown').append('<option value="-1" selected="selected">ALL</option>');
            $.each(feedback, function (idx) {
                $('#ddlSubBreakDown').append("<option value=\"" + feedback[idx].tariffClass + "\" >" + feedback[idx].name + "</option>");
            });
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}

/* ----------------------------------------------------- 
   *  eof PAYMENT PROFILE  
   ===================================================== */


