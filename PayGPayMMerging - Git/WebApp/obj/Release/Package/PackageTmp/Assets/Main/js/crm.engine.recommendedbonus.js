﻿//Added by Elango for Jira CRMT- 315 & 316
function ReferralHistoryViewDetails(RefID, MobileNo, Site_Code) {

    $('#Creditstatustext').val("");
    $('#Creditstatuscomment').val("");
    $('#SIMstatustext').val("");
    $('#SIMstatuscomment').val("");
    $('#Creditvalidstatustext').val("");
    $('#Userstatustext').val("");
    $('#Userstatuscomment').val("");
    $("#Names").html("");

    var url = apiServer + "/api/PAYGRecommendedBonus";
    var JSONSendData = {
        requestType: 3,
        User_Id: MobileNo,
        Site_Code: Site_Code,
        Referral_Id: RefID
    };
    $.ajax
    ({
        url: url,
        type: "POST",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            if (feedback != "") {
                $('#Creditstatustext').val(feedback[0].Credit_Status);
                $('#Creditstatuscomment').val(feedback[0].Credit_Status_Desc);
                $('#SIMstatustext').val(feedback[0].SIM_Status);
                $('#SIMstatuscomment').val(feedback[0].SIM_Status_Desc);
                $('#Creditvalidstatustext').val(feedback[0].Credit_Validity_Status);
                $('#Userstatustext').val(feedback[0].User_Status);
                $('#Userstatuscomment').val(feedback[0].User_Status_Desc);
                $("#Names").html(feedback[0].Invitee_Name);
            }
            else {
                $('#Creditstatustext').val($("#tblReferralHistory tbody tr").find("td:eq(6)").text());
                $('#SIMstatustext').val($("#tblReferralHistory tbody tr").find("td:eq(3)").text());
                $('#Creditvalidstatustext').val("Not Applicable");
                $('#Userstatustext').val("");
                $("#Names").html($("#tblReferralHistory tbody tr").find("td:eq(1)").text());
            }
            $("#User_ID").val(MobileNo);
            $("#Referral_ID").val(RefID);
            $("#Site_Code").val(Site_Code);
            $('#divCustomerReferralDetails').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
            DisableControlsForReferral();
        },
        error: function (feedback) {
            alert('error loading...');
        }
    });
}

$("#btnSaveReferral").click(function () {
    var MobilNo = $("#User_ID").val();
    var Referral_ID = $("#Referral_ID").val();
    var Creditstatuscomment = $('#Creditstatuscomment').val();
    var SIMstatuscomment = $('#SIMstatuscomment').val();
    var Userstatuscomment = $('#Userstatuscomment').val();
    var Site_Code = $("#Site_Code").val();
    var url = apiServer + "/api/PAYGRecommendedBonus";
    var JSONSendData = {
        requestType: 4,
        User_Id: MobilNo,
        Referral_Id: Referral_ID,
        Credit_Status_Desc: Creditstatuscomment,
        SIM_Status_Desc: SIMstatuscomment,
        User_Status_Desc: Userstatuscomment,
        Site_Code: Site_Code
    };
    $.ajax
    ({
        url: url,
        type: "POST",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('#divCustomerReferralDetails').hide();

            $("#ajax-screen-masking").hide();
            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });

            var errcode = feedback[0].errcode;
            if (errcode == 0) {

                $('.PMSuccess .modal-header').html(feedback[0].errsubject);
                $('.PMSuccess .modal-body').html(feedback[0].errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
            else {

                $('.PMFailed .modal-header').html(feedback[0].errsubject);
                $('.PMFailed .modal-body').html(feedback[0].errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }



        },
        error: function (feedback) {
            alert('error loading...');
        }
    });
});

function RBTransactionHistoryViewDetails(Trans_Id, MobileNo, Site_Code) {

    $('#Transferstatustext').val("");
    $('#Transferstatuscommenttext').val("");
    $('#Activebanktext').val("");
    $('#ActiveFirstnametext').val("");
    $('#Activesurnametext').val("");
    $('#Sortcodetext').val("");
    $('#Accountnotext').val("");
    $('#Creditavailabletext').val("");
    $('#Paymentstatuscomment').val("");

    var url = apiServer + "/api/PAYGRecommendedBonus";
    var JSONSendData = {
        requestType: 5,
        User_Id: MobileNo,
        Trans_Id: Trans_Id,
        Site_Code: Site_Code
    };
    $.ajax
    ({
        url: url,
        type: "POST",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            if (feedback != "") {
                $('#Transferstatustext').val(feedback[0].Transfer_Status);
                $('#Transferstatuscommenttext').val(feedback[0].Transfer_Status_Desc);
                $('#Activebanktext').val(feedback[0].Bank_Name);
                $('#ActiveFirstnametext').val(feedback[0].First_Name);
                $('#Activesurnametext').val(feedback[0].Surname);
                $('#Sortcodetext').val(feedback[0].Sort_Code);
                $('#Accountnotext').val(feedback[0].AC_Number);
                $('#Creditavailabletext').val(feedback[0].Credit_Available);
                $('#Paymentstatuscomment').val(feedback[0].Payment_Status_Desc);
                $('#PaymentStatusSelect option[value="' + feedback[0].Payment_Status + '"]').prop('selected', true);
            }
            else {
                $('#PaymentStatusSelect option[value="' + $("#tblTranactionHistory tbody tr").find("td:eq(3)").text() + '"]').prop('selected', true);
            }
            $("#User_ID").val(MobileNo);
            $("#Trans_Id").val(Trans_Id);
            $("#Site_Code").val(Site_Code);
            $('#divRBTransactionDetails').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
            DisableControlsForTransaction();
        },
        error: function (feedback) {
            alert('error loading...');
        }
    });
}

$("#btnSaveTransactionHistory").click(function () {
    var MobilNo = $("#User_ID").val();
    var Trans_Id = $("#Trans_Id").val();
    var Transfer_Status_Desc = $('#Transferstatuscommenttext').val();
    var Payment_Status = $('#PaymentStatusSelect :selected').text();
    var Payment_Status_Desc = $('#Paymentstatuscomment').val();
    var Site_Code = $("#Site_Code").val();

    var url = apiServer + "/api/PAYGRecommendedBonus";
    var JSONSendData = {
        requestType: 6,
        User_Id: MobilNo,
        Site_Code: Site_Code,
        Trans_Id: Trans_Id,
        Transfer_Status_Desc: Transfer_Status_Desc,
        Payment_Status: Payment_Status,
        Payment_Status_Desc: Payment_Status_Desc
    };
    $.ajax
    ({
        url: url,
        type: "POST",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('#divRBTransactionDetails').hide();

            $("#ajax-screen-masking").hide();
            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });

            var errcode = feedback[0].errcode;
            if (errcode == 0) {

                $('.PMSuccess .modal-header').html(feedback[0].errsubject);
                $('.PMSuccess .modal-body').html(feedback[0].errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
            else {

                $('.PMFailed .modal-header').html(feedback[0].errsubject);
                $('.PMFailed .modal-body').html(feedback[0].errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (feedback) {
            alert('error loading...');
        }
    });
});

function RBOverView(MobileNo, Month, Site_Code) {
    var url = apiServer + "/api/PAYGRecommendedBonus";
    var JSONSendData = {
        requestType: 0,
        User_Id: MobileNo,
        Site_Code: Site_Code
    };
    $.ajax
    ({
        url: url,
        type: "POST",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('#tblRBOverview').dataTable({
                bDestroy: false,
                bRetrieve: false,
                aaData: feedback,
                bPaginate: false,
                bFilter: false,
                bInfo: false,
                iDisplayLength: 0,
                aaSorting: [[0, "desc"]],
                bSort: false,
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Recommended Bouns Detail found."
                },
                aoColumns: [
                    { mDataProp: "Total_Credit_Available", sTitle: "Total Credit Available" },
                    { mDataProp: "SIM_Requests", sTitle: "SIM Requests" },
                    { mDataProp: "Active_SIMs", sTitle: "Active SIMs" },
                    { mDataProp: "Bonus_Credited", sTitle: "Bonus Credited" }
                ],
                fnDrawCallback: function () {
                    $("#tblRBOverview tbody td").css({ "font-size": "12px" });
                }
            });

            $("#tblRBOverview tbody td").css({ "font-size": "12px" });
            $("#tblRBOverview .row-fluid:eq(0)").remove();
            $("#tblRBOverview .row-fluid .span6:eq(0)").addClass("span4");
            $("#tblRBOverview .row-fluid .span6:eq(1)").addClass("span8");
            $("#tblRBOverview .row-fluid .span6").removeClass("span6");
            $("#tblRBOverview .row-fluid .span6").remove();
            $("#tblRBOverview thead tr:first th:last").append(" (" + Month + ")");

        },
        error: function (feedback) {
            $("#tblRBOverview").css({ "background-image": "none" }).html("").append("No Recommended Bouns Detail  found.").show();
        }
    });
}

function RBReferralHistory(MobileNo, Site_Code) {
    var Referral_Status = $("#selRefstatus :selected").val()
    var SIM_Status = $("#selSimstatus :selected").val()
    var Recharge_Mode = $("#selRechargemode :selected").val()
    var url = apiServer + "/api/PAYGRecommendedBonus";

    var JSONSendData = {
        requestType: 1,
        User_Id: MobileNo,
        Referral_Status: Referral_Status,
        SIM_Status: SIM_Status,
        Recharge_Mode: Recharge_Mode,
        Site_Code: Site_Code
    };

    $.ajax
    ({
        url: url,
        type: "POST",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('#tblReferralHistory').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: false,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [[0, "desc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Referral History Detail found."
                },
                aoColumnDefs: [{ "bVisible": false, "aTargets": [0] }],
                aoColumns: [
                    { mDataProp: "Referral_Id", sTitle: "Referral Id" },
                    {
                        mDataProp: "SIM_Order_Date", sTitle: "SIM Order Date",
                        fnRender: function (ob) {
                            return convertDateISOCustom(ob.aData.SIM_Order_Date, 0);
                        }
                    },
                    { mDataProp: "Invitee_Name", sTitle: "Invitee Name" },
                    { mDataProp: "Referral_Status", sTitle: "Referral Status" },
                    { mDataProp: "SIM_Status", sTitle: "SIM Status" },
                    { mDataProp: "Amount_Recharged", sTitle: "Amount Recharged" },
                    { mDataProp: "Value_Credited", sTitle: "Value Credited" },
                    { mDataProp: "Credit_Status", sTitle: "Credit Status" },
                    { mDataProp: "Recharge_Mode", sTitle: "Recharge Mode" },
                    {
                        mDataProp: "Action", sTitle: "Action", sType: "string", bSortable: false, bSearchable: false,
                        fnRender: function (ob) {
                            var actionURL = "";
                            actionURL = "<a href='#' id='aViewDetails' onclick='ReferralHistoryViewDetails(" + ob.aData.Referral_Id + "," + MobileNo + ",\"" + Site_Code + "\");'>View Details</a>";
                            return actionURL;
                        }
                    }
                ],
                fnDrawCallback: function () {
                    $("#tblReferralHistory tbody td").css({ "font-size": "12px" });
                }
            });
            $("#tblReferralHistory tbody td").css({ "font-size": "12px" });
            $("#tblReferralHistory .row-fluid:eq(0)").remove();
            $("#tblReferralHistory .row-fluid .span6:eq(0)").addClass("span4");
            $("#tblReferralHistory .row-fluid .span6:eq(1)").addClass("span8");
            $("#tblReferralHistory .row-fluid .span6").removeClass("span6");

            //$("#tblReferralHistory tbody tr").each(function () {
            //    // Within tr we find the last td child element and get content
            //    var Date = $(this).find("td:eq(0)").text();
            //    var Formatteddate = Date; //Get the current date
            //    Formatteddate.format("DD/MM/yyyy hh:mm:ss"); //2014-07-10
            //    $(this).find("td:eq(0)").append(Formatteddate);
            //});
        },
        error: function (feedback) {
            $("#tblReferralHistory").css({ "background-image": "none" }).html("").append("No Referral History Detail  found.").show();
        }
    });
}

function RBRTransactionHistory(MobileNo, Site_Code) {
    var Transfer_Type = $("#selPaymentStatus :selected").val()
    var Payment_Status = $("#selTransactionType :selected").val()

    var row = 0;
    var url = apiServer + "/api/PAYGRecommendedBonus";
    var JSONSendData = {
        requestType: 2,
        User_Id: MobileNo,
        Transfer_Type: Transfer_Type,
        Payment_Status: Payment_Status,
        Site_Code: Site_Code
    };
    $.ajax
    ({
        url: url,
        type: "POST",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('#tblTranactionHistory').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: false,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [[0, "desc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Transaction History Detail found."
                },
                aoColumnDefs: [{ "bVisible": false, "aTargets": [0] }],
                aoColumns: [
                    { mDataProp: "Trans_Id", sTitle: "Trans_Id" },
                    { mDataProp: "Request_Date", sTitle: "Request Date" },
                    { mDataProp: "Transfer_Type", sTitle: "Transfer Type" },
                    { mDataProp: "Transfer_Date", sTitle: "Transfer Date" },
                    { mDataProp: "Payment_Status", sTitle: "Payment Status" },
                    { mDataProp: "Bank_Account_Mapped", sTitle: "Bank Account Mapped" },
                    { mDataProp: "Active_Bank_Account", sTitle: "Active Bank Account" },
                    { mDataProp: "Amount_Transferred", sTitle: "Amount Transferred" },
                    {
                        mDataProp: "Action", sTitle: "Action",
                        fnRender: function (ob) {
                            var actionURL = "";
                            actionURL = "<a href='#' id='aViewDetails' onclick='RBTransactionHistoryViewDetails(" + ob.aData.Trans_Id + "," + MobileNo + ",\"" + Site_Code + "\");'>View Details</a>";
                            return actionURL;
                        }
                    }
                ],
                fnDrawCallback: function () {
                    $("#tblTranactionHistory tbody td").css({ "font-size": "12px" });
                }
            });
            $("#tblTranactionHistory tbody td").css({ "font-size": "12px" });
            $("#tblTranactionHistory .row-fluid:eq(0)").remove();
            $("#tblTranactionHistory .row-fluid .span6:eq(0)").addClass("span4");
            $("#tblTranactionHistory .row-fluid .span6:eq(1)").addClass("span8");
            $("#tblTranactionHistory .row-fluid .span6").removeClass("span6");
        },
        error: function (feedback) {
            $("#tblTranactionHistory").css({ "background-image": "none" }).html("").append("No Transaction History Detail  found.").show();
        }
    });
}

function DownloadReferralHistory(MobileNo, Site_Code) {
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    $("#ajax-screen-masking").show();

    var Referral_Status = $("#selRefstatus :selected").val()
    var SIM_Status = $("#selSimstatus :selected").val()
    var Recharge_Mode = $("#selRechargemode :selected").val()

    $.ajax({
        url: '/RecommendBonusDownload/GoDownLoadReferralHistory',
        type: 'get',
        data: { 'User_Id': MobileNo, 'Referral_Status': Referral_Status, 'SIM_Status': SIM_Status, 'Recharge_Mode': Recharge_Mode, 'Site_Code': Site_Code },
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            //$("#ajax-screen-masking").show();
        },
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            if (feedback != null) {
                var IFrameData = $('<iframe/>', { id: 'downloadIFrame', src: '/RecommendBonusDownload/GoDownLoadReferralHistoryFile?id=' + feedback.Text, style: 'display:none' });
                $("#ajax-screen-masking").append(IFrameData);
            }
            $("#ajax-screen-masking").hide();

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("File failed to generated due to Error ");// + xhr.status);
            $("#ajax-screen-masking").hide();
        }
    });
}

function DownloadTransactionHistory(MobileNo, Site_Code) {
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    $("#ajax-screen-masking").show();

    var Transfer_Type = $("#selPaymentStatus :selected").val()
    var Payment_Status = $("#selTransactionType :selected").val()


    $.ajax({
        url: '/RecommendBonusDownload/GoDownLoadTransactionHistory',
        type: 'get',
        data: { 'User_Id': MobileNo, 'Transfer_Type': Transfer_Type, 'Payment_Status': Payment_Status, 'Site_Code': Site_Code },
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            //$("#ajax-screen-masking").show();
        },
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            if (feedback != null) {
                var IFrameData = $('<iframe/>', { id: 'downloadIFrame', src: '/RecommendBonusDownload/GoDownLoadTransactionHistoryFile?id=' + feedback.Text, style: 'display:none' });
                $("#ajax-screen-masking").append(IFrameData);
            }
            $("#ajax-screen-masking").hide();

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("File failed to generated due to Error ");// + xhr.status);
            $("#ajax-screen-masking").hide();
        }
    });
}

$("#btnEditReferral").click(function () {
    $('#Creditstatuscomment').prop('disabled', false);
    $('#SIMstatuscomment').prop('disabled', false);
    $('#Userstatuscomment').prop('disabled', false);
});

$("#btnEditTransactionHistory").click(function () {
    $('#Transferstatuscommenttext').prop('disabled', false);
    $('#PaymentStatusSelect').prop('disabled', false);
    $('#Paymentstatuscomment').prop('disabled', false);
});

function DisableControlsForReferral() {
    $('#Creditstatustext').prop('disabled', true);
    $('#Creditstatuscomment').prop('disabled', true);
    $('#SIMstatustext').prop('disabled', true);
    $('#SIMstatuscomment').prop('disabled', true);
    $('#Creditvalidstatustext').prop('disabled', true);
    $('#Userstatustext').prop('disabled', true);
    $('#Userstatuscomment').prop('disabled', true);
}

function DisableControlsForTransaction() {
    $('#Transferstatustext').prop('disabled', true);
    $('#Transferstatuscommenttext').prop('disabled', true);
    $('#Activebanktext').prop('disabled', true);
    $('#ActiveFirstnametext').prop('disabled', true);
    $('#Activesurnametext').prop('disabled', true);
    $('#Sortcodetext').prop('disabled', true);
    $('#Accountnotext').prop('disabled', true);
    $('#Creditavailabletext').prop('disabled', true);
    $('#PaymentStatusSelect').prop('disabled', true);
    $('#Paymentstatuscomment').prop('disabled', true);
}

function RBBindReferralStatus(MobileNo, Site_Code) {
    var url = apiServer + "/api/PAYGRecommendedBonus";
    var JSONSendData = {
        requestType: 7,
        User_Id: MobileNo,
        Site_Code: Site_Code
    };
    $.ajax
    ({
        url: url,
        type: "POST",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $.each(feedback, function (key, value) {
                $("#selRefstatus").append($("<option></option>").val
                (value.Referral_Status).html(value.Referral_Status));
            });

        },
        error: function (feedback) {
            alert('error loading...');
        }
    });
}

function RBBindReferralSimStatus(MobileNo, Site_Code) {
    var url = apiServer + "/api/PAYGRecommendedBonus";
    var JSONSendData = {
        requestType: 8,
        User_Id: MobileNo,
        Site_Code: Site_Code
    };
    $.ajax
    ({
        url: url,
        type: "POST",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $.each(feedback, function (key, value) {
                $("#selSimstatus").append($("<option></option>").val
                (value.SIM_Status).html(value.SIM_Status));
            });

        },
        error: function (feedback) {
            alert('error loading...');
        }
    });
}

function RBBindRechargeMode(MobileNo, Site_Code) {
    var url = apiServer + "/api/PAYGRecommendedBonus";
    var JSONSendData = {
        requestType: 9,
        User_Id: MobileNo,
        Site_Code: Site_Code
    };
    $.ajax
    ({
        url: url,
        type: "POST",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $.each(feedback, function (key, value) {
                $("#selRechargemode").append($("<option></option>").val
                (value.Recharge_Mode).html(value.Recharge_Mode));
            });

        },
        error: function (feedback) {
            alert('error loading...');
        }
    });
}

function RBBindTransactionType(Site_Code) {
    var url = apiServer + "/api/PAYGRecommendedBonus";
    var JSONSendData = {
        requestType: 10,
        Site_Code: Site_Code
    };
    $.ajax
    ({
        url: url,
        type: "POST",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $.each(feedback, function (key, value) {
                $("#selTransactionType").append($("<option></option>").val
                (value.Transfer_Type).html(value.Transfer_Type));
            });

        },
        error: function (feedback) {
            alert('error loading...');
        }
    });
}

function RBBindPaymentStatus(Site_Code) {
    var url = apiServer + "/api/PAYGRecommendedBonus";
    var JSONSendData = {
        requestType: 11,
        Site_Code: Site_Code
    };
    $.ajax
    ({
        url: url,
        type: "POST",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $.each(feedback, function (key, value) {
                $("#selPaymentStatus").append($("<option></option>").val
                (value.Payment_Status).html(value.Payment_Status));

                $("#PaymentStatusSelect").append($("<option></option>").val
               (value.Payment_Status).html(value.Payment_Status));
            });

        },
        error: function (feedback) {
            alert('error loading...');
        }
    });
}

