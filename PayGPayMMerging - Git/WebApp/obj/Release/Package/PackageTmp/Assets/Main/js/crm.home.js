﻿function BindAgentsGrid(fromdate, todate) {
    $("#ajax-screen-masking").show();
    $("#tblAgentsPerformance tbody").html("");
    var url = apiServer + '/api/Home';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 1,
        fromdate: fromdate,
        todate: todate
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            if (feedback != null && feedback.length > 0) {
                for (var i = 0; i < feedback.length; i++) {
                    var htmlString = "<tr><td>" + feedback[i].Username + "</td><td>" + feedback[i].Assigned + "</td><td>" + feedback[i].Inprogress + "</td><td>" + feedback[i].Closed + "</td><td>" + feedback[i].agent_avg + "</td></tr>";
                    $("#tblAgentsPerformance tbody").append(htmlString);
                }
            }
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}

function BindUnresolvedTicketsGrid(fromdate, todate) {
    $("#ajax-screen-masking").show();
    $("#tblUnresolvedTickets tbody").html("");
    var url = apiServer + '/api/Home';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 2,
        fromdate: fromdate,
        todate: todate
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            if (feedback != null && feedback.length > 0) {
                for (var i = 0; i < feedback.length; i++) {
                    var htmlString = "<tr><td>" + feedback[i].Team + "</td><td>" + feedback[i].Inprogress + "</td><td>" + feedback[i].Unresolved_count + "</td></tr>";
                    $("#tblUnresolvedTickets tbody").append(htmlString);
                }
            }
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}

function BindOverviewTask(fromdate, todate) {
    $("#ajax-screen-masking").show();
    $("#mhEmailsReceived").html("0");
    $("#mhEmailsClosed").html("0");
    $("#mhEmailsInProgress").html("0");
    $("#mhEmailsOpen").html("0");
    $("#mhEmailsResolved").html("0");
    $("#mhEmailsOpened").html("0");
    var url = apiServer + '/api/Home';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 3,
        fromdate: fromdate,
        todate: todate
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            if (feedback != null && feedback.length > 0) {
                for (var i = 0; i < feedback.length; i++) {
                    if (feedback[i].status.toUpperCase() == "RECEIVED") {
                        $("#mhEmailsReceived").html(feedback[i].total_count);
                        //$("#h1Received").html(feedback[i].total_count);
                    }
                    else if (feedback[i].status.toUpperCase() == "CLOSED")
                        $("#mhEmailsClosed").html(feedback[i].total_count);
                    else if (feedback[i].status.toUpperCase() == "INPROGRESS")
                        $("#mhEmailsInProgress").html(feedback[i].total_count);
                    else if (feedback[i].status.toUpperCase() == "RESOLVED") {
                        $("#mhEmailsResolved").html(feedback[i].total_count);
                        //$("#h1Resolved").html(feedback[i].total_count);
                    }
                    else if (feedback[i].status.toUpperCase() == "OPEN") {
                        $("#mhEmailsOpened").html(feedback[i].total_count);
                    }
                }
            }
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}

function BindGraph(fromdate, todate) {
    $("#ajax-screen-masking").show();
    $('#column-chart').remove();
    $('#barChart').append('<canvas id="column-chart" height="200"><canvas>');
    //$('#line-chart').remove();
    //$('#lineChart').append('<canvas id="line-chart" height="200"><canvas>');
    var url = apiServer + '/api/Home';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 4,
        fromdate: fromdate,
        todate: todate
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            if (feedback != null && feedback.length > 0) {
                var labels = [], datas = [];
                feedback.forEach(function (e) {
                    labels.push(e.received_date);
                    datas.push(e.received_count);
                });

                //Bar Chart
                var ctx = $("#column-chart");

                var chartOptions = {
                    responsive: true,
                    maintainAspectRatio: false,
                    legend: false,
                    hover: {
                        mode: 'label'
                    },
                    scales: {
                        xAxes: [{
                            display: true,
                            gridLines: {
                                color: "#f5f5f5",
                                drawTicks: false,
                            },
                            scaleLabel: {
                                display: true,
                                labelString: 'Days'
                            }
                        }],
                        yAxes: [{
                            display: true,
                            gridLines: {
                                color: "#f5f5f5",
                                drawTicks: false,
                            },
                            scaleLabel: {
                                display: true,
                                labelString: 'Rec. Count'
                            },
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    },
                    title: {
                        display: true,
                        text: 'Bar Chart'
                    }
                };

                var chartData = {
                    labels: labels,
                    datasets: [{
                        label: '',
                        data: datas,
                        fill:false,
                        backgroundColor: "rgba(110, 92, 168, 1)",
                        borderColor: "transparent",
                        pointBackgroundColor: 'rgba(148,159,177,1)',
                        pointBorderColor: '#fff',
                        pointHoverBackgroundColor: '#fff',
                        pointHoverBorderColor: 'rgba(148,159,177,0.8)'
                    }]
                };

                var config = {
                    type: 'bar',
                    options: chartOptions,
                    data: chartData
                };

                var barChart = new Chart(ctx, config);

                //Line Chart
                var ctx2 = $("#line-chart");

                var chartOptions2 = {
                    responsive: true,
                    maintainAspectRatio: false,
                    legend: {
                        position: 'bottom',
                    },
                    hover: {
                        mode: 'label'
                    },
                    scales: {
                        xAxes: [{
                            display: true,
                            gridLines: {
                                color: "#f5f5f5",
                                drawTicks: false,
                            },
                            scaleLabel: {
                                display: true,
                                labelString: 'Days'
                            }
                        }],
                        yAxes: [{
                            display: true,
                            gridLines: {
                                color: "#f5f5f5",
                                drawTicks: false,
                            },
                            scaleLabel: {
                                display: true,
                                labelString: 'Count'
                            },
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    },
                    title: {
                        display: true,
                        text: 'Line Chart'
                    }
                };

                var chartData2 = {
                    labels: labels,
                    datasets: [{
                        label: '',
                        data: datas,
                        fill: false,
                        borderColor: "#666ee8",
                        pointBorderColor: "#666ee8",
                        pointBackgroundColor: "#FFF",
                        pointBorderWidth: 2,
                        pointHoverBorderWidth: 2,
                        pointRadius: 4,
                    }]
                };

                var config2 = {
                    type: 'line',
                    options: chartOptions2,
                    data: chartData2
                };

                var lineChart = new Chart(ctx2, config2);
            }
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}

function FillHomeEscalatingTeam() {
    $("#ajax-screen-masking").show();
    $('#ddlHomeEmailMsg').find('option').remove();
    var url = apiServer + '/api/EmailGenerateTicket';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 7,
        role_id: -1
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: true,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            var feedbackarr = jQuery.makeArray(JSON.stringify(feedback).split('}'));
            var icount;
            var actionOption = '<option value="-1" style=\"color:#c0c0c0\">--- Select ---</option>';
            for (icount = 0; icount < feedbackarr.length - 1; icount++) {
                actionOption += '<option ctlvalue="' + feedback[icount].Email + '"   value="' + feedback[icount].Id + '" style=\"padding:3px\">' + feedback[icount].Username + '</option>';
            }
            $('#ddlHomeEmailMsg').append(actionOption);
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}

function SendMail(email_to, email_subject, email_body) {
    $("#ajax-screen-masking").show();
    var url = apiServer + '/api/MailChimp';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 8,
        email_to: email_to,
        email_subject: email_subject,
        email_body: email_body
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: true,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            $(".modal-body p").html("Mail sent successfully!");
            $("#divHomeModal").modal("show");
            //alert("Mail sent successfully!");
            $('#ddlHomeEmailMsg').val("-1");
            $('#txtHomeEmailMsg').val("");
            $('#inpHomeEmailMsgtxt').val("");
            $(".bootstrap-tagsinput").find("span").remove();
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}