﻿/* ===================================================== 
   *  TAB CountrySaver 
   ----------------------------------------------------- */

/* ----------------------------------------------------------------
 * Function Name    : ViewCountrySaverList
 * Purpose          : to view Country Saver List
 * Added by         : Edi Suryadi
 * Create Date      : January 29th, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function ViewCountrySaverList(MobileNo, Sitecode) {
    var url = apiServer + '/api/countrysaver';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1)
        url = apiServer + '/api/countrysaverpaym';
    jQuery.support.cors = true;
    var JSONSendData = {
        MobileNo: MobileNo,
        Sitecode: Sitecode
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#TabProductCountrySaverLoader").hide();
            $('#tblProductCountrySaver').dataTable({
                bDestroy: true,
                aaData: feedback,
                iDisplayLength: 10,
                bPaginate: true,
                bFilter: false,
                bProcessing: true,
                bInfo: false,
                aaSorting: [[0, "asc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No package found."
                },
                aoColumns: [
                    { mDataProp: "PackageID", sTitle: "Package ID" },
                    { mDataProp: "Description", sTitle: "Description" },
                    {
                        mDataProp: "SubscriptionDate", sTitle: "Subscription Date",
                        fnRender: function (ob) {
                            return convertDateISO8601(ob.aData.SubscriptionDate, 3);
                        }
                    },
                    {
                        mDataProp: "RenewalDate", sTitle: "Renewal Date",
                        fnRender: function (ob) {
                            return convertDateISO8601(ob.aData.RenewalDate, 3);
                        }
                    },
                    {
                        mDataProp: "Amount", sTitle: "Amount",
                        fnRender: function (ob) {
                            return getCurrencySymbol(ob.aData.Currency) + " " + ob.aData.Amount.toFixed(2);
                        }
                    },
                    {
                        mDataProp: "DestinationNumber", sTitle: "Destination & Number"
                    },
                    {
                        mDataProp: "MinRemaining", sTitle: "Minutes Remaining", "bUseRendered": false,
                        fnRender: function (ob) {
                            return ob.aData.MinRemaining.toFixed(1);
                        }
                    },
                    { mDataProp: "Status_Description", sTitle: "Status" },
                    { mDataProp: "PaymentType_Description", sTitle: "Pay Mode" },
                    {
                        mDataProp: "Status", sTitle: "Action", sWidth: "280px", "bUseRendered": false,
                        fnRender: function (ob) {
                            //var data_param = ob.aData.RegID + "|" + ob.aData.PackageID + "|" + ob.aData.Description + "|" + ob.aData.DestinationNumber + "|" + ob.aData.PaymentType_Description;
                            //var status = ob.aData.Status;
                            //if (status == 7) {
                            //    var actionOption = "<select class=\"selectCountrySaverAction\" data-param=\"" + data_param + "\" >";
                            //    actionOption += '<option value="0" style=\"color:#c0c0c0\">--- Select an action ---</option>';
                            //    actionOption += '<option value="2" style=\"padding:3px\">Cancel</option>';
                            //    actionOption += '</select>';
                            //} else if (status == 13) {
                            //    var actionOption = "<select class=\"selectCountrySaverAction\" data-param=\"" + data_param + "\" >";
                            //    actionOption += '<option value="0" style=\"color:#c0c0c0\">--- Select an action ---</option>';
                            //    actionOption += '<option value="1" style=\"padding:3px\">Renew</option>';
                            //    actionOption += '</select>';
                            //} else if (status == 18) {
                            //    var actionOption = "<select class=\"selectCountrySaverAction\" data-param=\"" + data_param + "\" >";
                            //    actionOption += '<option value="0" style=\"color:#c0c0c0\">--- Select an action ---</option>';
                            //    actionOption += '<option value="3" style=\"padding:3px\">Reactivate</option>';
                            //    actionOption += '</select>';
                            //} else {
                            //    var actionOption = "<select class=\"selectCountrySaverAction\" disabled>";
                            //    actionOption += '<option value="0" style=\"color:#c0c0c0\">--- No action available ---</option>';
                            //    actionOption += '</select>';
                            //}
                            //return actionOption;


                            //Rajesh Code 
                            var status = ob.aData.Status;

                            var action1 = "<select class=\"selectCountrySaverAction\" data-country=\"" + ob.aData.Country + "\" data-regno=\"" + ob.aData.Reg_Number + "\" data-pt=\"" + ob.aData.Payment_Type + "\" data-ptd=\"" + ob.aData.PaymentType_Description + "\"data-plan\"" + ob.aData.PaymentType_Description + "\"data-product\"" + ob.aData.PaymentType_Description + "\"data-ordervalue\"" + ob.aData.PaymentType_Description + "\"data-duration\"" + ob.aData.PaymentType_Description + "\" ><option value='-1' style=\"padding:3px\">--Select Action--</option>";
                            if (status != 0) {
                                action1 += "<option value='1' style=\"padding:3px\">Reactivate</option> <option value='2' style=\"padding:3px\">Cancel</option> <option value='3' style=\"padding:3px\">Change Payment Mode</option>  <option value='4' style=\"padding:3px\">View Subscription History</option>";
                            } else if (status == 1) {
                                action1 += "<option value='2' style=\"padding:3px\">Cancel</option>  <option value='3' style=\"padding:3px\">Change Payment Mode</option>  <option value='4' style=\"padding:3px\">View Subscription History</option>";
                            }
                            if (status == 2) {
                                action1 += "<option value='1' style=\"padding:3px\">Reactivate</option>  <option value='3' style=\"padding:3px\">Change Payment Mode</option>  <option value='4' style=\"padding:3px\">View Subscription History</option>";
                            }
                            action1 += "</select>";
                            return action1;
                            //Rajesh Code 

                        }
                    }
                ],
                fnDrawCallback: function () {
                    $("#tblProductCountrySaverContainer input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                },
                fnInitComplete: function (ob) {
                    $("#tblProductCountrySaverContainer input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                }
            });
            $("#tblProductCountrySaver_wrapper .row-fluid:eq(0)").remove();
        },
        error: function (feedback) {
            $("#TabProductCountrySaverLoader").hide();
            $("#tblProductCountrySaverContainer").html("").append("No package found").show();
        }
    });
}
function ViewCountrySaverListPAYM(MobileNo, Sitecode) {
    var url = apiServer + '/api/countrysaver';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1)
        url = apiServer + '/api/countrysaverpaym';
    jQuery.support.cors = true;
    var JSONSendData = {
        InfoType: 5,
        MobileNo: MobileNo,
        Sitecode: Sitecode
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#TabProductCountrySaverLoader").hide();
            $('#tblProductCountrySaver').dataTable({
                bDestroy: true,
                aaData: feedback,
                iDisplayLength: 10,
                bPaginate: true,
                bFilter: false,
                bProcessing: true,
                bInfo: false,
                aaSorting: [[0, "asc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No package found."
                },
                aoColumns: [
                    { mDataProp: "PackageID", sTitle: "Package ID" },
                    { mDataProp: "Description", sTitle: "Description" },
                    {
                        mDataProp: "SubscriptionDate", sTitle: "Subscription Date",
                        fnRender: function (ob) {
                            return convertDateISO8601(ob.aData.SubscriptionDate, 3);
                        }
                    },
                    {
                        mDataProp: "RenewalDate", sTitle: "Renewal Date",
                        fnRender: function (ob) {
                            return convertDateISO8601(ob.aData.RenewalDate, 3);
                        }
                    },
                    {
                        mDataProp: "Amount", sTitle: "Amount",
                        fnRender: function (ob) {
                            return getCurrencySymbol(ob.aData.Currency) + " " + ob.aData.Amount.toFixed(2);
                        }
                    },
                    {
                        mDataProp: "DestinationNumber", sTitle: "Destination & Number"
                    },
                    {
                        mDataProp: "MinRemaining", sTitle: "Minutes Remaining", "bUseRendered": false,
                        fnRender: function (ob) {
                            return ob.aData.MinRemaining.toFixed(1);
                        }
                    },
                    //{ mDataProp: "Status_Description", sTitle: "Status" },
                    { mDataProp: "Main_Status", sTitle: "Status" },
                    { mDataProp: "PaymentType_Description", sTitle: "Pay Mode" },
                    {
                        mDataProp: "RegID", sTitle: "Action", sWidth: "280px", "bUseRendered": false,
                        fnRender: function (ob) {
                            var data_param = ob.aData.RegID + "|" + ob.aData.PackageID + "|" + ob.aData.Description + "|" + ob.aData.DestinationNumber + "|" + ob.aData.PaymentType_Description;
                            var status = ob.aData.Status;
                            if (status == 7) {
                                var actionOption = "<select class=\"selectCountrySaverAction\" data-param=\"" + data_param + "\" >";
                                actionOption += '<option value="0" style=\"color:#c0c0c0\">--- Select an action ---</option>';
                                actionOption += '<option value="2" style=\"padding:3px\">Cancel</option>';
                                actionOption += '</select>';
                            } else if (status == 13) {
                                var actionOption = "<select class=\"selectCountrySaverAction\" data-param=\"" + data_param + "\" >";
                                actionOption += '<option value="0" style=\"color:#c0c0c0\">--- Select an action ---</option>';
                                actionOption += '<option value="1" style=\"padding:3px\">Renew</option>';
                                actionOption += '</select>';
                            } else if (status == 18) {
                                var actionOption = "<select class=\"selectCountrySaverAction\" data-param=\"" + data_param + "\" >";
                                actionOption += '<option value="0" style=\"color:#c0c0c0\">--- Select an action ---</option>';
                                actionOption += '<option value="3" style=\"padding:3px\">Reactivate</option>';
                                actionOption += '</select>';
                            } else {
                                var actionOption = "<select class=\"selectCountrySaverAction\" disabled>";
                                actionOption += '<option value="0" style=\"color:#c0c0c0\">--- No action available ---</option>';
                                actionOption += '</select>';
                            }
                            return actionOption;
                        }
                    }
                ],
                fnDrawCallback: function () {
                    $("#tblProductCountrySaverContainer input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                },
                fnInitComplete: function (ob) {
                    $("#tblProductCountrySaverContainer input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                }
            });
            $("#tblProductCountrySaver_wrapper .row-fluid:eq(0)").remove();
        },
        error: function (feedback) {
            $("#TabProductCountrySaverLoader").hide();
            $("#tblProductCountrySaverContainer").html("").append("No package found").show();
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : ViewCountrySaverList
 * Purpose          : to view Country Saver List
 * Added by         : Edi Suryadi
 * Create Date      : January 29th, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function ViewCountrySaverList_New(MobileNo, Sitecode) {
    debugger;
    
    var url = apiServer + '/api/countrysaver';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1)
        url = apiServer + '/api/countrysaverpaym';
    jQuery.support.cors = true;
    var JSONSendData = {
        InfoType: 0,
        MobileNo: MobileNo,
        Sitecode: Sitecode
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#TabProductCountrySaverLoader").hide();
            $('#tblProductCountrySaver').dataTable({
                bDestroy: true,
                aaData: feedback,
                iDisplayLength: 10,
                bPaginate: true,
                bFilter: false,
                bProcessing: true,
                bInfo: false,
                aaSorting: [[0, "asc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Data Found."
                },
                aoColumns: [
                    //02-Jan-2015 : Moorthy : Modified to remove unwanted fields from Basic Subscription Details Tab 
					{ mDataProp: "packageid", sTitle: "Package ID" },
                    { mDataProp: "description", sTitle: "Description" },
                    { mDataProp: "Main_Status", sTitle: "Status" },
                    /*{ mDataProp: "mundio1", sTitle: "Mundio CLI-1" },
                     { mDataProp: "mundio2", sTitle: "Mundio CLI-2" },
                     { mDataProp: "mundio3", sTitle: "Mundio CLI-3" },
                     { mDataProp: "mundio4", sTitle: "Mundio CLI-4" },*/
                    {
                        mDataProp: "subscriptiondate", sTitle: "Initial Subscription Date",
                        fnRender: function (ob) {
                            return convertDateISO8601(ob.aData.subscriptiondate, 3);
                        }
                    },
                    {
                        mDataProp: "RenewalDate", sTitle: "Next Renewal Date",
                        fnRender: function (ob) {
                            return convertDateISO8601(ob.aData.RenewalDate, 3);
                        }
                    },
                    { mDataProp: "amount", sTitle: "Amount" },
                    //{
                    //    mDataProp: "amount", sTitle: "Amount",
                    //    fnRender: function (ob) {
                    //        return getCurrencySymbol(ob.aData.currency) + " " + ob.aData.amount;
                    //    }
                    //},
                    { mDataProp: "destinationCli1", sTitle: "Destination Number" },
                    /*
                    { mDataProp: "destinationCli1", sTitle: "Destination CLI-1" },
                    { mDataProp: "destinationCli2", sTitle: "Destination CLI-2" },
                    { mDataProp: "destinationCli3", sTitle: "Destination CLI-3" },
                    { mDataProp: "destinationCli4", sTitle: "Destination CLI-4" },
                    { mDataProp: "destinationCli5", sTitle: "Destination CLI-5" },*/
                    { mDataProp: "Minremaining", sTitle: "Minutes Remaining" },

                    //{
                    //    mDataProp: "MinRemaining", sTitle: "Minutes Remaining", "bUseRendered": false,
                    //    fnRender: function (ob) {
                    //        return ob.aData.MinRemaining.toFixed(1);
                    //    }
                    //},

                    { mDataProp: "PayModeRerence", sTitle: "Payment Mode" },
                    {
                        mDataProp: "status", sTitle: "Action", sWidth: "280px", "bUseRendered": false,
                        fnRender: function (ob) {

                            var actionOption = "<select class=\"selectCountrySaverAction\" data-bundleid=\"" + ob.aData.bundleid + "\"data-CountrySaverPlan=\"" + ob.aData.description + "\" data-AccountId=\"" + ob.aData.AccountNumber + "\" data-pack_dest=\"" + ob.aData.pack_dest + "\"data-Minremaining=\"" + ob.aData.Minremaining + "\"data-amount=\"" + ob.aData.amount + "\"data-currency=\"" + ob.aData.currency + "\">";
                            actionOption += '<option value="0" style=\"color:#c0c0c0\">--- Select an action ---</option>';
                            if (ob.aData.DisplayCancel == true) {
                                actionOption += '<option value="1" style=\"padding:3px\">Cancel</option>';
                            }
                            if (ob.aData.DisplayReactivate == true || ob.aData.DisplaySuspended == true || ob.aData.DisplayCancelled == true) {
                                actionOption += '<option value="2" style=\"padding:3px\">Reactivate</option>';
                            }
                            //if (ob.aData.DisplayChangepaymentMode == true) {
                            //    actionOption += '<option value="4" style=\"padding:3px\">Change Payment Mode</option>';
                            //}
                            actionOption += '<option value="3" style=\"padding:3px\">View Subscription History </option>';
                            actionOption += '</select>';
                            return actionOption;


                        }
                    }
                ],
                fnDrawCallback: function () {
                    $("#tblProductCountrySaverContainer input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                },
                fnInitComplete: function (ob) {
                    $("#tblProductCountrySaverContainer input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                }
            });
            $("#tblProductCountrySaver_wrapper .row-fluid:eq(0)").remove();
        },
        error: function (feedback) {
            $("#TabProductCountrySaverLoader").hide();
            $("#tblProductCountrySaverContainer").html("").append("No Data Found").show();
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : CountrySaversubscriptionHistory
 * Purpose          : View Country Saver Subscription list
 * Added by         : 
 * Create Date      : 
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function CountrySaversubscriptionHistory(Mobile, Sitecode, bundleid) {
    debugger;
    var url = apiServer + '/api/countrysaver';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1)
        url = apiServer + '/api/countrysaverpaym';
    jQuery.support.cors = true;
    var JSONSendData = {
        InfoType: 9,
        MobileNo: Mobile,
        Sitecode: Sitecode,
        bundleid: bundleid
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('#tblCountrySaverSubscriptionHistory').dataTable({
                bDestroy: true,
                aaData: feedback,
                iDisplayLength: 10,
                bPaginate: true,
                bFilter: false,
                bProcessing: true,
                bInfo: false,
                aaSorting: [[1, "desc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Subscription History found."
                },
                aoColumns:
                    [
                    { mDataProp: "Action", sTitle: "Action" },
                    {
                        mDataProp: "Date", sTitle: "Date", sType: "date-euro",
                        fnRender: function (ob) {
                            return convertDateISO8601(ob.aData.Date, 3);
                        }
                    },
                    { mDataProp: "Bundle_Details", sTitle: "Bundle Details" },
                    { mDataProp: "PaymentMode", sTitle: "Payment Mode" },
                    { mDataProp: "Payment_Status", sTitle: "Payment Status" }
                    ]
            });
            $('#tblCountrySaverSubscriptionHistory').css('width', '100%');
            $('#CountrySaverSubscriptionHistory').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        },
        error: function (feedback) {
            $("tblCountrySaverSubscriptionHistory").html("").append("No data found").show();
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : CountrySaverCancel
 * Purpose          :  CountrySaver Cancel Method
 * Added by         : 
 * Create Date      : 
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function CountrySaverCancelPAYM(accountid, Destination, Mobileno, Sitecode) {
    debugger;
    var url = apiServer + '/api/countrysaver';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1)
        url = apiServer + '/api/countrysaverpaym';
    //end changed
    jQuery.support.cors = true;
    var JSONSendData = {
        InfoType: 3,
        accountid: accountid,
        DestinationCli: Destination
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {

            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            if (feedback[0].errcode === 0) {

                $('.PMSuccess .modal-header').html(feedback[0].errsubject);
                $('.PMSuccess .modal-body').html(feedback[0].errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
                ViewCountrySaverListLLOM(Mobileno, Sitecode);
            }
            else {

                $('.PMFailed .modal-header').html(feedback[0].errsubject);
                $('.PMFailed .modal-body').html(feedback[0].errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (feedback) {

            $("tblCountrySaverSubscriptionHistory").html("").append("No data found").show();
        }
    });
}

function CountrySaverCancel(accountid, Destination, Mobileno, Sitecode) {
    var url = apiServer + '/api/countrysaver';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1)
        url = apiServer + '/api/countrysaverpaym';
    //end changed
    jQuery.support.cors = true;
    var JSONSendData = {
        InfoType: 3, //added by Hari - 25-02-2015
        accountid: accountid,
        DestinationCli: Destination
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {

            $('.PMSuccess .modal-footer .btnPMSuccessOKvalue').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOKvalue').attr({ "alt": "1" });
            if (feedback[0].errcode === 0) {

                $('.PMSuccess .modal-header').html(feedback[0].errsubject);
                $('.PMSuccess .modal-body').html(feedback[0].errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
                ViewCountrySaverList_New(Mobileno, Sitecode);
            }
            else {

                $('.PMFailed .modal-header').html(feedback[0].errsubject);
                $('.PMFailed .modal-body').html(feedback[0].errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (feedback) {

            $("tblCountrySaverSubscriptionHistory").html("").append("No data found").show();
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : GetLLOTGSavedCC
 * Purpose          : Get LLOTG Saved Credit Card
 * Added by         : Rajesh Natarajan
 * Create Date      : Dec 22, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */

function GetCountrySaverSavedCC(mobileno, sitecode) {
    //var url = apiServer + '/api/countrysaver';
    //jQuery.support.cors = true;
    //var JSONSendData = {
    //    //Id: 1,
    //    InfoType: 13,
    //    Mobileno: mobileno,
    //    Sitecode: sitecode,
    //};


    var url = apiServer + '/api/LLOTG';
    jQuery.support.cors = true;
    var JSONSendData = {
        Id: 1,
        Action: 4,
        MobileNo: mobileno,
        Sitecode: sitecode,
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('#selectocscccard').text('');
            $.each(feedback, function (key, value) {

                var cc = value.CardNumber.substring(0, 2) + ' ' + value.CardNumber.substring(2);
                $('#selectocscccard').append('<option value="' + value.CardNumber + '">XXXX XX' + cc + '</option>');
            });
        },
        error: function (feedback) {
            $("#TabProductLLOTGLoader").hide();
            $("#tblProductLLOTGContainer").html("").append("No package found").show();
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : CountrySaverCancel
 * Purpose          :  CountrySaver Cancel Method
 * Added by         : 
 * Create Date      : 
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function CountrySaverReactivtebybalance(Mobileno, Sitecode) {
    var url = apiServer + '/api/countrysaver';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1)
        url = apiServer + '/api/countrysaverpaym';
    //end changed
    jQuery.support.cors = true;
    var JSONSendData = {
        InfoType: 10, //added by Hari - 25-02-2015
        Mobileno: Mobileno,
        Sitecode: Sitecode
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {

            $('.PMSuccess .modal-footer .btnPMSuccessOKvalue').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOKvalue').attr({ "alt": "1" });
            if (feedback[0].errcode === 0) {

                $('.PMSuccess .modal-header').html(feedback[0].errsubject);
                $('.PMSuccess .modal-body').html(feedback[0].errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
                ViewCountrySaverList_New(Mobileno, Sitecode);
            }
            else {

                $('.PMFailed .modal-header').html(feedback[0].errsubject);
                $('.PMFailed .modal-body').html(feedback[0].errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (feedback) {

            $("tblCountrySaverSubscriptionHistory").html("").append("No data found").show();
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : PayByExistingCard
 * Purpose          : LLOTG Pay By Existing Credit Card
 * Added by         : Rajesh Natarajan
 * Create Date      : Dec 22, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function REACTIVATEBYEXISTINGCARD(cardNumber, cardcvv, CurMobileno, sitecode, productcode, amount) {

    var url = apiServer + '/api/countrysaver';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1)
        url = apiServer + '/api/countrysaverpaym';
    //var url = 'http://localhost:4520/' + 'api/countrysaver'; 
    jQuery.support.cors = true;
    var IpAdd = "";
    $.getJSON("http://jsonip.com?callback=?", function (data) {
        IpAdd = data.ip;
    });
    //var paymode = "";
    //if (LLOTGCurPayment_Type === "Pay By Credit") {
    //    var paymode = 2;
    //} else {
    //    var paymode = 1;
    //}
    jQuery.support.cors = true;

    var JSONSendData = {
        Id: 1,
        InfoType: 11,
        Mobileno: CurMobileno,
        Sitecode: sitecode,
        LoggedUser: productcode + ',' + amount + ',' + cardcvv + ',' + cardNumber
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (data) {
            $('.PMSuccess .modal-footer .btnPMSuccessOKvalue').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOKvalue').attr({ "alt": "1" });
            if (data.errcode == 0) {
                $('.PMSuccess .modal-header').html(data.errsubject);
                $('.PMSuccess .modal-body').html(data.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
                ViewCountrySaverList_New(mobileno, sitecode);
            }
            else {

                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (feedback) {
            $("#TabProductCountrySaverLoader").hide();
            $("#tblProductCountrySaverContainer").html("").append("No package found").show();
            return false;
            // alert(data);
        }
    });
    return true;
}

/* ----------------------------------------------------------------
 * Function Name    : PayByExistingCard
 * Purpose          : LLOTG Pay By Existing Credit Card
 * Added by         : Rajesh Natarajan
 * Create Date      : Dec 22, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function REACTIVATEBYNEWCARD(CurMobileno, sitecode, LLOTGCurProductCode, amount, currency, desinatioNumber) {

    //alert(desinatioNumber);
    var url = apiServer + '/api/countrysaver';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1)
        url = apiServer + '/api/countrysaverpaym';
    // jQuery.support.cors = true;
    //var paymode = "";
    //if (LLOTGCurPayment_Type === "Pay By Credit") {
    //    var paymode = 2;
    //} else {
    //    var paymode = 1;
    //}

    var JSONSendData = {
        InfoType: 12,
        Mobileno: CurMobileno,
        Sitecode: sitecode,
        LoggedUser:
         $("input:radio[name=PaymentMethodCardType]").val() + "^" +
        $('#PaymentMethodCCNumber').val() + "^" +
        $('#PaymentMethodFirstName').val() + "^" +
        $('#PaymentMethodLastName').val() + "^" +
        $('#PaymentMethodExpiryMonth').val() + "^" +
         $('#PaymentMethodExpiryYear').val() + "^" +
        $('#PaymentMethodCardVerfCode').val() + "^" +
        $('#iCSPBCAddress1').val() + "^" +
        //$('#PaymentMethodHouseNo').val() + "^" +
        $('#iCSPBCAddress2').val() + "^" +
        //$('#PaymentMethodAddress').val() + "^" +
          $('#iCSPBCTown').val() + "^" +
        //$('#PaymentMethodCity').val() + "^" +
       // $('#PaymentMethodCountry').val() + "^" +
      $("#sCSPBCCountry").find("option:selected").text() + "^" +
        $('#PaymentMethodPostcode').val() + "^" +
        $('#iCSPBCEmail').val() + "^" +
        $('#PaymentMethodPhone').val() + "^" +
         $('#PaymentMethodIssueNumber').val() + "^" +
        amount + "^" +
                LLOTGCurProductCode + "^" +
                 currency + "^" +
                desinatioNumber
    };

    $.ajax
({
    url: url,
    type: 'post',
    data: JSON.stringify(JSONSendData),
    dataType: 'json',
    contentType: "application/json;charset=utf-8",
    cache: false,
    success: function (data) {

        $('.PMSuccess .modal-footer .btnPMSuccessOKvalue').attr({ "alt": "1" });
        $('.PMFailed .modal-footer .btnPMFailedOKvalue').attr({ "alt": "1" });
        if (data.errcode == 0) {
            $('.PMSuccess .modal-header').html(data.errsubject);
            $('.PMSuccess .modal-body').html(data.errmsg);
            $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
            ViewCountrySaverList_New(CurMobileno, sitecode);
        }
        else {

            $('.PMFailed .modal-header').html(data.errsubject);
            $('.PMFailed .modal-body').html(data.errmsg);
            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }

    }, error: function (feedback) {
        $("#TabProductLLOTGLoader").hide();
        $("#tblProductLLOTGContainer").html("").append("No package found").show();
        return false;
    }
});
    //  return true;
};

/* ----------------------------------------------------------------
 * Function Name    : validateNewCC
 * Purpose          : validate New Credit Card form
 * Added by         : Rajesh Natarajan
 * Create Date      : Dec 22, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function validateNewCC1() {

    var isvalid = true;
    var csrdtype = $("input:radio[name=PaymentMethodCardType]").is(":checked");
    if (!csrdtype) {
        $('#PSCT').show();
        isvalid = false;
    } else {
        $('#PSCT').hide();

    }
    if ($('#PaymentMethodCCNumber').val() == "") {
        isvalid = false;
        $('#PECN').show();
    }
    else {
        $('#PECN').hide();
    }
    if ($('#PaymentMethodFirstName').val() == "") {
        isvalid = false;
        $('#PEFN').show();
    }
    else {
        $('#PEFN').hide();
    }
    if ($('#PaymentMethodLastName').val() == "") {
        isvalid = false;
        $('#PELN').show();
    }
    else {
        $('#PELN').hide();
    }
    if ($('#PaymentMethodExpiryMonth').val() == "0" && $('#PaymentMethodExpiryYear').val() == "0") {
        isvalid = false;
        $('#PEEMAY').show();
    }
    else {
        $('#PEEMAY').hide();
    }
    if ($('#PaymentMethodCardVerfCode').val() == "") {
        isvalid = false;
        $('#PECVC').show();
    }
    else {
        $('#PECVC').hide();
    }
    if ($('#iCSPBCAddress1').val() == "") {
        isvalid = false;
        $('#PEHN').show();
        $('#lblAddr1').show();
    }
    else {
        $('#PEHN').hide();
        $('#lblAddr1').hide();
    }
    if ($('#iCSPBCAddress2').val() == "") {
        isvalid = false;
        $('#PEA').show();
        $('#lblAddr2').show();
    }
    else {
        $('#PEA').hide();
        $('#lblAddr2').hide();
    }
    if ($('#iCSPBCAddress3').val() == "") {
        isvalid = false;
        $('#PEA').show();
        $('#lblAddr3').show();
    }
    else {
        $('#PEA').hide();
        $('#lblAddr3').hide();
    }
    if ($('#iCSPBCTown').val() == "") {
        isvalid = false;
        $('#PEC').show();
        $('#lblCity').show();
    }
    else {
        $('#PEC').hide();
        $('#lblCity').hide();
    }
    if ($('#sCSPBCCountry').val() == "-1") {
        isvalid = false;
        $('#PECAddress').show();
        $('#lblCountry').show();
    }
    else {
        $('#PECAddress').hide();
        $('#lblCountry').hide();
    }
    if ($('#PaymentMethodPostcode').val() == "") {
        isvalid = false;
        $('#PEPC').show();
        $('#lblPostCode').show();
    }
    else {
        $('#PEPC').hide();
        $('#lblPostCode').hide();
    }
    if ($('#iCSPBCEmail').val() != "") {
        var isemailvalid = validateEmail($('#iCSPBCEmail').val());
        if (!isemailvalid) {
            isvalid = false;
            $('#PEAVEA').show();
            $('#lblEmail').show();
        } else {
            $('#PEAVEA').hide();
            $('#lblEmail').hide();
        }
    }
    else {
        $('#PEAVEA').show();
        $('#lblEmail').show();
    }
    //if ($('#PaymentMethodPhone').val() == "") {
    //    isvalid = false;
    //    $('#PEAVP').show();
    //}
    //else {
    //    $('#PEAVP').hide();
    //}
    return isvalid;
}

function clearnewcardCountrySaver() {

    $("#PaymentMethodCCNumber").val('');
    $("#PaymentMethodFirstName").val('');
    $("#PaymentMethodLastName").val('');
    $("#PaymentMethodIssueNumber").val('');
    $("#PaymentMethodCardVerfCode").val('');
    $("#iCSPBCAddress1").val('');
    $("#iCSPBCAddress2").val('');
    $("#iCSPBCAddress3").val('');
    $("#iCSPBCTown").val('');
    $("#PaymentMethodPostcode").val('');
    $("#iCSPBCEmail").val('');
    $("#PaymentMethodPhone").val('');


}

/* ----------------------------------------------------------------
 * Function Name    : ViewCountrySaverADDCLIList
 * Purpose          : to view Country Saver List ADDCLI
 * Added by         : Rajesh Natarajan
 * Create Date      : Dec 26th, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function ViewCountrySaverADDCLIList(MobileNo, Sitecode) {
    var url = apiServer + '/api/countrysaver';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1)
        url = apiServer + '/api/countrysaverpaym';
    //end changed
    jQuery.support.cors = true;
    var JSONSendData = {
        InfoType: 5, //added by Hari - 25-02-2015
        MobileNo: MobileNo,
        Sitecode: Sitecode
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            //alert("success");            
            $("#TabProductCountrySaverADDCLILoader").hide();



            $.each(feedback, function (index, value) {
                console.log('My array has at position ' + index + ', this value: ' + value);

                var html = ("<div class=\"portlet box blue pbAdditionalCLI\" id=\"pbAdditionalCLI" + index + "\">");
                html = html + ("<div class=\"portlet-title\" style=\"height:30px;border-bottom:0\"><h4><span style=\"font-size:12px;font-weight:bold\">Package Name: " + feedback[index].Description + "</span></h4></div>");
                html = html + ("<div class=\"portlet-body\" id=\"pbAdditionalCLIBody" + index + "\")>");
                html = html + ("<table id=\"tblCountrySaverPackageInfo" + index + "\" border=\"0\" cellspacing=\"3\" cellpadding=\"3\">");
                html = html + ("<tbody>");
                html = html + ("<tr>");
                html = html + ("<td style=\"width:140px;\">Initial Subscription Date</td>");
                html = html + ("<td id=\"tdInitialSubscriptionDate" + index + "\" style=\"width:610px;border-bottom:1px dotted #e5e5e5;color:#505050\">" + convertDateISO8601(feedback[index].SubscriptionDate, 3) + "</td>");
                html = html + ("</tr>");
                html = html + ("<tr>");
                html = html + ("<td>Renewal Date</td>");
                html = html + ("<td id=\"tdRenewalDate" + index + "\" style=\"border-bottom:1px dotted #e5e5e5;color:#505050\">" + convertDateISO8601(feedback[index].RenewalDate, 3) + "</td>");
                html = html + ("</tr>");
                html = html + ("<tr>");
                html = html + ("<td>Destination CLI</td>");
                html = html + ("<td id=\"tdDestinationCLI" + index + "\" style=\"border-bottom:1px dotted #e5e5e5;color:#505050\">" + feedback[index].DestinationNumber + "</td>");
                html = html + ("</tr>");
                html = html + ("</tbody>");
                html = html + ("</table>");


                html = html + ("<table class=\"table table-bordered table-hover\" id=\"tblAdditionalCLI" + index + "\"></table>");
                html = html + ("<input type=\"hidden\" id=\"hdnCLI" + index + "\" value=\"" + feedback[index].Account_ID + "\" />");
                html = html + ("<input type=\"hidden\" id=\"hdnAccountID" + index + "\" value=\"" + feedback[index].AccountNumber + "\" />");
                html = html + ("<input type=\"button\" id=\"BtnAddCLI" + index + "\" class=\"btn blue BtnAddCLI\" value=\"Add\" style=\"float: right\" />");
                //html = html +("<input type=\"button\" id=\"BtnAddCLI\" value=\"Add\" style=\"float: right\" />");
                html = html + ("</div>");
                html = html + ("</div>");

                $("#CountrySaverAdditionalCLIContainer").append(html);

                $('#tblAdditionalCLI' + index).dataTable({
                    bDestroy: true,
                    aaData: feedback[index].AdditionalCLIs,
                    iDisplayLength: 10,
                    bPaginate: true,
                    bFilter: false,
                    bProcessing: true,
                    bInfo: false,
                    aaSorting: [[0, "asc"]],
                    oLanguage: {
                        "sInfoEmpty": '',
                        "sEmptyTable": "No package found."
                    },
                    aoColumns: [
                        { mDataProp: "CLI", sTitle: "Mundio CLI", sWidth: "100px" },
                        { mDataProp: "SubscriptionDate", sTitle: "Subscription Date", sWidth: "100px" },
                        { mDataProp: "MinUsed", sTitle: "Minutes Used", sWidth: "100px" },
                        { mDataProp: "Product_Type", sTitle: "Number Type", sWidth: "100px" },
                        { mDataProp: "Status", sTitle: "Status", sWidth: "100px" },
                        {
                            mDataProp: "Action", sTitle: "Action", sWidth: "200px",
                            fnRender: function (ob) {
                                var actionURL = "";
                                actionURL = "<table> <tr> <td style=\"border:none\"><button class=\"btn blue BtnChangeCLI\">Change CLI</button></td> <td style=\"border:none\"><button class=\"btn blue BtnRemoveCLI\">Delete CLI</button> </td> </tr> </table>";
                                return actionURL;
                            }
                        }
                        /*
                        { mDataProp: "Product_Type", sTitle: "Number Type" },
                        {
                            mDataProp: "SubscriptionDate", sTitle: "Action",
                            fnRender: function (ob) {
                                return "<input type='button' class='BtnRemoveCLI' value='Remove' />";
                            }
                        },
                        */
                    ],
                    fnDrawCallback: function (oSettings) {
                        $("#CountrySaverAdditionalCLIContainer input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                    }
                    ,
                    fnInitComplete: function (ob) {
                        $("#CountrySaverAdditionalCLIContainer input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                    }
                });

                $("#tblAdditionalCLI" + index + "_wrapper .row-fluid").remove();
            })



            //$("#tblAdditionalCLI_wrapper .row-fluid:eq(0)").remove();            
        },
        error: function (feedback) {
            $("#TabProductCountrySaverADDCLILoader").hide();
            $("#CountrySaverAdditionalCLIContainer").html("").append("No package found").show();
        }
    });
}
function ViewCountrySaverADDCLIListPAYM(MobileNo, Sitecode) {
    debugger;
    var url = apiServer + '/api/countrysaver';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1)
        url = apiServer + '/api/countrysaverpaym';
    //end changed
    jQuery.support.cors = true;
    var JSONSendData = {
        InfoType: 6,
        MobileNo: MobileNo,
        Sitecode: Sitecode
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            //alert("success");            
            $("#TabProductCountrySaverADDCLILoader").hide();



            //$.each(feedback, function (index, value) {
            //    console.log('My array has at position ' + index + ', this value: ' + value);

            //    var html = ("<div class=\"portlet box blue pbAdditionalCLI\" id=\"pbAdditionalCLI" + index + "\">");
            //    html = html + ("<div class=\"portlet-title\" style=\"height:30px;border-bottom:0\"><h4><span style=\"font-size:12px;font-weight:bold\">Package Name: " + feedback[index].Description + "</span></h4></div>");
            //    html = html + ("<div class=\"portlet-body\" id=\"pbAdditionalCLIBody" + index + "\")>");
            //    html = html + ("<table id=\"tblCountrySaverPackageInfo" + index + "\" border=\"0\" cellspacing=\"3\" cellpadding=\"3\">");
            //    html = html + ("<tbody>");
            //    html = html + ("<tr>");
            //    html = html + ("<td style=\"width:140px;\">Initial Subscription Date</td>");
            //    html = html + ("<td id=\"tdInitialSubscriptionDate" + index + "\" style=\"width:610px;border-bottom:1px dotted #e5e5e5;color:#505050\">" + convertDateISO8601(feedback[index].SubscriptionDate, 3) + "</td>");
            //    html = html + ("</tr>");
            //    html = html + ("<tr>");
            //    html = html + ("<td>Renewal Date</td>");
            //    html = html + ("<td id=\"tdRenewalDate" + index + "\" style=\"border-bottom:1px dotted #e5e5e5;color:#505050\">" + convertDateISO8601(feedback[index].RenewalDate, 3) + "</td>");
            //    html = html + ("</tr>");
            //    html = html + ("<tr>");
            //    html = html + ("<td>Destination CLI</td>");
            //    html = html + ("<td id=\"tdDestinationCLI" + index + "\" style=\"border-bottom:1px dotted #e5e5e5;color:#505050\">" + feedback[index].DestinationNumber + "</td>");
            //    html = html + ("</tr>");
            //    html = html + ("</tbody>");
            //    html = html + ("</table>");


            //    html = html + ("<table class=\"table table-bordered table-hover\" id=\"tblAdditionalCLI" + index + "\"></table>");
            //    html = html + ("<input type=\"hidden\" id=\"hdnCLI" + index + "\" value=\"" + feedback[index].Account_ID + "\" />");
            //    html = html + ("<input type=\"hidden\" id=\"hdnAccountID" + index + "\" value=\"" + feedback[index].AccountNumber + "\" />");
            //    html = html + ("<input type=\"button\" id=\"BtnAddCLI" + index + "\" class=\"btn blue BtnAddCLI\" value=\"Add\" style=\"float: right\" />");
            //    //html = html +("<input type=\"button\" id=\"BtnAddCLI\" value=\"Add\" style=\"float: right\" />");
            //    html = html + ("</div>");
            //    html = html + ("</div>");

            //    $("#CountrySaverAdditionalCLIContainer").append(html);

            $('#tblAdditionalCLI').dataTable({
                bDestroy: true,
                aaData: feedback,
                iDisplayLength: 10,
                bPaginate: true,
                bFilter: false,
                bProcessing: true,
                bInfo: false,
                aaSorting: [[0, "asc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No package found."
                },
                aoColumns: [
                    { mDataProp: "addon_msisdn", sTitle: "Number", sWidth: "100px" },
                    { mDataProp: "subscription_Date", sTitle: "Subscription Date", sWidth: "100px" },
                    { mDataProp: "minutes_used", sTitle: "Minutes Used", sWidth: "100px" },
                    { mDataProp: "clitype", sTitle: "Number Type", sWidth: "100px" }
                    /*,{ mDataProp: "Status", sTitle: "Status", sWidth: "100px" },
                    {
                        mDataProp: "Action", sTitle: "Action", sWidth: "200px",
                        fnRender: function (ob) {
                            var actionURL = "";
                            actionURL = "<table> <tr> <td style=\"border:none\"><button class=\"btn blue BtnChangeCLI\">Change CLI</button></td> <td style=\"border:none\"><button class=\"btn blue BtnRemoveCLI\">Delete CLI</button> </td> </tr> </table>";
                            return actionURL;
                        }
                    }*/
                    /*
                    { mDataProp: "Product_Type", sTitle: "Number Type" },
                    {
                        mDataProp: "SubscriptionDate", sTitle: "Action",
                        fnRender: function (ob) {
                            return "<input type='button' class='BtnRemoveCLI' value='Remove' />";
                        }
                    },
                    */
                ],
                fnDrawCallback: function (oSettings) {
                    $("#CountrySaverAdditionalCLIContainer input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                }
                ,
                fnInitComplete: function (ob) {
                    $("#CountrySaverAdditionalCLIContainer input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                }
            });

            $("#tblAdditionalCLI_wrapper .row-fluid").remove();
            //})



            //$("#tblAdditionalCLI_wrapper .row-fluid:eq(0)").remove();            
        },
        error: function (feedback) {
            $("#TabProductCountrySaverADDCLILoader").hide();
            $("#CountrySaverAdditionalCLIContainer").html("").append("No package found").show();
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : RefreshCountrySaverList
 * Purpose          : to refresh Country Saver List
 * Added by         : Edi Suryadi
 * Create Date      : February 04th, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function RefreshCountrySaverList(MobileNo, Sitecode) {

    $("#TabProductCountrySaverLoader").show();
    $("#tblProductCountrySaver_wrapper").remove();
    $("#tblProductCountrySaverContainer").append("<table class=\"table table-bordered table-hover\" id=\"tblProductCountrySaver\"></table>");

    var url = apiServer + '/api/countrysaver';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1)
        url = apiServer + '/api/countrysaverpaym';
    jQuery.support.cors = true;
    var JSONSendData = {
        MobileNo: MobileNo,
        Sitecode: Sitecode
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#TabProductCountrySaverLoader").hide();
            $('#tblProductCountrySaver').dataTable({
                bDestroy: true,
                aaData: feedback,
                iDisplayLength: 10,
                bPaginate: true,
                bFilter: false,
                bProcessing: true,
                bInfo: false,
                aaSorting: [[0, "asc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No package found."
                },
                aoColumns: [
                    { mDataProp: "PackageID", sTitle: "Package ID" },
                    { mDataProp: "Description", sTitle: "Description" },
                    {
                        mDataProp: "SubscriptionDate", sTitle: "Subscription Date",
                        fnRender: function (ob) {
                            return convertDateISO8601(ob.aData.SubscriptionDate, 3);
                        }
                    },
                    {
                        mDataProp: "RenewalDate", sTitle: "Renewal Date",
                        fnRender: function (ob) {
                            return convertDateISO8601(ob.aData.RenewalDate, 3);
                        }
                    },
                    {
                        mDataProp: "Amount", sTitle: "Amount",
                        fnRender: function (ob) {
                            return getCurrencySymbol(ob.aData.Currency) + " " + ob.aData.Amount.toFixed(2);
                        }
                    },
                    {
                        mDataProp: "DestinationNumber", sTitle: "Destination & Number"
                    },
                    {
                        mDataProp: "MinRemaining", sTitle: "Minutes Remaining", "bUseRendered": false,
                        fnRender: function (ob) {
                            return ob.aData.MinRemaining.toFixed(1);
                        }
                    },
                    { mDataProp: "Status_Description", sTitle: "Status" },
                    { mDataProp: "PaymentType_Description", sTitle: "Pay Mode" },
                    {
                        mDataProp: "RegID", sTitle: "Action", sWidth: "280px", "bUseRendered": false,
                        fnRender: function (ob) {
                            var data_param = ob.aData.RegID + "|" + ob.aData.PackageID + "|" + ob.aData.Description + "|" + ob.aData.DestinationNumber + "|" + ob.aData.PaymentType_Description;
                            var status = ob.aData.Status;
                            status = 18;
                            if (status == 7) {
                                var actionOption = "<select class=\"selectCountrySaverAction\" data-param=\"" + data_param + "\" >";
                                actionOption += '<option value="0" style=\"color:#c0c0c0\">--- Select an action ---</option>';
                                actionOption += '<option value="2" style=\"padding:3px\">Cancel</option>';
                                actionOption += '</select>';
                            } else if (status == 13) {
                                var actionOption = "<select class=\"selectCountrySaverAction\" data-param=\"" + data_param + "\" >";
                                actionOption += '<option value="0" style=\"color:#c0c0c0\">--- Select an action ---</option>';
                                actionOption += '<option value="1" style=\"padding:3px\">Renew</option>';
                                actionOption += '</select>';
                            } else if (status == 18) {
                                var actionOption = "<select class=\"selectCountrySaverAction\" data-param=\"" + data_param + "\" >";
                                actionOption += '<option value="0" style=\"color:#c0c0c0\">--- Select an action ---</option>';
                                actionOption += '<option value="3" style=\"padding:3px\">Reactivate</option>';
                                actionOption += '</select>';
                            } else {
                                var actionOption = "<select class=\"selectCountrySaverAction\" disabled>";
                                actionOption += '<option value="0" style=\"color:#c0c0c0\">--- No action available ---</option>';
                                actionOption += '</select>';
                            }
                            return actionOption;
                        }
                    }
                ],
                fnDrawCallback: function () {
                    $("#tblProductCountrySaverContainer input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                },
                fnInitComplete: function (ob) {
                    $("#tblProductCountrySaverContainer input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                }
            });
            $("#tblProductCountrySaver_wrapper .row-fluid:eq(0)").remove();
        },
        error: function (feedback) {
            $("#TabProductCountrySaverLoader").hide();
            $("#tblProductCountrySaverContainer").html("").append("No package found").show();
        }
    });
}

function CCRegisteredList(mobileno) {
    $("#tblCSPBCRegisteredCard").html("");
    var url = apiServer + '/api/payment/' + mobileno;
    $.getJSON(url, function (feedback) {
        $("#tblCSPBCRegisteredCard").dataTable({
            aaData: feedback,
            bDestroy: true,
            bRetrieve: true,
            bFilter: false,
            bSort: false,
            bLengthChange: false,
            aaSorting: [[0, "asc"]],
            oLanguage: {
                "sEmptyTable": 'No registered credit card found'
            },
            aoColumns: [
                {
                    mDataProp: "account_id", sTitle: "", "bUseRendered": false,
                    fnRender: function (ob) {
                        var optValue = "<center><input type=\"radio\" name=\"optSelectRegisteredCC\" value=\"" + ob.aData.Last6digitsCC + "\" data-subsid=\"" + ob.aData.SubscriptionID + "\"></center>";
                        return optValue;
                    }
                },
                {
                    mDataProp: "Last6digitsCC", sTitle: "Card Number", "bUseRendered": false,
                    fnRender: function (ob) {
                        var CardNo = "XXXX XXXX XX" + ob.aData.Last6digitsCC.toString().substr(0, 2) + " " + ob.aData.Last6digitsCC.toString().substr(2, 4);
                        return CardNo;
                    }
                },
                {
                    mDataProp: "expirydate", sTitle: "Expire date", "bUseRendered": false,
                    fnRender: function (ob) {
                        return convertDate06(ob.aData.expirydate, 1);
                    }
                },
                {
                    mDataProp: "account_id", sTitle: "CVC2 (Security) Numbers", sWidth: "280px", "bUseRendered": false,
                    fnRender: function (ob) {
                        var iCVCNumber = "<input type=\"text\" class=\"iCVCNumber\" />";
                        return iCVCNumber;
                    }
                }
            ]
        });
        $("#tblCSPBCRegisteredCard_wrapper .row-fluid").remove();
        $("#tblCSPBCRegisteredCard").show();
    });
}

/* ----------------------------------------------------------------
 * Function Name    : ReactivateByBalance
 * Purpose          : to reactivate country saver by balance
 * Added by         : Edi Suryadi
 * Create Date      : February 04th, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function ReactivateByBalance(SiteCode, RegIDX) {
    var url = apiServer + '/api/countrysaver';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1)
        url = apiServer + '/api/countrysaverpaym';

    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    jQuery.support.cors = true;
    var JSONSendData = {
        SiteCode: SiteCode,
        InfoType: 2,
        RegIDX: RegIDX,
        Paymode: 1 // By Balance
    };
    window.console.log(url);
    window.console.log(JSONSendData);
    //$.ajax({
    //    url: url,
    //    type: 'post',
    //    data: JSON.stringify(JSONSendData),
    //    dataType: 'json',
    //    contentType: "application/json;charset=utf-8",
    //    cache: false,
    //    beforeSend: function () {
    //        $("#ajax-screen-masking").show();
    //    },
    //    success: function (data) {
    //        $("#ajax-screen-masking").hide();

    //        $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
    //        $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });

    //        var stat = data.errcode;
    //        if (stat == 0) {
    //            $('.PMSuccess .modal-header').html(data.errsubject);
    //            $('.PMSuccess .modal-body').html(data.errmsg);
    //            $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
    //                'margin-left': function () {
    //                    return window.pageXOffset - ($(this).width() / 2);
    //                }
    //            });
    //        } else {
    //            $('.PMFailed .modal-header').html(data.errsubject);
    //            $('.PMFailed .modal-body').html(data.errmsg);
    //            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
    //                'margin-left': function () {
    //                    return window.pageXOffset - ($(this).width() / 2);
    //                }
    //            });
    //        }
    //    },
    //    error: function (data) {
    //        $("#ajax-screen-masking").hide();
    //        $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
    //        $('.PMFailed .modal-header').html(data.errsubject);
    //        $('.PMFailed .modal-body').html(data.errmsg);
    //        $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
    //            'margin-left': function () {
    //                return window.pageXOffset - ($(this).width() / 2);
    //            }
    //        });
    //    }
    //});
}

/* ----------------------------------------------------------------
 * Function Name    : ReactivateByNewCard
 * Purpose          : to reactivate country saver by New Card
 * Added by         : Edi Suryadi
 * Create Date      : February 04th, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function ReactivateByNewCard(SiteCode, RegIDX) {
    var url = apiServer + '/api/countrysaver';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1)
        url = apiServer + '/api/countrysaverpaym';

    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    jQuery.support.cors = true;
    var JSONSendData = {
        SiteCode: SiteCode,
        InfoType: 2,
        RegIDX: RegIDX,
        Paymode: 1 // By Balance
    };
    window.console.log(url);
    window.console.log(JSONSendData);
    //$.ajax({
    //    url: url,
    //    type: 'post',
    //    data: JSON.stringify(JSONSendData),
    //    dataType: 'json',
    //    contentType: "application/json;charset=utf-8",
    //    cache: false,
    //    beforeSend: function () {
    //        $("#ajax-screen-masking").show();
    //    },
    //    success: function (data) {
    //        $("#ajax-screen-masking").hide();

    //        $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
    //        $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });

    //        var stat = data.errcode;
    //        if (stat == 0) {
    //            $('.PMSuccess .modal-header').html(data.errsubject);
    //            $('.PMSuccess .modal-body').html(data.errmsg);
    //            $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
    //                'margin-left': function () {
    //                    return window.pageXOffset - ($(this).width() / 2);
    //                }
    //            });
    //        } else {
    //            $('.PMFailed .modal-header').html(data.errsubject);
    //            $('.PMFailed .modal-body').html(data.errmsg);
    //            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
    //                'margin-left': function () {
    //                    return window.pageXOffset - ($(this).width() / 2);
    //                }
    //            });
    //        }
    //    },
    //    error: function (data) {
    //        $("#ajax-screen-masking").hide();
    //        $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
    //        $('.PMFailed .modal-header').html(data.errsubject);
    //        $('.PMFailed .modal-body').html(data.errmsg);
    //        $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
    //            'margin-left': function () {
    //                return window.pageXOffset - ($(this).width() / 2);
    //            }
    //        });
    //    }
    //});
}

/* ----------------------------------------------------------------
 * Function Name    : ReactivateByRegisteredCard
 * Purpose          : to reactivate country saver by Registered Card
 * Added by         : Edi Suryadi
 * Create Date      : February 04th, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function ReactivateByRegisteredCard(MobileNo, CCNumber, CVCNumber) {
    var url = apiServer + '/api/payment/';

    var ajaxSpinnerTop = ($(window).outerHeight() / 3) + $(window).scrollTop();
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    var url = apiServer + '/api/payment/';
    jQuery.support.cors = true;
    var JSONSendData = {
        order_records: 8,
        account_id: MobileNo,
        card_number: CCNumber,
        card_verf_code: CVCNumber
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            var stat = data.errcode;
            if (stat == 0) {
                window.console.log("PASSED");
            } else {
                $("#ajax-screen-masking").hide();
                $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "" });
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (data) {
            $("#ajax-screen-masking").hide();
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "" });
            $('.PMFailed .modal-header').html(data.errsubject);
            $('.PMFailed .modal-body').html(data.errmsg);
            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });

}

/* ----------------------------------------------------------------
 * Function Name    : RenewByBalance
 * Purpose          : to renew country saver by balance
 * Added by         : Edi Suryadi
 * Create Date      : February 06th, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function RenewByBalance(SiteCode, RegIDX) {
    var url = apiServer + '/api/countrysaver';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1)
        url = apiServer + '/api/countrysaverpaym';

    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    jQuery.support.cors = true;
    var JSONSendData = {
        SiteCode: SiteCode,
        InfoType: 2,
        RegIDX: RegIDX,
        Paymode: 1 // By Balance
    };
    window.console.log(url);
    window.console.log(JSONSendData);
    //$.ajax({
    //    url: url,
    //    type: 'post',
    //    data: JSON.stringify(JSONSendData),
    //    dataType: 'json',
    //    contentType: "application/json;charset=utf-8",
    //    cache: false,
    //    beforeSend: function () {
    //        $("#ajax-screen-masking").show();
    //    },
    //    success: function (data) {
    //        $("#ajax-screen-masking").hide();

    //        $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
    //        $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });

    //        var stat = data.errcode;
    //        if (stat == 0) {
    //            $('.PMSuccess .modal-header').html(data.errsubject);
    //            $('.PMSuccess .modal-body').html(data.errmsg);
    //            $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
    //                'margin-left': function () {
    //                    return window.pageXOffset - ($(this).width() / 2);
    //                }
    //            });
    //        } else {
    //            $('.PMFailed .modal-header').html(data.errsubject);
    //            $('.PMFailed .modal-body').html(data.errmsg);
    //            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
    //                'margin-left': function () {
    //                    return window.pageXOffset - ($(this).width() / 2);
    //                }
    //            });
    //        }
    //    },
    //    error: function (data) {
    //        $("#ajax-screen-masking").hide();
    //        $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
    //        $('.PMFailed .modal-header').html(data.errsubject);
    //        $('.PMFailed .modal-body').html(data.errmsg);
    //        $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
    //            'margin-left': function () {
    //                return window.pageXOffset - ($(this).width() / 2);
    //            }
    //        });
    //    }
    //});
}

/* ----------------------------------------------------------------
 * Function Name    : RenewByNewCard
 * Purpose          : to renew country saver by New Card
 * Added by         : Edi Suryadi
 * Create Date      : February 04th, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function RenewByNewCard(SiteCode, RegIDX) {
    var url = apiServer + '/api/countrysaver';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1)
        url = apiServer + '/api/countrysaverpaym';

    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    jQuery.support.cors = true;
    var JSONSendData = {
        SiteCode: SiteCode,
        InfoType: 2,
        RegIDX: RegIDX,
        Paymode: 1 // By Balance
    };
    window.console.log(url);
    window.console.log(JSONSendData);
    //$.ajax({
    //    url: url,
    //    type: 'post',
    //    data: JSON.stringify(JSONSendData),
    //    dataType: 'json',
    //    contentType: "application/json;charset=utf-8",
    //    cache: false,
    //    beforeSend: function () {
    //        $("#ajax-screen-masking").show();
    //    },
    //    success: function (data) {
    //        $("#ajax-screen-masking").hide();

    //        $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
    //        $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });

    //        var stat = data.errcode;
    //        if (stat == 0) {
    //            $('.PMSuccess .modal-header').html(data.errsubject);
    //            $('.PMSuccess .modal-body').html(data.errmsg);
    //            $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
    //                'margin-left': function () {
    //                    return window.pageXOffset - ($(this).width() / 2);
    //                }
    //            });
    //        } else {
    //            $('.PMFailed .modal-header').html(data.errsubject);
    //            $('.PMFailed .modal-body').html(data.errmsg);
    //            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
    //                'margin-left': function () {
    //                    return window.pageXOffset - ($(this).width() / 2);
    //                }
    //            });
    //        }
    //    },
    //    error: function (data) {
    //        $("#ajax-screen-masking").hide();
    //        $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
    //        $('.PMFailed .modal-header').html(data.errsubject);
    //        $('.PMFailed .modal-body').html(data.errmsg);
    //        $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
    //            'margin-left': function () {
    //                return window.pageXOffset - ($(this).width() / 2);
    //            }
    //        });
    //    }
    //});
}

/* ----------------------------------------------------------------
 * Function Name    : RenewByRegisteredCard
 * Purpose          : to renew country saver by Registered Card
 * Added by         : Edi Suryadi
 * Create Date      : February 04th, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function RenewByRegisteredCard(MobileNo, CCNumber, CVCNumber) {
    var url = apiServer + '/api/payment/';

    var ajaxSpinnerTop = ($(window).outerHeight() / 3) + $(window).scrollTop();
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    var url = apiServer + '/api/payment/';
    jQuery.support.cors = true;
    var JSONSendData = {
        order_records: 8,
        account_id: MobileNo,
        card_number: CCNumber,
        card_verf_code: CVCNumber
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            var stat = data.errcode;
            if (stat == 0) {
                window.console.log("PASSED");
            } else {
                $("#ajax-screen-masking").hide();
                $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "" });
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (data) {
            $("#ajax-screen-masking").hide();
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "" });
            $('.PMFailed .modal-header').html(data.errsubject);
            $('.PMFailed .modal-body').html(data.errmsg);
            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });

}

$.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
    return {
        "iStart": oSettings._iDisplayStart,
        "iEnd": oSettings.fnDisplayEnd(),
        "iLength": oSettings._iDisplayLength,
        "iTotal": oSettings.fnRecordsTotal(),
        "iFilteredTotal": oSettings.fnRecordsDisplay(),
        "iPage": oSettings._iDisplayLength === -1 ?
			0 : Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
        "iTotalPages": oSettings._iDisplayLength === -1 ?
			0 : Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
    };
};

/* ----------------------------------------------------------------
 * Function Name    : ADDCLIAdd
 * Purpose          : to add new CLI
 * Added by         : Hari S
 * Create Date      : February 23rd, 2015
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function ADDCLIAdd(mobileNo, RegNo, siteCode, UserName, CLIType, txtCLINumber) {
    var url = apiServer + "/api/additionalcli/";
    jQuery.support.cors = true;
    var JSONSendData = {
        /*order_type: 11,
        order_id: orderID,
        imsi_country: imsiCountry,
        mobileNo: mobileNo,
        crm_login: loginID*/


        InfoType: 14,
        CLI: txtCLINumber,
        RegIDX: RegNo,
        Sitecode: siteCode,
        Mobileno: mobileNo,
        Product_Type: CLIType,
        //InfoType     
        //SubscriptionDate
        //MinUsed
        //Product_Type: 

    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $(".modal-backdrop, #portlet-config2").hide();
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();

            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });

            var stat = data.errcode;
            if (stat == 0) {
                $('.PMSuccess .modal-header').html(data.errsubject);
                $('.PMSuccess .modal-body').html(data.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            } else {
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (data) {
            $("#ajax-screen-masking").hide();
            $('.PMFailed .modal-header').html('General Error');
            $('.PMFailed .modal-body').html('Request failed.');
            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : ResetControlValuesmodalmsgAddCLI
 * Purpose          : to resset Add CLI modal msg box
 * Added by         : Hari S
 * Create Date      : February 24th, 2015
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function ResetControlValuesmodalmsgAddCLI() {
    $('input#byMobile').prop('checked', false);
    $('input#byLandLine').prop('checked', false);
    document.getElementById("byMobile").checked = false;
    document.getElementById("byLandLine").checked = false;
    $('input#byMobile').parent().removeClass("checked");
    $('input#byLandLine').parent().removeClass("checked");
    $('input#byMobile').parent().attr("aria-checked", "false");
    $('input#byLandLine').parent().attr("aria-checked", "false");
    $('input#txtCLINumber').val("");
    $("#modalADDCLIAdd label.errLabel").hide();
}

function DeleteCLIForm(SelsubscriberID, CLI, CLINumber, AccountNumber, FandFNumber) {
    var url = apiServer + "/api/additionalcli/";
    jQuery.support.cors = true;
    var JSONSendData = {
        InfoType: 15,
        SubscriberId: SelsubscriberID,
        FFNumber: FandFNumber,
        CLI: CLINumber,
        AccountId: AccountNumber
    };

    var rows = "";
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            if (feedback.errcode == 0) {
                $("#modalDeleteCLIConfirm").modal('hide');

                $('#ResultInfo').find("div:eq(1)").html(feedback.errmsg);
                $('#ResultInfo').find("div:eq(2) > button:eq(0)").onclick("window.location.reload();")
                $('#ResultInfo').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
                //document.getElementById("hdnFormAction").value = "DeleteCLI";
                //window.location = '/Customer/TabAdditionalCLIDetails/' + $.trim('@ViewBag._mobileno');
            }
            else {
                $("#modalDeleteCLIConfirm").modal('hide');
                $('#ErrMsg').html(feedback.errmsg);
                $('#NoData').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        }
    });
}

function ValidateAddCLIForm(bNext, controlId, subscriberId, cli, brand, DestNumber, AccountNumber) {
    var passed = true;
    $("#modalADDCLIAdd label.errLabel").hide();
    if ($('input#byMobile:checked').length == 0) {
        if ($('input#byLandLine:checked').length == 0) {
            $("#modalADDCLIAdd label.errLabel").eq(0).show();
            passed = false;
        }
    }
    if ($('input#txtCLINumber').val() == "") {
        $("#modalADDCLIAdd label.errLabel").eq(1).html($("#modalADDCLIAdd label.errLabel").eq(1).html().replace('Number is valid!!', 'Please enter a valid number!!').replace('Please enter a valid number!!', 'Please enter a valid number!!'));
        $("#modalADDCLIAdd label.errLabel:eq(1)").css({ 'background': '#f2dede', 'color': '#c84c4c' });
        $("#modalADDCLIAdd label.errLabel:eq(1) img").attr("src", "../../img/invalid_button.gif");
        $("#modalADDCLIAdd label.errLabel:eq(1) img").css({ height: 20, width: 20 });
        $("#modalADDCLIAdd label.errLabel").eq(1).show();
        passed = false;
    }
    else {

        //validate CLI Number
        //if ($('input#txtCLINumber').val().trim().length != 10) {
        //    alert('Length of CLI should be 10')
        //    passed = false;
        //}            
        //var url = '../../Customer/ValidateVectoneCLI';
        var url = apiServer + "/api/additionalcli/";

        //var subscriberId = $("#hdnSelSubscriberId").val();
        //var fandf = $("#hdnFandFNumber").val();
        //var subscriberId = null;
        var fandf = null;

        //var cli = $('input#hdnCLI').val();
        //var brand = $('input#hdnBrand').val();

        var cliType = GetNewCLICLIType();
        var cliNumber = GetNewCLICLINo();
        /*
        var DestNumber = 'Srilanka 94726183066';
        var AccountNumber = '2008453';
        */

        var JSONSendData = {
            InfoType: 16,
            //SubscriberId: subscriberId,
            //RegIDX: subscriberId,
            FandFNo: DestNumber,
            Mobileno: cli,
            Brand: brand,
            Product_Type: cliType,
            CLI: cliNumber,
            AccountId: AccountNumber
        };

        $.ajax
        ({
            url: url,
            type: 'post',
            data: JSON.stringify(JSONSendData),
            dataType: 'json',
            contentType: "application/json;charset=utf-8",
            cache: false,
            success: function (feedback) {
                if (feedback.errcode == 0) {
                    if (bNext) {
                        /*
                        if (controlId == "addCLINext") {
                            $("#modalADDCLIAdd").modal('hide');
                            $("#testmodel #myModalLabel").html("Payment Mode");
                            $("#testmodel #lblhead").html("Select the mode of payment");
                            $('#testmodel').modal({
                                backdrop: 'static',
                                keyboard: false
                            });
                        } else
                        */
                        if (controlId == "btnADDCLIAdd") {
                            /*
                            var FandFNumber = $('#hdnFandFNumber').val();
                            var AccountNumber = $('#hdnAccountNumber').val();
                            var CLI = $('#hdnCLI').val();
                            var Brand = $('#hdnBrand').val();
                            var SelSubscriberId = $('#hdnSelSubscriberId').val();
                            */

                            var FandFNumber = null;
                            //var AccountNumber = null;
                            //var CLI = $('#hdnCLI').val();
                            //var Brand = $('#hdnBrand').val();
                            var SelSubscriberId = subscriberId;

                            var CLIType = GetNewCLICLIType();
                            var CLINumber = GetNewCLICLINo();


                            jQuery.support.cors = true;
                            var JSONSendData = {
                                InfoType: 6,
                                //SubscriberId: SelSubscriberId,
                                //RegIDX: subscriberId,
                                FandFNo: DestNumber,
                                Mobileno: cli,
                                Brand: brand,
                                Product_Type: CLIType,
                                CLI: CLINumber,
                                AccountId: AccountNumber
                            };

                            var url = apiServer + "/api/additionalcli/";
                            var rows = "";
                            $.ajax
                            ({
                                url: url,
                                type: 'post',
                                data: JSON.stringify(JSONSendData),
                                dataType: 'json',
                                contentType: "application/json;charset=utf-8",
                                cache: false,
                                success: function (feedback) {
                                    if (feedback.errcode == 0) {
                                        $("#modalADDCLIAdd").modal('hide');

                                        $('#ResultInfo').find("div:eq(1)").html(feedback.errmsg);
                                        $('#ResultInfo').find("div:eq(2) > button:eq(0)").onclick("window.location.reload();")
                                        $('#ResultInfo').modal({ keyboard: false, backdrop: 'static' }).css({
                                            'margin-left': function () {
                                                return window.pageXOffset - ($(this).width() / 2);
                                            }
                                        });

                                        //document.getElementById("hdnFormAction").value = "DeleteCLI";
                                        //document.getElementById("hdnFormAction").value = "AddCLI";
                                        //$('form#frm').submit();
                                        //window.location = '/Customer/TabAdditionalCLIDetails/' + $.trim('@ViewBag._mobileno');                                        
                                    }
                                    else {
                                        $("#modalADDCLIAdd").modal('hide');

                                        $('#ErrMsg').html(feedback.errmsg);
                                        $('#NoData').modal({ keyboard: false, backdrop: 'static' }).css({
                                            'margin-left': function () {
                                                return window.pageXOffset - ($(this).width() / 2);
                                            }
                                        });

                                        //$("#modalmsgNote h3#myModalLabel").text("Add failed");
                                        //$("#modalmsgNote label#lblhead").text(feedback.err_message);
                                        //$('#modalmsgNote').modal({
                                        //    backdrop: 'static',
                                        //    keyboard: false
                                        //});
                                        return;
                                    }
                                }
                            });
                        }

                    } //if (bNext)
                    else {
                        $("#modalADDCLIAdd label.errLabel").eq(1).html($("#modalADDCLIAdd label.errLabel").eq(1).html().replace('Please enter a valid number!!', 'Number is valid!!').replace('Number is valid!!', 'Number is valid!!'));
                        $("#modalADDCLIAdd label.errLabel:eq(1)").css({ 'background': 'transparent', 'color': '#000000' });
                        $("#modalADDCLIAdd label.errLabel:eq(1) img").attr("src", "../../img/valid_button-check_blue.png");
                        $("#modalADDCLIAdd label.errLabel:eq(1) img").css({ height: 20, width: 20 });
                        $("#modalADDCLIAdd label.errLabel").eq(1).show();
                    }
                    passed = true;
                    return passed;
                }
                else { //feedback.errcode == 0
                    $("#modalADDCLIAdd label.errLabel").eq(1).html($("#modalADDCLIAdd label.errLabel").eq(1).html().replace('Number is valid!!', 'Please enter a valid number!!').replace('Please enter a valid number!!', 'Please enter a valid number!!'));
                    $("#modalADDCLIAdd label.errLabel:eq(1)").css({ 'background': '#f2dede', 'color': '#c84c4c' });
                    $("#modalADDCLIAdd label.errLabel:eq(1) img").attr("src", "../../img/invalid_button.gif");
                    $("#modalADDCLIAdd label.errLabel:eq(1) img").css({ height: 20, width: 20 });
                    $("#modalADDCLIAdd label.errLabel").eq(1).show();
                    passed = false;
                    return passed;
                }
            }
        });
    }
}

/*This code is for Customer Details - Add New CLI functionality*/
function GetNewCLICLIType() {

    if ($('input#byMobile:checked').length > 0) {
        return "M";
    }
    if ($('input#byLandLine:checked').length > 0) {
        return "L";
    }
    return null;
}

function GetNewCLICLINo() {
    cli = $('input#txtCLINumber').val();
    return cli;
}

/* ----------------------------------------------------------------
 * Function Name    : ViewCountrySaverList
 * Purpose          : to view Country Saver List
 * Added by         : Edi Suryadi
 * Create Date      : January 29th, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function ViewCountrySaverListLLOM(MobileNo, Sitecode) {
    debugger;
    
    var url = apiServer + '/api/countrysaver';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1)
        url = apiServer + '/api/countrysaverpaym';
    jQuery.support.cors = true;
    var JSONSendData = {
        InfoType: 5,
        MobileNo: MobileNo,
        Sitecode: Sitecode,

    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#TabProductCountrySaverLoader").hide();
            $('#tblProductCountrySaver').dataTable({
                bDestroy: true,
                aaData: feedback,
                iDisplayLength: 10,
                bPaginate: true,
                bFilter: false,
                bProcessing: true,
                bInfo: false,
                aaSorting: [[0, "asc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No package found."
                },
                aoColumns: [
                    { mDataProp: "packageid", sTitle: "Package ID" },
                    { mDataProp: "description", sTitle: "Description" },
                    { mDataProp: "Main_Status", sTitle: "Status" },
                    {
                        mDataProp: "subscriptiondate", sTitle: "Initial Subscription Date",
                        fnRender: function (ob) {
                            return convertDateISO8601(ob.aData.subscriptiondate, 3);
                        }
                    },
                    {
                        mDataProp: "RenewalDate", sTitle: "Next Renewal Date",
                        fnRender: function (ob) {
                            return convertDateISO8601(ob.aData.RenewalDate, 3);
                        }
                    },
                    { mDataProp: "amount", sTitle: "Amount" },
                    { mDataProp: "destinationCli1", sTitle: "Destination Number" },
                    { mDataProp: "Minremaining", sTitle: "Minutes Remaining" },
                    { mDataProp: "PayModeRerence", sTitle: "Payment Mode" },
                    //{ mDataProp: "BundleID", sTitle: "Bundle ID" },
                    //{ mDataProp: "Description", sTitle: "Bundle Name" },
                    ////Add by BSK for CRMT-221 reqd.
                    //{
                    //    mDataProp: "AccountNo", sTitle: "Account Number"
                    //},
                    //{
                    //    mDataProp: "DestinationNumber", sTitle: "F&F CLI"
                    //},
                    //{
                    //    mDataProp: "SubscriptionDate", sTitle: "Subscription Date",
                    //    fnRender: function (ob) {
                    //        return convertDateISO8601(ob.aData.SubscriptionDate, 3);
                    //    }
                    //},
                    //{
                    //    mDataProp: "RenewalDate", sTitle: "Renewal Date",
                    //    fnRender: function (ob) {
                    //        return convertDateISO8601(ob.aData.RenewalDate, 3);
                    //    }
                    //},
                    //{
                    //    mDataProp: "MinRemaining", sTitle: "Remaining Minutes", "bUseRendered": false,
                    //    fnRender: function (ob) {
                    //        return ob.aData.MinRemaining.toFixed(1);
                    //    }
                    //},
                    //{
                    //    mDataProp: "Amount", sTitle: "Amount",
                    //    fnRender: function (ob) {
                    //        return getCurrencySymbol(ob.aData.Currency) + " " + ob.aData.Amount;
                    //    }
                    //},                    
                    //{ mDataProp: "PaymentType_Description", sTitle: "Pay Mode" },
                    ////{ mDataProp: "Status_Description", sTitle: "Status" },                    
                    //{ mDataProp: "Main_Status", sTitle: "Status" },
                    {
                        mDataProp: "status", sTitle: "Action", sWidth: "280px", "bUseRendered": false,
                        fnRender: function (ob) {
                            var actionOption = "<select class=\"selectCountrySaverLLOMAction\" data-bundleid=\"" + ob.aData.bundleid + "\"data-CountrySaverPlan=\"" + ob.aData.description + "\" data-AccountId=\"" + ob.aData.AccountNumber + "\" data-pack_dest=\"" + ob.aData.pack_dest + "\"data-Minremaining=\"" + ob.aData.Minremaining + "\"data-amount=\"" + ob.aData.amount + "\"data-currency=\"" + ob.aData.currency + "\"data-packageid=\"" + ob.aData.packageid + "\"data-paymode=\"" + ob.aData.PayModeRerence + "\">";
                            actionOption += '<option value="0" style=\"color:#c0c0c0\">--- Select an action ---</option>';
                            if (ob.aData.DisplayCancel == true) {
                                actionOption += '<option value="1" style=\"padding:3px\">Cancel</option>';
                            }
                            if (ob.aData.DisplayReactivate == true || ob.aData.DisplaySuspended == true) {
                                actionOption += '<option value="2" style=\"padding:3px\">Reactivate</option>';
                            }
                            actionOption += '<option value="3" style=\"padding:3px\">View Subscription History </option>';
                            actionOption += '</select>';
                            return actionOption;
                            //var data_param = ob.aData.RegID + "|" + ob.aData.PackageID + "|" + ob.aData.BundleID + "|" + ob.aData.Description + "|" + ob.aData.DestinationNumber + "|" + ob.aData.PaymentType_Description;
                            //var data_packdest = ob.aData.Pack_Dest;
                            //var statusdesc = ob.aData.Main_Status;
                            //var data_AccountNo = ob.aData.AccountNo;
                            //var data_Amount = ob.aData.Amount;
                            //if (statusdesc == 'Active') {
                            //    //var actionOption = "<select class=\"selectCountrySaverLLOMAction\" data-packdest=\"" + data_packdest + "\" data-param=\"" + data_param + "\" >";
                            //    var actionOption = "<select class=\"selectCountrySaverLLOMAction\"  data-packdest=\"" + data_packdest + "\" data-AccountNo=\"" + data_AccountNo + "\" data-Amount=\"" + data_Amount + "\" data-param=\"" + data_param + "\" >";
                            //    actionOption += '<option value="0" style=\"color:#c0c0c0\">--- Select an action ---</option>';
                            //    actionOption += '<option value="2" style=\"padding:3px\">Cancel</option>';
                            //    //added for testing
                            //    //actionOption += '<option value="3" style=\"padding:3px\">Reactivate</option>';
                            //    //end added
                            //    actionOption += '<option value="4" style=\"padding:3px\">View Subscription History</option>';
                            //    actionOption += '</select>';
                            //}
                            ///*
                            //else if (status == 13) {
                            //    var actionOption = "<select class=\"selectCountrySaverLLOMAction\" data-packdest=\"" + data_packdest + "\" data-param=\"" + data_param + "\" >";
                            //    actionOption += '<option value="0" style=\"color:#c0c0c0\">--- Select an action ---</option>';
                            //    actionOption += '<option value="1" style=\"padding:3px\">Renew</option>';
                            //    actionOption += '<option value="4" style=\"padding:3px\">View Subscription History</option>';
                            //    actionOption += '</select>';
                            //} 
                            //*/
                            //else if (statusdesc == 'Cancelled' || statusdesc == 'Suspended') {
                            //    //var actionOption = "<select class=\"selectCountrySaverLLOMAction\" data-packdest=\"" + data_packdest + "\" data-param=\"" + data_param + "\" >";
                            //    var actionOption = "<select class=\"selectCountrySaverLLOMAction\" data-packdest=\"" + data_packdest + "\" data-AccountNo=\"" + data_AccountNo + "\" data-Amount=\"" + data_Amount + "\" data-param=\"" + data_param + "\" >";
                            //    actionOption += '<option value="0" style=\"color:#c0c0c0\">--- Select an action ---</option>';                                
                            //    actionOption += '<option value="3" style=\"padding:3px\">Reactivate</option>';
                            //    actionOption += '<option value="4" style=\"padding:3px\">View Subscription History</option>';
                            //    actionOption += '</select>';
                            //} else {
                            //    //var actionOption = "<select class=\"selectCountrySaverAction\" disabled>";
                            //    //actionOption += '<option value="0" style=\"color:#c0c0c0\">--- No action available ---</option>';
                            //    //var actionOption = "<select class=\"selectCountrySaverLLOMAction\" data-packdest=\"" + data_packdest + "\" data-param=\"" + data_param + "\" >";
                            //    var actionOption = "<select class=\"selectCountrySaverLLOMAction\" data-packdest=\"" + data_packdest + "\" data-AccountNo=\"" + data_AccountNo + "\" data-Amount=\"" + data_Amount + "\" data-param=\"" + data_param + "\" >";
                            //    actionOption += '<option value="0" style=\"color:#c0c0c0\">--- Select an action ---</option>';
                            //    actionOption += '<option value="4" style=\"padding:3px\">View Subscription History</option>';
                            //    actionOption += '</select>';
                            //}
                            //return actionOption;
                        }
                    }
                ],
                fnDrawCallback: function () {
                    $("#tblProductCountrySaverContainer input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                },
                fnInitComplete: function (ob) {
                    $("#tblProductCountrySaverContainer input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                }
            });
            $("#tblProductCountrySaver_wrapper .row-fluid:eq(0)").remove();
        },
        error: function (feedback) {
            $("#TabProductCountrySaverLoader").hide();
            $("#tblProductCountrySaverContainer").html("").append("No package found").show();
        }
    });
}


/*added on 19-01-2015 - by Hari - Country Saver Subscription History*/
//Add by BSK for CRMT-221 reqd.
function FillCountrySaverSubscriptionHistory(MobileNo, Sitecode, BundleId, AccountNo) {
    var url = apiServer + '/api/LOTG';
    jQuery.support.cors = true;
    var JSONSendData = {
        Id: BundleId,
        Action: 14,
        MobileNo: MobileNo,
        Sitecode: Sitecode,
        AccountNo: AccountNo
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('#tblCountrySaverSubscriptionHistory').dataTable({
                bDestroy: true,
                aaData: feedback,
                iDisplayLength: 10,
                bPaginate: true,
                bFilter: false,
                bProcessing: true,
                bInfo: false,
                aaSorting: [[0, "asc"]],
                aoColumns: [                   
                    {
                        mDataProp: "Date", sTitle: "Date",
                        fnRender: function (ob) {
                            return convertDateISO8601(ob.aData.Date, 3);
                        }
                    },
                    { mDataProp: "Action", sTitle: "Action" },
                    { mDataProp: "BundleDetails", sTitle: "Bundle Details" },
                    { mDataProp: "Amount", sTitle: "Amount" },
                    { mDataProp: "PaymentMode", sTitle: "Payment Mode" },
                    { mDataProp: "PaymentStatus", sTitle: "Payment Status" }
                ],
                fnDrawCallback: function () {
                    $("#tblCountrySaverSubscriptionHistoryContainer input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                },
                fnInitComplete: function (ob) {
                    $("#tblCountrySaverSubscriptionHistoryContainer input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                }
            });

            $('#CountrySaverSubscriptionHistory').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });

            $("#tblCountrySaverSubscriptionHistory_wrapper .row-fluid:eq(0)").remove();
        },
        error: function (feedback) {
            //$("#TabProductLOTGLoader").hide();
            $("#tblCountrySaverSubscriptionHistoryContainer").html("").append("No package found").show();
        }
    });
}
/*end added*/



/* ----------------------------------------------------------------
 * Function Name    : ReactivateByRegisteredCardCountrySaver
 * Purpose          : to reactivate country saver by Registered Card
 * Added by         : Edi Suryadi
 * Create Date      : February 04th, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function ReactivateByRegisteredCardCountrySaver(MobileNo, CCNumber, CVCNumber, SiteCode, BundleId, DestNo, totalValue, AccountNo) {
    //var url = apiServer + '/api/payment/';

    var ajaxSpinnerTop = ($(window).outerHeight() / 3) + $(window).scrollTop();
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    
    var url = apiServer + "/api/PAYMPlanBundles";
    var JSONSendData = {
        requestType: 82,
        SiteCode: SiteCode,
        MobileNo: MobileNo,
        //BundleId: BundleId,
        newplan: totalValue,
        //newplantext: newPlanName,
        calledby: "CRM-API",
        //ordervalue: orderValue,
        //discount: voucherValue,
        totalvalue: totalValue,
        card_number: CCNumber,
        card_verf_code: CVCNumber,
        SubscriptionID: BundleId,
        AccountNo: AccountNo
        //card_number: cardNo,
        //card_expiry_month: expMonth,
        //card_expiry_year: expYear,
        //card_issue_number: issueNo,
        //card_verf_code: cardVerf,
        //card_first_name: firstName,
        //card_last_name: lastName,
        //card_post_code: postCode,
        //card_house_number: houseNo,
        //card_address: address,
        //card_city: city,
        //card_country: country,
        //card_type: typeCard,
        //Reg_Number: RegNo,
        //SvcId: SvcId,
        //username: UserName,
        //payment_mode: payType
    };
    $.ajax
    ({
        url: url,
        type: "post",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            var stat = data[0].errcode;
            if (stat == 0) {
                window.console.log("PASSED");
                $('.PMSuccess .modal-header').html(data[0].errsubject);
                $('.PMSuccess .modal-body').html(data[0].errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            } else {
                $("#ajax-screen-masking").hide();
                $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "" });
                $('.PMFailed .modal-header').html(data[0].errsubject);
                $('.PMFailed .modal-body').html(data[0].errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (data) {
            $("#ajax-screen-masking").hide();
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "" });
            $('.PMFailed .modal-header').html(data.errsubject);
            $('.PMFailed .modal-body').html(data.errmsg);
            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    //}).done(function () {
    //    location.reload();
    });

}

/* ----------------------------------------------------- 
   *  eof TAB CountrySaver 
   ===================================================== */


/* ----------------------------------------------------------------
 * Function Name    : SCBTab
 * Purpose          : To display Somalia Country Saver data table
 * Added by         : Edi Suryadi
 * Create Date      : August 1st, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function CSBTab(mobileNo) {
    $("#CSBMainWrapper").append("<table class=\"table table-bordered table-hover\" id=\"tbcsb\"></table>");
    var url = apiServer + '/api/customercsb/' + mobileNo
    $.getJSON(url, function (data) {

        var oTable = $('#tbcsb').dataTable({
            aaData: data,
            bDestroy: true,
            bRetrieve: true,
            bFilter: false,
            bLengthChange: false,
            aaSorting: [[0, "asc"]],
            oLanguage: {
                "sInfo": 'Found <b>_TOTAL_</b> bundle(s).',
                "sInfoEmpty": '',
                "sEmptyTable": "No bundle found."
            },
            aoColumns: [

				{ mDataProp: "account_id", sTitle: 'No' },
				{ mDataProp: "dest_msisdn", sTitle: 'Dest. Number' },
				{ mDataProp: "dest_country", sTitle: 'Country' },
                { mDataProp: "new_sim", sTitle: 'New SIM' },
				{ mDataProp: "subscription_string", sTitle: 'Subscription' },
				{ mDataProp: "expiry_string", sTitle: 'Expiry' },
                { mDataProp: "renewal_method", sTitle: 'Renewal Method' },
                { mDataProp: "status", sTitle: 'Status' },
                { mDataProp: "remaining", sTitle: 'Remain' },
				{
				    mDataProp: "account_id", sTitle: 'Actions', bSortable: false,
				    fnRender: function (ob) {
				        if (ob.aData.new_sim != null) {
				            var isNew = 0;
				            if (ob.aData.new_sim.toLowerCase() == 'yes') {
				                var isNew = 1;
				            }
				        } else {
				            var isNew = 0;
				        }

				        var content = '<select class="selectCSBAction" onchange="CSBAction(this.value)" alt="' + ob.aData.account_id + '">';
				        content += '<option value="0">--- Select an action ---</option>';
				        content += '<option value="1_' + ob.aData.account_id + '|' + ob.aData.dest_msisdn + '|' + mobileNo + '|' + ob.aData.dest_country + '|' + ob.aData.renewal_method + '|' + ob.aData.order_id + '|' + isNew + '">View History</option>';
				        if (ob.aData.status !== null) {
				            if (ob.aData.status.toLowerCase() === 'active') {
				                if (ob.aData.renewal_method.toLowerCase() !== 'cancelled') {
				                    content += '<option value="2_' + ob.aData.account_id + '|' + ob.aData.dest_msisdn + '|' + mobileNo + '|' + ob.aData.dest_country + '|' + ob.aData.renewal_method + '|' + ob.aData.order_id + '|' + isNew + '">Change Payment Method</option>';
				                    content += '<option value="3_' + ob.aData.account_id + '|' + ob.aData.dest_msisdn + '|' + mobileNo + '|' + ob.aData.dest_country + '|' + ob.aData.renewal_method + '|' + ob.aData.order_id + '|' + isNew + '">Cancel</option>';
				                }
				            } else if (ob.aData.status.toLowerCase() === 'inactive') {
				                content += '<option value="4_' + ob.aData.account_id + '|' + ob.aData.dest_msisdn + '|' + mobileNo + '|' + ob.aData.dest_country + '|' + ob.aData.renewal_method + '|' + ob.aData.order_id + '|' + isNew + '">Re-Activate</option>';
				            }
				        }
				        content += '</select>';
				        return content;
				    }
				}
            ],
            fnDrawCallback: function (ob) {
                if (ob.bSorted || ob.bFiltered) {
                    for (var i = 0, iLen = ob.aiDisplay.length ; i < iLen ; i++) {
                        $('td:eq(0)', ob.aoData[ob.aiDisplay[i]].nTr).html(i + 1);
                    }
                }
                $("table#tbcsb select.selectCSBAction").css({ "color": "#a0a0a0" });
                $("table#tbcsb select.selectCSBAction option").not(":eq(0)").css({ "color": "#000000" });
                $("table#tbcsb option").css({ "padding": "3px 5px" });

            }
        });
        getLastCardUsed(mobileNo);
        $("#CSBMainWrapper").css({ "background-image": "none" });
        $('#tbcsb').show();
    })
}
