﻿//BreakageUsage
$(document).ready(function () {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!;
    var yyyy = today.getFullYear();
    if (dd < 10) { dd = '0' + dd } if (mm < 10) { mm = '0' + mm } today = dd + '/' + mm + '/' + yyyy;
    var date_fr = today;
    var date_to = today;

    var lastdate1 = $('#lastdate1').val();
    if (lastdate1 == '') {
        $('#dp1').attr('data-date', today);
        $('#dp1').datepicker().on('changeDate', function (e) {
            $('div.datepicker').hide();
        });
        $('#startdate').attr('value', today);
        $('#lastdate1').val(today);
    }
    else {
        $('#dp1').attr('data-date', lastdate1);
        $('#dp1').datepicker().on('changeDate', function (e) {
            $('div.datepicker').hide();
        });
        $('#startdate').attr('value', lastdate1);
    }

    var lastdate2 = $('#lastdate2').val();
    if (lastdate2 == '') {
        $('#dp2').attr('data-date', today);
        $('#dp2').datepicker().on('changeDate', function (e) {
            $('div.datepicker').hide();
        });
        $('#enddate').attr('value', today);
        $('#lastdate2').val(today);
    }
    else {
        $('#dp2').attr('data-date', lastdate2);
        $('#dp2').datepicker().on('changeDate', function (e) {
            $('div.datepicker').hide();
        });
        $('#enddate').attr('value', lastdate2);
    }
});
function BreakageUsage(mobileno, sitecode, date_fr, date_to) {
    $("#chFilterResultBody").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblCHFilterResult\"></table>");
    $("#chFilterResultBody").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var row = 0;
    var url = apiServer + "/api/BreakageUsage";
    var JSONSendData = {
        mobileno: mobileno,
        sitecode: sitecode,
        date_fr: date_fr,
        date_to: date_to
    };
    $.ajax
    ({
        url: url,
        type: "POST",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#chFilterResultBody").css({ "background-image": "none" });
            $('#tblCHFilterResult').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                iDisplayLength: 10,
                bPaginate: true,
                bFilter: false,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No records found."
                },
                aoColumns: [
                    //{
                    //    mDataProp: "logdate", sTitle: "Date",
                    //    fnRender: function (ob) {
                    //        return convertDate01(ob.aData.logdate);
                    //    }
                    //},
                    { mDataProp: "logdate", sTitle: "Date", sWidth: "200px" },
                    { mDataProp: "mobileno", sTitle: "Mobile no" },
                    { mDataProp: "package", sTitle: "Package", sWidth: "280px" },
                    { mDataProp: "GPRS_CHARGE", sTitle: "GPRS Usage MB" },
                    { mDataProp: "PACKAGE_ID", sTitle: "Package ID" },
                    { mDataProp: "INITIAL_BALANCE_MB", sTitle: "Initial Balance MB" },
                    { mDataProp: "ASSIGNED_BALANCE_MB", sTitle: "Assigned Balance MB" },
                ],
                fnDrawCallback: function () {
                    $("#tblCHFilterResult tbody td").css({ "font-size": "12px" });
                }
            });
            $("#tblCHFilterResult tbody td").css({ "font-size": "12px" });
        },
        error: function (feedback) {
            $("#chFilterResultBody").css({ "background-image": "none" }).html("").append("No records found.").show();
        }
    });
}