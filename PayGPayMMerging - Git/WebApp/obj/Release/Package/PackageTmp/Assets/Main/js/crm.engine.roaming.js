﻿/* ===================================================== 
   *  TAB Roaming DETAILS 
   ----------------------------------------------------- */

/* ----------------------------------------------------------------
 * Function Name    : RoamingDetailsInfo
 * Purpose          : to show Roaming Details Information
 * Added by         : Anees Thomas
 * Create Date      : Oct 12th, 2018
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function RoamingDetailsInfo(mobileno, ICCID, Sitecode) {
    debugger;
    var url = apiServer + '/api/roaming';
    jQuery.support.cors = true;
    var JSONSendData = {
        ICCID: ICCID,
        MOBILENO: mobileno,
        SITECODE: Sitecode
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            var errcode = -1;
            if (feedback != '' && feedback != null) {
                errcode = feedback.errcode;
            }
            if (errcode == 0) {
                $("table#tblCustomerRoamingDetails td#tdCSDRateIncoming").html(feedback.rateIncoming);
                $("table#tblCustomerRoamingDetails td#tdCSDRateIncommingSMS").html(feedback.rate_incomming_sms);
                $("table#tblCustomerRoamingDetails td#tdCSDRateOutging").html(feedback.rateoutging);
                $("table#tblCustomerRoamingDetails td#tdCSDRateOutgoingSMS").html(feedback.rate_outgoing_sms);
                $("table#tblCustomerRoamingDetails td#tdCSDRateDataCharge").html(feedback.rate_data_charge);
                $("table#tblCustomerRoamingDetails td#tdCSDRoamingCountry").html(feedback.roaming_country);
               
            } else {
                $("table#tblCustomerRoamingDetails td#tdCSDRateIncoming").html("");
                $("table#tblCustomerRoamingDetails td#tdCSDRateIncommingSMS").html("");
                $("table#tblCustomerRoamingDetails td#tdCSDRateOutging").html("");
                $("table#tblCustomerRoamingDetails td#tdCSDRateOutgoingSMS").html("");
                $("table#tblCustomerRoamingDetails td#tdCSDRateDataCharge").html("");
                $("table#tblCustomerRoamingDetails td#tdCSDRoamingCountry").html("");
                
            }
        }
    });
    $("#TabRoamingsLoader").hide();
    $("#tblCustomerRoamingContainer").show();
}

/* ----------------------------------------------------- 
   *  eof TAB GENERAL 
   ===================================================== */


