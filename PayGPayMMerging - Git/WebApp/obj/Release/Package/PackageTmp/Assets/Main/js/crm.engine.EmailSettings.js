﻿//Rule Assigning
function BindRuleAssigning() {
    $("#ajax-screen-masking").show();
    var url = apiServer + '/api/EmailSettings';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 1
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            if (feedback != null && feedback.length > 0) {
                $('#hdruletemplateid').val(feedback[0].template_id);
                if (feedback[0].queue_type == 1) {
                    $('#rdRoundRobin').prop("checked", true);
                }
                else {
                    $('#rdMasterKick').prop("checked", true);
                    $('#divRuleLimit').show();
                    $('#ruleLimit').val(feedback[0].queue_per_user);
                }
                if (feedback[0].esclation_delay == 0) {
                    $('#rdEOff').prop("checked", true);
                    $('#escalationLimit').val(0);
                }
                else {
                    $('#rdEOn').prop("checked", true);
                    $('#divEscalationLimit').show();
                    $('#escalationLimit').val(feedback[0].esclation_delay);
                }
            }
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}

function UpdateRuleAssigning(queue_type, queue_per_user, processtype, template_id, esclation_delay) {
    $("#ajax-screen-masking").show();
    var url = apiServer + '/api/EmailSettings';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 2,
        queue_type: queue_type,
        queue_per_user: queue_per_user,
        processtype: processtype,
        template_id: template_id,
        esclation_delay: esclation_delay
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            if (feedback != null && feedback.length > 0) {
                alert(feedback[0].errmsg);
                if (feedback[0].errcode == 0) {
                    BindRuleAssigning();
                }
            }
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}

//SMS Assigning
function BindSMSAssigning() {
    $("#ajax-screen-masking").show();
    $('#ddlSMSTemplate').html('');
    $('#txtSMSDescription').val('');
    var url = apiServer + '/api/EmailSettings';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 3
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            if (feedback != null && feedback.length > 0) {
                var actionOption = '<option ctlValue="" value="-1" style=\"padding:3px\">-- Select --</option>';
                for (icount = 0; icount < feedback.length; icount++) {
                    actionOption += '<option ctlValue="' + feedback[icount].sms_text + '"   value="' + feedback[icount].template_id + '" style=\"padding:3px\">' + feedback[icount].template_name + '</option>';
                }
                $('#ddlSMSTemplate').append(actionOption);
            }
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}

function UpdateSMSAssigning(templateid, template_name, description, processtype) {
    $("#ajax-screen-masking").show();
    var url = apiServer + '/api/EmailSettings';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 4,
        status: 1,
        template_name: template_name,
        modetype: 4,
        sms_text: description,
        processtype: processtype,
        template_id: templateid
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            if (feedback != null && feedback.length > 0) {
                alert(feedback[0].errmsg);
                if (feedback[0].errcode == 0) {
                    $('#txtSMSDescription').val('');
                    $('#ddlSMSTemplate').val('-1');
                    BindSMSAssigning();
                }
            }
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}

function BindEmailAssigning() {
    $("#ajax-screen-masking").show();
    $('#ddlEmailTemplate').html('');
    $('#txtEmailSubject').val('');
    var url = apiServer + '/api/EmailSettings';
    //var url = apiServer + '/api/MailChimp';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 5
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            if (feedback != null && feedback.length > 0) {
                var actionOption = '<option ctlValue="" ctlValue2="" subValue="" value="-1" style=\"padding:3px\">-- Select --</option>';
                for (icount = 0; icount < feedback.length; icount++) {
                    actionOption += '<option style=\"padding:3px\"  ctlValue3="' + feedback[icount].email_blob + ' ctlValue="' + feedback[icount].filename + '" subValue=\"' + feedback[icount].email_subject + '"\ value="' + feedback[icount].template_id + '">' + feedback[icount].template_name + '</option>';
                }
                $('#ddlEmailTemplate').append(actionOption);
            }
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}

function BindMailChimpTemplate() {
    $("#ajax-screen-masking").show();
    $('#ddlMailChimpTemplate').html('');
    var url = apiServer + '/api/MailChimp';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 5
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            if (feedback != null && feedback.length > 0) {
                var actionOption = '<option value="-1" style=\"padding:3px\">-- Select --</option>';
                for (icount = 0; icount < feedback.length; icount++) {
                    actionOption += '<option style=\"padding:3px\" ctlvalue="' + feedback[icount].PublishName + '" value="' + feedback[icount].Name + '">' + feedback[icount].Name + '</option>';
                }
                $('#ddlMailChimpTemplate').append(actionOption);
            }
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}

function UpdateEmailAssigning(templateid, templatefile, subject, filename, processtype) {
    $("#ajax-screen-masking").show();
    var formData = new FormData();
    //formData.append("file", file);
    formData.append("id", templateid);
    formData.append("template_name", templatefile);
    formData.append("email_subject", subject);
    formData.append("filename", filename);
    formData.append("processtype", processtype);
    //formData.append("email_blob", email_blob);
    var url = apiServer + '/api/EmailSettingsUpload';
    jQuery.support.cors = true
    $.ajax
    ({
        url: url,
        type: 'post',
        data: formData, 
        contentType: false, // Not to set any content header    
        processData: false, // Not to process data 
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            if (feedback != null && feedback.length > 0) {
                alert(feedback[0].errmsg);
                if (feedback[0].errcode == 0) {
                    $('#divHTML').removeAttr('data');
                    $('#htmlFile').val("");
                    $('#ddlMailChimpTemplate').val("-1");
                    $('#ddlEmailTemplate').val("-1");
                    $('#txtEmailSubject').val('');
                    BindEmailAssigning();
                }
            }
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}

//Category
function BindTicketType(sitecode) {
    $("#ajax-screen-masking").show();
    //$('#ddlTicketType').html('');
    $('#ddlTicketType').find('option').remove();
    var url = apiServer + '/api/EmailGenerateTicket';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 1,
        sitecode: sitecode
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            if (feedback != null && feedback.length > 0) {
                var actionOption = '<option ctlValue="" value="-1" style=\"padding:3px\">-- Select --</option>';
                for (icount = 0; icount < feedback.length; icount++) {
                    actionOption += '<option ctlValue="' + feedback[icount].Issue_Id + '"   value="' + feedback[icount].Issue_Id + '" style=\"padding:3px\">' + feedback[icount].Issue_Name + '</option>';
                }
                $('#ddlTicketType').append(actionOption);
            }
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}

function FillCategory(sitecode, Issue_Id) {
    $("#ajax-screen-masking").show();
    $('#ddlCategory').find('option').remove();

    var url = apiServer + '/api/EmailGenerateTicket';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 2,
        sitecode: sitecode,
        Issue_Id: Issue_Id
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: true,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            var feedbackarr = jQuery.makeArray(JSON.stringify(feedback).split('}'));
            var icount;
            var actionOption = '<option value="-1" style=\"color:#c0c0c0\">--- Select ---</option>';
            for (icount = 0; icount < feedbackarr.length - 1; icount++) {
                actionOption += '<option ctlValue="' + feedback[icount].Function_Id + '"   value="' + feedback[icount].Function_Id + '" style=\"padding:3px\">' + feedback[icount].Function_Name + '</option>';
            }
            $('#ddlCategory').append(actionOption);
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}

function FillSubCategory(sitecode, Function_Id) {
    $("#ajax-screen-masking").show();
    $('#ddlSubCategory').find('option').remove();

    var url = apiServer + '/api/EmailGenerateTicket';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 4,
        sitecode: sitecode,
        Function_Id: Function_Id
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: true,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            var feedbackarr = jQuery.makeArray(JSON.stringify(feedback).split('}'));
            var icount;
            var actionOption = '<option value="-1" style=\"color:#c0c0c0\">--- Select ---</option>';
            for (icount = 0; icount < feedbackarr.length - 1; icount++) {
                actionOption += '<option ctlValue="' + feedback[icount].Complaint_Id + '"   value="' + feedback[icount].Complaint_Id + '" style=\"padding:3px\">' + feedback[icount].Complaint_Name + '</option>';
            }
            $('#ddlSubCategory').append(actionOption);
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}

function InsertTicketType(Issue_Code, Issue_Name, Issue_Desc, process_type, issue_type_id, sitecode) {
    $("#ajax-screen-masking").show();
    var url = apiServer + '/api/EmailSettings';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 11,
        Issue_Code: Issue_Code,
        Issue_Name: Issue_Name,
        Issue_Desc: Issue_Desc,
        process_type: process_type,
        issue_type_id: issue_type_id,
        sitecode: sitecode
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            if (feedback != null && feedback.length > 0) {
                alert(feedback[0].errmsg);
                if (feedback[0].errcode == 0) {
                    BindTicketType(feedback[0].sitecode);
                }
            }
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}

function InsertCategory(issue_id, function_code, Function_Name, process_type, issue_function_id, sitecode) {
    $("#ajax-screen-masking").show();
    var url = apiServer + '/api/EmailSettings';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 12,
        issue_id: issue_id,
        function_code: function_code,
        Function_Name: Function_Name,
        process_type: process_type,
        issue_function_id: issue_function_id,
        sitecode: sitecode
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            if (feedback != null && feedback.length > 0) {
                alert(feedback[0].errmsg);
                if (feedback[0].errcode == 0) {
                    FillCategory(feedback[0].sitecode, feedback[0].issue_id);
                }
            }
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}

function InsertSubCategory(function_Id, Complaint_Code, Complaint_Name, function_code, process_type, Complaint_Id, sitecode) {
    $("#ajax-screen-masking").show();
    var url = apiServer + '/api/EmailSettings';
    jQuery.support.cors = true;
    var JSONSendData = {
        modetype: 13,
        function_Id: function_Id,
        Complaint_Code: Complaint_Code,
        Complaint_Name: Complaint_Name,
        function_code: function_code,
        process_type: process_type,
        Complaint_Id: Complaint_Id,
        sitecode: sitecode
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            if (feedback != null && feedback.length > 0) {
                alert(feedback[0].errmsg);
                if (feedback[0].errcode == 0) {
                    FillSubCategory(feedback[0].sitecode, feedback[0].function_Id);
                }
            }
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}