﻿/* ===================================================== 
   *  TAB GENERAL 
   ----------------------------------------------------- */

/* ----------------------------------------------------------------
 * Function Name    : Ovrview
 * Purpose          : to show general customer Information
 * Added by         : Moorthy
 * Create Date      : November 19th, 2018
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function GetOverview(productCode, searchText, Sitecode) {
    var SIM_Type = 'PAYG';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1) {
        SIM_Type = 'PAYM';
    }

    var url = apiServer + '/api/customeroverview';

    jQuery.support.cors = true;
    var JSONSendData = {
        Product_Code: productCode,
        MobileNo: searchText,
        Sitecode: Sitecode,
    };

    $("table#tblCustomerOverview td#tdCOLastlocationupdate").html("");
    $("table#tblCustomerOverview td#tdCOLastGPRSlocationupdate").html("");
    $("table#tblCustomerOverview td#tdCOLasttimelocationupdatesent").html("");
    $("table#tblCustomerOverview td#tdCOLastcall").html("");
    $("table#tblCustomerOverview td#tdCOLastSMS").html("");
    $("table#tblCustomerOverview td#tdCOLastbrowsing").html("");
    $("table#tblCustomerOverview td#tdCOLastTopup").html("");
    $("table#tblCustomerOverview td#tdCOCurrentBalance").html("");
    $("table#tblCustomerOverview td#tdCOBonusBalance").html("");
    $("table#tblCustomerOverview td#tdCOCurrentBundles").html("");
    $("table#tblCustomerOverview td#tdCOBundleAllowance").html("");
    $("table#tblCustomerOverview td#tdCOBundleUsage").html("");
    $("table#tblCustomerOverview td#tdCOHomeRoaming").html("");
    $("table#tblCustomerOverview td#tdCORoamingcountry").html("");
    $("table#tblCustomerOverview td#tdCOOutgoingcalls").html("");
    $("table#tblCustomerOverview td#tdCOOutgoingSMS").html("");
    $("table#tblCustomerOverview td#tdCODatabrowsing").html("");

    $("table#tblCustomerOverview td#tdCOreseller_name").html("");
    $("table#tblCustomerOverview td#tdCOportin_date").html("");
    $("table#tblCustomerOverview td#tdCOportin_operator").html("");
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#TabOverviewLoader").hide();
            $("#tblCustomerOverviewContainer").show();
            var errcode = -1;
            if (feedback != '' && feedback != null) {
                errcode = feedback.errcode;
            }

            if (feedback.last_update != null && feedback.last_update_str != "01-01-0001 00:00:00")
                $("table#tblCustomerOverview td#tdCOLastlocationupdate").html(capitalizeString(feedback.last_update_str));
            else
                $("table#tblCustomerOverview td#tdCOLastlocationupdate").html("");

            if (feedback.gprs_loc_update != null && feedback.gprs_loc_update_str != "01-01-0001 00:00:00")
                $("table#tblCustomerOverview td#tdCOLastGPRSlocationupdate").html(capitalizeString(feedback.gprs_loc_update_str));
            else
                $("table#tblCustomerOverview td#tdCOLastGPRSlocationupdate").html("");

            //TODO : Need to check
            //if (feedback.last_topup != null && feedback.last_topup != "01-01-0001 00:00:00")
            //    $("table#tblCustomerOverview td#tdCOLasttimelocationupdatesent").html(capitalizeString(feedback.last_topup_str));
            //else
            //    $("table#tblCustomerOverview td#tdCOLasttimelocationupdatesent").html("");

            if (feedback.last_call_date != null && feedback.last_call_date_str != "01-01-0001 00:00:00")
                $("table#tblCustomerOverview td#tdCOLastcall").html(capitalizeString(feedback.last_call_date_str));
            else
                $("table#tblCustomerOverview td#tdCOLastcall").html("");

            if (feedback.last_sms_date != null && feedback.last_sms_date_str != "01-01-0001 00:00:00")
                $("table#tblCustomerOverview td#tdCOLastSMS").html(capitalizeString(feedback.last_sms_date_str));
            else
                $("table#tblCustomerOverview td#tdCOLastSMS").html("");

            if (feedback.last_gprs_date != null && feedback.last_gprs_date_str != "01-01-0001 00:00:00")
                $("table#tblCustomerOverview td#tdCOLastbrowsing").html(capitalizeString(feedback.last_gprs_date_str));
            else
                $("table#tblCustomerOverview td#tdCOLastbrowsing").html("");

            if (feedback.last_topup != null && feedback.last_topup_str != "01-01-0001 00:00:00")
                $("table#tblCustomerOverview td#tdCOLastTopup").html(capitalizeString(feedback.last_topup_str));
            else
                $("table#tblCustomerOverview td#tdCOLastTopup").html("");

            if (feedback.balance != null)
                $("table#tblCustomerOverview td#tdCOCurrentBalance").html(feedback.balance);
            else
                $("table#tblCustomerOverview td#tdCOCurrentBalance").html("");

            if (feedback.bonus_balance != null)
                $("table#tblCustomerOverview td#tdCOBonusBalance").html(feedback.bonus_balance);
            else
                $("table#tblCustomerOverview td#tdCOBonusBalance").html("");

            if (feedback.bundle_name != null)
                $("table#tblCustomerOverview td#tdCOCurrentBundles").html(capitalizeString(feedback.bundle_name));
            else
                $("table#tblCustomerOverview td#tdCOCurrentBundles").html("");

            if (feedback.bundle_allowance != null)
                $("table#tblCustomerOverview td#tdCOBundleAllowance").html(feedback.bundle_allowance);
            else
                $("table#tblCustomerOverview td#tdCOBundleAllowance").html("");

            if (feedback.bundle_remain_allowance != null)
                $("table#tblCustomerOverview td#tdCOBundleUsage").html(feedback.bundle_remain_allowance);
            else
                $("table#tblCustomerOverview td#tdCOBundleUsage").html("");

            if (feedback.roaming_country != null && feedback.roaming_country != "") {
                $("table#tblCustomerOverview td#tdCOHomeRoaming").html("YES");

                $("table#tblCustomerOverview tr.trRoaming").show();
                if (feedback.roaming_country != null)
                    $("table#tblCustomerOverview td#tdCORoamingcountry").html(capitalizeString(feedback.roaming_country));
                else
                    $("table#tblCustomerOverview td#tdCORoamingcountry").html("");

                if (feedback.roaming_outcall != null)
                    $("table#tblCustomerOverview td#tdCOOutgoingcalls").html(feedback.roaming_outcall);
                else
                    $("table#tblCustomerOverview td#tdCOOutgoingcalls").html("");

                if (feedback.roaming_outsms != null)
                    $("table#tblCustomerOverview td#tdCOOutgoingSMS").html(feedback.roaming_outsms);
                else
                    $("table#tblCustomerOverview td#tdCOOutgoingSMS").html("");

                if (feedback.roaming_outdata != null)
                    $("table#tblCustomerOverview td#tdCODatabrowsing").html(feedback.roaming_outdata);
                else
                    $("table#tblCustomerOverview td#tdCODatabrowsing").html("");
            }
            else {
             
                $("table#tblCustomerOverview td#tdCOHomeRoaming").html("NO");
                $("table#tblCustomerOverview tr.trRoaming").hide();
            }

            if (feedback.reseller_name != null)
                $("table#tblCustomerOverview td#tdCOreseller_name").html(feedback.reseller_name);
            else
                $("table#tblCustomerOverview td#tdCOreseller_name").html("");

            if (feedback.portin_date != null)
                $("table#tblCustomerOverview td#tdCOportin_date").html(feedback.portin_date);
            else
                $("table#tblCustomerOverview td#tdCOportin_date").html("");

            if (feedback.portin_operator != null)
                $("table#tblCustomerOverview td#tdCOportin_operator").html(feedback.portin_operator);
            else
                $("table#tblCustomerOverview td#tdCOportin_operator").html("");
            
        }
    });
}