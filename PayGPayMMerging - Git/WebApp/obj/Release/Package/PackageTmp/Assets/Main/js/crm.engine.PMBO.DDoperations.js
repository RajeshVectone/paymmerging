﻿//Page load
$(document).ready(function () {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) { dd = '0' + dd } if (mm < 10) { mm = '0' + mm } today = dd + '/' + mm + '/' + yyyy;
    var lastdate1 = $('#lastdate1').val();
    if (lastdate1 == '') {
        $('#dp1').attr('data-date', today);
        $('#dp1').datepicker().on('changeDate', function (e) {
            $('div.datepicker').hide();
        });
        //$('#startdate').attr('value', today);
        //$('#lastdate1').val(today);
    }
    else {
        $('#dp1').attr('data-date', lastdate1);
        $('#dp1').datepicker().on('changeDate', function (e) {
            $('div.datepicker').hide();
        });
        // $('#startdate').attr('value', lastdate1);
    }
    var lastdate2 = $('#lastdate2').val();
    if (lastdate2 == '') {
        $('#dp2').attr('data-date', today);
        $('#dp2').datepicker().on('changeDate', function (e) {
            $('div.datepicker').hide();
        });
        // $('#enddate').attr('value', today);
        // $('#lastdate2').val(today);
    }
    else {
        $('#dp2').attr('data-date', lastdate2);
        $('#dp2').datepicker().on('changeDate', function (e) {
            $('div.datepicker').hide();
        });
        //$('#enddate').attr('value', lastdate2);
    }

});

//Number
function Number(txtValues) {
    var intRegex = /^\d+$/;
    if (intRegex.test(txtValues.val())) { }
    else { txtValues.val(''); }
}

//Date Time Format
function DateTimeValues(txtvalues) {
    var intRegex = /^[0-9 /]+$/;
    if (intRegex.test(txtvalues)) { }
    else { txtvalues.val(''); }
}

//Bank process
function SortCode(txtValues) {
    var intRegex = /^[0-9 -]+$/;
    if (intRegex.test(txtValues.val())) { }
    else { txtValues.val(''); }
}

//AlphaNumber
function alphaNumber(txtvalues) {
    var intRegex = /^[a-zA-Z0-9 -]+$/;
    if (intRegex.test(txtvalues.val())) { }
    else { txtvalues.val(''); }
}

//Check the All Check box
function chekedValues() {
    var checked = $('#chkSelectAll').prop('checked');
    if (checked) { $('.value').prop('checked', true); }
    else { $('.value').prop('checked', false); }
}

//DD Operation Grid View
function ViewDDStepUpProcess(DDoperation, FromDate, ToDate, CCTranID, SubsID, SortCode, AccNo, SiteCode) {
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    var row = 0;
    $("#tblDDOperationContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblDDOperation\"></table>");
    var url = apiServer + '/api/PMBODDOperations';
    jQuery.support.cors = true;
    var JSONSendData = {
        DDOperations: DDoperation,
        FromDate: FromDate,
        ToDate: ToDate,
        CCTransactionID: CCTranID,
        SubscriberID: SubsID,
        SortCode: SortCode,
        AccountNumber: AccNo,
        SiteCode:SiteCode
    };
    $.ajax({
        url: url,
        type: 'get',
        data: JSONSendData,
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#tblDDOperationContainer").hide();
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            $("#tblDDOperationContainer").show();
            $('#tblDDOperation').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                iDisplayLength: 50,
                bPaginate: false,
                bFilter: false,
                bInfo: false,
                aaSorting: [[1, "asc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No call PAGM found."
                },
                aoColumns: [
                    {
                        sTitle: "<input id='chkSelectAll' class='checkAll' type='checkbox' onclick='chekedValues();'></input>", mDataProp: "registerdate", "bSortable": false,
                        fnRender: function (ob) {
                            row = row + 1;
                            return "<input type='checkbox' class='value' id='chkChild_" + row + "' ></input>";
                        }
                    },
                    { mDataProp: "subscriberid", sTitle: "Subs. ID" },
                    { mDataProp: "pp_customer_id", sTitle: "Cust ID" },
                    {
                        mDataProp: "lastupdate", sTitle: "Subs. date",
                        fnRender: function (ob) {
                            // return convertDate01(ob.aData.lastupdate);
                            return ob.aData.lastupdate;
                        }
                    },
                    { mDataProp: "paymentref", sTitle: "CC trans ID" },
                    { mDataProp: "orderselected", sTitle: "Plan " },
                    { mDataProp: "sort_code", sTitle: "Sort Code" },
                    { mDataProp: "account_number", sTitle: "Acc. number" },
                    { mDataProp: "accountname", sTitle: "Acc. name" },
                    { mDataProp: "dd_action", sTitle: "Status" }
                ],
                fnDrawCallback: function () {
                    if (row <= 0) {
                        $("#tblDDOperation tbody").css({ "font-size": "12px", 'height': '0px', 'overflow': 'inherit', 'position': 'static' });
                    }
                    else {
                        $("#tblDDOperation tbody td").css({ "font-size": "12px" });
                    }
                }
            });
        },
        error: function () {
            $("#ajax-screen-masking").hide();
        }
    });
    row = 0;
}

//DDNew Download Process.
//function DownloadDDNew(count, subId, commands) {
function PayAwayDD(count, subId, commands, CustId_AccNo_SortCode) {
    var url = '/Download/Create_PayAway_DD_New_Files';
    jQuery.support.cors = true;
    var JSONSendData = {
        count: count,
        subId: subId,
        commands: commands
    };

    $.ajax({
        url: url,
        type: 'Post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {

            $('#filesName').html("File Is Generated sucessfully, File Name : " + feedback.Text + "\r\n" + UpdateSetupInProgress(CustId_AccNo_SortCode));
                $('#FileGenrated').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
        }
    });
}


//DDNew Download Process.
//function DownloadDDNew(count, subId, commands) {
function DownloadDDNew(count, subId, commands, CustId_AccNo_SortCode) {
    var url = '/Download/Create_PayAway_DD_New_Files';
    jQuery.support.cors = true;
    var JSONSendData = {
        count: count,
        subId: subId,
        commands: commands
    };

    $.ajax({
        url: url,
        type: 'get',
        data: JSONSendData,
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {

            $('#filesName').html("File Is Generated sucessfully, File Name : " + feedback.Text + "\r\n" + MoveToSetupInProgress(CustId_AccNo_SortCode));
            $('#FileGenrated').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });
}

//DDcollection Download Process.
function DownloadDDcollection(commands) {
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    var url = '/Download/Create_PayAway_DD_Collection_Files';
    jQuery.support.cors = true;
    var JSONSendData = {
        commands: commands
    };
    $.ajax({
        url: url,
        type: 'get',
        data: JSONSendData,
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {
                $("#ajax-screen-masking").hide();
                $('#filesName').html("File Is Generated sucessfully, File Name : " + feedback.Text);
                $('#FileGenrated').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("File failed to generated due to Error " + xhr.status);
            $("#ajax-screen-masking").hide();
        }
    });

    return true;
}

//Modified by BSK for Improvement : Jira CRMT-141
function UpdateSetupInProgress(CustId_AccNo_SortCode) {
    var retString = '';
    var url = apiServer + '/api/movenewtosetupinprogress';
    jQuery.support.cors = true;
    var JSONSendData = {
        CustId_AccNo_SortCode: CustId_AccNo_SortCode
    };
    $.ajax({
        url: url,
        type: 'Post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        //data: JSONSendData,
        success: function (feedback) {
            retString = "Customers Movement to Setup IN Progress Result : " + feedback;
        }
    });

    return retString;
}

//MoveToSetupInProgress(CustId_AccNo_SortCode);
function MoveToSetupInProgress(CustId_AccNo_SortCode) {
    //var ajaxSpinnerTop = $(window).outerHeight() / 2;
    //$("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    //$("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    var retString = ' ';
    var url = apiServer + '/api/movenewtosetupinprogress';
    jQuery.support.cors = true;
    var JSONSendData = {
        CustId_AccNo_SortCode: CustId_AccNo_SortCode
    };
    $.ajax({
        url: url,
        type: 'get',
        data: JSONSendData,
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            //$("#ajax-screen-masking").show();
        },
        success: function (feedback) {
            //$("#ajax-screen-masking").hide();
            //$('#SetupInProgressMsg').html("Customers Movement to Setup IN Progress Result : " + feedback);
            //$('#SetupInProgressTitle').modal({ keyboard: false, backdrop: 'static' }).css({
            //    'margin-left': function () {
            //        return window.pageXOffset - ($(this).width() / 2);
            //    }
            //});

            retString = "Customers Movement to Setup IN Progress Result : " + feedback;
        }
    });

    return retString;
}

//Clear text & Grid Values
function Clear() {
    $('#startdate').val('');
    $('#enddate').val('');
    $('#txtCCTransactionID').val('');
    $('#txtSubscriberID').val('');
    $('#txtSortCodetxt').val('');
    $('#AccountNumber').val('');
    $('#tblDDOperation').html('');
    $('#txtCommands').val('');
    $('#txtSortCode').val('');
    $('#txtAccountNumber').val('');
    
}

$('#btnODBack').click(function () {
    var DDoperation = $.trim($('#ddlDDoperation').val());
    var fromdate = $.trim($('#startdate').val());
    var todate = $.trim($('#enddate').val());
    var tranID = $.trim($('#txtCCTransactionID').val());
    var subID = $.trim($('#txtSubscriberID').val());
    var sorcode = $.trim($('#txtSortCode').val());
    var accountNo = $.trim($('#txtAccountNumber').val());
    var DDoperation = $.trim($('#ddlDDoperation').val());
    var SiteCode = $("input[name=gProductCode]").val();
    if (DDoperation == '1') {
        
        ViewDDStepUpProcess(DDoperation, fromdate, todate, tranID, subID, sorcode, accountNo, SiteCode);
    }
    Clear();
});

$('#btnODBacks').click(function () {
    var DDoperation = $.trim($('#ddlDDoperation').val());
    var fromdate = $.trim($('#startdate').val());
    var todate = $.trim($('#enddate').val());
    var tranID = $.trim($('#txtCCTransactionID').val());
    var subID = $.trim($('#txtSubscriberID').val());
    var sorcode = $.trim($('#txtSortCode').val());
    var accountNo = $.trim($('#txtAccountNumber').val());
    var DDoperation = $.trim($('#ddlDDoperation').val());
    var SiteCode = $("input[name=gProductCode]").val();
    if (DDoperation == '1') {
        ViewDDStepUpProcess(DDoperation, fromdate, todate, tranID, subID, sorcode, accountNo, SiteCode);
    }
});

$('#btnClear').click(function () {
    Clear();
});


