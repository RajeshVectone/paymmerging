﻿function GetBundleGroup(sitecode, product_code) {
    var url = apiServer + '/api/FreeDataBundle';
    jQuery.support.cors = true;
    var JSONSendData = {
        Type: 1,
        sitecode: sitecode,
        product_code: product_code
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            if (feedback != null && feedback.length > 0) {
                var actionOption = '<option ctlValue="" value="-1" style=\"padding:3px\">-- Select --</option>';
                for (icount = 0; icount < feedback.length; icount++) {
                    actionOption += '<option value="' + feedback[icount].category_id + '" style=\"padding:3px\">' + feedback[icount].category_name + '</option>';
                }
                $('#ddlCategory').append(actionOption);
            }

        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}
function GetBundleListInfo(sitecode, product_code, category_id) {
    $("#tblFreeDataBundleContainerBody").html("").append("<table class=\"table table-bordered\" id=\"tblFreeDataBundle\"></table>");
    var url = apiServer + '/api/FreeDataBundle';
    jQuery.support.cors = true;
    var JSONSendData = {
        Type: 2,
        sitecode: sitecode,
        product_code: product_code,
        category_id: category_id
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            $('#tblFreeDataBundle').dataTable({
                aaData: feedback,
                bDestroy: true,
                bRetrieve: true,
                iDisplayLength: 10,
                aaSorting: [],
                aoColumns: [
                        { mDataProp: "bundleid", sTitle: "Bundle ID" },
                        { mDataProp: "bundle_name", sTitle: "Bundle Name" },
                        { mDataProp: "bundle_price", sTitle: "Bundle Price" },
                        { mDataProp: "vec_ussd_prefix", sTitle: "USSD Prefix" },
                        { mDataProp: "bundle_call", sTitle: "Call" },
                        { mDataProp: "bundle_sms", sTitle: "SMS" },
                        { mDataProp: "bundle_data", sTitle: "Data" },
                        { mDataProp: "days_valid", sTitle: "Valid Days" },
                        {
                            mDataProp: "ACTION", sTitle: "Action", sWidth: "150px",
                            fnRender: function (ob) {
                                var actionURL = "<button ctlValue1=\"" + ob.aData.bundleid + "\" ctlValue2=\"" + ob.aData.bundle_name
                                        + "\" ctlValue3=\"" + ob.aData.bundle_price + "\" ctlValue4=\"" + ob.aData.bundle_type_name + "\"  class=\"btn blue btnSubscribe\" style=\"width:100px\">Subscribe</button>";
                                return actionURL;
                            }
                        }
                ],
                fnDrawCallback: function () {
                    $("#tblFreeDataBundleContainerBody tbody td").css({ "font-size": "12px" });
                }
            });

        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
            $("#tblFreeDataBundleContainerBody").html("").append("No records found").show();
        }
    });
}

function BundleSubscribeFree(sitecode, mobileno, bundleid, subscribed_user, bundle_price, bundle_category, bundle_name) {
    var url = apiServer + "/api/FreeDataBundle";
    var JSONSendData = {
        Type: 3,
        sitecode: sitecode,
        userinfo: mobileno,
        bundleid: bundleid,
        subscribed_user: subscribed_user,
        bundle_price: bundle_price,
        bundle_category: bundle_category,
        bundle_name: bundle_name
    };
    $.ajax
    ({
        url: url,
        type: "post",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {
            //var stat = feedback[0].errcode;
            $("#ajax-screen-masking").hide();
            if (feedback[0].errcode == "0")
                alert('Subscribed successfully!');
            else
                alert('Subscribed failed!');
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
            alert("Error");
        }
    });
}