﻿function Blocklog(mobileno) {
    $("#tblDDOperationContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblDDOperation\"></table>");
    $("#tblDDOperationContainer").css({ "min-height": "100px", "background-repeat": "no-repeat", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var row = 0;
    var url = apiServer + "/api/BUB";
    var JSONSendData = {
        mobileno: mobileno
    };
    $.ajax
    ({
        url: url,
        type: "GET",
        data: JSONSendData,
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('#tblDDOperation').html('');
            $("#tblDDOperationContainer").css({ "background-image": "none" });
            $('#tblDDOperation').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: true,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [[20, "desc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Data Available."
                },
                aoColumns: [
                    { mDataProp: "UPDATE_DATE", sTitle: 'UPDATE DATE' },
                    { mDataProp: "CURRENT_STATUS", sTitle: 'CURRENT STATUS' },
                    { mDataProp: "USER", sTitle: 'USER' },
                    { mDataProp: "REASON", sTitle: 'REASON' },


                ],
                fnDrawCallback: function () {
                    $("#tblPaymentProfile tbody td").css({ "font-size": "12px" });
                }
            });
            $("#tblDDOperation tbody td").css({ "font-size": "12px" });
        },
        error: function (feedback) {
            $("#tblDDOperationContainer").css({ "background-image": "none" }).html("").append("No Payment Profile PAYM found.").show();
        }
    });
}


function Block(mobileno,name,reason) {
    $("#tblDDOperationContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblDDOperation\"></table>");
    $("#tblDDOperationContainer").css({ "min-height": "100px", "background-repeat": "no-repeat", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var row = 0;
    var url = apiServer + "/api/BUBL";
    var JSONSendData = {
        mobileno: mobileno,
        user: name,
        reason: reason,
        status:'0'
    };
    $.ajax
    ({
        url: url,
        type: "GET",
        data: JSONSendData,
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('#tblDDOperation').html('');
            $("#tblDDOperationContainer").css({ "background-image": "none" });
            $('#tblDDOperation').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: true,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [[20, "desc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Data Available."
                },
                aoColumns: [                   
                     window.location.href = path

                ],
                fnDrawCallback: function () {
                    window.location.href = path;
                    $("#tblPaymentProfile tbody td").css({ "font-size": "12px" });
                }
            });
            $("#tblDDOperation tbody td").css({ "font-size": "12px" });
        },
        error: function (feedback) {
            $("#tblDDOperationContainer").css({ "background-image": "none" }).html("").append("No Data's Available.").show();
        }
    });
}




function unBlock(mobileno, name, reason) {
    $("#tblDDOperationContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblDDOperation\"></table>");
    $("#tblDDOperationContainer").css({ "min-height": "100px", "background-repeat": "no-repeat", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var row = 0;
    var url = apiServer + "/api/BUBL";
    var JSONSendData = {
        mobileno: mobileno,
        user: name,
        reason: reason,
        status: '1'
    };
    $.ajax
    ({
        url: url,
        type: "GET",
        data: JSONSendData,
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('#tblDDOperation').html('');
            $("#tblDDOperationContainer").css({ "background-image": "none" });
            $('#tblDDOperation').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: true,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [[20, "desc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Data Available."
                },
                aoColumns: [
                     window.location.href = path

                ],

                fnDrawCallback: function () {
                   
                    $("#tblPaymentProfile tbody td").css({ "font-size": "12px" });
                }
            });
            $("#tblDDOperation tbody td").css({ "font-size": "12px" });
        },
        error: function (feedback) {
            $("#tblDDOperationContainer").css({ "background-image": "none" }).html("").append("No Data's Available.").show();
        }
    });
}