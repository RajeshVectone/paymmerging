﻿/* ===================================================== 
   *  TAB BUNDLES 
   ----------------------------------------------------- */

/* ----------------------------------------------------------------
 * Function Name    : Country List
 * Purpose          : to show Country List
 * Added by         : karthik
 * Create Date      : March 14th, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function Countrylist() {
    var url = apiServer + '/api/HappyBundleDaillingNumber/';
    jQuery.support.cors = true;
    $.ajax
    ({
        url: url,
        type: 'get',     
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {

            $('#CountryList').text('');
            $("select[name=CountryList]").html("").append("<option value=\"-1\" selected>Select Country</option>");

            $.each(feedback, function (key, value) {                            
                $('#CountryList').append('<option value="' + value.country_Id + '">' + value.country_name + '</option>');
            });
        },
        error: function (feedback) {
            $("#pbBundleDaillingNumberListBody").css({ "background-image": "none" }).html("").append("No Country list  found").show();
        }
    });
}


/* ----------------------------------------------------------------
 * Function Name    : HappyBundleDaillingCountryListdata
 * Purpose          : to show HappyBundleDaillingCountryListdata  
 * Added by         : karthik
 * Create Date      : March 14th, 2015
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function CreateDailliNumber(Msisdn, Sitecode, Dailnumber) {

  

    var url = apiServer + '/api/HappyBundleDaillingNumber/';
    jQuery.support.cors = true;
    var JSONSendData = {
        Msisdn: Msisdn,
        Bundle_Type: 2,
        Sitecode: Sitecode,
        Dailnumber: Dailnumber
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            var stat = feedback[0].errcode;
            if (stat == 0) {
                $('.PMSuccess .modal-header').html(feedback[0].errsubject);
                $('.PMSuccess .modal-body').html(feedback[0].errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);                      
                    }
                });
            } else {
                $('.PMFailed .modal-header').html(feedback[0].errsubject);
                $('.PMFailed .modal-body').html(feedback[0].errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);                     
                    }
                });
            }
        }
        //error: function (feedback) {
        //    $("#pbBundleDaillingNumberListBody").css({ "background-image": "none" }).html("").append("No Bundel dailling number found").show();
        //}
    });
}


/* ----------------------------------------------------------------
 * Function Name    : HappyBundleDaillingCountryListdata
 * Purpose          : to show HappyBundleDaillingCountryListdata  
 * Added by         : karthik
 * Create Date      : March 14th, 2015
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function HappyBundleDaillingCountryListdata(Msisdn, Sitecode) {
    
    $("#pbBundleDaillingNumberListBody").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblBundleDaillingNumber\"></table>");
    $("#pbBundleDaillingNumberListBody").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });

    var url = apiServer + '/api/HappyBundleDaillingNumber/';
    jQuery.support.cors = true;
    var JSONSendData = {
         Msisdn: Msisdn,        
         Bundle_Type: 1,
         Sitecode: Sitecode       
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#pbBundleDaillingNumberListBody").css({ "background-image": "none" });
            $('#tblBundleDaillingNumber').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: false,
                bInfo: false,
                aaSorting: [[0, "desc"]],
                aoColumns: [
                    { mDataProp: "startdate_string", sTitle: "Date", sType: "date-euro" },
                    { mDataProp: "destinationNB", sTitle: "Destination Number" },
                    { mDataProp: "Mapping_number", sTitle: "Dialing Number" },
                    { mDataProp: "Mode", sTitle: "Mode Of Addition" }
                ],
            });

            $("#tblBundleDaillingNumber tbody td").css({ "font-size": "12px" });
            $("#tblBundleDaillingNumber_wrapper .row-fluid:eq(0)").remove();

            $("#tblBundleDaillingNumber_wrapper .row-fluid .span6:eq(0)").addClass("span4");
            $("#tblBundleDaillingNumber_wrapper .row-fluid .span6:eq(1)").addClass("span8");

            var DownloadAnchor = "<a id=\"downloadAnchor\" style=\"display:none;\"></a>";
            var btnExportHistory = "<button class=\"btn blue\" id=\"btnExportHappyBundleList\" name=\"btnExportHappyBundleList\">Download to Excel</button>";
            $("#tblBundleDaillingNumber_wrapper .row-fluid .span4").append(DownloadAnchor + btnExportHistory).show();
            ChekingHappybundledaillingnumber(Msisdn, Sitecode)
        },
        error: function (feedback) {
            $("#pbBundleDaillingNumberListBody").css({ "background-image": "none" }).html("").append("No Bundel dailling number found").show();
        }
    });
}

function ChekingHappybundledaillingnumber(Msisdn, Sitecode) {
    var url = apiServer + '/api/HappyBundleDaillingNumber/';
    jQuery.support.cors = true;
    var JSONSendData = {
        Msisdn: Msisdn,
        Bundle_Type: 3,
        Sitecode: Sitecode
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            var errcode = feedback[0].errcode;
            if (errcode == -1) {               
                   $("#btnCreateDaillingnumber").attr("disabled", "disabled",true);
                   $("#lblerrMessage").show();              
            }
        }
        
    });
}
/* ----------------------------------------------------- 
   *  eof TAB BUNDLES 
   ===================================================== */


