﻿/* ===================================================== 
   *  SIM ADDRESS LABELLING 
   ----------------------------------------------------- */

function FindOrder(OrderID, Sitecode) {
    $("button#AL_btnFind").hide();
    $("img#AL_ajaxloading").show();

    $("table#FormAddressLabelling tr.trAfterFound").remove();

    if (OrderID != "") {
        var regexAllNumber = /^\d+$/;
        if (regexAllNumber.test(OrderID) == true) {
            var url = apiServer + '/api/SIMOrderSearch/3?val=' + OrderID + "&Sitecode=" + Sitecode;
            $.getJSON(url, function (feedback) {
                if (feedback.errcode == undefined && feedback != "") {
                    for (var idx in feedback) {
                        var newRowData = "";
                        newRowData += "<tr class=\"trAfterFound\">";
                        newRowData += "<td>ICCID&nbsp;" + (parseInt(idx) + 1) + "</td>";
                        newRowData += "<td><input type=\"text\" class=\"AL_ICCID_List\" data-simid=\"" + feedback[idx].freesimid + "\" data-mimsiid=\"" + feedback[idx].mimsi_order_id + "\" id=\"AL_ICCID" + idx + "\" name=\"AL_ICCID" + idx + "\" disabled=\"disabled\" /></td>";
                        newRowData += "<td>&nbsp;</td>";
                        newRowData += "</tr>";
                        $("table#FormAddressLabelling").append(newRowData);
                        $("input[name=AL_ICCID0]").removeAttr("disabled").focus();
                    }
                    var btnProcess = "";
                    btnProcess += "<tr class=\"trAfterFound\">";
                    btnProcess += "<td>&nbsp;</td>";
                    btnProcess += "<td>";
                    btnProcess += "<div id=\"AL_btnProceedLoading\" style=\"display:none\"><img src=\"/Assets/Main/img/ajaxloader-03.gif\" />&emsp;Dispatching in progress...</div>";
                    btnProcess += "<button class=\"btn blue\" id=\"AL_btnProceed\">Proceed</button>";
                    btnProcess += "</td>";
                    btnProcess += "<td>&nbsp;</td>";
                    btnProcess += "</tr>";
                    $("table#FormAddressLabelling").append(btnProcess);
                } else {
                    var newRowData = "";
                    newRowData += "<tr class=\"trAfterFound\">";
                    newRowData += "<td>&nbsp;</td>";
                    newRowData += "<td><span class=\"alert-error\">Order not found</span></td>";
                    newRowData += "<td>&nbsp;</td>";
                    newRowData += "</tr>";
                    $("table#FormAddressLabelling").append(newRowData);
                }
            });
        } else {
            var regex2in1 = /^[M][A-Z]{2}\d+$/;
            if (regex2in1.test(OrderID) == true) {

                var url = apiServer + '/api/simordersearch/';
                jQuery.support.cors = true;
                var JSONSendData = {
                    sitecode: Sitecode,
                    searchby: "freesimid",
                    searchvalue: OrderID
                };
                $.ajax
                ({
                    url: url,
                    type: 'post',
                    data: JSON.stringify(JSONSendData),
                    dataType: 'json',
                    contentType: "application/json;charset=utf-8",
                    cache: false,
                    success: function (feedback) {
                        if (feedback.errcode == undefined && feedback != "") {
                            var newRowData = "";
                            newRowData += "<tr class=\"trAfterFound\">";
                            newRowData += "<td>ICCID</td>";
                            newRowData += "<td><input type=\"text\" class=\"AL_ICCID_List\" data-simid=\"" + feedback[0].freesimid + "\" data-mimsiid=\"" + feedback[0].mimsi_order_id + "\" id=\"AL_ICCID\" name=\"AL_ICCID\" /></td>";
                            newRowData += "<td>&nbsp;</td>";
                            newRowData += "</tr>";
                            $("table#FormAddressLabelling").append(newRowData);

                            var btnProcess = "";
                            btnProcess += "<tr class=\"trAfterFound\">";
                            btnProcess += "<td>&nbsp;</td>";
                            btnProcess += "<td>";
                            btnProcess += "<div id=\"AL_btnProceedLoading\" style=\"display:none\"><img src=\"/Assets/Main/img/ajaxloader-03.gif\" />&emsp;Dispatching in progress...</div>";
                            btnProcess += "<button class=\"btn blue\" id=\"AL_btnProceed\">Proceed</button>";
                            btnProcess += "</td>";
                            btnProcess += "<td>&nbsp;</td>";
                            btnProcess += "</tr>";
                            $("table#FormAddressLabelling").append(btnProcess);

                            $("input[name=AL_ICCID]").focus();
                        } else {
                            var newRowData = "";
                            newRowData += "<tr class=\"trAfterFound\">";
                            newRowData += "<td>&nbsp;</td>";
                            newRowData += "<td><span class=\"alert-error\">Order not found</span></td>";
                            newRowData += "<td>&nbsp;</td>";
                            newRowData += "</tr>";
                            $("table#FormAddressLabelling").append(newRowData);
                        }
                    }
                });
            } else {
                var newRowData = "";
                newRowData += "<tr class=\"trAfterFound\">";
                newRowData += "<td>&nbsp;</td>";
                newRowData += "<td><span class=\"alert-error\">Can\'t recognize the Order ID.</span></td>";
                newRowData += "<td>&nbsp;</td>";
                newRowData += "</tr>";
                $("table#FormAddressLabelling").append(newRowData);
            }
        }
    } else {
        var newRowData = "";
        newRowData += "<tr class=\"trAfterFound\">";
        newRowData += "<td>&nbsp;</td>";
        newRowData += "<td><span class=\"alert-error\">Order ID can not be blank.</span></td>";
        newRowData += "<td>&nbsp;</td>";
        newRowData += "</tr>";
        $("table#FormAddressLabelling").append(newRowData);
    }
    $("img#AL_ajaxloading").hide();
    $("button#AL_btnFind").show();
}

function addressLabellingLoad(SiteCode) {
    $("#TableSIMAddressLabellingContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"TableSIMAddressLabelling\"></table>");
    $("#TableSIMAddressLabellingContainer").css({ "height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')", "border": "1px solid #e5e5e5" });
    $("table#FormAddressLabelling input").attr("disabled", true);
    var url = apiServer + '/api/simorderlist/20?sitecode=' + SiteCode;
    $.getJSON(url, function (feedback) {

        $('#TableSIMAddressLabelling').dataTable({
            bDestroy: true,
            aaData: feedback,
            iDisplayLength: 20,
            bPaginate: true,
            bFilter: false,
            bProcessing: true,
            bInfo: false,
            aaSorting: [[5, "desc"]],
            aoColumns: [

                { mDataProp: "freesimid", sTitle: 'Order ID' },
                {
                    mDataProp: "firstname", sTitle: 'Full Name',
                    fnRender: function (ob) {
                        return ob.aData.firstname + " " + ob.aData.lastname;
                    }
                },
                { mDataProp: "iccid", sTitle: 'ICCID' },
                { mDataProp: "freesimstatus", sTitle: 'Status' },
                {
                    mDataProp: "registerdate", sTitle: 'Registration Date',
                    fnRender: function (ob) {
                        return convertDate03(ob.aData.registerdate);
                    }
                },
                { mDataProp: "sentdate", sTitle: 'Dispatch Date',
                    fnRender: function (ob) {
                        return convertDate03(ob.aData.sentdate);
                    }
                },
                { mDataProp: "SourceReg", sTitle: 'Source' }

            ],
            fnInitComplete: function (oSettings, json) {
                $("table#FormAddressLabelling button#AL_btnFind").addClass("blue").removeAttr("disabled");
                $("table#FormAddressLabelling input").removeAttr("disabled");
                $("input[name=freeSIMOrderID]").val("").focus();
            },
            fnDrawCallback: function (ob) {
                $("#TableSIMAddressLabellingContainer").css({ "background-image": "none", "border": "0" });
                $("#TableSIMAddressLabelling_wrapper .row-fluid").html("");
            }
        })
        $("#TableSIMAddressLabellingContainer").height($("#TableSIMAddressLabelling_wrapper").height());
    });
}

function postingOrderLabel(freesimid, iccid, sitecode, crm_login) {
    var url = apiServer + '/api/simorderlist';

    jQuery.support.cors = true;
    var JSONSendData = {
        freesimid: freesimid,
        iccid: iccid,
        sitecode : sitecode,
        crm_login: crm_login
    };
    
    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        async: false,
        timeout: 50000,
        success: function (data) {
            if (data.errcode == "0") {
                $("td.currentDispatchStatus").html("<span class=\"alert-success\">Success</span>");
            } else {
                $("td.currentDispatchStatus").html("<span class=\"alert-error\">Failed</span>");
                $("button#btnALDispatchingOK").show();
            }
            $("td.currentDispatchStatus").removeClass("currentDispatchStatus");
        }
    });
}


/* ----------------------------------------------------- 
   *  eof SIM ADDRESS LABELLING 
   ===================================================== */


