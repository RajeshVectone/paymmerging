﻿/* ===================================================== 
   *  TAB BUNDLES 
   ----------------------------------------------------- */


/* ----------------------------------------------------------------
 * Function Name    : Internet Profile
 * Purpose          : to show Intenet Profile  
 * Added by         : karthik
 * Create Date      : December 18th, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function GetUserListData() {

    $("#pbInternetProfileBody").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblPBInternetProfile\"></table>");
    $("#pbInternetProfileBody").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });

    var url = '/Users/getuserdetails';
    jQuery.support.cors = true;
    $.ajax
    ({
        url: url,
        type: 'post',
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#pbInternetProfileBody").css({ "background-image": "none" });
            $('#tblPBInternetProfile').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                iDisplayLength: 10,
                bPaginate: true,
                bFilter: true,
                bInfo: false,
                aaSorting: [[1, "desc"]],
                aoColumns: [

                    //{ mDataProp: "Id", sTitle: "id", bVisible: false },                   
                    { mDataProp: "Username", sTitle: "User Name" },
                    //{ mDataProp: "FirstName", sTitle: "First Name" },
                    //{ mDataProp: "LastName", sTitle: "Last Name" },
                    { mDataProp: "FullName", sTitle: "Full Name" },
                    { mDataProp: "Email", sTitle: "Email" },
                    { mDataProp: "Status", sTitle: "Status" },
                    {
                        mDataProp: "LastLoginDate_str", sTitle: "Last Login Date"
                        //fnRender: function (ob) {
                        //    return convertDateISOCustom(ob.aData.LastLoginDateUtc, 0);
                        //}
                    },
                    {
                        mDataProp: "Id", sTitle: "Action",
                        fnRender: function (ob) {
                            //var actionOption = "<select class=\"selectLLOTGAction\" data-country=\"" + ob.aData.Country + "\" data-regno=\"" + ob.aData.Reg_Number + "\" data-pt=\"" + ob.aData.Payment_Type + "\" data-ptd=\"" + ob.aData.PaymentType_Description + "\"data-balance=\"" + ob.aData.TotalAmount + "\"data-svcid=\"" + ob.aData.svc_id + "\"data-Planamount=\"" + ob.aData.Plan_amount + "\"data-planstatus=\"" + ob.aData.Status + "\" >";
                            var actionOption = "<select class=\"selectuserlistAction\" data-Id=\"" + ob.aData.Id + "\" data-UserName=\"" + ob.aData.Username + "\" id=\"selectuserlistAction\" >";;
                            actionOption += '<option value="0" style=\"color:#c0c0c0\">--- Select an action ---</option>';
                            actionOption += '<option value="1" style=\"padding:3px\">Edit</option>';
                            actionOption += '<option value="2" style=\"padding:3px\">Remove</option>';
                            if (ob.aData.Status == "Active") {
                                actionOption += '<option value="3" style=\"padding:3px\">Deactivate</option>';
                            }
                            if (ob.aData.Status == "Inactive") {
                                actionOption += '<option value="4" style=\"padding:3px\">Reactive</option>';
                            }
                            actionOption += '</select>';
                            return actionOption;
                        }
                    }

                ],
                fnDrawCallback: function () {
                    $("#pbInternetProfileBody tbody td").css({ "font-size": "12px" });
                }
            });

            //$("#tblPBInternetProfile_wrapper .row-fluid:eq(0)").remove();
            //$("#tblPBInternetProfile_wrapper .row-fluid .span6:eq(0)").addClass("span4");
            //$("#tblPBInternetProfile_wrapper .row-fluid .span6:eq(1)").addClass("span8");
            //$("#tblPBInternetProfile_wrapper .row-fluid .span6").removeClass("span6");

            var DownloadAnchor = "<a id=\"downloadAnchor\" style=\"display:none;\"></a>";
            var btnExportHistory = "<button class=\"btn blue\" id=\"btnUserlist\" name=\"btnUserlist\">Download</button>";
            //$("#tblPBInternetProfile_wrapper .row-fluid .span4").append(DownloadAnchor + btnExportHistory).show();
            $("div#pbInternetProfileBody  > div#tblPBInternetProfile_wrapper  > div.row-fluid:nth-child(3)  > div:nth-child(1)").append(DownloadAnchor + btnExportHistory).show();
        },
        error: function (feedback) {
            $("#pbInternetProfileBody").css({ "background-image": "none" }).html("").append("No User Details found").show();
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : GPRS setting sending to customer
 * Purpose          :  GPRS setting sending to customer
 * Added by         : karthik
 * Create Date      : June 4th, 2015
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function InsertNewUser(Username, FirstName, LastName, Email, Password, RetypePassword, Status, PermissionList, mode, User_Id, role_id) {
    var url = '/Users/NewUserInsert';;
    jQuery.support.cors = true;
    var JSONSendData = {
        Id: User_Id,
        Username: Username,
        FirstName: FirstName,
        LastName: LastName,
        Email: Email,
        Password: Password,
        RetypePassword: RetypePassword,
        Status: Status,
        PermissionList: PermissionList,
        role_id: role_id,
        mode: mode
        // sitecode: sitecode
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {

            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });

            // var errcode1 = errcode;
            var errcode = feedback.errcode;
            if (errcode == 0) {
                $("#GIAddUser").hide();
                $('.PMSuccess .modal-header').html(feedback.errsubject);
                $('.PMSuccess .modal-body').html(feedback.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
            else {

                $('#lblCheckusername').show();
                //$('.PMFailed .modal-header').html(feedback.errsubject);
                //$('.PMFailed .modal-body').html(feedback.errmsg);
                //$('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                //    'margin-left': function () {
                //        return window.pageXOffset - ($(this).width() / 2);
                //    }
                //});
            }
        },
        error: function (feedback) {
            $("#pbInternetProfileBody").css({ "background-image": "none" }).html("").append("No internet profile found").show();
        }
    });
}
/* ----------------------------------------------------------------
 * Function Name    : Delete User List
 * Purpose          : Delete User List
 * Added by         : karthik
 * Create Date      : June 4th, 2015
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function DeleteUser(Id, Username) {
    var url = '/Users/DeleteUserRecord';;
    jQuery.support.cors = true;
    var JSONSendData = {
        Id: Id,
        Username: Username,
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {

            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            $("#GIAddUser").hide();
            // var errcode1 = errcode;
            var errcode = feedback.errcode;
            if (errcode == 0) {
                $('.PMSuccess .modal-header').html(feedback.errsubject);
                $('.PMSuccess .modal-body').html(feedback.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
            else {
                $('.PMFailed .modal-header').html(feedback.errsubject);
                $('.PMFailed .modal-body').html(feedback.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (feedback) {
            $("#pbInternetProfileBody").css({ "background-image": "none" }).html("").append("No internet profile found").show();
        }
    });
}
/* ----------------------------------------------------------------
 * Function Name    : Deactivate
 * Purpose          : Deactivate
 * Added by         : karthik
 * Create Date      : June 4th, 2015
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function Deactivate(Id, Username) {
    var url = '/Users/Deactivate';;
    jQuery.support.cors = true;
    var JSONSendData = {
        Id: Id,
        Username: Username,
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {

            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            $("#GIAddUser").hide();
            // var errcode1 = errcode;
            var errcode = feedback.errcode;
            if (errcode == 0) {
                $('.PMSuccess .modal-header').html(feedback.errsubject);
                $('.PMSuccess .modal-body').html(feedback.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
            else {
                $('.PMFailed .modal-header').html(feedback.errsubject);
                $('.PMFailed .modal-body').html(feedback.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (feedback) {
            $("#pbInternetProfileBody").css({ "background-image": "none" }).html("").append("No internet profile found").show();
        }
    });
}
/* ----------------------------------------------------------------
 * Function Name    : Reactive
 * Purpose          : Reactive
 * Added by         : karthik
 * Create Date      : June 4th, 2015
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function Reactive(Id, Username) {
    var url = '/Users/Reactive';;
    jQuery.support.cors = true;
    var JSONSendData = {
        Id: Id,
        Username: Username,
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {

            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            $("#GIAddUser").hide();
            // var errcode1 = errcode;
            var errcode = feedback.errcode;
            if (errcode == 0) {
                $('.PMSuccess .modal-header').html(feedback.errsubject);
                $('.PMSuccess .modal-body').html(feedback.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
            else {
                $('.PMFailed .modal-header').html(feedback.errsubject);
                $('.PMFailed .modal-body').html(feedback.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (feedback) {
            $("#pbInternetProfileBody").css({ "background-image": "none" }).html("").append("No internet profile found").show();
        }
    });
}
/* ----------------------------------------------------------------
 * Function Name    : GPRS setting sending to customer
 * Purpose          :  GPRS setting sending to customer
 * Added by         : karthik
 * Create Date      : June 4th, 2015
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function GetUserPermissionlist(User_Id) {

    var url = '/Users/GetUserPermissionlist';;
    jQuery.support.cors = true;
    var JSONSendData = {
        Id: User_Id
        //Username: Username,
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {


            BasicinfoEdit(User_Id);

            $(':checkbox[name^=chklistitem]').filter(function (index, val) {
                if (feedback.indexOf(this.id) >= 0)
                    return this.checked = true;
            })

            //$('#InUsername').val(feedback[0].Username);
            //$('#inpassword').val(feedback[0].Password);
            //$('#inretypepassword').val(feedback[0].Password);
            //$('#infirstname').val(feedback[0].FirstName);
            //$('#inlastname').val(feedback[0].LastName);
            //$('#inemail').val(feedback[0].Email);
            //$('#UsernameStr3').text(User_Name);           
        },
        error: function (feedback) {
            $("#pbInternetProfileBody").css({ "background-image": "none" }).html("").append("No internet profile found").show();
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : GPRS setting sending to customer
 * Purpose          :  GPRS setting sending to customer
 * Added by         : karthik
 * Create Date      : June 4th, 2015
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function BasicinfoEdit(User_Id) {
    var url = '/Users/BasicinfoEdit';;
    jQuery.support.cors = true;
    var JSONSendData = {
        Id: User_Id
        //Username: Username,

    };
    $.ajax
    ({

        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('#InUsername').val(feedback[0].Username);
            $('#inpassword').val(feedback[0].Password);
            $('#inretypepassword').val(feedback[0].Password);
            $('#infirstname').val(feedback[0].FirstName);
            $('#inlastname').val(feedback[0].LastName);
            $('#inemail').val(feedback[0].Email);
            //$('#UsernameStr3').text(User_Name);
            $('#UsernameStr3').text(feedback[0].Username);
            $("#inUserRole").val(feedback[0].role_id);
            if (feedback[0].Active)
                //$("#inStatus").val('1');
                $("#inStatus").attr('selectedIndex', 0);
            else
                $("#inStatus").attr('selectedIndex', 1);
            //$("#inStatus").val('2');
        },
        error: function (feedback) {
            $("#pbInternetProfileBody").css({ "background-image": "none" }).html("").append("No internet profile found").show();
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : GPRS setting sending to customer
 * Purpose          :  GPRS setting sending to customer
 * Added by         : karthik
 * Create Date      : June 4th, 2015
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function GetPermissionlist(User_Id, mode) {
    var url = '/Users/GetPermissionlist';;
    jQuery.support.cors = true;
    $.ajax
    ({
        url: url,
        type: 'post',

        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            var errcode = feedback[0].errcode;
            var id = feedback[0].Id;
            var Name = feedback[0].Name;
            var table = $('<table  id="DropPermissiion" name="DropPermissiion" width="100%"></table>');
            var counter = 0;
            $('#Inpermissionlist').empty();
            $('#Inpermissionlist').append(table);
            var i = 0;
            $(feedback).each(function () {
                if (i == 0) {
                    table.append($('<tr></tr>').append($('<td  id="permissionlist" width="200px"></td>').append($('<input>').attr({
                        type: 'checkbox', name: 'chklistitem', value: this.Value, id: this.Id
                    })).append(
               $('<label style="margin-top:-16px;margin-left:20px">').attr({
                   //for: 'chklistitem' + this.Id++
               }).text(this.Name))));
                    i = i + 1;
                }
                else {
                    table.find($('tr:last').append($('<td id="permissionlist" width="200px"></td>').append($('<input>').attr({
                        type: 'checkbox', name: 'chklistitem', value: this.Value, id: this.Id
                    })).append(
                $('<label style="margin-top:-16px;margin-left:20px">').attr({
                    // for: 'chklistitem' + this.Id++
                }).text(this.Name))));
                    i = i - 1;
                }
            });
            if (mode == "2") {
                GetUserPermissionlist(User_Id);
            }
        },
        error: function (feedback) {
            $("#pbInternetProfileBody").css({ "background-image": "none" }).html("").append("No internet profile found").show();
        }
    });
}
/* ----------------------------------------------------------------
 * Function Name    : GPRS setting sending to customer
 * Purpose          :  GPRS setting sending to customer
 * Added by         : karthik
 * Create Date      : June 4th, 2015
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function GetAssignPermission(User_Id) {
    var url = '/Users/GetAssignPermission';;
    jQuery.support.cors = true;
    $.ajax
    ({
        url: url,
        type: 'post',

        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {

            var errcode = feedback[0].errcode;
            var id = feedback[0].Id;
            var Name = feedback[0].Name;
            var table = $('<table  id="DropPermissiion" name="DropPermissiion" width="100%"></table>');
            var counter = 0;
            $('#Inpermissionlist').empty();
            $('#Inpermissionlist').append(table);
            var i = 0;
            $(feedback).each(function () {
                if (i == 0) {
                    table.append($('<tr></tr>').append($('<td  width="200px"></td>').append($('<input>').attr({
                        type: 'checkbox', name: 'chklistitem', value: this.Value, id: this.Id
                    })).append(
               $('<label style="margin-top:-16px;margin-left:20px">').attr({
                   //for: 'chklistitem' + this.Id++
               }).text(this.Name))));
                    i = i + 1;
                }
                else {
                    table.find($('tr:last').append($('<td id="kkk" width="200px"></td>').append($('<input>').attr({
                        type: 'checkbox', name: 'chklistitem', value: this.Value, id: this.Id
                    })).append(
                $('<label style="margin-top:-16px;margin-left:20px">').attr({
                    // for: 'chklistitem' + this.Id++
                }).text(this.Name))));
                    i = i - 1;
                }

            });


        },
        error: function (feedback) {
            $("#pbInternetProfileBody").css({ "background-image": "none" }).html("").append("No internet profile found").show();
        }
    });
}

///* ---------------------------------------------------------------- 
// * Function Name    : GPRS setting sending to customer
// * Purpose          :  GPRS setting sending to customer
// * Added by         : karthik
// * Create Date      : June 4th, 2015
// * Last Update      : -
// * Update History   : -
// * ---------------------------------------------------------------- */
function clearfiled() {
    $('#InUsername').val("");
    $('#inpassword').val("");
    $('#inretypepassword').val("");
    $('#infirstname').val("");
    $('#inlastname').val("");
    $('#inemail').val("");
}


/* ----------------------------------------------------------------
 * Function Name    : validate New user
 * Purpose          : validate New user
 * Added by         : 
 * Create Date      : Dec 22, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function validateNewuser() {

    var isvalid = true;

    if ($('#InUsername').val() == "") {
        isvalid = false;
        $('#lblusername').show();
    }
    else {
        $('#lblusername').hide();
    }

    if ($('#inpassword').val() == "") {
        isvalid = false;
        $('#lblpwd').show();
    }
    else {
        $('#lblpwd').hide();
    }

    if ($('#inretypepassword').val() == "") {
        isvalid = false;
        $('#lblconpwd').show();
    }
    else {
        $('#lblconpwd').hide();
    }

    if ($('#infirstname').val() == "") {
        isvalid = false;
        $('#lblfirname').show();
    }
    else {
        $('#lblfirname').hide();
    }

    if ($('#inemail').val() == "") {
        isvalid = false;
        $('#lblemail').show();
    }
    else {
        $('#lblemail').hide();
    }

    var count = $("input[name='chklistitem']:checked").length;
    if (count > 0) {
        // if ($('#chklistitem input:checked').length > 0) {       
        $('#lblpermission').hide();
    }
    else {
        isvalid = false;
        $('#lblpermission').show();
    }

    if ($('#inpassword').val() === $('#inretypepassword').val()) {
        $('#lblconvalipwd').hide();
        // evt.preventDefault();
    }
    else {
        isvalid = false;
        $('#lblconvalipwd').show();
    }

    return isvalid;
};




function ValidateEmail(email) {
    var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    return expr.test(email);
};
/* ----------------------------------------------------- 
   *  eof TAB BUNDLES 
   ===================================================== */


/* User Role */
function BindUserRole() {
    var url = '/Users/GetUserRole';
    jQuery.support.cors = true;
    $.ajax
    ({
        url: url,
        type: 'post',
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            if (feedback != null && feedback.length > 0)
                var actionOption = '';
            for (icount = 0; icount < feedback.length; icount++) {
                actionOption += '<option value="' + feedback[icount].role_id + '" style=\"padding:3px\">' + feedback[icount].name + '</option>';
            }
            $('#inUserRole').append(actionOption);
        },
        error: function (feedback) {
        }
    });
}
