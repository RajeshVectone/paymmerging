﻿/*

Picks up data from API controller and flashes the data to UI.

*/
function ViewOperator(MobileNo, CollType ,SiteCode) {
    $("#tblDDOperationContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblDDOperation\"></table>");
    $("#tblDDOperationContainer").css({ "min-height": "100px", "overflow-y": "hidden", "overflow-x": "scroll", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var row = 0;
    var url = apiServer + "/api/pmbogridfill";
    var JSONSendData = {
        value: MobileNo,
        CollectionType: CollType,
        SiteCode : SiteCode
    };
    if (CollType == "CF") {
        $.ajax
        ({
            url: url,
            type: "GET",
            data: JSONSendData,
            dataType: "json",
            async: true,
            contentType: "application/json;charset=utf-8",
            cache: false,

            success: function (feedback) {

                $('#btn-SMSIntimation-submit').prop('disabled', false);
                $('#btn-Mark-submit').prop('disabled', false);
                $('#btn-DDCancellation-submit').prop('disabled', false);
                //$("#ajax-screen-masking").hide();
                $("#tblDDOperationContainer").css({ "background-image": "none" });
                $('#tblDDOperation').dataTable({
                    bDestroy: true,
                    bRetrieve: true,
                    aaData: feedback,
                    bPaginate: true,
                    bFilter: false,
                    bInfo: false,
                    iDisplayLength: 10,
                    aaSorting: [[2, "desc"]],
                    oLanguage: {
                        "sInfoEmpty": '',
                        "sEmptyTable": "No Collection Failure Upload found."
                    },

                    aoColumns: [
                            { mDataProp: "ref1", sTitle: "ref" },
                            { mDataProp: "transCode", sTitle: "transCode" },
                            { mDataProp: "returnCode", sTitle: "returnCode" },
                            { mDataProp: "returnDescription", sTitle: "returnDescription" },
                            { mDataProp: "originalProcessingDate", sTitle: "originalProcessingDate" },
                            { mDataProp: "valueOf", sTitle: "valueOf" },
                            { mDataProp: "currency", sTitle: "currency" },
                            { mDataProp: "number3", sTitle: "number3" },
                            { mDataProp: "ref4", sTitle: "ref4" },
                            { mDataProp: "name5", sTitle: "name5" },
                            { mDataProp: "sortCode6", sTitle: "sortCode6" },
                            { mDataProp: "bankName7", sTitle: "bankName7" },
                            { mDataProp: "branchName8", sTitle: "branchName8" },
                            { mDataProp: "numberOf", sTitle: "numberOf" },
                            { mDataProp: "valueOf9", sTitle: "valueOf9" },
                            { mDataProp: "currency10", sTitle: "currency10" }
                    ],
                    fnDrawCallback: function () {
                        $("#tblDDOperation tbody td").css({ "font-size": "12px" });
                    }
                });
                $("#tblDDOperation tbody td").css({ "font-size": "12px" });
            },
            error: function (feedback) {
                $("#tblDDOperationContainer").css({ "background-image": "none" }).html("").append("No Payment Profile PAYM found.").show();
            }
        });
    }
    else
    {
        $.ajax
        ({
            url: url,
            type: "GET",
            data: JSONSendData,
            dataType: "json",
            async: true,
            contentType: "application/json;charset=utf-8",
            cache: false,

            success: function (feedback) {

                $('#btn-SMSIntimation-submit').prop('disabled', false);
                $('#btn-Mark-submit').prop('disabled', false);
                $('#btn-DDCancellation-submit').prop('disabled', false);
                //$("#ajax-screen-masking").hide();
                $("#tblDDOperationContainer").css({ "background-image": "none" });
                $('#tblDDOperation').dataTable({
                    bDestroy: true,
                    bRetrieve: true,
                    aaData: feedback,
                    bPaginate: true,
                    bFilter: false,
                    bInfo: false,
                    iDisplayLength: 10,
                    aaSorting: [[2, "desc"]],
                    oLanguage: {
                        "sInfoEmpty": '',
                        "sEmptyTable": "No DD collection Upload found."
                    },
                    aoColumns: [
                            { mDataProp: "report_generation_date", sTitle: "report-generation-date" },
                            { mDataProp: "effective_date", sTitle: "effective-date" },
                            { mDataProp: "reference", sTitle: "Reference" },
                            { mDataProp: "payer_name", sTitle: "payer_name" },
                            { mDataProp: "payer_account_number", sTitle: "payer_account_number" },
                            { mDataProp: "payer_sort_code", sTitle: "payer_sort_code" },
                            { mDataProp: "reason_code", sTitle: "reason_code" },
                    ],
                    fnDrawCallback: function () {
                        $("#tblDDOperation tbody td").css({ "font-size": "12px" });
                    }
                });
                $("#tblDDOperation tbody td").css({ "font-size": "12px" });
            },
            error: function (feedback) {
                $("#tblDDOperationContainer").css({ "background-image": "none" }).html("").append("No Payment Profile PAYM found.").show();
            }
        });
    }
}
function SendSMS(UploadId) {
    var url = apiServer + "/api/SendCollectFailureSMS";
    var JSONSendData = {
        UploadId: UploadId
    };

    $.ajax({
        url: url,
        type: 'get',
        data: JSONSendData,
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            //  $("#ajax-screen-masking").show();
        },
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
           // if (CollType == 'CF') {
                $('#filesName').html("Send Intimation SMS - " + feedback );
            //}

            $('#FileGenrated').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
            $("#chFilterResultBody").css({ "background-image": "none" }).html("").append("Sending intimation SMS was not successful.").show();
            $('#filesName').html("SMS Intimation service Failed ! ");
            $('#FileGenrated').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                },
            });
        }
    });

}

function ManualMarking(UploadId, UploadedBy, CollType,SiteCode) {
    var row = 0;
    var url = apiServer + "/api/ManualMarkingCF";
    var JSONSendData = {
        UpldID: UploadId,
        UpldBy: UploadedBy,
        CollectionType: CollType
    };

    $.ajax({
        url: url,
        type: 'get',
        data: JSONSendData,
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
          //  $("#ajax-screen-masking").show();
        },
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            if (CollType == 'CF') {
                $('#filesName').html("Collection Failure - " + feedback + "<br /> Please click on the below Hyper-Link to check for availability of alternate DD Setup for the Customers. <br /><a href='/PMBO/TabViewNProcessFailures'> Click here </a>");
            }
            else if (CollType == 'DD') {
                
                $('#filesName').html("DD Cancellation - " + feedback + "<br /> Please click on the below Hyper-Link to suspend the service for the collection failed accounts <br /><a href='/PMBO/TabViewNProcessFailures'> Click here </a>");
            }

            $('#FileGenrated').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {

                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
            $("#chFilterResultBody").css({ "background-image": "none" }).html("").append("No Manual Marking data found.").show();
        }
    });
}