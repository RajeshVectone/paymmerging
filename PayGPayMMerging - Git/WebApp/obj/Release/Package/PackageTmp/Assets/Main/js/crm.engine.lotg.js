﻿/* ===================================================== 
   *  TAB LOTG 
   ----------------------------------------------------- */

/* ----------------------------------------------------------------
 * Function Name    : ViewLOTGList
 * Purpose          : to view LOTG List
 * Added by         : Edi Suryadi
 * Create Date      : December 23th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function ViewLOTGList(MobileNo, Sitecode) {
    
    var url = apiServer + '/api/LOTG';
    jQuery.support.cors = true;
    var JSONSendData = {
        Id: 1,
        Action : 1,
        MobileNo: MobileNo,
        Sitecode: Sitecode
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#TabProductLOTGLoader").hide();
            $('#tblProductLOTG').dataTable({
                bDestroy: true,
                aaData: feedback,
                iDisplayLength: 10,
                bPaginate: true,
                bFilter: false,
                bProcessing: true,
                bInfo: false,
                aaSorting: [],
                aoColumns: [
                    { mDataProp: "Country", sTitle: "Country" },
                    {
                        mDataProp: "StartDate", sTitle: "Start Date",
                        fnRender: function (ob) {
                            return convertDateISO8601(ob.aData.StartDate, 3);
                        }
                    },
                    {
                        mDataProp: "ExpiredDate", sTitle: "Expired Date",
                        fnRender: function (ob) {
                            return convertDateISO8601(ob.aData.ExpiredDate, 3);
                        }
                    },
                    { mDataProp: "Status_Description", sTitle: "Status" },
                    { mDataProp: "PaymentType_Description", sTitle: "Payment Type" },
                    {
                        mDataProp: "errsubject", sTitle: "Action", sWidth: "280px",
                        fnRender: function (ob) {
                            var actionOption = "<select class=\"selectLOTGAction\" data-country=\"" + ob.aData.Country + "\" data-regno=\"" + ob.aData.Reg_Number + "\" data-pt=\"" + ob.aData.Payment_Type + "\" data-ptd=\"" + ob.aData.PaymentType_Description + "\" >";
                            actionOption += '<option value="0" style=\"color:#c0c0c0\">--- Select an action ---</option>';

                            if (ob.aData.Cancelable == true) {
                                actionOption += '<option value="1" style=\"padding:3px\">Stop</option>';
                            }
                            if (ob.aData.Renewable == true) {
                                actionOption += '<option value="2" style=\"padding:3px\">Renew</option>';
                            }
                            if (ob.aData.ChangePayment == true) {
                                actionOption += '<option value="3" style=\"padding:3px\">Change Payment</option>';
                            }
                            actionOption += '</select>';
                            return actionOption;
                        }
                    }
                ],
                fnDrawCallback: function () {
                    $("#tblProductLOTGContainer input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                },
                fnInitComplete: function (ob) {
                    $("#tblProductLOTGContainer input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                }
            });
            $("#tblProductLOTG_wrapper .row-fluid:eq(0)").remove();
        },
        error: function (feedback) {
            $("#TabProductLOTGLoader").hide();
            $("#tblProductLOTGContainer").html("").append("No package found").show();
        }
    });
}



/* ===================================================== 
   *  TAB LLOM
   ----------------------------------------------------- */

/* ----------------------------------------------------------------
 * Function Name    : ViewLLOMList
 * Purpose          : to view LOTG List
 * Added by         : Edi Suryadi
 * Create Date      : December 23th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function ViewLLOMList(MobileNo, Sitecode) {

    var SIM_Type = 'PAYG';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1)
        SIM_Type = 'PAYM';  
    var url = apiServer + '/api/LOTG';
    jQuery.support.cors = true;
    var JSONSendData = {
        Id: 1,
        Action: 10,
        MobileNo: MobileNo,
        Sitecode: Sitecode,
        SIM_Type: SIM_Type,
        ProductCode: $('.clsproductcode')[0].innerHTML
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#TabProductLOTGLoader").hide();
            $('#tblProductLOTG').dataTable({
                bDestroy: true,
                aaData: feedback,
                iDisplayLength: 10,
                bPaginate: true,
                bFilter: false,
                bProcessing: true,
                bInfo: false,
                aaSorting: [],
                aoColumns: [
                    { mDataProp: "Country", sTitle: "Country" },
                    { mDataProp: "City", sTitle: "City" },
                    { mDataProp: "Reg_Number", sTitle: "Number" },
                    { mDataProp: "svc_id", sTitle: "Package ID" },
                    { mDataProp: "Plan", sTitle: "Plan" },
                    { mDataProp: "Plan_amount", sTitle: "Amount" },
                    { mDataProp: "SubscriptionDate", sTitle: "Start Date", sWidth: "100px" },
                    { mDataProp: "RenewalDate", sTitle: "Renewal Date", sWidth: "100px" },
                    { mDataProp: "Status", sTitle: "Status" },
                    { mDataProp: "PaymentType_Description", sTitle: "Payment Mode" },
                    //{ mDataProp: "Country", sTitle: "Country" },
                    //{ mDataProp: "City", sTitle: "Area" },                    
                    //{
                    //    mDataProp: "SubscriptionDate", sTitle: "Subscription Date",
                    //    fnRender: function (ob) {
                    //        return convertDateISO8601(ob.aData.SubscriptionDate, 3);
                    //    }
                    //},
                    //{
                    //    mDataProp: "RenewalDate", sTitle: "Renewal Date",
                    //    fnRender: function (ob) {
                    //        return convertDateISO8601(ob.aData.RenewalDate, 3);
                    //    }
                    //},
                    //{ mDataProp: "TotalAmount", sTitle: "Plan Cost" },                    
                    //{ mDataProp: "Status", sTitle: "Status" },
                    {
                        mDataProp: "errsubject", sTitle: "Action", sWidth: "280px",
                        fnRender: function (ob) {
                            var actionOption = "<select class=\"selectLLOMAction\" data-country=\"" + ob.aData.Country + "\" data-regno=\"" + ob.aData.Reg_Number + "\"  data-svcid=\"" + ob.aData.svc_id + "\" data-paymode=\"" + ob.aData.Payment_Type + "\" data-price=\"" + ob.aData.TotalAmount + "\" data-mode=\"" + ob.aData.Status + "\" >";
                            actionOption += '<option value="0" style=\"color:#c0c0c0\">--- Select an action ---</option>';

                            if (ob.aData.DisplayCancel == true) {
                                actionOption += '<option value="1" style=\"padding:3px\">Cancel</option>';
                            }
                            if (ob.aData.DisplayReactivate == true) {
                                actionOption += '<option value="2" style=\"padding:3px\">Reactivate</option>';
                            }
                            //if (ob.aData.ChangePayment == true) {
                                actionOption += '<option value="3" style=\"padding:3px\">View Subscription History</option>';
                            //}
                            actionOption += '</select>';
                            return actionOption;
                        }
                    }
                ],
                fnDrawCallback: function () {
                    $("#tblProductLOTGContainer input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                },
                fnInitComplete: function (ob) {
                    $("#tblProductLOTGContainer input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                }
            });
            $("#tblProductLOTG_wrapper .row-fluid:eq(0)").remove();
        },
        error: function (feedback) {
            $("#TabProductLOTGLoader").hide();
            $("#tblProductLOTGContainer").html("").append("No package found").show();
        }
    });
}

/*added on 19-01-2015 - by Hari - LLOM Subscription History*/
function FillLLOMSubscriptionHistory(MobileNo, RegNo, Sitecode)
{
    debugger;
    var url = apiServer + '/api/LOTG';
    jQuery.support.cors = true;
    var JSONSendData = {
        Id: 1,
        Action: 13,        
        MobileNo: MobileNo + '|' + RegNo,
        Sitecode: Sitecode,
        ProductCode: $('.clsproductcode')[0].innerHTML
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {            
            $('#tblLLOMSubscriptionHistory').dataTable({
                bDestroy: true,
                aaData: feedback,
                iDisplayLength: 10,
                bPaginate: true,
                bFilter: false,
                bProcessing: true,
                bInfo: false,
                aaSorting: [],
                /*
                aoColumns: [
                    { mDataProp: "CLI", sTitle: "CLI" },                    
                    {
                        mDataProp: "SubscriptionDate", sTitle: "Subscription Date",
                        fnRender: function (ob) {
                            return convertDateISO8601(ob.aData.SubscriptionDate, 3);
                        }
                    },
                    {
                         mDataProp: "RenewalDate", sTitle: "Renewal Date",
                         fnRender: function (ob) {
                             return convertDateISO8601(ob.aData.RenewalDate, 3);
                         }
                    },
                    { mDataProp: "RenewalStatus", sTitle: "Renewal Status" },
                ]
                */
                aoColumns: [
                    { mDataProp: "Action", sTitle: "Action" },
                    {
                        mDataProp: "Date", sTitle: "Date",
                        fnRender: function (ob) {
                            return convertDateISO8601(ob.aData.Date, 3);
                        }
                    },
                    {
                        mDataProp: "Status", sTitle: "Status"                       
                    },
                    { mDataProp: "Mode", sTitle: "Mode" },
                ]
                /*,
                fnDrawCallback: function () {
                    $("#tblLLOMSubscriptionHistoryContainer input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                },
                fnInitComplete: function (ob) {
                    $("#tblLLOMSubscriptionHistoryContainer input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                }*/
            });

            $('#LLOMSubscriptionHistory').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });

            //$("#tblLLOMSubscriptionHistory_wrapper .row-fluid:eq(0)").remove();
        },
        error: function (feedback) {
            //$("#TabProductLOTGLoader").hide();
            $("#tblLLOMSubscriptionHistoryContainer").html("").append("No package found").show();
        }
    });
}
/*end added*/

/* ===================================================== 
   *  TAB Bundle
   ----------------------------------------------------- */

/* ----------------------------------------------------------------
 * Function Name    : ViewBundleList
 * Purpose          : to view Bundle List
 * Added by         : Edi Suryadi
 * Create Date      : December 23th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function ViewBundleList(MobileNo, Sitecode) {

    var url = apiServer + '/api/LOTG';
    jQuery.support.cors = true;
    var JSONSendData = {
        Id: 1,
        Action: 9,
        MobileNo: MobileNo,
        Sitecode: Sitecode,
        ProductCode: $('.clsproductcode')[0].innerHTML
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#TabProductLOTGLoader").hide();
            $('#tblProductLOTG').dataTable({
                bDestroy: true,
                aaData: feedback,
                iDisplayLength: 10,
                bPaginate: true,
                bFilter: false,
                bProcessing: true,
                bInfo: false,
                aaSorting: [],
                aoColumns: [
                    { mDataProp: "bundleid", sTitle: "Bundle ID" },
                    { mDataProp: "name", sTitle: "Bundle Name" },
                    { mDataProp: "price", sTitle: "Price" },                    
                    {
                        mDataProp: "subscription_date", sTitle: "Subscription Date",
                        fnRender: function (ob) {
                            return convertDateISO8601(ob.aData.subscription_date, 3);
                        }
                    },
                    {
                        mDataProp: "renewal_date", sTitle: "Renewal Date",
                        fnRender: function (ob) {
                            return convertDateISO8601(ob.aData.renewal_date, 3);
                        }
                    },
                    { mDataProp: "renewal_delay", sTitle: "Renewal Period" },
                    { mDataProp: "sendmsg_mode", sTitle: "Notification Mode" },
                    { mDataProp: "Status", sTitle: "Status" },
                    //{ mDataProp: "TotalAmount", sTitle: "Plan Cost" },
                    {
                        mDataProp: "errsubject", sTitle: "Action", sWidth: "280px",
                        fnRender: function (ob) {
                            
                            var actionOption = "<select class=\"selectBundleAction\" data-bundleid=\"" + ob.aData.bundleid + "\" data-bundlename=\"" + ob.aData.name + "\" data-price=\"" + ob.aData.price + "\">";
                            actionOption += '<option value="0" style=\"color:#c0c0c0\">--- Select an action ---</option>';

                            if (ob.aData.DisplayCancel == true) {
                                actionOption += '<option value="1" style=\"padding:3px\">Cancel</option>';
                            }
                            if (ob.aData.DisplayReactivate == true) {
                                actionOption += '<option value="2" style=\"padding:3px\">Reactivate</option>';
                            }
                            //if (ob.aData.ChangePayment == true) {
                            actionOption += '<option value="3" style=\"padding:3px\">View Subscription History</option>';
                            //}
                            actionOption += '</select>';
                            return actionOption;
                        }
                    }
                ],
                fnDrawCallback: function () {
                    $("#tblProductLOTGContainer input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                },
                fnInitComplete: function (ob) {
                    $("#tblProductLOTGContainer input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                }
            });
            $("#tblProductLOTG_wrapper .row-fluid:eq(0)").remove();
        },
        error: function (feedback) {
            $("#TabProductLOTGLoader").hide();
            $("#tblProductLOTGContainer").html("").append("No package found").show();
        }
    });
}

//function CancelBundleConfirm(MobileNo, Sitecode) {
//    var url = apiServer + '/api/LOTG';
//    jQuery.support.cors = true;
//    var JSONSendData = {
//        Id: 1,
//        Action: 15,
//        MobileNo: MobileNo,
//        Sitecode: Sitecode
//    };
//    $.ajax
//   ({
//       url: url,
//       type: 'post',
//       data: JSON.stringify(JSONSendData),
//       dataType: 'json',
//       contentType: "application/json;charset=utf-8",
//       cache: false,
//       success: function (feedback) {
//           //find err_code and err_message and display          
//           if (feedback != null) {
//               if (feedback[0].errcode == "0") {
//                   $('#BundleCancelSubscription').modal("hide");
//                   $('#ResultInfo').modal({ keyboard: false, backdrop: 'static' }).css({
//                       'margin-left': function () {
//                           return window.pageXOffset - ($(this).width() / 2);
//                       }
//                   });
//                   window.location.reload();
//                   // $("#btnDirectDebit").css('display', 'none');
//                   $("#btnDirectDebit").prop('disabled', true);
//               }
//               else {
//                   $('#BundleCancelSubscription').modal("hide");
//                   $('#ErrMsg').html(feedback[0].errmsg);
//                   $('#NoData').modal({ keyboard: false, backdrop: 'static' }).css({
//                       'margin-left': function () {
//                           return window.pageXOffset - ($(this).width() / 2);
//                       }
//                   });
//               }
//           }
//           else {
//               $('#ErrMsg').html("There Some Problem For Adding DD-Setup");
//               $('#NoData').modal({ keyboard: false, backdrop: 'static' }).css({
//                   'margin-left': function () {
//                       return window.pageXOffset - ($(this).width() / 2);
//                   }
//               });
//           }
//           //end err_code and err_message and display
//       },
//       error: function (feedback) {
//           //$("#TabProductLOTGLoader").hide();
//           $("#tblLLOMSubscriptionHistoryContainer").html("").append("No package found").show();
//       }
//   });
//}

function CancelBundleConfirm(MobileNo, RegNo, Sitecode, BundleId, UserName) {
    var url = apiServer + '/api/LOTG';
    jQuery.support.cors = true;
    var JSONSendData = {
        Id: BundleId,
        Action: 15,
        MobileNo: MobileNo,
        Sitecode: Sitecode,
        LoggedUser: UserName,
        ProductCode: $('.clsproductcode')[0].innerHTML
    };
    $.ajax
   ({
       url: url,
       type: 'post',
       data: JSON.stringify(JSONSendData),
       dataType: 'json',
       contentType: "application/json;charset=utf-8",
       cache: false,
       success: function (feedback) {
           //find err_code and err_message and display          
           if (feedback != null) {
               if (feedback.errcode == "0") {
                   $('#BundleCancelSubscription').modal("hide");
                   //$('#ResultInfo').find("div:eq(1)").html(feedback[0].errmsg);
                   $('#ResultInfo').find("div:eq(1)").html(feedback.errmsg);
                  $($('#ResultInfo').find("div:eq(2) > button:eq(0)")).click(function () { window.location.reload(); })
                   $('#ResultInfo').modal({ keyboard: false, backdrop: 'static' }).css({
                       'margin-left': function () {
                           return window.pageXOffset - ($(this).width() / 2);
                       }
                   });
                   //window.location.reload();
                   // $("#btnDirectDebit").css('display', 'none');
                   //$("#btnDirectDebit").prop('disabled', true);
               }
               else {
                   $('#BundleCancelSubscription').modal("hide");
                   //$('#ErrMsg').html(feedback[0].errmsg);
                   $('#ErrMsg').html(feedback.errmsg);
                   $('#NoData').modal({ keyboard: false, backdrop: 'static' }).css({
                       'margin-left': function () {
                           return window.pageXOffset - ($(this).width() / 2);
                       }
                   });
               }
           }
           else {
               $('#ErrMsg').html("There Some Problem For Cancel Bundle");
               $('#NoData').modal({ keyboard: false, backdrop: 'static' }).css({
                   'margin-left': function () {
                       return window.pageXOffset - ($(this).width() / 2);
                   }
               });
           }
           //end err_code and err_message and display
       },
       error: function (feedback) {
           //$("#TabProductLOTGLoader").hide();
           $("#tblLLOMSubscriptionHistoryContainer").html("").append("No package found").show();
       }
   });
}


function CancelLLOMConfirm(MobileNo, RegNo, Sitecode, SvcId, UserName) {
    var url = apiServer + '/api/LOTG';
    jQuery.support.cors = true;
    var JSONSendData = {
        Id: SvcId,
        Action: 16,
        MobileNo: MobileNo,
        Sitecode: Sitecode,
        Reg_Number: RegNo,
        ProductCode: $('.clsproductcode')[0].innerHTML
    };
    $.ajax
   ({
       url: url,
       type: 'post',
       data: JSON.stringify(JSONSendData),
       dataType: 'json',
       contentType: "application/json;charset=utf-8",
       cache: false,
       success: function (feedback) {
           //find err_code and err_message and display          
           if (feedback != null) {
               if (feedback.errcode == "0") {
                   $('#LLOMCancelSubscription').modal("hide");
                   $('#ResultInfo').find("div:eq(1)").html(feedback.errmsg);
                   $($('#ResultInfo').find("div:eq(2) > button:eq(0)")).click(function () { window.location.reload(); })
                   $('#ResultInfo').modal({ keyboard: false, backdrop: 'static' }).css({
                       'margin-left': function () {
                           return window.pageXOffset - ($(this).width() / 2);
                       }
                   });
                   //window.location.reload();
                   // $("#btnDirectDebit").css('display', 'none');
                   //$("#btnDirectDebit").prop('disabled', true);
               }
               else {
                   $('#LLOMCancelSubscription').modal("hide");
                   $('#ErrMsg').html(feedback.errmsg);
                   $('#NoData').modal({ keyboard: false, backdrop: 'static' }).css({
                       'margin-left': function () {
                           return window.pageXOffset - ($(this).width() / 2);
                       }
                   });
               }
           }
           else {
               $('#ErrMsg').html("There Some Problem For Cancel LLOM");
               $('#NoData').modal({ keyboard: false, backdrop: 'static' }).css({
                   'margin-left': function () {
                       return window.pageXOffset - ($(this).width() / 2);
                   }
               });
           }
           //end err_code and err_message and display
       },
       error: function (feedback) {
           //$("#TabProductLOTGLoader").hide();
           $("#tblLLOMSubscriptionHistoryContainer").html("").append("No package found").show();
       }
   });
}

function CancelCountrySaverConfirm(MobileNo, BundleId, Sitecode, userId, AccountNo) {
    var url = apiServer + '/api/LOTG';
    jQuery.support.cors = true;
    var JSONSendData = {
        Id: BundleId,
        Action: 17,
        MobileNo: MobileNo,
        Pack_Dest: BundleId,
        Sitecode: Sitecode,
        username: userId,
        AccountNo: AccountNo,
        ProductCode: $('.clsproductcode')[0].innerHTML
    };
    $.ajax
   ({
       url: url,
       type: 'post',
       data: JSON.stringify(JSONSendData),
       dataType: 'json',
       contentType: "application/json;charset=utf-8",
       cache: false,
       success: function (feedback) {
            //find err_code and err_message and display          
           if (feedback != null) {
               debugger;
                if (feedback.errcode == "0") {
                    $('#CountrySaverCancelSubscription').modal("hide");
                    $('#ResultInfo').find("div:eq(1)").html(feedback.errmsg);
                    $($('#ResultInfo').find("div:eq(2) > button:eq(0)")).click(function () { window.location.reload(); })
                    $('#ResultInfo').modal({ keyboard: false, backdrop: 'static' }).css({
                        'margin-left': function () {
                            return window.pageXOffset - ($(this).width() / 2);
                        }
                    });

                    
                    //window.location.reload();
                    // $("#btnDirectDebit").css('display', 'none');
                    $("#btnDirectDebit").prop('disabled', true);
                }
                else {
                    $('#CountrySaverCancelSubscription').modal("hide");
                    $('#ErrMsg').html(feedback.errmsg);
                    $('#NoData').modal({ keyboard: false, backdrop: 'static' }).css({
                        'margin-left': function () {
                            return window.pageXOffset - ($(this).width() / 2);
                        }
                    });
                }
            }
            else {
                $('#ErrMsg').html("There Some Problem For Adding DD-Setup");
                $('#NoData').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }           
            //end err_code and err_message and display
       },
       error: function (feedback) {
           //$("#TabProductLOTGLoader").hide();
           $("#tblLLOMSubscriptionHistoryContainer").html("").append("No package found").show();
       }
   });
}


/*added on 19-01-2015 - by Hari - Bundle Subscription History*/
function FillBundleSubscriptionHistory(MobileNo, Sitecode, BundleId) {
    var url = apiServer + '/api/LOTG';
    jQuery.support.cors = true;
    var JSONSendData = {
        Id: BundleId,
        Action: 12,
        MobileNo: MobileNo,
        Sitecode: Sitecode,
        ProductCode: $('.clsproductcode')[0].innerHTML
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('#tblBundleSubscriptionHistory').dataTable({
                bDestroy: true,
                aaData: feedback,
                iDisplayLength: 10,
                bPaginate: true,
                bFilter: false,
                bProcessing: true,
                bInfo: false,
                aaSorting: [],
                aoColumns: [
                    {
                         mDataProp: "Date", sTitle: "Date",
                         fnRender: function (ob) {
                             return convertDateISO8601(ob.aData.Date, 3);
                         }
                    },
                    { mDataProp: "Action", sTitle: "Action" },
                    { mDataProp: "BundleDetails", sTitle: "Bundle Details" },
                    { mDataProp: "Amount", sTitle: "Amount" },
                    { mDataProp: "PaymentMode", sTitle: "Payment Mode" },
                    { mDataProp: "PaymentStatus", sTitle: "Payment Status" },
                ],
                fnDrawCallback: function () {
                    $("#tblBundleSubscriptionHistoryContainer input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                },
                fnInitComplete: function (ob) {
                    $("#tblBundleSubscriptionHistoryContainer input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                }
            });

            $('#BundleSubscriptionHistory').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });

            $('#tblBundleSubscriptionHistory').css('width', '100%');
            $("#tblBundleSubscriptionHistory_wrapper .row-fluid:eq(0)").remove();
        },
        error: function (feedback) {
            //$("#TabProductLOTGLoader").hide();
            $("#tblBundleSubscriptionHistoryContainer").html("").append("No package found").show();
        }
    });
}
/*end added*/


/* ----------------------------------------------------------------
 * Function Name    : RefreshLOTGList
 * Purpose          : to refresh LOTG List
 * Added by         : Edi Suryadi
 * Create Date      : December 24th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function RefreshLOTGList(MobileNo, Sitecode) {
    $("#TabProductLOTGLoader").show();
    $("#tblProductLOTG_wrapper").remove();
    $("#tblProductLOTGContainer").append("<table class=\"table table-bordered table-hover\" id=\"tblProductLOTG\"></table>");
    
    var url = apiServer + '/api/LOTG';
    jQuery.support.cors = true;
    var JSONSendData = {
        Id: 1,
        Action: 1,
        MobileNo: MobileNo,
        Sitecode: Sitecode,
        ProductCode: $('.clsproductcode')[0].innerHTML
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#TabProductLOTGLoader").hide();
            var lastAccessedPage = parseInt($("#tblProductLOTGContainer input[name=accessedPage]").val());
            var oTable = $('#tblProductLOTG').dataTable({
                bDestroy: true,
                aaData: feedback,
                iDisplayLength: 10,
                bPaginate: true,
                bFilter: false,
                bInfo: false,
                aaSorting: [],
                aoColumns: [
                    { mDataProp: "Country", sTitle: "Country" },
                    {
                        mDataProp: "StartDate", sTitle: "Start Date",
                        fnRender: function (ob) {
                            return convertDateISO8601(ob.aData.StartDate, 3);
                        }
                    },
                    {
                        mDataProp: "ExpiredDate", sTitle: "Expired Date",
                        fnRender: function (ob) {
                            return convertDateISO8601(ob.aData.ExpiredDate, 3);
                        }
                    },
                    { mDataProp: "Status_Description", sTitle: "Status" },
                    { mDataProp: "PaymentType_Description", sTitle: "Payment Type" },
                    {
                        mDataProp: "errsubject", sTitle: "Action", sWidth: "280px",
                        fnRender: function (ob) {
                            var actionOption = "<select class=\"selectLOTGAction\" data-country=\"" + ob.aData.Country + "\" data-regno=\"" + ob.aData.Reg_Number + "\" data-pt=\"" + ob.aData.Payment_Type + "\" data-ptd=\"" + ob.aData.PaymentType_Description + "\" >";
                            actionOption += '<option value="0" style=\"color:#c0c0c0\">--- Select an action ---</option>';

                            if (ob.aData.Cancelable == true) {
                                actionOption += '<option value="1" style=\"padding:3px\">Stop</option>';
                            }
                            if (ob.aData.Renewable == true) {
                                actionOption += '<option value="2" style=\"padding:3px\">Renew</option>';
                            }
                            if (ob.aData.ChangePayment == true) {
                                actionOption += '<option value="3" style=\"padding:3px\">Change Payment</option>';
                            }
                            actionOption += '</select>';
                            return actionOption;
                        }
                    }
                ],
                fnDrawCallback: function () {
                    $("#tblProductLOTGContainer input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                },
                fnInitComplete: function (ob) {
                    $("#tblProductLOTGContainer input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                }
            });
            $("#tblProductLOTG_wrapper .row-fluid:eq(0)").remove();
            $("#tblProductLOTGContainer input[name=accessedPage]").val(lastAccessedPage);
            oTable.fnPageChange(lastAccessedPage);
        },
        error: function (feedback) {
            $("#TabProductLOTGLoader").hide();
            $("#tblProductLOTGContainer").html("").append("No package found").show();
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : LTOGChangePayment 
 * Purpose          : To change payment method of LTOG 
 * Added by         : Edi Suryadi
 * Create Date      : December 23th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function LTOGChangePayment(MobileNo, RegNo, Sitecode, PaymentType, LoggedUser) {

    var url = apiServer + '/api/LOTG';
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    var JSONSendData = {
        Id: 1,
        Action: 3,
        MobileNo: MobileNo,
        Reg_Number: RegNo,
        Sitecode: Sitecode,
        Payment_Type: PaymentType,
        LoggedUser: LoggedUser,
        ProductCode: $('.clsproductcode')[0].innerHTML
    };

    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();

            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });

            var stat = data.errcode;
            if (stat == 0) {
                $('.PMSuccess .modal-header').html(data.errsubject);
                $('.PMSuccess .modal-body').html(data.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            } else {
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : LTOGRenewPayment 
 * Purpose          : To renew payment of LTOG 
 * Added by         : Edi Suryadi
 * Create Date      : December 23th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function LTOGRenewPayment(MobileNo, RegNo, Sitecode, LoggedUser) {

    var url = apiServer + '/api/LOTG';
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    jQuery.support.cors = true;
    var JSONSendData = {
        Id: 1,
        Action: 5,
        MobileNo: MobileNo,
        Reg_Number: RegNo,
        Sitecode: Sitecode,
        LoggedUser: LoggedUser,
        ProductCode: $('.clsproductcode')[0].innerHTML
    };

    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();

            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "2" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });

            var stat = data.errcode;
            if (stat == 0) {
                $('.PMSuccess .modal-header').html(data.errsubject);
                $('.PMSuccess .modal-body').html(data.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            } else {
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : LTOGStopPayment 
 * Purpose          : To stop payment of LTOG 
 * Added by         : Edi Suryadi
 * Create Date      : December 23th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function LTOGStopPayment(MobileNo, RegNo, Sitecode, PaymentType, LoggedUser) {

    var url = apiServer + '/api/LOTG';
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    var url = apiServer + '/api/LOTG';
    jQuery.support.cors = true;
    var JSONSendData = {
        Id: 1,
        Action: 8,
        MobileNo: MobileNo,
        Reg_Number : RegNo,
        Sitecode: Sitecode,
        Payment_Type: PaymentType,
        LoggedUser: LoggedUser,
        ProductCode: $('.clsproductcode')[0].innerHTML
    };

    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();

            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });

            var stat = data.errcode;
            if (stat == 0) {
                $('.PMSuccess .modal-header').html(data.errsubject);
                $('.PMSuccess .modal-body').html(data.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            } else {
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        }
    });
}

$.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
    return {
        "iStart": oSettings._iDisplayStart,
        "iEnd": oSettings.fnDisplayEnd(),
        "iLength": oSettings._iDisplayLength,
        "iTotal": oSettings.fnRecordsTotal(),
        "iFilteredTotal": oSettings.fnRecordsDisplay(),
        "iPage": oSettings._iDisplayLength === -1 ?
			0 : Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
        "iTotalPages": oSettings._iDisplayLength === -1 ?
			0 : Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
    };
};

/* ----------------------------------------------------- 
   *  eof TAB LOTG 
   ===================================================== */


