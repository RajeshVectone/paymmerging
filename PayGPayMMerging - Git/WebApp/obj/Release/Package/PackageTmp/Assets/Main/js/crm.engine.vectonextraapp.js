﻿/* ===================================================== 
   *  TAB GENERAL 
   ----------------------------------------------------- */

/* ----------------------------------------------------------------
 * Function Name    : Vectone Xtra App
 * Purpose          : to show Vectone Xtra App Information
 * Added by         : Moorthy
 * Create Date      : December 5th, 2018
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function BindVectoneXtraDetails(MobileNo) {
    debugger;
    var SIM_Type = 'PAYG';
    if (window.location.href.toLowerCase().indexOf('/paym/') > -1) {
        SIM_Type = 'PAYM';
    }

    var url = apiServer + '/api/vectonextraapp';

    jQuery.support.cors = true;
    var JSONSendData = {
        MobileNo: MobileNo,
        Type:1
    };

    $("table#tblVectoneXtraApp td#tdVXAsignup_date").html("");
    $("table#tblVectoneXtraApp td#tdVXAcreate_by").html("");
    $("table#tblVectoneXtraApp td#tdVXAnumber_type").html("");
    $("table#tblVectoneXtraApp td#tdVXAlast_login_date").html("");
    $("table#tblVectoneXtraApp td#tdVXApurchased_vno").html("");
    $("table#tblVectoneXtraApp td#tdVXApurchased_did").html("");
    $("table#tblVectoneXtraApp td#tdVXAfree_virtual_no").html("");

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            debugger;
            $("#TabVectoneXtraAppLoader").hide();
            $("#tblVectoneXtraAppContainer").show();
            var errcode = -1;
            if (feedback != '' && feedback != null) {
                errcode = feedback.errcode;
            }

            if (feedback.signup_date != null && feedback.signup_date_string != "01-01-0001 12:00:00 AM")
                $("table#tblVectoneXtraApp td#tdVXAsignup_date").html(feedback.signup_date_string);
            else
                $("table#tblVectoneXtraApp td#tdVXAsignup_date").html("");

            if (feedback.create_by != null)
                $("table#tblVectoneXtraApp td#tdVXAcreate_by").html(feedback.create_by);
            else
                $("table#tblVectoneXtraApp td#tdVXAcreate_by").html("");

            if (feedback.number_type != null)
                $("table#tblVectoneXtraApp td#tdVXAnumber_type").html(feedback.number_type);
            else
                $("table#tblVectoneXtraApp td#tdVXAnumber_type").html("");

            if (feedback.last_login_date != null && feedback.last_login_date_string != "01-01-0001 12:00:00 AM")
                $("table#tblVectoneXtraApp td#tdVXAlast_login_date").html(feedback.last_login_date_string);
            else
                $("table#tblVectoneXtraApp td#tdVXAlast_login_date").html("");

            if (feedback.purchased_vno != null)
                $("table#tblVectoneXtraApp td#tdVXApurchased_vno").html(feedback.purchased_vno);
            else
                $("table#tblVectoneXtraApp td#tdVXApurchased_vno").html("");

            if (feedback.purchased_did != null)
                $("table#tblVectoneXtraApp td#tdVXApurchased_did").html(feedback.purchased_did);
            else
                $("table#tblVectoneXtraApp td#tdVXApurchased_did").html("");

            if (feedback.free_virtual_no != null)
                $("table#tblVectoneXtraApp td#tdVXAfree_virtual_no").html(feedback.free_virtual_no);
            else
                $("table#tblVectoneXtraApp td#tdVXAfree_virtual_no").html("");
        }
    });
}

//CRM Enhancement 2
function BindVectoneXtraCallHistory(mobileno, sitecode, date_fr, date_to) {
    debugger;
    $("#tblVXACHSContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblVXACHS\"></table>");
    $("#tblVXACHSContainer").css({ "min-height": "100px", "background-repeat": "no-repeat", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var row = 0;
    var url = apiServer + "/api/vectonextraapp";
    var JSONSendData = {
        MobileNo: mobileno,
        Sitecode: sitecode,
        DateFrom: date_fr,
        DateTo: date_to,
        Type: 2
    };
    $.ajax
    ({
        url: url,
        type: "POST",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('#tblVXACHS').html('');
            $("#tblVXACHSContainer").css({ "background-image": "none" });
            $('#tblVXACHS').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                iDisplayLength: 10,
                bPaginate: true,
                bFilter: true,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [[0, "desc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No records found."
                },
                aoColumns: [
                    { mDataProp: "History_Type", sTitle: "History Type", },
                    { mDataProp: "Date_Time_string", sTitle: "Date Time" },
                    { mDataProp: "CLI", sTitle: "CLI" },
                    { mDataProp: "Destination_Number", sTitle: "Dest. Number" },
                    { mDataProp: "Type", sTitle: "Type" },
                    { mDataProp: "Destination_Code", sTitle: "Dest. Code" },
                    { mDataProp: "Duration_Min", sTitle: "Duration Min" },
                    { mDataProp: "Data_Usage", sTitle: "Data Usage" },
                    { mDataProp: "Tariff_Class", sTitle: "Tariff Class" },
                    { mDataProp: "Roaming_Zone", sTitle: "Roaming Zone" },
                    { mDataProp: "Package_Name", sTitle: "Package Name" },
                    { mDataProp: "Net_Charges", sTitle: "Net Charges" },
                    { mDataProp: "Balance", sTitle: "Balance" }
                ],
                fnDrawCallback: function () {
                    $("#tblVXACHS tbody td").css({ "font-size": "12px" });
                }
            });
            $("#tblVXACHS tbody td").css({ "font-size": "12px" });
        },
        error: function (feedback) {
            $("#tblVXACHSContainer").css({ "background-image": "none" }).html("").append("No records found.").show();
        }
    });
}