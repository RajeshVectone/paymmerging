﻿/* ===================================================== 
   *  AUSTRIA PORT IN - PORTING REQUEST
   ----------------------------------------------------- */
/* ----------------------------------------------------------------
 * Function Name    : ATPortINSaveAndSend
 * Purpose          : to save data and send the porting request
 * Added by         : Edi Suryadi
 * Create Date      : September 30th, 2013
 * Last Update      : October 03rd, 2013
 * Update History   : -
 * ---------------------------------------------------------------- */
function ATPortINSaveAndSend(p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20,p21, p22, p23, p24, p25, p26, p27, p28) {
    var url = apiServer + '/api/atporting/';

    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    jQuery.support.cors = true;
    var JSONSendData = {
        Title: p1,
        Firstname: p2,
        Lastname: p3,
        Houseno: p4,
        Address1: p5,
        Address2: p6,
        City: p7,
        Postcode: p8,
        State: p9,
        Country: p10,
        CountryCode: p11,
        Telephone: p12,
        BBMsisdn: p13,
        CompanyName: p14,
        Fax: p15,
        Email: p16,
        DOB: p17,
        SubscriberType: p18,
        CustomerType: p19,
        ReceipientOperator: p20,
        ReceipientOperatorCode: p21,
        PortedMsisdn: p22,
        PUK: p23,
        VoiceMail: p24,
        PortingCode: p25,
        Donor: p26,
        DonorCode: p27,
        DesiredPortedDate : p28,
        ActionType: 1, // 1:Request Porting, 2:Request Cancel, 3:Request Neuv Info List , 5:Send Neuv Info, 8:Transaction Report
        Sitecode: "BAU"
    };

    //window.console.log(JSONSendData);

    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();

            var stat = data.errcode;
            if (stat == 0) {
                $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
                $('.PMSuccess .modal-header').html(data.errsubject);
                $('.PMSuccess .modal-body').html(data.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            } else {
                $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "2" });
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (data) {
            $("#ajax-screen-masking").hide();
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            $('.PMFailed .modal-header').html(data.errsubject);
            $('.PMFailed .modal-body').html(data.errmsg);
            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });
}

function ATPortINGetComboBoxList(InfoType) {
    var url = apiServer + '/api/atporting/';
    jQuery.support.cors = true;
    var JSONSendData = {
        InfoType: InfoType,
        Sitecode: "BAU"
    };

    $.ajax({
        url: url,
        type: 'GET',
        data: JSONSendData,
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: true,
        success: function (data) {
            var optionData = "";
            // Country List
            if (InfoType == 2) {
                for (var idx in data) {
                    optionData += "<option value=" + data[idx].CountryCode + ">" + data[idx].CountryName + "</option>";
                }
                $("select[name=ATPortINRequestPorting_sCountry]").html("").append(optionData).removeAttr("disabled");
            // Subscriber Type
            } else if (InfoType == 3) {
                for (var idx in data) {
                    optionData += "<option value=" + data[idx].SubscriberType + " alt=\"" + data[idx].TypeDesc + "\">" + data[idx].TypeName + "</option>";
                }
                $("select[name=ATPortINRequestPorting_sSubscriberType]").html("").append(optionData).removeAttr("disabled");
            // Donor Operator
            } else if (InfoType == 5) {
                for (var idx in data) {
                    if (data[idx].IsHome != 1) {
                        optionData += "<option value=" + data[idx].MnpId + " alt=\"" + data[idx].MnpCode + "\">" + data[idx].Label + "</option>";
                    }
                }
                $("select[name=ATPortINRequestPorting_sDonor]").html("").append(optionData).removeAttr("disabled");
            }
        }
    });
}


/* ----------------------------------------------------- 
   *  eof AUSTRIA PORT IN - PORTING REQUEST
   ===================================================== */






