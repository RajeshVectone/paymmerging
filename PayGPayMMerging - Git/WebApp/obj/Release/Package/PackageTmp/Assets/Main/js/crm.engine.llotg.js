﻿/* ===================================================== 
   *  TAB LLOTG 
   ----------------------------------------------------- */

/* ----------------------------------------------------------------
 * Function Name    : ViewLLOTGList
 * Purpose          : to view Landline On The Go List
 * Added by         : Edi Suryadi
 * Create Date      : January 28th, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function ViewLLOTGList(MobileNo, Sitecode) {

    var url = apiServer + '/api/LLOTG';
    jQuery.support.cors = true;
    var JSONSendData = {
        Id: 1,
        Action: 1,
        MobileNo: MobileNo,
        Sitecode: Sitecode
    };    
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#TabProductLLOTGLoader").hide();
            $('#tblProductLLOTG').dataTable({
                bDestroy: true,
                aaData: feedback,
                iDisplayLength: 10,
                bPaginate: true,
                bFilter: false,
                bProcessing: true,
                bInfo: false,
                aaSorting: [],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No package found."
                },
                aoColumns: [
                    { mDataProp: "Country", sTitle: "Country" },
                    { mDataProp: "City", sTitle: "City" },
                    { mDataProp: "Reg_Number", sTitle: "Number" },
                    { mDataProp: "svc_id", sTitle: "Package ID" },
                    { mDataProp: "Plan", sTitle: "Plan" },
                    { mDataProp: "Plan_amount", sTitle: "Amount" },
                    { mDataProp: "SubscriptionDate", sTitle: "Start Date", sWidth: "100px" },
                    { mDataProp: "RenewalDate", sTitle: "Renewal Date", sWidth: "100px" },
                    { mDataProp: "Status", sTitle: "Status" },
                    { mDataProp: "PaymentType_Description", sTitle: "Payment Mode" },
                    {
                        mDataProp: "errsubject", sTitle: "Action", sWidth: "280px",
                        fnRender: function (ob) {                          
                            debugger;
                            var actionOption = "<select class=\"selectLLOTGAction\" data-country=\"" + ob.aData.Country + "\" data-regno=\"" + ob.aData.Reg_Number + "\" data-pt=\"" + ob.aData.Payment_Type + "\" data-ptd=\"" + ob.aData.PaymentType_Description + "\"data-balance=\"" + ob.aData.TotalAmount + "\"data-svcid=\"" + ob.aData.svc_id + "\"data-Planamount=\"" + ob.aData.Plan_amount + "\"data-planstatus=\"" + ob.aData.Status + "\" >";
                            actionOption += '<option value="0" style=\"color:#c0c0c0\">--- Select an action ---</option>';
                            if (ob.aData.DisplayCancel == true) {
                                actionOption += '<option value="1" style=\"padding:3px\">Cancel</option>';
                            }
                            if (ob.aData.DisplayReactivate == true || ob.aData.Displaynotactive == true || ob.aData.DisplaySuspended == true || ob.aData.DisplayRejected == true) {
                                actionOption += '<option value="2" style=\"padding:3px\">Reactivate</option>';
                            }
                            if (ob.aData.DisplayChangepayment == true) {
                                actionOption += '<option value="4" style=\"padding:3px\">Change Payment Mode</option>';
                            }
                            actionOption += '<option value="3" style=\"padding:3px\">View Subscription History </option>';
                            actionOption += '</select>';
                            return actionOption;
                        }
                    }
                ],
                fnDrawCallback: function () {
                    $("#tblProductLLOTGContainer input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                },
                fnInitComplete: function (ob) {
                    $("#tblProductLLOTGContainer input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                }
            });
            $("#tblProductLLOTG_wrapper .row-fluid:eq(0)").remove();
        },
        error: function (feedback) {
            $("#TabProductLLOTGLoader").hide();
            $("#tblProductLLOTGContainer").html("").append("No package found").show();
        }
    });
}
function ViewLLOTGListPAYM(MobileNo, Sitecode) {
    
    var url = apiServer + '/api/LOTG';
    jQuery.support.cors = true;
    var JSONSendData = {
        Id: 1,
        Action : 1,
        MobileNo: MobileNo,
        Sitecode: Sitecode
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#TabProductLLOTGLoader").hide();
            $('#tblProductLLOTG').dataTable({
                bDestroy: true,
                aaData: feedback,
                iDisplayLength: 10,
                bPaginate: true,
                bFilter: false,
                bProcessing: true,
                bInfo: false,
                aaSorting: [],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No package found."
                },
                aoColumns: [
                    { mDataProp: "Country", sTitle: "Country" },
                    { mDataProp: "Country", sTitle: "City" },
                    { mDataProp: "Country", sTitle: "Number" },
                    { mDataProp: "Status_Description", sTitle: "Package ID" },
                    { mDataProp: "Country", sTitle: "Plan" },
                    { mDataProp: "Status_Description", sTitle: "Amount" },
                    {
                        mDataProp: "StartDate", sTitle: "Start Date",
                        fnRender: function (ob) {
                            return convertDateISO8601(ob.aData.StartDate, 3);
                        }
                    },
                    {
                        mDataProp: "ExpiredDate", sTitle: "Last Renewed Date",
                        fnRender: function (ob) {
                            return convertDateISO8601(ob.aData.ExpiredDate, 3);
                        }
                    },
                    {
                        mDataProp: "ExpiredDate", sTitle: "Renewal Date",
                        fnRender: function (ob) {
                            return convertDateISO8601(ob.aData.ExpiredDate, 3);
                        }
                    },
                    
                    { mDataProp: "Status_Description", sTitle: "Status" },
                    { mDataProp: "PaymentType_Description", sTitle: "Payment Mode" },
                    { mDataProp: "PaymentType_Description", sTitle: "Last action by" }
                    //{
                    //    mDataProp: "errsubject", sTitle: "Action", sWidth: "280px",
                    //    fnRender: function (ob) {
                    //        var data_param = ob.aData.Country + "|" + ob.aData.Country + "|" + ob.aData.Country + "|" + ob.aData.Country + "|" + ob.aData.PaymentType_Description;
                    //        var actionOption = "<select class=\"selectLLOTGAction\" data-param=\"" + data_param + "\" >";
                    //        actionOption += '<option value="0" style=\"color:#c0c0c0\">--- Select an action ---</option>';

                    //        //if (ob.aData.Cancelable == true) {
                    //        //    actionOption += '<option value="1" style=\"padding:3px\">Stop</option>';
                    //        //}
                    //        //if (ob.aData.Renewable == true) {
                    //        //    actionOption += '<option value="2" style=\"padding:3px\">Renew</option>';
                    //        //}
                    //        //if (ob.aData.ChangePayment == true) {
                    //        //    actionOption += '<option value="3" style=\"padding:3px\">Change Payment</option>';
                    //        //}
                    //        actionOption += '<option value="1" style=\"padding:3px\">Cancel</option>';
                    //        actionOption += '<option value="2" style=\"padding:3px\">Reactivate</option>';
                    //        actionOption += '</select>';
                    //        return actionOption;
                    //    }
                    //}
                ],
                fnDrawCallback: function () {
                    $("#tblProductLLOTGContainer input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                },
                fnInitComplete: function (ob) {
                    $("#tblProductLLOTGContainer input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                }
            });
            $("#tblProductLLOTG_wrapper .row-fluid:eq(0)").remove();
        },
        error: function (feedback) {
            $("#TabProductLLOTGLoader").hide();
            $("#tblProductLLOTGContainer").html("").append("No package found").show();
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : LLTOGviewsubscription
 * Purpose          : LLOM viewsubscription
 * Added by         : Edi Suryadi
 * Create Date      : january 28th, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function LLTOGviewsubscription(MobileNo, Sitecode, Reg_number) {
    //$("#TabProductLLOTGLoader").show();
    //$("#tblProductLLOTG_wrapper").remove();
    //$("#tblProductLLOTGContainer").append("<table class=\"table table-bordered table-hover\" id=\"tblProductLLOTG\"></table>");
    var url = apiServer + '/api/LLOTG';
    jQuery.support.cors = true;
    var JSONSendData = {
        Id: 2,
        Action: 2,
        MobileNo: MobileNo,
        Reg_Number: Reg_number,
        Sitecode: Sitecode
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            //$("#TabProductLLOTGLoader").hide();
            // var lastAccessedPage = parseInt($("#tblProductLLOTGContainer input[name=accessedPage]").val());
            $('#tblLLOTGSubscriptionHistory').dataTable({
                bDestroy: true,
                aaData: feedback,
                iDisplayLength: 10,
                bPaginate: true,
                bFilter: false,
                bProcessing: true,
                bInfo: false,
                aaSorting: [],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Subscription History found."
                },

                aoColumns:
                    [
                    { mDataProp: "Action", sTitle: "Action" },
                    {
                        mDataProp: "Date", sTitle: "Date", sType: "date-euro",
                        fnRender: function (ob) {
                            return convertDateISO8601(ob.aData.Date, 3);
                        }
                    },
                    {
                        mDataProp: "Status", sTitle: "Status"
                    },
                    { mDataProp: "Mode", sTitle: "Mode" },
                    ],
                /*  [
                  { mDataProp: "Date", sTitle: "Date" },
                  { mDataProp: "Payment_Type", sTitle: "Payment Type" },
                  { mDataProp: "Amount", sTitle: "Amount" },
                  { mDataProp: "Payment_Method", sTitle: "Payment Method" },
                  { mDataProp: "Payment_Status", sTitle: "Payment Status" },
                  
              ],*/

            });
            $('#LLOTGSubscriptionHistory').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });

        },
        error: function (feedback) {
            $("#TabProductLLOTGLoader").hide();
            $("#tblProductLLOTGContainer").html("").append("No Subscription found").show();
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : GetLLOTGPlans
 * Purpose          : Get LLOTG Plans
 * Added by         : Rajesh Natarajan
 * Create Date      : Dec 22, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */

function GetLLOTGPlans(country, paytype, sitecode) {

    var url = apiServer + '/api/LLOTG/GetPlanpackage?';

    jQuery.support.cors = true;
    var JSONSendData = {
        country: country,
        paytype: paytype,
        sitecode: sitecode
    };

    $.ajax
    ({
        url: url,
        type: 'get',
        data: JSONSendData,
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {

            var LLOTGPlans = "";
            LLOTGPlans += "<ul  style='list-style: none;width: auto;'>";

            $.each(feedback, function (key, value) {


                LLOTGPlans += "<li><input type=radio style='margin: 15px;' data-ordervalue=\"" + $.trim(value.Price) + "\"data-plan=\"" + $.trim(value.Plan_Desc) + "\"data-newsvcid=\"" + $.trim(value.svc_id) + "\"data-currency=\"" + $.trim(value.currency) + "\">" + $.trim(value.Plan_Descsymbol) + '</li>';


            });
            LLOTGPlans += '</ul>';

            $('#LLOTGPlans').html(LLOTGPlans);
            $('#LLOTGSelectPlan').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        },
        error: function (feedback) {
            $("#TabProductLLOTGLoader").hide();
            $("#tblProductLLOTGContainer").html("").append("No Plan found").show();
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : CancelLLOTGSubscription
 * Purpose          : Cancel LLOTG Subscription
 * Added by         : Rajesh Natarajan
 * Create Date      : Dec 22, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */

function CancelLLOTGSubscription(mobileno, sitecode, reg_number, svcid) {

    var url = apiServer + '/api/LLOTG';
    jQuery.support.cors = true;
    //if (paymode === "Pay By Credit") {
    //    paymode = 2;
    //} else {
    //    paymode = 1;
    //}
    var JSONSendData = {
        Id: 1,
        Action: 3,
        MobileNo: mobileno,
        Sitecode: sitecode,
        LoggedUser: reg_number + ',' + svcid
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {

            if (feedback === 0) {

                $('#LLOTGResultInfo').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });

            } else {
                $('#LLOTGResultInfostatus').text('Request Failed');
                $('#LLOTGResultInfo').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }

        },
        error: function (feedback) {
            $("#TabProductLLOTGLoader").hide();
            return false;
        }
    });
    return true;
};

/* ----------------------------------------------------------------
 * Function Name    : GetUserBalance
 * Purpose          : LLOTG Get User Balance
 * Added by         : Rajesh Natarajan
 * Create Date      : Dec 22, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function GetUserBalance(mobileno, country, sitecode, paymode, amount, data_curreny) {

    var url = apiServer + '/api/LLOTG';
    jQuery.support.cors = true;
    //if (paymode === "Pay By Credit") {
    //    paymode = 2;
    //} else {
    //    paymode = 1;
    //}
    var JSONSendData = {
        Id: 1,
        Action: 6,
        MobileNo: mobileno,
        Sitecode: sitecode,
        LoggedUser: country + ',' + paymode + ',' + amount
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {


            $.each(feedback, function (key, value) {
                $('#tdOCFreeMins').text(value.Freemins);
                $('#tdOCCAB').text(value.Balance + data_curreny);
            });
        },
        error: function (feedback) {
            $("#TabProductLLOTGLoader").hide();
            $("#tblProductLLOTGContainer").html("").append("No package found").show();
        }
    });
};

/* ----------------------------------------------------------------
 * Function Name    : PayByBalance
 * Purpose          : LLOTG Pay By Balance
 * Added by         : Rajesh Natarajan
 * Create Date      : Dec 22, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function SuspendedtoCancelled(CurMobileno, sitecode, old_svcid) {

    var url = apiServer + '/api/LLOTG';
    jQuery.support.cors = true;
    var JSONSendData = {
        Id: 1,
        Action: 11,
        MobileNo: CurMobileno,
        Sitecode: sitecode,
        LoggedUser: old_svcid
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {

            //if (feedback === 0) {

            //    ReactivateLLOTGSubscription(CurMobileno, sitecode, Reg_number, svcid, PaymentType);

            //} else {
            //    $('#LLOTGResultInformationfinal').text('Payment Failed');
            //    $('#LLOTGResultInformation').modal({ keyboard: false, backdrop: 'static' }).css({
            //        'margin-left': function () {
            //            return window.pageXOffset - ($(this).width() / 2);
            //        }
            //    });
            //}
        },
        error: function (feedback) {
            $("#TabProductLLOTGLoader").hide();
            $("#tblProductLLOTGContainer").html("").append("No package found").show();
            return false;
        }
    });
    return true;
};


/* ----------------------------------------------------------------
 * Function Name    : PayByBalance
 * Purpose          : LLOTG Pay By Balance
 * Added by         : Rajesh Natarajan
 * Create Date      : Dec 22, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function PayByBalance(CurMobileno, sitecode, Reg_number, svcid, PaymentType, amount, mode) {

    var url = apiServer + '/api/LLOTG';
    jQuery.support.cors = true;
    var JSONSendData = {
        Id: 1,
        Action: 5,
        MobileNo: CurMobileno,
        Sitecode: sitecode,
        LoggedUser: amount,
        mode: mode
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {

            if (feedback === 0) {
                //if (mode == 1) {
                //    SuspendedtoCancelled(CurMobileno, sitecode, old_svcid)
                //}
                ReactivateLLOTGSubscription(CurMobileno, sitecode, Reg_number, svcid, PaymentType, mode);

            } else {
                $('#LLOTGResultInformationfinal').text('Payment Failed');
                $('#LLOTGResultInformation').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (feedback) {
            $("#TabProductLLOTGLoader").hide();
            $("#tblProductLLOTGContainer").html("").append("No package found").show();
            return false;
        }
    });
    return true;
};


/* ----------------------------------------------------------------
 * Function Name    : ReactivateLLOTGSubscription
 * Purpose          : Reactivate LLOTG Subscription
 * Added by         : Rajesh Natarajan
 * Create Date      : Dec 22, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */

function ReactivateLLOTGSubscription(mobileno, sitecode, reg_number, svcid, PaymentType, mode) {
    //CurMobileno, sitecode, Reg_number, svcid, PaymentType
    var url = apiServer + '/api/LLOTG';
    jQuery.support.cors = true;
    //if (PaymentType === "Pay By Credit") {
    //    paymode = 2;
    //} else {
    //    paymode = 1;
    //}
    var JSONSendData = {
        Id: 1,
        Action: 7,
        MobileNo: mobileno,
        Sitecode: sitecode,
        mode: mode,
        //Reg_Number:reg_number,
        LoggedUser: reg_number + ',' + svcid + ',' + PaymentType
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {            
            $('.PMSuccess .modal-footer .btnPMSuccessOKvalue').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOKvalue').attr({ "alt": "1" });

            if (feedback.errcode === 0) {

                $('.PMSuccess .modal-header').html(feedback.errsubject);
                $('.PMSuccess .modal-body').html(feedback.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });

                //$('#LLOTGResultInformation').modal({ keyboard: false, backdrop: 'static' }).css({
                //    'margin-left': function () {
                //        return window.pageXOffset - ($(this).width() / 2);
                //    }
                //});
                //ViewLLOTGList(mobileno, sitecode);
            }
            else {

                $('.PMFailed .modal-header').html(feedback.errsubject);
                $('.PMFailed .modal-body').html(feedback.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
                //$('#LLOTGResultInformationfinal').text('Payment Failed');
                //$('#LLOTGResultInformation').modal({ keyboard: false, backdrop: 'static' }).css({
                //    'margin-left': function () {
                //        return window.pageXOffset - ($(this).width() / 2);
                //    }
                //});
            }
        },
        error: function (feedback) {
            $("#TabProductLLOTGLoader").hide();
            $("#tblProductLLOTGContainer").html("").append("No package found").show();
            return false;
        }
    });
    return true;
};



/* ----------------------------------------------------------------
 * Function Name    : GetLLOTGSavedCC
 * Purpose          : Get LLOTG Saved Credit Card
 * Added by         : Rajesh Natarajan
 * Create Date      : Dec 22, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */

function GetLLOTGSavedCC(mobileno, sitecode) {

    var url = apiServer + '/api/LLOTG';
    jQuery.support.cors = true;
    var JSONSendData = {
        Id: 1,
        Action: 4,
        MobileNo: mobileno,
        Sitecode: sitecode,
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('#selectocscccard').text('');
            $.each(feedback, function (key, value) {

                var cc = value.CardNumber.substring(0, 2) + ' ' + value.CardNumber.substring(2);
                $('#selectocscccard').append('<option value="' + value.CardNumber + '">XXXX XX' + cc + '</option>');
            });
        },
        error: function (feedback) {
            $("#TabProductLLOTGLoader").hide();
            $("#tblProductLLOTGContainer").html("").append("No package found").show();
        }
    });
};




/* ----------------------------------------------------------------
 * Function Name    : PayByExistingCard
 * Purpose          : LLOTG Pay By Existing Credit Card
 * Added by         : Rajesh Natarajan
 * Create Date      : Dec 22, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function PayByExistingCard(mobileno, sitecode, Brand, amount, CardNumber, CardCVV, paymode, LLOTGCurReg_Number, country, svc_id, mode) {

    var url = apiServer + '/api/LLOTG';
    jQuery.support.cors = true;
    var IpAdd = "";
    $.getJSON("http://jsonip.com?callback=?", function (data) {
        IpAdd = data.ip;
    });
    //var paymode = "";
    //if (LLOTGCurPayment_Type === "Pay By Credit") {
    //    var paymode = 2;
    //} else {
    //    var paymode = 1;
    //}
    jQuery.support.cors = true;
    var JSONSendData = {
        Id: 1,
        Action: 8,
        MobileNo: mobileno,
        Sitecode: sitecode,
        mode: mode,
        LoggedUser: Brand + ',' + amount + ',' + CardNumber + ',' + CardCVV + ',' + LLOTGCurReg_Number + ',' + country + ',' + paymode + ',' + svc_id
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (data) {          

            $('.PMSuccess .modal-footer .btnPMSuccessOKvalue').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOKvalue').attr({ "alt": "1" });
            if (data.errcode == 0) {
                $('.PMSuccess .modal-header').html(data.errsubject);
                $('.PMSuccess .modal-body').html(data.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
                ViewLLOTGList(mobileno, sitecode);
            }
            else {

                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
            //if (feedback === 0) {

            //    //$('#LLOTGResultInformation').modal({ keyboard: false, backdrop: 'static' }).css({
            //    //    'margin-left': function () {
            //    //        return window.pageXOffset - ($(this).width() / 2);
            //    //    }
            //    //});
            //    //ViewLLOTGList(mobileno, sitecode);
            //} else {

            //    //$('#LLOTGResultInformationfinal').text('Payment Failed');
            //    //$('#LLOTGResultInformation').modal({ keyboard: false, backdrop: 'static' }).css({
            //    //    'margin-left': function () {
            //    //        return window.pageXOffset - ($(this).width() / 2);
            //    //    }
            //    //});
            //    // ViewLLOTGList(mobileno, sitecode);
        },
        error: function (feedback) {
            $("#TabProductLLOTGLoader").hide();
            $("#tblProductLLOTGContainer").html("").append("No package found").show();
            return false;
        }
    });
    return true;
};


/* ----------------------------------------------------------------
 * Function Name    : validateNewCC
 * Purpose          : validate New Credit Card form
 * Added by         : Rajesh Natarajan
 * Create Date      : Dec 22, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function validateNewCC() {
    debugger;
    var isvalid = true;
    var csrdtype = $("input:radio[name=PaymentMethodCardType]").is(":checked");
    if (!csrdtype) {
        $('#PSCT').show();
        isvalid = false;
    } else {
        $('#PSCT').hide();

    }
    if ($('#PaymentMethodCCNumber').val() == "") {
        isvalid = false;
        $('#PECN').show();
    }
    else {
        $('#PECN').hide();
    }
    if ($('#PaymentMethodFirstName').val() == "") {
        isvalid = false;
        $('#PEFN').show();
    }
    else {
        $('#PEFN').hide();
    }
    if ($('#PaymentMethodLastName').val() == "") {
        isvalid = false;
        $('#PELN').show();
    }
    else {
        $('#PELN').hide();
    }
    if ($('#PaymentMethodExpiryMonth').val() == "0" && $('#PaymentMethodExpiryYear').val() == "0") {
        isvalid = false;
        $('#PEEMAY').show();
    }
    else {
        $('#PEEMAY').hide();
    }
    if ($('#PaymentMethodCardVerfCode').val() == "") {
        isvalid = false;
        $('#PECVC').show();
    }
    else {
        $('#PECVC').hide();
    }
    if ($('#iCSPBCAddress1').val() == "") {
        isvalid = false;
        $('#PEHN').show();
        $('#lblAddr1').show();        
    }
    else {
        $('#PEHN').hide();
        $('#lblAddr1').hide();
    }
    if ($('#iCSPBCAddress2').val() == "") {
        isvalid = false;
        $('#PEA').show();
        $('#lblAddr2').show();        
    }
    else {
        $('#PEA').hide();
        $('#lblAddr2').hide();
    }
    if ($('#iCSPBCAddress3').val() == "") {
        isvalid = false;
        $('#PEA').show();
        $('#lblAddr3').show();
    }
    else {
        $('#PEA').hide();
        $('#lblAddr3').hide();
    }
    if ($('#iCSPBCTown').val() == "") {
        isvalid = false;
        $('#PEC').show();
        $('#lblCity').show();
    }
    else {
        $('#PEC').hide();
        $('#lblCity').hide();        
    }
    if ($('#sCSPBCCountry').val() == "-1") {
        isvalid = false;
        $('#PECAddress').show();
        $('#lblCountry').show();        
    }
    else {
        $('#PECAddress').hide();
        $('#lblCountry').hide();        
    }
    if ($('#PaymentMethodPostcode').val() == "") {
        isvalid = false;
        $('#PEPC').show();
        $('#lblPostCode').show();        
    }
    else {
        $('#PEPC').hide();
        $('#lblPostCode').hide();
    }
    if ($('#iCSPBCEmail').val() != "") {
        var isemailvalid = validateEmail($('#iCSPBCEmail').val());
        if (!isemailvalid) {
            isvalid = false;
            $('#PEAVEA').show();
            $('#lblEmail').show();            
        } else {
            $('#PEAVEA').hide();
            $('#lblEmail').hide();
        }
    }
    else {
        $('#PEAVEA').show();
        $('#lblEmail').show();
    }
    //if ($('#PaymentMethodPhone').val() == "") {
    //    isvalid = false;
    //    $('#PEAVP').show();
    //}
    //else {
    //    $('#PEAVP').hide();
    //}
    return isvalid;
};


/* ----------------------------------------------------------------
 * Function Name    : clearnewcard
 * Purpose          : clear  new card data
 * Added by         : karthik
 * Create Date      : Dec 22, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function clearnewcard() {

    $("#PaymentMethodCCNumber").val('');
    $("#PaymentMethodFirstName").val('');
    $("#PaymentMethodLastName").val('');
    $("#PaymentMethodIssueNumber").val('');
    $("#PaymentMethodCardVerfCode").val('');
    $("#iCSPBCAddress1").val('');
    $("#iCSPBCAddress2").val('');
    $("#iCSPBCAddress3").val('');
    $("#iCSPBCTown").val('');
    $("#PaymentMethodPostcode").val('');
    $("#iCSPBCEmail").val('');
    $("#PaymentMethodPhone").val('');
    
    
    
    
    //$("#PaymentMethodHouseNo").val('');
    //$("#PaymentMethodAddress").val('');
    //$("#PaymentMethodCity").val('');
    //$("#PaymentMethodPostcode").val('');
    //$("#PaymentMethodEmail").val('');
    //$("#PaymentMethodPhone").val('');

};

/* ----------------------------------------------------------------
 * Function Name    : validateEmail
 * Purpose          : validate new cc Email
 * Added by         : Rajesh Natarajan
 * Create Date      : Dec 22, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function validateEmail(email) {

    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

/* ----------------------------------------------------------------
 * Function Name    : PayByNewCard
 * Purpose          : Pay By New credit Card
 * Added by         : Rajesh Natarajan
 * Create Date      : Dec 22, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function PayByNewCard(mobileno, sitecode, Brand, paymode, amount, Reg_number, svcid, data_currency, mode) {
    debugger;
    var url = apiServer + '/api/LLOTG';
    // jQuery.support.cors = true;
    //var paymode = "";
    //if (LLOTGCurPayment_Type === "Pay By Credit") {
    //    var paymode = 2;
    //} else {
    //    var paymode = 1;
    //}

    var JSONSendData = {
        Id: 1,
        Action: 9,
        MobileNo: mobileno,
        Sitecode: sitecode,
        mode: mode,
        LoggedUser:
         $("input:radio[name=PaymentMethodCardType]").val() + "^" +
        $('#PaymentMethodCCNumber').val() + "^" +
        $('#PaymentMethodFirstName').val() + "^" +
        $('#PaymentMethodLastName').val() + "^" +
        $('#PaymentMethodExpiryMonth').val() + "^" +
         $('#PaymentMethodExpiryYear').val() + "^" +
        $('#PaymentMethodCardVerfCode').val() + "^" +
          $('#iCSPBCAddress1').val() + "^" +
        //$('#PaymentMethodHouseNo').val() + "^" +
        $('#iCSPBCAddress2').val() + "^" +
        //$('#PaymentMethodAddress').val() + "^" +
          $('#iCSPBCTown').val() + "^" +          
        //$('#PaymentMethodCity').val() + "^" +
       // $('#PaymentMethodCountry').val() + "^" +
        $("#sCSPBCCountry").find("option:selected").text() + "^" +
        $('#PaymentMethodPostcode').val() + "^" +
        $('#iCSPBCEmail').val() + "^" +
        $('#PaymentMethodPhone').val() + "^" +
         $('#PaymentMethodIssueNumber').val() + "^" +
        amount + "^" +
                Brand + "^" +
                Reg_number + "^" +
                paymode + "^" +
                svcid + "^" +
                data_currency
    };

    $.ajax
({
    url: url,
    type: 'post',
    data: JSON.stringify(JSONSendData),
    dataType: 'json',
    contentType: "application/json;charset=utf-8",
    cache: false,
    success: function (data) {

        $('.PMSuccess .modal-footer .btnPMSuccessOKvalue').attr({ "alt": "1" });
        $('.PMFailed .modal-footer .btnPMFailedOKvalue').attr({ "alt": "1" });        
        if (data.errcode == 0) {

            $('.PMSuccess .modal-header').html(data.errsubject);
            $('.PMSuccess .modal-body').html(data.errmsg);
            $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });            
            ViewLLOTGList(mobileno, sitecode);
        }
        else {

            $('.PMFailed .modal-header').html(data.errsubject);
            $('.PMFailed .modal-body').html(data.errmsg);
            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }

        //if (feedback.errcode === 0) {
        //    $('#LLOTGResultInformation').modal({ keyboard: false, backdrop: 'static' }).css({
        //        'margin-left': function () {
        //            return window.pageXOffset - ($(this).width() / 2);
        //        }
        //    });

        //} else {

        //    $('#LLOTGResultInformationfinal').text('Payment Failed');
        //    $('#LLOTGResultInformation').modal({ keyboard: false, backdrop: 'static' }).css({
        //        'margin-left': function () {
        //            return window.pageXOffset - ($(this).width() / 2);
        //        }
        //    });
        //}

    }, error: function (feedback) {
        $("#TabProductLLOTGLoader").hide();
        $("#tblProductLLOTGContainer").html("").append("No package found").show();
        return false;
    }
});
    //  return true;
}
/* ----------------------------------------------------------------
 * Function Name    : RefreshLLOTGList
 * Purpose          : to refresh Landline On The Go List
 * Added by         : Edi Suryadi
 * Create Date      : january 28th, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function RefreshLLOTGList(MobileNo, Sitecode) {
    debugger;
    $("#TabProductLLOTGLoader").show();
    $("#tblProductLLOTG_wrapper").remove();
    $("#tblProductLLOTGContainer").append("<table class=\"table table-bordered table-hover\" id=\"tblProductLLOTG\"></table>");
    
    var url = apiServer + '/api/LLOTG';
    jQuery.support.cors = true;
    var JSONSendData = {
        Id: 1,
        Action: 1,
        MobileNo: MobileNo,
        Sitecode: Sitecode
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#TabProductLLOTGLoader").hide();
            var lastAccessedPage = parseInt($("#tblProductLLOTGContainer input[name=accessedPage]").val());
            $('#tblProductLLOTG').dataTable({
                bDestroy: true,
                aaData: feedback,
                iDisplayLength: 10,
                bPaginate: true,
                bFilter: false,
                bProcessing: true,
                bInfo: false,
                aaSorting: [],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No package found."
                },
                aoColumns: [
                    { mDataProp: "Country", sTitle: "Country" },
                    { mDataProp: "Country", sTitle: "City" },
                    { mDataProp: "Country", sTitle: "Number" },
                    { mDataProp: "Status_Description", sTitle: "Package ID" },
                    { mDataProp: "Country", sTitle: "Plan" },
                    { mDataProp: "Status_Description", sTitle: "Amount" },
                    {
                        mDataProp: "StartDate", sTitle: "Start Date",
                        fnRender: function (ob) {
                            return convertDateISO8601(ob.aData.StartDate, 3);
                        }
                    },
                    {
                        mDataProp: "ExpiredDate", sTitle: "Last Renewed Date",
                        fnRender: function (ob) {
                            return convertDateISO8601(ob.aData.ExpiredDate, 3);
                        }
                    },
                    {
                        mDataProp: "ExpiredDate", sTitle: "Renewal Date",
                        fnRender: function (ob) {
                            return convertDateISO8601(ob.aData.ExpiredDate, 3);
                        }
                    },

                    { mDataProp: "Status_Description", sTitle: "Status" },
                    { mDataProp: "PaymentType_Description", sTitle: "Payment Mode" },
                    { mDataProp: "PaymentType_Description", sTitle: "Last action by" }
                    //{
                    //    mDataProp: "errsubject", sTitle: "Action", sWidth: "280px",
                    //    fnRender: function (ob) {
                    //        var actionOption = "<select class=\"selectLLOTGAction\" data-country=\"" + ob.aData.Country + "\" data-regno=\"" + ob.aData.Reg_Number + "\" data-pt=\"" + ob.aData.Payment_Type + "\" data-ptd=\"" + ob.aData.PaymentType_Description + "\" >";
                    //        actionOption += '<option value="0" style=\"color:#c0c0c0\">--- Select an action ---</option>';

                    //        //if (ob.aData.Cancelable == true) {
                    //        //    actionOption += '<option value="1" style=\"padding:3px\">Stop</option>';
                    //        //}
                    //        //if (ob.aData.Renewable == true) {
                    //        //    actionOption += '<option value="2" style=\"padding:3px\">Renew</option>';
                    //        //}
                    //        //if (ob.aData.ChangePayment == true) {
                    //        //    actionOption += '<option value="3" style=\"padding:3px\">Change Payment</option>';
                    //        //}
                    //        actionOption += '<option value="1" style=\"padding:3px\">Cancel</option>';
                    //        actionOption += '<option value="2" style=\"padding:3px\">Reactivate</option>';
                    //        actionOption += '</select>';
                    //        return actionOption;
                    //    }
                    //}
                ],
                fnDrawCallback: function () {
                    $("#tblProductLLOTGContainer input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                },
                fnInitComplete: function (ob) {
                    $("#tblProductLLOTGContainer input[name=accessedPage]").val(this.fnPagingInfo().iPage);
                }
            });
            $("#tblProductLLOTG_wrapper .row-fluid:eq(0)").remove();
            $("#tblProductLLOTGContainer input[name=accessedPage]").val(lastAccessedPage);
            oTable.fnPageChange(lastAccessedPage);
        },
        error: function (feedback) {
            $("#TabProductLLOTGLoader").hide();
            $("#tblProductLLOTGContainer").html("").append("No package found").show();
        }
    });
}

$.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
    return {
        "iStart": oSettings._iDisplayStart,
        "iEnd": oSettings.fnDisplayEnd(),
        "iLength": oSettings._iDisplayLength,
        "iTotal": oSettings.fnRecordsTotal(),
        "iFilteredTotal": oSettings.fnRecordsDisplay(),
        "iPage": oSettings._iDisplayLength === -1 ?
			0 : Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
        "iTotalPages": oSettings._iDisplayLength === -1 ?
			0 : Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
    };
};

/* ----------------------------------------------------- 
   *  eof TAB LLOTG 
   ===================================================== */


