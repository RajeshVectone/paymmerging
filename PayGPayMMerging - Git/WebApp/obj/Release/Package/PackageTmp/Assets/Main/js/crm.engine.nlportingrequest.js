﻿/* ===================================================== 
   *  NL PORTING REQUEST
   ----------------------------------------------------- */

/* ----------------------------------------------------------------
 * Function Name    : NLPortINNewRequestSubmit
 * Purpose          : to submit the port in request
 * Added by         : Edi Suryadi
 * Create Date      : September 11th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function NLPortINNewRequestSubmit(p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19) {
    var url = apiServer + '/api/nlporting';

    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    jQuery.support.cors = true;
    var JSONSendData = {
        rsp: p1,
        rno: p2,
        dsp: p3,
        dno: p4,
        requestdate: p5,
        CustId: p6,
        Initials: p7,
        Prefix: p8,
        ContactName: p9,
        CompanyName: p10,
        Street: p11,
        Houseno: p12,
        HouseNoExt: p13, 
        Zipcode: p14,
        City: p15,
        Email: p16, 
        TelpNo: p17,
        Iccid: p18,
        Msisdn: p19, 
        //NoteOfMsg : "", // Hardcode from backend October 10th, 2013 - 13:01
        IsResent : "false",
        sitecode: "BNL",
        SubType: 2
    };

    //window.console.log(JSONSendData);

    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();

            var stat = data.errcode;
            if (stat == 0) {
                $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
                $('.PMSuccess .modal-header').html(data.errsubject);
                $('.PMSuccess .modal-body').html(data.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            } else {
                $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "2" });
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        },
        error: function (data) {
            $("#ajax-screen-masking").hide();
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
            $('.PMFailed .modal-header').html(data.errsubject);
            $('.PMFailed .modal-body').html(data.errmsg);
            $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : NLPortINNewRequestValidateInput
 * Purpose          : to validate input
 * Added by         : Edi Suryadi
 * Create Date      : September 24th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */

function NLPortINNewRequestValidateInput() {

    var passed = true;

    /* Validate Street */
    var Street = $("input[name=NLPortINRequest_iStreet]").val();
    if (Street == "") {
        passed = false;
    } 

    /* Validate House No */
    var regexHouseNo = /^[0-9]{1,5}$/;
    var HouseNo = $("input[name=NLPortINRequest_iHouseNumber]").val();
    if (regexHouseNo.test(HouseNo) == false || HouseNo == "") {
        $("span#err_NLPortINRequest_iHouseNumber").fadeIn();
        passed = false;
    } else {
        $("span#err_NLPortINRequest_iHouseNumber").hide();
    }

    /* Validate House No Ext */
    var regexHouseNoExt = /^[0-9]{1,4}$/;
    var HouseNoExt = $("input[name=NLPortINRequest_iHouseNumberExt]").val();
    if (regexHouseNoExt.test(HouseNoExt) == false && HouseNoExt != "") {
        $("span#err_NLPortINRequest_iHouseNumberExt").fadeIn();
        passed = false;
    } else {
        $("span#err_NLPortINRequest_iHouseNumberExt").hide();
    }

    /* Validate Zip Code */
    var regexZipCode = /^[1-9]\d{3}[A-Z]{2}$/;
    var ZipCode = $("input[name=NLPortINRequest_iZipCode]").val();
    if (regexZipCode.test(ZipCode) == false || ZipCode == "") {
        $("span#err_NLPortINRequest_iZipCode").fadeIn();
        passed = false;
    } else {
        $("span#err_NLPortINRequest_iZipCode").hide();
    }

    /* Validate City */
    var City = $("input[name=NLPortINRequest_iCity]").val();
    if (City == "") {
        passed = false;
    }

    /* Validate Email */
    var Email = $("input[name=NLPortINRequest_iEmail]").val();
    if (validateEmail(Email) == false && Email != "") {
        $("span#err_NLPortINRequest_iEmail").fadeIn();
        passed = false;
    } else {
        $("span#err_NLPortINRequest_iEmail").hide();
    }

    /* Validate Mobile Number 
    *  A dutch mobile phone number always contains 10 number, starting with 061, 062, 063, 064, 065 or 068
    */
    var regexMobileNumber = /^[0][6](?:[1-5]|[8])\d{7}$/;
    var MobileNumber = $("input[name=NLPortINRequest_iMobileNumber]").val();
    if (regexMobileNumber.test(MobileNumber) == false || MobileNumber == "") {
        $("span#err_NLPortINRequest_iMobileNumber").fadeIn();
        passed = false;
    } else {
        $("span#err_NLPortINRequest_iMobileNumber").hide();
    }

    /* Validate Vectone Number */
    var VectoneNumber = $("input[name=NLPortINRequest_iVectoneNumber]").val();
    if (VectoneNumber == "") {
        passed = false;
    }

    return passed;
}

/* ----------------------------------------------------------------
 * Function Name    : clearForm
 * Purpose          : to clear the form content
 * Added by         : Edi Suryadi
 * Create Date      : September 27th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function NLPortINNewRequestClearForm() {
    $("span.err_NLPortINRequest").hide();
    $("select[name=NLPortINRequest_sRecipientServiceProvider]").val("BMNL");
    $("select[name=NLPortINRequest_sRecipientNetworkOperator]").val("BMNL");
    $("select[name=NLPortINRequest_sDonorServiceProvider]").val("ACHT");
    $("select[name=NLPortINRequest_sDonorNetworkOperator]").val("ETMB");
    $("input[name=NLPortINRequest_iRequestDate]").val(dateFormat(new Date(AddNowDateWithFiveWorkingDays()), "dd/mm/yyyy hh:MM:ss")).change();
    $("select[name=NLPortINRequest_sCustomerType]").val("1");

    $("input[name=NLPortINRequest_iCustomerID]").val("");
    $("input[name=NLPortINRequest_iInitials]").val("");
    $("input[name=NLPortINRequest_iPrefix]").val("");
    $("input[name=NLPortINRequest_iLastname]").val("");
    $("input[name=NLPortINRequest_iCompanyName]").val("");
    $("input[name=NLPortINRequest_iStreet]").val("");
    $("input[name=NLPortINRequest_iHouseNumber]").val("");
    $("input[name=NLPortINRequest_iHouseNumberExt]").val("");
    $("input[name=NLPortINRequest_iZipCode]").val("");
    $("input[name=NLPortINRequest_iCity]").val("");
    $("input[name=NLPortINRequest_iEmail]").val("");
    $("input[name=NLPortINRequest_iMobileNumber]").val("");
    $("input[name=NLPortINRequest_iDonorICCID]").val("");
    $("input[name=NLPortINRequest_iVectoneNumber]").val("");

}

/* ----------------------------------------------------------------
 * Function Name    : NLPortINGetServiceProviderList
 * Purpose          : to get service provider list for combobox
 * Added by         : Edi Suryadi
 * Create Date      : Ocotber 01st, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function NLPortINGetServiceProviderList() {
    $("select[name=NLPortINRequest_sRecipientServiceProvider]").html("").attr("disabled", true);
    $("select[name=NLPortINRequest_sRecipientNetworkOperator]").html("").attr("disabled", true);
    $("select[name=NLPortINRequest_sDonorServiceProvider]").html("").attr("disabled", true);
    $("select[name=NLPortINRequest_sDonorNetworkOperator]").html("").attr("disabled", true);

    var url = apiServer + '/api/nlporting';
    jQuery.support.cors = true;
    var JSONSendData = {
        OptType: "SP", // SP = Service Provider ; NO = Network Operator
        ProviderCode: "", // null to get Service provider list.To get Network Operator,please provide provider code
        sitecode: "BNL",
        SubType: 100
    };
    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (data) {
            var optionContent = "";
            for (i = 0; i < data.length; i++) {
                optionContent += "<option value=\"" + data[i].Name + "\">" + data[i].FullName + "</option>";
            }
            $("select[name=NLPortINRequest_sRecipientServiceProvider]").append(optionContent);
            $("select[name=NLPortINRequest_sRecipientServiceProvider] option[value=BMNL]").attr("selected", true);
            
            $("select[name=NLPortINRequest_sDonorServiceProvider]").append(optionContent);
            $("select[name=NLPortINRequest_sDonorServiceProvider] option[value=ACHT]").attr("selected", true);
        },
        complete: function (data) {
            $("select[name=NLPortINRequest_sDonorServiceProvider]").removeAttr("disabled");
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : NLPortINGetNetworkOperatorList
 * Purpose          : to get network operator list for combobox
 * Added by         : Edi Suryadi
 * Create Date      : Ocotber 01st, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function NLPortINGetNetworkOperatorList(ServiceProviderType, isNewLoad) {
    var ProviderCode = "";
    if (ServiceProviderType == "RSP") {
        $("select[name=NLPortINRequest_sRecipientNetworkOperator]").html("").attr("disabled", true);
        $("img#NLPortINRequest_sRecipientNetworkOperator_Loader").show();
        if (isNewLoad) {
            ProviderCode = "BMNL";
        } else {
            ProviderCode = $("select[name=NLPortINRequest_sRecipientServiceProvider] option:selected").val();
        }
    } else if (ServiceProviderType == "DSP") {
        $("select[name=NLPortINRequest_sDonorNetworkOperator]").html("").attr("disabled", true);
        $("img#NLPortINRequest_sDonorNetworkOperator_Loader").show();
        if (isNewLoad) {
            ProviderCode = "ACHT";
        } else {
            ProviderCode = $("select[name=NLPortINRequest_sDonorServiceProvider] option:selected").val();
        }
    }
    var url = apiServer + '/api/nlporting';
    jQuery.support.cors = true;
    var JSONSendData = {
        OptType: "NO", // SP = Service Provider ; NO = Network Operator
        ProviderCode: ProviderCode, // null to get Service provider list.To get Network Operator,please provide provider code
        sitecode: "BNL",
        SubType: 100
    };
    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (data) {
            var optionContent = "";

            for (i = 0; i < data.length; i++) {
                optionContent += "<option value=\"" + data[i].Name + "\">" + data[i].FullName + "</option>";
            }

            if (ServiceProviderType == "RSP") {
                $("select[name=NLPortINRequest_sRecipientNetworkOperator]").append(optionContent);
                $("img#NLPortINRequest_sRecipientNetworkOperator_Loader").hide();
            } else if (ServiceProviderType == "DSP") {
                $("select[name=NLPortINRequest_sDonorNetworkOperator]").append(optionContent);
                $("img#NLPortINRequest_sDonorNetworkOperator_Loader").hide();
            }

        },
        complete: function (data) {
            if (ServiceProviderType == "DSP") {
                $("select[name=NLPortINRequest_sDonorNetworkOperator]").removeAttr("disabled");
            }
        }
    });
}
/* ----------------------------------------------------- 
   *  eof NL PORTING REQUEST
   ===================================================== */






