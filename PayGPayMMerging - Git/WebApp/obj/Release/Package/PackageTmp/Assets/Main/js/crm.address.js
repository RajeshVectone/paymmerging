﻿(function ($) {
    function potong(str) {
        if (!str || typeof str != 'string')
            return null;
        return str.replace(/^[\s]+/, '').replace(/[\s]+$/, '').replace(/[\s]{2,}/, ' ');
    }

    function AddressListBegin(key, username, objPostcode, objErrorMessage) {
        var _postcode = $.trim($(objPostcode).val());
        $(objErrorMessage).html("").hide();

        var strUrl = "";
        // Build the url
        strUrl = "https://services.postcodeanywhere.co.uk/PostcodeAnywhere/Interactive/FindByPostcode/v1.00/json.ws?";
        strUrl += "&Key=" + escape(key);
        strUrl += "&Postcode=" + escape(_postcode);
        strUrl += "&UserName=" + username.toString();
        strUrl += "&CallbackFunction=AddressListEnd";
        // Make the request
        if ($("#pcascript").length) try { $('head').remove("#pcascript") } catch (e) { }
        var scriptTag = "<script type=\"text/javascript\" src=\"" + strUrl + "\" id=\"pcascript\"></script>"
        $('head').append(scriptTag);
    }

    function AddressListEnd(response) {
        if (response.length == 1 && typeof (response[0].Error) != 'undefined') {
            $("#trAddressList").show();
            $("#lstAddressIP").hide();
            $("#errListAddressIP").html(response[0].Description).show();
        }
        else {
            if (response.length == 0) {
                $("#trAddressList").show();
                $("#lstAddressIP").hide();
                $("#errListAddressIP").html("Sorry, no matching items found").show();
            }
            else {

                $("#errListAddressIP").html("").hide();
                var lstAddressIP = document.getElementById("lstAddressIP");
                $("#lstAddressIP").html("");
                $("#lstAddressIP").append("<option value=\"\">- Select Address -</option>");
                var optAddress = "";
                for (var i in response) {
                    optAddress += "<option value=\"" + response[i].Id + "\">" + response[i].StreetAddress + "</option>";
                }
                $("#lstAddressIP").append(optAddress);
                $("#lstAddressIP").live("click", function () {
                    SelectAddress('KG22-JN94-EF64-MW54', $(this).val());
                });

                $("#lstAddressIP").show();
                $("#trAddressList").show();
            }
        }
        $("img#btnFindLoader").hide();
        $("input#btnFind").show();
    }

    function SelectAddress(Key, Id) {
        if (!Id) return;

        var strUrl = "";
        // Build the url
        strUrl = "https://services.postcodeanywhere.co.uk/PostcodeAnywhere/Interactive/RetrieveById/v1.00/json.ws?";
        strUrl += "&Key=" + escape(Key);
        strUrl += "&Id=" + escape(Id);
        strUrl += "&UserName='SWITC11123'";
        strUrl += "&CallbackFunction=SelectAddressEnd";
        // Make the request
        if ($("#pcascript").length) try { $('head').remove("#pcascript") } catch (e) { }
        var scriptTag = "<script type=\"text/javascript\" src=\"" + strUrl + "\" id=\"pcascript\"></script>"
        $('head').append(scriptTag);

        // User interface changed
        $("input#btnFind").val('Change');
        $("input#post_code").attr("readonly", true);
        $("input#house_number").attr("readonly", false);
        $("input#address").attr("readonly", false);
        $("input#city").attr("readonly", false);
    }

    function SelectAddressEnd(response) {
        if (response.length == 1 && typeof (response[0].Error) != 'undefined') {
            $("#trAddressList").show();
            $("#lstAddressIP").hide();
            $("#errListAddressIP").html(response[0].Description).show();
        }
        else {
            if (response.length == 0) {
                $("#trAddressList").show();
                $("#lstAddressIP").hide();
                $("#errListAddressIP").html("Sorry, no matching items found").show();
            }
            else {
                $("#lstAddressIP").hide();
                $("#trAddressList").hide();

                var hno = response[0].Line1.split(" ");
                var street = "";
                $("input#house_number").val(potong(hno[0]));

                for (i = 1; i < hno.length; i++) {
                    street += hno[i] + " ";
                }
                street = street + " " + response[0].Line2 + " " + response[0].Line3 + response[0].Line4 + " " + response[0].Line5;

                $("input#address").val(potong(street));
                $("input#city").val(response[0].PostTown);
            }
        }
    }
});