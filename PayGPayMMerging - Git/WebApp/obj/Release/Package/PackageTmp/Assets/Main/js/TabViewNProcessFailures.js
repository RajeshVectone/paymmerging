﻿function FetchData(DDYear, DDMonth, DDStatus, CustomerId, PaymtStatus, MobileNo, ConnStatus, PaymentRef, PayMode, SiteCode) {
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    $("#ajax-screen-masking").show();
    var row = 0;
    $("#tblDDOperationContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblDDOperation\"></table>");
    //$("#tblDDOperation").css({ "min-height": "100px", "overflow-y": "scroll", "overflow-x": "hidden", "background-image": "url('')" });
    $("#tblDDOperationContainer").css({ "min-height": "100px", "overflow-y": "scroll", "overflow-x": "hidden", "background-image": "url('')" });
    //$("#tblDDOperationContainer").css({  "overflow-y": "scroll", "overflow-x": "hidden", "background-image": "url('')" });
    jQuery.support.cors = true;
    var url = apiServer + "/api/ddviewforprocessfailure";
    var JSONSendData = {
            ddYear: DDYear,
            ddMonth: DDMonth,
            ddStatus: DDStatus,
            ddCustID:CustomerId,
            ddPaymtStatus:PaymtStatus,
            ddMobileNo: MobileNo,
            ddConnStatus:ConnStatus,
            ddPaymentRef: PaymentRef,
            ddPayMode: PayMode,
            sitecode: SiteCode
    };
   
    $.ajax({
        url: url,
        type: 'get',
        data: JSONSendData,
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#tblDDOperationContainer").hide();

        },
        success: function (feedback) {
            $('#ddButtonGrps').css('display', 'block');
            $('#lblRows').css('display', 'block');
            $('#tblDDOperation').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                iDisplayLength: 50,
                bPaginate: false,
                bFilter: false,
                bInfo: false,
                aaSorting: [[2, "asc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No call PAGM found."
                },

                aoColumns: [
                    {
                        sTitle: "<input id='chkSelectAll' class='checkAll' type='checkbox' onclick='chekedValues();'></input>", sDefaultContent: "", "bSortable": false,
                        fnRender: function (ob) {
                            row = row + 1;
                            return "<input type='checkbox' class='value' id='chkChild_" + row + "' ></input>";
                        }
                    },
                        { mDataProp: "Customer_ID", sTitle: "Customer ID" },
                        { mDataProp: "Subscription_Id", sTitle: "Subscription ID" },
                        { mDataProp: "MobileNo", sTitle: "Mobile Number" },
                        { mDataProp: "DD_Status", sTitle: "DD Status" },
                        { mDataProp: "Amount", sTitle: "Amount" },
                        { mDataProp: "pay_reference", sTitle: "Payment Reference" },
                        { mDataProp: "PaymentStatus", sTitle: "Payment Status" },
                        { mDataProp: "CustomerName", sTitle: "Customer Name" },
                        { mDataProp: "Connection_status", sTitle: "Connection Status" },
                        { mDataProp: "Payment_Attempts", sTitle: "Pay Attempts" },
                        { mDataProp: "Processing_Date", sTitle: "Processing Date" },
                        { mDataProp: "Payment_Mode", sTitle: "Pay Mode" }
                ],
                fnDrawCallback: function () {
                    if (row <= 0) {
                        $("#tblDDOperation tbody td").css({ "font-size": "11px", 'height': '0px', 'overflow': 'inherit', 'position': 'static' });
                        //$("#tblDDOperation thead tr").css({ "font-size": "11px"});
                    }
                    else {
                        $("#tblDDOperation tbody td").css({ "font-size": "12px" });
                    }
                },
            });

            $("#ajax-screen-masking").hide();
            $("#tblDDOperationContainer").show();
            var rows = $("#tblDDOperation").dataTable().fnGetNodes();
            var vText = document.getElementById("lblRows");
            vText.innerHTML = "The Number of Rows - " + rows.length;

        },
        error: function (feedback) {
            //$("#chFilterResultBody").css({ "background-image": "none" }).html("").append("No call history found.").show();
            $("#ajax-screen-masking").hide();
            $('#lblRows').css('display', 'none');
        }
    });
    row = 0;
}


function chekedValues() {
    var rows = $("#tblDDOperation").dataTable().fnGetNodes();
    var checked1 = $('#chkSelectAll').prop('checked');
        //$('.value').prop('checked', true);
        for (var i = 0; i < rows.length; i++) {
            $('#chkChild_' + (i + 1)).prop('checked', checked1);
        }
    if(checked1 == true)
        DownloadAll = 'Y';
        else
        DownloadAll = 'N';
}
function DownloadDDFromFilterData(DDYear, DDMonth, DDStatus, CustomerId, PaymtStatus, MobileNo, ConnStatus, PaymentRef, PayMode, ALL,SiteCode)
{
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    $("#ajax-screen-masking").show();

    var url = '/Download/DDViewForProcessFailure';

    jQuery.support.cors = true;
    var JSONSendData = {
        ddYear: DDYear,
        ddMonth: DDMonth,
        ddStatus: DDStatus,
        ddCustID:CustomerId,
        ddPaymtStatus:PaymtStatus,
        ddMobileNo: MobileNo,
        ddConnStatus:ConnStatus,
        ddPaymentRef: PaymentRef,
        ddPayMode: PayMode,
        ALL: ALL,
        sitecode:SiteCode
    };

    $.ajax({
        url: url,
        type: 'get',
        data: JSONSendData,
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            //$("#ajax-screen-masking").show();
        },
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            if (feedback.errcode == 0) {
                var IFrameData = $('<iframe/>', { id: 'downloadIFrame', src: '/Download/GoDownLoadVPFData?id=' + feedback.Text, style: 'display:none' });
                $("#ajax-screen-masking").append(IFrameData);
            }
            $("#ajax-screen-masking").hide();

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("File failed to generated due to Error ");// + xhr.status);
            $("#ajax-screen-masking").hide();
        }
    });
}

function SuspendService(uploadedBy, customerIds,SiteCode){
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    $("#ajax-screen-masking").show();
    var url = apiServer + "/api/suspendservice";
    jQuery.support.cors = true;
    var JSONSendData = {
        UpdatedBy:uploadedBy,
        CustIDs: customerIds,
        sitecode:Sitecode

    };
    $.ajax({
        url: url,
        type: 'get',
        data: JSONSendData,
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            $('#filesName').html("Suspend Service Operation Result : \r\n" + feedback);
            $('#FileGenrated').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                },
            });
           // window.location = "/PMBO/TabViewNProcessFailures";
        },
        error: function (feedback) {
            $("#chFilterResultBody").css({ "background-image": "none" }).html("").append("Api Error: Suspend Service was not successful.").show();
            $("#ajax-screen-masking").hide();
            $('#filesName').html("Suspend Service Failed ! " );
            $('#FileGenrated').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                },
            });
        }
    });
}

//Modified by BSK for Improvement : Jira CRMT-141
function SuspendServiceNew(uploadedBy, customerIds,SiteCode) {
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    $("#ajax-screen-masking").show();
    var url = apiServer + "/api/suspendservice";
    jQuery.support.cors = true;
    var JSONSendData = {
        UpdatedBy: uploadedBy,
        CustIDs: customerIds,
        sitecode:SiteCode
    };
    $.ajax({
        url: url,
        type: 'Post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        //data: JSONSendData,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            $('#filesName').html("Suspend Service Operation Result : \r\n" + feedback);
            $('#FileGenrated').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                },
            });
            // window.location = "/PMBO/TabViewNProcessFailures";
        },
        error: function (feedback) {
            $("#chFilterResultBody").css({ "background-image": "none" }).html("").append("Api Error: Suspend Service was not successful.").show();
            $("#ajax-screen-masking").hide();
            $('#filesName').html("Suspend Service Failed ! ");
            $('#FileGenrated').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                },
            });
        }
    });
}

function ActivateService(uploadedBy, customerIds) {
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    $("#ajax-screen-masking").show();
    var url = apiServer + "/api/activateservice";
    jQuery.support.cors = true;
    var JSONSendData = {
        UpdatedBy: uploadedBy,
        CustIDs: customerIds
    };
    $.ajax({
        url: url,
        type: 'get',
        data: JSONSendData,
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            $('#filesName').html("Activate Service Operation Result : \r\n" + feedback);
            $('#FileGenrated').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                },
                
            });
           // window.location = "/PMBO/TabViewNProcessFailures";
        },
        error: function (feedback) {
            $("#chFilterResultBody").css({ "background-image": "none" }).html("").append("Api Error: Activate Service was not successful.").show();
            $("#ajax-screen-masking").hide();
            $('#filesName').html("Activate Service Failed ! ");
            $('#FileGenrated').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                },
            });

        }
    });
}

//Modified by BSK for Improvement : Jira CRMT-141
function ActivateServiceNew(uploadedBy, customerIds ,SiteCode) {
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    $("#ajax-screen-masking").show();
    var url = apiServer + "/api/activateservice";
    jQuery.support.cors = true;
    var JSONSendData = {
        UpdatedBy: uploadedBy,
        CustIDs: customerIds,
        sitecode:SiteCode
    };
    $.ajax({
        url: url,
        type: 'Post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        //data: JSONSendData,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            $('#filesName').html("Activate Service Operation Result : \r\n" + feedback);
            $('#FileGenrated').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                },

            });
            // window.location = "/PMBO/TabViewNProcessFailures";
        },
        error: function (feedback) {
            $("#chFilterResultBody").css({ "background-image": "none" }).html("").append("Api Error: Activate Service was not successful.").show();
            $("#ajax-screen-masking").hide();
            $('#filesName').html("Activate Service Failed ! ");
            $('#FileGenrated').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                },
            });

        }
    });
}

//function ProcessPaymentCC(uploadBy, customerIds) {
function ProcessPaymentCC(customerIds,SiteCode) {
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    $("#ajax-screen-masking").show();
    var url = apiServer + "/api/processpaymentcc_";
    jQuery.support.cors = true;
    var JSONSendData = {
        //UpdatedBy: uploadedBy,
        CustomerIds: customerIds,
        sitecode :SiteCode
    };
    $.ajax({
        url: url,
        type: 'get',
        data: JSONSendData,
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            $('#filesName').html("CC Process Payment  have been queued for " + feedback);
            $('#FileGenrated').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
         //   window.location = "/PMBO/TabViewNProcessFailures";
        },
            error: function (feedback) {
                $("#chFilterResultBody").css({ "background-image": "none" }).html("").append("Api Error: Process Payment with CC was not successful.").show();
                $("#ajax-screen-masking").hide();
                $('#filesName').html("Process Payment for CC Failed ! ");
                $('#FileGenrated').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    },
                });

            }
    });
}


//success: function (feedback) {
//    $('#ddButtonGrps').css('display', 'block');
//    $("#tblDDOperationContainer").css({ "background-image": "none" });
//    $('#tblDDOperation').dataTable({
//        bDestroy: true,
//        "bScrollInfinite": true,
//        "bScrollCollapse": false,
//        "sScrollY": "300px",
//        "scrollX": true,
//        bRetrieve: true,
//        aaData: feedback,
//        "bAutoWidth": true,
//        bPaginate: true,
//        bFilter: false,
//        bInfo: false,
//        iDisplayLength: 50,
//        aaSorting: [[1, "desc"]],
//        oLanguage: {
//            "sInfoEmpty": '',
//            "sEmptyTable": "No data found for the filters selected."
//        },

//        aoColumns: [
//            {
//                sTitle: "<input id='chkSelectAll' class='checkAll' type='checkbox' onclick='chekedValues();'></input>", sDefaultContent: "", "bSortable": false,
//                fnRender: function (ob) {
//                    row = row + 1;
//                    return "<input type='checkbox' class='value' id='chkChild_" + row + "' ></input>";
//                }
//            },
//                { mDataProp: "Customer_ID", sTitle: "Customer ID" },
//                { mDataProp: "Subscription_Id", sTitle: "Subscription Id" },
//                { mDataProp: "MobileNo", sTitle: "MobileNumber" },
//                { mDataProp: "DD_Status", sTitle: "DD Status" },
//                { mDataProp: "Amount", sTitle: "Amount" },
//                { mDataProp: "pay_reference", sTitle: "Payment Reference" },
//                { mDataProp: "PaymentStatus", sTitle: "Payment Status" },
//                { mDataProp: "CustomerName", sTitle: "Customer Name" },
//                { mDataProp: "Connection_status", sTitle: "Connection Status" },
//                { mDataProp: "Payment_Attempts", sTitle: "Payment Attempts" },
//                { mDataProp: "Processing_Date", sTitle: "Processing Date" },
//                { mDataProp: "Payment_Mode", sTitle: "Payment Mode" }
//        ],
//        fnDrawCallback: function () {
//            if (row <= 0) {
//                $("#tblDDOperation tbody").css({ "font-size": "12px", 'height': '0px', 'overflow': 'inherit', 'position': 'static' });
//            }
//            else {
//                $("#tblDDOperation tbody td").css({ "font-size": "12px" });
//            }
//        },
//        "fnInitComplete": function () {
//            this.fnAdjustColumnSizing(true);
//        }
//    });

//    $("#ajax-screen-masking").hide();
//    $("#tblDDOperationContainer").show();
//    $("#tblDDOperation tbody td").css({ "font-size": "12px" });
//},


//success: function (feedback) {
//    $('#ddButtonGrps').css('display', 'block');
//    $("#tblDDOperationContainer").css({ "background-image": "none" });
//    $('#tblDDOperation').dataTable({
//        bDestroy: true,
//        bRetrieve: true,
//        aaData: feedback,
//        iDisplayLength: 50,
//        //"bAutoWidth": true,
//        bPaginate: true,
//        bFilter: false,
//        bInfo: false,
//        aaSorting: [[1, "desc"]],
//        oLanguage: {
//            "sInfoEmpty": '',
//            "sEmptyTable": "No data found for the filters selected."
//        },

//        aoColumns: [
//            {
//                sTitle: "<input id='chkSelectAll' class='checkAll' type='checkbox' onclick='chekedValues();'></input>", sDefaultContent: "", "bSortable": false,
//                fnRender: function (ob) {
//                    row = row + 1;
//                    return "<input type='checkbox' class='value' id='chkChild_" + row + "' ></input>";
//                }
//            },
//                { mDataProp: "Customer_ID", sTitle: "Customer ID" },
//                { mDataProp: "Subscription_Id", sTitle: "Subscription Id" },
//                { mDataProp: "MobileNo", sTitle: "MobileNumber" },
//                { mDataProp: "DD_Status", sTitle: "DD Status" },
//                { mDataProp: "Amount", sTitle: "Amount" },
//                { mDataProp: "pay_reference", sTitle: "Payment Reference" },
//                { mDataProp: "PaymentStatus", sTitle: "Payment Status" },
//                { mDataProp: "CustomerName", sTitle: "Customer Name" },
//                { mDataProp: "Connection_status", sTitle: "Connection Status" },
//                { mDataProp: "Payment_Attempts", sTitle: "Payment Attempts" },
//                { mDataProp: "Processing_Date", sTitle: "Processing Date" },
//                { mDataProp: "Payment_Mode", sTitle: "Payment Mode" }
//        ],
//        fnDrawCallback: function () {
//            if (row <= 0) {
//                $("#tblDDOperation tbody").css({ "font-size": "12px", 'height': '0px', 'overflow': 'inherit', 'position': 'static' });
//            }
//            else {
//                $("#tblDDOperation tbody td").css({ "font-size": "12px" });
//            }
//        }
//        //,
//        //"fnInitComplete": function () {
//        //    this.fnAdjustColumnSizing(true);
//        //}
//    });

//    $("#ajax-screen-masking").hide();
//    $("#tblDDOperationContainer").show();
//    $("#tblDDOperation tbody td").css({ "font-size": "12px" });
//},


//function FetchData(DDYear, DDMonth, DDStatus, CustomerId, PaymtStatus, MobileNo, ConnStatus, PaymentRef, PayMode) {
//    var ajaxSpinnerTop = $(window).outerHeight() / 2;
//    $("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
//    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
//    $("#ajax-screen-masking").show();
//    $("#tblDDOperationContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblDDOperation\"></table>");
//    $("#tblDDOperationContainer").css({ "min-height": "100px", "overflow-y": "hidden", "overflow-x": "scroll", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
//    var row = 0;
//    var url = apiServer + "/api/ddviewforprocessfailure";
//    var JSONSendData = {
//        ddYear: DDYear,
//        ddMonth: DDMonth,
//        ddStatus: DDStatus,
//        ddCustID: CustomerId,
//        ddPaymtStatus: PaymtStatus,
//        ddMobileNo: MobileNo,
//        ddConnStatus: ConnStatus,
//        ddPaymentRef: PaymentRef,
//        ddPayMode: PayMode
//    };

//    $.ajax({
//        url: url,
//        type: 'get',
//        data: JSONSendData,
//        dataType: 'json',
//        contentType: "application/json;charset=utf-8",
//        cache: false,
//        beforeSend: function () {
//            $("#tblDDOperationContainer").hide();

//        },
//        success: function (feedback) {
//            $('#ddButtonGrps').css('display', 'block');
//            $("#tblDDOperationContainer").css({ "background-image": "none" });
//            $('#tblDDOperation').dataTable({
//                bDestroy: true,
//                "bScrollInfinite": true,
//                "bScrollCollapse": false,
//                "sScrollY": "300px",
//                "scrollX": true,
//                bRetrieve: true,
//                aaData: feedback,
//                bPaginate: false,
//                bFilter: false,
//                bInfo: false,
//                iDisplayLength: 50,
//                aaSorting: [[1, "desc"]],
//                oLanguage: {
//                    "sInfoEmpty": '',
//                    "sEmptyTable": "No data found for the filters selected."
//                },

//                aoColumns: [
//                    {
//                        sTitle: "<input id='chkSelectAll' class='checkAll' type='checkbox' onclick='chekedValues();'></input>", sDefaultContent: "", "bSortable": false,
//                        fnRender: function (ob) {
//                            row = row + 1;
//                            return "<input type='checkbox' class='value' id='chkChild_" + row + "' ></input>";
//                        }
//                    },
//                        { mDataProp: "Customer_ID", sTitle: "Customer ID" },
//                        { mDataProp: "Subscription_Id", sTitle: "Subscription Id" },
//                        { mDataProp: "MobileNo", sTitle: "MobileNumber" },
//                        { mDataProp: "DD_Status", sTitle: "DD Status" },
//                        { mDataProp: "Amount", sTitle: "Amount" },
//                        { mDataProp: "pay_reference", sTitle: "Payment Reference" },
//                        { mDataProp: "PaymentStatus", sTitle: "Payment Status" },
//                        { mDataProp: "CustomerName", sTitle: "Customer Name" },
//                        { mDataProp: "Connection_status", sTitle: "Connection Status" },
//                        { mDataProp: "Payment_Attempts", sTitle: "Payment Attempts" },
//                        { mDataProp: "Processing_Date", sTitle: "Processing Date" },
//                        { mDataProp: "Payment_Mode", sTitle: "Payment Mode" }
//                ],
//                fnDrawCallback: function () {
//                    if (row <= 0) {
//                        $("#tblDDOperation tbody").css({ "font-size": "12px", 'height': '0px', 'overflow': 'inherit', 'position': 'static' });
//                    }
//                    else {
//                        $("#tblDDOperation tbody td").css({ "font-size": "12px" });
//                    }
//                },
//                //"fnInitComplete": function () {
//                //    this.fnAdjustColumnSizing(true);
//                //}
//            });

//            $("#ajax-screen-masking").hide();
//            $("#tblDDOperationContainer").show();
//            $("#tblDDOperation tbody td").css({ "font-size": "12px" });
//        },
//        error: function (feedback) {
//            //$("#chFilterResultBody").css({ "background-image": "none" }).html("").append("No call history found.").show();
//            $("#ajax-screen-masking").hide();
//        }
//    });
//    row = 0;
//}