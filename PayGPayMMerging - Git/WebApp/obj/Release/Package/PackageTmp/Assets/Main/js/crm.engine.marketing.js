﻿function getpaymentprofiledownloadMarketing(paymentMode) {
    if (paymentMode == "0") {
        paymentMode = '';
    }
    var i = 0;
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    var url = '/Download/DownloadCreateTransactionMarketing'
    var JSONSendData = {
        paymentMode: paymentMode
    };
    $.ajax
    ({
        url: url,
        type: 'get',
        dataType: 'json',
        data: JSONSendData,
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#tblDDOperationContainer").hide();
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {
            if (feedback.errcode == 0) {
                $("#ajax-screen-masking").hide();
                $("#tblDDOperationContainer").show();
                var IFrameData = $('<iframe/>', { id: 'downloadIFrame', src: '/Download/DownloadViewTransactionMarketing?id=' + feedback.Text, style: 'display:none' });
                $("#ajax-screen-masking").append(IFrameData);
            }
        }
    });
}
function getpaymentprofiledownloadMarketing_v2(val1, val2, val3, val4, val5, val6) {
    var i = 0;
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    var url = '/Download/DownloadTransactionMarketing_v1'
    var JSONSendData = {
        product: val1,
        type: val2,
        fromdate: val3,
        todate: val4,
        loggedin: val5,
        toppedup: val6

    };
    $.ajax
    ({
        url: url,
        type: 'get',
        dataType: 'json',
        data: JSONSendData,
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#tblDDOperationContainer").hide();
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {
            if (feedback.errcode == 0) {
                $("#ajax-screen-masking").hide();
                $("#tblDDOperationContainer").show();
                var IFrameData = $('<iframe/>', { id: 'downloadIFrame', src: '/Download/DownloadViewTransactionMarketing_v2?id=' + feedback.Text, style: 'display:none' });
                $("#ajax-screen-masking").append(IFrameData);
            }
        }
    });
}

function DownloadgocardDDDtls(mode, sitecode) {
    
    var i = 0;
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    var url = '/Download/DownloadGocardReport'
    var JSONSendData = {
        Mode: mode,
        Sitecode: sitecode
    };
    $.ajax
    ({
        url: url,
        type: 'get',
        dataType: 'json',
        data: JSONSendData,
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#tblDDOperationContainer").hide();
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {
            if (feedback.errcode == 0) {
                $("#ajax-screen-masking").hide();
                $("#tblDDOperationContainer").show();
                var IFrameData = $('<iframe/>', { id: 'downloadIFrame', src: '/Download/DownloadViewTransactionMarketing?id=' + feedback.Text, style: 'display:none' });
                $("#ajax-screen-masking").append(IFrameData);
            }
        }
    });
}