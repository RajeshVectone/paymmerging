﻿/* ===================================================== 
   *  TAB BUNDLES 
   ----------------------------------------------------- */

/* ----------------------------------------------------------------
 * Function Name    : SubscribedBundles
 * Purpose          : to show subscribed bundles
 * Added by         : Edi Suryadi
 * Create Date      : September 05th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function SubscribedBundles(Msisdn, Iccid, Sitecode, balance, tariffclass, user_role) {

    $("#pbSubscribedBundlesBody").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblPBSubscribedBundles\"></table>");
    $("#pbSubscribedBundlesBody").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });

    var url = apiServer + '/api/bundle/';
    jQuery.support.cors = true;
    var JSONSendData = {
        Msisdn: Msisdn,
        Iccid: Iccid,
        Bundle_Type: 2,
        Sitecode: Sitecode,
        balance: balance,
        tariffclass: tariffclass,
        ProductCode: $('.clsproductcode')[0].innerHTML
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#pbSubscribedBundlesBody").css({ "background-image": "none" });
            $('#tblPBSubscribedBundles').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: true,
                bInfo: false,
                iDisplayLength: 10,
                sScrollX: "100%",
                bScrollCollapse: true,
                aaSorting: [],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Data Available."
                },
                aoColumns: [
                    { mDataProp: "bundleid", sTitle: "Bundle ID", sWidth: "50px" },
                    //{ mDataProp: "name", sTitle: "Bundle Name", sWidth: "120px" },
                    {
                        mDataProp: "actionlink", sTitle: "Bundle Name", sWidth: "120px",
                        fnRender: function (ob) {
                            if (ob.aData.is_limited_min_flag == 1 && ob.aData.config_id > 0) {
                                //CRM Enhancement 2
                                return "<a href=\"javascript:void(0)\" " + "\" ctrlValue1=\"" + ob.aData.subscription_date + "\" ctrlValue2=\"" + ob.aData.international_tariff + "\" style=\"text-decoration: underline;color:blue;\" class=\"orderIDAnchor\" param-data=\"" + ob.aData.config_id + "\" bundle-data=\"" + ob.aData.bundleid + "\">" + ob.aData.name + "</a>";
                            }
                            else
                                return ob.aData.name;
                        }
                    },
                    { mDataProp: "price", sTitle: "Price", sWidth: "50px" },
                    { mDataProp: "renewal_mode", sTitle: "Renewal Mode", sWidth: "60px" },
                    { mDataProp: "auto_renewal_status", sTitle: "Auto Renewal Status", sWidth: "60px" },
                    { mDataProp: "renewal_delay", sTitle: "Renewal Period", sWidth: "80px" },
                    { mDataProp: "subscriptiondate_string", sTitle: "Subscription Date", sWidth: "100px" },
                    { mDataProp: "renewaldate_string", sTitle: "Renewal Date", sWidth: "100px" },
                    { mDataProp: "sendmsg_mode", sTitle: "Notification Mode", sWidth: "95px" },
                    { mDataProp: "bundle_type", sTitle: "Bundle Type", sWidth: "70px" },
                    { mDataProp: "Status", sTitle: "Status", sWidth: "45px" },
                    { mDataProp: "payment_reference", sTitle: "Payment Ref.", sWidth: "150px" },
                    { mDataProp: "purchased_by", sTitle: "Purchased By", sWidth: "150px" },
                    {
                        mDataProp: "assigned_allowance", sTitle: "Bundle Allowance", sWidth: "120px",
                        fnRender: function (ob) {
                            if (ob.aData.assigned_allowance != null) {
                                var arr = ob.aData.assigned_allowance.split('|');
                                var bundleAllowance = "";
                                $.each(arr, function (i) {
                                    bundleAllowance += "<span style=\"font-size:12px\">" + arr[i] + "</span><br>";
                                });
                                //CRM Enhancement 2
                                if (ob.aData.is_limited_min_flag == 1 && ob.aData.config_id > 0) {
                                    return "<a href=\"javascript:void(0)\" " + "\" ctrlValue1=\"" + ob.aData.subscription_date + "\" ctrlValue2=\"" + ob.aData.international_tariff + "\" style=\"text-decoration: underline;color:blue;\" class=\"orderIDAnchor\" param-data=\"" + ob.aData.config_id + "\" bundle-data=\"" + ob.aData.bundleid + "\">" + bundleAllowance + "</a>";
                                }
                                else
                                    return bundleAllowance;
                                return bundleAllowance;
                            }
                            else
                                return "";
                        }
                    },
                    {
                        mDataProp: "avaialable_allowance", sTitle: "Bundle Balance", sWidth: "120px",
                        fnRender: function (ob) {
                            if (ob.aData.avaialable_allowance != null) {
                                var arr = ob.aData.avaialable_allowance.split('|');
                                var bundleBalance = "";
                                $.each(arr, function (i) {
                                    bundleBalance += "<span style=\"font-size:12px\">" + arr[i] + "</span><br>";
                                });
                                return bundleBalance;
                            }
                            else
                                return "";
                        }
                    },
                    {
                        mDataProp: "group_type", sTitle: "Unsubscribe Type", bSortable: false, sWidth: "95px",
                        fnRender: function (ob) {
                            var uType = "";
                            uType += "<input type=\"radio\" value=\"1\" style=\"margin-top:-2px\">&nbsp;<span style=\"font-size:12px\">Expire Date</span><br>";
                            uType += "<input type=\"radio\" value=\"2\" style=\"margin-top:-2px\" checked=\"checked\">&nbsp;<span style=\"font-size:12px\">Immediately</span>";
                            return uType;
                        }
                    },
                    {
                        mDataProp: "action", sTitle: "Action", bSortable: false,
                        fnRender: function (ob) {
                            var actionURL = "";
                            actionURL = "<button class=\"btn blue btnPBSBUnsubscribe\">Unsubscribe</button>";
                            return actionURL;
                        }
                    },
                    {
                        mDataProp: "action2", sTitle: "Action", bSortable: false, sWidth: "95px",
                        fnRender: function (ob) {
                            if (ob.aData.Status == "Active") {
                                var actionURL = "<button ctrlValue=\"" + ob.aData.bundleid + "\" ctrlText=\"" + ob.aData.name + "\" class=\"btn blue btnPBSBAddData\" style=\"width:100px\">Add Data / Valid Days</button>";
                                return actionURL;
                            }
                            else {
                                var actionURL = "<button class=\"btn lightgrey btnPBSBAddData\" style=\"width:100px\" disabled=\"disabled\">Add Data / Valid Days</button>";
                                return actionURL;
                            }
                        }
                    },
                    //27-Dec-2018 : Moorthy : Exchange Bundles
                    {
                        mDataProp: "action3", sTitle: "Action", bSortable: false, sWidth: "95px",
                        fnRender: function (ob) {
                            if (user_role.toUpperCase() == "ADMIN" && ob.aData.Status == "Active") {
                                var actionURL = "<button ctrlValue=\"" + ob.aData.bundleid + "\" ctrlText=\"" + ob.aData.name + "\" ctrlValue2=\"" + ob.aData.price + "\"  class=\"btn blue btnPBSBExchangeBundle\" style=\"width:100px\">Exchange Bundle</button>";
                                return actionURL;
                            }
                            else {
                                var actionURL = "<button class=\"btn lightgrey btnPBSBExchangeBundle\" style=\"width:100px\" disabled=\"disabled\">Exchange Bundle</button>";
                                return actionURL;
                            }
                        }
                    }, 
                    {
                        mDataProp: "action4", sTitle: "Action", bSortable: false, sWidth: "95px",
                        fnRender: function (ob) {
                            if (ob.aData.Status == "Active") {
                                var actionURL = "<button ctrlValue=\"" + ob.aData.bundleid + "\" ctrlText=\"" + ob.aData.name + "\" ctrlValue2=\"" + ob.aData.price + "\"  class=\"btn blue btnPBSBAdjustBundle\" style=\"width:100px\">Adjust Bundle</button>";
                                return actionURL;
                            }
                            else {
                                var actionURL = "<button class=\"btn lightgrey btnPBSBAdjustBundle\" style=\"width:100px\" disabled=\"disabled\">Adjust Bundle</button>";
                                return actionURL;
                            }
                        }
                    }
                ],
                fnDrawCallback: function (ob) {
                    for (var i = 0, iLen = ob.aiDisplay.length ; i < iLen ; i++) {
                        $("table#tblPBSubscribedBundles tbody tr:eq(" + i + ") td").each(function (idxTD) {
                            //Moorthy : Modified for PAYM & PAYG Merging
                            if (idxTD == 15) {//if (idxTD == 6) {
                                $(this).find(":radio").attr({ "name": "radioPBSubscribeBundle" + i });
                            }
                        });
                    }
                }
            });
            //$("#tblPBSubscribedBundles_wrapper .row-fluid").remove();
        },
        error: function (feedback) {
            $("#pbSubscribedBundlesBody").css({ "background-image": "none" }).html("").append("No subscribed bundle found").show();
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : AvailableBundles
 * Purpose          : to show subscribed bundles
 * Added by         : Edi Suryadi
 * Create Date      : September 05th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function AvailableBundles(Msisdn, Iccid, Sitecode, balance, tariffclass) {

    $("#pbAvailableBundlesBody").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblPBAvailableBundles\"></table>");
    $("#pbAvailableBundlesBody").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });

    var url = apiServer + '/api/bundle/';
    jQuery.support.cors = true;
    var JSONSendData = {
        Msisdn: Msisdn,
        Iccid: Iccid,
        Bundle_Type: 1,
        Sitecode: Sitecode,
        balance: balance,
        tariffclass: tariffclass,
        ProductCode: $('.clsproductcode')[0].innerHTML
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#pbAvailableBundlesBody").css({ "background-image": "none" });
            $('#tblPBAvailableBundles').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: true,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [],
                aoColumns: [
                    { mDataProp: "bundleid", sTitle: "Bundle ID" },
                    { mDataProp: "name", sTitle: "Bundle Name" },
                    { mDataProp: "price", sTitle: "Price" },
                    { mDataProp: "ussd_code", sTitle: "Subscription Code" },
                    { mDataProp: "ussd_balance_check", sTitle: "Balance Check Code" },
                    { mDataProp: "renewal_mode", sTitle: "Renewal Mode" },
                    { mDataProp: "renewal_delay", sTitle: "Renewal Period" },
                    { mDataProp: "sendmsg_mode", sTitle: "Notification Mode" },
                    //CRM Enhancement 2
                    { mDataProp: "category_name", sTitle: "Category Name" },
                    {
                        mDataProp: "action", sTitle: "Action", bSortable: false,
                        fnRender: function (ob) {
                            var actionURL = "";
                            actionURL = "<button class=\"btn blue btnPBABSubscribe\">Subscribe</button>";
                            return actionURL;
                        }
                    }
                ]
            });
            /*        $("#tblPBAvailableBundles_wrapper .row-fluid:eq(0)").remove();
                    $("div#tblDDOperation_wrapper > div.row-fluid > div.span6:eq(1)").css({ "width": "50%", "margin": "0 auto" });
                    $("div#tblDDOperation_wrapper > div.row-fluid > div.span6 > div#tblDDOperation_filter").css({ "float": "left", "padding-left": "0", "margin-left": "0", "margin-right": "auto" });
                    $("div#tblDDOperation_wrapper > div.row-fluid > div.span6 > div#dataTablesBreadCrumb").remove();
                    $("<div class=dataTables_filter id=dataTablesBreadCrumb style='float:left'><label>" + "<a href='./TabLsearchcustomer'>Home</a>&nbsp;|&nbsp;" + crumburl + "&nbsp;|&nbsp;" + crumburl1 + "</a></label></div>").insertAfter($("div#tblDDOperation_wrapper > div.row-fluid > div.span6 > div#tblDDOperation_length"));
                    */
        },
        error: function (feedback) {
            $("#pbAvailableBundlesBody").css({ "background-image": "none" }).html("").append("No available bundle found").show();
        }
    });

}


/* ----------------------------------------------------------------
 * Function Name    : DoUnsubscribe
 * Purpose          : to do unsubscribe
 * Added by         : Edi Suryadi
 * Create Date      : September 05th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function DoUnsubscribe(msisdn, sitecode, bundleID, UnsubscribeType) {
    var url = apiServer + '/api/bundle/';
    var ajaxSpinnerTop = ($(window).outerHeight() / 2) + $(window).scrollTop();
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    jQuery.support.cors = true;
    var JSONSendData = {
        msisdn: msisdn,
        sitecode: sitecode,
        Bundle_Type: 4,
        bundleid: bundleID,
        UnsubscribeType: UnsubscribeType,
        pack_dest: '',
        crm_user: $('.username').html(),
        ProductCode: $('.clsproductcode')[0].innerHTML
    };

    $("#ProductBundlesMessage").hide();
    $(".modal-backdrop").hide();

    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();

            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });

            var stat = data.errcode;
            if (stat == 0) {
                $('.PMSuccess .modal-header').html(data.errsubject);
                $('.PMSuccess .modal-body').html(data.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            } else {
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : DoSubscribe
 * Purpose          : to do subscribe
 * Added by         : Edi Suryadi
 * Create Date      : September 06th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function DoSubscribe(msisdn, sitecode, bundleID) {
    var url = apiServer + '/api/bundle/';
    var ajaxSpinnerTop = ($(window).outerHeight() / 2) + $(window).scrollTop();
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    jQuery.support.cors = true;
    var JSONSendData = {
        msisdn: msisdn,
        sitecode: sitecode,
        Bundle_Type: 3,
        bundleid: bundleID,
        pack_dest: '',
        ProductCode: $('.clsproductcode')[0].innerHTML
    };

    $("#ProductBundlesMessage").hide();
    $(".modal-backdrop").hide();

    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();

            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });

            var stat = data.errcode;
            if (stat == 0) {
                $('.PMSuccess .modal-header').html(data.errsubject);
                $('.PMSuccess .modal-body').html(data.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            } else {
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        }
    });
}

//Bundle Name Click
//CRM Enhancement 2
function BundleNameClick(config_id, sitecode, bundle_name, msisdn, international_tariff, subscription_date) {
    $("#pbBundleRates").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblBundleRates\"></table>");
    $("#pbBundleRates").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    $("#BundleRatesName").html("");
    var url = apiServer + '/api/bundle/';
    jQuery.support.cors = true;
    var JSONSendData = {
        config_id: config_id,
        Bundle_Type: 8,
        Sitecode: sitecode,
        msisdn: msisdn,
        international_tariff: international_tariff,
        subscription_date: subscription_date
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#BundleRatesName").html(feedback[0].bundle_call);
            $("#pbBundleRates").css({ "background-image": "none" });
            $('#tblBundleRates').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: false,
                bFilter: false,
                bInfo: false,
                bSorting: false,
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Data Available."
                },
                aoColumns: [
                    {
                        mDataProp: "country", sTitle: "Country", sWidth: "150px",
                        fnRender: function (ob) {
                            //return '<img width="20" height="20" src="../../Assets/Main/img/flag/Afghanistan.svg">' + ob.aData.country + '<span>Afghanistan</span>';
                            return ob.aData.country;
                        }
                    },
                    //{ mDataProp: "country_code", sTitle: "Country Code", sWidth: "50px" },
                    //{ mDataProp: "bundle_min_category", sTitle: "Min.", sWidth: "50px" },
                    //{ mDataProp: "category_name", sTitle: "Category Name", sWidth: "150px" },
                    { mDataProp: "fixed", sTitle: "Landline", sWidth: "50px" },
                    { mDataProp: "mobile", sTitle: "Mobile", sWidth: "50px" },
                    { mDataProp: "fixed_used_min", sTitle: "Landline Used&nbsp;Min.", sWidth: "50px" },
                    { mDataProp: "mobile_used_min", sTitle: "Mobile Used&nbsp;Min.", sWidth: "50px" },
                    { mDataProp: "country_type", sTitle: "Country&nbsp;Type", sWidth: "50px" }
                ]
            });
            //$("#tblPBSubscribedBundles_wrapper .row-fluid").remove();
            //$("#BundleRatesName").html(bundle_name);
            $('#ProductBundlesMessage3').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
        },
        error: function (feedback) {
            $("#pbBundleRates").css({ "background-image": "none" }).html("").append("No Data Available.").show();
        }
    });
}

//27-Dec-2018 : Moorthy : Exchange Bundles
function PopulateExchangeBundleCategory(planName, planId, planValue) {
    $('#ddlEBBundleCategory').find('option').remove();
    $("#PayMChangePlanSelect").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblCustExchangeBundle\"></table>");
    $("#btnChangePlanConfirm").attr("disabled", "disabled");

    var sitecode = $('#CustExchangeBundle_SiteCode').val();
    $("#CustExchangeBundle_CurrentPlan").val(planName);
    $("#CustExchangeBundle_CurrentPlanId").val(planId);
    $("#CustExchangeBundle_CurrentPlanValue").val(planValue);
    var url = apiServer + "/api/PAYMPlanBundles";
    var JSONSendData = {
        requestType: 96,
        SiteCode: sitecode
    };
    $.ajax
    ({
        url: url,
        type: "post",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            var category_id = 0;
            $("#CustExchangeBundle").css({ "background-image": "none" });
            $('#divCustExchangeBundle').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
            var feedbackarr = jQuery.makeArray(JSON.stringify(feedback).split('}'));
            var icount;
            var actionOption = '';
            for (icount = 0; icount < feedbackarr.length - 1; icount++) {
                if (icount == 0) {
                    category_id = feedback[icount].category_id;
                }
                actionOption += '<option value="' + feedback[icount].category_id + '" style=\"padding:3px\">' + feedback[icount].category_name + '</option>';
            }
            $('#ddlEBBundleCategory').append(actionOption);
            PopulateExchangeBundle(category_id);
        },
        error: function (feedback) {
            $("#CustExchangeBundle").css({ "background-image": "none" });
        }
    });
}

 
function PopulateAdjustBundle(planName, planId, planValue) {
    
}

//27-Dec-2018 : Moorthy : Exchange Bundles
function PopulateExchangeBundle(category_id) {
    var planName = $("#CustExchangeBundle_CurrentPlan").val();
    var planId = $("#CustExchangeBundle_CurrentPlanId").val();
    var sitecode = $('#CustExchangeBundle_SiteCode').val();
    $("#btnCustExchangeBundleConfirm").attr("disabled", "disabled");
    $("#CustExchangeBundle").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblCustExchangeBundle\"></table>");
    $("#CustExchangeBundle").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var row = 0;
    var url = apiServer + "/api/PAYMPlanBundles";
    var JSONSendData = {
        requestType: 97,
        SiteCode: sitecode,
        CategoryId: category_id
    };
    $.ajax
    ({
        url: url,
        type: "post",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            if (feedback.length > 0)
                $("#btnCustExchangeBundleConfirm").removeAttr("disabled");
            $("#CustExchangeBundle").css({ "background-image": "none" });
            $('#tblCustExchangeBundle').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: false,
                bFilter: false,
                bInfo: false,
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Bundle Selection found."
                },
                aoColumns: [
                    {
                        mDataProp: "bundleid", sTitle: "Select", bSortable: false,
                        fnRender: function (oObj) {
                            return '<div class="radio">' +
                                '<label>' +
                                '<input type="radio" id="radioCustExchangeBundle" name="radioCustExchangeBundle" value="' + oObj.aData.bundleid + '_' + oObj.aData.plan_value + '_' + oObj.aData.plan_name + '">' +
                                '<label>' +
                                '</div>';
                        }
                    },
                    { mDataProp: "plan_name", sTitle: "Bundle Name", bSortable: false },
                    { mDataProp: "plan_value", sTitle: "Plan Value", bSortable: false },
                    { mDataProp: "bundle_call", sTitle: "Call", bSortable: false },
                    { mDataProp: "bundle_sms", sTitle: "Sms", bSortable: false },
                    { mDataProp: "bundle_data", sTitle: "Data", bSortable: false }
                ],
                fnDrawCallback: function () {
                    $("#tblCustExchangeBundle tbody td").css({ "font-size": "12px" });
                }
            });
            $("#tblCustExchangeBundle tbody td").css({ "font-size": "12px" });
            $("#tblCustExchangeBundle_wrapper .row-fluid:eq(0)").remove();
            $("#tblCustExchangeBundle_wrapper .row-fluid .span6:eq(0)").addClass("span4");
            $("#tblCustExchangeBundle_wrapper .row-fluid .span6:eq(1)").addClass("span8");
            $("#tblCustExchangeBundle_wrapper .row-fluid .span6").removeClass("span6");
        },
        error: function (feedback) {
            $("#CustExchangeBundle").css({ "background-image": "none" }).html("").append("No Bundle Selection found.").show();
        }
    });
}

//27-Dec-2018 : Moorthy : Exchange Bundles
function CustExchangeBundle(msisdn, sitecode) {
    debugger;
    if ($("input[name='radioCustExchangeBundle']:checked").length == 1) {
        var oldPlanName = $("#CustExchangeBundle_CurrentPlan").val();
        var oldPlanBundleId = $("#CustExchangeBundle_CurrentPlanId").val();
        var oldPlanValue = $("#CustExchangeBundle_CurrentPlanValue").val();
        var newPlan = $("input[name='radioCustExchangeBundle']:checked").val();
        var process_flag = $("#chkCustExchangeBundle").prop("checked") == true ? 1 : 0;
        var newPlanArr = newPlan.split('_');
        if (newPlanArr.length > 1) {
            newPlanBundleId = newPlanArr[0];
            newPlanValue = newPlanArr[1];
            newPlanName = newPlanArr[2];
        }
        if (oldPlanBundleId != null && !isNaN(oldPlanBundleId) && newPlanBundleId != null && !isNaN(newPlanBundleId)) {
            if (oldPlanBundleId == newPlanBundleId) {
                alert("New plan is the same as the old one");
            }
            else {
                var calledby = $(".username").html();
                var url = apiServer + "/api/bundle";
                var JSONSendData = {
                    Bundle_Type: 9,
                    msisdn: msisdn,
                    sitecode: sitecode,
                    current_bundleid: oldPlanBundleId,
                    new_bundleid: newPlanBundleId,
                    paymode: 1,
                    process_flag: process_flag,
                    processby: "CRM",
                    crm_user: $(".username").html()
                };
                $.ajax
                ({
                    url: url,
                    type: "post",
                    data: JSON.stringify(JSONSendData),
                    dataType: "json",
                    async: true,
                    contentType: "application/json;charset=utf-8",
                    cache: false,
                    success: function (feedback) {
                        $('#divCustExchangeBundle').modal('hide');
                        alert(feedback[0].errmsg);
                        if (feedback[0].errcode == 0)
                            location.reload();
                    },
                    error: function (feedback) {
                        $('#divCustExchangeBundle').modal('hide');
                        $("#CustExchangeBundle").css({ "background-image": "none" });
                    }
                });
            }
        }
        else {
            alert("Bundle Selection caused some general error");
            $('#divCustExchangeBundle').modal('hide');
        }
    }
    else {
        alert("Select Bundle!");
        $("#divCustExchangeBundle").modal('show');
    }
}
/* ----------------------------------------------------- 
   *  eof TAB BUNDLES 
   ===================================================== */


