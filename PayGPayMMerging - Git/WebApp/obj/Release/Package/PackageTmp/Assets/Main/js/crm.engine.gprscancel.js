﻿/* ===================================================== 
   *  CMK  Gprs Cancel Location Script 
   ----------------------------------------------------- */

function Gprscancellocation() {
        var ajaxSpinnerTop = $(window).outerHeight() / 2;
        $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
        $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
        var tr1 = $('#tdCSDICCID').text();
        var tr3 = $('#tdCSDSitecode').text();
        var url = apiServer + '/api/GprsCancel';
        jQuery.support.cors = true;
        var JSONSendData = {
            ICCID: tr1,
            Sitecode: tr3,
            type:2
        };
        $("#Nothing").hide();
        $(".modal-backdrop").hide();
        $.ajax
        ({
            url: url,
            type: 'get',
            data: JSONSendData,
            dataType: 'json',
            contentType: "application/json;charset=utf-8",
            cache: false,
            beforeSend: function () {
                $("#ajax-screen-masking").show();
            },
            success: function (data) {

               
                $("#ajax-screen-masking").hide();

                $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
                $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });

              
                
                var stat = data[0].imsi_active;
                
                if (stat != null) {
                  
                    $('.PMSuccess .modal-header').html("GPRS Cancel Location");
                    $('.PMSuccess .modal-body').html("GPRS Cancel Location Successful");
                    $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                        'margin-left': function () {
                            return window.pageXOffset - ($(this).width() / 2);
                        }
                    });
                } else {
                    
                    $('.PMFailed .modal-header').html("GPRS Cancel Location");
                    $('.PMFailed .modal-body').html("GPRS Cancel Location Failed");
                    $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                        'margin-left': function () {
                            return window.pageXOffset - ($(this).width() / 2);
                        }
                    });
                }

            },
            error: function (data) {
                $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
                $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });
                $("#ajax-screen-masking").hide();
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        });
    }

/* ----------------------------------------------------- 
   *  CMK Gprs Cancel Location Script
   ===================================================== */

    
