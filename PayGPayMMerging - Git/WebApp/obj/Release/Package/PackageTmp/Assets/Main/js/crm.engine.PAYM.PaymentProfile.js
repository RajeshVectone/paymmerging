﻿function PaymentProfilePAYG(MobileNo, ProductCode) {
    $("#PayMPaymentProfile").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblPaymentProfile\"></table>");
    $("#PayMPaymentProfile").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var row = 0;
    var EnableAddNewDirectDebitButton = false;
    var url = apiServer + "/api/PAYMPaymentProfile";
    var JSONSendData = {
        MobileNo: MobileNo,
        ProductCode: ProductCode
    };
    $.ajax
    ({
        url: url,
        type: "GET",
        data: JSONSendData,
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#PayMPaymentProfile").css({ "background-image": "none" });
            $('#tblPaymentProfile').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: false,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [[2, "desc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Payment Profile PAYM found."
                },
                aoColumns: [
                    { mDataProp: "PaymentProfile", sTitle: "Payment Profile" },
                    { mDataProp: "Status", sTitle: "Status", sWidth: "75px" },
                    {
                        mDataProp: "SetupDate", sTitle: "Setup Date",
                        fnRender: function (ob) {
                            return convertDate01(ob.aData.SetupDate);
                        }
                    },
                    {
                        mDataProp: "LastDateofUse", sTitle: "Last Date of Use",
                        fnRender: function (ob) {
                            return convertDate01(ob.aData.LastDateofUse);
                        }
                    },
                    { mDataProp: "LastPaymentAmount", sTitle: "Last Payment Amount(£)" },
                    //{ mDataProp: "CardNumber", sTitle: "Card Number" },
                    { mDataProp: "CardNumber", sTitle: "Card Number", "bVisible": false },
                    {
                        sTitle: "Action", mDataProp: "Action", "bSortable": false,
                        fnRender: function (ob) {
                            var list = 0;
                            var cc_no = (ob.aData.CardNumber == null) ? null : ob.aData.CardNumber;
                            //if (ob.aData.PaymentProfile.indexOf('credit or debit card') >= 0) {
                            //    list = 1;
                            //      }
                            if (cc_no.length == 6) {
                                list = 1;
                            }
                            else
                                list = 0;
                            //var parameters = $(list).text() + "," + MobileNo.text() + "," + cc_no.text();
                            var parameters = list + "," + MobileNo + ",'" + cc_no.split(",")[0] + "'," + cc_no.split(",")[1] + ",'" + ProductCode + "'";
                            //08-Aug-2015 : Moorthy Modified to fix the popup not showing error
                            var DownloadList = '<a class="btn blue" onclick="getdownload(' + parameters + ');"> View More Info</a>';
                            return DownloadList;
                        }
                    },
                ],
                fnDrawCallback: function () {
                    $("#tblPaymentProfile tbody td").css({ "font-size": "12px" });
                }
            });
            $("#tblPaymentProfile tbody td").css({ "font-size": "12px" });
            $("#tblPaymentProfile_wrapper .row-fluid:eq(0)").remove();
            $("#tblPaymentProfile_wrapper .row-fluid .span6:eq(0)").addClass("span4");
            $("#tblPaymentProfile_wrapper .row-fluid .span6:eq(1)").addClass("span8");
            $("#tblPaymentProfile_wrapper .row-fluid .span6").removeClass("span6");
            //08-Aug-2015 : Moorthy Modified to add ProductCode
            DetermineDirectDebitActive(MobileNo, ProductCode);
        },
        error: function (feedback) {
            $("#PayMPaymentProfile").css({ "background-image": "none" }).html("").append("No Payment Profile PAYM found.").show();
            // $("#btnDirectDebit").css('display', 'none');
        }
    });
}

function ViewOperator(MobileNo) {
    $("#tblDDOperationContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblDDOperation\"></table>");
    $("#tblDDOperationContainer").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var row = 0;
    var url = apiServer + "/api/pmbogridfill";
    var JSONSendData = {
        value: MobileNo
    };
    $.ajax
    ({
        url: url,
        type: "GET",
        data: JSONSendData,
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#tblDDOperationContainer").css({ "background-image": "none" });
            $('#tblDDOperation').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: false,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [[2, "desc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Payment Profile PAYM found."
                },
                aoColumns: [
                        { mDataProp: "ref1", sTitle: "ref1" },
                        { mDataProp: "transCode", sTitle: "transCode" },
                        { mDataProp: "returnCode", sTitle: "returnCode" },
                        { mDataProp: "returnDescription", sTitle: "returnDescription" },
                        { mDataProp: "originalProcessingDate", sTitle: "originalProcessingDate" },
                        { mDataProp: "valueOf", sTitle: "valueOf" },
                        { mDataProp: "currency", sTitle: "currency" },
                        { mDataProp: "number3", sTitle: "number3" },
                        { mDataProp: "ref4", sTitle: "ref4" },
                        { mDataProp: "name5", sTitle: "name5" },
                        { mDataProp: "sortCode6", sTitle: "sortCode6" },
                        { mDataProp: "bankName7", sTitle: "bankName7" },
                ],
                fnDrawCallback: function () {
                    $("#tblDDOperation tbody td").css({ "font-size": "12px" });
                }
            });
            $("#tblDDOperation tbody td").css({ "font-size": "12px" });
        },
        error: function (feedback) {
            $("#tblDDOperationContainer").css({ "background-image": "none" }).html("").append("No Payment Profile PAYM found.").show();
        }
    });
}

function getdownload(details, MobileNo, cc_no, id, ProductCode) {
    var url = apiServer + "/api/PAYMPaymentProfile";
    var JSONSendData = {
        MobileNo: MobileNo,
        Paymentmode: details,
        CardNumber: cc_no + ',' + id,
        ProductCode: ProductCode
    };
    // alert(cc_no);
    $.ajax
    ({
        url: url,
        type: "Post",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            debugger;
            if (feedback[0] != null) {
                if (details == "1") {
                    //Credit Card
                    $('#PAYMDirectDebit').hide();
                    $('#PAYMCrediCard').show();
                    $('#PAYMDirectDebit2').hide();
                    $('#ViewInfo').css('width', '675px;');
                    $('#PaymentMode').html('Credit Card Details');
                    //$("table#tblPAYMCrediCard td#tdNameOfCardHolder").html(feedback[0].CardNumber.toString());
                    //$("table#tblPAYMCrediCard td#tdStreet").html('-');
                    //$("table#tblPAYMCrediCard td#tdCardType").html(feedback[0].ProviderCode.toString());
                    //$("table#tblPAYMCrediCard td#tdCity").html('-');
                    //$("table#tblPAYMCrediCard td#tdCardNumber").html(feedback[0].CardNumber.toString());
                    //$("table#tblPAYMCrediCard td#tdCountry").html('-');
                    ////$("table#tblPAYMCrediCard td#tdExpiryDate").html(feedback[0].expirydate.toString());
                    //$("table#tblPAYMCrediCard td#tdPostCode").html('-');
                    //$("table#tblPAYMCrediCard td#tdStatus").html('-');
                    //$('#ViewInfo').modal({ keyboard: false, backdrop: 'static' }).css({
                    //    'margin-left': function () {
                    //        return window.pageXOffset - ($(this).width() / 2);
                    //    }
                    //});
                    //                    public string CardNumber { get; set; }

                    $("table#tblPAYMCrediCard td#tdNameOfCardHolder").html(feedback[0].Name.toString() == null ? "No Name" : feedback[0].Name.toString());
                    $("table#tblPAYMCrediCard td#tdStreet").html(feedback[0].Street.toString());
                    // $("table#tblPAYMCrediCard td#tdCardType").html(feedback[0].ProviderCode.toString());
                    $("table#tblPAYMCrediCard td#tdCity").html(feedback[0].city.toString());
                    $("table#tblPAYMCrediCard td#tdPostCode").html(feedback[0].Postcode.toString());
                    $("table#tblPAYMCrediCard td#tdStatus").html(feedback[0].Status.toString());
                    $("table#tblPAYMCrediCard td#tdExpiryDate").html(feedback[0].Expirydate.toString());
                    $("table#tblPAYMCrediCard td#tdCardNumber").html(feedback[0].CC_No.toString());
                    $("table#tblPAYMCrediCard td#tdCountry").html('-');



                    $('#ViewInfo').modal({ keyboard: false, backdrop: 'static' }).css({
                        'margin-left': function () {
                            return window.pageXOffset - ($(this).width() / 2);
                        }
                    });
                }
                else if (details == "0") {
                    //Account Deatils
                    if (ProductCode != 'VMAT' && ProductCode != 'VMNL' && ProductCode != 'VMFR') {
                        $('#PAYMDirectDebit').show();
                        $('#PAYMCrediCard').hide();
                        $('#PAYMDirectDebit2').hide();
                        $('#ViewInfo').css('width', '400px;');
                        $('#PaymentMode').html('Direct Debit Details');
                        $("table#tblPAYMDDStepUp td#tdBankName").html(feedback[0].BankName.toString());
                        $("table#tblPAYMDDStepUp td#tdBankReference").html(feedback[0].BankReference.toString());
                        $("table#tblPAYMDDStepUp td#tdSortCode").html(feedback[0].SortCode.toString());
                        $("table#tblPAYMDDStepUp td#tdAccountNumber").html(feedback[0].AccountNumber.toString());
                        $("table#tblPAYMDDStepUp td#tdAccountName").html(feedback[0].AccountName.toString());
                        $("table#tblPAYMDDStepUp td#tdDDStatus").html(feedback[0].DDStatus.toString());
                        $('#ViewInfo').modal({ keyboard: false, backdrop: 'static' }).css({
                            'margin-left': function () {
                                return window.pageXOffset - ($(this).width() / 2);
                            }
                        });
                    }
                    else {
                        $('#PAYMDirectDebit2').show();
                        $('#PAYMDirectDebit').hide();
                        $('#PAYMCrediCard').hide();
                        $('#ViewInfo').css('width', '400px;');
                        $('#PaymentMode').html('Direct Debit Details');

                        $("table#tblPAYMDDStepUp2 td#tdAccountName2").html(feedback[0].AccountName.toString());
                        $("table#tblPAYMDDStepUp2 td#tdIBAN").html(feedback[0].AccountNumber.toString());
                        if (feedback[0].Country != null)
                            $("table#tblPAYMDDStepUp2 td#tdCountry2").html(feedback[0].Country.toString());
                        $('#ViewInfo').modal({ keyboard: false, backdrop: 'static' }).css({
                            'margin-left': function () {
                                return window.pageXOffset - ($(this).width() / 2);
                            }
                        });
                    }

                    ////07-Feb-2017 : Moorthy : Modified for VMAT, VMNL & VMFR (Added Bank Code) 
                    //if ($('.clsproductcode')[0].innerHTML == "VMAT" || $('.clsproductcode')[0].innerHTML == "VMNL" || $('.clsproductcode')[0].innerHTML == "VMFR") {
                    //    $('.clsSortCode2').hide();
                    //    $('.clsBankCode2').show();
                    //    $("table#tblPAYMDDStepUp td#tdBankCode").html(feedback[0].SortCode.toString());
                    //}
                    //else {
                    //    $('.clsSortCode2').show();
                    //    $('.clsBankCode2').hide();
                    //    $("table#tblPAYMDDStepUp td#tdSortCode").html(feedback[0].SortCode.toString());
                    //}

                    ////07-Feb-2017 : Moorthy : Added for VMFR 
                    //if ($('.clsproductcode')[0].innerHTML == "VMFR") {
                    //    $('.clsBranchCode2').show();
                    //    $("table#tblPAYMDDStepUp td#tdBranchCode").html(feedback[0].BranchCode.toString());
                    //}
                    //else {
                    //    $('.clsBranchCode2').hide();
                    //}
                }
                else {
                    $("table#tblPAYMCrediCard td#tdNameOfCardHolder").html('');
                    $("table#tblPAYMCrediCard td#tdStreet").html('');
                    $("table#tblPAYMCrediCard td#tdCardType").html('');
                    $("table#tblPAYMCrediCard td#tdCity").html('');
                    $("table#tblPAYMCrediCard td#tdCardNumber").html('');
                    $("table#tblPAYMCrediCard td#tdCountry").html('');
                    $("table#tblPAYMCrediCard td#tdExpiryDate").html('');
                    $("table#tblPAYMCrediCard td#tdPostCode").html('');
                    $("table#tblPAYMCrediCard td#tdStatus").html('');
                    $("table#tblPAYMDDStepUp td#tdBankName").html('');

                    ////07-Feb-2017 : Moorthy : Modified for VMAT , VMNL & VMFR (Added Bank Code) 
                    //if ($('.clsproductcode')[0].innerHTML == "VMAT" || $('.clsproductcode')[0].innerHTML == "VMNL" || $('.clsproductcode')[0].innerHTML == "VMFR") {
                    //    $('.clsSortCode2').hide();
                    //    $('.clsBankCode2').show();
                    //    $("table#tblPAYMDDStepUp td#tdBankCode").html('');
                    //}
                    //else {
                    //    $('.clsSortCode2').show();
                    //    $('.clsBankCode2').hide();
                    //    $("table#tblPAYMDDStepUp td#tdSortCode").html('');
                    //}

                    ////07-Feb-2017 : Moorthy : Added for VMFR 
                    //if ($('.clsproductcode')[0].innerHTML == "VMFR") {
                    //    $('.clsBranchCode2').show();
                    //    $("table#tblPAYMDDStepUp td#tdBranchCode").html('');
                    //}
                    //else {
                    //    $('.clsBranchCode2').hide();
                    //}

                    $("table#tblPAYMDDStepUp td#tdAccountNumber").html('');
                    $("table#tblPAYMDDStepUp td#tdAccountName").html('');
                    $("table#tblPAYMDDStepUp td#tdDDStatus").html('');
                    $("table#tblPAYMDDStepUp td#tdBankReference").html('');
                    $("table#tblPAYMDDStepUp td#tdSortCode").html("");
                    $("table#tblPAYMDDStepUp td#tdAccountName2").html('');
                    $("table#tblPAYMDDStepUp td#tdIBAN").html('');
                    $("table#tblPAYMDDStepUp td#tdCountry2").html('');
                }
            }
            else {
                $('#ErrMsg').html("There No Data");
                $('#NoData').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        }
    });
}

function DDStausList(DDStatusID) {
    var result = "";
    if (DDStatusID == "1") {
        return result = "Instruction submitted";
    }
    else if (DDStatusID == "2") {
        return result = "Instruction failed";
    }
    else if (DDStatusID == "3") {
        return result = "Block Excess";
    }
    else if (DDStatusID == "4") {
        return result = "Suspend";
    }
    else if (DDStatusID == "5") {
        return result = "Unblock Excess Usage";
    }
    else if (DDStatusID == "6") {
        return result = "Unsuspend Account";
    }
    else if (DDStatusID == "7") {
        return result = "Collection Failed";
    }
    else if (DDStatusID == "8") {
        return result = "Immediate Close";
    }
    else if (DDStatusID == "9") {
        return result = "Schedule Close";
    }
    else if (DDStatusID == "10") {
        return result = "Instruction Changed";
    }
    else if (DDStatusID == "11") {
        return result = "Setup in Progress";
    }
    else {
        return "-";
    }
}

$(document).ready(function () {
    $('#txtFirst').keyup(function () {
        var txtfirst = $('#txtFirst').val().length;
        if (txtfirst >= 2) { $('#txtSecond').focus(); }
        else { $('#txtFirst').focus(); }
    });
    $('#txtSecond').keyup(function () {
        var txtfirst = $('#txtSecond').val().length;
        if (txtfirst >= 2) { $('#txtThird').focus(); }
        else { $('#txtSecond').focus(); }
    });
    $('#txtThird').keyup(function () {
        var txtfirst = $('#txtThird').val().length;
        if (txtfirst >= 2) { }
        else { $('#txtThird').focus(); }
    });
});

function Number(txtValues) {
    var intRegex = /^\d+$/;
    if (intRegex.test(txtValues.val())) { }
    else { txtValues.val(''); }
}
//07-Feb-2017 : Moorthy : Modified for VMAT & VMNL (Added Bank Code) 
function Clear() {
    $('#txtFirst').val('');
    $('#txtSecond').val('');
    $('#txtThird').val('');
    $('#txtAccountNumber').val('');
    $('#txtAccountName').val('');


    //01-Jun-2017 : Moorthy : Modified for VMAT, VMNL & VMFR
    $('#txtAccountName2').val('');
    $('#txtIBAN').val('');
    $('#SelectCountry').find('option').remove().end();

    //07-Feb-2017 : Moorthy : Modified for VMAT, VMNL & VMFR (Added Bank Code) 
    //$('#txtBankCode').val('');
    //var productCode = $('.clsproductcode')[0].innerHTML;
    //if (productCode == "VMAT" || productCode == "VMNL" || productCode == "VMFR") {
    //    $('.clsSortCode').hide();
    //    $('.clsBankCode').show();
    //}
    //else {
    //    $('.clsSortCode').show();
    //    $('.clsBankCode').hide();
    //}

    //07-Feb-2017 : Moorthy : Modified for VMFR (Added Branch Code) 
    //$('#txtBranchCode').val('');
    //if (productCode == "VMFR") {
    //    $('.clsBranchCode').show();
    //}
    //else {
    //    $('.clsBranchCode').hide();
    //}
}
//08-Aug-2015 : Moorthy Modified to add ProductCode
function DetermineDirectDebitActive(MobileNo, ProductCode) {
    var url = apiServer + "/api/isdirectdebitactive";
    var JSONSendData = {
        MobileNo: MobileNo,
        ProductCode: ProductCode
    };
    $.ajax
    ({
        url: url,
        type: "GET",
        data: JSONSendData,
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            if (feedback.toString() == "Y") {
                $("#btnDirectDebit").prop('disabled', false);
                //$("#btnDirectDebit").prop('disabled', true);
            }
            else {
                //$("#btnDirectDebit").css('display', 'block');
                $("#btnDirectDebit").prop('disabled', false);
            }
        },
        error: function (feedback) {
            //$("#tblDDOperationContainer").css({ "background-image": "none" }).html("").append("No Payment Profile PAYM found.").show();
            //$("#btnDirectDebit").css('display', 'block');
            $("#btnDirectDebit").prop('disabled', false);
        }
    });
}

//Service Validation 
function CallService(sortCode, accountNumber) {
    var jsondata = {};


    var url = "http://192.168.41.23:8962/VANServices.svc/VMUK/1/" + sortCode + "/" + accountNumber + "/ValidatingProcessV2014";


    //https://www.bankaccountchecker.com/listener.php?key=40d4b09e8ff222e7bc3308d565ff2c47&password=Murali!@34&output=xml&type=uk&sortcode=779119&bankaccount=77821968

    //$.ajax({
    //    type: "GET", //GET or POST or PUT or DELETE verb
    //    url: url, // Location of the service
    //    data: "{}",
    //    contentType: "application/json; charset=utf-8", // content type sent to server
    //    dataType: "json", //Expected data format from server
    //    cache: false,
    //    async: false,
    //    //processdata: false, //True or False
    //    crossDomain: true,
    //    success: function (feedback) {//On Successfull service call StatusInformation
    //        if (feedback != null) 
    //        {
    //            //alert(feedback.toString())
    //            alert(feedback)
    //        }
    //    },
    //    error: function (feedback) {
    //        //alert(feedback.toString())
    //        alert(feedback)
    //    }
    //});

    $.ajax({
        url: url,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        data: "{}",
        dataType: "json",
        success: function (data) {
            alert(data)

        }
    });
}



//Get the DD-Setup function
//30-Jan-2017 : Moorthy : Added for product code
function validateSaveDDSetUp(subscriberID, MobileNo, bankName, bankCode, sortCode, accountNumber, accountName, updateby, productCode, branchCode, country, sitecode, pp_customer_id) {
    debugger;
    var aStatus = true;

    //07-Feb-2017 : Moorthy : Modified for VMAT & VMNL (Added Bank Code) 
    //Sort Code
    //if (productCode == "VMAT" || productCode == "VMNL" || productCode == "VMFR") {
    //    if (sortCode != '' && sortCode != null) {
    //        $('#rfvBankCode').removeClass();
    //        $('#rfvBankCode').html('');
    //    }
    //    else {
    //        aStatus = false;
    //        $('#rfvBankCode').addClass('errMsg');
    //        $('#rfvBankCode').html('*');
    //    }
    //}
    //else {
    if (productCode != "VMAT" && productCode != "VMNL" && productCode != "VMFR" && productCode != "VMBE") {
        //Bank Names
        if (bankName != '-- Select Bank Name --' && bankCode != '') {
            $('#rfvSelectBank').removeClass();
            $('#rfvSelectBank').html('');
        }
        else {
            aStatus = false;
            $('#rfvSelectBank').addClass('errMsg');
            $('#rfvSelectBank').html('*');
        }
        //Sort Code
        if (sortCode != '' && sortCode != null) {
            if (sortCode.length > 6) {
                aStatus = false;
                $('#rfvSortCode').addClass('errMsg');
                $('#rfvSortCode').html('*');
            }
            else {
                $('#rfvSortCode').removeClass();
                $('#rfvSortCode').html('');
            }
        }
        else {
            aStatus = false;
            $('#rfvSortCode').addClass('errMsg');
            $('#rfvSortCode').html('*');
        }
        //Account Number
        if (accountNumber != '' && accountNumber != null) {
            $('#rfvAccountNumber').removeClass();
            $('#rfvAccountNumber').html('');
        }
        else {
            aStatus = false;
            $('#rfvAccountNumber').addClass('errMsg');
            $('#rfvAccountNumber').html('*');
        }
        //Account Name
        if (accountName != '' && accountName != null) {
            $('#rfvAccountName').removeClass();
            $('#rfvAccountName').html('');
        }
        else {
            aStatus = false;
            $('#rfvAccountName').addClass('errMsg');
            $('#rfvAccountName').html('*');
        }
    }
    else {
        //Account Name
        if (accountName != '' && accountName != null) {
            $('#rfvAccountName2').removeClass();
            $('#rfvAccountName2').html('');
        }
        else {
            aStatus = false;
            $('#rfvAccountName2').addClass('errMsg');
            $('#rfvAccountName2').html('*');
        }

        //IBAN
        if (accountNumber != '' && accountNumber != null) {
            $('#rfvIBAN').removeClass();
            $('#rfvIBAN').html('');
        }
        else {
            aStatus = false;
            $('#rfvIBAN').addClass('errMsg');
            $('#rfvIBAN').html('*');
        }
    }
    //}

    //if (productCode == "VMFR") {
    //    if (branchCode != '' && branchCode != null) {
    //        $('#rfvBranchCode').removeClass();
    //        $('#rfvBranchCode').html('');
    //    }
    //    else {
    //        aStatus = false;
    //        $('#rfvBranchCode').addClass('errMsg');
    //        $('#rfvBranchCode').html('*');
    //    }
    //}



    //Service Validation called
    //if (aStatus)
    //{
    //    CallService(sortCode, accountNumber);
    //    //aStatus = false;
    //}


    if (aStatus) {
        var url = apiServer + "/api/PAYMDDStetup";
        var JSONSendData = {
            subscriberID: subscriberID,
            mobileNo: MobileNo,
            bankName: bankName,
            bankCode: bankCode,
            sortCode: sortCode,
            accoutNumber: accountNumber,
            accountName: accountName,
            updateby: updateby,
            productCode: productCode,
            branchCode: branchCode,
            country: country,
            sitecode: sitecode,
            pp_customer_id: pp_customer_id
        };
        $.ajax
        ({
            url: url,
            type: "POST",
            data: JSON.stringify(JSONSendData),
            dataType: "json",
            async: true,
            contentType: "application/json;charset=utf-8",
            cache: false,
            success: function (feedback) {
                debugger;
                Clear();
                if (feedback != null) {
                    if (feedback[0].errcode == "0") {
                        if (productCode != "VMAT" && productCode != "VMNL" && productCode != "VMFR")
                            $('#AddDDSteup').modal("hide");
                        else
                            $('#AddDDSteup2').modal("hide");
                        $('#ResultInfo').modal({ keyboard: false, backdrop: 'static' }).css({
                            'margin-left': function () {
                                return window.pageXOffset - ($(this).width() / 2);
                            }
                        });
                        window.location.reload();
                        // $("#btnDirectDebit").css('display', 'none');
                        $("#btnDirectDebit").prop('disabled', true);
                    }
                    else {
                        if (productCode != "VMAT" && productCode != "VMNL" && productCode != "VMFR")
                            $('#AddDDSteup').modal("hide");
                        else
                            $('#AddDDSteup2').modal("hide");
                        $('#ErrMsg').html(feedback[0].errmsg);
                        $('#NoData').modal({ keyboard: false, backdrop: 'static' }).css({
                            'margin-left': function () {
                                return window.pageXOffset - ($(this).width() / 2);
                            }
                        });
                    }
                }
                else {
                    $('#ErrMsg').html("There Some Problem For Adding DD-Setup");
                    $('#NoData').modal({ keyboard: false, backdrop: 'static' }).css({
                        'margin-left': function () {
                            return window.pageXOffset - ($(this).width() / 2);
                        }
                    });
                }
            },
            error: function (feedback) {
                debugger;
                Clear();
                if (productCode != "VMAT" && productCode != "VMNL" && productCode != "VMFR")
                    $('#AddDDSteup').modal("hide");
                else
                    $('#AddDDSteup2').modal("hide");
                $('#ErrMsg').html("There Some Problem For Adding DD-Setup");
                $('#NoData').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        });
    }
    //else { }
    //Clear();
}








