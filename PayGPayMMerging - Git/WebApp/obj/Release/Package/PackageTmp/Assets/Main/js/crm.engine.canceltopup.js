﻿/* ===================================================== 
   *  Cancel Topup Script 
   ----------------------------------------------------- */

function InsertCancelTopup(sitecode, mobileno, paymentRef, Amount, Reason, refund_by) {
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    var url = apiServer + '/api/CancelTopup';
    jQuery.support.cors = true;
    var JSONSendData = {
        sitecode: sitecode,
        mobileno: mobileno,
        paymentRef: paymentRef,
        Amount: Amount,
        Reason: Reason,
        requestedby: refund_by,
        CancelModeType: 1
    };
    $.ajax
({
    url: url,
    type: 'post',
    data: JSON.stringify(JSONSendData),
    dataType: 'json',
    contentType: "application/json;charset=utf-8",
    cache: false,
    beforeSend: function () {
        $("#ajax-screen-masking").show();
    },
    success: function (feedback) {
        $("#ajax-screen-masking").hide();
        if (feedback != null) {
            alert(feedback.errmsg);
        }
    },
    error: function (feedback) {
        $("#ajax-screen-masking").hide();
    }
});
}

//CancelTopup Search
function getcanceltopupreport(sitecode, date_fr, date_to) {
    $("#tblDDOperationContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblDDOperation\"></table>");
    $("#tblDDOperationContainer").css({ "min-height": "100px", "background-repeat": "no-repeat", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var row = 0;
    var url = apiServer + "/api/CancelTopup";
    var JSONSendData = {
        sitecode: sitecode,
        date_from: date_fr,
        date_to: date_to,
        CancelModeType: 2
    };
    $.ajax
    ({
        url: url,
        type: "POST",
        data: JSON.stringify(JSONSendData),
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $('#tblDDOperation').html('');
            $("#tblDDOperationContainer").css({ "background-image": "none" });
            $('#tblDDOperation').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                iDisplayLength: 10,
                bPaginate: true,
                bFilter: true,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [[0, "desc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No records found."
                },
                aoColumns: [
                    { mDataProp: "REQUEST_DATE_String", sTitle: "Request Date", },
                    { mDataProp: "MOBILENO", sTitle: "Mobile No" },
                    { mDataProp: "PAYMENT_REF", sTitle: "Reference ID" },
                    { mDataProp: "AMOUNT", sTitle: "Amount" },
                    { mDataProp: "PREV_BALANCE", sTitle: "Prev Balance" },
                    { mDataProp: "AFTER_BALANCE", sTitle: "After Balance" },
                    { mDataProp: "REASON", sTitle: "Reason" },
                    { mDataProp: "REQUEST_BY", sTitle: "Request By" },
                    {
                        mDataProp: "ACTION", sTitle: "Action", sWidth: "100px",
                        fnRender: function (ob) {
                            var actionURL = "";
                            if ($.trim(ob.aData.STATUS) == "0")
                                actionURL = "<button data-sc=\"" + ob.aData.sitecode + "\" data-mn=\"" + ob.aData.MOBILENO + "\" data-pid=\"" + ob.aData.PAYMENT_REF + "\" data-amt=\"" + ob.aData.AMOUNT + "\" class=\"btn blue btnCTRefund\">Submit</button>";
                            else
                                actionURL = "<button class=\"btn lightgrey btnCTRefund\" disabled=\"disabled\">Submit</button>";
                            return actionURL;
                        }
                    }
                ],
                fnDrawCallback: function () {
                    $("#tblDDOperation tbody td").css({ "font-size": "12px" });
                }
            });
            $("#tblDDOperation tbody td").css({ "font-size": "12px" });
        },
        error: function (feedback) {
            $("#tblDDOperationContainer").css({ "background-image": "none" }).html("").append("No records found.").show();
        }
    });
}

//CancelTopup Download
function downloadcanceltopupreport(sitecode, date_fr, date_to) {
    var i = 0;
    jQuery.support.cors = true;
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-screen-masking #ajax-loader1").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    $("#ajax-screen-masking #downloadIFrame").remove();
    var url = '/Download/DownloadCancelTopupReport';
    var JSONSendData = {
        sitecode: sitecode,
        date_fr: date_fr,
        date_to: date_to
    };
    $.ajax
    ({
        url: url,
        type: 'get',
        dataType: 'json',
        data: JSONSendData,
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {

            if (feedback.errcode == 0) {
                $("#ajax-screen-masking").hide();
                var IFrameData = $('<iframe/>', { id: 'downloadIFrame', src: '/Download/DownloadFinanceCancelTopupReport?id=' + feedback.Text, style: 'display:none' });
                $("#ajax-screen-masking").append(IFrameData);
            }
        }
    });
}

function UpdateCancelTopup(sitecode, mobileno, paymentRef, Amount, Reason, refund_by, process_type) {
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    var url = apiServer + '/api/CancelTopup';
    jQuery.support.cors = true;
    var JSONSendData = {
        sitecode: sitecode,
        mobileno: mobileno,
        paymentRef: paymentRef,
        Amount: Amount,
        Reason: Reason,
        requestedby: refund_by,
        CancelModeType: 3,
        process_type: process_type
    };
    $.ajax
({
    url: url,
    type: 'post',
    data: JSON.stringify(JSONSendData),
    dataType: 'json',
    contentType: "application/json;charset=utf-8",
    cache: false,
    beforeSend: function () {
        $("#ajax-screen-masking").show();
    },
    success: function (feedback) {
        $("#ajax-screen-masking").hide();
        if (feedback != null) {
            alert(feedback.errmsg);
            if (feedback.errcode == 0)
                window.location.reload();
        }
    },
    error: function (feedback) {
        $("#ajax-screen-masking").hide();
    }
});
}