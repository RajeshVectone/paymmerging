﻿/* ===================================================== 
   *  TAB TOPUP TRANSFER 
   ----------------------------------------------------- */

/* ----------------------------------------------------------------
 * Function Name    : ViewTopupTransferList
 * Purpose          : to view Topup Transfer List
 * Added by         : Edi Suryadi
 * Create Date      : January 27th, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function ViewTopupTransferList(MobileNo, SiteCode, StartDate, EndDate, Name) {
    var DonorCountry = null;
    $("#ttFilterResultContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblTopupTransferResult\"></table>");
    $("#ttFilterResultContainer").css({
        "min-height": "100px",
        "border-top": "2px solid #e5e5e5",
        "margin-top" : "10px",
        "padding-top" : "10px",
        "background": "#ffffff url('/Assets/Main/img/ajaxloader-02.gif') center center no-repeat"
    });
    var url = apiServer + '/api/airtimetransfer/';
    jQuery.support.cors = true;
    var JSONSendData = {
        MobileNo: MobileNo,
        SiteCode: SiteCode,
        DateFrom: StartDate,
        DateTo: EndDate,
        Name: Name
    };
    
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#TabProductLOTGLoader").hide();
            $('#tblTopupTransferResult').dataTable({
                bDestroy: true,
                aaData: feedback,
                iDisplayLength: 10,
                bPaginate: true,
                bFilter: false,
                bProcessing: false,
                bInfo: false,
                aaSorting: [],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Topup transfer found."
                },
                aoColumns: [
                    {
                        mDataProp: "AirTimeDate", sTitle: "Date", "bUseRendered": false, "bSortable": true, sWidth: "100px",
                        fnRender: function (ob) {
                            return convertDateISOCustom(ob.aData.AirTimeDate, 1);
                        }
                    },
                    { mDataProp: "RecipientID", sTitle: "Recipient Number" },
                    { mDataProp: "RecipientName", sTitle: "Recipient Name" },
                    {
                        mDataProp: "TransferAmount", sTitle: "Amount Transferred",
                        fnRender: function (ob) {
                            if (ob.aData.DonorCountry=='GB') { DonorCountry = "GBP"; }
                            else if (ob.aData.DonorCountry == 'IN') { DonorCountry = "INR"; }
                            else if (ob.aData.DonorCountry == 'EU') { DonorCountry = "EUR"; }
                            return getCurrencySymbol(DonorCountry) + " " + ob.aData.TransferAmount.toFixed(2);
                        }
                    },
                    {
                        mDataProp: "ReceivedAmount", sTitle: "Amount Received",
                        fnRender: function (ob) {
                            return getCurrencySymbol(ob.aData.Currency) + " " + ob.aData.ReceivedAmount.toFixed(2);
                        }
                    },
                    { mDataProp: "Status", sTitle: "Transfer Status" },
                    { mDataProp: "AirtimeID", sTitle: "Transaction Reference" },
                    { mDataProp: "FailedReason", sTitle: "Failure Reason" },
                    { mDataProp: "ProviderName", sTitle: "Name of the Provider" },
                    //{
                    //    mDataProp: "ProviderAmount", sTitle: "Amount from Provider",
                    //    fnRender: function (ob) {
                    //        return getCurrencySymbol(ob.aData.Currency) + " " + ob.aData.ProviderAmount.toFixed(2);
                    //    }
                    //},
                    { mDataProp: "Method", sTitle: "Method of Transfer" }
                ],
                fnInitComplete: function (ob) {
                    $("#tblTopupTransferResult .row-fluid:eq(0) .span6").html("");
                    var searchResultStr = "Searching Result:&nbsp;<b><span id=\"fnRecordsTotal\">" + ob.fnRecordsTotal() + "</span></b> transfer(s) found.";
                    $("#tblTopupTransferResult_wrapper .span6:eq(0)").css({"float" : "left" , "height" : "30px", "line-height" : "30px", "font-size" : "14px"}).html(searchResultStr);
                    $("#tblTopupTransferResult td").css({ "font-size": "12px" });
                }
            });
            $("#ttFilterResultContainer").css({"background-image" : "none"});
        },
        error: function (feedback) {
            $("#ttFilterResultContainer").html("").append("No package found").show();
        }
    });
}

/* ----------------------------------------------------- 
   *  eof TAB TOPUP TRANSFER 
   ===================================================== */


