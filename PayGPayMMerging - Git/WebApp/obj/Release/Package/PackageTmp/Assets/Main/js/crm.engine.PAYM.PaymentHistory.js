﻿function PAYMPaymentHistroy(MobileNo, ProductCode) {
    $("#PayMDirectDebit").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblDirectDebit\"></table>");
    $("#PayMDirectDebit").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });
    var row = 0;
    var url = apiServer + "/api/PAYMPaymentHistory";
    var JSONSendData = {
        MobileNo: MobileNo,
        ProductCode: ProductCode
    };
    $.ajax
    ({
        url: url,
        type: "GET",
        data: JSONSendData,
        dataType: "json",
        async: true,
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#PayMDirectDebit").css({ "background-image": "none" });
            $('#tblDirectDebit').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: false,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [[0, "desc"]],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Payment history PAYM found."
                },
                aoColumns: [
                    {
                        mDataProp: "PaymentDate", sTitle: "Payment Date",
                        fnRender: function (ob) {
                            return convertDate01(ob.aData.PaymentDate);
                        }
                    },
                    { mDataProp: "PaymentProfile", sTitle: "Payment Profile" },
                    { mDataProp: "PaymentAmount", sTitle: "Payment Amount" },
                    { mDataProp: "PaymentStatus", sTitle: "Payment Status" },
                    { mDataProp: "Pay_Reference", sTitle: "Payment Reference" },
                    { mDataProp: "Pay_Mode", sTitle: "Payment Mode" },
                ],
                fnDrawCallback: function () {
                    $("#tblDirectDebit tbody td").css({ "font-size": "12px" });
                }
            });
            $("#tblDirectDebit tbody td").css({ "font-size": "12px" });
            $("#tblDirectDebit_wrapper .row-fluid:eq(0)").remove();
            $("#tblDirectDebit_wrapper .row-fluid .span6:eq(0)").addClass("span4");
            $("#tblDirectDebit_wrapper .row-fluid .span6:eq(1)").addClass("span8");
            $("#tblDirectDebit_wrapper .row-fluid .span6").removeClass("span6");
        },
        error: function (feedback) {
            $("#PayMDirectDebit").css({ "background-image": "none" }).html("").append("No Payment history PAYM found.").show();
        }
    });
}