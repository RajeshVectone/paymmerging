﻿/* ===================================================== 
   *  TAB CDR HISTORY  
   ----------------------------------------------------- */

$(document).ready(function () {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    if (dd < 10) { dd = '0' + dd } if (mm < 10) { mm = '0' + mm } today = dd + '/' + mm + '/' + yyyy;


    //Added by karthik Jira:CRMT-198
    var Stdate = new Date();
    Stdate.setDate(Stdate.getDate() - 9);
    var Stdd = Stdate.getDate();
    var Stmm = Stdate.getMonth() + 1; //January is 0!
    var Styyyy = Stdate.getFullYear();
    if (Stdd < 10) { Stdd = '0' + Stdd } if (Stmm < 10) { Stmm = '0' + Stmm } Stdate = Stdd + '/' + Stmm + '/' + Styyyy;

    var lastdate1 = $('#lastdate1').val();
    if (lastdate1 == '') {
        $('#dp1').attr('data-date', Stdate);
        $('#dp1').datepicker().on('changeDate', function (e) {
            $('div.datepicker').hide();
        });
        $('#startdate').attr('value', Stdate);
        $('#lastdate1').val(Stdate);
    }
    else {
        $('#dp1').attr('data-date', lastdate1);
        $('#dp1').datepicker().on('changeDate', function (e) {
            $('div.datepicker').hide();
        });
        $('#startdate').attr('value', lastdate1);
    }

    var lastdate2 = $('#lastdate2').val();
    if (lastdate2 == '') {
        $('#dp2').attr('data-date', today);
        $('#dp2').datepicker().on('changeDate', function (e) {
            $('div.datepicker').hide();
        });
        $('#enddate').attr('value', today);
        $('#lastdate2').val(today);
    }
    else {
        $('#dp2').attr('data-date', lastdate2);
        $('#dp2').datepicker().on('changeDate', function (e) {
            $('div.datepicker').hide();
        });
        $('#enddate').attr('value', lastdate2);
    }

    $('#OrderListSearchBtn').click(function () {
        var orderType = $("select#orderType").val();
        var fromDate = $.trim($('#startdate').val());
        var toDate = $.trim($('#enddate').val());
        orderList(orderType, fromDate, toDate);
    });

});





/* ----------------------------------------------------------------
 * Function Name    : AllHistory
 * Purpose          : to show history of all
 * Added by         : Edi Suryadi
 * Create Date      : December 19th, 2014
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function AllHistory(mobileno, Iccid, Sitecode, startDate, endDate, Packageid) {
    $("span#strHistoryType").html("&nbsp;of All History");
    $("#chFilterResultBody").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblCHFilterResult\"></table>");
    $("#chFilterResultBody").css({"overflow": "auto", "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });

    var url = apiServer + '/api/cdr_v1/';
    jQuery.support.cors = true;
    var JSONSendData = {
        mobileno: mobileno,
        Iccid: Iccid,
        HistoryType: 0,
        Sitecode: Sitecode,
        Date_From: startDate,
        Date_To: endDate,
        //modif by karthik Jira CRMT-198
        Packageid: Packageid,
        productCode: $('.clsproductcode')[0].innerHTML
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#chFilterResultBody").css({ "background-image": "none" });
            var oTable = $('#tblCHFilterResult').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: false,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No  history found."
                },
                aoColumns: [
                    { mDataProp: "History_Type", sTitle: "History Type" },

                    //{ mDataProp: "dateTime_String", sTitle: "Call & Time" },

                    {
                        mDataProp: "Date_Time", sTitle: "Call & Time", "bUseRendered": false,
                        fnRender: function (ob) {
                            return convertDateISOCustom(ob.aData.Date_Time, 0);
                        }
                    },


                    { mDataProp: "CLI", sTitle: "CLI" },
                    { mDataProp: "Destination_Number", sTitle: "Destination Number" },
                    { mDataProp: "Package_Name", sTitle: "Package Name" },
                    { mDataProp: "Type", sTitle: "Type" },
                    { mDataProp: "Destination_Code", sTitle: "Destination Code" },
                    {
                        mDataProp: "DurationMin_string", sTitle: "Duration (Min:sec)",
                        fnRender: function (ob) {
                            return ob.aData.DurationMin_string + " Min";
                        }
                    },
                    {
                        mDataProp: "DataUsage_String", sTitle: "Data Usage(in MB)",
                        fnRender: function (ob) {
                            return ob.aData.DataUsage_String + " MB";
                        }
                    },
                    { mDataProp: "Tariff_Class", sTitle: "Tariff Class" },
                    { mDataProp: "Roaming_Zone", sTitle: "Roaming Zone" },
                    //CRM Enhancement 2
                    {
                        mDataProp: "ExcessCreditUsage", sTitle: "Excess Credit&nbsp;Usage",
                        fnRender: function (ob) {
                            if (ob.aData.Status == 1) {
                                return "";
                            }
                            else {
                                return ob.aData.Net_Charges;
                            }
                        }
                    },
                    { mDataProp: "Net_Charges", sTitle: "Net Charges" },
                    { mDataProp: "Balance_String", sTitle: "Balance" },
                    {
                        mDataProp: "Status", sTitle: "Within Bundle",
                        fnRender: function (ob) {
                            if (ob.aData.Status == 1) {
                                return "Yes";
                            }
                            else {
                                return "No";
                            }
                        }
                    }
                ]
            });
            if (oTable.length > 0) {
                oTable.fnAdjustColumnSizing();
            }
            $("#tblCHFilterResult_wrapper .row-fluid:eq(0)").remove();
            $("#tblCHFilterResult_wrapper .row-fluid .span6:eq(0)").addClass("span4");
            $("#tblCHFilterResult_wrapper .row-fluid .span6:eq(1)").addClass("span8");
            $("#tblCHFilterResult_wrapper .row-fluid .span6").removeClass("span6");

            var DownloadAnchor = "<a id=\"downloadAnchor\" style=\"display:none;\"></a>";
            var btnExportHistory = "<button class=\"btn blue\" id=\"btnExportCDRHistory\" name=\"btnExportCDRHistory\">Download to Excel</button>";
            $("#tblCHFilterResult_wrapper .row-fluid .span4").append(DownloadAnchor + btnExportHistory).show();
        },
        error: function (feedback) {
            $("#chFilterResultBody").css({ "background-image": "none" }).html("").append("No  history found.").show();
        }
    });
}


/* ----------------------------------------------------------------
 * Function Name    : CallHistory
 * Purpose          : to show history of Call
 * Added by         : Edi Suryadi
 * Create Date      : September 09th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function CallHistory(mobileno, Iccid, Sitecode, startDate, endDate, Packageid) {
    $("span#strHistoryType").html("&nbsp;of Call History");
    $("#chFilterResultBody").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblCHFilterResult\"></table>");
    $("#chFilterResultBody").css({"overflow": "auto", "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });

    var url = apiServer + '/api/cdr_v1/';
    jQuery.support.cors = true;
    var JSONSendData = {
        mobileno: mobileno,
        Iccid: Iccid,
        HistoryType: 1,
        Sitecode: Sitecode,
        Date_From: startDate,
        Date_To: endDate,
        //modif by karthik Jira CRMT-198
        Packageid: Packageid,
        productCode: $('.clsproductcode')[0].innerHTML
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#chFilterResultBody").css({ "background-image": "none" });
            var oTable = $('#tblCHFilterResult').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: false,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No call history found."
                },
                aoColumns: [
                    { mDataProp: "History_Type", sTitle: "History Type" },

                   // { mDataProp: "dateTime_String", sTitle: "Call & Time" },

                    {
                        mDataProp: "Date_Time", sTitle: "Call & Time", "bUseRendered": false,
                        fnRender: function (ob) {
                            return convertDateISOCustom(ob.aData.Date_Time, 0);
                        }
                    },

                    { mDataProp: "CLI", sTitle: "CLI" },
                    { mDataProp: "Destination_Number", sTitle: "Destination Number" },
                    { mDataProp: "Package_Name", sTitle: "Package Name" },
                    { mDataProp: "Type", sTitle: "Type" },
                    { mDataProp: "Destination_Code", sTitle: "Destination Code" },
                    {
                        mDataProp: "DurationMin_string", sTitle: "Duration (Min:sec)",
                        fnRender: function (ob) {
                            return ob.aData.DurationMin_string + " Min";
                        }
                    },
                    {
                        mDataProp: "DataUsage_String", sTitle: "Data Usage(in MB)",
                        fnRender: function (ob) {
                            return ob.aData.DataUsage_String + " MB";
                        }
                    },
                    { mDataProp: "Tariff_Class", sTitle: "Tariff Class" },
                    { mDataProp: "Roaming_Zone", sTitle: "Roaming Zone" },
                    //CRM Enhancement 2
                    {
                        mDataProp: "ExcessCreditUsage", sTitle: "Excess Credit&nbsp;Usage",
                        fnRender: function (ob) {
                            if (ob.aData.Status == 1) {
                                return "";
                            }
                            else {
                                return ob.aData.Net_Charges;
                            }
                        }
                    },
                    { mDataProp: "Net_Charges", sTitle: "Net Charges" },
                    { mDataProp: "Balance_String", sTitle: "Balance" },
                    {
                        mDataProp: "Status", sTitle: "Within Bundle",
                        fnRender: function (ob) {
                            if (ob.aData.Status == 1) {
                                return "Yes";
                            }
                            else {
                                return "No";
                            }
                        }
                    }
                ]
            });
            if (oTable.length > 0) {
                oTable.fnAdjustColumnSizing();
            }
            $("#tblCHFilterResult_wrapper .row-fluid:eq(0)").remove();
            $("#tblCHFilterResult_wrapper .row-fluid .span6:eq(0)").addClass("span4");
            $("#tblCHFilterResult_wrapper .row-fluid .span6:eq(1)").addClass("span8");
            $("#tblCHFilterResult_wrapper .row-fluid .span6").removeClass("span6");

            var DownloadAnchor = "<a id=\"downloadAnchor\" style=\"display:none;\"></a>";
            var btnExportHistory = "<button class=\"btn blue\" id=\"btnExportCDRHistory\" name=\"btnExportCDRHistory\">Download to Excel</button>";
            $("#tblCHFilterResult_wrapper .row-fluid .span4").append(DownloadAnchor + btnExportHistory).show();
        },
        error: function (feedback) {
            $("#chFilterResultBody").css({ "background-image": "none" }).html("").append("No call history found.").show();
        }
    });
}
/* ----------------------------------------------------------------
 * Function Name    : TextHistory
 * Purpose          : to show history of Text/SMS
 * Added by         : Edi Suryadi
 * Create Date      : September 09th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function TextHistory(mobileno, Iccid, Sitecode, startDate, endDate, Packageid) {
    $("span#strHistoryType").html("&nbsp;of Text History");
    $("#chFilterResultBody").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblCHFilterResult\"></table>");
    $("#chFilterResultBody").css({ "overflow": "auto", "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });

    var url = apiServer + '/api/cdr_v1/';
    jQuery.support.cors = true;
    var JSONSendData = {
        mobileno: mobileno,
        Iccid: Iccid,
        HistoryType: 2,
        Sitecode: Sitecode,
        Date_From: startDate,
        Date_To: endDate,
        //modif by karthik Jira CRMT-198
        Packageid: Packageid,
        productCode: $('.clsproductcode')[0].innerHTML
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#chFilterResultBody").css({ "background-image": "none" });
            var oTable = $('#tblCHFilterResult').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: false,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No text history found."
                },
                aoColumns: [
                   { mDataProp: "History_Type", sTitle: "History Type" },

                   // { mDataProp: "dateTime_String", sTitle: "Call & Time" },
                     {
                         mDataProp: "Date_Time", sTitle: "Call & Time", "bUseRendered": false,
                         fnRender: function (ob) {
                             return convertDateISOCustom(ob.aData.Date_Time, 0);
                         }
                     },
                    { mDataProp: "CLI", sTitle: "CLI" },
                    { mDataProp: "Destination_Number", sTitle: "Destination Number" },
                    { mDataProp: "Package_Name", sTitle: "Package Name" },
                    { mDataProp: "Type", sTitle: "Type" },
                    { mDataProp: "Destination_Code", sTitle: "Destination Code" },
                    {
                        mDataProp: "DurationMin_string", sTitle: "Duration (Min:sec)",
                        fnRender: function (ob) {
                            return ob.aData.DurationMin_string + " Min";
                        }
                    },
                    {
                        mDataProp: "DataUsage_String", sTitle: "Data Usage(in MB)",
                        fnRender: function (ob) {
                            return ob.aData.DataUsage_String + " MB";
                        }
                    },
                    { mDataProp: "Tariff_Class", sTitle: "Tariff Class" },
                    { mDataProp: "Roaming_Zone", sTitle: "Roaming Zone" },
                    //CRM Enhancement 2
                    {
                        mDataProp: "ExcessCreditUsage", sTitle: "Excess Credit&nbsp;Usage",
                        fnRender: function (ob) {
                            if (ob.aData.Status == 1) {
                                return "";
                            }
                            else {
                                return ob.aData.Net_Charges;
                            }
                        }
                    },
                    { mDataProp: "Net_Charges", sTitle: "Net Charges" },
                    { mDataProp: "Balance_String", sTitle: "Balance" },
                    {
                        mDataProp: "Status", sTitle: "Within Bundle",
                        fnRender: function (ob) {
                            if (ob.aData.Status == 1) {
                                return "Yes";
                            }
                            else {
                                return "No";
                            }
                        }
                    }
                ]
            });
            if (oTable.length > 0) {
                oTable.fnAdjustColumnSizing();
            }
            $("#tblCHFilterResult_wrapper .row-fluid:eq(0)").remove();
            $("#tblCHFilterResult_wrapper .row-fluid .span6:eq(0)").addClass("span4");
            $("#tblCHFilterResult_wrapper .row-fluid .span6:eq(1)").addClass("span8");
            $("#tblCHFilterResult_wrapper .row-fluid .span6").removeClass("span6");
            
            var DownloadAnchor = "<a id=\"downloadAnchor\" style=\"display:none;\"></a>";
            var btnExportHistory = "<button class=\"btn blue\" id=\"btnExportCDRHistory\" name=\"btnExportCDRHistory\">Download to Excel</button>";
            $("#tblCHFilterResult_wrapper .row-fluid .span4").append(DownloadAnchor + btnExportHistory).show();
        },
        error: function (feedback) {
            $("#chFilterResultBody").css({ "background-image": "none" }).html("").append("No text history found.").show();
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : DataHistory
 * Purpose          : to show history of Data/GPRS
 * Added by         : Edi Suryadi
 * Create Date      : September 09th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function DataHistory(mobileno, Iccid, Sitecode, startDate, endDate, Packageid) {
    $("span#strHistoryType").html("&nbsp;of Data History");
    $("#chFilterResultBody").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblCHFilterResult\"></table>");
    $("#chFilterResultBody").css({"overflow": "auto", "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });

    var url = apiServer + '/api/cdr_v1/';
    jQuery.support.cors = true;
    var JSONSendData = {
        mobileno: mobileno,
        Iccid: Iccid,
        HistoryType: 3,
        Sitecode: Sitecode,
        Date_From: startDate,
        Date_To: endDate,
        //modif by karthik Jira CRMT-198
        Packageid: Packageid,
        productCode: $('.clsproductcode')[0].innerHTML
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#chFilterResultBody").css({ "background-image": "none" });
            var oTable = $('#tblCHFilterResult').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: false,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No data history found."
                },
                aoColumns: [
                   { mDataProp: "History_Type", sTitle: "History Type" },

                   // { mDataProp: "dateTime_String", sTitle: "Call & Time" },

                    {
                        mDataProp: "Date_Time", sTitle: "Call & Time", "bUseRendered": false,
                        fnRender: function (ob) {
                            return convertDateISOCustom(ob.aData.Date_Time, 0);
                        }
                    },
                    { mDataProp: "CLI", sTitle: "CLI" },
                    { mDataProp: "Destination_Number", sTitle: "Destination Number" },
                    { mDataProp: "Package_Name", sTitle: "Package Name" },
                    { mDataProp: "Type", sTitle: "Type" },
                    { mDataProp: "Destination_Code", sTitle: "Destination Code" },
                    {
                        mDataProp: "DurationMin_string", sTitle: "Duration (Min:sec)",
                        fnRender: function (ob) {
                            return ob.aData.DurationMin_string + " Min";
                        }
                    },
                    {
                        mDataProp: "DataUsage_String", sTitle: "Data Usage(in MB)",
                        fnRender: function (ob) {
                            return ob.aData.DataUsage_String + " MB";
                        }
                    },
                    { mDataProp: "Tariff_Class", sTitle: "Tariff Class" },
                    { mDataProp: "Roaming_Zone", sTitle: "Roaming Zone" },
                    //CRM Enhancement 2
                    {
                        mDataProp: "ExcessCreditUsage", sTitle: "Excess Credit&nbsp;Usage",
                        fnRender: function (ob) {
                            if (ob.aData.Status == 1) {
                                return "";
                            }
                            else {
                                return ob.aData.Net_Charges;
                            }
                        }
                    },
                    { mDataProp: "Net_Charges", sTitle: "Net Charges" },
                    { mDataProp: "Balance_String", sTitle: "Balance" },
                    {
                        mDataProp: "Status", sTitle: "Within Bundle",
                        fnRender: function (ob) {
                            if (ob.aData.Status == 1) {
                                return "Yes";
                            }
                            else {
                                return "No";
                            }
                        }
                    }
                ]
            });
            if (oTable.length > 0) {
                oTable.fnAdjustColumnSizing();
            }
            $("#tblCHFilterResult_wrapper .row-fluid:eq(0)").remove();
            $("#tblCHFilterResult_wrapper .row-fluid .span6:eq(0)").addClass("span4");
            $("#tblCHFilterResult_wrapper .row-fluid .span6:eq(1)").addClass("span8");
            $("#tblCHFilterResult_wrapper .row-fluid .span6").removeClass("span6");

            var DownloadAnchor = "<a id=\"downloadAnchor\" style=\"display:none;\"></a>";
            var btnExportHistory = "<button class=\"btn blue\" id=\"btnExportCDRHistory\" name=\"btnExportCDRHistory\">Download to Excel</button>";
            $("#tblCHFilterResult_wrapper .row-fluid .span4").append(DownloadAnchor + btnExportHistory).show();
        },
        error: function (feedback) {
            $("#chFilterResultBody").css({ "background-image": "none" }).html("").append("No data history found.").show();
        }
    });
}

//CRM Enhancement 2
function GetExcessUsage(mobileno, Iccid, Sitecode, startDate, endDate, Packageid) {
    $("span#strHistoryType").html("&nbsp;of All History");
    $("#chFilterResultBody").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblCHFilterResult\"></table>");
    $("#chFilterResultBody").css({ "overflow": "auto", "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });

    var url = apiServer + '/api/cdr_v1/';
    jQuery.support.cors = true;
    var JSONSendData = {
        mobileno: mobileno,
        Iccid: Iccid,
        HistoryType: 7,
        Sitecode: Sitecode,
        Date_From: startDate,
        Date_To: endDate,
        //modif by karthik Jira CRMT-198
        Packageid: Packageid
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#chFilterResultBody").css({ "background-image": "none" });
            var oTable = $('#tblCHFilterResult').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: false,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No  history found."
                },
                aoColumns: [
                    { mDataProp: "History_Type", sTitle: "History Type" },

                    //{ mDataProp: "dateTime_String", sTitle: "Call & Time" },

                    {
                        mDataProp: "Date_Time", sTitle: "Call & Time", "bUseRendered": false,
                        fnRender: function (ob) {
                            return convertDateISOCustom(ob.aData.Date_Time, 0);
                        }
                    },


                    { mDataProp: "CLI", sTitle: "CLI" },
                    { mDataProp: "Destination_Number", sTitle: "Destination Number" },
                    { mDataProp: "Package_Name", sTitle: "Package Name" },
                    { mDataProp: "Type", sTitle: "Type" },
                    { mDataProp: "Destination_Code", sTitle: "Destination Code" },
                    {
                        mDataProp: "DurationMin_string", sTitle: "Duration (Min:sec)",
                        fnRender: function (ob) {
                            return ob.aData.DurationMin_string + " Min";
                        }
                    },
                    {
                        mDataProp: "DataUsage_String", sTitle: "Data Usage(in MB)",
                        fnRender: function (ob) {
                            return ob.aData.DataUsage_String + " MB";
                        }
                    },
                    { mDataProp: "Tariff_Class", sTitle: "Tariff Class" },
                    { mDataProp: "Roaming_Zone", sTitle: "Roaming Zone" },
                    { mDataProp: "Net_Charges", sTitle: "Net Charges" },
                    { mDataProp: "Balance_String", sTitle: "Balance" },
                    {
                        mDataProp: "Status", sTitle: "Within Bundle",
                        fnRender: function (ob) {
                            if (ob.aData.Status == 1) {
                                return "Yes";
                            }
                            else {
                                return "No";
                            }
                        }
                    }
                ]
            });
            if (oTable.length > 0) {
                oTable.fnAdjustColumnSizing();
            }
            $("#tblCHFilterResult_wrapper .row-fluid:eq(0)").remove();
            $("#tblCHFilterResult_wrapper .row-fluid .span6:eq(0)").addClass("span4");
            $("#tblCHFilterResult_wrapper .row-fluid .span6:eq(1)").addClass("span8");
            $("#tblCHFilterResult_wrapper .row-fluid .span6").removeClass("span6");

            var DownloadAnchor = "<a id=\"downloadAnchor\" style=\"display:none;\"></a>";
            var btnExportHistory = "<button class=\"btn blue\" id=\"btnExportCDRHistory\" name=\"btnExportCDRHistory\">Download to Excel</button>";
            $("#tblCHFilterResult_wrapper .row-fluid .span4").append(DownloadAnchor + btnExportHistory).show();
        },
        error: function (feedback) {
            $("#chFilterResultBody").css({ "background-image": "none" }).html("").append("No  history found.").show();
        }
    });
}
/* ----------------------------------------------------- 
   *  eof TAB CDR HISTORY 
   ===================================================== */


