﻿/* ===================================================== 
   *  TAB DAILY USAGE HISTORY  
   ----------------------------------------------------- */

$(document).ready(function () {
    //Start Date is two months back and End Date is Current Date
    var todaystart = new Date();
    var dd = 1;
    var mm = todaystart.getMonth() + 1; //January is 0!
    var yyyy = todaystart.getFullYear();
    if (mm == 1) {
        mm = 11;
        yyyy = yyyy - 1;
    }
    else if (mm == 2) {
        mm = 12;
        yyyy = yyyy - 1;
    }
    else {
        mm = mm - 2;
    }
    if (dd < 10) { dd = '0' + dd } if (mm < 10) { mm = '0' + mm } todaystart = dd + '/' + mm + '/' + yyyy;

    var todayend = new Date();
    var dd = todayend.getDate();
    var mm = todayend.getMonth() + 1; //January is 0!
    var yyyy = todayend.getFullYear();
    if (dd < 10) { dd = '0' + dd } if (mm < 10) { mm = '0' + mm } todayend = dd + '/' + mm + '/' + yyyy;

    var lastdate1 = $('#lastdate1').val();
    if (lastdate1 == '') {
        $('#dp1').attr('data-date', todaystart);
        $('#dp1').datepicker().on('changeDate', function (e) {
            $('div.datepicker').hide();
        });
        $('#startdate').attr('value', todaystart);
        $('#lastdate1').val(todaystart);
    }
    else {
        $('#dp1').attr('data-date', lastdate1);
        $('#dp1').datepicker().on('changeDate', function (e) {
            $('div.datepicker').hide();
        });
        $('#startdate').attr('value', lastdate1);
    }

    var lastdate2 = $('#lastdate2').val();
    if (lastdate2 == '') {
        $('#dp2').attr('data-date', todayend);
        $('#dp2').datepicker().on('changeDate', function (e) {
            $('div.datepicker').hide();
        });
        $('#enddate').attr('value', todayend);
        $('#lastdate2').val(todayend);
    }
    else {
        $('#dp2').attr('data-date', lastdate2);
        $('#dp2').datepicker().on('changeDate', function (e) {
            $('div.datepicker').hide();
        });
        $('#enddate').attr('value', lastdate2);
    }

});

/* ----------------------------------------------------------------
 * Function Name    : DailyUsageHistory
 * Purpose          : to show usage history of Msisdn
 * Added by         : Edi Suryadi
 * Create Date      : February 25th, 2019
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function DailyUsageHistory(Msisdn, Sitecode, startDate, endDate) {
    $("#pthFilterResultBody").html("").append("<table class=\"table table-bordered table-hover\" id=\"tblPTHFilterResult\"></table>");
    $("#pthFilterResultBody").css({ "min-height": "100px", "background-image": "url('/Assets/Main/img/ajaxloader-02.gif')" });

    var url = apiServer + '/api/topUp/';
    jQuery.support.cors = true;
    var JSONSendData = {
        Msisdn: Msisdn,
        SearchType: 4,
        Sitecode: Sitecode,
        StartDate: startDate,
        EndDate: endDate,
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#pthFilterResultBody").css({ "background-image": "none" });
            $('#tblPTHFilterResult').dataTable({
                bDestroy: true,
                bRetrieve: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: false,
                bInfo: false,
                iDisplayLength: 10,
                aaSorting: [],
                oLanguage: {
                    "sInfoEmpty": '',
                    "sEmptyTable": "No Daily Usage history found."
                },
                aoColumns: [
                    { mDataProp: "BREAKAGE_TYPE", sTitle: "Daily usage" },
                    { mDataProp: "CALL", sTitle: "Calls" },
                    { mDataProp: "DATA", sTitle: "Data" },
                    { mDataProp: "SMS", sTitle: "SMS" }
                ]
            });
            $("#tblPTHFilterResult_wrapper .row-fluid:eq(0)").remove();
            $("#tblPTHFilterResult_wrapper .row-fluid .span6:eq(0)").addClass("span4");
            $("#tblPTHFilterResult_wrapper .row-fluid .span6:eq(1)").addClass("span8");
            $("#tblPTHFilterResult_wrapper .row-fluid .span6").removeClass("span6");
        },
        error: function (feedback) {
            $("#pthFilterResultBody").css({ "background-image": "none" }).html("").append("No Daily Usage history found.").show();
        }
    });
}

/* ----------------------------------------------------- 
   *  eof TAB DAILY USAGE HISTORY 
   ===================================================== */


