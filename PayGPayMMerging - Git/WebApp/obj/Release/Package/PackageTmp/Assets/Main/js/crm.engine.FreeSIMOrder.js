﻿/* ===================================================== 
   *  FREE SIM ORDER
   ----------------------------------------------------- */

function PlaceOrder(sitecode, firstname, lastname, houseno, address1, address2, city, postcode, state, mobilephone, email, subscriberchannel, sourceaddress, ordersimurl, ip_address, nb_of_sim) {
    $("#tdSubscriberId").html("");
    $("#tdFreeSimId").html("");
    $("#ajax-screen-masking").show();
    var url = apiServer + '/api/FreeSIMOrder';;
    jQuery.support.cors = true;
    var JSONSendData = {
        sitecode: sitecode,
        firstname: firstname,
        lastname: lastname,
        houseno: houseno,
        address1: address1,
        address2: address2,
        city: city,
        postcode: postcode,
        state: state,
        mobilephone: mobilephone,
        email: email,
        subscriberchannel: subscriberchannel,
        sourceaddress: sourceaddress,
        ordersimurl: ordersimurl,
        ip_address: ip_address,
        nb_of_sim: nb_of_sim
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            if (feedback[0].errcode == 0) {
                Clear();
                $("#tdSubscriberId").html(feedback[0].subsId);
                $("#tdFreeSimId").html(feedback[0].freesimid_out);
                $("#SuccessMessage").modal("show");
            }
            else {
                alert(feedback[0].errmsg);
                $("#lblFSOError").html(feedback[0].errmsg);
            }
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}

function Clear() {
    $("#txtFSOFirstName").val("");
    $("#txtFSOLastName").val("");
    $("#txtFSOHouseNo").val("");
    $("#txtFSOAddress1").val("");
    $("#txtFSOAddress2").val("");
    $("#txtFSOCity").val("");
    $("#txtFSOState").val("");
    $("#txtFSOPostcode").val("");
    $("#txtFSOMobileNo").val("");
    $("#txtFSOEmail").val("");
    $("#txtFSOQty").val("1");
    $("#lblFSOError").html("");
    $("#tdSubscriberId").html("");
    $("#tdFreeSimId").html("");
}

function Validate() {
    $("#lblFSOError").html("");
    if ($("#txtFSOFirstName").val().trim() != "") {
        if ($("#txtFSOLastName").val().trim() != "") {
            if ($("#txtFSOHouseNo").val().trim() != "") {
                if ($("#txtFSOAddress1").val().trim() != "") {
                    //if ($("#txtFSOAddress2").val().trim() != "") {
                        if ($("#txtFSOCity").val().trim() != "") {
                            //if ($("#txtFSOState").val().trim() != "") {
                                if ($("#txtFSOPostcode").val().trim() != "") {
                                    if ($("#txtFSOMobileNo").val().trim() != "") {
                                        if ($.isNumeric($("#txtFSOMobileNo").val())) {
                                            if ($("#txtFSOEmail").val().trim() != "") {
                                                if (ValidateEmail($("#txtFSOEmail").val())) {
                                                    if ($("#txtFSOQty").val().trim() != "") {
                                                        return true;
                                                    }
                                                    else {
                                                        $('#lblFSOError').html("Enter Qty!");
                                                    }
                                                }
                                                else {
                                                    $("#lblFSOError").html("Invalid Email Address.");
                                                }
                                            } else {
                                                $('#lblFSOError').html("Enter Email!");
                                            }
                                        } else {
                                            $("#lblFSOError").html("Invalid Mobile No.");
                                        }
                                    } else {
                                        $('#lblFSOError').html("Enter Mobile Phone!");
                                    }
                                } else {
                                    $('#lblFSOError').html("Enter Postcode!");
                                }
                            //} else {
                            //    $('#lblFSOError').html("Enter State!");
                            //}
                        } else {
                            $('#lblFSOError').html("Enter City!");
                        }
                    } else {
                        $('#lblFSOError').html("Enter Address1!");
                    }
                //} else {
                //    $('#lblFSOError').html("Enter Address!");
                //}
            } else {
                $('#lblFSOError').html("Enter House No!");
            }
        } else {
            $('#lblFSOError').html("Enter Last Name!");
        }
    } else {
        $('#lblFSOError').html("Enter First Name!");
    }
    return false;
}

function ValidateEmail(email) {
    var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    return expr.test(email);
};

/* ----------------------------------------------------- 
   *  eof FREE SIM ORDER 
   ===================================================== */
