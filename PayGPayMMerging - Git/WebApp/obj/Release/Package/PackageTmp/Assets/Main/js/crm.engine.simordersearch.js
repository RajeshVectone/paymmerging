﻿/* ===================================================== 
   *  SIM ORDER SEARCH 
   ----------------------------------------------------- */

function orderSearch(action_type, search_text, fromdate, todate) {
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    var url = apiServer + '/api/simorder/';
    jQuery.support.cors = true;
    var JSONSendData = {
        action_type: action_type,
        search_text: search_text,
        fromdate: fromdate,
        todate: todate
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#orderSearchContainer").hide();
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            $("#orderSearchContainer").show();
            $('#tbOrderSearch').fadeIn();
            $('#tbOrderSearch').dataTable({
                bDestroy: true,
                aaData: feedback,
                bPaginate: true,
                bFilter: false,
                bInfo: false,
                aaSorting: [[0, "asc"]],
                aoColumns: [
                    {
                        mDataProp: "freesim_order_id", sTitle: 'Free SIM ID',
                        fnRender: function (ob) {
                            return "<center><a href=\"javascript:void(0)\" class=\"orderIDAnchor\" alt=\"" + ob.aData.freesim_order_id + '|' + search_text + '|' + fromdate + '|' + todate + "\">" + ob.aData.freesim_order_id + "</a></center>";
                        }
                    },
                    { mDataProp: "title", sTitle: 'Title' },
                    { mDataProp: "first_name", sTitle: 'First Name' },
                    { mDataProp: "last_name", sTitle: 'Last Name' },
                    { mDataProp: "order_status", sTitle: 'Status' },
                    { mDataProp: "order_date_string", sTitle: 'Register Date' },
                    { mDataProp: "cybersource_id", sTitle: 'Cybersource ID' },
                    { mDataProp: "quantity", sTitle: 'Quantity' },
                    { mDataProp: "source_reg", sTitle: 'Source' },
                    { mDataProp: "order_source", sTitle: 'Source Address' },
                    { mDataProp: "ordersim_url", sTitle: 'Ordersim URL' }


                ],
                fnDrawCallback: function (ob) {
                    if (ob.fnRecordsTotal() <= ob._iDisplayLength) {
                        $('.dataTables_paginate').hide();
                    }
                    $("#tbOrderSearch_wrapper .row-fluid:eq(0) .span6:eq(1)").remove();
                    $("#tbOrderSearch_wrapper .row-fluid:eq(0) .span6").html("").addClass("span12").removeClass("span6");
                    var SearchType = $("select[name=SearchType] option:selected").text();
                    var orderType = $("select[name=orderType] option:selected").text();
                    var searchResultStr = "Searching <b>" + SearchType + "</b> (" + orderType + ") from " + $("input#startdate").val() + " to " + $("input#enddate").val() + " : " + ob.fnRecordsTotal() + " result data.";
                    $("#tbOrderSearch_wrapper .row-fluid:eq(0) .span12").html(searchResultStr);
                    $("#tbOrderSearch td").css({ "font-size": "12px" });
                    $("#tbOrderSearch td a").css({ "text-decoration": "underline" });
                }
            });
        },
        error: function () {
            $("#ajax-screen-masking").hide();
        }
    });
}

function orderDetail(orderID, search_text, fromdate, todate) {
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });

    var url = apiServer + '/api/simorder/';
    jQuery.support.cors = true;
    var JSONSendData = {
        action_type: 17,
        search_text: search_text,
        fromdate: fromdate,
        todate: todate,
        freesim_order_id: orderID
    };

    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (feedback) {
            $("#ajax-screen-masking").hide();

            $('table.tblOrderDetail #tdODTitle').html(feedback.title);
            $('table.tblOrderDetail #tdODFirstName').html(feedback.first_name);
            $('table.tblOrderDetail #tdODLastName').html(feedback.last_name);
            $('table.tblOrderDetail #tdODHouseNo').html(feedback.address1);
            $('table.tblOrderDetail #tdODAddress').html(feedback.address2);
            $('table.tblOrderDetail #tdODCity').html(feedback.town);
            $('table.tblOrderDetail #tdODPostcode').html(feedback.postcode);
            $('table.tblOrderDetail #tdODMobilePhone').html(feedback.mobileno);
            $('table.tblOrderDetail #tdODEmail').html(feedback.email);
            $('table.tblOrderDetail #tdODStatus').html(feedback.order_status);
            $('table.tblOrderDetail #tdODSourceAddress').html(feedback.order_source);
            $('table.tblOrderDetail #tdODOrderSIMURL').html(feedback.ordersim_url);
            $('table.tblOrderDetail #tdODRegDate').html(feedback.order_date_string);
            $('table.tblOrderDetail #tdODSentDate').html(feedback.sent_date_string);
            $('table.tblOrderDetail #tdODActivateDate').html(feedback.activate_date_string);
            $('#orderDetail').modal({ keyboard: false, backdrop: 'static' }).css({
                'margin-left': function () {
                    return window.pageXOffset - ($(this).width() / 2);
                }
            });
            $("[name=input_iccid]").focus();
        },
        error: function () {
            $("#ajax-screen-masking").hide();
        }
    });
}

/* ----------------------------------------------------------------
 * Function Name    : orderDetailSubmit 
 * Purpose          : To submit order detail 
 * Added by         : Edi Suryadi
 * Create Date      : August 30th, 2013
 * Last Update      : -
 * Update History   : -
 * ---------------------------------------------------------------- */
function orderDetailSubmit(mobileno, destno) {

    var url = apiServer + '/api/customercsb/';
    var ajaxSpinnerTop = $(window).outerHeight() / 2;
    $("#ajax-loader").css({ "margin-top": ajaxSpinnerTop + "px" });
    $("#ajax-screen-masking").css({ "height": $(document).outerHeight() + "px" });
    jQuery.support.cors = true;
    var JSONSendData = {
        order_records: 'xxxxx',
        mobileno: mobileno,
        destno: destno
    };

    $("#CSBCPMBundle").hide();
    $(".modal-backdrop").hide();

    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        beforeSend: function () {
            $("#ajax-screen-masking").show();
        },
        success: function (data) {
            $("#ajax-screen-masking").hide();

            $('.PMSuccess .modal-footer .btnPMSuccessOK').attr({ "alt": "1" });
            $('.PMFailed .modal-footer .btnPMFailedOK').attr({ "alt": "1" });

            var stat = data.errcode;
            if (stat == 0) {
                $('.PMSuccess .modal-header').html(data.errsubject);
                $('.PMSuccess .modal-body').html(data.errmsg);
                $('.PMSuccess').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            } else {
                $('.PMFailed .modal-header').html(data.errsubject);
                $('.PMFailed .modal-body').html(data.errmsg);
                $('.PMFailed').modal({ keyboard: false, backdrop: 'static' }).css({
                    'margin-left': function () {
                        return window.pageXOffset - ($(this).width() / 2);
                    }
                });
            }
        }
    });
}


/* ----------------------------------------------------- 
   *  eof SIM ORDER SEARCH 
   ===================================================== */



