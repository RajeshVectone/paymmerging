﻿/* ===================================================== 
   *  PAYMENT 
   ----------------------------------------------------- */

function GetCustomerWisePaymentBreakup(sitecode, searchby, searchvalue, date_from, date_to) {
    $("#ajax-screen-masking").show();
    $("#TablePaymentBreakupContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"TablePaymentBreakup\"></table>");
    var url = apiServer + '/api/Payment';
    jQuery.support.cors = true;
    var JSONSendData = {
        paymentSearchType: 1,
        sitecode: sitecode,
        searchby: searchby,
        searchvalue: searchvalue,
        date_from: date_from,
        date_to: date_to
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            if (feedback != null) {
                $('#TablePaymentBreakup').dataTable({
                    bDestroy: true,
                    bRetrieve: true,
                    aaData: feedback,
                    bPaginate: false,
                    bFilter: false,
                    bInfo: false,
                    bAutoWidth: false,
                    oLanguage: {
                        "sInfoEmpty": '',
                        "sEmptyTable": "No records found."
                    },
                    aoColumns: [
                        { mDataProp: "payment_mode", sTitle: "payment mode", sWidth: 250 },
                        { mDataProp: "purchase_amount", sTitle: "purchase amount", sWidth: 50 }
                    ],
                    fnDrawCallback: function () {
                    }
                });
                $('#TablePaymentBreakupContainer tr:last').css("font-weight", "bold");
            }
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}

function GetCustomerWisePaymentDetails(sitecode, searchby, searchvalue, date_from, date_to) {
    $("#ajax-screen-masking").show();
    $("#TablePaymentDetailsContainer").html("").append("<table class=\"table table-bordered table-hover\" id=\"TablePaymentDetails\"></table>");
    var url = apiServer + '/api/Payment';
    jQuery.support.cors = true;
    var JSONSendData = {
        paymentSearchType: 2,
        sitecode: sitecode,
        searchby: searchby,
        searchvalue: searchvalue,
        date_from: date_from,
        date_to: date_to
    };
    $.ajax
    ({
        url: url,
        type: 'post',
        data: JSON.stringify(JSONSendData),
        dataType: 'json',
        contentType: "application/json;charset=utf-8",
        cache: false,
        success: function (feedback) {
            $("#ajax-screen-masking").hide();
            if (feedback != null) {
                $('#TablePaymentDetails').dataTable({
                    bDestroy: true,
                    bRetrieve: true,
                    aaData: feedback,
                    iDisplayLength: 10,
                    bPaginate: true,
                    bFilter: true,
                    bInfo: false,
                    bAutoWidth: false,
                    oLanguage: {
                        "sInfoEmpty": '',
                        "sEmptyTable": "No records found."
                    },
                    aoColumns: [
                        { mDataProp: "account_id", sTitle: "Mobile No" },
                        { mDataProp: "payment_mode", sTitle: "Payment Mode" },
                        { mDataProp: "reference_id", sTitle: "Reference ID" },
                        { mDataProp: "amount", sTitle: "Amount", sWidth: 50 },
                        { mDataProp: "currency", sTitle: "Currency", sWidth: 50 },
                        { mDataProp: "payment_date_string", sTitle: "Payment Date" },
                        { mDataProp: "email", sTitle: "Email" },
                        { mDataProp: "IPpaymentclient", sTitle: "IP Client" },
                        { mDataProp: "ReplyMessage", sTitle: "Reply Meesage" },
                        { mDataProp: "CC_NO", sTitle: "CC NO", sWidth: 50 },
                        {
                            mDataProp: "service_added", sTitle: "Service Added", sWidth: 100, bVisible : searchby == "2" ? true: false,
                            fnRender: function (ob) {
                                return ob.aData.service_added == 0 ? "No" : "Yes";
                            }
                        }
                    ],
                    fnDrawCallback: function () {
                    }
                });
            }
        },
        error: function (feedback) {
            $("#ajax-screen-masking").hide();
        }
    });
}
/* ----------------------------------------------------- 
   *  eof PAYMENT 
   ===================================================== */



