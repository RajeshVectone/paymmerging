﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRM3.WebApp.Attributes;
using CRM_API.Models;

namespace CRM3.WebApp.Controllers
{
    public class PAYMController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        // GET: /PAYM/Detail1/447574659865
        //Menus - 1.TabGeneral 2.TabCallSettings 3.TabSIMDetails
        public ActionResult Detail1(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            try
            {
                switch (model.ActiveTab)
                {
                    case "TabGeneral":
                        return Redirect(string.Format("/PAYM/TabGeneral/{0}-{1}", id, SIM));
                    case "TabCallSettings":
                        return Redirect(string.Format("/PAYM/TabCallSettings/{0}-{1}", id, SIM));
                    case "TabSIMDetails":
                        return Redirect(string.Format("/PAYM/TabSIMDetails/{0}-{1}", id, SIM));
                    case "TabSMSCampaignSettings":
                        return Redirect(string.Format("/PAYM/TabSMSCampaignSettings/{0}-{1}", id, SIM));
                    case "TabTraiffDetails":
                        return Redirect(string.Format("/Customer/TabTariffDetails/{0}-{1}", id, SIM));
                    case "TabRoamingDetails":
                        return Redirect(string.Format("/PAYM/TabRoamingDetails/{0}-{1}", id, SIM));
                    case "TabAutoRenewal":
                        return Redirect(string.Format("/PAYM/TabAutoRenewal/{0}-{1}", id, SIM));
                    default:
                        return Redirect(string.Format("/PAYM/TabGeneral/{0}-{1}", id, SIM));
                }
            }
            catch (Exception)
            {

                return Redirect(string.Format("/PAYM/TabGeneral/{0}-{1}", id, SIM));
            }

        }

        // GET: /PAYM/TabGeneral/MobileNo
        public ActionResult TabGeneral(string id)
        {
            try
            {
                string SIM = id.Split('-')[1];
                id = id.Split('-')[0];

                if (id != null)
                {
                    if (HttpContext.Session["GlobalProduct"] == null)
                    {
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        HttpContext.Session["GlobalProduct"] = _product.mundio_product;
                    }
                }

                if (HttpContext.Session["GlobalProduct"] != null)
                {
                    if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                        return RedirectToAction("Index", "Product");
                    else
                    {
                        var page = new CustomerDetailPage();
                        try
                        {
                            //var _CustDetails = CRM_API.DB.Customer.SProc.Search_Cust(HttpContext.Session["GlobalProduct"].ToString(), "MobileNo", id);
                            ////07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                            //ViewBag._CustId = _CustDetails.CustomerID == 0 ? "N/A" : Convert.ToString(_CustDetails.CustomerID);
                            //ViewBag._ConnStatus = _CustDetails.ConnectionStatus;
                            //HttpContext.Session["CustId"] = ViewBag._CustId;
                            //HttpContext.Session["ConnStatus"] = _CustDetails.ConnectionStatus;

                            var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                            page.ActiveTab = "TabGeneral";
                            page.MobileNo = id;
                            //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                            page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);

                            page.Detail2in1 = new Customer2in1(id);

                            //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                            ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                            ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                            HttpContext.Session["CustId"] = ViewBag._CustId;
                            HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                            ViewBag._mobileno = id;
                            ViewBag._sitecode = _product.sitecode;
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                            ViewBag._iccid = page.Customer.iccid;
                            ViewBag._balance = page.Customer.balance;
                            ViewBag._tariffclass = page.Customer.tariffclass;
                            ViewBag._firstname = page.Customer.first_name;
                            ViewBag._lastname = page.Customer.last_name;
                            ViewBag._email = page.Customer.Email;
                            ViewBag._subscriberid = page.Customer.SubscriberID;
                            ViewBag.SIM = SIM;
                        }
                        catch (Exception ex)
                        { page.ErrMsg = ex.Message; }
                        return View("Detail1", page);
                    }
                }
                else
                { return RedirectToAction("Index", "Product"); }
            }
            catch (Exception)
            {
                return RedirectToAction("Search", "Customer");
                throw;
            }
        }

        //Added by karthik Jira:CRMT-196
        // GET: /PAYM/TabTariffDetails/MobileNo
        public ActionResult TabTariffDetails(string id)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                    return RedirectToAction("Index", "Product");
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        //var _CustDetails = CRM_API.DB.Customer.SProc.Search_Cust(HttpContext.Session["GlobalProduct"].ToString(), "MobileNo", id);
                        ////07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        //ViewBag._CustId = _CustDetails.CustomerID == 0 ? "N/A" : Convert.ToString(_CustDetails.CustomerID);
                        //ViewBag._ConnStatus = _CustDetails.ConnectionStatus;
                        //HttpContext.Session["CustId"] = ViewBag._CustId;
                        //HttpContext.Session["ConnStatus"] = _CustDetails.ConnectionStatus;

                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        page.ActiveTab = "TabTariffDetails";
                        page.MobileNo = id;
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    { page.ErrMsg = ex.Message; }
                    return View("Detail1", page);
                }
            }
            else
            { return RedirectToAction("Index", "Product"); }
        }

        // GET: /PAYM/TabCallSettings/MobileNo
        public ActionResult TabCallSettings(string id)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                    return RedirectToAction("Index", "Product");
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        //ViewBag._CustId = HttpContext.Session["CustId"].ToString();
                        //ViewBag._ConnStatus = HttpContext.Session["ConnStatus"].ToString();

                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        page.ActiveTab = "TabCallSettings";
                        page.MobileNo = id;
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;

                    }
                    catch (Exception ex)
                    { page.ErrMsg = ex.Message; }
                    return View("Detail1", page);
                }
            }
            else
            { return RedirectToAction("Index", "Product"); }
        }

        // GET: /PAYM/TabSIMDetails/MobileNo
        public ActionResult TabSIMDetails(string id)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                    return RedirectToAction("Index", "Product");
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        //ViewBag._CustId = HttpContext.Session["CustId"].ToString();
                        //ViewBag._ConnStatus = HttpContext.Session["ConnStatus"].ToString();
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        page.ActiveTab = "TabSIMDetails";
                        page.MobileNo = id;
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    { page.ErrMsg = ex.Message; }
                    return View("Detail1", page);
                }
            }
            else
            { return RedirectToAction("Index", "Product"); }
        }

        public ActionResult TabSMSCampaignSettings(string id)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                    return RedirectToAction("Index", "Product");
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        //ViewBag._CustId = HttpContext.Session["CustId"].ToString();
                        //ViewBag._ConnStatus = HttpContext.Session["ConnStatus"].ToString();
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        page.ActiveTab = "TabSMSCampaignSettings";
                        page.MobileNo = id;
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    { page.ErrMsg = ex.Message; }
                    return View("Detail1", page);
                }
            }
            else
            { return RedirectToAction("Index", "Product"); }
        }
        public ActionResult SmsCampaignMarketing(string id)
        {
            var page = new CustomerDetailPage();
            try
            {
                string SIM = id.Split('-')[1];
                id = id.Split('-')[0];

                //ViewBag._CustId = HttpContext.Session["CustId"].ToString();
                //ViewBag._ConnStatus = HttpContext.Session["ConnStatus"].ToString();
                var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                page.ActiveTab = "TabSMSCampaignSettings";
                page.MobileNo = id;
                //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                page.Detail2in1 = new Customer2in1(id);

                //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                HttpContext.Session["CustId"] = ViewBag._CustId;
                HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                ViewBag._mobileno = id;
                ViewBag._sitecode = _product.sitecode;
                ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                ViewBag._iccid = page.Customer.iccid;
                ViewBag._balance = page.Customer.balance;
                ViewBag._tariffclass = page.Customer.tariffclass;
                ViewBag._firstname = page.Customer.first_name;
                ViewBag._firstname = page.Customer.last_name;
                ViewBag._subscriberid = page.Customer.SubscriberID;
            }
            catch (Exception ex)
            { page.ErrMsg = ex.Message; }
            return View("Detail1", page);
        }
        public ActionResult SmsCampaignEmailMarketing(string id)
        {
            var page = new CustomerDetailPage();
            try
            {
                string SIM = id.Split('-')[1];
                id = id.Split('-')[0];

                //ViewBag._CustId = HttpContext.Session["CustId"].ToString();
                //ViewBag._ConnStatus = HttpContext.Session["ConnStatus"].ToString();
                var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                page.ActiveTab = "TabSMSCampaignSettings";
                page.MobileNo = id;
                //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                page.Detail2in1 = new Customer2in1(id);

                //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                HttpContext.Session["CustId"] = ViewBag._CustId;
                HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                ViewBag._mobileno = id;
                ViewBag._sitecode = _product.sitecode;
                ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                ViewBag._iccid = page.Customer.iccid;
                ViewBag._balance = page.Customer.balance;
                ViewBag._tariffclass = page.Customer.tariffclass;
                ViewBag._firstname = page.Customer.first_name;
                ViewBag._firstname = page.Customer.last_name;
                ViewBag._subscriberid = page.Customer.SubscriberID;
            }
            catch (Exception ex)
            { page.ErrMsg = ex.Message; }
            return View("Detail1", page);
        }
        public ActionResult TabSMSCampaignmarketingSettings(string id)
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                    return RedirectToAction("Index", "Product");
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        string SIM = id.Split('-')[1];
                        id = id.Split('-')[0];

                        //ViewBag._CustId = HttpContext.Session["CustId"].ToString();
                        //ViewBag._ConnStatus = HttpContext.Session["ConnStatus"].ToString();
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        page.ActiveTab = "TabSMSCampaignSettings";
                        page.MobileNo = id;
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    { page.ErrMsg = ex.Message; }
                    return View("Detail1", page);
                }
            }
            else
            { return RedirectToAction("Index", "Product"); }
        }
        public ActionResult TabSMSCampaignemailmarketingSettings(string id)
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                    return RedirectToAction("Index", "Product");
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        string SIM = id.Split('-')[1];
                        id = id.Split('-')[0];

                        //ViewBag._CustId = HttpContext.Session["CustId"].ToString();
                        //ViewBag._ConnStatus = HttpContext.Session["ConnStatus"].ToString();
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        page.ActiveTab = "TabSMSCampaignSettings";
                        page.MobileNo = id;
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    { page.ErrMsg = ex.Message; }
                    return View("Detail1", page);
                }
            }
            else
            { return RedirectToAction("Index", "Product"); }
        }
        public ActionResult TabSMSmarketingchangeviewhistory(string id)
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                    return RedirectToAction("Index", "Product");
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        string SIM = id.Split('-')[1];
                        id = id.Split('-')[0];

                        //ViewBag._CustId = HttpContext.Session["CustId"].ToString();
                        //ViewBag._ConnStatus = HttpContext.Session["ConnStatus"].ToString();
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        page.ActiveTab = "TabSMSCampaignSettings";
                        page.MobileNo = id;
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    { page.ErrMsg = ex.Message; }
                    return View("Detail1", page);
                }
            }
            else
            { return RedirectToAction("Index", "Product"); }
        }
        // GET: /PAYM/Detail2/447574659865
        //Menus - 1.TabBundles
        public ActionResult Detail2(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            ViewBag._SimType = SIM;
            switch (model.ActiveTab)
            {
                case "TabBundles":
                    return Redirect(string.Format("/PAYM/TabBundles/{0}-{1}", id, SIM));
                case "TabBundle":
                    return Redirect(string.Format("/PAYM/TabBundle/{0}-{1}", id, SIM));
                case "TabLLOM":
                    return Redirect(string.Format("/PAYM/TabLLOM/{0}-{1}", id, SIM));
                case "TabCountySaver":
                    return Redirect(string.Format("/PAYM/TabCountrySaver/{0}-{1}", id, SIM));
                //Added by Karthik Jira:CRMT-239 
                case "InternetProfile":
                    return Redirect(string.Format("/PAYM/InternetProfile/{0}-{1}", id, SIM));
                default:
                    return Redirect(string.Format("/PAYM/TabBundles/{0}-{1}", id, SIM));
            }

        }

        // GET: /PAYM/TabBundles/MobileNo
        public ActionResult TabBundles(string id)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];


            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                    return RedirectToAction("Index", "Product");
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        //ViewBag._CustId = HttpContext.Session["CustId"].ToString();
                        //ViewBag._ConnStatus = HttpContext.Session["ConnStatus"].ToString();
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        page.ActiveTab = "TabBundles";
                        page.MobileNo = id;
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                        //Added by Elango for Jira CRMT-216
                        ViewBag._Email = page.Customer.Email;
                    }
                    catch (Exception ex)
                    { page.ErrMsg = ex.Message; }
                    return View("Detail2", page);
                }
            }
            else
            { return RedirectToAction("Index", "Product"); }
        }

        // GET: /PAYM/TabGeneral/MobileNo
        public ActionResult TabBundle(string id)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            if (id != null)
            {
                var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                HttpContext.Session["GlobalProduct"] = _product.mundio_product;
            }

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                    return RedirectToAction("Index", "Product");
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        page.ActiveTab = "TabBundle";
                        page.MobileNo = id;
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    { page.ErrMsg = ex.Message; }
                    return View("Detail2", page);
                }
            }
            else
            { return RedirectToAction("Index", "Product"); }
        }

        // GET: /PAYM/TabCallSettings/MobileNo
        public ActionResult TabLLOM(string id)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                    return RedirectToAction("Index", "Product");
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        page.ActiveTab = "TabLLOM";
                        page.MobileNo = id;
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    { page.ErrMsg = ex.Message; }
                    return View("Detail2", page);
                }
            }
            else
            { return RedirectToAction("Index", "Product"); }
        }

        // GET: /PAYM/TabSIMDetails/MobileNo
        public ActionResult TabCountrySaver(string id)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                    return RedirectToAction("Index", "Product");
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        page.ActiveTab = "TabCountrySaver";
                        page.MobileNo = id;
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    { page.ErrMsg = ex.Message; }
                    return View("Detail2", page);
                }
            }
            else
            { return RedirectToAction("Index", "Product"); }
        }

        //Added by Karthik Jira:CRMT-239 
        // GET: /PAYM/InternetProfile/MobileNo
        public ActionResult InternetProfile(string id)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                    return RedirectToAction("Index", "Product");
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        page.ActiveTab = "InternetProfile";
                        page.MobileNo = id;
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    { page.ErrMsg = ex.Message; }
                    return View("Detail2", page);
                }
            }
            else
            { return RedirectToAction("Index", "Product"); }
        }

        // GET: /PAYM/Detail3/447574659865
        //Menus - 1.TabCallHistory
        public ActionResult Detail3(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            ViewBag._SimType = SIM;
            switch (model.ActiveTab)
            {
                case "TabCallHistory":
                    return Redirect(string.Format("/PAYM/TabCallHistory/{0}-{1}", id, SIM));
                default:
                    return Redirect(string.Format("/PAYM/TabCallHistory/{0}-{1}", id, SIM));
            }
        }

        // GET: /PAYM/TabCallHistory/MobileNo
        public ActionResult TabCallHistory(string id)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                    return RedirectToAction("Index", "Product");
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        page.ActiveTab = "TabCallHistory";
                        page.MobileNo = id;
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    { page.ErrMsg = ex.Message; }
                    return View("Detail3", page);
                }
            }
            else
            { return RedirectToAction("Index", "Product"); }
        }

        // GET: /PAYM/Detail4/447574659865
        //Menu - 1. TabBills
        public ActionResult Detail4(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            ViewBag._SimType = SIM;
            switch (model.ActiveTab)
            {
                case "TabBill":
                    return Redirect(string.Format("/PAYM/TabBills/{0}-{1}", id, SIM));
                default:
                    return Redirect(string.Format("/PAYM/TabBills/{0}-{1}", id, SIM));
            }
        }

        // GET: /PAYM/TabBill/MobileNo
        public ActionResult TabBills(string id)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                    return RedirectToAction("Index", "Product");
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {

                        ViewBag.FinanceTab = HttpContext.Session["Finance_TAB"] != null ? HttpContext.Session["Finance_TAB"].ToString() : "CRM";

                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        page.ActiveTab = "TabBills";
                        page.MobileNo = id;
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._lastname = page.Customer.last_name;
                        ViewBag._email = page.Customer.Email;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    { page.ErrMsg = ex.Message; }
                    return View("Detail4", page);
                }
            }
            else
            { return RedirectToAction("Index", "Product"); }
        }

        // GET: /PAYM/Detail4/447574659865
        //Menus - 1. TabPaymentProfile 2. TabPaymentHistroy
        public ActionResult Detail5(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            ViewBag._SimType = SIM;
            switch (model.ActiveTab)
            {
                case "TabPaymentProfile":
                    return Redirect(string.Format("/PAYM/TabPaymentProfile/{0}-{1}", id, SIM));
                case "TabPaymentHistroy":
                    return Redirect(string.Format("/PAYM/TabPaymentHistroy/{0}-{1}", id, SIM));
                case "TabDailyUsageHistory":
                    return Redirect(string.Format("/PAYM/TabDailyUsageHistory/{0}-{1}", id, SIM));
                default:
                    return Redirect(string.Format("/PAYM/TabPaymentHistroy/{0}-{1}", id, SIM));
            }
        }

        // GET: /PAYM/TabPaymentProfile/MobileNo
        public ActionResult TabPaymentProfile(string id)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                    return RedirectToAction("Index", "Product");
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        ViewBag.FinanceTab = HttpContext.Session["Finance_TAB"] != null ? HttpContext.Session["Finance_TAB"].ToString() : "CRM";
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        page.ActiveTab = "TabPaymentProfile";
                        page.MobileNo = id;
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    { page.ErrMsg = ex.Message; }
                    return View("Detail5", page);
                }
            }
            else
            { return RedirectToAction("Index", "Product"); }
        }

        // GET: /PAYM/TabPaymentHistroy/MobileNo
        public ActionResult TabPaymentHistroy(string id)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                    return RedirectToAction("Index", "Product");
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        page.ActiveTab = "TabPaymentHistroy";
                        page.MobileNo = id;
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    { page.ErrMsg = ex.Message; }
                    return View("Detail5", page);
                }
            }
            else
            { return RedirectToAction("Index", "Product"); }
        }

        // GET: /PAYM/Detail6/MobileNo
        //Menus - 1. TabSIMSwap
        public ActionResult Detail6(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            ViewBag._SimType = SIM;
            switch (model.ActiveTab)
            {
                case "TabSIMSwap":
                    return Redirect(string.Format("/PAYM/TabSIMSwap/{0}-{1}", id, SIM));
                default:
                    return Redirect(string.Format("/PAYM/TabSIMSwap/{0}-{1}", id, SIM));
            }
        }

        // GET: /PAYM/TabSIMSwap/MobileNo
        public ActionResult TabSIMSwap(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();

                    try
                    {

                        page.ActiveTab = "TabSIMSwap";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product); 
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }

                    return View("Detail6", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        // GET: /PAYM/Detail7/MobileNo
        //Menus - 1. TabIssuesTracking
        public ActionResult Detail7(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            ViewBag._SimType = SIM;
            switch (model.ActiveTab)
            {
                case "TabIssuesTracking":
                    return Redirect(string.Format("/PAYM/TabIssuesTracking/{0}-{1}", id, SIM));
                default:
                    return Redirect(string.Format("/PAYM/TabIssuesTracking/{0}-{1}", id, SIM));
            }
        }

        // GET: /PAYM/TabIssuesTracking/MobileNo
        public ActionResult TabIssuesTracking(string id, CustomerDetailPage model)
        {
            //18-Jul-2017 : Moorthy : Modified if the simType is empty
            //string SIM = id.Split('-')[1];
            //id = id.Split('-')[0];
            string SIM = "";
            if (id.Split('-').Length > 1 && id.Split('-')[1] != "undefined")
            {
                SIM = id.Split('-')[1];
                id = id.Split('-')[0];
            }
            else
            {
                if (Request.Url.LocalPath.ToLower().Contains("/paym/"))
                    SIM = "PAYM";
                else
                    SIM = "PAYM";
                id = id.Split('-')[0];
            }
            ViewBag._SimType = SIM;
            ViewBag._AgentEmailId = CRM_API.Helpers.Session.CurrentUser.email_id;
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        page.ActiveTab = "TabIssuesTracking";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }
                    return View("Detail7", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        // GET: /PAYM/Detail7/MobileNo
        //Menus - 1. TabIssuesTracking
        public ActionResult Detail8(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            ViewBag._SimType = SIM;
            switch (model.ActiveTab)
            {
                case "TabServiceStatusHistory":
                    return Redirect(string.Format("/PAYM/TabServiceStatusHistory/{0}-{1}", id, SIM));
                case "TabAccountstatus":
                    return Redirect(string.Format("/PAYM/TabAccountstatus/{0}-{1}", id, SIM));
                default:
                    return Redirect(string.Format("/PAYM/TabServiceStatusHistory/{0}-{1}", id, SIM));
            }
        }

        // GET: /PAYM/TabIssuesTracking/MobileNo
        public ActionResult TabServiceStatusHistory(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        page.ActiveTab = "TabServiceStatusHistory";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }
                    return View("Detail8", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        // GET: /PAYM/TabIssuesTracking/MobileNo
        public ActionResult TabAccountstatus(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        page.ActiveTab = "TabAccountstatus";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }
                    return View("Detail8", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        public ActionResult Detail9(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            ViewBag._SimType = SIM;
            switch (model.ActiveTab)
            {
                case "TabSMSCampaignTracking":
                    return Redirect(string.Format("/PAYM/TabSMSCampaignTracking/{0}-{1}", id, SIM));
                default:
                    return Redirect(string.Format("/PAYM/TabSMSCampaignTracking/{0}-{1}", id, SIM));
            }
        }

        public ActionResult TabSMSCampaignTracking(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        //ViewBag._CustId = HttpContext.Session["CustId"].ToString();
                        //ViewBag._ConnStatus = HttpContext.Session["ConnStatus"].ToString();
                        page.ActiveTab = "TabSMSCampaignTracking";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }
                    return View("Detail9", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        public ActionResult TabBasicSubscriptionDetails(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();

                    try
                    {

                        page.ActiveTab = "TabBasicSubscriptionDetails";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }

                    return View("Detail2", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        public ActionResult TabAdditionalCLIDetails(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();

                    try
                    {

                        page.ActiveTab = "TabAdditionalCLIDetails";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }

                    return View("Detail2", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        public ActionResult TabCSB(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();

                    try
                    {

                        page.ActiveTab = "TabCSB";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }

                    return View("Detail2", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        // GET: /PAYM/TabRoamingDetails/MobileNo
        public ActionResult TabRoamingDetails(string id)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                    return RedirectToAction("Index", "Product");
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        //ViewBag._CustId = HttpContext.Session["CustId"].ToString();
                        //ViewBag._ConnStatus = HttpContext.Session["ConnStatus"].ToString();
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        page.ActiveTab = "TabRoamingDetails";
                        page.MobileNo = id;
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    { page.ErrMsg = ex.Message; }
                    return View("Detail1", page);
                }
            }
            else
            { return RedirectToAction("Index", "Product"); }
        }

        // GET: /PayM/TabBreakageUsage/MobileNo
        public ActionResult TabBreakageUsage(string id)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                    return RedirectToAction("Index", "Product");
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        //ViewBag._CustId = HttpContext.Session["CustId"].ToString();
                        //ViewBag._ConnStatus = HttpContext.Session["ConnStatus"].ToString();
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        page.ActiveTab = "TabBreakageUsage";
                        page.MobileNo = id;
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    { page.ErrMsg = ex.Message; }
                    return View("Detail1", page);
                }
            }
            else
            { return RedirectToAction("Index", "Product"); }
        }

        // GET: /PayM/TabPOverview/MobileNo
        public ActionResult TabPOverview(string id)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            CRM_API.Helpers.Session.GetProductByMobile = null;

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                    return RedirectToAction("Index", "Product");
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        page.ActiveTab = "TabPOverview";
                        page.MobileNo = id;
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);

                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    { page.ErrMsg = ex.Message; }
                    return View("_TabPOverview", page);
                }
            }
            else
            { return RedirectToAction("Index", "Product"); }
        }

        // GET: /PayM/TabVectoneXtraApp/MobileNo
        public ActionResult TabVectoneXtraApp(string id)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                    return RedirectToAction("Index", "Product");
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        page.ActiveTab = "TabVectoneXtraApp";
                        page.MobileNo = id;
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);

                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    { page.ErrMsg = ex.Message; }
                    return View("_TabVectoneXtraApp", page);
                }
            }
            else
            { return RedirectToAction("Index", "Product"); }
        }

        //Rajesh: Porting
        public ActionResult TabPorting(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                    return RedirectToAction("Index", "Product");
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        //ViewBag._CustId = HttpContext.Session["CustId"].ToString();
                        //ViewBag._ConnStatus = HttpContext.Session["ConnStatus"].ToString();
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        page.ActiveTab = "TabDetailPorting";
                        page.MobileNo = id;
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    { page.ErrMsg = ex.Message; }
                    return View("_TabPorting", page);
                }
            }
            else
            { return RedirectToAction("Index", "Product"); }
        }

        //TabFreeCredit
        public ActionResult TabFreeCredit(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();

                    try
                    {

                        page.ActiveTab = "TabFreeCredit";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }

                    return View("Detail2", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        // GET: /PAYM/Detail10/MobileNo
        public ActionResult Detail10(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            ViewBag._SimType = SIM;
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                    return RedirectToAction("Index", "Product");
                else
                {
                    switch (model.ActiveTab)
                    {
                        case "OrderManagement":
                            return Redirect(string.Format("/PAYM/OrderManagement/{0}-{1}", id, SIM));
                        default:
                            return RedirectToAction("Index", "Product");
                    }
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        public ActionResult OrderManagement(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            ViewBag._SimType = SIM;
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        page.ActiveTab = "OrderManagement";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }
                    return View("Detail10", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        //25-Feb-2019 : Moorthy : CRMIMP-18 - Daily usage history
        //
        // GET: /5 minutes/TabDailyUsageHistory/MobileNo//
        public ActionResult TabDailyUsageHistory(string id)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                    return RedirectToAction("Index", "Product");
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        page.ActiveTab = "TabDailyUsageHistory";
                        page.MobileNo = id;
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    { page.ErrMsg = ex.Message; }
                    return View("Detail5", page);
                }
            }
            else
            { return RedirectToAction("Index", "Product"); }
        }

        //TabFreeDataBundle
        public ActionResult TabFreeDataBundle(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();

                    try
                    {

                        page.ActiveTab = "TabFreeDataBundle";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;

                        ViewBag._user_role = CRM_API.Helpers.Session.CurrentUser.user_role;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }

                    return View("Detail2", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        // GET: /PAYM/TabComplaints/MobileNo
        public ActionResult TabComplaints(string id, CustomerDetailPage model)
        {
            string SIM = "";
            if (id.Split('-').Length > 1 && id.Split('-')[1] != "undefined")
            {
                SIM = id.Split('-')[1];
                id = id.Split('-')[0];
            }
            else
            {
                if (Request.Url.LocalPath.ToLower().Contains("/paym/"))
                    SIM = "PAYM";
                else
                    SIM = "PAYG";
                id = id.Split('-')[0];
            }
            ViewBag._SimType = SIM;
            ViewBag._AgentEmailId = CRM_API.Helpers.Session.CurrentUser.email_id;
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        page.ActiveTab = "TabComplaints";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._lastname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                        ViewBag._userid = CRM_API.Helpers.Session.CurrentUser.userid;
                        ViewBag._user_role = CRM_API.Helpers.Session.CurrentUser.user_role;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }
                    return View("Detail20", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        // GET: /PAYM/TabAutoRenewal/MobileNo
        public ActionResult TabAutoRenewal(string id)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                    return RedirectToAction("Index", "Product");
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        page.ActiveTab = "TabAutoRenewal";
                        page.MobileNo = id;
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    { 
                        page.ErrMsg = ex.Message; 
                    }
                    return View("Detail1", page);
                }
            }
            else
            { return RedirectToAction("Index", "Product"); }
        }
    }
}
