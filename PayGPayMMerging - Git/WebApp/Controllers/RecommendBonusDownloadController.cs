﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using CRM3.WebApp.Attributes;

namespace CRM3.WebApp.Controllers
{
    [CRM3Auth]
    public class RecommendBonusDownloadController : Controller
    {
        //Added by elango for Jira 135 & 136
        //This is the Actual function that will download the file to the client 
        [HttpGet]
        public JsonResult GoDownLoadReferralHistory(string User_Id, string Referral_Status, string SIM_Status, string Recharge_Mode, string Site_Code)
        {
            var result = CRM_API.DB.PAYGRecommendedBonus.SProc.GetRBReferralHistory(User_Id,Site_Code, Referral_Status, SIM_Status, Recharge_Mode);

            StringBuilder str = new StringBuilder();
            str.Append("<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv=\"content-type\" content=\"text/plain; charset=UTF-8\"/><style>.xlText{ mso-number-format:'@';}</style></head><body>");
            str.Append("<table>");
            str.Append("<thead>");
            str.Append("<h1>Recommended Bonus Referral History</h1>");
            str.Append("<tr>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>SIM Order Date</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Invitee Name</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Referral Status</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>SIM Status</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Amount Recharged</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Value Credited</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Credit Status</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Recharge Mode</th>");
            str.Append("</tr>");
            str.Append("</thead>");
            str.Append("<tbody>");
            foreach (CRM_API.Models.PAYGRBReferralHistory val in result)
            {
                str.Append("<tr>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.SIM_Order_Date.ToString()) ? "" : val.SIM_Order_Date.ToString()) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Invitee_Name) ? "" : val.Invitee_Name) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Referral_Status) ? "" : val.Referral_Status) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.SIM_Status) ? "" : val.SIM_Status) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Amount_Recharged) ? "" : val.Amount_Recharged) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Value_Credited) ? "" : val.Value_Credited) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Credit_Status) ? "" : val.Credit_Status) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Recharge_Mode) ? "" : val.Recharge_Mode) + "</td>");
                str.Append("</tr>");
            }
            str.Append("</tbody>");
            str.Append("</table>");
            str.Append("</body></html>");


            string _random = Guid.NewGuid().ToString();
            TempData[_random] = str.ToString();
            var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = 0, errmsg = "", Text = _random, };
            return Json(resy, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public FileContentResult GoDownLoadReferralHistoryFile(string id)
        {
            var result = (string)TempData[id];
            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=RecommendedBonus_ReferralHistory_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(result.ToString());
            return new FileContentResult(temp, "application/vnd.txt");
        }

        //This is the Actual function that will download the file to the client 
        [HttpGet]
        public JsonResult GoDownLoadTransactionHistory(string User_Id, string Transfer_Type, string Payment_Status, string Site_Code)
        {
            var result = CRM_API.DB.PAYGRecommendedBonus.SProc.GetRBTransactionHistory(User_Id, Site_Code,Transfer_Type, Payment_Status);

            StringBuilder str = new StringBuilder();
            str.Append("<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv=\"content-type\" content=\"text/plain; charset=UTF-8\"/><style>.xlText{ mso-number-format:'@';}</style></head><body>");
            str.Append("<table>");
            str.Append("<thead>");
            str.Append("<h1>Recommended Bonus Transaction History</h1>");
            str.Append("<tr>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Request Date</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Transfer Type</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Transfer Date</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Payment Status</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Bank Account Mapped</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Active Bank Account</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Amount Transferred</th>");
            str.Append("</tr>");
            str.Append("</thead>");
            str.Append("<tbody>");
            foreach (CRM_API.Models.PAYGRBTransactionHistory val in result)
            {
                str.Append("<tr>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Request_Date.ToString()) ? "" : val.Request_Date.ToString()) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Transfer_Type) ? "" : val.Transfer_Type) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Transfer_Date) ? "" : val.Transfer_Date) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Payment_Status) ? "" : val.Payment_Status) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Bank_Account_Mapped) ? "" : val.Bank_Account_Mapped) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Active_Bank_Account) ? "" : val.Active_Bank_Account) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Amount_Transferred) ? "" : val.Amount_Transferred) + "</td>");
                str.Append("</tr>");
            }
            str.Append("</tbody>");
            str.Append("</table>");
            str.Append("</body></html>");


            string _random = Guid.NewGuid().ToString();
            TempData[_random] = str.ToString();
            var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = 0, errmsg = "", Text = _random, };
            return Json(resy, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public FileContentResult GoDownLoadTransactionHistoryFile(string id)
        {
            var result = (string)TempData[id];
            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=RecommendedBonus_TransactionHistory_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(result.ToString());
            return new FileContentResult(temp, "application/vnd.txt");
        }

    }
}
