﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRM3.WebApp.Attributes;
using System.Text;
using System.IO;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace CRM3.WebApp.Controllers
{
    [CRM3Auth]
    public class ReportsDownloadController : Controller
    {
        /* --------------------------------------------------------------------------------------------------------------- */
        /* SIM ORDER LIST */

        [HttpPost]
        public ActionResult DownloadSIMOrderList(CRM_API.Models.SIMOrderRequest listRequest, string CountryName)
        {
            var result = GetSimOrder_Downloadable(listRequest);
            StringBuilder str = new StringBuilder();
            str.Append("<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv=\"content-type\" content=\"text/plain; charset=UTF-8\"/><style>.xlText{ mso-number-format:'@';}</style></head><body>");
            str.Append("<h1>SIM Order List</h1>");
            str.Append("<h3>Country : <span>" + CountryName + "</span></h3>");
            if (string.IsNullOrEmpty(listRequest.searchby))
            {
                str.Append("<h3>Period : " + listRequest.fromdate.ToString() + " - " + listRequest.todate.ToString() + "</h3>");
            }
            str.Append("<table>");
            str.Append("<thead>");
            str.Append("<tr>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>FreeSimId</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Subscriber ID</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Title</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>First Name</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Last Name</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>House No</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Address</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>City</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Post Code</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Email</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Mobile No</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>ICCID</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Freesim Status</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Register Date</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Activate Date</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Sent Date</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Quantity</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Cybersource ID</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Source</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Source Address</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Ordersim URL</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>MSISDN Referrer</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Mail Reference</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Country you call most</th>");          
            str.Append("</tr>");
            str.Append("</thead>");
            str.Append("<tbody>");
            foreach (CRM_API.Models.FreesimOrderItem val in result)
            {
                str.Append("<tr>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (val.freesimid != null ? val.freesimid : "") + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.subscriberid + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.title) ? "" : val.title) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.firstname) ? "" : val.firstname) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.lastname) ? "" : val.lastname) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;' class='xlText'>" + (string.IsNullOrEmpty(val.houseno) ? "" : val.houseno) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Address) ? "" : val.Address) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.city) ? "" : val.city) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.postcode) ? "" : val.postcode) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.email) ? "" : val.email) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.mobilephone) ? "" : val.mobilephone) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.iccid) ? "" : val.iccid) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.freesimstatus) ? "" : val.freesimstatus) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.registerdate) ? "" : val.registerdate) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.activatedate) ? "" : val.activatedate) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.sentdate) ? "" : val.sentdate) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.Quantity + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.CyberSourceId) ? "" : val.CyberSourceId) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.SourceReg) ? "" : val.SourceReg) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.source_address) ? "" : val.source_address) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.ordersim_url) ? "" : val.ordersim_url) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.msisdn_referrer) ? "" : val.msisdn_referrer) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.royal_mail_reference) ? "" : val.royal_mail_reference) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.fav_call_country) ? "" : val.fav_call_country) + "</td>");
                str.Append("</tr>");
            }
            str.Append("</tbody>");
            str.Append("</table>");
            str.Append("</body></html>");

            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=Freesim_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(str.ToString());
            string _random = Guid.NewGuid().ToString();
            TempData[_random] = str.ToString();
            var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = 0, errmsg = "Download Ready", Text = _random };
            return Json(resy, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public FileContentResult GoDownloadSIMOrderList(string id)
        {
            var result = (string)TempData[id];
            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=SIMOrderList_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(result.ToString());
            return new FileContentResult(temp, "application/vnd.ms-excel");
        }

        private IEnumerable<CRM_API.Models.FreesimOrderItem> GetSimOrder_Downloadable(CRM_API.Models.SIMOrderRequest listRequest)
        {
            if (string.IsNullOrEmpty(listRequest.searchby))
            {
                var dateFrom = DateTime.Parse(listRequest.fromdate);
                var dateUntil = DateTime.Parse(listRequest.todate);
                var result = CRM_API.DB.SIMOrder.SProc.GetFreeSimForDownload(dateFrom.ToString("yyyy-MM-dd"), dateUntil.ToString("yyyy-MM-dd"), listRequest.status.ToLower(), listRequest.sitecode);
                var resy = CRM_API.DB.SIMOrder.SProc.Get2in1AllOrder(listRequest.status, dateFrom.ToString("yyyy-MM-dd"), dateUntil.ToString("yyyy-MM-dd"), listRequest.sitecode);
                return result.Union(resy);
            }
            else
            {
                switch (listRequest.searchby.ToLower())
                {
                    case "freesimid":
                        if (listRequest.searchvalue.Contains("M"))
                        {
                            listRequest.order_id = int.Parse(listRequest.searchvalue.Substring(3));
                            return CRM_API.DB.SIMOrder.SProc.Search2in1Order(listRequest);
                        }
                        else
                            return CRM_API.DB.SIMOrder.SProc.GetFreeSimForDownloadByFreesimid(listRequest.searchvalue, listRequest.sitecode);

                    case "fullname":
                        listRequest.full_name = listRequest.searchvalue;
                        var resi = CRM_API.DB.SIMOrder.SProc.GetFreeSimForDownloadByFullname(listRequest.searchvalue, listRequest.sitecode);
                        var resj = resi.Union(CRM_API.DB.SIMOrder.SProc.Search2in1Order(listRequest));
                        //return Request.CreateResponse(HttpStatusCode.OK, resi as IEnumerable<Models.FreesimOrderItem>);
                        return resj as IEnumerable<CRM_API.Models.FreesimOrderItem>;
                    default: throw new ArgumentNullException("Search Key Undifined");
                }
            }
        }
        
        /* eof SIM ORDER LIST */
        /* --------------------------------------------------------------------------------------------------------------- */


        /* --------------------------------------------------------------------------------------------------------------- */
        /* CDR HISTORY */
        [HttpPost]
        public ActionResult DownloadCDRHistory(CRM_API.Models.ReportsMo.reportsMobileReq In)
        {
            var result = GetCDRHistory_Downloadable(In);
            var HistoryName = "";
            StringBuilder str = new StringBuilder();
            str.Append("<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv=\"content-type\" content=\"text/plain; charset=UTF-8\"/></head><body>");
            str.Append("<head><style>.xlLongDate{mso-number-format:'dd-MM-yyyy';}.xlText{mso-number-format:'@';}</style></head>");
          
            switch (In.Step)
            {
                case CRM_API.Models.ReportsMo.Search.Mob:
                    HistoryName = "Customer search by Mobile Number";
                    str.Append("<h1>Customer search by Mobile Number</h1>");
                    FormatDataCallHistory(result, ref str);
                    break;
                case CRM_API.Models.ReportsMo.Search.Date:
                    HistoryName = "Customer search by Date formate";
                    str.Append("<h1>Customer search by Date formate</h1>");
                    FormatDataSMSHistory(result, ref str);
                    break;                
            }
            str.Append("</html>");

            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=" + HistoryName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(str.ToString());
            string _random = Guid.NewGuid().ToString();
            TempData[_random] = str.ToString();
            var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = 0, errmsg = HistoryName , Text = _random, };
            return Json(resy, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public FileContentResult GoDownloadCDRHistory(string id,string historyName)
        {
            var result = (string)TempData[id];
            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=" + historyName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(result.ToString());
            return new FileContentResult(temp, "application/vnd.ms-excel");
        }

        private static void FormatDataCallHistory(dynamic result, ref StringBuilder str)
        {
            str.Append("<table>");
            str.Append("<thead>");
            str.Append("<tr>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Submit Date</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Submit</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Function</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Complaint</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Totall_Calls</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Moblie No</th>");
            str.Append("</tr>");
            str.Append("</thead>");
            str.Append("<tbody>");
            foreach (CRM_API.Models.ReportsMo.reportsMobileRSP val in (result as IEnumerable<CRM_API.Models.ReportsMo.reportsMobileRSP>))
            {
                
                str.Append("<tr>");
                str.Append("<td class='xlLongDate' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.Submit_Date); 
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.Issue_Type + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.Function + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.Complaint + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.Total_Calls + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.MobileNo+ "</td>");

                str.Append("</tr>");
            }
            str.Append("</tbody>");
            str.Append("</table>");
            str.Append("</body>");
        }

        private static void FormatDataSMSHistory(dynamic result, ref StringBuilder str)
        {
            str.Append("<table>");
            str.Append("<thead>");
            str.Append("<tr>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Submit Date</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Submit</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Function</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Complaint</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Totall_Calls</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Mobile No</th>");
            str.Append("</tr>");
            str.Append("</thead>");
            str.Append("<tbody>");
            foreach (CRM_API.Models.ReportsMo.reportsMobileRSP val in (result as IEnumerable<CRM_API.Models.ReportsMo.reportsMobileRSP>))
            {
                str.Append("<tr>");
                str.Append("<td class='xlLongDate' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.Submit_Date+ "</td>"); 
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.Issue_Type+ "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" +val.Function + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" +val.Complaint + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.Total_Calls + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.MobileNo + "</td>");
                str.Append("</tr>");
            }
            str.Append("</tbody>");
            str.Append("</table>");
            str.Append("</body>");
        }

       

        private dynamic GetCDRHistory_Downloadable(CRM_API.Models.ReportsMo.reportsMobileReq In)
        {
            CRM_API.DB.Common.ErrCodeMsg errResult = new CRM_API.DB.Common.ErrCodeMsg()
            {
                errcode = -1,
                errsubject = "General Error",
                errmsg = "Empty New Order"
            };
            try
            {
                switch (In.Step)
                {
                    case CRM_API.Models.ReportsMo.Search.Mob:
                        return GetCallHistory(In);
                    case CRM_API.Models.ReportsMo.Search.Date:
                        return GetSMSHistory(In);
                    default: break;
                }
            }
            catch (Exception ex)
            {
                errResult.errmsg = "WEB-API: " + ex.Message;
                errResult.errcode = -1;
            }
            return errResult;
        }

        private IEnumerable<CRM_API.Models.ReportsMo.reportsMobileRSP> GetCallHistory(CRM_API.Models.ReportsMo.reportsMobileReq In)
        {
            var result = CRM_API.DB.Report.Reported.GetIssueList(In.mobileno, In.sitecode);
            return result.ToArray();
        }
        private IEnumerable<CRM_API.Models.ReportsMo.reportsMobileRSP> GetSMSHistory(CRM_API.Models.ReportsMo.reportsMobileReq In)
        {
            var result = CRM_API.DB.Report.Reported.GetIssueTrackerBy(In.startdate, In.enddate, In.sitecode);
            return result.ToArray();
        }

        

        /* eof CDR HISTORY */
        /* --------------------------------------------------------------------------------------------------------------- */

    }
}
