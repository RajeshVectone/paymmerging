﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRM3.WebApp.Attributes;
using CRM_API.Models;
using System.Data;
using System.Data.OleDb;
using System.Configuration;
using System.Data.SqlClient;
using Dapper;

namespace CRM3.WebApp.Controllers
{
    [CRM3Auth]
    public class LLOTGSearchController : Controller
    {
        //
        // GET: /LLOTGSearch/

        public ActionResult Index()
        {
            return View();
        }


        public ActionResult TabLsearchcustomer(String id, String value, String city, String areaid, String pro)
        {
            //id = integer value
            //value = country
            //city = city
            //areaid = area
            if (TempData["Country"] != null)
            {
                String country = TempData["Country"].ToString();
                ViewBag.CityCountry = country;
                TempData["Country"] = country;
            }
            if (value != null)
            {
                TempData["Country"] = value;
            }
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    HttpContext.Session["GlobalProduct"] = "VMUK";
                }
                else
                {                   
                    try
                    {
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = "MCM";
                    }
                    catch (Exception ex)
                    {
                       
                    }
                }
            }
            else
            {
                HttpContext.Session["GlobalProduct"] = "VMUK";
                ViewBag._sitecode = "MCM";
                ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
            }
            return View();
        }

    }
}
