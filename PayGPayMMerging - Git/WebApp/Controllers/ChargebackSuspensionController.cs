﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRM3.WebApp.Attributes;
using CRM_API.Models;
using System.Data;
using System.Data.OleDb;
using System.Configuration;
using System.Data.SqlClient;
using Dapper;
using System.Text;

namespace CRM3.WebApp.Controllers
{
    [CRM3Auth]
    public class ChargebackSuspensionController : Controller
    {
        // GET: /ChargebackSuspension/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult TabLchargebacksuspension()
        {
            ViewBag._userid = CRM_API.Helpers.Session.CurrentUser.userid;
            ViewBag._roleid = CRM_API.Helpers.Session.CurrentUser.user_role;
            ViewBag._role_id = CRM_API.Helpers.Session.CurrentUser.roleid;

            ViewBag.LoginName = string.IsNullOrEmpty(CRM_API.Helpers.Session.CurrentUser.first_name) ?
                                CRM_API.Helpers.Session.CurrentUser.user_login :
                                string.Format("{0} {1}", CRM_API.Helpers.Session.CurrentUser.first_name, CRM_API.Helpers.Session.CurrentUser.last_name);

            if (TempData["Country"] != null)
            {
                String country = TempData["Country"].ToString();
                ViewBag.CityCountry = country;
                TempData["Country"] = country;
            }
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    HttpContext.Session["GlobalProduct"] = "VMUK";
                }
                else
                {
                    ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    ViewBag._sitecode = "MCM";
                }
            }
            else
            {
                HttpContext.Session["GlobalProduct"] = "VMUK";
                ViewBag._sitecode = "MCM";
                ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
            }
            return View();
        }
    }
}

