﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRM3.WebApp.Attributes;
using CRM_API.Models;
using System.Net.Mail;
using System.Configuration;
using System.Net.Http;

namespace CRM3.WebApp.Controllers
{
    [CRM3Auth]
    public class CustomerController : Controller
    {
        //
        // GET: /Customer/

        public ActionResult Index()
        {
            return RedirectToAction("Search", "Customer");
        }


        //
        // POST: /Customer/Search

        public ActionResult Search()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    //ViewBag._Finance = "CRM";
                    HttpContext.Session["Finance_TAB"] = "CRM";
                    return View();
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }

        }


        //
        // GET: /Customer/Detail1/MobileNo
        public ActionResult Detail1(string id, CustomerDetailPage model)
        {

            try
            {

                string SIM = id.Split('-')[1];
                id = id.Split('-')[0];
                ViewBag._SimType = SIM;
                switch (model.ActiveTab)
                {
                    case "TabGeneral":
                        return Redirect(string.Format("/Customer/TabGeneral/{0}-{1}", id, SIM));
                    case "TabCallSettings":
                        return Redirect(string.Format("/Customer/TabCallSettings/{0}-{1}", id, SIM));
                    case "TabSIMDetails":
                        return Redirect(string.Format("/Customer/TabSIMDetails/{0}-{1}", id, SIM));
                    case "TabSMSCampaignSettings":
                        return Redirect(string.Format("/PAYM/TabSMSCampaignSettings/{0}-{1}", id, SIM));
                    case "TabTraiffDetails":
                        return Redirect(string.Format("/Customer/TabTariffDetails/{0}-{1}", id, SIM));
                    case "TabAutoRenewal":
                        return Redirect(string.Format("/Customer/TabAutoRenewal/{0}-{1}", id, SIM));
                    default:
                        return Redirect(string.Format("/Customer/TabGeneral/{0}-{1}", id, SIM));
                }
            }
            catch (Exception)
            {
                return Redirect(string.Format("/Customer/TabGeneral/{0}", id));
            }
        }
        //
        // GET: /Customer/TabGeneral/MobileNo
        public ActionResult TabGeneral(string id)
        {
            try
            {
                string SIM = id.Split('-')[1];
                id = id.Split('-')[0];

                if (id != null)
                {
                    var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                    if (HttpContext.Session["GlobalProduct"] == null)
                    {
                        HttpContext.Session["GlobalProduct"] = _product.mundio_product;
                    }
                    HttpContext.Session["SiteCode"] = _product.sitecode;
                }

                if (HttpContext.Session["GlobalProduct"] != null)
                {
                    if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                    {
                        return RedirectToAction("Index", "Product");
                    }
                    else
                    {
                        var page = new CustomerDetailPage();
                        try
                        {
                            ////06-Aug-2015 : Moorthy Added : as per PaymController method
                            //var _CustDetails = CRM_API.DB.Customer.SProc.Search_Cust(HttpContext.Session["GlobalProduct"].ToString(), "MobileNo", id);
                            ////07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                            //ViewBag._CustId = _CustDetails.CustomerID == 0 ? "N/A" : Convert.ToString(_CustDetails.CustomerID);
                            //ViewBag._ConnStatus = _CustDetails.ConnectionStatus;
                            //HttpContext.Session["CustId"] = ViewBag._CustId;
                            //HttpContext.Session["ConnStatus"] = _CustDetails.ConnectionStatus;

                            var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                            page.ActiveTab = "TabGeneral";
                            page.MobileNo = id;
                            //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                            page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                            page.Detail2in1 = new Customer2in1(id);

                            //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                            ViewBag._CustId = page.Customer.CustomerID;// == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                            ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                            HttpContext.Session["CustId"] = ViewBag._CustId;
                            HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                            ViewBag._mobileno = id;
                            ViewBag._sitecode = _product.sitecode;
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                            ViewBag._iccid = page.Customer.iccid;
                            ViewBag._balance = page.Customer.balance;
                            ViewBag._tariffclass = page.Customer.tariffclass;
                            ViewBag._firstname = page.Customer.first_name;
                            ViewBag._lastname = page.Customer.last_name;
                            ViewBag._email = page.Customer.Email;
                            ViewBag._subscriberid = page.Customer.SubscriberID;

                        }
                        catch (Exception ex)
                        {
                            page.ErrMsg = ex.Message;
                        }

                        return View("Detail1", page);
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Product");
                }
            }
            catch (Exception)
            {
                
                return RedirectToAction("Search", "Customer");
            }
        }

        //Added by karthik Jira:196
        //
        // GET: /Customer/TabTariffDetails/MobileNo

        public ActionResult TabTariffDetails(string id)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        //var _CustDetails = CRM_API.DB.Customer.SProc.Search_Cust(HttpContext.Session["GlobalProduct"].ToString(), "MobileNo", id);
                        ////07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        //ViewBag._CustId = _CustDetails.CustomerID == 0 ? "N/A" : Convert.ToString(_CustDetails.CustomerID);
                        //ViewBag._ConnStatus = _CustDetails.ConnectionStatus;
                        //HttpContext.Session["CustId"] = ViewBag._CustId;
                        //HttpContext.Session["ConnStatus"] = _CustDetails.ConnectionStatus;

                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        page.ActiveTab = "TabTariffDetails";
                        page.MobileNo = id;
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;

                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }

                    return View("Detail1", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        //
        // GET: /Customer/TabCallSettings/MobileNo

        public ActionResult TabCallSettings(string id)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();

                    try
                    {
                        //ViewBag._CustId = HttpContext.Session["CustId"].ToString();
                        //ViewBag._ConnStatus = HttpContext.Session["ConnStatus"].ToString();

                        page.ActiveTab = "TabCallSettings";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }

                    return View("Detail1", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        //
        // GET: /Customer/TabCallSettings/MobileNo

        public ActionResult TabSIMDetails(string id)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();

                    try
                    {
                        //ViewBag._CustId = HttpContext.Session["CustId"].ToString();
                        //ViewBag._ConnStatus = HttpContext.Session["ConnStatus"].ToString();

                        page.ActiveTab = "TabSIMDetails";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }

                    return View("Detail1", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        //
        // GET: /Customer/Detail2/MobileNo
        public ActionResult Detail2(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            ViewBag._SimType = SIM;
            switch (model.ActiveTab)
            {

                case "TabBundles":
                    return Redirect(string.Format("/Customer/TabBundles/{0}-{1}", id, SIM));
                case "TabPackages":
                    return Redirect(string.Format("/Customer/TabPackages/{0}-{1}", id, SIM));
                case "Tab2in1":
                    return Redirect(string.Format("/Customer/Tab2in1/{0}-{1}", id, SIM));
                case "TabCSB":
                    return Redirect(string.Format("/Customer/TabCSB/{0}-{1}", id, SIM));
                case "TabLOTG":
                    return Redirect(string.Format("/Customer/TabLOTG/{0}-{1}", id, SIM));
                case "TabCountrySaver":
                    return Redirect(string.Format("/Customer/TabCountrySaver/{0}-{1}", id, SIM));
                case "TabLLOTG":
                    return Redirect(string.Format("/Customer/TabLLOTG/{0}-{1}", id, SIM));
                case "TabTopupTransfer":
                    return Redirect(string.Format("/Customer/TabTopupTransfer/{0}-{1}", id, SIM));
                //Added by Karthik Jira:CRMT-196 
                case "InternetProfile":
                    return Redirect(string.Format("/Customer/InternetProfile/{0}-{1}", id, SIM));
                //Added by Karthik Jira:CRMT-231 
                case "TabHappyBundleDaillingNumber":
                    return Redirect(string.Format("/Customer/TabHappyBundleDaillingNumber/{0}-{1}", id, SIM));
                case "TabAddBalance":
                    return Redirect(string.Format("/Customer/TabAddBalance/{0}-{1}", id, SIM));
                case "TabTransferBalance":
                    return Redirect(string.Format("/Customer/TabTransferBalance/{0}-{1}", id, SIM));
                default:
                    return Redirect(string.Format("/Customer/TabBundles/{0}-{1}", id, SIM));
            }
        }

        //
        // GET: /Customer/TabBundles/MobileNo

        public ActionResult TabBundles(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();

                    try
                    {
                        //ViewBag._CustId = HttpContext.Session["CustId"].ToString();
                        //ViewBag._ConnStatus = HttpContext.Session["ConnStatus"].ToString();

                        page.ActiveTab = "TabBundles";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;

                        //27-Dec-2018 : Moorthy : Exchange Bundles
                        ViewBag._roleid = CRM_API.Helpers.Session.CurrentUser.user_role;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }

                    return View("Detail2", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        //
        // GET: /Customer/TabPackages/MobileNo

        public ActionResult TabPackages(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();

                    try
                    {
                        //ViewBag._CustId = HttpContext.Session["CustId"].ToString();
                        //ViewBag._ConnStatus = HttpContext.Session["ConnStatus"].ToString();

                        page.ActiveTab = "TabPackages";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }

                    return View("Detail2", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        //
        // GET: /Customer/Tab2in1/MobileNo

        public ActionResult Tab2in1(string id, CustomerDetailPage model)
        {
            string SIM = "";
            try
            {
                id = id.Split('-')[0];
                SIM = id.Split('-')[1];
               
            }
            catch (Exception)
            {
                SIM = "PAYG";
                //throw;
            }
            
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();

                    try
                    {

                        page.ActiveTab = "Tab2in1";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }

                    return View("Detail2", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        //
        // GET: /Customer/TabCSB/MobileNo

        public ActionResult TabCSB(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();

                    try
                    {
                        page.ActiveTab = "TabCSB";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }

                    return View("Detail2", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        //
        // GET: /Customer/TabLOTG/MobileNo

        public ActionResult TabLOTG(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();

                    try
                    {
                        page.ActiveTab = "TabLOTG";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }

                    return View("Detail2", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        //
        // GET: /Customer/TabCountrySaver/MobileNo

        public ActionResult TabCountrySaver(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();

                    try
                    {
                        page.ActiveTab = "TabCountrySaver";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }

                    return View("Detail2", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        //
        // GET: /Customer/TabBasicSubscriptionDetails/MobileNo

        public ActionResult TabBasicSubscriptionDetails(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();

                    try
                    {
                        page.ActiveTab = "TabBasicSubscriptionDetails";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    { page.ErrMsg = ex.Message; }
                    return View("Detail2", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        //
        // GET: /Customer/TabAdditionalCLIDetails/MobileNo

        public ActionResult TabAdditionalCLIDetails(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        page.ActiveTab = "TabAdditionalCLIDetails";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    { page.ErrMsg = ex.Message; }
                    return View("Detail2", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        //
        // GET: /Customer/TabDestinationCLIdetails/MobileNo

        public ActionResult TabDestinationCLIdetails(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        page.ActiveTab = "TabDestinationCLIdetails";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    { page.ErrMsg = ex.Message; }
                    return View("Detail2", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        //
        // GET: /Customer/TabLLOTG/MobileNo

        public ActionResult TabLLOTG(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        page.ActiveTab = "TabLLOTG";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;

                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;
                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    { page.ErrMsg = ex.Message; }
                    return View("Detail2", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        //
        // GET: /Customer/TabTopupTransfer/MobileNo

        public ActionResult TabTopupTransfer(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        page.ActiveTab = "TabTopupTransfer";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    { page.ErrMsg = ex.Message; }
                    return View("Detail2", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        //Added by elango for Jira 135 & 136
        public ActionResult TabRecommendBonus(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        page.ActiveTab = "TabRecommendBonus";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    { page.ErrMsg = ex.Message; }
                    return View("Detail2", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        public ActionResult TabOverview(string id)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();

                    try
                    {
                        page.ActiveTab = "TabOverview";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._ReportMonth = DateTime.Now.ToString("MMMM");
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }

                    return View("Detail2", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        public ActionResult TabReferralHistory(string id)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();

                    try
                    {
                        page.ActiveTab = "TabReferralHistory";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }

                    return View("Detail2", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        public ActionResult TabTransactionHistory(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();

                    try
                    {

                        page.ActiveTab = "TabTransactionHistory";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }

                    return View("Detail2", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        //Added by Karthik Jira:CRMT-196
        //
        // GET: /Customer/InternetProfile/MobileNo
        public ActionResult InternetProfile(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();

                    try
                    {

                        page.ActiveTab = "InternetProfile";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }

                    return View("Detail2", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }


        //Added by Karthik Jira:CRMT-231
        //
        // GET: /Customer/TabHappyBundleDaillingNumber/MobileNo
        public ActionResult TabHappyBundleDaillingNumber(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();

                    try
                    {

                        page.ActiveTab = "TabHappyBundleDaillingNumber";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }

                    return View("Detail2", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        //
        // GET: /Customer/Detail3/MobileNo
        public ActionResult Detail3(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            ViewBag._SimType = SIM;
            switch (model.ActiveTab)
            {
                case "TabCallHistory":
                default:
                    return Redirect(string.Format("/Customer/TabCallHistory/{0}-{1}", id, SIM));
            }
        }

        //
        // GET: /Customer/TabCallHistory/MobileNo

        public ActionResult TabCallHistory(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();

                    try
                    {

                        page.ActiveTab = "TabCallHistory";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }

                    return View("Detail3", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        //Added by karthik Jira:CRMT-198
        //
        // GET: /Customer/Detail8/MobileNo
        public ActionResult Detail3_v1(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            ViewBag._SimType = SIM;
            switch (model.ActiveTab)
            {
                case "TabCallHistory_v1":
                default:
                    return Redirect(string.Format("/Customer/TabCallHistory_v1/{0}-{1}", id, SIM));
            }
        }

        //Added by karthik Jira:CRMT-198
        //
        // GET: /Customer/TabCallHistory_New/MobileNo

        public ActionResult TabCallHistory_v1(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            ViewBag._SimType = SIM;
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();

                    try
                    {

                        page.ActiveTab = "TabCallHistory_v1";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }

                    return View("Detail3_v1", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        //
        // GET: /Customer/Detail4/MobileNo
        public ActionResult Detail4(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            ViewBag._SimType = SIM;
            switch (model.ActiveTab)
            {
                case "TabTopupHistory":
                    return Redirect(string.Format("/Customer/TabTopupHistory/{0}-{1}", id, SIM));
                case "TabAutoTopup":
                    return Redirect(string.Format("/Customer/TabAutoTopup/{0}-{1}", id, SIM));
                case "TabPaymentHistoryPAYG":
                    return Redirect(string.Format("/Customer/TabPaymentHistoryPAYG/{0}-{1}", id, SIM));
                case "TabPaymentProfile":
                    return Redirect(string.Format("/Customer/TabPaymentProfile/{0}-{1}", id, SIM));
                case "TabTotalUsageHistory"://CRMT-232 total usage history
                    return Redirect(string.Format("/Customer/TabTotalUsageHistory/{0}-{1}", id, SIM));
                case "TabChargeback"://CRMIMP-21 Chargeback Tab
                    return Redirect(string.Format("/Customer/TabChargeback/{0}-{1}", id, SIM));
                default:
                    return Redirect(string.Format("/Customer/TabPaymentHistory/{0}-{1}", id, SIM));
            }
        }

        //
        // GET: /Customer/TabTopupHistory/MobileNo

        public ActionResult TabTopupHistory(string id, CustomerDetailPage model)
        {
            
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            ViewBag._SimType = SIM;
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();

                    try
                    {

                        page.ActiveTab = "TabTopupHistory";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }

                    return View("Detail4", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        //
        // GET: /Customer/TabAutoTopup/MobileNo

        public ActionResult TabAutoTopup(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            ViewBag._SimType = SIM;
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();

                    try
                    {

                        page.ActiveTab = "TabAutoTopup";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }

                    return View("Detail4", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        //
        // GET: /Customer/TabPaymentHistoryPAYG/MobileNo

        public ActionResult TabPaymentHistoryPAYG(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            ViewBag._SimType = SIM;
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();

                    try
                    {

                        page.ActiveTab = "TabPaymentHistoryPAYG";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;

                        ViewBag._userid = CRM_API.Helpers.Session.CurrentUser.userid;
                        ViewBag._roleid = CRM_API.Helpers.Session.CurrentUser.user_role;
                        ViewBag._user_role = CRM_API.Helpers.Session.CurrentUser.user_role == User_Role.Admin || CRM_API.Helpers.Session.CurrentUser.user_role == User_Role.TeamLeader ? "1" : "0";
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }

                    return View("Detail4", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        //
        // GET: /Customer/TabPaymentProfile/MobileNo

        public ActionResult TabPaymentProfile(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            ViewBag._SimType = SIM;
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();

                    try
                    {

                        page.ActiveTab = "TabPaymentProfile";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }

                    return View("Detail4", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        //CRMT-232 Total usage History
        //
        // GET: /Customer/TabTotalUsageHistory/MobileNo
        public ActionResult TabTotalUsageHistory(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            ViewBag._SimType = SIM;
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();

                    try
                    {

                        page.ActiveTab = "TabTotalUsageHistory";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }

                    return View("Detail4", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        //CRMIMP-21 Chargeback Tab
        //
        // GET: /Customer/TabChargeback/MobileNo
        public ActionResult TabChargeback(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            ViewBag._SimType = SIM;
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    ViewBag._userid = CRM_API.Helpers.Session.CurrentUser.userid;
                    ViewBag._roleid = CRM_API.Helpers.Session.CurrentUser.user_role;
                    ViewBag._role_id = CRM_API.Helpers.Session.CurrentUser.roleid;

                    ViewBag.LoginName = string.IsNullOrEmpty(CRM_API.Helpers.Session.CurrentUser.first_name) ?
                                        CRM_API.Helpers.Session.CurrentUser.user_login :
                                        string.Format("{0} {1}", CRM_API.Helpers.Session.CurrentUser.first_name, CRM_API.Helpers.Session.CurrentUser.last_name);

                    var page = new CustomerDetailPage();
                    try
                    {

                        page.ActiveTab = "TabChargeback";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._lastname = page.Customer.last_name;
                        ViewBag._email = page.Customer.Email;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }
                    return View("Detail4", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        //
        // GET: /Customer/Detail5/MobileNo
        public ActionResult Detail5(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            ViewBag._SimType = SIM;
            switch (model.ActiveTab)
            {
                case "TabSIMSwap":
                    return Redirect(string.Format("/Customer/TabSIMSwap/{0}-{1}", id, SIM));
                default:
                    return Redirect(string.Format("/Customer/TabSIMSwap/{0}-{1}", id, SIM));
            }
        }

        //
        // GET: /Customer/TabSIMSwap/MobileNo

        public ActionResult TabSIMSwap(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            ViewBag._SimType = SIM;
            
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();

                    try
                    {

                        page.ActiveTab = "TabSIMSwap";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }

                    return View("Detail5", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        //
        // GET: /Customer/Detail6/MobileNo
        public ActionResult Detail6(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            ViewBag._SimType = SIM;
            switch (model.ActiveTab)
            {
                case "TabIssuesTracking":
                    return Redirect(string.Format("/Customer/TabIssuesTracking/{0}-{1}", id, SIM));
                default:
                    return Redirect(string.Format("/Customer/TabIssuesTracking/{0}-{1}", id, SIM));
            }
        }

        //
        // GET: /Customer/TabIssuesTracking/MobileNo

        public ActionResult TabIssuesTracking(string id, CustomerDetailPage model)
        {
            //18-Jul-2017 : Moorthy : Added if the simType is empty
            string SIM = "";
            if (id.Split('-').Length > 1 && id.Split('-')[1] != "undefined")
            {
                SIM = id.Split('-')[1];
                id = id.Split('-')[0];
            }
            else
            {
                if (Request.Url.LocalPath.ToLower().Contains("/paym/"))
                    SIM = "PAYM";
                else
                    SIM = "PAYM";
                id = id.Split('-')[0];
            }
            ViewBag._SimType = SIM;
            ViewBag._AgentEmailId = CRM_API.Helpers.Session.CurrentUser.email_id;
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();

                    try
                    {

                        page.ActiveTab = "TabIssuesTracking";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }

                    return View("Detail9", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        public ActionResult TabEmailTracking(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            var page = new CustomerDetailPage();
            try
            {

                page.ActiveTab = "TabEmailTracking";
                page.MobileNo = id;
                var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                page.Detail2in1 = new Customer2in1(id);

                //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                HttpContext.Session["CustId"] = ViewBag._CustId;
                HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                ViewBag._mobileno = id;
                ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                ViewBag._sitecode = _product.sitecode;
                ViewBag._iccid = page.Customer.iccid;
                ViewBag._balance = page.Customer.balance;
                ViewBag._tariffclass = page.Customer.tariffclass;
                ViewBag._firstname = page.Customer.first_name;
                ViewBag._firstname = page.Customer.last_name;
                ViewBag._subscriberid = page.Customer.SubscriberID;
            }
            catch (Exception ex)
            {
                page.ErrMsg = ex.Message;
            }
            return View("TabEmailTracking",page);
        }


        [HttpPost]
        public ActionResult SendMail(Email objEmail)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(ConfigurationSettings.AppSettings["SMTPServer"].ToString());
                if (!String.IsNullOrEmpty(objEmail.toAddress))
                {
                    foreach (var address in objEmail.toAddress.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        mail.To.Add(address);
                    }
                }
                else
                {
                    mail.To.Add(new MailAddress("k.rajesh@vectone.com"));
                }

                 mail.From = new MailAddress(ConfigurationSettings.AppSettings["EMAILDONOTREPLY"].ToString());

                //mail.Bcc.Add(new MailAddress("s.anandhakumar@vectone.com"));
                //mail.Bcc.Add(new MailAddress("r.murali@vectone.com"));
                //mail.Bcc.Add(new MailAddress("p.benney@vectone.com"));
                //mail.Bcc.Add(new MailAddress("p.loganathan@vectone.com"));
                //mail.Bcc.Add(new MailAddress("d.tharmasirirajah@mundio.com"));
                mail.Subject = objEmail.title;

                mail.IsBodyHtml = true;
                string htmlBody;

                htmlBody = objEmail.msg;

                mail.Body = htmlBody;

                //SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential(ConfigurationSettings.AppSettings["SMTPLogin"].ToString(), ConfigurationSettings.AppSettings["SMTPPassword"].ToString());
                // SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json("Failure", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult AssignAgent(Agent Model)
        {
            //email_queue_assign_to_user
            return Json("Value assigned to Agent", JsonRequestBehavior.AllowGet);
        }
        
        // GET: /Customer/Detail7/MobileNo
        public ActionResult Detail7(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            ViewBag._SimType = SIM;
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                    return RedirectToAction("Index", "Product");
                else
                {
                    switch (model.ActiveTab)
                    {
                        case "OrderManagement":
                            return Redirect(string.Format("/Customer/OrderManagement/{0}-{1}", id, SIM));
                        default:
                            return RedirectToAction("Index", "Product");
                    }
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        public ActionResult OrderManagement(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            ViewBag._SimType = SIM;
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        page.ActiveTab = "OrderManagement";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;

                        ViewBag._user_role = CRM_API.Helpers.Session.CurrentUser.user_role;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }
                    return View("Detail7", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        public ActionResult TabSMSCampaignTracking(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        //ViewBag._CustId = HttpContext.Session["CustId"].ToString();
                        //ViewBag._ConnStatus = HttpContext.Session["ConnStatus"].ToString();
                        page.ActiveTab = "TabSMSCampaignTracking";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }
                    return View("Detail10", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        public ActionResult TabSMSCampaignSettings(string id)
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                    return RedirectToAction("Index", "Product");
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        //ViewBag._CustId = HttpContext.Session["CustId"].ToString();
                        //ViewBag._ConnStatus = HttpContext.Session["ConnStatus"].ToString();
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        page.ActiveTab = "TabSMSCampaignSettings";
                        page.MobileNo = id;
                        page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    { page.ErrMsg = ex.Message; }
                    return View("Detail1", page);
                }
            }
            else
            { return RedirectToAction("Index", "Product"); }
        }

        public ActionResult Detail8(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            ViewBag._SimType = SIM;
            switch (model.ActiveTab)
            {
                case "TabAccountstatus":
                    return Redirect(string.Format("/Customer/TabAccountstatus/{0}-{1}", id, SIM));
                default:
                    return Redirect(string.Format("/Customer/TabAccountstatus/{0}-{1}", id, SIM));
            }
        }

        //
        // GET: /Customer/TabIssuesTracking/MobileNo

        public ActionResult TabAccountstatus(string id, CustomerDetailPage model)
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();

                    try
                    {

                        page.ActiveTab = "TabAccountstatus";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }

                    return View("Detail8", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        // GET: /Customer/TabRoamingDetails/MobileNo
        public ActionResult TabRoamingDetails(string id)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                    return RedirectToAction("Index", "Product");
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        //ViewBag._CustId = HttpContext.Session["CustId"].ToString();
                        //ViewBag._ConnStatus = HttpContext.Session["ConnStatus"].ToString();
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        page.ActiveTab = "TabRoamingDetails";
                        page.MobileNo = id;
                        page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    { page.ErrMsg = ex.Message; }
                    return View("Detail1", page);
                }
            }
            else
            { return RedirectToAction("Index", "Product"); }
        }


        // GET: /Customer/TabBreakageUsage/MobileNo
        public ActionResult TabBreakageUsage(string id)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                    return RedirectToAction("Index", "Product");
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        //ViewBag._CustId = HttpContext.Session["CustId"].ToString();
                        //ViewBag._ConnStatus = HttpContext.Session["ConnStatus"].ToString();
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        page.ActiveTab = "TabBreakageUsage";
                        page.MobileNo = id;
                        page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    { page.ErrMsg = ex.Message; }
                    return View("Detail1", page);
                }
            }
            else
            { return RedirectToAction("Index", "Product"); }
        }

        // GET: /Customer/TabCOverview/MobileNo
        public ActionResult TabCOverview(string id)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            CRM_API.Helpers.Session.GetProductByMobile = null;

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                    return RedirectToAction("Index", "Product");
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        //ViewBag._CustId = HttpContext.Session["CustId"].ToString();
                        //ViewBag._ConnStatus = HttpContext.Session["ConnStatus"].ToString();
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        page.ActiveTab = "TabCOverview";
                        page.MobileNo = id;
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    { page.ErrMsg = ex.Message; }
                    return View("_TabCOverview", page);
                }
            }
            else
            { return RedirectToAction("Index", "Product"); }
        }

        //TabAddBalance
        public ActionResult TabAddBalance(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();

                    try
                    {

                        page.ActiveTab = "TabAddBalance";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }

                    return View("Detail2", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        //TabTransferBalance
        public ActionResult TabTransferBalance(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();

                    try
                    {

                        page.ActiveTab = "TabTransferBalance";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }

                    return View("Detail2", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        // GET: /Customer/TabVectoneXtraApp/MobileNo
        public ActionResult TabVectoneXtraApp(string id)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                    return RedirectToAction("Index", "Product");
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        //ViewBag._CustId = HttpContext.Session["CustId"].ToString();
                        //ViewBag._ConnStatus = HttpContext.Session["ConnStatus"].ToString();
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        page.ActiveTab = "TabVectoneXtraApp";
                        page.MobileNo = id;
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    { page.ErrMsg = ex.Message; }
                    return View("_TabVectoneXtraApp", page);
                }
            }
            else
            { return RedirectToAction("Index", "Product"); }
        }

        // GET: /Customer/TabSMSCampaignTracking
        public ActionResult Detail10(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            ViewBag._SimType = SIM;
            switch (model.ActiveTab)
            {
                case "TabSMSCampaignTracking":
                    return Redirect(string.Format("/Customer/TabSMSCampaignTracking/{0}-{1}", id, SIM));
                default:
                    return Redirect(string.Format("/Customer/TabSMSCampaignTracking/{0}-{1}", id, SIM));
            }
        }

        //Rajesh: Porting
        public ActionResult TabPorting(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                    return RedirectToAction("Index", "Product");
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        //ViewBag._CustId = HttpContext.Session["CustId"].ToString();
                        //ViewBag._ConnStatus = HttpContext.Session["ConnStatus"].ToString();
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        page.ActiveTab = "TabDetailPorting";
                        page.MobileNo = id;
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    { page.ErrMsg = ex.Message; }
                    return View("_TabPorting", page);
                }
            }
            else
            { return RedirectToAction("Index", "Product"); }
        }

        //TabFreeCredit
        public ActionResult TabFreeCredit(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();

                    try
                    {

                        page.ActiveTab = "TabFreeCredit";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile :  CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }

                    return View("Detail2", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        //02-Jan-2019 : Moorthy : Added to avoid fetching Customer Detail & Personal Info everytime from the DB
        [HttpPost]
        public ActionResult CustomerDetailInfo(CRM_API.Models.CustomerSearchPage In)
        {
            string SubscriberID;
            CRM_API.Models.CustomerSearchResult resHistoryValues = CRM_API.DB.Customer.SProc.CustomerDetailInfo(In.Product_Code, In.SearchText, In.SIM_Type);
            var TempRes = resHistoryValues;

            CRM_API.Models.CustomerSearchResult resAccountInfo = CRM_API.DB.Customer.SProc.CustomerPersonalInfo(In.Product_Code, In.SearchText, In.Sitecode);
            if (resAccountInfo.SubscriberID == null || resAccountInfo.SubscriberID == "0")
            {
                SubscriberID = CRM_API.DB.Customer.SProc.InsertCustomer(TempRes, In.Sitecode, In.SearchText).ToString();
                resHistoryValues = CRM_API.DB.Customer.SProc.CustomerDetailInfo(In.Product_Code, In.SearchText, In.SIM_Type);
                resAccountInfo = CRM_API.DB.Customer.SProc.CustomerPersonalInfo(In.Product_Code, In.SearchText, In.Sitecode);
            }
            #region remap object

            resHistoryValues.first_name = resAccountInfo.FirstName;
            resHistoryValues.last_name = resAccountInfo.LastName;
            resHistoryValues.Email = resAccountInfo.Email;
            resHistoryValues.Houseno = resAccountInfo.Houseno;
            resHistoryValues.address1 = resAccountInfo.address1;
            resHistoryValues.address2 = resAccountInfo.address2;
            resHistoryValues.city = resAccountInfo.city;
            resHistoryValues.postcode = resAccountInfo.postcode;
            resHistoryValues.BirthDate = resAccountInfo.BirthDate;
            resHistoryValues.MobileNo = resHistoryValues.mobileno;
            resHistoryValues.ICCID = resHistoryValues.iccid;
            resHistoryValues.SimType = resAccountInfo.SimType;
            resHistoryValues.product = resAccountInfo.product;
            resHistoryValues.balance = resAccountInfo.balance;
            resHistoryValues.ConnectionStatus = resHistoryValues.ConnectionStatus;
            // Added
            resHistoryValues.SubscriberID = resAccountInfo.SubscriberID;
            //13-Jan-2017 : Moorthy Added for ID File Download Func
            resHistoryValues.proof_file_name = resAccountInfo.proof_file_name;
            //15-Jun-2017 : Moorthy Added for Security Answer
            resHistoryValues.securityanswer = resAccountInfo.securityanswer;
            //27-Nov-2018 : Moorthy Added for Alternate Number
            resHistoryValues.Telephone = resAccountInfo.Telephone;

            //Added : 22-Dec-2018 : CRM Enhancement
            resHistoryValues.bonus_balance = resAccountInfo.bonus_balance;
            resHistoryValues.passwd = resAccountInfo.passwd;
            #endregion

            return Json(resHistoryValues, JsonRequestBehavior.AllowGet);
            //if (resHistoryValues == null)
            //    return new CustomerSearchResult();
            //else
            //    return resHistoryValues;
        }

        //25-Feb-2019 : Moorthy : CRMIMP-18 - Daily usage history
        //
        // GET: /Customer/TabDailyUsageHistory/MobileNo
        public ActionResult TabDailyUsageHistory(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            ViewBag._SimType = SIM;
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();

                    try
                    {

                        page.ActiveTab = "TabDailyUsageHistory";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        //page.Customer = CRM_API.DB.Customer.SProc.GetByMobileNo(id, _product.mundio_product);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        //07-Aug-2015 : Moorthy Modified : To display N/A if the CustomerID is zero
                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }

                    return View("Detail4", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        //TabFreeDataBundle
        public ActionResult TabFreeDataBundle(string id, CustomerDetailPage model)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();

                    try
                    {

                        page.ActiveTab = "TabFreeDataBundle";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }

                    return View("Detail2", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        // GET: /PAYM/Customer/MobileNo
        public ActionResult TabComplaints(string id, CustomerDetailPage model)
        {
            string SIM = "";
            if (id.Split('-').Length > 1 && id.Split('-')[1] != "undefined")
            {
                SIM = id.Split('-')[1];
                id = id.Split('-')[0];
            }
            else
            {
                if (Request.Url.LocalPath.ToLower().Contains("/paym/"))
                    SIM = "PAYM";
                else
                    SIM = "PAYG";
                id = id.Split('-')[0];
            }
            ViewBag._SimType = SIM;
            ViewBag._AgentEmailId = CRM_API.Helpers.Session.CurrentUser.email_id;
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        page.ActiveTab = "TabComplaints";
                        page.MobileNo = id;
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._lastname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                        ViewBag._userid = CRM_API.Helpers.Session.CurrentUser.userid;
                        ViewBag._user_role = CRM_API.Helpers.Session.CurrentUser.user_role;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }
                    return View("Detail20", page);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }


        // GET: /PAYM/TabAutoRenewal/MobileNo
        public ActionResult TabAutoRenewal(string id)
        {
            string SIM = id.Split('-')[1];
            id = id.Split('-')[0];

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                    return RedirectToAction("Index", "Product");
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(id);
                        page.ActiveTab = "TabAutoRenewal";
                        page.MobileNo = id;
                        page.Customer = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, id, SIM);
                        page.Detail2in1 = new Customer2in1(id);

                        ViewBag._CustId = page.Customer.CustomerID == 0 ? "N/A" : Convert.ToString(page.Customer.CustomerID);
                        ViewBag._ConnStatus = page.Customer.ConnectionStatus;
                        HttpContext.Session["CustId"] = ViewBag._CustId;
                        HttpContext.Session["ConnStatus"] = page.Customer.ConnectionStatus;

                        ViewBag._mobileno = id;
                        ViewBag._sitecode = _product.sitecode;
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._iccid = page.Customer.iccid;
                        ViewBag._balance = page.Customer.balance;
                        ViewBag._tariffclass = page.Customer.tariffclass;
                        ViewBag._firstname = page.Customer.first_name;
                        ViewBag._firstname = page.Customer.last_name;
                        ViewBag._subscriberid = page.Customer.SubscriberID;
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }
                    return View("Detail1", page);
                }
            }
            else
            { return RedirectToAction("Index", "Product"); }
        }
    }
}
