﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRM_API.Models.Permission;
using CRM3.WebApp.UserRoleEntity;
using System.Text;
using CRM3.WebApp.Attributes;
using System.Data.Entity;
using System.Data;
using Newtonsoft.Json;
using NLog;
using CRM_API.DB.Auth;
using CRM_API.Models;

namespace CRM3.WebApp.Controllers
{
    //Enquiries
    [CRM3Auth]
    public class EnquiriesController : Controller
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public UserRolePermissionsEntities _userRolePerObject = new UserRolePermissionsEntities();
        // GET: /Users/

        public ActionResult Index()
        {
            return View();
        }
        // Get CountryList




        //// Added Insert EnquieryDetails
        //[HttpPost]
        //public JsonResult NewEnquiryInsert(CRM_API.Models.Permission.User _user)
        //{
        //    try
        //    {
        //        string permission = _user.PermissionList;
        //        permission = permission.TrimEnd(',');
        //        CRM_API.Models.CustomerSearchResult NewuserInsert = CRM_API.DB.Customer.SProc.NewUserInsert
        //            (
        //            _user.Id,
        //            _user.Username,
        //            _user.Password,
        //            _user.Status,
        //            _user.FirstName,
        //            _user.LastName,
        //            _user.Email,
        //            _user.Username,
        //            permission,
        //            _user.role_id);

        //        if (_user.errcode == 0)
        //        {
        //            _user.errcode = 0;
        //            _user.errmsg = "New recoreds insert sucessfully";
        //            _user.errsubject = "General Information";
        //            return Json(_user);
        //        }
        //        else
        //        {
        //            return Json(_user);
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        _user.errcode = -1;
        //        _user.errmsg = "New User Creation Having Some Problem...";
        //        _user.errsubject = "General Information";
        //        return Json(_user);
        //    }
        //}

        [HttpPost]
        public JsonResult NewEnquiryInsert(CRM_API.Models.EnquiryInput _Addenquiry)
        {
            try
            {
                //string permission = _user.PermissionList;
                //permission = permission.TrimEnd(',');
                CRM_API.Models.EnquiryInput ResultNewEnquiry = CRM_API.DB.Enquiry.SProc.NewEnquiryInsert
                    (
                    _Addenquiry.Enquirer_Name,
                    _Addenquiry.Mobileno,
                    _Addenquiry.Email,
                    _Addenquiry.Existing_Network,
                    _Addenquiry.Country,
                    _Addenquiry.Enquiry_Type,
                    _Addenquiry.Query,
                    _Addenquiry.Comments,
                    _Addenquiry.mode,
                    _Addenquiry.Enquiry_ID,
                    1,//status = 1
                    _Addenquiry.fk_agent_id,
                    _Addenquiry.fk_close_agent);

                if (_Addenquiry.errcode == 0)
                {
                    ResultNewEnquiry.errcode = 0; 
                    //ResultNewEnquiry.errsubject = "General Information";
                    return Json(ResultNewEnquiry);
                }
                else
                {
                    return Json(ResultNewEnquiry);
                }
            }
            catch (Exception)
            {
                _Addenquiry.errcode = -1;
                _Addenquiry.errmsg = "New User Creation Having Some Problem...";
                //ResultNewEnquiry.errsubject = "General Information";
                return Json(_Addenquiry);
            }
        }

        [HttpPost]
        public JsonResult InsertCommentsHistory(CRM_API.Models.EnquiryInput _Addenquiry)
        {
            try
            { 

                 Log.Debug("Get Get InsertCommentsHistory");
                IEnumerable<CRM_API.Models.CommonModels> EnquiryOutputList = new List<CRM_API.Models.CommonModels>();
                EnquiryOutputList = CRM_API.DB.Enquiry.SProc.InsertHistory(_Addenquiry);
                return Json(EnquiryOutputList);
            }
            catch (Exception)
            {
                _Addenquiry.errcode = -1;
                _Addenquiry.errmsg = "New User Creation Having Some Problem...";
                //ResultNewEnquiry.errsubject = "General Information";
                return Json(_Addenquiry);
            }
        }

        [HttpGet]
        public ActionResult UserList()
        {
            CRM_API.Models.Permission.User data = new CRM_API.Models.Permission.User();
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    return View(data);
                    //return View("TabUserList");
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }
        //Get GetUserPermissionlist 
        public JsonResult GetUserPermissionlist(CRM_API.Models.Permission.User _user)
        {
            List<CRM_API.Models.Permission.User> getpermission = new List<CRM_API.Models.Permission.User>();
            List<CRM_API.Models.Permission.PermissionRecord> getpermission1 = new List<CRM_API.Models.Permission.PermissionRecord>();
            var data = (from s in _userRolePerObject.Users where s.Id == _user.Id select s).ToList();
            var permmissionRecords = data[0].PermissionRecords;
            var permmissionCheck = permmissionRecords.Select(x => x.Id);           
            var jsonValue = JsonConvert.SerializeObject(permmissionCheck);
            return Json(jsonValue);
        }
        public JsonResult BasicinfoEdit(EnquiryInput objEnquiryInput)
        {
           //crm_get_enqiry_info_ID

            try
            {

                Log.Debug("Get Get Enquiry List by iD");
                IEnumerable<CRM_API.Models.EnquiryOutput> EnquiryOutputList = new List<CRM_API.Models.EnquiryOutput>();

                EnquiryOutputList = CRM_API.DB.Enquiry.SProc.EnquiryDetailsbyID(objEnquiryInput);
                return Json(EnquiryOutputList);
            }
            catch (Exception e)
            {
                Log.Debug("Exception");
                Log.Error(e);
            }
            return Json("No data");
        }
        //get permission list
        public JsonResult GetPermissionlist()
        {
            List<CRM_API.Models.Permission.PermissionRecord> getpermission = new List<CRM_API.Models.Permission.PermissionRecord>();
            var data = (from s in _userRolePerObject.PermissionRecords select new { s.Id, s.Name, s.SystemName, s.Category }).ToList();

            foreach (var perinfo in data)
            {
                getpermission.Add(new CRM_API.Models.Permission.PermissionRecord
                {
                    Id = perinfo.Id,
                    Name = perinfo.Name,
                    SystemName = perinfo.SystemName,
                    Category = perinfo.Category,
                    errcode = 0,
                    errmsg = "Get User Permission",
                    errsubject = "General Info"

                });
            }
            return Json(getpermission);


        }
        //Display User information list 
        [HttpPost]
        public JsonResult GetEnquiryList(EnquiryInput objEnquiryInput)
        {
            try
            {

                Log.Debug("Get Get Enquiry List");
                IEnumerable<CRM_API.Models.EnquiryOutput> EnquiryOutputList = new List<CRM_API.Models.EnquiryOutput>();

                EnquiryOutputList = CRM_API.DB.Enquiry.SProc.EnquiryDetails(objEnquiryInput);
                return Json(EnquiryOutputList);
            }
            catch (Exception e)
            {
                Log.Debug("Exception");
                Log.Error(e);
            }
            return Json("No data");
        }
        [HttpPost]
        public JsonResult GetHistoryList(EnquiryInput objEnquiryInput)
        {
            try
            {

                Log.Debug("Get Get Enquiry List");
                IEnumerable<CRM_API.Models.EnquiryComments> EnquiryOutputList = new List<CRM_API.Models.EnquiryComments>();

                EnquiryOutputList = CRM_API.DB.Enquiry.SProc.EnquiryCommentsInfo(objEnquiryInput);
                return Json(EnquiryOutputList);
            }
            catch (Exception e)
            {
                Log.Debug("Exception");
                Log.Error(e);
            }
            return Json("No data");
        } 



        [HttpPost]
        public JsonResult BindCountry()
        {
            try
            {
                Log.Debug("BindCountry list ");
                IEnumerable<CRM_API.Models.EnquiryCountry> userRole = new List<CRM_API.Models.EnquiryCountry>();
                userRole = CRM_API.DB.Enquiry.SProc.GetCountryInfo();
                return Json(userRole);
            }
            catch (Exception e)
            {
                Log.Debug("Exception");
                Log.Error(e);
            }
            return Json("No data");
        }
        [HttpPost]
        public JsonResult BindEnquiryType()
        {
            try
            {
                Log.Debug("BindEnquiryType list ");
                IEnumerable<CRM_API.Models.EnquiryType> userRole = new List<CRM_API.Models.EnquiryType>();
                userRole = CRM_API.DB.Enquiry.SProc.GetEnquiryType();
                return Json(userRole);
            }
            catch (Exception e)
            {
                Log.Debug("Exception");
                Log.Error(e);
            }
            return Json("No data");
        }
        [HttpPost]
        public JsonResult BindStatus(string userID)
        {
            try
            {
                Log.Debug("BindStatus list ");
                IEnumerable<CRM_API.Models.BindStatus> userRole = new List<CRM_API.Models.BindStatus>();
                userRole = CRM_API.DB.Enquiry.SProc.GetBindStatus(userID);
                return Json(userRole);
            }
            catch (Exception e)
            {
                Log.Debug("Exception");
                Log.Error(e);
            }
            return Json("No data");
        }
        [HttpGet]
        public FileContentResult GoDownloadEnquiry(string id, string historyName)
        {
            var result = (string)TempData[id];
            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=" + historyName.Replace(" ", "") + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(result.ToString());
            return new FileContentResult(temp, "application/vnd.ms-excel");
        }
        [HttpPost]
        public ActionResult UsersDownload(EnquiryInput objEnquiryInput)
        {
            var result = Getenquriyinfo(objEnquiryInput);
            var HistoryName = "";
            StringBuilder str = new StringBuilder();
            str.Append("<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv=\"content-type\" content=\"text/plain; charset=UTF-8\"/></head><body>");
            str.Append("<head><style>.xlLongDate{mso-number-format:'dd-mm-yyyy hh:mm:ss AM/PM';}.xlText{mso-number-format:'@';}</style></head>");
            HistoryName = "User Enquiry";
            str.Append("<h3>User  Enquiry </h3>");
            UserEnquiriesListDowload(result, ref str);
            str.Append("</html>");
            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=" + HistoryName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(str.ToString());
            string _random = Guid.NewGuid().ToString();
            TempData[_random] = str.ToString();
            var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = 0, errmsg = HistoryName, Text = _random, };
            return Json(resy, JsonRequestBehavior.AllowGet);
        }

        //Html template for download
        private static void UserEnquiriesListDowload(dynamic result, ref StringBuilder str)
        {
            str.Append("<table>");
            str.Append("<thead>");
            str.Append("<tr>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'> Enquiry ID</th>");
            str.Append("<th  style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Name</th>");
            str.Append("<th  style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Email Address</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Mobile Number</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Agent Name</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Enquiry ON</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Enquiry Type</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Comments</th>"); 
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>History Comments</th>"); 
            str.Append("</tr>");
            str.Append("</thead>");
            str.Append("<tbody>");
            foreach (CRM_API.Models.EnquiryOutput val in (result as IEnumerable<CRM_API.Models.EnquiryOutput>))
            {
                str.Append("<tr>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Enquiry_ID.ToString()) ? "" : (val.Enquiry_ID.ToString())) + "</td>");
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Enquirer_Name as string) ? "" : (val.Enquirer_Name.ToString())) + "</td>");
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Email as string) ? "" : (val.Email.ToString())) + "</td>");
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Mobileno as string) ? "" : (val.Mobileno.ToString())) + "</td>");
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.agent as string) ? "" : (val.agent.ToString())) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Enquiry_Date_string as string) ? "" : (val.Enquiry_Date_string.ToString())) + "</td>");
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Enquiry_Name.ToString()) ? "" : (val.Enquiry_Name.ToString())) + "</td>");
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Comments.ToString()) ? "" : (val.Comments.ToString())) + "</td>");

                if (!string.IsNullOrEmpty(val.h_comments))
                {
                    string[] list = val.h_comments.Split(new string[] { "||" }, StringSplitOptions.None);
                    int i = 0;
                    string resultvalue = string.Empty;
                    foreach (string value in list)
                    {
                        if (i == 0)
                        {
                            resultvalue = "1." + value+"<br>";
                        }
                        else
                        {
                            int count = i + 1;
                            resultvalue = resultvalue + count + "." + value + "<br>";
                        }
                        i =  i + 1;
                    }
                    str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" +
                             (string.IsNullOrEmpty(resultvalue) ? "" : (resultvalue)) + "</td>");
                }
                else
                {
                    str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" +
                            "" + "</td>");
                }

                str.Append("</tr>");
            }
            str.Append("</tbody>");
            str.Append("</table>");
            str.Append("</body>");
        }
        private dynamic Getenquriyinfo(EnquiryInput objEnquiryInput)
        {
            try
            {
                //DB
                Log.Debug("Get Get Enquiry List by iD");
                IEnumerable<CRM_API.Models.EnquiryOutput> result = new List<CRM_API.Models.EnquiryOutput>();

                result = CRM_API.DB.Enquiry.SProc.EnquiryDetails(objEnquiryInput);
                return result ?? new List<EnquiryOutput>();
            }
            catch (Exception ex)
            {
                Log.Error("BL crm_get_enqiry_info: {0}", ex.Message);
                throw new Exception("BL crm_get_enqiry_info: " + ex.Message);
            }
        }

    } 
    
}
