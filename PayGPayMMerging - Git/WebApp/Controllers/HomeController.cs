﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRM3.WebApp.Attributes;

namespace CRM3.WebApp.Controllers
{
    [CRM3Auth]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (!string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                }
                else
                {
                    return RedirectToAction("Index", "Product");
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
            return View();
        }

    }
}
