﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRM3.WebApp.Attributes;
using CRM_API.Models;
using System.Data;
using System.Data.OleDb;
using System.Configuration;
using System.Data.SqlClient;
using Dapper;

namespace CRM3.WebApp.Controllers
{
    [CRM3Auth]
    public class FinanceController : Controller
    {
        //
        // GET: /Finance/

        public ActionResult Index()
        {
            return View();
        }


        //View TransactionTabDDOperation
        public ActionResult Tabfsearchcustomer()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    HttpContext.Session["GlobalProduct"] = "VMUK";
                }
                else
                {
                    var page = new financeviewtrans();
                    try
                    {
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = "MCM";
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }
                }
            }
            else
            {
                HttpContext.Session["GlobalProduct"] = "VMUK";
                ViewBag._sitecode = "MCM";
                ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
            }
            return View();
        }


        public ActionResult Tabfsearchtranasction()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                ViewBag._userid = CRM_API.Helpers.Session.CurrentUser.user_login;
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    HttpContext.Session["GlobalProduct"] = "VMUK";
                }
                else
                {
                    var page = new financeviewtrans();
                    try
                    {
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = "MCM";
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }
                }
            }
            else
            {
                HttpContext.Session["GlobalProduct"] = "VMUK";
                ViewBag._sitecode = "MCM";
                ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
            }
            return View();
        }

        public ActionResult Tabfbreakagereport()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    HttpContext.Session["GlobalProduct"] = "VMUK";
                }
                else
                {
                    var page = new financeviewtrans();
                    try
                    {
                        if (HttpContext.Session["GlobalProduct"].ToString() == "VMFR")
                        {
                            ViewBag._sitecode = "MFR";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else if (HttpContext.Session["GlobalProduct"].ToString() == "VMAT")
                        {
                            ViewBag._sitecode = "BAU";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else if (HttpContext.Session["GlobalProduct"].ToString() == "VMNL")
                        {
                            ViewBag._sitecode = "BNL";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else if (HttpContext.Session["GlobalProduct"].ToString() == "VMBE")
                        {
                            ViewBag._sitecode = "MBE";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else
                        {
                            ViewBag._sitecode = "MCM";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }
                }
            }
            else
            {
                HttpContext.Session["GlobalProduct"] = "VMUK";
                ViewBag._sitecode = "MCM";
                ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
            }
            return View();
        }

        //AddBalanceReport
        public ActionResult Tabfaddbalancereport()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    HttpContext.Session["GlobalProduct"] = "VMUK";
                }
                else
                {
                    var page = new addbalancereport();
                    try
                    {
                        if (HttpContext.Session["GlobalProduct"].ToString() == "VMFR")
                        {
                            ViewBag._sitecode = "MFR";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else if (HttpContext.Session["GlobalProduct"].ToString() == "VMAT")
                        {
                            ViewBag._sitecode = "BAU";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else if (HttpContext.Session["GlobalProduct"].ToString() == "VMNL")
                        {
                            ViewBag._sitecode = "BNL";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else if (HttpContext.Session["GlobalProduct"].ToString() == "VMBE")
                        {
                            ViewBag._sitecode = "MBE";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else
                        {
                            ViewBag._sitecode = "MCM";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }
                }
            }
            else
            {
                HttpContext.Session["GlobalProduct"] = "VMUK";
                ViewBag._sitecode = "MCM";
                ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
            }
            return View();
        }

        //CancelTopup
        public ActionResult Tabfcanceltopup()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                ViewBag._userid = CRM_API.Helpers.Session.CurrentUser.user_login;

                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    HttpContext.Session["GlobalProduct"] = "VMUK";
                }
                else
                {
                    var page = new addbalancereport();
                    try
                    {
                        if (HttpContext.Session["GlobalProduct"].ToString() == "VMFR")
                        {
                            ViewBag._sitecode = "MFR";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else if (HttpContext.Session["GlobalProduct"].ToString() == "VMAT")
                        {
                            ViewBag._sitecode = "BAU";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else if (HttpContext.Session["GlobalProduct"].ToString() == "VMNL")
                        {
                            ViewBag._sitecode = "BNL";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else if (HttpContext.Session["GlobalProduct"].ToString() == "VMBE")
                        {
                            ViewBag._sitecode = "MBE";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else
                        {
                            ViewBag._sitecode = "MCM";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }
                }
            }
            else
            {
                HttpContext.Session["GlobalProduct"] = "VMUK";
                ViewBag._sitecode = "MCM";
                ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
            }
            return View();
        }

        //TransferBalanceReport
        public ActionResult Tabftransferbalancereport()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    HttpContext.Session["GlobalProduct"] = "VMUK";
                }
                else
                {
                    var page = new addbalancereport();
                    try
                    {
                        if (HttpContext.Session["GlobalProduct"].ToString() == "VMFR")
                        {
                            ViewBag._sitecode = "MFR";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else if (HttpContext.Session["GlobalProduct"].ToString() == "VMAT")
                        {
                            ViewBag._sitecode = "BAU";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else if (HttpContext.Session["GlobalProduct"].ToString() == "VMNL")
                        {
                            ViewBag._sitecode = "BNL";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else if (HttpContext.Session["GlobalProduct"].ToString() == "VMBE")
                        {
                            ViewBag._sitecode = "MBE";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else
                        {
                            ViewBag._sitecode = "MCM";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }
                }
            }
            else
            {
                HttpContext.Session["GlobalProduct"] = "VMUK";
                ViewBag._sitecode = "MCM";
                ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
            }
            return View();
        }

        //Issue History Report
        public ActionResult Tabfissuehistorylist()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                ViewBag._userid = CRM_API.Helpers.Session.CurrentUser.user_login;

                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    HttpContext.Session["GlobalProduct"] = "VMUK";
                }
                else
                {
                    var page = new addbalancereport();
                    try
                    {
                        if (HttpContext.Session["GlobalProduct"].ToString() == "VMFR")
                        {
                            ViewBag._sitecode = "MFR";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else if (HttpContext.Session["GlobalProduct"].ToString() == "VMAT")
                        {
                            ViewBag._sitecode = "BAU";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else if (HttpContext.Session["GlobalProduct"].ToString() == "VMNL")
                        {
                            ViewBag._sitecode = "BNL";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else if (HttpContext.Session["GlobalProduct"].ToString() == "VMBE")
                        {
                            ViewBag._sitecode = "MBE";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else
                        {
                            ViewBag._sitecode = "MCM";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }
                }
            }
            else
            {
                HttpContext.Session["GlobalProduct"] = "VMUK";
                ViewBag._sitecode = "MCM";
                ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
            }
            return View();
        }

        //05-Feb-2019 : Moorthy : Added for auto Topup Report
        public ActionResult Tabfautotopupreport()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                ViewBag._userid = CRM_API.Helpers.Session.CurrentUser.user_login;

                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    HttpContext.Session["GlobalProduct"] = "VMUK";
                }
                else
                {
                    try
                    {
                        if (HttpContext.Session["GlobalProduct"].ToString() == "VMFR")
                        {
                            ViewBag._sitecode = "MFR";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else if (HttpContext.Session["GlobalProduct"].ToString() == "VMAT")
                        {
                            ViewBag._sitecode = "BAU";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else if (HttpContext.Session["GlobalProduct"].ToString() == "VMNL")
                        {
                            ViewBag._sitecode = "BNL";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else if (HttpContext.Session["GlobalProduct"].ToString() == "VMBE")
                        {
                            ViewBag._sitecode = "MBE";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else
                        {
                            ViewBag._sitecode = "MCM";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                    }
                    catch (Exception ex)
                    {
                        
                    }
                }
            }
            else
            {
                HttpContext.Session["GlobalProduct"] = "VMUK";
                ViewBag._sitecode = "MCM";
                ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
            }
            return View();
        }

        //05-Feb-2019 : Moorthy : Added for General Report
        public ActionResult Tabfgeneralreport()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                ViewBag._userid = CRM_API.Helpers.Session.CurrentUser.user_login;

                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    HttpContext.Session["GlobalProduct"] = "VMUK";
                }
                else
                {
                    try
                    {
                        if (HttpContext.Session["GlobalProduct"].ToString() == "VMFR")
                        {
                            ViewBag._sitecode = "MFR";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else if (HttpContext.Session["GlobalProduct"].ToString() == "VMAT")
                        {
                            ViewBag._sitecode = "BAU";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else if (HttpContext.Session["GlobalProduct"].ToString() == "VMNL")
                        {
                            ViewBag._sitecode = "BNL";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else if (HttpContext.Session["GlobalProduct"].ToString() == "VMBE")
                        {
                            ViewBag._sitecode = "MBE";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else
                        {
                            ViewBag._sitecode = "MCM";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
            else
            {
                HttpContext.Session["GlobalProduct"] = "VMUK";
                ViewBag._sitecode = "MCM";
                ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
            }
            return View();
        }

        //Tabffreedatabundlereport
        public ActionResult Tabffreedatabundlereport()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    HttpContext.Session["GlobalProduct"] = "VMUK";
                }
                else
                {
                    var page = new addbalancereport();
                    try
                    {
                        if (HttpContext.Session["GlobalProduct"].ToString() == "VMFR")
                        {
                            ViewBag._sitecode = "MFR";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else if (HttpContext.Session["GlobalProduct"].ToString() == "VMAT")
                        {
                            ViewBag._sitecode = "BAU";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else if (HttpContext.Session["GlobalProduct"].ToString() == "VMNL")
                        {
                            ViewBag._sitecode = "BNL";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else if (HttpContext.Session["GlobalProduct"].ToString() == "VMBE")
                        {
                            ViewBag._sitecode = "MBE";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else
                        {
                            ViewBag._sitecode = "MCM";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }
                }
            }
            else
            {
                HttpContext.Session["GlobalProduct"] = "VMUK";
                ViewBag._sitecode = "MCM";
                ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
            }
            return View();
        }
    }
}
