﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRM3.WebApp.Attributes;
using System.Text;
using System.IO;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace CRM3.WebApp.Controllers
{
    [CRM3Auth]
    public class SIMOrderController : Controller
    {
        //
        // GET: /SIMOrder/

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        //
        // GET: /SIMOrder/List

        public ActionResult List()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    return View("List");
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        //
        // GET: /SIMOrder/Details/5

        public ActionResult Label()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    return View("Label");
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        //
        // GET: /SIMOrder/Search

        public ActionResult Search()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    return View("Search");
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }
        // Del

        public ActionResult FreeSIMOrder()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    if (HttpContext.Session["GlobalProduct"].ToString() == "VMFR")
                    {
                        ViewBag._sitecode = "MFR";
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    }
                    else if (HttpContext.Session["GlobalProduct"].ToString() == "VMAT")
                    {
                        ViewBag._sitecode = "BAU";
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    }
                    else if (HttpContext.Session["GlobalProduct"].ToString() == "VMNL")
                    {
                        ViewBag._sitecode = "BNL";
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    }
                    else if (HttpContext.Session["GlobalProduct"].ToString() == "VMBE")
                    {
                        ViewBag._sitecode = "MBE";
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    }
                    else
                    {
                        ViewBag._sitecode = "MCM";
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    }
                    return View("FreeSIMOrder");
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }
    }
}
