﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRM_API.Models.Permission;
using CRM3.WebApp.UserRoleEntity;
using System.Text;
using CRM3.WebApp.Attributes;
using System.Data.Entity;
using System.Data;
using Newtonsoft.Json;
using NLog;
using CRM_API.DB.Auth;
using CRM_API.Models;

namespace CRM3.WebApp.Controllers
{
    [CRM3Auth]
    public class EmailsController : Controller
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        [HttpGet]
        public ActionResult EmailList(string id)
        {
            CRM_API.Models.EmailModel data = new CRM_API.Models.EmailModel();
            //if (id == "1")
            //{
            //    if (CRM_API.Helpers.Session.CurrentUser.user_role == User_Role.Admin || CRM_API.Helpers.Session.CurrentUser.user_role == User_Role.Manager || CRM_API.Helpers.Session.CurrentUser.user_role == User_Role.TeamLeader)
            //    {
            //        data.ActiveTab = "TabEmailQueue";
            //    }
            //    else
            //        return RedirectToAction("Index", "Product");
            //}
            if (id == "1")
            {
                if ((CRM_API.Helpers.Session.CurrentUser.user_role == User_Role.Admin || CRM_API.Helpers.Session.CurrentUser.user_role == User_Role.Manager || CRM_API.Helpers.Session.CurrentUser.user_role == User_Role.TeamLeader) || CRM_API.Helpers.Session.CurrentUser.user_role == User_Role.Agent)
                {
                    data.ActiveTab = "TabMyEmailQueue";
                }
                else
                    return RedirectToAction("Index", "Product");
            }
            else
            {
                if ((CRM_API.Helpers.Session.CurrentUser.user_role == User_Role.Admin || CRM_API.Helpers.Session.CurrentUser.user_role == User_Role.Manager || CRM_API.Helpers.Session.CurrentUser.user_role == User_Role.TeamLeader) || CRM_API.Helpers.Session.CurrentUser.user_role == User_Role.EsclatingTeam)
                {
                    data.ActiveTab = "TabMyEmailTickets";
                }
                else
                    return RedirectToAction("Index", "Product");
            }

            ViewBag._userid = CRM_API.Helpers.Session.CurrentUser.userid;
            ViewBag._roleid = CRM_API.Helpers.Session.CurrentUser.user_role;
            ViewBag._role_id = CRM_API.Helpers.Session.CurrentUser.roleid;

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    if (HttpContext.Session["GlobalProduct"].ToString() == "VMFR")
                    {
                        ViewBag._sitecode = "MFR";
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    }
                    else if (HttpContext.Session["GlobalProduct"].ToString() == "VMAT")
                    {
                        ViewBag._sitecode = "BAU";
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    }
                    else if (HttpContext.Session["GlobalProduct"].ToString() == "VMNL")
                    {
                        ViewBag._sitecode = "BNL";
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    }
                    else if (HttpContext.Session["GlobalProduct"].ToString() == "VMBE")
                    {
                        ViewBag._sitecode = "MBE";
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    }
                    else
                    {
                        ViewBag._sitecode = "MCM";
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    }

                    return View(data);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        [HttpGet]
        public ActionResult EmailSettings(string id)
        {
            CRM_API.Models.EmailModel data = new CRM_API.Models.EmailModel();

            ViewBag._userid = CRM_API.Helpers.Session.CurrentUser.userid;
            ViewBag._roleid = CRM_API.Helpers.Session.CurrentUser.user_role;
            ViewBag._role_id = CRM_API.Helpers.Session.CurrentUser.roleid;

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    if (HttpContext.Session["GlobalProduct"].ToString() == "VMFR")
                    {
                        ViewBag._sitecode = "MFR";
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    }
                    else if (HttpContext.Session["GlobalProduct"].ToString() == "VMAT")
                    {
                        ViewBag._sitecode = "BAU";
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    }
                    else if (HttpContext.Session["GlobalProduct"].ToString() == "VMNL")
                    {
                        ViewBag._sitecode = "BNL";
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    }
                    else if (HttpContext.Session["GlobalProduct"].ToString() == "VMBE")
                    {
                        ViewBag._sitecode = "MBE";
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    }
                    else
                    {
                        ViewBag._sitecode = "MCM";
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    }

                    return View(data);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        [HttpGet]
        public ActionResult EmailSettings1(string id)
        {
            CRM_API.Models.EmailModel data = new CRM_API.Models.EmailModel();

            ViewBag._userid = CRM_API.Helpers.Session.CurrentUser.userid;
            ViewBag._roleid = CRM_API.Helpers.Session.CurrentUser.user_role;
            ViewBag._role_id = CRM_API.Helpers.Session.CurrentUser.roleid;

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    if (HttpContext.Session["GlobalProduct"].ToString() == "VMFR")
                    {
                        ViewBag._sitecode = "MFR";
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    }
                    else if (HttpContext.Session["GlobalProduct"].ToString() == "VMAT")
                    {
                        ViewBag._sitecode = "BAU";
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    }
                    else if (HttpContext.Session["GlobalProduct"].ToString() == "VMNL")
                    {
                        ViewBag._sitecode = "BNL";
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    }
                    else if (HttpContext.Session["GlobalProduct"].ToString() == "VMBE")
                    {
                        ViewBag._sitecode = "MBE";
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    }
                    else
                    {
                        ViewBag._sitecode = "MCM";
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    }

                    return View(data);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }
        public ActionResult TabEmailQueue(string id)
        {
            var page = new EmailModel();
            ViewBag.activetab = "TabEmailQueue";

            try
            {
                page.ActiveTab = "TabEmailQueue";
                ViewBag._userid = CRM_API.Helpers.Session.CurrentUser.userid;
                ViewBag._roleid = CRM_API.Helpers.Session.CurrentUser.user_role;
                ViewBag._role_id = CRM_API.Helpers.Session.CurrentUser.roleid;
            }
            catch (Exception ex)
            {
                page.ErrMsg = ex.Message;
            }
            return View();
        }

        public ActionResult TabMyEmailQueue(string id)
        {
            var page = new EmailModel();
            ViewBag.activetab = "TabMyEmailQueue";

            try
            {
                page.ActiveTab = "TabMyEmailQueue";
                ViewBag._userid = CRM_API.Helpers.Session.CurrentUser.userid;
                ViewBag._roleid = CRM_API.Helpers.Session.CurrentUser.user_role;
                ViewBag._role_id = CRM_API.Helpers.Session.CurrentUser.roleid;
            }
            catch (Exception ex)
            {
                page.ErrMsg = ex.Message;
            }
            return View();
        }

        public ActionResult TabMyEmailTickets(string id)
        {
            var page = new EmailModel();
            ViewBag.activetab = "TabMyEmailTickets";

            try
            {
                page.ActiveTab = "TabMyEmailTickets";
                ViewBag._userid = CRM_API.Helpers.Session.CurrentUser.userid;
                ViewBag._roleid = CRM_API.Helpers.Session.CurrentUser.user_role;
                ViewBag._role_id = CRM_API.Helpers.Session.CurrentUser.roleid;
            }
            catch (Exception ex)
            {
                page.ErrMsg = ex.Message;
            }
            return View();
        }
    }
}
