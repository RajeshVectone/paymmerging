﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRM_API.Models.Permission;
using CRM3.WebApp.UserRoleEntity;
using System.Text;
using CRM3.WebApp.Attributes;
using System.Data.Entity;
using System.Data;
using Newtonsoft.Json;
using NLog;
using CRM_API.DB.Auth;
using CRM_API.Models.Docs;

namespace CRM3.WebApp.Controllers
{
    [CRM3Auth]
    public class DocsController : Controller
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        [HttpGet]
        public ActionResult DocList(string id)
        {
            DocModel data = new DocModel();
            ViewBag._userid = CRM_API.Helpers.Session.CurrentUser.userid;
            ViewBag._roleid = CRM_API.Helpers.Session.CurrentUser.user_role;
            ViewBag._role_id = CRM_API.Helpers.Session.CurrentUser.roleid;

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    if (HttpContext.Session["GlobalProduct"].ToString() == "VMFR")
                    {
                        ViewBag._sitecode = "MFR";
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    }
                    else if (HttpContext.Session["GlobalProduct"].ToString() == "VMAT")
                    {
                        ViewBag._sitecode = "BAU";
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    }
                    else if (HttpContext.Session["GlobalProduct"].ToString() == "VMNL")
                    {
                        ViewBag._sitecode = "BNL";
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    }
                    else if (HttpContext.Session["GlobalProduct"].ToString() == "VMBE")
                    {
                        ViewBag._sitecode = "MBE";
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    }
                    else
                    {
                        ViewBag._sitecode = "MCM";
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    }

                    return View(data);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        [HttpGet]
        public ActionResult ArchiveList(string id)
        {
            DocModel data = new DocModel();
            ViewBag._userid = CRM_API.Helpers.Session.CurrentUser.userid;
            ViewBag._roleid = CRM_API.Helpers.Session.CurrentUser.user_role;
            ViewBag._role_id = CRM_API.Helpers.Session.CurrentUser.roleid;

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    if (HttpContext.Session["GlobalProduct"].ToString() == "VMFR")
                    {
                        ViewBag._sitecode = "MFR";
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    }
                    else if (HttpContext.Session["GlobalProduct"].ToString() == "VMAT")
                    {
                        ViewBag._sitecode = "BAU";
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    }
                    else if (HttpContext.Session["GlobalProduct"].ToString() == "VMNL")
                    {
                        ViewBag._sitecode = "BNL";
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    }
                    else if (HttpContext.Session["GlobalProduct"].ToString() == "VMBE")
                    {
                        ViewBag._sitecode = "MBE";
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    }
                    else
                    {
                        ViewBag._sitecode = "MCM";
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    }

                    return View(data);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        [HttpGet]
        public ActionResult DocUpload(string id)
        {
            DocModel data = new DocModel();
            ViewBag._userid = CRM_API.Helpers.Session.CurrentUser.userid;
            ViewBag._roleid = CRM_API.Helpers.Session.CurrentUser.user_role;
            ViewBag._role_id = CRM_API.Helpers.Session.CurrentUser.roleid;

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    if (HttpContext.Session["GlobalProduct"].ToString() == "VMFR")
                    {
                        ViewBag._sitecode = "MFR";
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    }
                    else if (HttpContext.Session["GlobalProduct"].ToString() == "VMAT")
                    {
                        ViewBag._sitecode = "BAU";
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    }
                    else if (HttpContext.Session["GlobalProduct"].ToString() == "VMNL")
                    {
                        ViewBag._sitecode = "BNL";
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    }
                    else if (HttpContext.Session["GlobalProduct"].ToString() == "VMBE")
                    {
                        ViewBag._sitecode = "MBE";
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    }
                    else
                    {
                        ViewBag._sitecode = "MCM";
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    }

                    return View(data);
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }
    }
}
