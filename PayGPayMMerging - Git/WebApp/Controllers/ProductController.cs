﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRM3.WebApp.Attributes;

namespace CRM3.WebApp.Controllers
{
    [CRM3Auth]
    public class ProductController : Controller
    {
        public ActionResult Index()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (!string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                }
            }

            return View();
        }

        public ActionResult Select(string Id, string RedirectToController = null, string RedirectToAct = null)
        {
            HttpContext.Session["GlobalProduct"] = Id;
            if (string.IsNullOrEmpty(RedirectToController) || string.IsNullOrEmpty(RedirectToAct))
            {
                //TODO Need to uncomment
                //Dashboard Page
                if (CRM_API.Helpers.Session.CurrentUser.user_role == CRM_API.Models.User_Role.Admin || CRM_API.Helpers.Session.CurrentUser.user_role == CRM_API.Models.User_Role.Manager || CRM_API.Helpers.Session.CurrentUser.user_role == CRM_API.Models.User_Role.TeamLeader)
                    return RedirectToAction("Index", "Home");
                else
                    return RedirectToAction("Search", "Customer");
            }
            else
            {
                return RedirectToAction(RedirectToAct, RedirectToController);
            }
        }

    }
}
