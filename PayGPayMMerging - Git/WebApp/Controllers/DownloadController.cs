﻿using System;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRM3.WebApp.Attributes;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Globalization;
using Stimulsoft.Report.Export;
using Stimulsoft.Report;
using NLog;
using CRM_API.Models.BreakageUsageModels;
using CRM_API.DB.BreakageUsage;
using CRM_API.Models;
using CRM_API.DB;
using CRM_API.Models.Complaint;
using CRM_API.Models.BundleService;
using System.Configuration;
using CRM_API.Models.SIMActivation;
using CRM_API.Models.AutoRenewalHistory;


namespace CRM3.WebApp.Controllers
{
    [CRM3Auth]
    public class DownloadController : Controller
    {
        static readonly Logger Log = LogManager.GetCurrentClassLogger();
        /* --------------------------------------------------------------------------------------------------------------- */
        /* SIM ORDER LIST */
        //Create file .txt Format
        //Download/Create_PayAway_DD_Collection_Files --> PayAway_DD_New_Instructions
        [HttpGet]
        public ActionResult Create_PayAway_DD_Collection_Files(string commands)
        {
            try
            {
                string fileName = "/PMBO_Files/" + "PayAway_Collection_" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + ".txt";
                // string fileName = "PMBO_Files\\" + "PayAway_Collection_" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + ".txt";
                string path = AppDomain.CurrentDomain.BaseDirectory + fileName;
                FileStream fl = new FileStream(path, FileMode.CreateNew);
                StreamWriter writer = new StreamWriter(fl, Encoding.UTF8);
                CRM_API.Models.PMBOSearch objSearch = new CRM_API.Models.PMBOSearch();
                objSearch.FromDate = null;
                objSearch.ToDate = null;
                objSearch.CCTransactionID = null;
                objSearch.SubscriberID = null;
                objSearch.SortCode = null;
                objSearch.AccountNumber = null;
                //Shifted by Barani to reduce the double time in downloading - Starts
                CRM_API.Models.log objLog = new CRM_API.Models.log();
                objLog.user_name = "Mundio";
                var additionalresult = CRM_API.DB.PMBODDOperations.SetPaymentStatus(objLog);
                List<CRM_API.Models.SetPaymentOutput> lstAddRes = additionalresult.ToList();
                //Shifted by Barani to reduce the double time in downloading - Ends
                var result = CRM_API.DB.PMBODDOperations.getPMBODDCollectionInstructions(objSearch);
                foreach (var resultView in result)
                {
                    //CRM_API.Models.PMBOSearch objNewValues = new CRM_API.Models.PMBOSearch();
                    //objNewValues.FromDate = null;
                    //objNewValues.ToDate = null;
                    //objNewValues.CCTransactionID = null;
                    //objNewValues.SubscriberID = resultView.subscriberid;
                    //objNewValues.SortCode = null;
                    //objNewValues.AccountNumber = null;
                    ////var CollectionResult = CRM_API.DB.PMBODDOperations.getPMBODDCollectionInstructions(objNewValues);
                    //foreach (var list in resultView)
                    //{
                    StringBuilder sb = new StringBuilder();
                    sb.Append((resultView.sort_code == string.Empty || resultView.sort_code == "" || resultView.sort_code == null || resultView.sort_code.Length < 6) ? null : resultView.sort_code.Substring(0, 2) + "-" + resultView.sort_code.Substring(2, 2) + "-" + resultView.sort_code.Substring(4, 2));
                    sb.Append(",");
                    sb.Append((resultView.account_number == string.Empty || resultView.account_number == "" || resultView.account_number == null) ? null : resultView.account_number);
                    sb.Append(",");
                    sb.Append((resultView.accountname == string.Empty || resultView.accountname == "" || resultView.accountname == null) ? null : resultView.accountname.Length > 18 ? resultView.accountname.Substring(0, 18) : resultView.accountname);
                    sb.Append(",");
                    sb.Append(Convert.ToDecimal(resultView.outstanding_amount).ToString("N2"));
                    sb.Append(",");
                    sb.Append((resultView.bank_reference == string.Empty || resultView.bank_reference == "" || resultView.bank_reference == null) ? null : resultView.bank_reference);
                    sb.Append(",");
                    sb.Append("17");
                    writer.WriteLine(sb.ToString());

                    //writer.Write((resultView.sort_code == string.Empty || resultView.sort_code == "" || resultView.sort_code == null) ? null : resultView.sort_code.Substring(0, 2) + "-" + resultView.sort_code.Substring(2, 2) + "-" + resultView.sort_code.Substring(4, 2)); writer.Write(",");
                    //writer.Write((resultView.account_number == string.Empty || resultView.account_number == "" || resultView.account_number == null) ? null : resultView.account_number); writer.Write(",");
                    //writer.Write((resultView.accountname == string.Empty || resultView.accountname == "" || resultView.accountname == null) ? null : resultView.accountname.Length > 18 ? resultView.accountname.Substring(0, 18) : resultView.accountname); writer.Write(",");
                    //writer.Write(Convert.ToDecimal(resultView.outstanding_amount).ToString("N2")); writer.Write(",");
                    //writer.Write((resultView.bank_reference == string.Empty || resultView.bank_reference == "" || resultView.bank_reference == null) ? null : resultView.bank_reference); writer.Write(",");
                    //writer.Write("17");
                    //writer.WriteLine();
                    //}
                }
                writer.Flush();
                fl.Position = 0;
                writer.Close();
                string[] lists = fileName.Split('/');
                //Save the Log Files
                //CRM_API.Models.log objLog = new CRM_API.Models.log();
                objLog.operation = 1;
                objLog.status = 17;
                objLog.status = -1;
                //objLog.user_name = "Mundio";
                objLog.operationid = 17;
                objLog.operation_name = "Collections Instructions";
                objLog.total_rows = Convert.ToInt32(result.Count());
                objLog.filename = lists[2].ToString();
                // objLog.filename = lists[0].ToString();
                objLog.notes = commands;
                objLog.opsid = 0;
                var Newresult = CRM_API.DB.PMBODDOperations.SavePMBOLog(objLog);
                //var additionalresult = CRM_API.DB.PMBODDOperations.SetPaymentStatus(objLog);
                //List<CRM_API.Models.SetPaymentOutput> lstAddRes= additionalresult.ToList();
                string getaddresult = lstAddRes[0].errmsg;
                HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=" + lists[2].ToString());
                // HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=" + lists[0].ToString());
                this.Response.ContentType = "application/vnd.txt";
                var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = 0, errmsg = "Ready", Text = lists[2].ToString() + "\r\n," + getaddresult };
                // var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = 0, errmsg = "Ready", Text = lists[0].ToString() + "\r\n," + getaddresult };
                return Json(resy, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //Create file .txt Format
        //Download/Create_PayAway_DD_New_Files --> PayAway_DD_New_Instructions
        //[HttpGet]
        [HttpPost]
        public ActionResult Create_PayAway_DD_New_Files(string count, string subId, string commands)
        {
            try
            {
                string fileName = "/PMBO_Files/" + "PayAway_DD_" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + ".csv";
                string path = AppDomain.CurrentDomain.BaseDirectory + fileName;
                FileStream fl = new FileStream(path, FileMode.CreateNew);
                StreamWriter writer = new StreamWriter(fl, Encoding.UTF8);
                //For SubscriberID.
                foreach (string subID in subId.Split(','))
                {
                    if (!string.IsNullOrEmpty(subID))
                    {
                        CRM_API.Models.PMBOSearch objSearch = new CRM_API.Models.PMBOSearch();
                        objSearch.FromDate = null;
                        objSearch.ToDate = null;
                        objSearch.CCTransactionID = null;
                        objSearch.SubscriberID = subID;
                        objSearch.SortCode = null;
                        objSearch.AccountNumber = null;
                        //getting the exact output.
                        var result = CRM_API.DB.PMBODDOperations.getPMBODDNewInstructions(objSearch);
                        //Set the DD-Opeartions.
                        var collectionSubID = CRM_API.DB.PMBODDOperations.savePMBOSaveCollections(subID, 11, "Mundio").ToArray();
                        if (collectionSubID[0].email != null)
                        {
                            foreach (var newresult in result)
                            {
                                writer.Write((newresult.sort_code == string.Empty || newresult.sort_code == "" || newresult.sort_code == null) ? null : newresult.sort_code.Substring(0, 2) + "-" + newresult.sort_code.Substring(2, 2) + "-" + newresult.sort_code.Substring(4, 2)); writer.Write(",");
                                writer.Write((newresult.account_number == string.Empty || newresult.account_number == "" || newresult.account_number == null) ? null : newresult.account_number); writer.Write(",");
                                writer.Write((newresult.accountname == string.Empty || newresult.accountname == "" || newresult.accountname == null) ? null : newresult.accountname.Length > 18 ? newresult.accountname.Substring(0, 18) : newresult.accountname); writer.Write(",");
                                writer.Write(Convert.ToDecimal(newresult.outstanding_amount).ToString("N2")); writer.Write(",");
                                writer.Write((newresult.bank_reference == string.Empty || newresult.bank_reference == "" || newresult.bank_reference == null) ? null : newresult.bank_reference); writer.Write(",");
                                writer.Write("0N");
                                writer.WriteLine();
                            }
                        }
                    }
                }
                writer.Flush();
                fl.Position = 0;
                writer.Close();
                string[] list = fileName.Split('/');
                //Save the Log Files
                CRM_API.Models.log objLog = new CRM_API.Models.log();
                objLog.operation = 1;
                objLog.status = 1;
                objLog.status = -1;
                objLog.user_name = "Mundio";
                objLog.operationid = 1;
                objLog.operation_name = "DD New Instructions";
                objLog.total_rows = Convert.ToInt32(count);
                objLog.filename = list[2].ToString();
                objLog.notes = commands;
                objLog.opsid = 0;
                var Newresult = CRM_API.DB.PMBODDOperations.SavePMBOLog(objLog);

                HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=" + list[2].ToString());
                this.Response.ContentType = "application/vnd.csv";
                var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = 0, errmsg = "Ready", Text = list[2].ToString(), };
                return Json(resy, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [HttpPost]
        public ActionResult DownloadCDRHistory(CRM_API.Models.CDR.CallHistorySearch In)
        {
            var result = GetCDRHistory_Downloadable(In);
            var HistoryName = "";
            StringBuilder str = new StringBuilder();
            str.Append("<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv=\"content-type\" content=\"text/plain; charset=UTF-8\"/></head><body>");
            str.Append("<head><style>.xlLongDate{mso-number-format:'dd-mm-yyyy hh:mm:ss AM/PM';}.xlText{mso-number-format:'@';}</style></head>");
            var dateFrom = In.Date_from_day_string + "/" + In.Date_from_month_string + "/" + In.Date_from_year_string;
            var dateTo = In.Date_To_day_string + "/" + In.Date_to_month_string + "/" + In.Date_To_year_string;
            switch (In.HistoryType)
            {
                //Added by karthik Jira:CRMT-198
                case CRM_API.Models.CDR.CDRType.AllHistory:
                    HistoryName = "AllHistory";
                    str.Append("<h1>All History</h1>");
                    str.Append("<h3>Period : " + dateFrom + " - " + dateTo + "</h3>");
                    FormatDataAllHistory(result, ref str);
                    break;
                case CRM_API.Models.CDR.CDRType.CallHistory:
                    HistoryName = "CallHistory";
                    str.Append("<h1>Call History</h1>");
                    str.Append("<h3>Period : " + dateFrom + " - " + dateTo + "</h3>");
                    FormatDataCallHistory(result, ref str);
                    break;
                case CRM_API.Models.CDR.CDRType.SMSHistory:
                    HistoryName = "SMSHistory";
                    str.Append("<h1>SMS/Text History</h1>");
                    str.Append("<h3>Period : " + dateFrom + " - " + dateTo + "</h3>");
                    FormatDataSMSHistory(result, ref str);
                    break;
                case CRM_API.Models.CDR.CDRType.GPRSHistory:
                    HistoryName = "GPRSHistory";
                    str.Append("<h1>GPRS/Data History</h1>");
                    str.Append("<h3>Period : " + dateFrom + " - " + dateTo + "</h3>");
                    FormatDataGPRSHistory(result, ref str);
                    break;
                case CRM_API.Models.CDR.CDRType.ExcessUsage:
                    HistoryName = "ExcessUsage";
                    str.Append("<h1>Excess Usage</h1>");
                    str.Append("<h3>Period : " + dateFrom + " - " + dateTo + "</h3>");
                    FormatDataAllHistory(result, ref str);
                    break;
                default: break;
            }
            str.Append("</html>");

            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=" + HistoryName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(str.ToString());
            string _random = Guid.NewGuid().ToString();
            TempData[_random] = str.ToString();
            var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = 0, errmsg = HistoryName, Text = _random, };
            return Json(resy, JsonRequestBehavior.AllowGet);
        }

        //CRMT-232  Usage history download
        [HttpPost]
        public ActionResult DownloadUsageHistory(CRM_API.Models.TotalUsageHistory.CustomerTotalUsageHistoryRequest In)
        {
            var result = GetUsageHistory_Downloadable(In);
            var HistoryName = "";
            StringBuilder str = new StringBuilder();
            str.Append("<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv=\"content-type\" content=\"text/plain; charset=UTF-8\"/></head><body>");
            str.Append("<head><style>.xlLongDate{mso-number-format:'dd-mm-yyyy hh:mm:ss AM/PM';}.xlText{mso-number-format:'@';}</style></head>");

            var dateFrom = In.Date_from_day_string + "/" + In.Date_from_month_string + "/" + In.Date_from_year_string;
            var dateTo = In.Date_To_day_string + "/" + In.Date_to_month_string + "/" + In.Date_To_year_string;

            HistoryName = "UsageHistory";
            str.Append("<h1>Usage History</h1>");
            str.Append("<h3>Period : " + dateFrom + " - " + dateTo + "</h3>");
            FormatDataUsageHistoryDowload(result, ref str);
            str.Append("</html>");

            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=" + HistoryName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(str.ToString());
            string _random = Guid.NewGuid().ToString();
            TempData[_random] = str.ToString();
            var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = 0, errmsg = HistoryName, Text = _random, };
            return Json(resy, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DownloadCountryDetails(String Area, String AreaId, String CountryName, String searchvalue, Boolean isDownload, String api)
        {
            //var result = GetSimOrder_Downloadable(listRequest);

            var result = CRM_API.DB.viewtransaction.lottmproductareaid(Area, AreaId);

            StringBuilder str = new StringBuilder();
            str.Append("<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv=\"content-type\" content=\"text/plain; charset=UTF-8\"/><style>.xlText{ mso-number-format:'@';}</style></head><body>");
            str.Append("<h1>Number Pool List</h1>");
            str.Append(@"<h3>Country\City : <span>" + CountryName + @"\" + Area + "</span></h3>");
            //if (string.IsNullOrEmpty(listRequest.searchby))
            //{
            //    str.Append("<h3>Period : " + listRequest.fromdate.ToString() + " - " + listRequest.todate.ToString() + "</h3>");
            //}
            str.Append("<table>");
            str.Append("<thead>");
            str.Append("<tr>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>LLOM Number</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Vectone Number</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Country</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Area</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Plan</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Start Date</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Subscription Date</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Duration(Inactive Days)</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Status</th>");
            str.Append("</tr>");
            str.Append("</thead>");
            str.Append("<tbody>");

            foreach (CRM_API.Models.lotgareanameid val in result)
            {
                str.Append("<tr>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.LLOMNumber) ? "" : val.LLOMNumber) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.VectoneNumber) ? "" : val.VectoneNumber) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Country) ? "" : val.Country) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.area_name) ? "" : val.area_name) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Plan) ? "" : val.Plan) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;' class='xlText'>" + (string.IsNullOrEmpty(val.StartDate) ? "" : val.StartDate) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.SubscriptionDate) ? "" : val.SubscriptionDate) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Duration) ? "" : val.Duration) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.status) ? "" : val.status) + "</td>");
                str.Append("</tr>");
            }

            str.Append("</tbody>");
            str.Append("</table>");
            str.Append("</body></html>");


            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=NumberPoolDetailsByArea_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(str.ToString());
            string _random = Guid.NewGuid().ToString();
            TempData[_random] = str.ToString();
            var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = 0, errmsg = "Download Ready", Text = _random };
            return Json(resy, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public ActionResult DownloadCreateTransaction(string paymentMode)
        {
            IEnumerable<CRM_API.Models.financeviewtransdownload> objNewViewTransaction = null;
            string Status = "";
            if (string.IsNullOrEmpty(paymentMode))
            {
                //objNewViewTransaction = CRM_API.DB.viewtransaction.getpaymentdetails().ToArray();
                objNewViewTransaction = CRM_API.DB.viewtransaction.getpaymentdetailsdownload().ToArray();
                Status = "All";
            }
            else if (paymentMode == "100")
            {
                objNewViewTransaction = CRM_API.DB.viewtransaction.geterrrCodedownload("get_latest_details_from_tpayment_by_errcode_V6", paymentMode);
                Status = "Accepted";
            }
            else if (paymentMode == "101")
            {
                objNewViewTransaction = CRM_API.DB.viewtransaction.geterrrCodedownload("get_latest_details_from_tpayment_by_errcode_V7", paymentMode);
                Status = "Rejected";
            }
            else if (paymentMode == "200")
            {
                objNewViewTransaction = CRM_API.DB.viewtransaction.geterrrCodedownload("get_latest_details_from_tpayment_by_errcode_V8", paymentMode);
                Status = "Manually Accepted";
            }
            else if (paymentMode == "201")
            {
                objNewViewTransaction = CRM_API.DB.viewtransaction.geterrrCodedownload("get_latest_details_from_tpayment_by_errcode_V9", paymentMode);
                Status = "Manually Rejected";
            }
            else if (paymentMode == "301")
            {
                objNewViewTransaction = CRM_API.DB.viewtransaction.getpaymentdetailsautotop();
                Status = "Auto Top";
            }
            var result = objNewViewTransaction.ToList();
            StringBuilder str = new StringBuilder();
            var filename = "ViewTransactions";
            str.Append("<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{View Transactions}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv=\"content-type\" content=\"text/plain; charset=UTF-8\"/><style>.xlText{ mso-number-format:'@';}</style></head><body>");
            str.Append("<h1>View Transactions </h1>");
            str.Append("<h3>Transaction Status : <span>" + Status + "</span></h3>");
            str.Append("<table>");
            str.Append("<thead>");
            str.Append("<tr>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Reference ID</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Payment Agent</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Product Code</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Account ID</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>CC No</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Amount</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Currency</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Created Date</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Subscription ID</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Status</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>IpAddress</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Email</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Balance</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Code</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Reject Reason</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>ECI</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Fraud Score</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Days_from_1st_login</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Days_from_1st_topup</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>days_from_Lasttopup</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>total_spend</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>total_spend_consum</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>spend_last3months</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>spend_curr_month</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Noof_cardtrans</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Noof_voucher_trans</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Noof_failed_cardtrans</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Noof_success_cardtrans_30Days</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Noof_Fail_cardtrans_30Days</th>");
            //str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Request Id</th>");
            //str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Farud Score</th>");
            str.Append("</tr>");
            str.Append("</thead>");
            str.Append("<tbody>");



            foreach (CRM_API.Models.financeviewtransdownload val in result)
            {
                str.Append("<tr>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.REFERENCE_ID) ? "" : val.REFERENCE_ID) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.PAYMENT_AGENT) ? "" : val.PAYMENT_AGENT) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.PRODUCT_CODE) ? "" : val.PRODUCT_CODE) + "</td>");
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.ACCOUNT_ID) ? "" : val.ACCOUNT_ID) + "</td>");
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.CC_NO) ? "" : val.CC_NO) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.TOTALAMOUNT) ? "" : val.TOTALAMOUNT) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.CURRENCY) ? "" : val.CURRENCY) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.CREATED_DATE) ? "" : val.CREATED_DATE) + "</td>");
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.SUBSCRIPTIONID) ? "" : val.SUBSCRIPTIONID) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.CURRENT_STATUS) ? "" : val.CURRENT_STATUS) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.IP_Address) ? "" : val.IP_Address) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.email) ? "" : val.email) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Current_Balance) ? "" : val.Current_Balance) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.CS_ERROR_CODE) ? "" : val.CS_ERROR_CODE) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.ReasonforReject) ? "" : val.ReasonforReject) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.ECI_Value) ? "" : val.ECI_Value) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.fraud_score) ? "" : val.fraud_score) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Days_from_1st_login) ? "" : val.Days_from_1st_login) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Days_from_1st_topup) ? "" : val.Days_from_1st_topup) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.days_from_Lasttopup) ? "" : val.days_from_Lasttopup) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.total_spend) ? "" : val.total_spend) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.total_spend_consum) ? "" : val.total_spend_consum) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.spend_last3months) ? "" : val.spend_last3months) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.spend_curr_month) ? "" : val.spend_curr_month) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Noof_cardtrans) ? "" : val.Noof_cardtrans) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Noof_voucher_trans) ? "" : val.Noof_voucher_trans) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Noof_failed_cardtrans) ? "" : val.Noof_failed_cardtrans) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Noof_success_cardtrans_30Days) ? "" : val.Noof_success_cardtrans_30Days) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Noof_Fail_cardtrans_30Days) ? "" : val.Noof_Fail_cardtrans_30Days) + "</td>");
                // str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.requestid) ? "" : val.requestid) + "</td>");
                // str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.fraud_score) ? "" : val.fraud_score) + "</td>");
                str.Append("</tr>");
            }
            str.Append("</tbody>");
            str.Append("</table>");
            str.Append("</body></html>");

            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=ViewTransaction_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            //HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=" + filename + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(str.ToString());
            string _random = Guid.NewGuid().ToString();
            TempData[_random] = str.ToString();

            var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = 0, errmsg = "Download Ready", Text = _random };
            return Json(resy, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult DownloadCreateTransactionNew(string paymentMode)
        {
            try
            {
                Log.Info("DownloadCreateTransactionNew");
                IEnumerable<CRM_API.Models.financeviewtransdownload> objNewViewTransaction = null;
                string Status = "";
                if (string.IsNullOrEmpty(paymentMode))
                {
                    //objNewViewTransaction = CRM_API.DB.viewtransaction.getpaymentdetails().ToArray();
                    objNewViewTransaction = CRM_API.DB.viewtransaction.getpaymentdetailsdownload().ToArray();
                    Status = "All";
                }
                else if (paymentMode == "100")
                {
                    objNewViewTransaction = CRM_API.DB.viewtransaction.geterrrCodedownload("get_latest_details_from_tpayment_by_errcode_V6", paymentMode);
                    Status = "Accepted";
                }
                else if (paymentMode == "101")
                {
                    objNewViewTransaction = CRM_API.DB.viewtransaction.geterrrCodedownload("get_latest_details_from_tpayment_by_errcode_V7", paymentMode);
                    Status = "Rejected";
                }
                else if (paymentMode == "200")
                {
                    objNewViewTransaction = CRM_API.DB.viewtransaction.geterrrCodedownload("get_latest_details_from_tpayment_by_errcode_V8", paymentMode);
                    Status = "Manually Accepted";
                }
                else if (paymentMode == "201")
                {
                    objNewViewTransaction = CRM_API.DB.viewtransaction.geterrrCodedownload("get_latest_details_from_tpayment_by_errcode_V9", paymentMode);
                    Status = "Manually Rejected";
                }
                else if (paymentMode == "301")
                {
                    objNewViewTransaction = CRM_API.DB.viewtransaction.getpaymentdetailsautotop();
                    Status = "Auto Top";
                }
                var result = objNewViewTransaction.ToList();
                StringBuilder str = new StringBuilder();
                var filename = "ViewTransactions";
                str.Append("<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{View Transactions}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv=\"content-type\" content=\"text/plain; charset=UTF-8\"/><style>.xlText{ mso-number-format:'@';}</style></head><body>");
                str.Append("<h1>View Transactions </h1>");
                str.Append("<h3>Transaction Status : <span>" + Status + "</span></h3>");
                str.Append("<table>");
                str.Append("<thead>");
                str.Append("<tr>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Reference ID</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Payment Agent</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Product Code</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Account ID</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>CC No</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Amount</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Currency</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Created Date</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Subscription ID</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Status</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>IpAddress</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Email</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Balance</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Code</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Reject Reason</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>ECI</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Fraud Score</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Days_from_1st_login</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Days_from_1st_topup</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>days_from_Lasttopup</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>total_spend</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>total_spend_consum</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>spend_last3months</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>spend_curr_month</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Noof_cardtrans</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Noof_voucher_trans</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Noof_failed_cardtrans</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Noof_success_cardtrans_30Days</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Noof_Fail_cardtrans_30Days</th>");
                //str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Request Id</th>");
                //str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Farud Score</th>");
                //26-Aug-2019 : Moorthy added for retrieving SRD value
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Scheme Reference Data</th>");
                str.Append("</tr>");
                str.Append("</thead>");
                str.Append("<tbody>");



                foreach (CRM_API.Models.financeviewtransdownload val in result)
                {
                    str.Append("<tr>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.REFERENCE_ID) ? "" : val.REFERENCE_ID) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.PAYMENT_AGENT) ? "" : val.PAYMENT_AGENT) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.PRODUCT_CODE) ? "" : val.PRODUCT_CODE) + "</td>");
                    str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.ACCOUNT_ID) ? "" : val.ACCOUNT_ID) + "</td>");
                    str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.CC_NO) ? "" : val.CC_NO) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.TOTALAMOUNT) ? "" : val.TOTALAMOUNT) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.CURRENCY) ? "" : val.CURRENCY) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.CREATED_DATE) ? "" : val.CREATED_DATE) + "</td>");
                    str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.SUBSCRIPTIONID) ? "" : val.SUBSCRIPTIONID) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.CURRENT_STATUS) ? "" : val.CURRENT_STATUS) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.IP_Address) ? "" : val.IP_Address) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.email) ? "" : val.email) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Current_Balance) ? "" : val.Current_Balance) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.CS_ERROR_CODE) ? "" : val.CS_ERROR_CODE) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.ReasonforReject) ? "" : val.ReasonforReject) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.ECI_Value) ? "" : val.ECI_Value) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.fraud_score) ? "" : val.fraud_score) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Days_from_1st_login) ? "" : val.Days_from_1st_login) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Days_from_1st_topup) ? "" : val.Days_from_1st_topup) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.days_from_Lasttopup) ? "" : val.days_from_Lasttopup) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.total_spend) ? "" : val.total_spend) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.total_spend_consum) ? "" : val.total_spend_consum) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.spend_last3months) ? "" : val.spend_last3months) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.spend_curr_month) ? "" : val.spend_curr_month) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Noof_cardtrans) ? "" : val.Noof_cardtrans) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Noof_voucher_trans) ? "" : val.Noof_voucher_trans) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Noof_failed_cardtrans) ? "" : val.Noof_failed_cardtrans) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Noof_success_cardtrans_30Days) ? "" : val.Noof_success_cardtrans_30Days) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Noof_Fail_cardtrans_30Days) ? "" : val.Noof_Fail_cardtrans_30Days) + "</td>");
                    // str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.requestid) ? "" : val.requestid) + "</td>");
                    // str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.fraud_score) ? "" : val.fraud_score) + "</td>");
                    //26-Aug-2019 : Moorthy added for retrieving SRD value
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.srd) ? "" : val.srd) + "</td>");
                    str.Append("</tr>");
                }
                str.Append("</tbody>");
                str.Append("</table>");
                str.Append("</body></html>");

                HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=ViewTransaction_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
                //HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=" + filename + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
                this.Response.ContentType = "application/vnd.ms-excel";
                byte[] temp = System.Text.Encoding.UTF8.GetBytes(str.ToString());
                string _random = Guid.NewGuid().ToString();
                TempData[_random] = str.ToString();

                var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = 0, errmsg = "Download Ready", Text = _random };
                return Json(resy, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = -1, errmsg = ex.Message };
                return Json(resy, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult DownloadCreateTransactionMarketing(string paymentMode)
        {
            IEnumerable<CRM_API.Models.marketinggrid> objNewViewTransaction = null;
            string Status = "";
            string name = "";
            if (paymentMode == "1")
            {
                name = "ShipmentConfirmation";
            }
            if (paymentMode == "2")
            {
                name = "DeliveryCheckback";
            }
            if (paymentMode == "3")
            {
                name = "Activation";
            }
            if (paymentMode == "4")
            {
                name = "Reminder1";
            }
            if (paymentMode == "5")
            {
                name = "Reminder2";
            }
            if (paymentMode == "6")
            {
                name = "Reminder3";
            }

            objNewViewTransaction = CRM_API.DB.gettransaction.geterrrCodedownload("get_sim_Marketing_Reports", paymentMode);
            var result = objNewViewTransaction.ToList();
            StringBuilder str = new StringBuilder();
            str.Append("<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{Sim Order}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv=\"content-type\" content=\"text/plain; charset=UTF-8\"/><style>.xlText{ mso-number-format:'@';}</style></head><body>");
            str.Append("<h1>" + name + " Report</h1>");
            str.Append("<h3>Downloaded Date : <span>" + DateTime.Now + "</span></h3>");
            str.Append("<table>");
            str.Append("<thead>");
            str.Append("<tr>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>FreesimId</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Iccid</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Email_Address</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>First_Name</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Last_Name</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Address</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>City</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Mobile_Number</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Activate_Date</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Sent_Date</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Source_Address</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Ordersim_Url</th>");
            str.Append("</tr>");
            str.Append("</thead>");
            str.Append("<tbody>");
            foreach (CRM_API.Models.marketinggrid val in result)
            {
                str.Append("<tr>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.FreesimId) ? "" : val.FreesimId) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.ICCID) ? "" : val.ICCID) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Email_Address) ? "" : val.Email_Address) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.First_Name) ? "" : val.First_Name) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Last_Name) ? "" : val.Last_Name) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Address) ? "" : val.Address) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.City) ? "" : val.City) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Mobile_Number) ? "" : val.Mobile_Number) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Activate_Date) ? "" : val.Activate_Date) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Sent_Date) ? "" : val.Sent_Date) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Source_Address) ? "" : val.Source_Address) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Ordersim_Url) ? "" : val.Ordersim_Url) + "</td>");


                str.Append("</tr>");
            }
            str.Append("</tbody>");
            str.Append("</table>");
            str.Append("</body></html>");



            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=SIMORDER_" + name + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(str.ToString());
            string _random = Guid.NewGuid().ToString();
            string _name = "Simorder_" + name;
            TempData[_random] = str.ToString();
            Session["filename"] = "simorder_" + name;
            var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = 0, errmsg = "Download Ready", Text = _random };
            return Json(resy, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult DownloadTransactionMarketing_v1(string product, string type, string fromdate, string todate, string loggedin, string toppedup)
        {
            IEnumerable<CRM_API.Models.marketinggrid_v2> objNewViewTransaction = null;
            string Status = "";
            string name = "";
            string conditions = "";

            if (type == "1")
            {
                name = "SentDate";
            }
            if (type == "2")
            {
                name = "TopUp";
            }
            if (type == "3")
            {
                name = "FirstUpdate";
            }

            if (loggedin == "1")
            {
                conditions = "Customerhasloggedin";
            }
            if (toppedup == "1")
            {
                conditions = "Customerhastoppedin ";
            }

            if (toppedup == "1" && loggedin == "1")
            {
                conditions = "Customerhastoppedin& " + "Customerhastoppedin";
            }


            objNewViewTransaction = CRM_API.DB.gettransaction.geterrrCodedownload_v1(product, type, fromdate, todate, loggedin, toppedup);
            var result = objNewViewTransaction.ToList();
            StringBuilder str = new StringBuilder();
            str.Append("<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{Sim Order}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv=\"content-type\" content=\"text/plain; charset=UTF-8\"/><style>.xlText{ mso-number-format:'@';}</style></head><body>");
            str.Append("<h1>" + name + " Report</h1>");
            str.Append("<h3>Downloaded Date : <span>" + DateTime.Now + "</span></h3>");
            str.Append("<h3>Date Duration : <span>" + fromdate + " to " + todate + " </span></h3>");
            str.Append("<h4>Conditions : <span>" + conditions + "</span></h3>");
            str.Append("<table>");
            str.Append("<thead>");
            str.Append("<tr>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>FreesimId</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Iccid</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Email_Address</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>First_Name</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Last_Name</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Address</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>City</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Mobile_Number</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Activate_Date</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Sent_Date</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Source_Address</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Ordersim_Url</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>First_update</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Last_update</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Last_Topupdate</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>SimType</th>");
            str.Append("</tr>");
            str.Append("</thead>");
            str.Append("<tbody>");
            foreach (CRM_API.Models.marketinggrid_v2 val in result)
            {
                str.Append("<tr>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.FreesimId) ? "" : val.FreesimId) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.ICCID) ? "" : val.ICCID) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Email_Address) ? "" : val.Email_Address) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.First_Name) ? "" : val.First_Name) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Last_Name) ? "" : val.Last_Name) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Address) ? "" : val.Address) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.City) ? "" : val.City) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Mobile_Number) ? "" : val.Mobile_Number) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Activate_Date) ? "" : val.Activate_Date) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Sent_Date) ? "" : val.Sent_Date) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Source_Address) ? "" : val.Source_Address) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Ordersim_Url) ? "" : val.Ordersim_Url) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.First_update) ? "" : val.First_update) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Last_update) ? "" : val.Last_update) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Last_Topupdate) ? "" : val.Last_Topupdate) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.SimType) ? "" : val.SimType) + "</td>");


                str.Append("</tr>");
            }
            str.Append("</tbody>");
            str.Append("</table>");
            str.Append("</body></html>");



            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=SIMORDER_" + name + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(str.ToString());
            string _random = Guid.NewGuid().ToString();
            string _name = "Simorder_" + name;
            TempData[_random] = str.ToString();
            Session["filename"] = "simorder_" + name;
            var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = 0, errmsg = "Download Ready", Text = _random };
            return Json(resy, JsonRequestBehavior.AllowGet);
        }

        //Dowload PDF for PAYM - Bill Generations
        [HttpGet]
        public ActionResult DownloadPAYMBills(string MobileNo, string year, string Month)
        {
            //Calculation for From date --period ex : 201403, To date --billmonth ex : 201404
            try
            {
                string fromdate = "";
                string todate = "";
                if (Month != "12")
                {
                    if (Convert.ToInt32(Month) > 1)
                    {
                        if (Month.Length == 1) { fromdate = "0" + (Convert.ToInt32(Month) - 1).ToString(); }
                        else { fromdate = (Convert.ToInt32(Month) - 1).ToString(); }
                        fromdate = year + fromdate;
                        if (Month.Length == 1) { todate = "0" + (Convert.ToInt32(Month) - 1).ToString(); }
                        else { todate = (Convert.ToInt32(Month) - 1).ToString(); }
                        todate = year + todate;
                    }
                    else if (Convert.ToInt32(Month) == 1)
                    {
                        fromdate = (Convert.ToInt32(year) - 1).ToString() + "12";
                        todate = Convert.ToInt32(year).ToString() + "01";
                    }
                }
                else
                {
                    fromdate = year + "11";
                    todate = year.ToString() + "12";
                }
                Stimulsoft.Report.StiReport report = new Stimulsoft.Report.StiReport();
                report.Load(Server.MapPath("~/Vectone Bill_004.mrt"));
                report["@mobileno"] = MobileNo.ToString();
                report["@period"] = fromdate.ToString();
                report["@billmonth"] = todate.ToString();
                report.Render();
                if (Month.Length > 1) { }
                else { Month = "0" + Month; }
                string fileName = "PAYM_" + MobileNo + "_" + Month + "_" + year + ".pdf";
                if (System.IO.File.Exists(Server.MapPath("~/Download/" + fileName)))
                {
                    System.IO.File.Delete(Server.MapPath("~/Download/" + fileName));
                }
                StiPdfExportSettings objPDFExport = new StiPdfExportSettings();
                report.ExportDocument(StiExportFormat.Pdf, Server.MapPath("~/Download/" + fileName), objPDFExport);
                var resy = new CRM_API.Models.Common.ErrCodeMsgExt { err_code = 0, err_message = fileName, Text = null };
                return Json(resy, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var resy = new CRM_API.Models.Common.ErrCodeMsgExt { err_code = 0, err_message = ex.Message, Text = null };
                return Json(resy, JsonRequestBehavior.AllowGet);
            }
        }

        //Download the Both Pay_Way_Section.
        public ActionResult DownloadPaywayFiles(string fileId, string sitecode)
        {
            try
            {
                var fileNames = CRM_API.DB.PMBOPayway.getFileNames(fileId).ToArray();
                string path = AppDomain.CurrentDomain.BaseDirectory + "PMBO_Files/";
                //string path = AppDomain.CurrentDomain.BaseDirectory ;
                var fileContents = System.IO.File.ReadAllText(path + fileNames[0].filename);
                HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=" + fileNames[0].filename);
                this.Response.ContentType = "application/vnd.txt";
                byte[] temp = System.Text.Encoding.UTF8.GetBytes(fileContents.ToString());
                string _random = Guid.NewGuid().ToString();
                TempData[_random] = fileContents.ToString();
                //Save the Log Files
                CRM_API.Models.log objLog = new CRM_API.Models.log();
                objLog.operation = 2;
                objLog.user_name = "Mundio";
                objLog.status = 1;
                objLog.opsid = Convert.ToInt32(fileId);
                var Newresult = CRM_API.DB.PMBODDOperations.SavePMBOLog(objLog);
                //Status Chages
                CRM_API.DB.PMBOPayway.getUpdateStatus(fileId);
                var resy = new CRM_API.Models.Common.ErrCodeMsgExt { err_code = 0, err_message = fileNames[0].filename, Text = _random };
                return Json(resy, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var resy = new CRM_API.Models.Common.ErrCodeMsgExt { err_code = -1, err_message = ex.Message, Text = ex.Message };
                return Json(resy, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public FileContentResult DownloadPaywaysList(string id, string filename)
        {
            var result = (string)TempData[id];
            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
            this.Response.ContentType = "application/vnd.txt";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(result.ToString());
            return new FileContentResult(temp, "application/vnd.txt");
        }

        [HttpPost]
        public ActionResult DownloadProductDetails(String paymentMode, String searchvalue, Boolean isDownload, String api)
        {
            //var result = GetSimOrder_Downloadable(listRequest);


            IEnumerable<CRM_API.Models.lotgproductcityview_v2> lotgareanameids = CRM_API.DB.viewtransaction.lottmproductcityview_v3(paymentMode);



            StringBuilder str = new StringBuilder();

            str.Append("<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv=\"content-type\" content=\"text/plain; charset=UTF-8\"/><style>.xlText{ mso-number-format:'@';}</style></head><body>");
            str.Append("<h1>SIM Order List</h1>");
            str.Append("<h3>Product : <span>" + paymentMode + "</span></h3>");
            /*if (string.IsNullOrEmpty(listRequest.searchby))
            {
                str.Append("<h3>Period : " + listRequest.fromdate.ToString() + " - " + listRequest.todate.ToString() + "</h3>");
            }
            */
            str.Append("<table>");
            str.Append("<thead>");
            str.Append("<tr>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>LLOM Number</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Vectone Number</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Country</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Area</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Plan</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Start Date</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Subscription Date</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Duration(Inactive Days)</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Status</th>");
            str.Append("</tr>");
            str.Append("</thead>");
            str.Append("<tbody>");
            foreach (CRM_API.Models.lotgproductcityview_v2 val in lotgareanameids)
            {
                str.Append("<tr>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.LLOMNumber) ? "" : val.LLOMNumber) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.VectoneNumber) ? "" : val.VectoneNumber) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Country) ? "" : val.Country) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.area_name) ? "" : val.area_name) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Plan) ? "" : val.Plan) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;' class='xlText'>" + (string.IsNullOrEmpty(val.StartDate) ? "" : val.StartDate) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.SubscriptionDate) ? "" : val.SubscriptionDate) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Duration) ? "" : val.Duration) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.status) ? "" : val.status) + "</td>");
                str.Append("</tr>");
            }
            str.Append("</tbody>");
            str.Append("</table>");
            str.Append("</body></html>");


            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=NumberPoolDetailsByProduct" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(str.ToString());
            string _random = Guid.NewGuid().ToString();
            TempData[_random] = str.ToString();
            var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = 0, errmsg = "Download Ready", Text = _random };
            return Json(resy, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult DownloadsCreateTransaction(string paymentMode, string date)
        {
            IEnumerable<CRM_API.Models.financeviewtransdownload> objNewViewTransaction = null;


            //    //objNewViewTransaction = CRM_API.DB.viewtransaction.getpaymentdetails().ToArray();
            //objNewViewTransaction = CRM_API.DB.viewtransaction.getpaymentdetailsdownloads_New(Transation_Status, Rejection_Status, Product_Code, Customer_Name, payment_Ref, mobile_Number, EmailAddress, ICCID, startdate, enddate).ToArray();


            string Status = "";
            if (string.IsNullOrEmpty(paymentMode))
            {
                //objNewViewTransaction = CRM_API.DB.viewtransaction.getpaymentdetails().ToArray();
                objNewViewTransaction = CRM_API.DB.viewtransaction.getpaymentdetailsdownloads(date).ToArray();
                Status = "All View Transaction";
            }
            else if (paymentMode == "100")
            {
                objNewViewTransaction = CRM_API.DB.viewtransaction.getserrrCodedownload("get_latest_details_from_tpayment_by_errcode_sv6", paymentMode, date);
                Status = "All Sucess Transaction";
            }
            else if (paymentMode == "101")
            {
                objNewViewTransaction = CRM_API.DB.viewtransaction.getserrrCodedownload("get_latest_details_from_tpayment_by_errcode_sv7", paymentMode, date);
                Status = "All Reject Transaction";
            }
            else if (paymentMode == "200")
            {
                objNewViewTransaction = CRM_API.DB.viewtransaction.getserrrCodedownload("get_latest_details_from_tpayment_by_errcode_sv8", paymentMode, date);
                Status = "All Manually Accepted Transction";
            }
            else if (paymentMode == "201")
            {
                objNewViewTransaction = CRM_API.DB.viewtransaction.getserrrCodedownload("get_latest_details_from_tpayment_by_errcode_sv9", paymentMode, date);
                Status = "All Manually Reject Transction";
            }


            var result = objNewViewTransaction.ToList();
            StringBuilder str = new StringBuilder();
            str.Append("<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{Search Transactions}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv=\"content-type\" content=\"text/plain; charset=UTF-8\"/><style>.xlText{ mso-number-format:'@';}</style></head><body>");
            str.Append("<h1>Search Transactions </h1>");
            str.Append("<h3>View Transaction by Status : <span>" + Status + "</span></h3>");
            //str.Append("<h3>Transaction  Status : <span>" + Transation_Status + "</span></h3>");
            //str.Append("<h3>Period : " + Convert.ToDateTime(startdate).ToString("dd/MM/yyyy") + " - " + Convert.ToDateTime(enddate).ToString("dd/MM/yyyy") + "</h3>");
            str.Append("<table>");
            str.Append("<thead>");
            str.Append("<tr>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Reference ID</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Payment Agent</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Product Code</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Account ID</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>CC No</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Amount</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Currency</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Created Date</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Subscription ID</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Status</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>IpAddress</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Email</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Balance</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Code</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Reject Reason</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>ECI</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Days_from_1st_login</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Days_from_1st_topup</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>days_from_Lasttopup</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>total_spend</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>total_spend_consum</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>spend_last3months</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>spend_curr_month</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Noof_cardtrans</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Noof_voucher_trans</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Noof_failed_cardtrans</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Noof_success_cardtrans_30Days</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Noof_Fail_cardtrans_30Days</th>");
            //26-Aug-2019 : Moorthy added for retrieving SRD value
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Scheme Reference Data</th>");
            str.Append("</tr>");
            str.Append("</thead>");
            str.Append("<tbody>");



            foreach (CRM_API.Models.financeviewtransdownload val in result)
            {
                str.Append("<tr>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.REFERENCE_ID) ? "" : val.REFERENCE_ID) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.PAYMENT_AGENT) ? "" : val.PAYMENT_AGENT) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.PRODUCT_CODE) ? "" : val.PRODUCT_CODE) + "</td>");
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.ACCOUNT_ID) ? "" : val.ACCOUNT_ID) + "</td>");
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.CC_NO) ? "" : val.CC_NO) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.TOTALAMOUNT) ? "" : val.TOTALAMOUNT) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.CURRENCY) ? "" : val.CURRENCY) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.CREATED_DATE) ? "" : val.CREATED_DATE) + "</td>");
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.SUBSCRIPTIONID) ? "" : val.SUBSCRIPTIONID) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.CURRENT_STATUS) ? "" : val.CURRENT_STATUS) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.IP_Address) ? "" : val.IP_Address) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.email) ? "" : val.email) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Current_Balance) ? "" : val.Current_Balance) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.CS_ERROR_CODE) ? "" : val.CS_ERROR_CODE) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.ReasonforReject) ? "" : val.ReasonforReject) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.ECI_Value) ? "" : val.ECI_Value) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Days_from_1st_login) ? "" : val.Days_from_1st_login) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Days_from_1st_topup) ? "" : val.Days_from_1st_topup) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.days_from_Lasttopup) ? "" : val.days_from_Lasttopup) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.total_spend) ? "" : val.total_spend) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.total_spend_consum) ? "" : val.total_spend_consum) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.spend_last3months) ? "" : val.spend_last3months) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.spend_curr_month) ? "" : val.spend_curr_month) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Noof_cardtrans) ? "" : val.Noof_cardtrans) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Noof_voucher_trans) ? "" : val.Noof_voucher_trans) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Noof_failed_cardtrans) ? "" : val.Noof_failed_cardtrans) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Noof_success_cardtrans_30Days) ? "" : val.Noof_success_cardtrans_30Days) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Noof_Fail_cardtrans_30Days) ? "" : val.Noof_Fail_cardtrans_30Days) + "</td>");
                //26-Aug-2019 : Moorthy added for retrieving SRD value
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.srd) ? "" : val.srd) + "</td>");
                str.Append("</tr>");
            }
            str.Append("</tbody>");
            str.Append("</table>");
            str.Append("</body></html>");

            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=SearchTransactions_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            // HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=" + filename + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(str.ToString());
            string _random = Guid.NewGuid().ToString();
            TempData[_random] = str.ToString();

            var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = 0, errmsg = "SearchTransactions", Text = _random };
            return Json(resy, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DownloadSIMOrderList(CRM_API.Models.SIMOrderRequest listRequest, string CountryName)
        {
            var result = GetSimOrder_Downloadable(listRequest);

            StringBuilder str = new StringBuilder();
            str.Append("<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv=\"content-type\" content=\"text/plain; charset=UTF-8\"/><style>.xlText{ mso-number-format:'@';}</style></head><body>");
            str.Append("<h1>SIM Order List</h1>");
            str.Append("<h3>Country : <span>" + CountryName + "</span></h3>");
            if (string.IsNullOrEmpty(listRequest.searchby))
            {
                str.Append("<h3>Period : " + listRequest.fromdate.ToString() + " - " + listRequest.todate.ToString() + "</h3>");
            }
            str.Append("<table>");
            str.Append("<thead>");
            str.Append("<tr>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>FreeSimId</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Subscriber ID</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Title</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>First Name</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Last Name</th>");
            //11-Jan-2017: Dhakshinamoorthy : Modified for '!CRM - Special characters implementation' issue
            if (CountryName.ToLower() == "serbia")
            {
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Address</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>House No</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Post Code</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>City</th>");
            }
            else
            {
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>House No</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Address</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>City</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Post Code</th>");
            }
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Email</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Mobile No</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>ICCID</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Freesim Status</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Register Date</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Activate Date</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Sent Date</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Quantity</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Cybersource ID</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Source</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Source Address</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Ordersim URL</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>MSISDN Referrer</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Mail Reference</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Country you call most</th>");
            //11-Jan-2016 : Moorthy : Added new column IP Address
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>IP Address</th>");
            str.Append("</tr>");
            str.Append("</thead>");
            str.Append("<tbody>");
            foreach (CRM_API.Models.FreesimOrderItem val in result)
            {
                str.Append("<tr>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (val.freesimid != null ? val.freesimid : "") + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.subscriberid + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.title) ? "" : val.title) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.firstname) ? "" : val.firstname) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.lastname) ? "" : val.lastname) + "</td>");
                //11-Jan-2017: Dhakshinamoorthy : Modified for '!CRM - Special characters implementation' issue
                if (CountryName.ToLower() == "serbia")
                {
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Address) ? "" : val.Address) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;' class='xlText'>" + (string.IsNullOrEmpty(val.houseno) ? "" : val.houseno) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.postcode) ? "" : val.postcode) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.city) ? "" : val.city) + "</td>");
                }
                else
                {
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;' class='xlText'>" + (string.IsNullOrEmpty(val.houseno) ? "" : val.houseno) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Address) ? "" : val.Address) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.city) ? "" : val.city) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.postcode) ? "" : val.postcode) + "</td>");
                }
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.email) ? "" : val.email) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.mobilephone) ? "" : val.mobilephone) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.iccid) ? "" : val.iccid) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.freesimstatus) ? "" : val.freesimstatus) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;' class='xlText'>" + (string.IsNullOrEmpty(val.registerdate) ? "" : val.registerdate) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;' class='xlText'>" + (string.IsNullOrEmpty(val.activatedate) ? "" : val.activatedate) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;' class='xlText'>" + (string.IsNullOrEmpty(val.sentdate) ? "" : val.sentdate) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.Quantity + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.CyberSourceId) ? "" : val.CyberSourceId) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.SourceReg) ? "" : val.SourceReg) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.source_address) ? "" : val.source_address) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.ordersim_url) ? "" : val.ordersim_url) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.msisdn_referrer) ? "" : val.msisdn_referrer) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.royal_mail_reference) ? "" : val.royal_mail_reference) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.fav_call_country) ? "" : val.fav_call_country) + "</td>");
                //11-Jan-2016 : Moorthy : Added new column IP Address
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + Convert.ToString(val.ipaddress) + "</td>");
                str.Append("</tr>");
            }
            str.Append("</tbody>");
            str.Append("</table>");
            str.Append("</body></html>");

            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=Freesim_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(str.ToString());
            string _random = Guid.NewGuid().ToString();
            TempData[_random] = str.ToString();
            var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = 0, errmsg = "Download Ready", Text = _random };
            return Json(resy, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public FileContentResult DownloadsViewTransaction(string id, string filename)
        {
            var result = (string)TempData[id];
            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=ViewTransactions.xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(result.ToString());
            return new FileContentResult(temp, "application/vnd.ms-excel");
        }

        [HttpGet]
        public FileContentResult DownloadViewTransaction(string id, string filename)
        {
            var result = (string)TempData[id];
            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=ViewTransactions_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(result.ToString());
            return new FileContentResult(temp, "application/vnd.ms-excel");
        }

        [HttpGet]
        public FileContentResult DownloadFinanceTransaction(string id)
        {
            var result = (string)TempData[id];
            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=ViewTransactions_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(result.ToString());
            return new FileContentResult(temp, "application/vnd.ms-excel");
        }

        [HttpGet]
        public FileContentResult DownloadSearchTransaction(string id, string filename)
        {
            var result = (string)TempData[id];
            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=SearchTransactions_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(result.ToString());
            return new FileContentResult(temp, "application/vnd.ms-excel");
        }
        [HttpGet]
        public FileContentResult GoDownloadCDRHistory(string id, string historyName)
        {
            var result = (string)TempData[id];
            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=" + historyName.Replace(" ", "") + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(result.ToString());
            return new FileContentResult(temp, "application/vnd.ms-excel");
        }

        [HttpGet]
        public FileContentResult GoDownloadCountryDetails(string id)
        {
            var result = (string)TempData[id];
            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=NumberPoolDetailsByArea_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(result.ToString());
            return new FileContentResult(temp, "application/vnd.ms-excel");
        }
        [HttpGet]
        public FileContentResult DownloadViewTransactionMarketing_v2(string id, string filename)
        {
            var result = (string)TempData[id];
            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=" + Session["filename"] + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(result.ToString());
            return new FileContentResult(temp, "application/vnd.ms-excel");
        }
        [HttpGet]
        public FileContentResult GoDownloadProductDetails(string id)
        {
            var result = (string)TempData[id];
            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=NumberPoolDetailsByProduct" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(result.ToString());
            return new FileContentResult(temp, "application/vnd.ms-excel");
        }
        [HttpGet]
        public FileContentResult DownloadViewTransactionMarketing(string id, string filename)
        {
            var result = (string)TempData[id];
            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=" + Session["filename"] + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(result.ToString());
            return new FileContentResult(temp, "application/vnd.ms-excel");
        }
        [HttpGet]
        public FileContentResult GoDownloadSIMOrderList(string id)
        {
            var result = (string)TempData[id];
            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=SIMOrderList_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(result.ToString());
            return new FileContentResult(temp, "application/vnd.ms-excel");
        }

        //added by karthik Jira:CRMT-198
        private static void FormatDataAllHistory(dynamic result, ref StringBuilder str)
        {
            str.Append("<table>");
            str.Append("<thead>");
            str.Append("<tr>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Type</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Call Date and Time</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>CLI</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Destination Number</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Package Name</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Type</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Destination Code</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Duration(Min:sec)</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Data Usage(in MB)</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Tariff Class</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Romaing Zone</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Excess Credit Usage</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Net Charges</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Balance</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Within Bundle</th>");
            str.Append("</tr>");
            str.Append("</thead>");
            str.Append("<tbody>");
            foreach (CRM_API.Models.CDR.CallHistory_v1 val in (result as IEnumerable<CRM_API.Models.CDR.CallHistory_v1>))
            {
                str.Append("<tr>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.History_Type as string) ? "" : (val.History_Type.ToString())) + "</td>");
                //str.Append("<td class='xlLongDate' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.dateTime_String as string) ? "" : DateTime.ParseExact(val.dateTime_String, "dd-MM-yyyy hh:mm:ss tt", System.Globalization.CultureInfo.InvariantCulture).ToString("dd-MMM-yyyy hh:mm:ss tt")) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.dateTime_String as string) ? "" : (val.dateTime_String.ToString())) + "</td>");
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.CLI as string) ? "" : (val.CLI.ToString())) + "</td>");
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Destination_Number as string) ? "" : (val.Destination_Number.ToString())) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Package_Name as string) ? "" : val.Package_Name) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Type as string) ? "" : (val.Type.ToString())) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Destination_Code as string) ? "" : (val.Destination_Code.ToString())) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.DurationMin_string + " Min" as string) ? "" : val.DurationMin_string + " Min") + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.DataUsage_String + " MB" as string) ? "" : val.DataUsage_String + " MB") + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Tariff_Class as string) ? "" : val.Tariff_Class) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Roaming_Zone as string) ? "" : val.Roaming_Zone) + "</td>");
                if (val.Status == 1)
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + "" + "</td>");
                else
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Net_Charges) ? "" : val.Net_Charges) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Net_Charges as string) ? "" : val.Net_Charges) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Balance_String as string) ? "" : (val.Balance.ToString())) + "</td>");
                string status = "";
                if (val.Status == 1)
                {
                    status = "Yes";
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(status as string) ? "" : (status.ToString())) + "</td>");
                }
                else
                {
                    status = "No";
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(status as string) ? "" : (status.ToString())) + "</td>");
                }

                str.Append("</tr>");
            }
            str.Append("</tbody>");
            str.Append("</table>");
            str.Append("</body>");
        }

        //added by karthik Jira:CRMT-198
        private static void FormatDataCallHistory(dynamic result, ref StringBuilder str)
        {
            str.Append("<table>");
            str.Append("<thead>");
            str.Append("<tr>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Type</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Call Date and Time</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>CLI</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Destination Number</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Package Name</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Type</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Destination Code</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Duration(Min:sec)</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Data Usage(in MB)</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Tariff Class</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Romaing Zone</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Net Charges</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Balance</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Within Bundle</th>");
            str.Append("</tr>");
            str.Append("</thead>");
            str.Append("<tbody>");
            foreach (CRM_API.Models.CDR.CallHistory_v1 val in (result as IEnumerable<CRM_API.Models.CDR.CallHistory_v1>))
            {
                str.Append("<tr>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.History_Type as string) ? "" : (val.History_Type.ToString())) + "</td>");
                // str.Append("<td class='xlLongDate' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.dateTime_String as string) ? "" : DateTime.ParseExact(val.dateTime_String, "dd-MM-yyyy hh:mm:ss tt", System.Globalization.CultureInfo.InvariantCulture).ToString("dd-MMM-yyyy hh:mm:ss tt")) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.dateTime_String as string) ? "" : (val.dateTime_String.ToString())) + "</td>");
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.CLI as string) ? "" : (val.CLI.ToString())) + "</td>");
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Destination_Number as string) ? "" : (val.Destination_Number.ToString())) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Package_Name as string) ? "" : val.Package_Name) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Type as string) ? "" : (val.Type.ToString())) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Destination_Code as string) ? "" : (val.Destination_Code.ToString())) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.DurationMin_string + " Min" as string) ? "" : val.DurationMin_string + " Min") + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.DataUsage_String + " MB" as string) ? "" : val.DataUsage_String + " MB") + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Tariff_Class as string) ? "" : val.Tariff_Class) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Roaming_Zone as string) ? "" : val.Roaming_Zone) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Net_Charges as string) ? "" : val.Net_Charges) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Balance_String as string) ? "" : (val.Balance.ToString())) + "</td>");
                string status = "";
                if (val.Status == 1)
                {
                    status = "Yes";
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(status as string) ? "" : (status.ToString())) + "</td>");
                }
                else
                {
                    status = "No";
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(status as string) ? "" : (status.ToString())) + "</td>");
                }
                str.Append("</tr>");
            }
            str.Append("</tbody>");
            str.Append("</table>");
            str.Append("</body>");
        }

        //Added by karthik Jira:CRMT-198
        private static void FormatDataGPRSHistory(dynamic result, ref StringBuilder str)
        {
            str.Append("<table>");
            str.Append("<thead>");
            str.Append("<tr>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Type</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Call Date and Time</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>CLI</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Destination Number</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Package Name</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Type</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Destination Code</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Duration(Min:sec)</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Data Usage(in MB)</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Tariff Class</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Romaing Zone</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Net Charges</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Balance</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Within Bundle</th>");
            str.Append("</tr>");
            str.Append("</thead>");
            str.Append("<tbody>");
            foreach (CRM_API.Models.CDR.CallHistory_v1 val in (result as IEnumerable<CRM_API.Models.CDR.CallHistory_v1>))
            {
                str.Append("<tr>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.History_Type as string) ? "" : (val.History_Type.ToString())) + "</td>");
                //str.Append("<td class='xlLongDate' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.dateTime_String as string) ? "" : DateTime.ParseExact(val.dateTime_String, "dd-MM-yyyy hh:mm:ss tt", System.Globalization.CultureInfo.InvariantCulture).ToString("dd-MMM-yyyy hh:mm:ss tt")) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.dateTime_String as string) ? "" : (val.dateTime_String.ToString())) + "</td>");
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.CLI as string) ? "" : (val.CLI.ToString())) + "</td>");
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Destination_Number as string) ? "" : (val.Destination_Number.ToString())) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Package_Name as string) ? "" : val.Package_Name) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Type as string) ? "" : (val.Type.ToString())) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Destination_Code as string) ? "" : (val.Destination_Code.ToString())) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.DurationMin_string + " Min" as string) ? "" : val.DurationMin_string + " Min") + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.DataUsage_String + " MB" as string) ? "" : val.DataUsage_String + " MB") + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Tariff_Class as string) ? "" : val.Tariff_Class) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Roaming_Zone as string) ? "" : val.Roaming_Zone) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Net_Charges as string) ? "" : val.Net_Charges) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Balance_String as string) ? "" : (val.Balance.ToString())) + "</td>");
                string status = "";
                if (val.Status == 1)
                {
                    status = "Yes";
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(status as string) ? "" : (status.ToString())) + "</td>");
                }
                else
                {
                    status = "No";
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(status as string) ? "" : (status.ToString())) + "</td>");
                }
                str.Append("</tr>");
            }
            str.Append("</tbody>");
            str.Append("</table>");
            str.Append("</body>");
        }

        //Added by karthik Jira:CRMT-198
        private static void FormatDataSMSHistory(dynamic result, ref StringBuilder str)
        {
            str.Append("<table>");
            str.Append("<thead>");
            str.Append("<tr>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Type</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Call Date and Time</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>CLI</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Destination Number</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Package Name</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Type</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Destination Code</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Duration(Min:sec)</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Data Usage(in MB)</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Tariff Class</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Romaing Zone</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Net Charges</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Balance</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Within Bundle</th>");
            str.Append("</tr>");
            str.Append("</thead>");
            str.Append("<tbody>");
            foreach (CRM_API.Models.CDR.CallHistory_v1 val in (result as IEnumerable<CRM_API.Models.CDR.CallHistory_v1>))
            {
                str.Append("<tr>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.History_Type as string) ? "" : (val.History_Type.ToString())) + "</td>");
                // str.Append("<td class='xlLongDate' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.dateTime_String as string) ? "" : DateTime.ParseExact(val.dateTime_String, "dd-MM-yyyy hh:mm:ss tt", System.Globalization.CultureInfo.InvariantCulture).ToString("dd-MMM-yyyy hh:mm:ss tt")) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.dateTime_String as string) ? "" : (val.dateTime_String.ToString())) + "</td>");
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.CLI as string) ? "" : (val.CLI.ToString())) + "</td>");
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Destination_Number as string) ? "" : (val.Destination_Number.ToString())) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Package_Name as string) ? "" : val.Package_Name) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Type as string) ? "" : (val.Type.ToString())) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Destination_Code as string) ? "" : (val.Destination_Code.ToString())) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.DurationMin_string + " Min" as string) ? "" : val.DurationMin_string + " Min") + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.DataUsage_String + " MB" as string) ? "" : val.DataUsage_String + " MB") + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Tariff_Class as string) ? "" : val.Tariff_Class) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Roaming_Zone as string) ? "" : val.Roaming_Zone) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Net_Charges as string) ? "" : val.Net_Charges) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Balance_String as string) ? "" : (val.Balance.ToString())) + "</td>");
                string status = "";
                if (val.Status == 1)
                {
                    status = "Yes";
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(status as string) ? "" : (status.ToString())) + "</td>");
                }
                else
                {
                    status = "No";
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(status as string) ? "" : (status.ToString())) + "</td>");
                }
                str.Append("</tr>");
            }
            str.Append("</tbody>");
            str.Append("</table>");
            str.Append("</body>");
        }

        //added by karthik Jira:CRMT-232
        private static void FormatDataUsageHistoryDowload(dynamic result, ref StringBuilder str)
        {
            str.Append("<table>");
            str.Append("<thead>");
            str.Append("<tr>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Date</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Reason Of Spend</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Value</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Balance Before Usage</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Balance After Usage</th>");
            str.Append("</tr>");
            str.Append("</thead>");
            str.Append("<tbody>");
            foreach (CRM_API.Models.TotalUsageHistory.Customerpaymenthistory val in (result as IEnumerable<CRM_API.Models.TotalUsageHistory.Customerpaymenthistory>))
            {
                str.Append("<tr>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.paydate_String as string) ? "" : (val.paydate_String.ToString())) + "</td>");
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.type as string) ? "" : (val.type.ToString())) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.totalusage_String as string) ? "" : (val.totalusage_String.ToString())) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.beforebal_String as string) ? "" : (val.beforebal_String.ToString())) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.afterbal_String as string) ? "" : (val.afterbal_String.ToString())) + "</td>");
                str.Append("</tr>");
            }
            str.Append("</tbody>");
            str.Append("</table>");
            str.Append("</body>");
        }

        private dynamic GetCDRHistory_Downloadable(CRM_API.Models.CDR.CallHistorySearch In)
        {
            CRM_API.DB.Common.ErrCodeMsg errResult = new CRM_API.DB.Common.ErrCodeMsg()
            {
                errcode = -1,
                errsubject = "General Error",
                errmsg = "Empty New Order"
            };
            try
            {
                switch (In.HistoryType)
                {
                    case CRM_API.Models.CDR.CDRType.AllHistory:
                        return GetAllHistory(In);
                    case CRM_API.Models.CDR.CDRType.CallHistory:
                        return GetCallHistory(In);
                    case CRM_API.Models.CDR.CDRType.SMSHistory:
                        return GetSMSHistory(In);
                    case CRM_API.Models.CDR.CDRType.GPRSHistory:
                        return GetGPRSHistory(In);
                    //Added by karthik Jira:CRMT-198
                    case CRM_API.Models.CDR.CDRType.ExcessUsage:
                        return GetExcessUsage(In);
                    default: break;
                }
            }
            catch (Exception ex)
            {
                errResult.errmsg = "WEB-API: " + ex.Message;
                errResult.errcode = -1;
            }
            return errResult;
        }

        //CRMT-232 get date from Usage user history 
        private dynamic GetUsageHistory_Downloadable(CRM_API.Models.TotalUsageHistory.CustomerTotalUsageHistoryRequest In)
        {
            CRM_API.DB.Common.ErrCodeMsg errResult = new CRM_API.DB.Common.ErrCodeMsg()
            {
                errcode = -1,
                errsubject = "General Error",
                errmsg = "Empty New Order"
            };
            try
            {
                switch (In.type)
                {
                    case CRM_API.Models.TotalUsageHistory.Action.History:
                        return GetUsageHistory(In);
                    default: break;
                }
            }
            catch (Exception ex)
            {
                errResult.errmsg = "WEB-API: " + ex.Message;
                errResult.errcode = -1;
            }
            return errResult;
        }

        private IEnumerable<CRM_API.Models.CDR.CallHistory_v1> GetExcessUsage(CRM_API.Models.CDR.CallHistorySearch In)
        {
            IEnumerable<CRM_API.Models.CDR.CallHistory_v1> result =
                           CRM_API.DB.CDR.SProc_v1.AllHistory(In.mobileno, In.Date_From, In.Date_To, In.Sitecode, In.Packageid, 0).OrderByDescending(o => o.Date_Time);
            if (result == null || result.Count() == 0)
                return new List<CRM_API.Models.CDR.CallHistory_v1>().ToArray();
            else
            {
                return result.Where(r => r.Status == 0).ToArray();
            }
        }


        //Added by karthi Jira:CRMT-198 
        //All History data displaying 
        private IEnumerable<CRM_API.Models.CDR.CallHistory_v1> GetAllHistory(CRM_API.Models.CDR.CallHistorySearch In)
        {
            IEnumerable<CRM_API.Models.CDR.CallHistory_v1> result = CRM_API.DB.CDR.SProc_v1.AllHistory(In.mobileno, In.Date_From, In.Date_To, In.Sitecode, "", 0).OrderByDescending(o => o.Date_Time);
            if (result == null)
                return new List<CRM_API.Models.CDR.CallHistory_v1>().ToArray();
            else if (result.Count() == 0)
                return new List<CRM_API.Models.CDR.CallHistory_v1>().ToArray();
            else
                return result.ToArray();
        }

        //Added by karthi Jira:CRMT-198 
        //Call history data showing
        private IEnumerable<CRM_API.Models.CDR.CallHistory_v1> GetCallHistory(CRM_API.Models.CDR.CallHistorySearch In)
        {
            // string pad = string.Empty;
            IEnumerable<CRM_API.Models.CDR.CallHistory_v1> result = CRM_API.DB.CDR.SProc_v1.GetCallHistory(In.mobileno, In.Date_From, In.Date_To, In.Sitecode, "", 1).OrderByDescending(o => o.Date_Time);
            if (result == null)
                return new List<CRM_API.Models.CDR.CallHistory_v1>().ToArray();
            else if (result.Count() == 0)
                return new List<CRM_API.Models.CDR.CallHistory_v1>().ToArray();
            else
                return result.ToArray();
        }


        //Added by karthi Jira:CRMT-198 
        //Call SMS data showing
        private IEnumerable<CRM_API.Models.CDR.CallHistory_v1> GetSMSHistory(CRM_API.Models.CDR.CallHistorySearch In)
        {
            IEnumerable<CRM_API.Models.CDR.CallHistory_v1> result = CRM_API.DB.CDR.SProc_v1.GetSMSHistory(In.mobileno, In.Date_From, In.Date_To, In.Sitecode, "", 2).OrderByDescending(o => o.Date_Time);
            if (result == null)
                return new List<CRM_API.Models.CDR.CallHistory_v1>().ToArray();
            else if (result.Count() == 0)
                return new List<CRM_API.Models.CDR.CallHistory_v1>().ToArray();
            else
                return result.ToArray();
        }

        //Added by karthi Jira:CRMT-198 
        //Call SMS data showing
        private IEnumerable<CRM_API.Models.CDR.CallHistory_v1> GetGPRSHistory(CRM_API.Models.CDR.CallHistorySearch In)
        {
            IEnumerable<CRM_API.Models.CDR.CallHistory_v1> result = CRM_API.DB.CDR.SProc_v1.GetGPRSHistory(In.mobileno, In.Date_From, In.Date_To, In.Sitecode, "", 3).OrderByDescending(o => o.Date_Time);
            if (result == null)
                return new List<CRM_API.Models.CDR.CallHistory_v1>().ToArray();
            else if (result.Count() == 0)
                return new List<CRM_API.Models.CDR.CallHistory_v1>().ToArray();
            else
                return result.ToArray();
        }

        //CRMT-232 
        private IEnumerable<CRM_API.Models.TotalUsageHistory.Customerpaymenthistory> GetUsageHistory(CRM_API.Models.TotalUsageHistory.CustomerTotalUsageHistoryRequest In)
        {
            ///**** Single SP old logic  *****/
            //// string pad = string.Empty;
            //IEnumerable<CRM_API.Models.TotalUsageHistory.Customerpaymenthistory> result = CRM_API.DB.TotalUsageHistory.SProc.GettotalusagehistoryCall(In.Msisdn, In.Sitecode, In.date_from, In.date_to);
            //if (result == null)
            //    return new List<CRM_API.Models.TotalUsageHistory.Customerpaymenthistory>().ToArray();
            //else if (result.Count() == 0)
            //    return new List<CRM_API.Models.TotalUsageHistory.Customerpaymenthistory>().ToArray();
            //else
            //    return result.ToArray();

            ///**** Single SP old Logic *****/



            /***** New SP Multiple SP ****/
            // string pad = string.Empty;
            IEnumerable<CRM_API.Models.TotalUsageHistory.Customerpaymenthistory> result = (CRM_API.DB.TotalUsageHistory.SProc.GettotalusagehistoryCall(In.Msisdn, In.Sitecode, In.Date_From1, In.Date_To1)
                                                                                               .Concat(CRM_API.DB.TotalUsageHistory.SProc.GettotalusagehistorySMS(In.Msisdn, In.Sitecode, In.Date_From1, In.Date_To1))
                                                                                               .Concat(CRM_API.DB.TotalUsageHistory.SProc.GettotalusagehistoryData(In.Msisdn, In.Sitecode, In.Date_From1, In.Date_To1))
                                                                                               .Concat(CRM_API.DB.TotalUsageHistory.SProc.GettotalusagehistoryLLOM(In.Msisdn, In.Sitecode, In.Date_From1, In.Date_To1)).OrderByDescending(s => s.paydate));
            if (result == null)
                return new List<CRM_API.Models.TotalUsageHistory.Customerpaymenthistory>().ToArray();
            else if (result.Count() == 0)
                return new List<CRM_API.Models.TotalUsageHistory.Customerpaymenthistory>().ToArray();
            else
                return result.ToArray();
            /***** New SP Multiple SP ****/
        }

        private IEnumerable<CRM_API.Models.FreesimOrderItem> GetSimOrder_Downloadable(CRM_API.Models.SIMOrderRequest listRequest)
        {
            if (string.IsNullOrEmpty(listRequest.searchby))
            {
                var dateFrom = DateTime.Parse(listRequest.fromdate);
                var dateUntil = DateTime.Parse(listRequest.todate);
                var result = CRM_API.DB.SIMOrder.SProc.GetFreeSimForDownload(dateFrom.ToString("yyyy-MM-dd"), dateUntil.ToString("yyyy-MM-dd"), listRequest.status.ToLower(), listRequest.sitecode);
                var resy = CRM_API.DB.SIMOrder.SProc.Get2in1AllOrder(listRequest.status, dateFrom.ToString("yyyy-MM-dd"), dateUntil.ToString("yyyy-MM-dd"), listRequest.sitecode);
                return result.Union(resy);
            }
            else
            {
                switch (listRequest.searchby.ToLower())
                {
                    case "freesimid":
                        if (listRequest.searchvalue.Contains("M"))
                        {
                            listRequest.order_id = int.Parse(listRequest.searchvalue.Substring(3));
                            return CRM_API.DB.SIMOrder.SProc.Search2in1Order(listRequest);
                        }
                        else
                            return CRM_API.DB.SIMOrder.SProc.GetFreeSimForDownloadByFreesimid(listRequest.searchvalue, listRequest.sitecode);

                    case "fullname":
                        listRequest.full_name = listRequest.searchvalue;
                        var resi = CRM_API.DB.SIMOrder.SProc.GetFreeSimForDownloadByFullname(listRequest.searchvalue, listRequest.sitecode);
                        var resj = resi.Union(CRM_API.DB.SIMOrder.SProc.Search2in1Order(listRequest));
                        //return Request.CreateResponse(HttpStatusCode.OK, resi as IEnumerable<Models.FreesimOrderItem>);
                        return resj as IEnumerable<CRM_API.Models.FreesimOrderItem>;
                    default: throw new ArgumentNullException("Search Key Undifined");
                }
            }
        }

        /* eof SIM ORDER LIST */
        /* --------------------------------------------------------------------------------------------------------------- */
        /* --------------------------------------------------------------------------------------------------------------- */
        /* CDR HISTORY */

        /* eof CDR HISTORY */
        /* --------------------------------------------------------------------------------------------------------------- */
        /* PMBO For the DD-Operations */

        //Modified By BSK for Prev.Month Data download issue
        //Dowload PDF for PAYM - Bill Details
        [HttpGet]
        public ActionResult DownloadPAYMBillsDetails(string MobileNo, string year, string Month, string productCode)
        {
            System.Web.HttpContext.Current.Session["GlobalProduct"] = productCode;

            //Calculation for From date --period ex : 201403, To date --billmonth ex : 201404
            try
            {
                string fromdate = "";
                string todate = "";
                if (Month != "12")
                {
                    if (Convert.ToInt32(Month) > 1)
                    {
                        //if (Month.Length == 1) { fromdate = "0" + (Convert.ToInt32(Month) - 1).ToString(); }
                        //else { fromdate = (Convert.ToInt32(Month) - 1).ToString(); }
                        //fromdate = year + fromdate;
                        //if (Month.Length == 1) { todate = "0" + (Convert.ToInt32(Month) - 1).ToString(); }
                        //else { todate = (Convert.ToInt32(Month) - 1).ToString(); }
                        //todate = year + todate;

                        if (Month.Length == 1) { fromdate = "0" + (Convert.ToInt32(Month)).ToString(); }
                        else { fromdate = (Convert.ToInt32(Month)).ToString(); }
                        fromdate = year + fromdate;
                        if (Month.Length == 1) { todate = "0" + (Convert.ToInt32(Month)).ToString(); }
                        else { todate = (Convert.ToInt32(Month)).ToString(); }
                        todate = year + todate;

                    }
                    else if (Convert.ToInt32(Month) == 1)
                    {
                        todate = (Convert.ToInt32(year) - 1).ToString() + "12";
                        fromdate = Convert.ToInt32(year).ToString() + "01";
                    }
                }
                else
                {
                    fromdate = year + "12";
                    todate = year + "12";
                }
                Stimulsoft.Report.StiReport report = new Stimulsoft.Report.StiReport();
                report.Load(Server.MapPath(string.Format("~/Vectone Bill_004_{0}.mrt", Convert.ToString(HttpContext.Session["GlobalProduct"]))));
                report["@mobileno"] = MobileNo.ToString();
                report["@period"] = fromdate.ToString();
                report["@billmonth"] = todate.ToString();
                report.Render();
                if (Month.Length > 1) { }
                else { Month = "0" + Month; }
                string fileName = "PAYM_" + MobileNo + "_" + Month + "_" + year + ".pdf";
                if (System.IO.File.Exists(Server.MapPath("~/Download/" + fileName)))
                {
                    System.IO.File.Delete(Server.MapPath("~/Download/" + fileName));
                }
                StiPdfExportSettings objPDFExport = new StiPdfExportSettings();
                report.ExportDocument(StiExportFormat.Pdf, Server.MapPath("~/Download/" + fileName), objPDFExport);
                var resy = new CRM_API.Models.Common.ErrCodeMsgExt { err_code = 0, err_message = fileName, Text = null };
                return Json(resy, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var resy = new CRM_API.Models.Common.ErrCodeMsgExt { err_code = 0, err_message = ex.Message, Text = null };
                return Json(resy, JsonRequestBehavior.AllowGet);
            }
        }

        //This is to prepare the EXCEL SHEET
        [HttpGet]
        public ActionResult DDViewForProcessFailure(string ddYear, string ddMonth, string ddStatus, int? ddCustID, string ddPaymtStatus, string ddMobileNo, string ddConnStatus, string ddPaymentRef, string ddPayMode, string ALL)
        {
            var result = new List<CRM_API.Models.DDViewNProcessFailure>();
            if (ALL == "Y")
            {
                ddYear = (ddYear == "") ? null : ddYear;
                ddMonth = (ddMonth == "") ? null : ddMonth;
                ddStatus = (ddStatus == "") ? null : ddStatus;
                ddPaymtStatus = (ddPaymtStatus == "") ? null : ddPaymtStatus;
                ddMobileNo = (ddMobileNo == "") ? null : ddMobileNo;
                ddConnStatus = (ddConnStatus == "") ? null : ddConnStatus;
                ddPaymentRef = (ddPaymentRef == "") ? null : ddPaymentRef;
                ddPayMode = (ddPayMode == "") ? null : ddPayMode;

                result = CRM_API.DB.PMBODDOperations.GetDataForViewNProcessFailure(ddYear, ddMonth, ddStatus, ddCustID, ddPaymtStatus, ddMobileNo, ddConnStatus, ddPaymentRef, ddPayMode, "");
            }
            else
            {
                result = CRM_API.DB.PMBODDOperations.GetDataForViewNProcessFailureForDownload(ddYear, ddMonth, ddStatus, ddCustID.ToString(), ddPaymtStatus, ddMobileNo, ddConnStatus, ddPaymentRef, ddPayMode);
            }
            List<CRM_API.Models.DDViewNProcessFailure> lstRes = result.ToList();
            StringBuilder str = new StringBuilder();
            str.Append("<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv=\"content-type\" content=\"text/plain; charset=UTF-8\"/><style>.xlText{ mso-number-format:'@';}</style></head><body>");
            str.Append("<h1>View and Process Failure </h1>");

            if (lstRes.Count > 0)
            {
                str.Append("<table>");
                str.Append("<thead>");
                str.Append("<tr>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Customer_ID</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Subscription_Id</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>MobileNo</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>DD_Status</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Amount</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>pay_reference</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>PaymentStatus</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>CustomerName</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Connection_status</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Payment_Attempts</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Processing_Date</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Payment_Mode</th>");
                str.Append("</tr>");
                str.Append("</thead>");
                str.Append("<tbody>");
                foreach (CRM_API.Models.DDViewNProcessFailure val in lstRes)
                {
                    str.Append("<tr>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (val.Customer_ID != null ? val.Customer_ID : "") + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (val.Subscription_Id != null ? val.Subscription_Id : "") + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>'" + (val.MobileNo != null ? val.MobileNo : "") + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (val.DD_Status != null ? val.DD_Status : "") + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (val.Amount != null ? val.Amount : "") + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (val.pay_reference != null ? val.pay_reference : "") + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (val.PaymentStatus != null ? val.PaymentStatus : "") + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (val.CustomerName != null ? val.CustomerName : "") + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (val.Connection_status != null ? val.Connection_status : "") + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (val.Payment_Attempts != null ? val.Payment_Attempts : "") + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (val.Processing_Date != null ? val.Processing_Date : "") + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (val.Payment_Mode != null ? val.Payment_Mode : "") + "</td>");
                    //                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.royal_mail_reference) ? "" : val.royal_mail_reference) + "</td>");
                    str.Append("</tr>");
                }
                str.Append("</tbody>");
                str.Append("</table>");
            }
            else
            {
                str.Append("<h1>No data To be exported</h1>");
            }
            str.Append("</body></html>");

            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=VPF_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(str.ToString());
            string _random = Guid.NewGuid().ToString();
            TempData[_random] = str.ToString();
            var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = 0, errmsg = "Download Ready", Text = _random };


            //return File(temp, "application/vnd.ms-excel");

            return Json(resy, JsonRequestBehavior.AllowGet); //This function keeps the sheet ready in WEBSERVER cache,  GoDownLoadVPFData will be called from the AJAX
            //to get the download immediately

        }

        //This is the Actual function that will download the file to the client 
        [HttpGet]
        public FileContentResult GoDownLoadVPFData(string id)
        {
            var result = (string)TempData[id];
            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=VPF_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(result.ToString());
            return new FileContentResult(temp, "application/vnd.ms-excel");
        }

        /*Added on 10 Feb 2015 - For Download SMS Campaign List*/
        [HttpPost]
        public ActionResult DownloadSmsCampaignSMSMarketing(String mobileNo, String siteCode)
        {
            try
            {
                IEnumerable<CRM_API.Models.SMSMarketingHistory> resValues = CRM_API.DB.SMSCampaign.SProc.GetSMSMarketingHistory(mobileNo, siteCode);
                StringBuilder str = new StringBuilder();
                str.Append("<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv=\"content-type\" content=\"text/plain; charset=UTF-8\"/></head><body>");
                str.Append("<table>");
                str.Append("<thead>");
                str.Append("<tr>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Action</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Date</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Mode</th>");
                str.Append("</tr>");
                str.Append("</thead>");
                str.Append("<tbody>");
                foreach (CRM_API.Models.SMSMarketingHistory val in resValues)
                {
                    str.Append("<tr>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>" + (string.IsNullOrEmpty(val.Action as string) ? "" : (val.Action as string)) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>" + val.Date + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>" + (string.IsNullOrEmpty(val.Mode) ? "" : val.Mode) + "</td>");
                    str.Append("</tr>");
                }
                str.Append("</tbody>");
                str.Append("</table>");
                str.Append("</body></html>");
                HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=VPF_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
                this.Response.ContentType = "application/vnd.ms-excel";
                byte[] temp = System.Text.Encoding.UTF8.GetBytes(str.ToString());
                string _random = Guid.NewGuid().ToString();
                TempData[_random] = str.ToString();
                var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = 0, errmsg = "Download Ready", Text = _random };


                //return File(temp, "application/vnd.ms-excel");

                return Json(resy, JsonRequestBehavior.AllowGet); //This function keeps the sheet ready in WEBSERVER cache,  GoDownLoadVPFData will be called from the AJAX
                //to get the download immediately
            }
            catch (Exception ex)
            {
                var resy = new CRM_API.Models.Common.ErrCodeMsgExt { err_code = -1, err_message = ex.Message, Text = null };
                return Json(resy, JsonRequestBehavior.AllowGet);
            }

            //if (resValues == null)
            //    return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.PAYMPlan>().ToArray());
            //else
            //    return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }


        [HttpPost]
        public ActionResult DownloadSmsCampaignEmailMarketing(String MobileNo, String Sitecode)
        {
            try
            {
                IEnumerable<CRM_API.Models.SMSMarketingHistory> resValues = CRM_API.DB.SMSCampaign.SProc.GetSMSMarketingHistory(MobileNo, Sitecode);
                StringBuilder str = new StringBuilder();
                str.Append("<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv=\"content-type\" content=\"text/plain; charset=UTF-8\"/></head><body>");
                str.Append("<table>");
                str.Append("<thead>");
                str.Append("<tr>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Action</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Date</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Mode</th>");
                str.Append("</tr>");
                str.Append("</thead>");
                str.Append("<tbody>");
                foreach (CRM_API.Models.SMSMarketingHistory val in resValues)
                {
                    str.Append("<tr>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>" + (string.IsNullOrEmpty(val.Action as string) ? "" : (val.Action as string)) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>" + val.Date + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>" + (string.IsNullOrEmpty(val.Mode) ? "" : val.Mode) + "</td>");
                    str.Append("</tr>");
                }
                str.Append("</tbody>");
                str.Append("</table>");
                str.Append("</body></html>");
                HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=VPF_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
                this.Response.ContentType = "application/vnd.ms-excel";
                byte[] temp = System.Text.Encoding.UTF8.GetBytes(str.ToString());
                string _random = Guid.NewGuid().ToString();
                TempData[_random] = str.ToString();
                var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = 0, errmsg = "Download Ready", Text = _random };

                return Json(resy, JsonRequestBehavior.AllowGet); //This function keeps the sheet ready in WEBSERVER cache,  GoDownLoadVPFData will be called from the AJAX
                //to get the download immediately
            }
            catch (Exception ex)
            {
                var resy = new CRM_API.Models.Common.ErrCodeMsgExt { err_code = -1, err_message = ex.Message, Text = null };
                return Json(resy, JsonRequestBehavior.AllowGet);
            }

            //if (resValues == null)
            //    return Request.CreateResponse(HttpStatusCode.NotFound, new List<CRM_API.Models.PAYMPlan>().ToArray());
            //else
            //    return Request.CreateResponse(HttpStatusCode.OK, resValues.ToArray());
        }

        //This is the Actual function that will download the file to the client 
        [HttpGet]
        public FileContentResult GoDownLoadSmsCampaignSMSMarketing(string id)
        {
            var result = (string)TempData[id];
            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=VPF_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(result.ToString());
            return new FileContentResult(temp, "application/vnd.ms-excel");
        }
        /*End added*/

        //This is the Actual function that will download the file to the client 
        [HttpGet]
        public FileContentResult GoDownLoadSmsCampaignEmailMarketing(string id)
        {
            var result = (string)TempData[id];
            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=VPF_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(result.ToString());
            return new FileContentResult(temp, "application/vnd.ms-excel");
        }
        /*End added*/

        [HttpGet]
        public ActionResult DownloadGocardReport(string Mode, string SiteCode)
        {
            IEnumerable<CRM_API.Models.DDgoSubmit> objNewViewTransaction = null;
            string status = "";
            if (Mode == "1")
            {
                status = "pending_submission";
            }
            else if (Mode == "2")
            {
                status = "paid_out";
            }
            else if (Mode == "3")
            {
                status = "Failed";
            }

            objNewViewTransaction = CRM_API.DB.gettransaction.GocardDownload(Mode, SiteCode);
            var result = objNewViewTransaction.ToList();
            StringBuilder str = new StringBuilder();
            str.Append("<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{Sim Order}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv=\"content-type\" content=\"text/plain; charset=UTF-8\"/><style>.xlText{ mso-number-format:'@';}</style></head><body>");
            str.Append("<h1> Direct Debit Account " + status + " Detail Report</h1>");
            str.Append("<h3>Downloaded Date : <span>" + DateTime.Now + "</span></h3>");
            str.Append("<table>");
            str.Append("<thead>");
            str.Append("<tr>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Customer ID</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Subscriber ID</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Name</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>E-mail</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Ac Holder Name</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Reference ID</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Charged On</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Amount</th>");
            //str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Currency</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Mandate ID</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Status </th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Period</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Account Status</th>");
            //str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Bank Name</th>");
            //str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Customer Reference</th>");
            str.Append("</tr>");
            str.Append("</thead>");
            str.Append("<tbody>");
            foreach (CRM_API.Models.DDgoSubmit val in result)
            {
                string date = Convert.ToString(val.charge_date);
                string amount = Convert.ToString(val.amount);
                str.Append("<tr>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.pp_customer_id) ? "" : val.pp_customer_id) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.subscriberid) ? "" : val.subscriberid) + "</td>");

                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Name) ? "" : val.Name) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.email) ? "" : val.email) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.AcHldrName) ? "" : val.AcHldrName) + "</td>");

                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.reference_id) ? "" : val.reference_id) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(date) ? "" : date) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(amount) ? "" : amount) + "</td>");
                //str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.currency) ? "" : val.currency) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.mandate_id) ? "" : val.mandate_id) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.status) ? "" : val.status) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.cust_reference) ? "" : val.cust_reference) + "</td>");

                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.AccountStatus) ? "" : val.AccountStatus) + "</td>");
                //str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.BankName) ? "" : val.BankName) + "</td>");
                //str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.cust_metadata_reference) ? "" : val.cust_metadata_reference) + "</td>");

                str.Append("</tr>");
            }
            str.Append("</tbody>");
            str.Append("</table>");
            str.Append("</body></html>");



            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=goCardLess_" + status + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(str.ToString());
            string _random = Guid.NewGuid().ToString();
            string _name = "goCardLess_" + status;
            TempData[_random] = str.ToString();
            Session["filename"] = "goCardLess_" + status;
            var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = 0, errmsg = "Download Ready", Text = _random };
            return Json(resy, JsonRequestBehavior.AllowGet);
        }

        //13-Jan-2017 : Moorthy Added for ID File Download Func
        [HttpGet]
        public ActionResult DownloadIDProof(string filename)
        {
            try
            {
                string remoteUri = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["DOWNLOADIDPROOF"]);
                WebClient myWebClient = new WebClient();
                byte[] myDataBuffer = myWebClient.DownloadData(remoteUri + filename);
                HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
                string contenttype = myWebClient.ResponseHeaders["Content-Type"];
                this.Response.ContentType = contenttype;
                string _random = Guid.NewGuid().ToString();
                TempData[_random] = myDataBuffer;
                var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = 0, errmsg = contenttype, Text = _random, data_type = Path.GetExtension(filename), };
                return Json(resy, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new CRM_API.Models.Common.ErrCodeMsgExt() { errcode = -1, errmsg = ex.Message, Text = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        //13-Jan-2017 : Moorthy Added for ID File Download Func
        [HttpGet]
        public FileContentResult GoDownloadIDProof(string id, string ext, string type)
        {
            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=IDProof_" + DateTime.Now.ToString("yyyyMMdd") + ext);
            this.Response.ContentType = type;
            byte[] temp = (byte[])TempData[id];
            return new FileContentResult(temp, type);
        }

        [HttpGet]
        public ActionResult DownloadBreakageReport(string mobileno, string sitecode, string date_fr, string date_to)
        {
            try
            {
                Log.Info("DownloadBreakageReport");
                BreakageUsageInput Model = new BreakageUsageInput();
                Model.mobileno = mobileno;
                Model.sitecode = sitecode;
                Model.date_fr = date_fr;
                Model.date_to = date_to;
                List<BreakageUsageOutput> resultValues = BreakageUsage.GetBreakageUsageInfo(Model).ToList();

                StringBuilder str = new StringBuilder();
                str.Append("<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{Breakage Report}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv=\"content-type\" content=\"text/plain; charset=UTF-8\"/><style>.xlText{ mso-number-format:'@';}</style></head><body>");
                str.Append("<h3>Breakage Report</h3>");
                str.Append("<table>");
                str.Append("<thead>");
                str.Append("<tr>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Date</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Mobile no</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Package</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>GPRS Usage MB</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Package ID</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Initial Balance MB</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Assigned Balance MB</th>");
                str.Append("</tr>");
                str.Append("</thead>");
                str.Append("<tbody>");

                foreach (BreakageUsageOutput val in resultValues)
                {
                    str.Append("<tr>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.logdate) ? "" : val.logdate) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.mobileno) ? "" : val.mobileno) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.package) ? "" : val.package) + "</td>");
                    str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.GPRS_CHARGE ?? 0 + "</td>");
                    str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.PACKAGE_ID ?? 0 + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.INITIAL_BALANCE_MB ?? 0 + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.ASSIGNED_BALANCE_MB ?? 0 + "</td>");
                    str.Append("</tr>");
                }
                str.Append("</tbody>");
                str.Append("</table>");
                str.Append("</body></html>");

                HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=BreakageReport_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
                this.Response.ContentType = "application/vnd.ms-excel";
                byte[] temp = System.Text.Encoding.UTF8.GetBytes(str.ToString());
                string _random = Guid.NewGuid().ToString();
                TempData[_random] = str.ToString();

                var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = 0, errmsg = "Download Ready", Text = _random };
                return Json(resy, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = -1, errmsg = ex.Message };
                return Json(resy, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public FileContentResult DownloadFinanceBreakageReport(string id)
        {
            var result = (string)TempData[id];
            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=BreakageReport_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(result.ToString());
            return new FileContentResult(temp, "application/vnd.ms-excel");
        }

        //AddBalanceReport
        [HttpGet]
        public ActionResult DownloadAddBalanceReport(string calledby, string sitecode, string date_fr, string date_to)
        {
            try
            {
                Log.Info("DownloadAddBalanceReport");
                AddBalanceReportInput Model = new AddBalanceReportInput();
                Model.sitecode = sitecode;
                Model.calledby = calledby;
                Model.date_from = date_fr;
                Model.date_to = date_to;
                List<AddBalanceReportOutput> resultValues = AddBalance.GetAddBalanceReport(Model).ToList();

                StringBuilder str = new StringBuilder();
                str.Append("<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{Add Balance Report}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv=\"content-type\" content=\"text/plain; charset=UTF-8\"/><style>.xlText{ mso-number-format:'@';}</style></head><body>");
                str.Append("<h3>Add Balance Report</h3>");
                str.Append("<table>");
                str.Append("<thead>");
                str.Append("<tr>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Mobile no</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Reference ID</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Username</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Amount</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Prev Balance</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>After Balance</th>");
                str.Append("</tr>");
                str.Append("</thead>");
                str.Append("<tbody>");

                foreach (AddBalanceReportOutput val in resultValues)
                {
                    str.Append("<tr>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.mobileno) ? "" : val.mobileno) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.referenceID) ? "" : val.referenceID) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.username) ? "" : val.username) + "</td>");
                    str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.amount ?? 0 + "</td>");
                    str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.prevbalance ?? 0 + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.afterbalance ?? 0 + "</td>");
                    str.Append("</tr>");
                }
                str.Append("</tbody>");
                str.Append("</table>");
                str.Append("</body></html>");

                HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=AddBalanceReport_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
                this.Response.ContentType = "application/vnd.ms-excel";
                byte[] temp = System.Text.Encoding.UTF8.GetBytes(str.ToString());
                string _random = Guid.NewGuid().ToString();
                TempData[_random] = str.ToString();

                var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = 0, errmsg = "Download Ready", Text = _random };
                return Json(resy, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = -1, errmsg = ex.Message };
                return Json(resy, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public FileContentResult DownloadFinanceAddBalanceReport(string id)
        {
            var result = (string)TempData[id];
            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=AddBalanceReport_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(result.ToString());
            return new FileContentResult(temp, "application/vnd.ms-excel");
        }

        //CDR History Download
        [HttpGet]
        public ActionResult DownloadCDRHistory(string mobileno, string siteCode, DateTime startDate, DateTime endDate, string packageId, int historyType)
        {
            try
            {
                Log.Info("DownloadCDRHistory");
                IEnumerable<CRM_API.Models.CDR.CallHistory_v1> resultValues = null;
                switch (historyType)
                {
                    case 1:
                        resultValues = CRM_API.DB.CDR.SProc_v1.GetCallHistory(mobileno, startDate, endDate, siteCode, packageId, 1).OrderByDescending(o => o.Date_Time);
                        break;
                    case 2:
                        resultValues = CRM_API.DB.CDR.SProc_v1.GetSMSHistory(mobileno, startDate, endDate, siteCode, packageId, 2).OrderByDescending(o => o.Date_Time);//-->2
                        break;
                    case 3:
                        resultValues = CRM_API.DB.CDR.SProc_v1.GetGPRSHistory(mobileno, startDate, endDate, siteCode, packageId, 3).OrderByDescending(o => o.Date_Time);//-->3
                        break;
                    case 0:
                        resultValues = CRM_API.DB.CDR.SProc_v1.AllHistory(mobileno, startDate, endDate, siteCode, packageId, 0).OrderByDescending(o => o.Date_Time);//-->0
                        break;
                    case 5:
                        resultValues = CRM_API.DB.CDR.SProc_v1.CallandSMSHistory(mobileno, startDate, endDate, siteCode, packageId, 5).OrderByDescending(o => o.Date_Time);//-->5
                        break;
                    default: break;
                }


                StringBuilder str = new StringBuilder();
                str.Append("<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{CDR History}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv=\"content-type\" content=\"text/plain; charset=UTF-8\"/><style>.xlText{ mso-number-format:'@';}</style></head><body>");
                str.Append("<h3>CDR History</h3>");
                str.Append("<table>");
                str.Append("<thead>");
                str.Append("<tr>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>History Type</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Call & Time</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>CLI</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Destination Number</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Type</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Destination Code</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Duration (Min:sec)</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Data Usage(in MB)</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Tariff Class</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Roaming Zone</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Net Charges</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Balance</th>");
                str.Append("</tr>");
                str.Append("</thead>");
                str.Append("<tbody>");

                foreach (CRM_API.Models.CDR.CallHistory_v1 val in resultValues)
                {
                    str.Append("<tr>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.History_Type) ? "" : val.History_Type) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.dateTime_String) ? "" : val.dateTime_String) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.CLI) ? "" : val.CLI) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Destination_Number) ? "" : val.Destination_Number) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Type) ? "" : val.Type) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Destination_Code) ? "" : val.Destination_Code) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.DurationMin_string) ? "" : val.DurationMin_string + " Min") + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.DataUsage_String) ? "" : val.DataUsage_String + " MB") + "</td>");

                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Tariff_Class) ? "" : val.Tariff_Class) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Roaming_Zone) ? "" : val.Roaming_Zone) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Net_Charges) ? "" : val.Net_Charges) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Balance_String) ? "" : val.Balance_String) + "</td>");
                    str.Append("</tr>");
                }
                str.Append("</tbody>");
                str.Append("</table>");
                str.Append("</body></html>");

                HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=CDRHistory_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
                this.Response.ContentType = "application/vnd.ms-excel";
                byte[] temp = System.Text.Encoding.UTF8.GetBytes(str.ToString());
                string _random = Guid.NewGuid().ToString();
                TempData[_random] = str.ToString();

                var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = 0, errmsg = "Download Ready", Text = _random };
                return Json(resy, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = -1, errmsg = ex.Message };
                return Json(resy, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public FileContentResult DownloadCDRHistoryFile(string id)
        {
            var result = (string)TempData[id];
            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=CDRHistory_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(result.ToString());
            return new FileContentResult(temp, "application/vnd.ms-excel");
        }

        //CancelTopupReport
        [HttpGet]
        public ActionResult DownloadCancelTopupReport(string sitecode, string date_fr, string date_to)
        {
            try
            {
                Log.Info("DownloadCancelTopupReport");
                CancelTopupInput Model = new CancelTopupInput();
                Model.sitecode = sitecode;
                Model.date_from = date_fr;
                Model.date_to = date_to;
                List<CancelTopupReportOutput> resultValues = CancelTopup.GetCancelTopupReport(Model).ToList();

                StringBuilder str = new StringBuilder();
                str.Append("<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{Cancel Topup Report}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv=\"content-type\" content=\"text/plain; charset=UTF-8\"/><style>.xlText{ mso-number-format:'@';}</style></head><body>");
                str.Append("<h3>Cancel Topup Report</h3>");
                str.Append("<table>");
                str.Append("<thead>");
                str.Append("<tr>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Request Date</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Mobile no</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Reference ID</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Amount</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Prev Balance</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>After Balance</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Reason</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Request By</th>");
                str.Append("</tr>");
                str.Append("</thead>");
                str.Append("<tbody>");

                foreach (CancelTopupReportOutput val in resultValues)
                {
                    str.Append("<tr>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.REQUEST_DATE_String) ? "" : val.REQUEST_DATE_String) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.MOBILENO) ? "" : val.MOBILENO) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.PAYMENT_REF) ? "" : val.PAYMENT_REF) + "</td>");
                    str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.AMOUNT ?? 0 + "</td>");
                    str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.PREV_BALANCE ?? 0 + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.AFTER_BALANCE ?? 0 + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.REASON) ? "" : val.REASON) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.REQUEST_BY) ? "" : val.REQUEST_BY) + "</td>");
                    str.Append("</tr>");
                }
                str.Append("</tbody>");
                str.Append("</table>");
                str.Append("</body></html>");

                HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=CancelTopupReport_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
                this.Response.ContentType = "application/vnd.ms-excel";
                byte[] temp = System.Text.Encoding.UTF8.GetBytes(str.ToString());
                string _random = Guid.NewGuid().ToString();
                TempData[_random] = str.ToString();

                var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = 0, errmsg = "Download Ready", Text = _random };
                return Json(resy, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = -1, errmsg = ex.Message };
                return Json(resy, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public FileContentResult DownloadFinanceCancelTopupReport(string id)
        {
            var result = (string)TempData[id];
            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=CancelTopupReport_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(result.ToString());
            return new FileContentResult(temp, "application/vnd.ms-excel");
        }

        //TransferBalanceReport
        [HttpGet]
        public ActionResult DownloadTransferBalanceReport(string calledby, string sitecode, string date_fr, string date_to)
        {
            try
            {
                Log.Info("DownloadTransferBalanceReport");
                TransferBalanceReportInput Model = new TransferBalanceReportInput();
                Model.sitecode = sitecode;
                Model.calledby = calledby;
                Model.date_from = date_fr;
                Model.date_to = date_to;
                List<TransferBalanceReportOutput> resultValues = TransferBalance.GetTransferBalanceReport(Model).ToList();

                StringBuilder str = new StringBuilder();
                str.Append("<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{Transfer Balance Report}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv=\"content-type\" content=\"text/plain; charset=UTF-8\"/><style>.xlText{ mso-number-format:'@';}</style></head><body>");
                str.Append("<h3>Transfer Balance Report</h3>");
                str.Append("<table>");
                str.Append("<thead>");
                str.Append("<tr>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Reference ID</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Debit Date</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Donor Mobile No</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Debit Amount</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Credit Date</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Receiver Mobile No</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Credit Amount</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Process By</th>");
                str.Append("</tr>");
                str.Append("</thead>");
                str.Append("<tbody>");

                foreach (TransferBalanceReportOutput val in resultValues)
                {
                    str.Append("<tr>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.referenceid) ? "" : val.referenceid) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.debit_date_string) ? "" : val.debit_date_string) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.donor_mobileno) ? "" : val.donor_mobileno) + "</td>");
                    str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.debit_amount ?? 0 + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.credit_date_string) ? "" : val.credit_date_string) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.receiver_mobileno) ? "" : val.receiver_mobileno) + "</td>");
                    str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.credit_amount ?? 0 + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.process_by) ? "" : val.process_by) + "</td>");
                    str.Append("</tr>");
                }
                str.Append("</tbody>");
                str.Append("</table>");
                str.Append("</body></html>");

                HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=TransferBalanceReport_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
                this.Response.ContentType = "application/vnd.ms-excel";
                byte[] temp = System.Text.Encoding.UTF8.GetBytes(str.ToString());
                string _random = Guid.NewGuid().ToString();
                TempData[_random] = str.ToString();

                var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = 0, errmsg = "Download Ready", Text = _random };
                return Json(resy, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = -1, errmsg = ex.Message };
                return Json(resy, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public FileContentResult DownloadFinanceTransferBalanceReport(string id)
        {
            var result = (string)TempData[id];
            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=TransferBalanceReport_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(result.ToString());
            return new FileContentResult(temp, "application/vnd.ms-excel");
        }

        //28-Dec-2018 : Moorthy : Added for Bundle breakdown
        [HttpPost]
        public ActionResult DownloadBundleBreakdown(CRM_API.Models.TotalUsageHistory.CustomerTotalUsageHistoryRequest In)
        {
            var result = GetBundleBreakdown_Downloadable(In);
            var HistoryName = "";
            StringBuilder str = new StringBuilder();
            str.Append("<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv=\"content-type\" content=\"text/plain; charset=UTF-8\"/></head><body>");
            str.Append("<head><style>.xlLongDate{mso-number-format:'dd-mm-yyyy hh:mm:ss AM/PM';}.xlText{mso-number-format:'@';}</style></head>");

            var dateFrom = In.Date_from_day_string + "/" + In.Date_from_month_string + "/" + In.Date_from_year_string;
            var dateTo = In.Date_To_day_string + "/" + In.Date_to_month_string + "/" + In.Date_To_year_string;

            HistoryName = "BundleBreakdown";
            str.Append("<h1>Bundle Breakdown</h1>");
            str.Append("<h3>Period : " + dateFrom + " - " + dateTo + "</h3>");
            FormatDataBundleBreakdownDowload(result, ref str);
            str.Append("</html>");

            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=" + HistoryName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(str.ToString());
            string _random = Guid.NewGuid().ToString();
            TempData[_random] = str.ToString();
            var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = 0, errmsg = HistoryName, Text = _random, };
            return Json(resy, JsonRequestBehavior.AllowGet);
        }

        //28-Dec-2018 : Moorthy : Added for Bundle breakdown
        private dynamic GetBundleBreakdown_Downloadable(CRM_API.Models.TotalUsageHistory.CustomerTotalUsageHistoryRequest In)
        {
            CRM_API.Models.TotalUsageHistory.GetCallHistorySearchCBSRef errResult = new CRM_API.Models.TotalUsageHistory.GetCallHistorySearchCBSRef()
            {
                errcode = -1,
                errsubject = "General Error",
                errmsg = "Empty New Order"
            };
            try
            {
                switch (In.type)
                {
                    case CRM_API.Models.TotalUsageHistory.Action.GetCallHistorySearchCBS:
                        return GetCallHistorySearchCBS(In);
                    default: break;
                }
            }
            catch (Exception ex)
            {
                errResult.errmsg = "WEB-API: " + ex.Message;
                errResult.errcode = -1;
            }
            return errResult;
        }

        //28-Dec-2018 : Moorthy : Added for Bundle breakdown
        private IEnumerable<CRM_API.Models.TotalUsageHistory.GetCallHistorySearchCBSRef> GetCallHistorySearchCBS(CRM_API.Models.TotalUsageHistory.CustomerTotalUsageHistoryRequest x)
        {
            IEnumerable<CRM_API.Models.TotalUsageHistory.GetCallHistorySearchCBSRef> result = CRM_API.DB.TotalUsageHistory.SProc.GetCallHistorySearchCBS(x.Msisdn, x.Date_From1, x.Date_To1, x.search_type, x.Sitecode, x.PackageID, x.BundleName);
            if (result == null)
                return new List<CRM_API.Models.TotalUsageHistory.GetCallHistorySearchCBSRef>().ToArray();
            else
                return result.ToArray();
        }

        //28-Dec-2018 : Moorthy : Added for Bundle breakdown
        private static void FormatDataBundleBreakdownDowload(dynamic result, ref StringBuilder str)
        {
            str.Append("<table>");
            str.Append("<thead>");
            str.Append("<tr>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Bundle Name</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>History Type</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Call & Time</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>CLI</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Destination Number</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Type</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Destination Code</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Duration (Min:sec)</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Data Usage(in MB)</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Tariff Class</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Net Charges</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Balance</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Roaming</th>");
            str.Append("</tr>");
            str.Append("</thead>");
            str.Append("<tbody>");
            foreach (CRM_API.Models.TotalUsageHistory.GetCallHistorySearchCBSRef val in (result as IEnumerable<CRM_API.Models.TotalUsageHistory.GetCallHistorySearchCBSRef>))
            {
                str.Append("<tr>");
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.Package_Name + "</td>");
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.History_Type + "</td>");
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.Date_Time + "</td>");
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.CLI + "</td>");
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.Destination_Number + "</td>");
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.Type + "</td>");
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.Destination_Code + "</td>");
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.Duration_Min + "</td>");
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + Convert.ToString(val.Data_Usage) + "</td>");
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.Tariff_Class + "</td>");
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.Net_Charges + "</td>");
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.Balance + "</td>");
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.roaming_status + "</td>");
                str.Append("</tr>");
            }
            str.Append("</tbody>");
            str.Append("</table>");
            str.Append("</body>");
        }

        //Issue History List
        [HttpGet]
        public ActionResult DownloadIssueHistoryList(string sitecode, string date_fr, string date_to)
        {
            try
            {
                Log.Info("DownloadCancelTopupReport");
                IssueHistoryListInput Model = new IssueHistoryListInput();
                Model.sitecode = sitecode;
                Model.date_from = date_fr;
                Model.date_to = date_to;
                List<IssueHistoryListOutput> resultValues = IssueHistoryList.GetIssueHistoryList(Model).ToList();

                StringBuilder str = new StringBuilder();
                str.Append("<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{Issue History Report}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv=\"content-type\" content=\"text/plain; charset=UTF-8\"/><style>.xlText{ mso-number-format:'@';}</style></head><body>");
                str.Append("<h3>Issue History Report</h3>");
                str.Append("<table>");
                str.Append("<thead>");
                str.Append("<tr>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Customer Name</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Subscriber ID</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Mobile No</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Contact Type</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Call Type</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Category</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Sub Category</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Status</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Interval</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Escalated To</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Problem Desc.</th>");
                //str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Type Desc.</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Submit Date</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Submit By</th>");
                str.Append("</tr>");
                str.Append("</thead>");
                str.Append("<tbody>");

                foreach (IssueHistoryListOutput val in resultValues)
                {
                    str.Append("<tr>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Customer_Name) ? "" : val.Customer_Name) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Subscriber_ID) ? "" : val.Subscriber_ID) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Mobile_number) ? "" : val.Mobile_number) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.contact_type) ? "" : val.contact_type) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Call_Type) ? "" : val.Call_Type) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Category) ? "" : val.Category) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Sub_category) ? "" : val.Sub_category) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Status) ? "" : val.Status) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Interval) ? "" : val.Interval) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.esclated_to) ? "" : val.esclated_to) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.problemdesc) ? "" : val.problemdesc) + "</td>");
                    //str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.typedesc) ? "" : val.typedesc) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.submitdate_String) ? "" : val.submitdate_String) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Submitby) ? "" : val.Submitby) + "</td>");
                    str.Append("</tr>");
                }
                str.Append("</tbody>");
                str.Append("</table>");
                str.Append("</body></html>");

                HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=IssueHistoryList_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
                this.Response.ContentType = "application/vnd.ms-excel";
                byte[] temp = System.Text.Encoding.UTF8.GetBytes(str.ToString());
                string _random = Guid.NewGuid().ToString();
                TempData[_random] = str.ToString();

                var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = 0, errmsg = "Download Ready", Text = _random };
                return Json(resy, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = -1, errmsg = ex.Message };
                return Json(resy, JsonRequestBehavior.AllowGet);
            }
        }

        //Issue History Report
        [HttpGet]
        public FileContentResult DownloadFinanceIssueHistoryList(string id)
        {
            var result = (string)TempData[id];
            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=SMSHistoryReport_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");//30/052019
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(result.ToString());
            return new FileContentResult(temp, "application/vnd.ms-excel");
        }

        //05-Feb-2019 : Moorthy : Added for auto Topup Report
        [HttpGet]
        public ActionResult DownloadAutoTopupReport(string date)
        {
            try
            {
                Log.Info("DownloadAutoTopupReport");
                var resultValues = CRM_API.DB.viewtransaction.GetAutoTopupReport(date);

                StringBuilder str = new StringBuilder();
                str.Append("<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{Auto Topup Report}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv=\"content-type\" content=\"text/plain; charset=UTF-8\"/><style>.xlText{ mso-number-format:'@';}</style></head><body>");
                str.Append("<h3>Auto Topup Report</h3>");
                str.Append("<table>");
                str.Append("<thead>");
                str.Append("<tr>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Reference ID</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Account Id</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Subscription Id</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Debit Amount</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Currency</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Credit Date</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Status</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Code</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Scheme Reference Data</th>");
                str.Append("</tr>");
                str.Append("</thead>");
                str.Append("<tbody>");

                foreach (AutotopupReport val in resultValues)
                {
                    str.Append("<tr>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.sorderid) ? "" : val.sorderid) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.mobileno) ? "" : val.mobileno) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.subscription_id) ? "" : val.subscription_id) + "</td>");
                    str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.amount ?? 0 + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.currency) ? "" : val.currency) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.reg_date) ? "" : val.reg_date) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.decision) ? "" : val.decision) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.payment_status ?? 0 + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.srd) ? "" : val.srd) + "</td>");
                    str.Append("</tr>");
                }
                str.Append("</tbody>");
                str.Append("</table>");
                str.Append("</body></html>");

                HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=AutoTopupReport_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
                this.Response.ContentType = "application/vnd.ms-excel";
                byte[] temp = System.Text.Encoding.UTF8.GetBytes(str.ToString());
                string _random = Guid.NewGuid().ToString();
                TempData[_random] = str.ToString();

                var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = 0, errmsg = "Download Ready", Text = _random };
                return Json(resy, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = -1, errmsg = ex.Message };
                return Json(resy, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public FileContentResult DownloadFinanceAutoTopupReport(string id)
        {
            var result = (string)TempData[id];
            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=AutoTopupReport_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(result.ToString());
            return new FileContentResult(temp, "application/vnd.ms-excel");
        }

        //12-Feb-2019 : Moorthy : Added for General Report
        [HttpGet]
        public ActionResult DownloadGeneralReport(string date)
        {
            try
            {
                Log.Info("DownloadGeneralReport");
                var resultValues = CRM_API.DB.viewtransaction.GetGeneralReport(date);

                StringBuilder str = new StringBuilder();
                str.Append("<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{General Report}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv=\"content-type\" content=\"text/plain; charset=UTF-8\"/><style>.xlText{ mso-number-format:'@';}</style></head><body>");
                str.Append("<h3>General Report</h3>");
                str.Append("<table>");
                str.Append("<thead>");
                str.Append("<tr>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Site Code</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Created Date</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Account Id</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Reference Id</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Amount</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Payment Mode</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Payment Agent</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Product Code</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Service Type</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Currency</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Subscription Id</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>CC No</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Client IP</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Email</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Status</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Err Code</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Err Msg</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>First Login</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>ECI Value</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Scheme Reference Data</th>");
                str.Append("</tr>");
                str.Append("</thead>");
                str.Append("<tbody>");

                foreach (GeneralReport val in resultValues)
                {
                    str.Append("<tr>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.sitecode) ? "" : val.sitecode) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.created_date) ? "" : val.created_date) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.account_id) ? "" : val.account_id) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.reference_id) ? "" : val.reference_id) + "</td>");
                    str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.totalamount ?? 0 + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.payment_mode) ? "" : val.payment_mode) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.payment_agent) ? "" : val.payment_agent) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.product_code) ? "" : val.product_code) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.service_type) ? "" : val.service_type) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.currency) ? "" : val.currency) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.subscriptionid) ? "" : val.subscriptionid) +
                        "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.cc_no) ? "" : val.cc_no) +
                        "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.ipclient) ? "" : val.ipclient) +
                        "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.email) ? "" : val.email) +
                        "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.status) ? "" : val.status) +
                        "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.error_code + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.error_msg) ? "" : val.error_msg) +
                        "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.sim_first_login) ? "" : val.sim_first_login) +
                        "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.ECI_Value +
                        "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.srd) ? "" : val.srd) +
                        "</td>");
                    str.Append("</tr>");
                }
                str.Append("</tbody>");
                str.Append("</table>");
                str.Append("</body></html>");

                HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=GeneralReport_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
                this.Response.ContentType = "application/vnd.ms-excel";
                byte[] temp = System.Text.Encoding.UTF8.GetBytes(str.ToString());
                string _random = Guid.NewGuid().ToString();
                TempData[_random] = str.ToString();

                var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = 0, errmsg = "Download Ready", Text = _random };
                return Json(resy, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = -1, errmsg = ex.Message };
                return Json(resy, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public FileContentResult DownloadFinanceGeneralReport(string id)
        {
            var result = (string)TempData[id];
            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=GeneralReport_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(result.ToString());
            return new FileContentResult(temp, "application/vnd.ms-excel");
        }

        //FreeDataBundleReport
        [HttpGet]
        public ActionResult DownloadFreeDataBundleReport(string sitecode, string from_date, string to_date)
        {
            try
            {
                Log.Info("DownloadFreeDataBundleReport");
                FreeDataBundleInput Model = new FreeDataBundleInput();
                Model.sitecode = sitecode;
                Model.from_date = from_date;
                Model.to_date = to_date;
                List<GetFreelySubscribedBundleReportOutput> resultValues = FreeDataBundleDB.GetFreelySubscribedBundleReport(Model).ToList();

                StringBuilder str = new StringBuilder();
                str.Append("<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{Transfer Balance Report}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv=\"content-type\" content=\"text/plain; charset=UTF-8\"/><style>.xlText{ mso-number-format:'@';}</style></head><body>");
                str.Append("<h3>Free Data Bundle Report</h3>");
                str.Append("<table>");
                str.Append("<thead>");
                str.Append("<tr>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Mobile No</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Bundle Category</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Bundle Name</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Bundle Price</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Log Date</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Subscribed User</th>");
                str.Append("</tr>");
                str.Append("</thead>");
                str.Append("<tbody>");

                foreach (GetFreelySubscribedBundleReportOutput val in resultValues)
                {
                    str.Append("<tr>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.mobileno) ? "" : val.mobileno) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.bundle_category) ? "" : val.bundle_category) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.bundle_name) ? "" : val.bundle_name) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.bundle_price) ? "" : val.bundle_price) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (val.logdate == null ? "" : Convert.ToString(val.logdate)) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.subs_user) ? "" : val.subs_user) + "</td>");
                    str.Append("</tr>");
                }
                str.Append("</tbody>");
                str.Append("</table>");
                str.Append("</body></html>");

                HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=FreeDataBundleReport_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
                this.Response.ContentType = "application/vnd.ms-excel";
                byte[] temp = System.Text.Encoding.UTF8.GetBytes(str.ToString());
                string _random = Guid.NewGuid().ToString();
                TempData[_random] = str.ToString();

                var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = 0, errmsg = "Download Ready", Text = _random };
                return Json(resy, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = -1, errmsg = ex.Message };
                return Json(resy, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public FileContentResult DownloadFinanceFreeDataBundleReport(string id)
        {
            var result = (string)TempData[id];
            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=FreeDataBundleReport_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(result.ToString());
            return new FileContentResult(temp, "application/vnd.ms-excel");
        }
        //Issue History List
        [HttpGet]
        public ActionResult DownloadSMSIssueHistoryList(string sitecode, string date_fr, string date_to)
        {
            try
            {
                Log.Info("DownloadSMSIssueHistoryList Report");
                SMSIssueHistoryListInput Model = new SMSIssueHistoryListInput();
                Model.sitecode = sitecode;
                Model.date_from = date_fr;
                Model.date_to = date_to;
                List<SMSIssueHistoryListOutput> resultValues = IssueHistoryList.GetSMSIssueHistoryList(Model).ToList();

                StringBuilder str = new StringBuilder();
                str.Append("<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{SMS History Report}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv=\"content-type\" content=\"text/plain; charset=UTF-8\"/><style>.xlText{ mso-number-format:'@';}</style></head><body>");
                str.Append("<h3>SMS History Report</h3>");
                str.Append("<table>");
                str.Append("<thead>");
                str.Append("<tr>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>MSISDN</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Agent Name</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>SMS Date</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Time</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>SMS Type</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>SMS Text</th>");
                str.Append("</tr>");
                str.Append("</thead>");
                str.Append("<tbody>");

                foreach (SMSIssueHistoryListOutput val in resultValues)
                {
                    str.Append("<tr>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.msisdn) ? "" : val.msisdn) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.agent_name) ? "" : val.agent_name) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.sms_date_String) ? "" : val.sms_date_String) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.time) ? "" : val.time) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.sms_type) ? "" : val.sms_type) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.sms_text) ? "" : val.sms_text) + "</td>");
                    str.Append("</tr>");
                }
                str.Append("</tbody>");
                str.Append("</table>");
                str.Append("</body></html>");

                HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=SMSHistoryList_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
                this.Response.ContentType = "application/vnd.ms-excel";
                byte[] temp = System.Text.Encoding.UTF8.GetBytes(str.ToString());
                string _random = Guid.NewGuid().ToString();
                TempData[_random] = str.ToString();

                var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = 0, errmsg = "Download Ready", Text = _random };
                return Json(resy, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = -1, errmsg = ex.Message };
                return Json(resy, JsonRequestBehavior.AllowGet);
            }
        }

        //Complaints History Report
        [HttpGet]
        public FileContentResult DownloadComplaintsHistoryList(string id)
        {
            var result = (string)TempData[id];
            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=ComplaintsHistoryReport_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(result.ToString());
            return new FileContentResult(temp, "application/vnd.ms-excel");
        }

        //Complaints History
        [HttpGet]
        public ActionResult DownloadComplaintsHistory(string date_fr, string date_to)
        {
            try
            {
                Log.Info("DownloadComplaintsHistory Report");
                ComplaintModels Model = new ComplaintModels();
                Model.date_from = date_fr;
                Model.date_to = date_to;
                List<ComplaintModels> resultValues = ComplaintsHistory.GetComplaintsHistoryList(Model.date_from, Model.date_to, Model.cur_user).ToList();

                StringBuilder str = new StringBuilder();
                str.Append("<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{Complaints History Report}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv=\"content-type\" content=\"text/plain; charset=UTF-8\"/><style>.xlText{ mso-number-format:'@';}</style></head><body>");
                str.Append("<h3>Complaints History Report</h3>");
                str.Append("<table>");
                str.Append("<thead>");
                str.Append("<tr>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>TicketId</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Mobile Number</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Issue Generated Date</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Ticket Type</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Category</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Sub category</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Agent Name</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Esclated User</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Description</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Issue Completed Date</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Aging</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Status</th>");
                str.Append("</tr>");
                str.Append("</thead>");
                str.Append("<tbody>");

                foreach (ComplaintModels val in resultValues)
                {
                    str.Append("<tr>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Ticket_id) ? "" : val.Ticket_id) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.mobileno) ? "" : val.mobileno) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.issue_generate_date_string) ? "" : val.issue_generate_date_string) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Issue_Name) ? "" : val.Issue_Name) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Function_Name) ? "" : val.Function_Name) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.sub_category) ? "" : val.sub_category) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.agent_name) ? "" : val.agent_name) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.eslated_user) ? "" : val.eslated_user) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.descr) ? "" : val.descr) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.issue_complete_date_string) ? "" : val.issue_complete_date_string) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.aging) ? "" : val.aging) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.status) ? "" : val.status) + "</td>");
                    str.Append("</tr>");
                }
                str.Append("</tbody>");
                str.Append("</table>");
                str.Append("</body></html>");

                HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=ComplaintsHistory_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
                this.Response.ContentType = "application/vnd.ms-excel";
                byte[] temp = System.Text.Encoding.UTF8.GetBytes(str.ToString());
                string _random = Guid.NewGuid().ToString();
                TempData[_random] = str.ToString();

                var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = 0, errmsg = "Download Ready", Text = _random };
                return Json(resy, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = -1, errmsg = ex.Message };
                return Json(resy, JsonRequestBehavior.AllowGet);
            }
        }

        //Add Service History Report
        [HttpGet]
        public FileContentResult DownloadBundleServiceHistoryList(string id)
        {
            var result = (string)TempData[id];
            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=AddServiceHistoryReport_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(result.ToString());
            return new FileContentResult(temp, "application/vnd.ms-excel");
        }

        //Add Service History
        [HttpGet]
        public ActionResult DownloadBundleServiceHistory(string date_fr, string date_to)
        {
            try
            {
                Log.Info("DownloadBundleServiceHistory Report");
                BundleServiceModels Model = new BundleServiceModels();
                Model.date_from = date_fr;
                Model.date_to = date_to;
                List<BundleServiceModels> resultValues = BundleServiceHistory.GetBundleServiceHistoryList(Model.date_from, Model.date_to).ToList();

                StringBuilder str = new StringBuilder();
                str.Append("<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{Add Service History Report}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv=\"content-type\" content=\"text/plain; charset=UTF-8\"/><style>.xlText{ mso-number-format:'@';}</style></head><body>");
                str.Append("<h3>Add Service History Report</h3>");
                str.Append("<table>");
                str.Append("<thead>");
                str.Append("<tr>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Mobile Number</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Top-up Date</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Payment Reference</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Amount</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Currency</th>");
                str.Append("</tr>");
                str.Append("</thead>");
                str.Append("<tbody>");

                foreach (BundleServiceModels val in resultValues)
                {
                    str.Append("<tr>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.mobileno) ? "" : val.mobileno) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.topup_date_string) ? "" : val.topup_date_string) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.paymentref) ? "" : val.paymentref) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.amount.ToString()) ? "" : val.amount.ToString()) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.ccdc_curr) ? "" : val.ccdc_curr) + "</td>");
                    str.Append("</tr>");
                }
                str.Append("</tbody>");
                str.Append("</table>");
                str.Append("</body></html>");

                HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=AddServiceHistory_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
                this.Response.ContentType = "application/vnd.ms-excel";
                byte[] temp = System.Text.Encoding.UTF8.GetBytes(str.ToString());
                string _random = Guid.NewGuid().ToString();
                TempData[_random] = str.ToString();

                var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = 0, errmsg = "Download Ready", Text = _random };
                return Json(resy, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = -1, errmsg = ex.Message };
                return Json(resy, JsonRequestBehavior.AllowGet);
            }
        }

        //Doc Repository
        [HttpGet]
        public FileContentResult DownloadDocFile(string id)
        {
            var result = System.IO.File.ReadAllBytes(Path.Combine(ConfigurationManager.AppSettings["UPLOADFILEPATH"], id));
            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=" + id);
            this.Response.ContentType = "application/pdf";
            return new FileContentResult(result, "application/pdf");
        }

        //SIM Activation History Report
        [HttpGet]
        public FileContentResult DownloadSIMActivationList(string id)
        {
            var result = (string)TempData[id];
            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=SIMActivationHistoryReport_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(result.ToString());
            return new FileContentResult(temp, "application/vnd.ms-excel");
        }
        //SIM Activation History Report
        [HttpGet]
        public FileContentResult DownloadRenewalList(string id)
        {
            var result = (string)TempData[id];
            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=AutoRenewalHistoryReport_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(result.ToString());
            return new FileContentResult(temp, "application/vnd.ms-excel");
        }

        //SIM Activation History
        [HttpGet]
        public ActionResult DownloadSIMActivationHistory(string date_fr, string date_to)
        {
            try
            {
                Log.Info("DownloadSIMActivationHistory Report");
                BundleServiceModels Model = new BundleServiceModels();
                Model.date_from = date_fr;
                Model.date_to = date_to;
                List<SIMActivationModels> resultValues = SIMActivationHistory.GetSIMActivationHistoryList(Model.date_from, Model.date_to).ToList();

                StringBuilder str = new StringBuilder();
                str.Append("<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{SIM Activation History Report}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv=\"content-type\" content=\"text/plain; charset=UTF-8\"/><style>.xlText{ mso-number-format:'@';}</style></head><body>");
                str.Append("<h3>SIM Activation History Report</h3>");
                str.Append("<table>");
                str.Append("<thead>");
                str.Append("<tr>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Date</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Mobile Number</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>ICCID</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Activated By</th>");
                str.Append("</tr>");
                str.Append("</thead>");
                str.Append("<tbody>");

                foreach (SIMActivationModels val in resultValues)
                {
                    str.Append("<tr>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.act_date_string) ? "" : val.act_date_string) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.mobileNo) ? "" : val.mobileNo) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.iccid) ? "" : val.iccid) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.act_by) ? "" : val.act_by) + "</td>");
                    str.Append("</tr>");
                }
                str.Append("</tbody>");
                str.Append("</table>");
                str.Append("</body></html>");

                HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=AddServiceHistory_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
                this.Response.ContentType = "application/vnd.ms-excel";
                byte[] temp = System.Text.Encoding.UTF8.GetBytes(str.ToString());
                string _random = Guid.NewGuid().ToString();
                TempData[_random] = str.ToString();

                var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = 0, errmsg = "Download Ready", Text = _random };
                return Json(resy, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = -1, errmsg = ex.Message };
                return Json(resy, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult DownloadAutoRenewalHistory(string sitecode, string msisdn, string date_fr, string date_to)
        {
            try
            {
                Log.Info("DownloadAutoRenewalHistory Report");
                AutoRenewalHistoryModels Model = new AutoRenewalHistoryModels();
                Model.sitecode = sitecode;
                Model.msisdn = msisdn;
                Model.date_from = date_fr;
                Model.date_to = date_to;
                List<AutoRenewalHistoryModels> resultValues = AutoRenewalHistory.GetAutoRenewalHistoryList(Model.sitecode, Model.msisdn, Model.date_from, Model.date_to).ToList();

                StringBuilder str = new StringBuilder();
                str.Append("<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{AutoRenewalHistory Report}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv=\"content-type\" content=\"text/plain; charset=UTF-8\"/><style>.xlText{ mso-number-format:'@';}</style></head><body>");
                str.Append("<h3>Auto Renewal History Report</h3>");
                str.Append("<table>");
                str.Append("<thead>");
                str.Append("<tr>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>mobileno</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>iccid</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>bundleid</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>bundle_name</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>create_date</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>create_by</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>comments</th>");
                str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>process_name</th>");
                str.Append("</tr>");
                str.Append("</thead>");
                str.Append("<tbody>");

                foreach (AutoRenewalHistoryModels val in resultValues)
                {
                    str.Append("<tr>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.mobileno) ? "" : val.mobileno) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.iccid) ? "" : val.iccid) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + val.bundleid + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.bundle_name) ? "" : val.bundle_name) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.create_date_string) ? "" : val.create_date_string) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.create_by) ? "" : val.create_by) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.comments) ? "" : val.comments) + "</td>");
                    str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.process_name) ? "" : val.process_name) + "</td>");
                    str.Append("</tr>");
                }
                str.Append("</tbody>");
                str.Append("</table>");
                str.Append("</body></html>");

                HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=AutoRenewalHistory_" + Model.msisdn + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
                this.Response.ContentType = "application/vnd.ms-excel";
                byte[] temp = System.Text.Encoding.UTF8.GetBytes(str.ToString());
                string _random = Guid.NewGuid().ToString();
                TempData[_random] = str.ToString();

                var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = 0, errmsg = "Download Ready", Text = _random };
                return Json(resy, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = -1, errmsg = ex.Message };
                return Json(resy, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
