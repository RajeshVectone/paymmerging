﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRM3.WebApp.Attributes;

namespace CRM3.WebApp.Controllers
{
    [CRM3Auth]
    public class PortINController : Controller
    {
        //
        // GET: /PortIN/

        public ActionResult Index()
        {
            return RedirectToAction("UKSubmitRequest");
        }

        // UNITED KINGDOM
        // GET: /PortIN/UKSubmitRequest
        public ActionResult UKSubmitRequest()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    return View();
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        // UNITED KINGDOM
        // GET: /PortIn/UKModifyRequest
        public ActionResult UKModifyRequest()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    return View();
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        // UNITED KINGDOM
        // GET: /PortIn/UKSMSRequest
        public ActionResult UKSMSRequest()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    return View();
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        // NETHERLAND
        // GET: /PortIn/NLPortInList
        public ActionResult NLPortInList()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    return View();
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        // NETHERLAND
        // GET: /PortIn/NLPortingRequest
        public ActionResult NLPortingRequest()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    return View();
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        // AUSTRIA
        // GET: /PortIn/ATSendRequestNuev
        public ActionResult ATSendRequestNuev()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    return View();
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        // AUSTRIA
        // GET: /PortIn/ATRequestNuevList
        public ActionResult ATRequestNuevList()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    return View();
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        // AUSTRIA
        // GET: /PortIn/ATPortingRequest
        public ActionResult ATPortingRequest()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    return View();
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        // AUSTRIA
        // GET: /PortIn/ATPortingCancel
        public ActionResult ATPortingCancel()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    return View();
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        // AUSTRIA
        // GET: /PortIn/ATTransactionReport
        public ActionResult ATTransactionReport()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    return View();
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        // SWEDEN
        // GET: /PortIn/SEPortingRequest
        public ActionResult SEPortingRequest()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    return View();
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        // SWEDEN
        // GET: /PortIn/SEPortInList
        public ActionResult SEPortInList()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    return View();
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        // SWEDEN
        // GET: /PortIn/SERejectedPortInList
        public ActionResult SERejectedPortInList()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    //10-Aug-2015 : Moorthy Added to display Error Page instead of SERejectedPortInList page(new screen)
                    return View("Error");
                    //10-Aug-2015 : Moorthy commented to avoid "Server Error in '/' Application." error
                    //return View();
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }
    }
}
