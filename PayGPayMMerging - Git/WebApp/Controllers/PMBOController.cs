﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRM3.WebApp.Attributes;
using CRM_API.Models;
using System.Data;
using System.Data.OleDb;
using System.Configuration;
using System.Data.SqlClient;
using Dapper;
using System.Collections;

namespace CRM3.WebApp.Controllers
{
    [CRM3Auth]
    public class PMBOController : Controller
    {
        // GET: /PMBO/
        public ActionResult Index()
        {
            return View();
        }

        //DD Operations
        public ActionResult TabDDOperation()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    HttpContext.Session["GlobalProduct"] = "VMUK";
                }
                else
                {
                    var page = new PMBOPageModel();
                    try
                    {
                        if (HttpContext.Session["GlobalProduct"].ToString() == "VMFR")
                        {
                            ViewBag._sitecode = "MFR";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else if (HttpContext.Session["GlobalProduct"].ToString() == "VMAT")
                        {
                            ViewBag._sitecode = "BAU";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else if (HttpContext.Session["GlobalProduct"].ToString() == "VMNL")
                        {
                            ViewBag._sitecode = "BNL";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else
                        {
                            ViewBag._sitecode = "MCM";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }
                }
            }
            else
            {
                HttpContext.Session["GlobalProduct"] = "VMUK";
                ViewBag._sitecode = "MCM";
                ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
            }
            return View();
        }

        //Pay way files 
        public ActionResult TabPaywayfiles()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                { HttpContext.Session["GlobalProduct"] = "VMUK"; }
                else
                {
                    var page = new PMBOPageModel();
                    try
                    {
                        if (HttpContext.Session["GlobalProduct"].ToString() == "VMFR")
                        {
                            ViewBag._sitecode = "MFR";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else if (HttpContext.Session["GlobalProduct"].ToString() == "VMAT")
                        {
                            ViewBag._sitecode = "BAU";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else if (HttpContext.Session["GlobalProduct"].ToString() == "VMNL")
                        {
                            ViewBag._sitecode = "BNL";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else
                        {
                            ViewBag._sitecode = "MCM";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }
                }
            }
            else
            {
                HttpContext.Session["GlobalProduct"] = "VMUK";
                ViewBag._sitecode = "MCM";
                ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
            }
            return View();
        }

        //Manual Marking 
        public ActionResult TabManualMarking(PMBOPageModel model)
        {
            model.ActiveTab = "testFirst";
            if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
            { HttpContext.Session["GlobalProduct"] = "VMUK"; }
            switch (model.ActiveTab)
            {
                case "TabUploadNUpdateFailures":
                    return Redirect("/PMBO/testFirst/");
                case "TabViewNProcessFailures":
                    return Redirect("/PayMonthly/TabViewNProcessFailures/");

            }
            return View();
        }

        //Sub - Failure Transaction
        //public ActionResult TabFailureTransaction()
        //{
        //    var page = new PMBOPageModel();
        //    try
        //    {
        //        page.ActiveTab = "TabFailureTransaction";
        //        ViewBag._sitecode = "MCM";
        //        ViewBag._productcode = "VMUK";
        //        HttpContext.Session["GlobalProduct"] = "VMUK";
        //    }
        //    catch (Exception ex)
        //    {
        //        page.ErrMsg = ex.Message;
        //    }
        //    return View("TabManualMarking", page);
        //}

        ////Sub - Search Customer
        //public ActionResult TabSearchCustomer()
        //{
        //    var page = new PMBOPageModel();
        //    try
        //    {
        //        page.ActiveTab = "TabSearchCustomer";
        //        ViewBag._sitecode = "MCM";
        //        ViewBag._productcode = "VMUK";
        //        HttpContext.Session["GlobalProduct"] = "VMUK";
        //    }
        //    catch (Exception ex)
        //    {
        //        page.ErrMsg = ex.Message;
        //    }

        //    return View("TabManualMarking", page);
        //}

        //Sub - Failure Transaction
        //public ActionResult TabUploadNUpdateFailures()
        //{
        //    List<ManualMarkingPage> lstForGrid = new List<ManualMarkingPage>();
        //    var page = new ManualMarkingPage();
        //    try
        //    {
        //        if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
        //        { HttpContext.Session["GlobalProduct"] = "VMUK"; }
        //        if (TempData["CheckVal"] != null)
        //        {
        //            //using (var Conn2 = new SqlConnection(CRM_API.Helpers.Config.MCMCRMConnection))
        //            //{
        //            //    var resForGrid = Conn2.Query<ManualMarkingPage>(
        //            //                       "get_latest_upload_info_for_grid",
        //            //                       new
        //            //                       {
        //            //                           @id = 12
        //            //                       }, commandType: CommandType.StoredProcedure);

        //            //    if (resForGrid.Count() > 0)
        //            //    {


        //            //        lstForGrid = resForGrid.ToList();

        //            //        TempData["CheckVal"] = lstForGrid;


        //            //        foreach (var item in lstForGrid)
        //            //        {
        //            //            item.ActiveTab = "TabUploadNUpdateFailures";
        //            //        }

        //            //    }
        //            //}
        //        }
        //        else
        //        {
        //            ViewBag.ModelCheck = null;
        //            lstForGrid = null;
        //        }

        //        if (ViewBag.ModelCheck != null)
        //        {
        //            page.ActiveTab = "TabUploadNUpdateFailures";
        //            ViewBag._sitecode = "MCM";
        //            ViewBag._productcode = "VMUK";
        //            HttpContext.Session["GlobalProduct"] = "VMUK";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        page.ErrMsg = ex.Message;
        //    }

        //    //return View(new ManualMarkingPage() { =lstForGrid() });


        //    return View("TabUploadNUpdateFailures", lstForGrid);
        //}

        public ActionResult TabUploadNUpdateFailures()
        {
            var page = new PMBOPageModel();
            try
            {                
                //ViewBag._sitecode = "MCM";
                //ViewBag._productcode = "VMUK";
                //HttpContext.Session["GlobalProduct"] = "VMUK";
                page.ActiveTab = "TabUploadNUpdateFailures";
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                { HttpContext.Session["GlobalProduct"] = "VMUK"; }
                else
                {
                    try
                    {
                        if (HttpContext.Session["GlobalProduct"].ToString() == "VMFR")
                        {
                            ViewBag._sitecode = "MFR";
                            ViewBag._productcode = "VMFR";
                            HttpContext.Session["GlobalProduct"] = "VMFR";
                        }
                        else if (HttpContext.Session["GlobalProduct"].ToString() == "VMAT")
                        {
                            ViewBag._sitecode = "BAU";
                            ViewBag._productcode = "VMAT";
                            HttpContext.Session["GlobalProduct"] = "VMAT";
                        }
                        else if (HttpContext.Session["GlobalProduct"].ToString() == "VMNL")
                        {
                            ViewBag._sitecode = "BNL";
                            ViewBag._productcode = "VMNL";
                            HttpContext.Session["GlobalProduct"] = "VMNL";
                        }
                        else
                        {
                            ViewBag._sitecode = "MCM";
                            ViewBag._productcode = "VMUK";
                            HttpContext.Session["GlobalProduct"] = "VMUK";
                        }
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }
                }
            }
            catch (Exception ex)
            {
                page.ErrMsg = ex.Message;
            }
            return View("TabUploadNUpdateFailures", page);
        }

        //Sub - Search Customer
        public ActionResult TabViewNProcessFailures()
        {
            var page = new PMBOPageModel();
            try
            {
                page.ActiveTab = "TabViewNProcessFailures";
                if (HttpContext.Session["GlobalProduct"].ToString() == "VMFR")
                {
                    ViewBag._sitecode = "MFR";
                    ViewBag._productcode = "VMFR";
                    HttpContext.Session["GlobalProduct"] = "VMFR";
                }
                else if (HttpContext.Session["GlobalProduct"].ToString() == "VMAT")
                {
                    ViewBag._sitecode = "BAU";
                    ViewBag._productcode = "VMAT";
                    HttpContext.Session["GlobalProduct"] = "VMAT";
                }
                else if (HttpContext.Session["GlobalProduct"].ToString() == "VMNL")
                {
                    ViewBag._sitecode = "BNL";
                    ViewBag._productcode = "VMNL";
                    HttpContext.Session["GlobalProduct"] = "VMNL";
                }
                else
                {
                    ViewBag._sitecode = "MCM";
                    ViewBag._productcode = "VMUK";
                    HttpContext.Session["GlobalProduct"] = "VMUK";
                }
            }
            catch (Exception ex)
            {
                page.ErrMsg = ex.Message;
            }

            return View("TabViewNProcessFailures", page);
        }

        public ActionResult testFirst()
        {
            var page = new PMBOPageModel();
            try
            {
                page.ActiveTab = "TabViewNProcessFailures";
                if (HttpContext.Session["GlobalProduct"].ToString() == "VMFR")
                {
                    ViewBag._sitecode = "MFR";
                    ViewBag._productcode = "VMFR";
                    HttpContext.Session["GlobalProduct"] = "VMFR";
                }
                else if (HttpContext.Session["GlobalProduct"].ToString() == "VMAT")
                {
                    ViewBag._sitecode = "BAU";
                    ViewBag._productcode = "VMAT";
                    HttpContext.Session["GlobalProduct"] = "VMAT";
                }
                else if (HttpContext.Session["GlobalProduct"].ToString() == "VMNL")
                {
                    ViewBag._sitecode = "BNL";
                    ViewBag._productcode = "VMNL";
                    HttpContext.Session["GlobalProduct"] = "VMNL";
                }
                else
                {
                    ViewBag._sitecode = "MCM";
                    ViewBag._productcode = "VMUK";
                    HttpContext.Session["GlobalProduct"] = "VMUK";
                }
            }
            catch (Exception ex)
            {
                page.ErrMsg = ex.Message;
            }
            return View("testFirst", page);
        }

        public ActionResult Manualmarking1()
        {
            var page = new PMBOPageModel();
            try
            {
                page.ActiveTab = "TabViewNProcessFailures";
                if (HttpContext.Session["GlobalProduct"].ToString() == "VMFR")
                {
                    ViewBag._sitecode = "MFR";
                    ViewBag._productcode = "VMFR";
                    HttpContext.Session["GlobalProduct"] = "VMFR";
                }
                else if (HttpContext.Session["GlobalProduct"].ToString() == "VMAT")
                {
                    ViewBag._sitecode = "BAU";
                    ViewBag._productcode = "VMAT";
                    HttpContext.Session["GlobalProduct"] = "VMAT";
                }
                else if (HttpContext.Session["GlobalProduct"].ToString() == "VMNL")
                {
                    ViewBag._sitecode = "BNL";
                    ViewBag._productcode = "VMNL";
                    HttpContext.Session["GlobalProduct"] = "VMNL";
                }
                else
                {
                    ViewBag._sitecode = "MCM";
                    ViewBag._productcode = "VMUK";
                    HttpContext.Session["GlobalProduct"] = "VMUK";
                }
            }
            catch (Exception ex)
            {
                page.ErrMsg = ex.Message;
            }
            return View("Manualmarking1", page);
        }

        [HttpPost]
        public ActionResult UploadExcelSheet(HttpPostedFileBase file)
        {
            List<ManualMarking> ListModel = new List<ManualMarking>();
            DataTable dtdata = new DataTable();
            List<ManualMarkingPage> lstForGrid = new List<ManualMarkingPage>();
            List<DDCancellation> ListDDModel = new List<DDCancellation>();
            string strCollectionType = Request.Form["CollectionType"].ToString();

            if (Request.Files["file"] != null)
            {
                if (Request.Files["file"].ContentLength > 0)
                {
                    string fileExtension = System.IO.Path.GetExtension(Request.Files["file"].FileName);

                    if (fileExtension == ".xls" || fileExtension == ".xlsx")
                    {
                        string fileLocation = Server.MapPath("~/Content/") + Request.Files["file"].FileName;
                        if (System.IO.File.Exists(fileLocation))
                        {

                            System.IO.File.Delete(fileLocation);
                        }
                        Request.Files["file"].SaveAs(fileLocation);
                        string excelConnectionString = string.Empty;
                        excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                        //connection String for xls file format.
                        if (fileExtension == ".xls")
                        {
                            excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileLocation + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                        }
                        //connection String for xlsx file format.
                        else if (fileExtension == ".xlsx")
                        {

                            excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                        }
                        //Create Connection to Excel work book and add oledb namespace
                        OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                        excelConnection.Open();
                        DataTable dt = new DataTable();
                        dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                        if (dt == null)
                        {
                            return null;
                        }

                        String[] excelSheets = new String[dt.Rows.Count];
                        int t = 0;
                        //excel data saves in temp file here.
                        foreach (DataRow row in dt.Rows)
                        {
                            excelSheets[t] = row["TABLE_NAME"].ToString();
                            t++;
                        }
                        OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);
                        string query = string.Format("Select * from [{0}]", excelSheets[0]);
                        using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                        {
                            dataAdapter.Fill(dtdata);
                        }
                        excelConnection1.Close();
                        excelConnection.Close();
                    }


                    for (int i = 0; i < dtdata.Rows.Count; i++)
                    {
                        if (strCollectionType == "CF") //Collection Failure
                        {
                            try
                            {
                                if (dtdata.Rows[i]["ref"].ToString() != null)
                                {
                                    ManualMarking item = new ManualMarking();
                                    item.ref1 = dtdata.Rows[i]["ref"].ToString();
                                    item.transCode = dtdata.Rows[i]["transCode"].ToString();
                                    item.returnCode = dtdata.Rows[i]["returnCode"].ToString();
                                    item.returnDescription = dtdata.Rows[i]["returnDescription"].ToString();
                                    item.originalProcessingDate = dtdata.Rows[i]["originalProcessingDate"].ToString();
                                    item.valueOf = dtdata.Rows[i]["valueOf"].ToString();
                                    item.currency = dtdata.Rows[i]["currency"].ToString();
                                    item.number3 = dtdata.Rows[i]["number3"].ToString();
                                    item.ref4 = dtdata.Rows[i]["ref4"].ToString();
                                    item.name5 = dtdata.Rows[i]["name5"].ToString();
                                    item.sortCode6 = dtdata.Rows[i]["sortCode6"].ToString();
                                    item.bankName7 = dtdata.Rows[i]["bankName7"].ToString();
                                    item.branchName8 = dtdata.Rows[i]["branchName8"].ToString();
                                    item.numberOf = dtdata.Rows[i]["numberOf"].ToString();
                                    item.valueOf9 = dtdata.Rows[i]["valueOf9"].ToString();
                                    item.currency10 = dtdata.Rows[i]["currency10"].ToString();
                                    if (i == 0)
                                    {
                                        //item.originalProcessingDate
                                        var ThisPeriod1 = DateTime.Today.Year.ToString() + DateTime.Today.Month.ToString().PadLeft(2, '0');
                                        var x2 = item.originalProcessingDate.TrimEnd().TrimStart();
                                        var PeriodInXLS = x2.Substring(6, 4) + x2.Substring(3, 2);

                                        if (ThisPeriod1 != PeriodInXLS)
                                        {
                                            throw new Exception("Wrong XLS sheet for this Period!" + ThisPeriod1);
                                        }

                                    }
                                    ListModel.Add(item);
                                }
                            }
                            catch (Exception e)
                            {
                                return Redirect("/PMBO/TabUploadNUpdateFailures?id=WRONG&CollType=CF&FName=" + file.FileName);
                            }
                        }
                        else //DD Cancellation
                        {
                            try
                            {
                                if (dtdata.Rows[i]["report-generation-date"].ToString() != null)
                                {
                                    DDCancellation item = new DDCancellation();
                                    item.report_generation_date = dtdata.Rows[i]["report-generation-date"].ToString();
                                    item.effective_date = dtdata.Rows[i]["effective-date"].ToString();
                                    item.reference = dtdata.Rows[i]["reference"].ToString();
                                    item.payer_name = dtdata.Rows[i]["payer-name"].ToString();
                                    item.payer_account_number = dtdata.Rows[i]["payer-account-number"].ToString();
                                    item.payer_sort_code = dtdata.Rows[i]["payer-sort-code"].ToString();
                                    item.reason_code = dtdata.Rows[i]["reason-code"].ToString();

                                    ListDDModel.Add(item);
                                }
                            }
                            catch (Exception e)
                            {
                                return Redirect("/PMBO/TabUploadNUpdateFailures?id=WRONG&CollType=CF&FName=" + file.FileName);
                            }
                        }
                    }

                   // string conn = ConfigurationManager.ConnectionStrings["MCMCRMConnection"].ConnectionString; // this one is insert in .20 server 

                    string conn = CRM_API.Helpers.Config.MCMCRMConnection.ToString();
                    
                    SqlConnection con = new SqlConnection(conn);
                    List<ManualMarking> list = new List<ManualMarking>();
                    con.Open();
                    string guiId = Guid.NewGuid().ToString();

                    if (strCollectionType == "CF") //Collection Failure
                    {
                        using (var cmd = new SqlCommand("INSERT INTO collection_failure_upload_temp(refId,ref1,transCode,returnCode,returnDescription,originalProcessingDate,valueOf,currency,number3,ref4,name5,sortCode6,bankName7,branchName8,numberOf,valueOf9,currency10)VALUES('" + guiId + "',@ref1,@transCode,@returnCode,@returnDescription,@originalProcessingDate,@valueOf,@currency,@number3,@ref4,@name5,@sortCode6,@bankName7,@branchName8,@numberOf,@valueOf9,@currency10)", con))
                        {
                            cmd.Parameters.Add("@ref1", SqlDbType.VarChar);
                            cmd.Parameters.Add("@transCode", SqlDbType.VarChar);
                            cmd.Parameters.Add("@returnCode", SqlDbType.VarChar);
                            cmd.Parameters.Add("@returnDescription", SqlDbType.VarChar);
                            cmd.Parameters.Add("@originalProcessingDate", SqlDbType.VarChar);
                            cmd.Parameters.Add("@valueOf", SqlDbType.VarChar);
                            cmd.Parameters.Add("@currency", SqlDbType.VarChar);
                            cmd.Parameters.Add("@number3", SqlDbType.VarChar);
                            cmd.Parameters.Add("@ref4", SqlDbType.VarChar);
                            cmd.Parameters.Add("@name5", SqlDbType.VarChar);
                            cmd.Parameters.Add("@sortCode6", SqlDbType.VarChar);
                            cmd.Parameters.Add("@bankName7", SqlDbType.VarChar);
                            cmd.Parameters.Add("@branchName8", SqlDbType.VarChar);
                            cmd.Parameters.Add("@numberOf", SqlDbType.VarChar);
                            cmd.Parameters.Add("@valueOf9", SqlDbType.VarChar);
                            cmd.Parameters.Add("@currency10", SqlDbType.VarChar);

                            foreach (var Itemvalue in ListModel)
                            {
                                cmd.Parameters["@ref1"].Value = Itemvalue.ref1;
                                cmd.Parameters["@transCode"].Value = Itemvalue.transCode;
                                cmd.Parameters["@returnCode"].Value = Itemvalue.returnCode;
                                cmd.Parameters["@returnDescription"].Value = Itemvalue.returnDescription;
                                cmd.Parameters["@originalProcessingDate"].Value = Itemvalue.originalProcessingDate; //Convert.ToDateTime(Itemvalue.originalProcessingDate);
                                cmd.Parameters["@valueOf"].Value = Itemvalue.valueOf;
                                cmd.Parameters["@currency"].Value = Itemvalue.currency;
                                cmd.Parameters["@number3"].Value = Itemvalue.number3;
                                cmd.Parameters["@ref4"].Value = Itemvalue.ref4;
                                cmd.Parameters["@name5"].Value = Itemvalue.name5;
                                cmd.Parameters["@sortCode6"].Value = Itemvalue.sortCode6;
                                cmd.Parameters["@bankName7"].Value = Itemvalue.bankName7;
                                cmd.Parameters["@branchName8"].Value = Itemvalue.branchName8;
                                cmd.Parameters["@numberOf"].Value = Itemvalue.numberOf;
                                cmd.Parameters["@valueOf9"].Value = Itemvalue.valueOf9;
                                cmd.Parameters["@currency10"].Value = Itemvalue.currency10;
                                int rowsAffected = cmd.ExecuteNonQuery();
                            }
                        }
                        using (var cmd = new SqlCommand("INSERT INTO collection_failure_upload_params_temp(Upload_Count,Upload_XLS_Filename,Upload_By_User)VALUES(@Upload_Count,@Upload_XLS_Filename,@Upload_By_User)", con))
                        {
                            cmd.Parameters.Add("@Upload_Count", SqlDbType.Int);
                            cmd.Parameters.Add("@Upload_XLS_Filename", SqlDbType.VarChar);
                            cmd.Parameters.Add("@Upload_By_User", SqlDbType.VarChar);

                            cmd.Parameters["@Upload_Count"].Value = ListModel.Count;
                            cmd.Parameters["@Upload_XLS_Filename"].Value = file.FileName;
                            cmd.Parameters["@Upload_By_User"].Value = CRM_API.Helpers.Session.CurrentUser.user_login;
                            int rowsAffected = cmd.ExecuteNonQuery();
                        }

                        using (var Conn1 = new SqlConnection(CRM_API.Helpers.Config.MCMCRMConnection))
                        {
                            Conn1.Open();
                            var results = Conn1.Query<ManualMarkingXLSUploadResult>(
                                    "populate_collection_failure_upload",
                                    new
                                    {
                                        @ref_id = guiId
                                    }, commandType: CommandType.StoredProcedure);
                            // return RedirectToAction("TabUploadNUpdateFailures?id=12", "PMBO");

                            // return RedirectToAction("TabUploadNUpdateFailures", "PMBO", new { id = "?id="+12 });
                            ManualMarkingXLSUploadResult objResult1 = new ManualMarkingXLSUploadResult();
                            List<ManualMarkingXLSUploadResult> lst1 = results.ToList();
                            return Redirect("/PMBO/TabUploadNUpdateFailures?id=" + lst1[0].id + "&CollType=CF&FName=" + file.FileName);
                            //if (results.Count() > 0)
                            //{
                            //    ManualMarkingXLSUploadResult objResult = new ManualMarkingXLSUploadResult();
                            //    List<ManualMarkingXLSUploadResult> lst = results.ToList();
                            //    if (lst[0].errcode == 0)
                            //    {
                            //        var resForGrid = Conn1.Query<ManualMarkingPage>(
                            //                "get_latest_upload_info_for_grid",
                            //                new
                            //                {
                            //                    @id = 12
                            //                }, commandType: CommandType.StoredProcedure);

                            //        if (resForGrid.Count() > 0)
                            //        {
                            //            lstForGrid = resForGrid.ToList();
                            //            TempData["CheckVal"] = lstForGrid;
                            //        }
                            //    }
                            //    else
                            //    {
                            //        //return lst[0].errmsg;
                            //    }
                            //}
                        }
                    }
                    else //DD Cancellation
                    {
                        using (var cmd = new SqlCommand("INSERT INTO DD_Cancellation_temp(Refid,report_generation_date,effective_date,reference,payer_name,payer_account_number,payer_sort_code,reason_code)VALUES('" + guiId + "',@report_generation_date,@effective_date,@reference,@payer_name,@payer_account_number,@payer_sort_code,@reason_code)", con))
                        {
                            cmd.Parameters.Add("@report_generation_date", SqlDbType.VarChar);
                            cmd.Parameters.Add("@effective_date", SqlDbType.VarChar);
                            cmd.Parameters.Add("@reference", SqlDbType.VarChar);
                            cmd.Parameters.Add("@payer_name", SqlDbType.VarChar);
                            cmd.Parameters.Add("@payer_account_number", SqlDbType.VarChar);
                            cmd.Parameters.Add("@payer_sort_code", SqlDbType.VarChar);
                            cmd.Parameters.Add("@reason_code", SqlDbType.VarChar);

                            foreach (var Itemvalue in ListDDModel)
                            {
                                cmd.Parameters["@report_generation_date"].Value = Itemvalue.report_generation_date;
                                cmd.Parameters["@effective_date"].Value = Itemvalue.effective_date;
                                cmd.Parameters["@reference"].Value = Itemvalue.reference;
                                cmd.Parameters["@payer_name"].Value = Itemvalue.payer_name;
                                cmd.Parameters["@payer_account_number"].Value = Itemvalue.payer_account_number;
                                cmd.Parameters["@payer_sort_code"].Value = Itemvalue.payer_sort_code;
                                cmd.Parameters["@reason_code"].Value = Itemvalue.reason_code;
                                int rowsAffected = cmd.ExecuteNonQuery();
                            }
                        }
                        using (var cmd = new SqlCommand("INSERT INTO DD_Cancellation_upload_params(Upload_Count,Upload_XLS_Filename,Upload_By_User)VALUES(@Upload_Count,@Upload_XLS_Filename,@Upload_By_User)", con))
                        {
                            cmd.Parameters.Add("@Upload_Count", SqlDbType.Int);
                            cmd.Parameters.Add("@Upload_XLS_Filename", SqlDbType.VarChar);
                            cmd.Parameters.Add("@Upload_By_User", SqlDbType.VarChar);

                            cmd.Parameters["@Upload_Count"].Value = ListDDModel.Count;
                            cmd.Parameters["@Upload_XLS_Filename"].Value = file.FileName;
                            cmd.Parameters["@Upload_By_User"].Value = CRM_API.Helpers.Session.CurrentUser.user_login;
                            int rowsAffected = cmd.ExecuteNonQuery();
                        }
                        using (var Conn1 = new SqlConnection(CRM_API.Helpers.Config.MCMCRMConnection))
                        {
                            Conn1.Open();
                            var results = Conn1.Query<ManualMarkingXLSUploadResult>(
                                    "populate_DD_Cancellation_upload",
                                    new
                                    {
                                        @ref_id = guiId
                                    }, commandType: CommandType.StoredProcedure);
                            ManualMarkingXLSUploadResult objResult1 = new ManualMarkingXLSUploadResult();
                            List<ManualMarkingXLSUploadResult> lst1 = results.ToList();
                            return Redirect("/PMBO/TabUploadNUpdateFailures?id=" + lst1[0].id + "&CollType=DD&FName=" + file.FileName);
                        }
                    }
                }
            }
            return RedirectToAction("TabUploadNUpdateFailures", "PMBO");
        }

        public static SelectListItem CreateListItem(string text, string value)
        {
            SelectListItem item = new SelectListItem();

            item.Text = text;
            item.Value = value;

            return item;
        }

        //
        // POST: /Customer/Search
        public ActionResult PMBOCustomer()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    HttpContext.Session["Finance_TAB"] = "PMBO";
                    return View();
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

        ////Reports
        //public ActionResult Reports()
        //{
        //    string[] stringArray = { "2013,2014"};
        //    string str = "PMBOreport/Test";
        //    if (HttpContext.Session["GlobalProduct"] != null)
        //    {
        //    //    CRM_API.Models.ReportingServicesReportViewModel model = new CRM_API.Models.ReportingServicesReportViewModel(str, new List<Microsoft.Reporting.WebForms.ReportParameter>()
        //    //{ 
        //    //    new Microsoft.Reporting.WebForms.ReportParameter("Year", stringArray,false) });
        //    //    return View("Reports", model);
        //    }
        //    else
        //    {
        //        HttpContext.Session["GlobalProduct"] = "VMUK";
        //        ViewBag._sitecode = "MCM";
        //        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
        //    }
        //    return View();
        //}

        //Reports

        public ActionResult Reports()
        {

            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    HttpContext.Session["GlobalProduct"] = "VMUK";
                }
                else
                {
                    var page = new PMBOPageModel();
                    try
                    {

                        if (HttpContext.Session["GlobalProduct"].ToString() == "VMFR")
                        {
                            ViewBag._sitecode = "MFR";
                            ViewBag._productcode = "VMFR";
                            HttpContext.Session["GlobalProduct"] = "VMFR";
                        }
                        else if (HttpContext.Session["GlobalProduct"].ToString() == "VMAT")
                        {
                            ViewBag._sitecode = "BAU";
                            ViewBag._productcode = "VMAT";
                            HttpContext.Session["GlobalProduct"] = "VMAT";
                        }
                        else if (HttpContext.Session["GlobalProduct"].ToString() == "VMNL")
                        {
                            ViewBag._sitecode = "BNL";
                            ViewBag._productcode = "VMNL";
                            HttpContext.Session["GlobalProduct"] = "VMNL";
                        }
                        else
                        {
                            ViewBag._sitecode = "MCM";
                            ViewBag._productcode = "VMUK";
                            HttpContext.Session["GlobalProduct"] = "VMUK";
                        }
                        if (Session["hdnFlag"] != null)
                        {

                            ViewBag.hdnFlag = Session["hdnFlag"].ToString();
                        }
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }
                }
            }
            else
            {
                HttpContext.Session["GlobalProduct"] = "VMUK";
                ViewBag._sitecode = "MCM";
                ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
            }
            return View();
        }

        public ActionResult ReportsViewer(ReportingServicesReportViewModel test)
        {

            Session["rptName"] = test.ddlReportList;
            //Session["EnterText"] = test.EnterText;
            Session["ddlApplyLayout"] = test.ddlApplyLayout;
            Session["ddMonth"] = test.ddMonth;
            Session["ddPlanType"] = test.ddPlanType;
            Session["ddYear"] = test.ddYear;
            Session["ddreportType"] = test.ddreportType;

            return View("~/Views/PMBO/Rldc.ascx");
        }

        //Modified by MB for Improvement : Jira CRMT-112
        [HttpPost]
        public ActionResult drpfill(string rptType)
        {

            List<SelectListItem> repValues = new List<SelectListItem>();

            if (rptType == "DDFailure")
            {
                repValues.Add(CreateListItem("Not Applicable", "00"));
            }
            else if (rptType == "AccountStatus")
            {
                repValues.Add(CreateListItem("Suspended", "1"));
                repValues.Add(CreateListItem("Closed", "2"));
                repValues.Add(CreateListItem("Active", "3"));
                repValues.Add(CreateListItem("Excess blocked", "4"));
                repValues.Add(CreateListItem("Inactive", "17"));
                repValues.Add(CreateListItem("Not dispatched", "18"));
                repValues.Add(CreateListItem("Closing", "19"));
                repValues.Add(CreateListItem("Unknown ", "20"));
                repValues.Add(CreateListItem("All", "0"));
            }
            else if (rptType == "PayMSub")
            {
                repValues.Add(CreateListItem("New Customer", "6"));
                repValues.Add(CreateListItem("PayG to PayM Conversion", "7"));
                repValues.Add(CreateListItem("All", "0"));
            }
            else if (rptType == "PlanChange")
            {
                repValues.Add(CreateListItem("Upgrade Count", "8"));
                repValues.Add(CreateListItem("Downgrade Count", "9"));
            }
            else if (rptType == "RevenueSummary" || rptType == "PaymentFailed")
            {
                repValues.Add(CreateListItem("Direct Debit", "10"));
                repValues.Add(CreateListItem("Credit Card", "11"));
            }
            else if (rptType == "PaymentCollected")
            {
                repValues.Add(CreateListItem("Interim Payment Details", "12"));
                repValues.Add(CreateListItem("Payment Attempt by CC post DD-Failures-Success", "13"));
                repValues.Add(CreateListItem("Payment Attempt by CC post DD-Failures-Failures", "14"));
            }
            else if (rptType == "Utlilization")
            {
                repValues.Add(CreateListItem("80% Credit Limit Threshold Usage Customer Report", "15"));
                repValues.Add(CreateListItem("100% Credit Limit Threshold Usage Customer Report", "16"));
            }
            else
            {
                repValues.Add(CreateListItem("Select", "17"));
            }

            return Json(new SelectList(repValues, "Text", "Value"));
        } 
        
        ////Report RDL
        //public ActionResult TabManualMarking(PMBOPageModel model)
        //{
        //    model.ActiveTab = "testFirst";
        //    if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
        //    { HttpContext.Session["GlobalProduct"] = "VMUK"; }
        //    switch (model.ActiveTab)
        //    {
        //        case "TabUploadNUpdateFailures":
        //            return Redirect("/PMBO/testFirst/");
        //        case "TabViewNProcessFailures":
        //            return Redirect("/PayMonthly/TabViewNProcessFailures/");

        //    }
        //    return View();
        //}

        //public ActionResult TabUploadNUpdateFailures()
        //{

        //    var page = new PMBOPageModel();
        //    try
        //    {
        //        page.ActiveTab = "TabUploadNUpdateFailures";
        //        ViewBag._sitecode = "MCM";
        //        ViewBag._productcode = "VMUK";
        //        HttpContext.Session["GlobalProduct"] = "VMUK";
        //    }
        //    catch (Exception ex)
        //    {
        //        page.ErrMsg = ex.Message;
        //    }
        //    return View("TabUploadNUpdateFailures", page);
        //}      

        public ActionResult TabDDgoSubmit()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                { HttpContext.Session["GlobalProduct"] = "VMUK"; }
                else
                {
                    var page = new PMBOPageModel();
                    try
                    {
                        if (HttpContext.Session["GlobalProduct"].ToString() == "VMFR")
                        {
                            ViewBag._sitecode = "MFR";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else if (HttpContext.Session["GlobalProduct"].ToString() == "VMAT")
                        {
                            ViewBag._sitecode = "BAU";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else if (HttpContext.Session["GlobalProduct"].ToString() == "VMNL")
                        {
                            ViewBag._sitecode = "BNL";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else
                        {
                            ViewBag._sitecode = "MCM";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }
                }
            }
            else
            {
                HttpContext.Session["GlobalProduct"] = "VMUK";
                ViewBag._sitecode = "MCM";
                ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
            }
            return View();
        }
        public ActionResult TabDDgoReceive()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                { HttpContext.Session["GlobalProduct"] = "VMUK"; }
                else
                {
                    var page = new PMBOPageModel();
                    try
                    {
                        if (HttpContext.Session["GlobalProduct"].ToString() == "VMFR")
                        {
                            ViewBag._sitecode = "MFR";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else if (HttpContext.Session["GlobalProduct"].ToString() == "VMAT")
                        {
                            ViewBag._sitecode = "BAU";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else if (HttpContext.Session["GlobalProduct"].ToString() == "VMNL")
                        {
                            ViewBag._sitecode = "BNL";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else
                        {
                            ViewBag._sitecode = "MCM";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }
                }
            }
            else
            {
                HttpContext.Session["GlobalProduct"] = "VMUK";
                ViewBag._sitecode = "MCM";
                ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
            }
            return View();
        }
        public ActionResult TabDDgoReject()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                { HttpContext.Session["GlobalProduct"] = "VMUK"; }
                else
                {
                    var page = new PMBOPageModel();
                    try
                    {
                        if (HttpContext.Session["GlobalProduct"].ToString() == "VMFR")
                        {
                            ViewBag._sitecode = "MFR";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else if (HttpContext.Session["GlobalProduct"].ToString() == "VMAT")
                        {
                            ViewBag._sitecode = "BAU";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else if (HttpContext.Session["GlobalProduct"].ToString() == "VMNL")
                        {
                            ViewBag._sitecode = "BNL";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                        else
                        {
                            ViewBag._sitecode = "MCM";
                            ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        }
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }
                }
            }
            else
            {
                HttpContext.Session["GlobalProduct"] = "VMUK";
                ViewBag._sitecode = "MCM";
                ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
            }
            return View();
        }
    }
}
