﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRM_API.DB.Auth;
using CRM_API.Models;

namespace CRM3.WebApp.Controllers
{
    public class AuthController : Controller
    {
        //public ActionResult Login(LoginPage model)
        //{
        //    var page = new LoginPage();

        //    try
        //    {
        //        if (Request.QueryString["No"].ToString() != String.Empty)
        //        {
        //            var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(Request.QueryString["No"].ToString());
        //            CRM_API.Models.CustomerSearchResult resHistoryValues = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, Request.QueryString["No"].ToString(), "");
        //            var TempRes = resHistoryValues;
        //            CRM_API.Models.CustomerSearchResult resAccountInfo = CRM_API.DB.Customer.SProc.CustomerPersonalInfo(_product.mundio_product, Request.QueryString["No"].ToString(), _product.sitecode);
        //            string CusType = resHistoryValues.bill_type.ToString();
        //            var result = SProc.Login("CallCenter", "Agent@123");

        //            if (result.errcode != 0)
        //                throw new Exception(result.errmsg);

        //            // Setup the token
        //            CRM_API.Helpers.Cookie.Token = result.token;
        //            HttpContext.Session["GlobalProduct"] = _product.mundio_product;//Request.QueryString["No"].ToString();
        //            HttpContext.Session["Finance_TAB"] = "CRM";
        //            if (CRM_API.Helpers.Session.NoCurrentUser)
        //            {
        //                CRM_API.Helpers.Session.CurrentUser = SProc.GetSessionInfo(result.token);
        //                CRM_API.Helpers.Session.SiteConnection = CRM_API.DB.Country.CountrySProc.GetConnection(CRM_API.Helpers.Session.CurrentUser.site_code);
        //            }

        //            var user = SProc.GetUserIDByToken(result.token);
        //            CRM_API.Helpers.Session.CurrentRoles = SProc.GetRolesByUserID(user.user_id);
        //            if (CusType == "PAYM")
        //            {
        //                return Redirect(string.Format("/PAYM/TabGeneral/{0}", Request.QueryString["No"].ToString()));
        //            }
        //            else if (CusType == "PAYG")
        //            {
        //                return Redirect(string.Format("/Customer/TabGeneral/{0}", Request.QueryString["No"].ToString()));
        //            }
        //            else
        //            {
        //                return View("Login", page);
        //            }
        //        }
        //        else
        //        {
        //            //set default values
        //            page.LoginName = model.LoginName;
        //            page.ReturnUrl = model.ReturnUrl;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        //page.ErrMsg = ex.Message;
        //    }
        //    return View(page);
        //}
        public ActionResult Login(LoginPage model)
        {
            var page = new LoginPage();

            try
            {
                if (Request.QueryString["No"].ToString() != String.Empty)
                {
                    var _product = CRM_API.Helpers.Session.GetProductByMobile != null ? CRM_API.Helpers.Session.GetProductByMobile : CRM_API.DB.Product.ProductSProc.GetProductByMobile(Request.QueryString["No"].ToString());
                    CRM_API.Models.CustomerSearchResult resHistoryValues = CRM_API.DB.Customer.SProc.CustomerDetailInfo(_product.mundio_product, Request.QueryString["No"].ToString(), "");
                    var TempRes = resHistoryValues;
                    CRM_API.Models.CustomerSearchResult resAccountInfo = CRM_API.DB.Customer.SProc.CustomerPersonalInfo(_product.mundio_product, Request.QueryString["No"].ToString(), _product.sitecode);
                    string CusType = resHistoryValues.bill_type.ToString();
                    var result = SProc.Login("CallCenter", "Agent@123");

                    if (result.errcode != 0)
                        throw new Exception(result.errmsg);

                    // Setup the token
                    CRM_API.Helpers.Cookie.Token = result.token;
                    HttpContext.Session["GlobalProduct"] = _product.mundio_product;//Request.QueryString["No"].ToString();
                    HttpContext.Session["Finance_TAB"] = "CRM";
                    if (CRM_API.Helpers.Session.NoCurrentUser)
                    {
                        CRM_API.Helpers.Session.CurrentUser = SProc.GetSessionInfo(result.token);
                        CRM_API.Helpers.Session.SiteConnection = CRM_API.DB.Country.CountrySProc.GetConnection(CRM_API.Helpers.Session.CurrentUser.site_code);
                    }

                    var user = SProc.GetUserIDByToken(result.token);
                    CRM_API.Helpers.Session.CurrentRoles = SProc.GetRolesByUserID(user.user_id);
                    //Modified for astra 
                    CRM_API.Helpers.Session.CurrentUserPermission = SProc.GetPermission(CRM_API.Helpers.Session.CurrentUser.userid);
                    if (CusType == "PAYM")
                    {
                        //return Redirect(string.Format("/PAYM/TabGeneral/{0}", Request.QueryString["No"].ToString()));
                        return Redirect(string.Format("/PAYM/TabPOverview/{0}-{1}", Request.QueryString["No"].ToString(), CusType));
                    }
                    else if (CusType == "PAYG")
                    {
                        //return Redirect(string.Format("/Customer/TabGeneral/{0}", Request.QueryString["No"].ToString()));
                        return Redirect(string.Format("/Customer/TabCOverview/{0}-{1}", Request.QueryString["No"].ToString(), CusType));
                    }
                    else
                    {
                        return View("Login", page);
                    }
                }
                else
                {
                    //set default values
                    page.LoginName = model.LoginName;
                    page.ReturnUrl = model.ReturnUrl;
                }
            }
            catch (Exception ex)
            {
                //page.ErrMsg = ex.Message;
            }
            return View(page);
        }

        [HttpPost]
        public ActionResult DoLogin(LoginPage model)
        {
            var page = new LoginPage();

            try
            {
                //set default values
                page.LoginName = model.LoginName;
                page.ReturnUrl = model.ReturnUrl;

                //do login
                //var result = SProc.Login(model.LoginName, model.Password, model.Country); // Old Version mark
                var result = SProc.Login(model.LoginName, model.Password);

                if (result.errcode != 0)
                    throw new Exception(result.errmsg);

                // Setup the token
                CRM_API.Helpers.Cookie.Token = result.token;

                //redirect
                if (Url.IsLocalUrl(model.ReturnUrl))
                    return Redirect(model.ReturnUrl);
                else
                    return RedirectToAction("Index", "Product"); 
            }
            catch (Exception ex)
            {
                page.ErrMsg = ex.Message;
            }

            return View("Login", page);
        }

        public ActionResult Logoff()
        {
            var page = new LoginPage();

            try
            {
                var result = SProc.Logoff(CRM_API.Helpers.Cookie.Token);
                CRM_API.Helpers.Cookie.Token = "";
                CRM_API.Helpers.Session.CurrentUser = null;
                CRM_API.Helpers.Session.SiteConnection = null;
            }
            catch
            {
                //do nothing
            }

            return RedirectToAction("Index", "Home");
        }
    }
}
