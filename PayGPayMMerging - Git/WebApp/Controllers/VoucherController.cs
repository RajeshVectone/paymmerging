﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRM3.WebApp.Attributes;

namespace CRM3.WebApp.Controllers
{
    [CRM3Auth]
    public class VoucherController : Controller
    {
        //
        // GET: /Voucher/

        public ActionResult Index()
        {
            return RedirectToAction("Checker");
        }

        public ActionResult Checker()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    return View();
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }

    }
}
