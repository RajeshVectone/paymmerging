﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRM3.WebApp.Controllers
{
    //10-Aug-2015 : Moorthy Added to show Pending List screen without functionaility
    public class PaymentController : Controller
    {
        //
        // GET: /Payment/

        public ActionResult Index()
        {
            return View();
        }

        //
        //Pending List
        public ActionResult PendingList()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    if (HttpContext.Session["GlobalProduct"].ToString() == "VMFR")
                    {
                        ViewBag._sitecode = "MFR";
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    }
                    else if (HttpContext.Session["GlobalProduct"].ToString() == "VMAT")
                    {
                        ViewBag._sitecode = "BAU";
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    }
                    else if (HttpContext.Session["GlobalProduct"].ToString() == "VMNL")
                    {
                        ViewBag._sitecode = "BNL";
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    }
                    else if (HttpContext.Session["GlobalProduct"].ToString() == "VMBE")
                    {
                        ViewBag._sitecode = "MBE";
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    }
                    else
                    {
                        ViewBag._sitecode = "MCM";
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    }

                    return View();
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }
    }
}
