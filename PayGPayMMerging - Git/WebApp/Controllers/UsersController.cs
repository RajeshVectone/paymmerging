﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRM_API.Models.Permission;
using CRM3.WebApp.UserRoleEntity;
using System.Text;
using CRM3.WebApp.Attributes;
using System.Data.Entity;
using System.Data;
using Newtonsoft.Json;
using NLog;
using CRM_API.DB.Auth;
using CRM_API.Models;

namespace CRM3.WebApp.Controllers
{
    [CRM3Auth]
    public class UsersController : Controller
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public UserRolePermissionsEntities _userRolePerObject = new UserRolePermissionsEntities();
        // GET: /Users/

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult UserList()
        {
            CRM_API.Models.Permission.User data = new CRM_API.Models.Permission.User();
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    return View(data);
                    //return View("TabUserList");
                }
            }
            else
            {
                return RedirectToAction("Index", "Product");
            }
        }
        //Get GetUserPermissionlist 
        public JsonResult GetUserPermissionlist(CRM_API.Models.Permission.User _user)
        {
            List<CRM_API.Models.Permission.User> getpermission = new List<CRM_API.Models.Permission.User>();
            List<CRM_API.Models.Permission.PermissionRecord> getpermission1 = new List<CRM_API.Models.Permission.PermissionRecord>();


            //  CRM3.WebApp.UserRoleEntity.User getpermission = new UserRoleEntity.User();
            var data = (from s in _userRolePerObject.Users where s.Id == _user.Id select s).ToList();
            var permmissionRecords = data[0].PermissionRecords;
            var permmissionCheck = permmissionRecords.Select(x => x.Id);

            //foreach (var perinfo in data)
            //{
            //    getpermission.Add(new CRM_API.Models.Permission.User
            //    {

            //        Id = perinfo.Id,
            //        Username = perinfo.Username,
            //        FirstName = perinfo.FirstName,
            //        LastName = perinfo.LastName,
            //        Password = perinfo.Password,
            //        Email = perinfo.Email,
            //        errcode = 0,
            //        errmsg = "User Info Updated Sucessfully",
            //        errsubject = "General Info"

            //    });
            //    foreach (var permission in perinfo.PermissionRecords)
            //    {
            //        getpermission.ElementAt(getpermission.Count - 1).PermissionRecords.Add(new CRM_API.Models.Permission.PermissionRecord
            //        {
            //            Id = permission.Id,
            //            Name = permission.Name,
            //            SystemName = permission.SystemName,
            //            Category = permission.Category
            //        });
            //    }
            //    //   var x =new CRM_API.Models.Permission.User
            //    //    {

            //    //        Id = perinfo.Id,
            //    //        Username = perinfo.Username,
            //    //        errcode = 0,



            //    //    };
            //    //   x.PermissionRecords.Add(
            //}

            var jsonValue = JsonConvert.SerializeObject(permmissionCheck);
            return Json(jsonValue);


        }
        public JsonResult BasicinfoEdit(CRM_API.Models.Permission.User _user)
        {
            List<CRM_API.Models.Permission.User> getpermission = new List<CRM_API.Models.Permission.User>();
            var data = (from s in _userRolePerObject.Users where s.Id == _user.Id select s).ToList();
            foreach (var perinfo in data)
            {
                getpermission.Add(new CRM_API.Models.Permission.User
                {

                    Id = perinfo.Id,
                    Username = perinfo.Username,
                    FirstName = perinfo.FirstName,
                    LastName = perinfo.LastName,
                    Password = perinfo.Password,
                    Email = perinfo.Email,
                    errcode = 0,
                    errmsg = "User Info Updated Sucessfully",
                    errsubject = "General Info",
                    Active = perinfo.Active,
                    role_id = perinfo.role_id == null ? 1 : Convert.ToInt32(perinfo.role_id),
                    AssignEmails = perinfo.assign_email == null ? "1" : Convert.ToString(perinfo.assign_email)
                });
            }
            return Json(getpermission);
        }
        //get permission list
        public JsonResult GetPermissionlist()
        {
            List<CRM_API.Models.Permission.PermissionRecord> getpermission = new List<CRM_API.Models.Permission.PermissionRecord>();
            var data = (from s in _userRolePerObject.PermissionRecords select new { s.Id, s.Name, s.SystemName, s.Category }).ToList();

            foreach (var perinfo in data)
            {
                getpermission.Add(new CRM_API.Models.Permission.PermissionRecord
                {
                    Id = perinfo.Id,
                    Name = perinfo.Name,
                    SystemName = perinfo.SystemName,
                    Category = perinfo.Category,
                    errcode = 0,
                    errmsg = "Get User Permission",
                    errsubject = "General Info"

                });
            }
            return Json(getpermission);


        }
        //Display User information list
        [HttpPost]
        public JsonResult getuserdetails()
        {
            try
            {
                Log.Debug("Get User information list ");
                List<CRM_API.Models.Permission.User> getuser = new List<CRM_API.Models.Permission.User>();
                var data = (from s in _userRolePerObject.Users where !s.Deleted == true select new { s.Username, s.FirstName, s.LastName, s.LastLoginDateUtc, s.Active, s.Email, s.Id, s.CreatedOnUtc, s.assign_email }).ToList();
                foreach (var userinfo in data)
                {
                    getuser.Add(new CRM_API.Models.Permission.User
                    {
                        Id = userinfo.Id,
                        Username = userinfo.Username,
                        FirstName = userinfo.FirstName,
                        LastName = userinfo.LastName,
                        FullName = userinfo.FirstName + " " + userinfo.LastName,
                        LastLoginDateUtc = userinfo.LastLoginDateUtc,
                        LastLoginDate_str = Convert.ToString(userinfo.LastLoginDateUtc),
                        Email = userinfo.Email,
                        CreatedOnUtc = userinfo.CreatedOnUtc,
                        Status = (userinfo.Active == true) ? "Active" : "Inactive",
                        AssignEmails = userinfo.assign_email == null ? "1" : Convert.ToString(userinfo.assign_email)
                    });
                }
                return Json(getuser);
            }
            catch (Exception e)
            {
                Log.Debug("Exception");
                Log.Error(e);
            }
            return Json("No data");
        }

        //Insert user information from table 
        [HttpPost]
        public JsonResult NewUserInsert(CRM_API.Models.Permission.User _user)
        {
            try
            {
                string permission = _user.PermissionList;
                permission = permission.TrimEnd(',');
                CRM_API.Models.CustomerSearchResult NewuserInsert = CRM_API.DB.Customer.SProc.NewUserInsert
                    (
                    _user.Id,
                    _user.Username,
                    _user.Password,
                    _user.Status,
                    _user.AssignEmails,
                    _user.FirstName,
                    _user.LastName,
                    _user.Email,
                    _user.Username,
                    permission,
                    _user.role_id);

                if (NewuserInsert != null)
                {
                    _user.errcode = NewuserInsert.errcode;
                    _user.errmsg = NewuserInsert.errmsg;
                    _user.errsubject = "General Information";
                    return Json(_user);
                }
                else
                {
                    _user.errcode = -1;
                    _user.errmsg = "New User Creation Having Some Problem...";
                    _user.errsubject = "General Information";
                    return Json(_user);
                }
            }
            catch (Exception ex)
            {
                Log.Error("NewUserInsert : " + ex.Message, ex);
                _user.errcode = -1;
                _user.errmsg = ex.Message;
                _user.errsubject = "General Information";
                return Json(_user);
            }
        }

        //Delete User Info  Deactivate
        [HttpPost]
        public JsonResult DeleteUserRecord(CRM_API.Models.Permission.User _user)
        {
            var data = (from s in _userRolePerObject.Users where s.Id == _user.Id select s).ToList();
            foreach (var ss in data)
            {
                ss.Deleted = true;
                ss.CreatedOnUtc = ss.CreatedOnUtc;
                ss.LastLoginDateUtc = ss.LastLoginDateUtc;
                ss.LastActivityDateUtc = ss.LastActivityDateUtc;
                _userRolePerObject.Entry(ss).State = EntityState.Modified;
            }
            _userRolePerObject.SaveChanges();
            _user.errcode = 0;
            _user.errmsg = "Recorded Deleted sucessfully";
            _user.errsubject = "General Information";
            return Json(_user);



            //CRM3.WebApp.UserRoleEntity.PermissionRecord permission = _userRolePerObject.PermissionRecords.Find(_user.Id);
            //_userRolePerObject.PermissionRecords.Remove(permission);
            //_userRolePerObject.SaveChanges();
            //CRM3.WebApp.UserRoleEntity.User data = _userRolePerObject.Users.Find(_user.Id);
            //_userRolePerObject.Users.Remove(data);
            //_userRolePerObject.SaveChanges();
            //_user.errcode = 0;
            //_user.errmsg = "Recorded Deleted sucessfully";
            //_user.errsubject = "General Information";
            //return Json(_user);
        }
        //Deactivate User Info  
        [HttpPost]
        public JsonResult Deactivate(CRM_API.Models.Permission.User _user)
        {
            var data = (from s in _userRolePerObject.Users where s.Id == _user.Id select s).ToList();
            foreach (var ss in data)
            {
                ss.Active = false;
                ss.CreatedOnUtc = ss.CreatedOnUtc;
                ss.LastLoginDateUtc = ss.LastLoginDateUtc;
                ss.LastActivityDateUtc = ss.LastActivityDateUtc;
                _userRolePerObject.Entry(ss).State = EntityState.Modified;
            }
            _userRolePerObject.SaveChanges();
            _user.errcode = 0;
            _user.errmsg = "User Deactivted sucessfully";
            _user.errsubject = "General Information";
            return Json(_user);
        }
        //Reactive User Info  
        [HttpPost]
        public JsonResult Reactive(CRM_API.Models.Permission.User _user)
        {
            var data = (from s in _userRolePerObject.Users where s.Id == _user.Id select s).ToList();
            foreach (var ss in data)
            {
                ss.Active = true;
                ss.CreatedOnUtc = ss.CreatedOnUtc;
                ss.LastLoginDateUtc = ss.LastLoginDateUtc;
                ss.LastActivityDateUtc = ss.LastActivityDateUtc;
                _userRolePerObject.Entry(ss).State = EntityState.Modified;
            }
            _userRolePerObject.SaveChanges();
            _user.errcode = 0;
            _user.errmsg = "User  Reactived sucessfully";
            _user.errsubject = "General Information";
            return Json(_user);
        }
        private dynamic Getuserinfo()
        {
            List<CRM_API.Models.Permission.User> getuser = new List<CRM_API.Models.Permission.User>();
            var data = (from s in _userRolePerObject.Users where !s.Deleted == true select new { s.Username, s.FirstName, s.LastName, s.LastLoginDateUtc, s.Active, s.Email, s.Id }).ToList();
            foreach (var userinfo in data)
            {
                getuser.Add(new CRM_API.Models.Permission.User
                {
                    Username = userinfo.Username,
                    FirstName = userinfo.FirstName,
                    LastName = userinfo.LastName,
                    FullName = userinfo.FirstName + " " + userinfo.LastName,
                    LastLoginDateUtc = userinfo.LastLoginDateUtc,
                    LastLoginDate_str = Convert.ToString(userinfo.LastLoginDateUtc),
                    Email = userinfo.Email,
                    Status = (userinfo.Active == true) ? "Active" : "Inactive"
                });
            }
            return (getuser);
        }
        //Download the user information
        [HttpPost]
        public ActionResult UsersDownload()
        {
            var result = Getuserinfo();
            var HistoryName = "";
            StringBuilder str = new StringBuilder();
            str.Append("<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv=\"content-type\" content=\"text/plain; charset=UTF-8\"/></head><body>");
            str.Append("<head><style>.xlLongDate{mso-number-format:'dd-mm-yyyy hh:mm:ss AM/PM';}.xlText{mso-number-format:'@';}</style></head>");
            HistoryName = "User History";
            str.Append("<h1>User  History </h1>");
            UserHistoryDowload(result, ref str);
            str.Append("</html>");
            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=" + HistoryName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(str.ToString());
            string _random = Guid.NewGuid().ToString();
            TempData[_random] = str.ToString();
            var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = 0, errmsg = HistoryName, Text = _random, };
            return Json(resy, JsonRequestBehavior.AllowGet);
        }

        //Html template for download
        private static void UserHistoryDowload(dynamic result, ref StringBuilder str)
        {
            str.Append("<table>");
            str.Append("<thead>");
            str.Append("<tr>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>User Name</th>");
            str.Append("<th  style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Full Name</th>");
            str.Append("<th  style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Email</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Status</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Last Login Date</th>");

            str.Append("</tr>");
            str.Append("</thead>");
            str.Append("<tbody>");
            foreach (CRM_API.Models.Permission.User val in (result as IEnumerable<CRM_API.Models.Permission.User>))
            {
                str.Append("<tr>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Username as string) ? "" : (val.Username.ToString())) + "</td>");
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.FullName as string) ? "" : (val.FullName.ToString())) + "</td>");
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Email as string) ? "" : (val.Email.ToString())) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Status as string) ? "" : (val.Status.ToString())) + "</td>");

                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.LastLoginDate_str as string) ? "" : (val.LastLoginDate_str.ToString())) + "</td>");
                str.Append("</tr>");
            }
            str.Append("</tbody>");
            str.Append("</table>");
            str.Append("</body>");
        }
        [HttpGet]
        public FileContentResult GoDownloaduserHistory(string id, string historyName)
        {
            var result = (string)TempData[id];
            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=" + historyName.Replace(" ", "") + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(result.ToString());
            return new FileContentResult(temp, "application/vnd.ms-excel");
        }

        //User Role
        [HttpPost]
        public JsonResult GetUserRole()
        {
            try
            {
                Log.Debug("Get User Role information list ");
                IEnumerable<CRM_API.Models.UserRole> userRole = new List<CRM_API.Models.UserRole>();
                userRole = CRM_API.DB.Customer.SProc.GetUserRoleInfo();
                return Json(userRole);
            }
            catch (Exception e)
            {
                Log.Debug("Exception");
                Log.Error(e);
            }
            return Json("No data");
        }
    }
}
