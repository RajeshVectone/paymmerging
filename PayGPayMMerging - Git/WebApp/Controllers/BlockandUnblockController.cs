﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRM3.WebApp.Attributes;
using CRM_API.Models;
using System.Data;
using System.Data.OleDb;
using System.Configuration;
using System.Data.SqlClient;
using Dapper;
using System.Text;

namespace CRM3.WebApp.Controllers
{
    [CRM3Auth]
    public class BlockandUnblockController : Controller
    {
        //
        // GET: /LLOTGSearch/

        public ActionResult Index()
        {
            return View();
        }


        public ActionResult TabLsearchblockandunblock(String id, String value, String city, String areaid, String pro)
        {
            ViewBag.LoginName = string.IsNullOrEmpty(CRM_API.Helpers.Session.CurrentUser.first_name) ?
                                CRM_API.Helpers.Session.CurrentUser.user_login :
                                string.Format("{0} {1}", CRM_API.Helpers.Session.CurrentUser.first_name, CRM_API.Helpers.Session.CurrentUser.last_name);

            if (TempData["Country"] != null)
            {
                String country = TempData["Country"].ToString();
                ViewBag.CityCountry = country;
                TempData["Country"] = country;
            }
            if (value != null)
            {
                TempData["Country"] = value;
            }
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    HttpContext.Session["GlobalProduct"] = "VMUK";
                }
                else
                {
                    try
                    {
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = "MCM";
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
            else
            {
                HttpContext.Session["GlobalProduct"] = "VMUK";
                ViewBag._sitecode = "MCM";
                ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
            }
            return View();

        }

        [HttpPost]
        public ActionResult UploadExcelSheet(HttpPostedFileBase file)
        {
            // string Block_type = Request.Form["HidBlocktype"].ToString();
            DataTable dtdata = new DataTable();


            if (Request.Files["file"] != null)
            {
                if (Request.Files["file"].ContentLength > 0)
                {
                    string fileExtension = System.IO.Path.GetExtension(Request.Files["file"].FileName);

                    if (fileExtension == ".xls" || fileExtension == ".xlsx")
                    {
                        string fileLocation = Server.MapPath("~/Content/") + Request.Files["file"].FileName;
                        if (System.IO.File.Exists(fileLocation))
                        {

                            System.IO.File.Delete(fileLocation);
                        }
                        Request.Files["file"].SaveAs(fileLocation);
                        string excelConnectionString = string.Empty;
                        excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                        //connection String for xls file format.
                        if (fileExtension == ".xls")
                        {
                            excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileLocation + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                        }
                        //connection String for xlsx file format.
                        else if (fileExtension == ".xlsx")
                        {

                            excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                        }
                        //Create Connection to Excel work book and add oledb namespace
                        OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                        excelConnection.Open();
                        DataTable dt = new DataTable();
                        dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                        if (dt == null)
                        {
                            return null;
                        }

                        String[] excelSheets = new String[dt.Rows.Count];
                        int t = 0;
                        //excel data saves in temp file here.
                        foreach (DataRow row in dt.Rows)
                        {
                            excelSheets[t] = row["TABLE_NAME"].ToString();
                            t++;
                        }
                        OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);
                        string query = string.Format("Select * from [{0}]", excelSheets[0]);
                        using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                        {
                            dataAdapter.Fill(dtdata);
                        }
                        excelConnection1.Close();
                        excelConnection.Close();
                    }
                    foreach (DataColumn column in dtdata.Columns)
                    {
                        string name = column.ColumnName;
                        if (name == "ICCID")
                        {
                            /** ICCID value **/
                            string ICCID;
                            StringBuilder striccid = new StringBuilder();
                            for (int i = 0; i < dtdata.Rows.Count; i++)
                            {
                                ICCID = dtdata.Rows[i]["ICCID"].ToString();
                                striccid.Append(ICCID).Append(",");
                            }
                            string tempiccid = striccid.ToString().Substring(0, striccid.Length - 2);
                            TempData["tempiccid"] = tempiccid;

                            // ViewBag.tempiccid = tempiccid;

                            /** ICCID value **/
                        }
                        if (name == "Mobile")
                        {
                            /** Mobile value **/
                            string Mobile;
                            StringBuilder strmobile = new StringBuilder();
                            for (int i = 0; i < dtdata.Rows.Count; i++)
                            {
                                Mobile = dtdata.Rows[i]["Mobile"].ToString();
                                strmobile.Append(Mobile).Append(",");
                            }
                            string tempMobile = strmobile.ToString().Substring(0, strmobile.Length - 2);
                            TempData["tempMobile"] = tempMobile;
                            //ViewBag.tempMobile = tempMobile;
                            /** Mobile value **/

                        }
                    }
                }
            }
            return RedirectToAction("TabLsearchblockandunblock", "BlockandUnblock");
        }

        [HttpPost]
        public ActionResult BlockandUnBlockDownload(CRM_API.Models.BlockUnBlockModels.BlockUnblock In)
        {
            var result = GetBlockandUnBlockHistory_Downloadable(In);
            var HistoryName = "";
            StringBuilder str = new StringBuilder();
            str.Append("<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv=\"content-type\" content=\"text/plain; charset=UTF-8\"/></head><body>");
            str.Append("<head><style>.xlLongDate{mso-number-format:'dd-mm-yyyy hh:mm:ss AM/PM';}.xlText{mso-number-format:'@';}</style></head>");

            var dateFrom = In.Date_from_day_string + "/" + In.Date_from_month_string + "/" + In.Date_from_year_string;
            var dateTo = In.Date_To_day_string + "/" + In.Date_to_month_string + "/" + In.Date_To_year_string;

            if (In.mode == 1)
            {
                HistoryName = "BlockHistory";
                str.Append("<h1>Block  History </h1>");
                str.Append("<h3>Period : " + dateFrom + " - " + dateTo + "</h3>");
                FormatBlockandUnblockHistoryDowload(result, ref str);
            }
            else
            {
                HistoryName = "UnblockHistory";
                str.Append("<h1>UnBlock History </h1>");
                str.Append("<h3>Period : " + dateFrom + " - " + dateTo + "</h3>");
                FormatBlockandUnblockHistoryDowload1(result, ref str);
            }
            str.Append("</html>");
            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=" + HistoryName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(str.ToString());
            string _random = Guid.NewGuid().ToString();
            TempData[_random] = str.ToString();
            var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = 0, errmsg = HistoryName, Text = _random, };
            return Json(resy, JsonRequestBehavior.AllowGet);
        }
        private static void FormatBlockandUnblockHistoryDowload(dynamic result, ref StringBuilder str)
        {
            str.Append("<table>");
            str.Append("<thead>");
            str.Append("<tr>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Referance ID</th>");
            str.Append("<th  style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>CLI</th>");
            str.Append("<th  style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>ICCID</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Brand</th>");

            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>User</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Date and Time</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Master Balance</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Currency</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Reason For Blocking</th>");
            //str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Brand</th>");
            str.Append("</tr>");
            str.Append("</thead>");
            str.Append("<tbody>");
            foreach (CRM_API.Models.BlockUnBlockModels.BlockUnBlockReports val in (result as IEnumerable<CRM_API.Models.BlockUnBlockModels.BlockUnBlockReports>))
            {
                str.Append("<tr>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Batch_Id as string) ? "" : (val.Batch_Id.ToString())) + "</td>");
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Mobile_No as string) ? "" : (val.Mobile_No.ToString())) + "</td>");
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Iccid as string) ? "" : (val.Iccid.ToString())) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.brand as string) ? "" : (val.brand.ToString())) + "</td>");

                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Process_By as string) ? "" : (val.Process_By.ToString())) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Log_Datestring as string) ? "" : (val.Log_Datestring.ToString())) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.master_bal as string) ? "" : (val.master_bal.ToString())) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Currency as string) ? "" : (val.Currency.ToString())) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.reason as string) ? "" : (val.reason.ToString())) + "</td>");
                //str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.brand as string) ? "" : (val.brand.ToString())) + "</td>");
                str.Append("</tr>");
            }
            str.Append("</tbody>");
            str.Append("</table>");
            str.Append("</body>");
        }
        private static void FormatBlockandUnblockHistoryDowload1(dynamic result, ref StringBuilder str)
        {
            str.Append("<table>");
            str.Append("<thead>");
            str.Append("<tr>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Referance ID</th>");
            str.Append("<th  style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>CLI</th>");
            str.Append("<th  style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>ICCID</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Brand</th>");

            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>User</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Date and Time</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Master Balance</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Currency</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Reason For UnBlocking</th>");
            //str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Brand</th>");
            str.Append("</tr>");
            str.Append("</thead>");
            str.Append("<tbody>");
            foreach (CRM_API.Models.BlockUnBlockModels.BlockUnBlockReports val in (result as IEnumerable<CRM_API.Models.BlockUnBlockModels.BlockUnBlockReports>))
            {
                str.Append("<tr>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Batch_Id as string) ? "" : (val.Batch_Id.ToString())) + "</td>");
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Mobile_No as string) ? "" : (val.Mobile_No.ToString())) + "</td>");
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Iccid as string) ? "" : (val.Iccid.ToString())) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.brand as string) ? "" : (val.brand.ToString())) + "</td>");

                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Process_By as string) ? "" : (val.Process_By.ToString())) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Log_Datestring as string) ? "" : (val.Log_Datestring.ToString())) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.master_bal as string) ? "" : (val.master_bal.ToString())) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Currency as string) ? "" : (val.Currency.ToString())) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.reason as string) ? "" : (val.reason.ToString())) + "</td>");
                //str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.brand as string) ? "" : (val.brand.ToString())) + "</td>");
                str.Append("</tr>");
            }
            str.Append("</tbody>");
            str.Append("</table>");
            str.Append("</body>");
        }
        private dynamic GetBlockandUnBlockHistory_Downloadable(CRM_API.Models.BlockUnBlockModels.BlockUnblock In)
        {
            CRM_API.DB.Common.ErrCodeMsg errResult = new CRM_API.DB.Common.ErrCodeMsg()
            {
                errcode = -1,
                errsubject = "General Error",
                errmsg = "Empty New Order"
            };
            try
            {
                switch (In.Block_Type)
                {
                    case CRM_API.Models.BlockUnBlockModels.BlockUnBlockmode.Reports:
                        return GetBlockunblockHistory(In);
                    default: break;
                }
            }
            catch (Exception ex)
            {
                errResult.errmsg = "WEB-API: " + ex.Message;
                errResult.errcode = -1;
            }
            return errResult;
        }
        //CRMT-231 
        private IEnumerable<CRM_API.Models.BlockUnBlockModels.BlockUnBlockReports> GetBlockunblockHistory(CRM_API.Models.BlockUnBlockModels.BlockUnblock In)
        {
            // string pad = string.Empty;
            IEnumerable<CRM_API.Models.BlockUnBlockModels.BlockUnBlockReports> result = CRM_API.DB.BlockUnBlockModels.SProc.Getbothblockandunblockreports(In.product, In.mode, In.StartDate, In.EndDate);
            if (result == null)
                return new List<CRM_API.Models.BlockUnBlockModels.BlockUnBlockReports>().ToArray();
            else if (result.Count() == 0)
                return new List<CRM_API.Models.BlockUnBlockModels.BlockUnBlockReports>().ToArray();
            else
                return result.ToArray();
        }
        [HttpGet]
        public FileContentResult GoDownloadBlockUnblockHistory(string id, string historyName)
        {
            var result = (string)TempData[id];
            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=" + historyName.Replace(" ", "") + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(result.ToString());
            return new FileContentResult(temp, "application/vnd.ms-excel");
        }

        public ActionResult TabLfruadblockandunblock(String id, String value, String city, String areaid, String pro)
        {
            ViewBag.LoginName = string.IsNullOrEmpty(CRM_API.Helpers.Session.CurrentUser.first_name) ?
                                CRM_API.Helpers.Session.CurrentUser.user_login :
                                string.Format("{0} {1}", CRM_API.Helpers.Session.CurrentUser.first_name, CRM_API.Helpers.Session.CurrentUser.last_name);

            if (TempData["Country"] != null)
            {
                String country = TempData["Country"].ToString();
                ViewBag.CityCountry = country;
                TempData["Country"] = country;
            }
            if (value != null)
            {
                TempData["Country"] = value;
            }
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    HttpContext.Session["GlobalProduct"] = "VMUK";
                }
                else
                {
                    try
                    {
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = "MCM";
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
            else
            {
                HttpContext.Session["GlobalProduct"] = "VMUK";
                ViewBag._sitecode = "MCM";
                ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
            }
            return View();

        }
    }
}

