﻿using CRM_API.Models;
using System;
using System.Web;
using System.Web.Mvc;
using CRM3.WebApp.Attributes;

namespace CRM3.WebApp.Controllers
{
    [CRM3Auth]
    public class ReportsController : Controller
    {
        //
        // GET: /Reports/

        public ActionResult Marketing()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    HttpContext.Session["GlobalProduct"] = "VMUK";
                }
                else
                {
                    try
                    {
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = "MCM";
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
            else
            {
                HttpContext.Session["GlobalProduct"] = "VMUK";
                ViewBag._sitecode = "MCM";
                ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
            }
            return View();
        }

        public ActionResult OMarketing()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    HttpContext.Session["GlobalProduct"] = "VMUK";
                }
                else
                {
                    try
                    {
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = "MCM";
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
            else
            {
                HttpContext.Session["GlobalProduct"] = "VMUK";
                ViewBag._sitecode = "MCM";
                ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
            }
            return View();
        }

        public ActionResult Tabfsearchcustomer()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    HttpContext.Session["GlobalProduct"] = "VMUK";
                }
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = "MCM";
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }
                }
            }
            else
            {
                HttpContext.Session["GlobalProduct"] = "VMUK";
                ViewBag._sitecode = "MCM";
                ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
            }
            return View();
        }

        public ActionResult Tabfsearchtranasction()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    HttpContext.Session["GlobalProduct"] = "VMUK";
                }
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        //ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();

                        //ViewBag._sitecode = "MCM";

                        if (HttpContext.Session["GlobalProduct"].ToString() == "DMAT") { ViewBag._sitecode = "BAU"; ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString(); }
                        if (HttpContext.Session["GlobalProduct"].ToString() == "VMAT") { ViewBag._sitecode = "BAU"; ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString(); }
                        if (HttpContext.Session["GlobalProduct"].ToString() == "DMBE") { ViewBag._sitecode = "MBE"; ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString(); }
                        if (HttpContext.Session["GlobalProduct"].ToString() == "VMBE") { ViewBag._sitecode = "MBE"; ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString(); }
                        if (HttpContext.Session["GlobalProduct"].ToString() == "DMDK") { ViewBag._sitecode = "BDK"; ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString(); }
                        if (HttpContext.Session["GlobalProduct"].ToString() == "VMDK") { ViewBag._sitecode = "BDK"; ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString(); }
                        if (HttpContext.Session["GlobalProduct"].ToString() == "DMFI") { ViewBag._sitecode = "MFI"; ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString(); }
                        if (HttpContext.Session["GlobalProduct"].ToString() == "VMFI") { ViewBag._sitecode = "MFI"; ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString(); }
                        if (HttpContext.Session["GlobalProduct"].ToString() == "CTPFR") { ViewBag._sitecode = "FR1"; ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString(); }
                        if (HttpContext.Session["GlobalProduct"].ToString() == "DMFR") { ViewBag._sitecode = "MFR"; ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString(); }
                        if (HttpContext.Session["GlobalProduct"].ToString() == "VMFR") { ViewBag._sitecode = "MFR"; ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString(); }
                        if (HttpContext.Session["GlobalProduct"].ToString() == "DMNL") { ViewBag._sitecode = "BNL"; ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString(); }
                        if (HttpContext.Session["GlobalProduct"].ToString() == "GCM") { ViewBag._sitecode = "GCM"; ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString(); }
                        if (HttpContext.Session["GlobalProduct"].ToString() == "VMNL") { ViewBag._sitecode = "BNL"; ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString(); }
                        //if (HttpContext.Session["GlobalProduct"].ToString() == "DMPL") { ViewBag._sitecode = "MPL"; ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString(); }
                        //if (HttpContext.Session["GlobalProduct"].ToString() == "VMPL") { ViewBag._sitecode = "MPL"; ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString(); }
                        //Modify by karthi Jira:23
                        if (HttpContext.Session["GlobalProduct"].ToString() == "DMPL") { ViewBag._sitecode = "BPL"; ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString(); }
                        if (HttpContext.Session["GlobalProduct"].ToString() == "VMPL") { ViewBag._sitecode = "BPL"; ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString(); }
                        if (HttpContext.Session["GlobalProduct"].ToString() == "DMPT") { ViewBag._sitecode = "MPT"; ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString(); }
                        if (HttpContext.Session["GlobalProduct"].ToString() == "VMPT") { ViewBag._sitecode = "MPT"; ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString(); }
                        if (HttpContext.Session["GlobalProduct"].ToString() == "CTPCH") { ViewBag._sitecode = "CH1"; ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString(); }
                        if (HttpContext.Session["GlobalProduct"].ToString() == "DMSE") { ViewBag._sitecode = "BSE"; ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString(); }
                        if (HttpContext.Session["GlobalProduct"].ToString() == "VMSE") { ViewBag._sitecode = "BSE"; ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString(); }
                        if (HttpContext.Session["GlobalProduct"].ToString() == "CTPUK") { ViewBag._sitecode = "LO2"; ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString(); }
                        if (HttpContext.Session["GlobalProduct"].ToString() == "DMUK") { ViewBag._sitecode = "MCM"; ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString(); }
                        if (HttpContext.Session["GlobalProduct"].ToString() == "VMUK") { ViewBag._sitecode = "MCM"; ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString(); }
                        if (HttpContext.Session["GlobalProduct"].ToString() == "CTPUS") { ViewBag._sitecode = "US1"; ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString(); }
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }
                }
            }
            else
            {
                HttpContext.Session["GlobalProduct"] = "VMUK";
                ViewBag._sitecode = "MCM";
                ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
            }
            return View();
        }

        public ActionResult TabfSMSHistory()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    HttpContext.Session["GlobalProduct"] = "VMUK";
                }
                else
                {
                    var page = new CustomerDetailPage();
                    try
                    {
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = "MCM";
                    }
                    catch (Exception ex)
                    {
                        page.ErrMsg = ex.Message;
                    }
                }
            }
            else
            {
                HttpContext.Session["GlobalProduct"] = "VMUK";
                ViewBag._sitecode = "MCM";
                ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
            }
            return View();
        }

        //View TransactionTabDDOperation
        public ActionResult Tabmarketing()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    HttpContext.Session["GlobalProduct"] = "VMUK";
                }
                else
                {
                    try
                    {
                        ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                        ViewBag._sitecode = "MCM";
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
            else
            {
                HttpContext.Session["GlobalProduct"] = "VMUK";
                ViewBag._sitecode = "MCM";
                ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
            }
            return View();
        }

        //View Complaints
        public ActionResult TabComplaints()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    HttpContext.Session["GlobalProduct"] = "VMUK";
                }
                try
                {
                    ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    ViewBag._sitecode = "MCM";
                    ViewBag._user_role = CRM_API.Helpers.Session.CurrentUser.user_role;
                    ViewBag._userid = CRM_API.Helpers.Session.CurrentUser.userid;
                }
                catch { }
            }
            else
            {
                HttpContext.Session["GlobalProduct"] = "VMUK";
                ViewBag._sitecode = "MCM";
                ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                ViewBag._user_role = CRM_API.Helpers.Session.CurrentUser.user_role;
                ViewBag._userid = CRM_API.Helpers.Session.CurrentUser.userid;
            }
            return View();
        }

        //View Add Service by EXE
        public ActionResult TabBundleService()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    HttpContext.Session["GlobalProduct"] = "VMUK";
                }
                else
                {
                    ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    ViewBag._sitecode = "MCM";
                }
            }
            else
            {
                HttpContext.Session["GlobalProduct"] = "VMUK";
                ViewBag._sitecode = "MCM";
                ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
            }
            return View();
        }

        //SIM Activation History
        public ActionResult TabSIMActivation()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    HttpContext.Session["GlobalProduct"] = "VMUK";
                }
                else
                {
                    ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    ViewBag._sitecode = "MCM";
                }
            }
            else
            {
                HttpContext.Session["GlobalProduct"] = "VMUK";
                ViewBag._sitecode = "MCM";
                ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
            }
            return View();
        }

        //Auto Renewal History
        public ActionResult TabAutoRenewalHistory()
        {
            if (HttpContext.Session["GlobalProduct"] != null)
            {
                if (string.IsNullOrEmpty(HttpContext.Session["GlobalProduct"].ToString()))
                {
                    HttpContext.Session["GlobalProduct"] = "VMUK";
                }
                else
                {
                    ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
                    ViewBag._sitecode = "MCM";
                }
            }
            else
            {
                HttpContext.Session["GlobalProduct"] = "VMUK";
                ViewBag._sitecode = "MCM";
                ViewBag._productcode = HttpContext.Session["GlobalProduct"].ToString();
            }
            return View();
        }
    }
}