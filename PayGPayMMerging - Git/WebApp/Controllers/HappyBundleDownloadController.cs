﻿using System;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRM3.WebApp.Attributes;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Globalization;
using Stimulsoft.Report.Export;
using Stimulsoft.Report;


namespace CRM3.WebApp.Controllers
{
    [CRM3Auth]
    public class HappyBundleDownloadController : Controller
    {
        //CRMT-232  Happy Bundle Dailling number  download
        [HttpPost]
        public ActionResult DownloadHappyBundleDaillingHistory(CRM_API.Models.HappyBundle.HappyBundleSearch In)
        {
            var result = GetHappyBundleDaillingNumberHistory_Downloadable(In);
            var HistoryName = "";
            StringBuilder str = new StringBuilder();
            str.Append("<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv=\"content-type\" content=\"text/plain; charset=UTF-8\"/></head><body>");
            str.Append("<head><style>.xlLongDate{mso-number-format:'dd-mm-yyyy hh:mm:ss AM/PM';}.xlText{mso-number-format:'@';}</style></head>");           
            HistoryName = "HappyBundleDaillingNumber";
            str.Append("<h1>Happy Bundle Dailling Number</h1>");         
            FormatHappyBundleDaillingNumberHistoryDowload(result, ref str);
            str.Append("</html>");
            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=" + HistoryName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(str.ToString());
            string _random = Guid.NewGuid().ToString();
            TempData[_random] = str.ToString();
            var resy = new CRM_API.Models.Common.ErrCodeMsgExt { errcode = 0, errmsg = HistoryName, Text = _random, };
            return Json(resy, JsonRequestBehavior.AllowGet);
        }


        
        [HttpGet]
        public FileContentResult GoDownloadHappyBundleHistory(string id, string historyName)
        {
            var result = (string)TempData[id];
            HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=" + historyName.Replace(" ", "") + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xls");
            this.Response.ContentType = "application/vnd.ms-excel";
            byte[] temp = System.Text.Encoding.UTF8.GetBytes(result.ToString());
            return new FileContentResult(temp, "application/vnd.ms-excel");
        }

       

        //added by karthik Jira:CRMT-231
        private static void FormatHappyBundleDaillingNumberHistoryDowload(dynamic result, ref StringBuilder str)
        {
            str.Append("<table>");
            str.Append("<thead>");
            str.Append("<tr>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Date</th>");
            str.Append("<th  style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Destination Number</th>");
            str.Append("<th  style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Dialing Number</th>");
            str.Append("<th style='border:1px solid #000000;border-width:thin;font-size:10pt;font-weight:bold'>Mode Of Addition</th>");           
            str.Append("</tr>");
            str.Append("</thead>");
            str.Append("<tbody>");
            foreach (CRM_API.Models.HappyBundle.HappyBundlelist val in (result as IEnumerable<CRM_API.Models.HappyBundle.HappyBundlelist>))
            {
                str.Append("<tr>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.startdate_string as string) ? "" : (val.startdate_string.ToString())) + "</td>");
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.destinationNB as string) ? "" : (val.destinationNB.ToString())) + "</td>");
                str.Append("<td class='xlText' style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Mapping_number as string) ? "" : (val.Mapping_number.ToString())) + "</td>");
                str.Append("<td style='border:1px solid #000000;border-width:thin;font-size:10pt;'>" + (string.IsNullOrEmpty(val.Mode as string) ? "" : (val.Mode.ToString())) + "</td>");  
                str.Append("</tr>");
            }
            str.Append("</tbody>");
            str.Append("</table>");
            str.Append("</body>");
        }

       

       
        //CRMT-231 HappyBundleDaillingNumber history 
        private dynamic GetHappyBundleDaillingNumberHistory_Downloadable(CRM_API.Models.HappyBundle.HappyBundleSearch In)
        {
            CRM_API.DB.Common.ErrCodeMsg errResult = new CRM_API.DB.Common.ErrCodeMsg()
            {
                errcode = -1,
                errsubject = "General Error",
                errmsg = "Empty New Order"
            };
            try
            {
                switch (In.Bundle_Type)
                {
                    case CRM_API.Models.HappyBundle.HappyBundleType.HappyBundleList:
                        return GetHappyBubdledaillingnumberHistory(In);
                    default: break;
                }
            }
            catch (Exception ex)
            {
                errResult.errmsg = "WEB-API: " + ex.Message;
                errResult.errcode = -1;
            }
            return errResult;
        }

       
        //CRMT-231 
        private IEnumerable<CRM_API.Models.HappyBundle.HappyBundlelist> GetHappyBubdledaillingnumberHistory(CRM_API.Models.HappyBundle.HappyBundleSearch In)
        {
            // string pad = string.Empty;
            IEnumerable<CRM_API.Models.HappyBundle.HappyBundlelist> result = CRM_API.DB.HappyBundle.SProc.HappyBundleDaillingNumberList(In.msisdn,In.sitecode);
            if (result == null)
                return new List<CRM_API.Models.HappyBundle.HappyBundlelist>().ToArray();
            else if (result.Count() == 0)
                return new List<CRM_API.Models.HappyBundle.HappyBundlelist>().ToArray();
            else
                return result.ToArray();
        }
       
        /* eof Happy Bundle  HISTORY */
        /* --------------------------------------------------------------------------------------------------------------- */

        //Added on 16-03-2015 - JIRA - CRMT - 231
        /* --------------------------------------------------------------------------------------------------------------- */
    }
}
