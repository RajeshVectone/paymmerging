﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using CRM_API.DB.Auth;

namespace CRM3.WebApp.Attributes
{
    public class CRM3AuthAttribute : ActionFilterAttribute
    {
        private IEnumerable<string> roles;

        public CRM3AuthAttribute(params string[] roles)
        {
            this.roles = roles;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                string token = "";
                try
                {
                    token = CRM_API.Helpers.Cookie.Token;
                }
                catch { /* do nothing */ }
                
                /** do basic authentication (check if token is registered) **/
                var user = SProc.GetUserIDByToken(token);
                if (user == null)
                    throw new Exception();

                /** if no user information, get the user session information **/
                if (CRM_API.Helpers.Session.NoCurrentUser)
                {
                    //** Get Permission Id **/
                    CRM_API.Helpers.Session.CurrentUserPermission = SProc.GetPermission(user.user_id);
                    CRM_API.Helpers.Session.CurrentUser = SProc.GetSessionInfo(token);
                    CRM_API.Helpers.Session.SiteConnection = CRM_API.DB.Country.CountrySProc.GetConnection(CRM_API.Helpers.Session.CurrentUser.site_code);
                }

                /** if no role is specified, pass the validation **/
                if (this.roles.Count() == 0)
                    return;

                /** check the allowed roles **/
                CRM_API.Helpers.Session.CurrentRoles = SProc.GetRolesByUserID(user.user_id);

                //primitive loop for speed
                for (var i = 0; i < this.roles.Count(); i++)
                {
                    for (var j = 0; j < CRM_API.Helpers.Session.CurrentRoles.Count(); j++)
                    {
                        if (this.roles.ElementAt(i) == CRM_API.Helpers.Session.CurrentRoles.ElementAt(j).role)
                            return;
                    }
                }

                //if we get this far, something's wrong
                throw new Exception();
            }
            catch
            {
                filterContext.Result = new RedirectResult("/Auth/Login");
            }

        }
    }
}