function TwoInOneTab(){
	var url = 'http://192.168.1.230:1830/api/customer2in1/447451931090';
	$.getJSON(url, function(data){
		
		$('#tb2in1').dataTable({
			aaData: data,
			
			aaSorting: [[ 0, "asc" ]],
			aoColumns: [
				
				{ mDataProp: "imsi_mobileno", sTitle: 'IMSI Mobile Number' },
				{ mDataProp: "imsi_country", sTitle: 'IMSI Country'},
				{ mDataProp: "order_date", sTitle: 'Order Date' },
				{ mDataProp: "order_status", sTitle: 'Order Status' },
				{ mDataProp: "process_date", sTitle: 'Process Date' },
				{
					mDataProp: 'imsi', sTitle: 'Action', bSortable: false,
					fnRender: function (ob) {
						var content = '<select onchange="selAction(this.value)>';
						content += '<option value="0">Select</option>'; 
						content += '</select>';

						if (ob.aData.order_status.toLowerCase() === 'open') {                        
							var content = '<select onchange="selAction(this.value)">';
							content += '<option value="0">Select</option>'; 
							content += '<option value="2_'+ob.aData.order_id+'">Activate</option>';
							content += '</select>';
						}

						return content;
					}
				}
				
			]
		})
		$('#loader').hide();
		$('#tb2in1').show();
	})
}

function selAction(ini){
	var x = ini.split('_');
	var type = x[0], order_id = x[1];	
	if(type == 2){
		$('#orderId').val(order_id);
		$('#modalActivation').click();
	}
}

function CustomerSearch(par){
	var url = 'http://192.168.1.230:1830/api/customersearch/'+par;
	
	$.ajax 
	({
		url: url,
		type : 'GET',
		dataType: 'json',
		cache:false,
		success: function (data){
			$('#mobileNo').text(data.MobileNo);
			$('#fullName').text(data.FirstName+' '+data.LastName);
			$('#email').text(data.Email);
			$('#iccid').text(data.ICCID);
			$('#serialNo').text(data.SerialNumber);
			$('#status').text(data.Status);
			$('#action').append('<a href="#'+data.MobileNo+'" class="btn">Select Customer</a>');
		}
	});
}

function addNewCountry(d1,d2,d3,d4,d5,d6,d7,d8,d9){
	var url = 'http://192.168.1.230:1830/api/customer2in1/';
	
	 jQuery.support.cors = true;
		var deliveryAddress = {
			mobileno:'447451931090',
			imsi_country:'Holland',
            imsi_mobileno:'447574751175',
            order_date:'2013-07-17 T20:37:56.07',
            order_status:'OPEN',
            process_date:'0001-01-01T00:00:00',
            imsi_location:'',
            imsi:'',
            first_name:d1,
            last_name:d2,
            address2:d4,
            address1:d3,
            town:d5,
            postcode:d6,
            country:d7,
            email:d8,
            contact:d9,
            can_be_activated:'1'
		};  
			
		$.ajax 
		({
			url: url,
			type : 'POST',
			data:JSON.stringify(deliveryAddress),
			dataType: 'json',
			contentType: "application/json;charset=utf-8",	
			cache:false,
			success: function  (data) {
				var stat = data.errcode;
				if(stat == 0){
					// alert('shop added');
					window.location = 'SearchCustomerResult.html';
				}
				else{
					// alert(data.Errmsg);
				}
			}		
		});	
}

function activate(d1){
	var url = 'http://192.168.1.230:1830/api/customer2in1activate/';
	
	 jQuery.support.cors = true;
		var deliveryAddress = {
			order_id:d1,
            imsi_mobileno:'447574751175'
		};  
			
		$.ajax 
		({
			url: url,
			type : 'post',
			data:JSON.stringify(deliveryAddress),
			dataType: 'json',
			contentType: "application/json;charset=utf-8",	
			cache:false,
			success: function  (data) {
				var stat = data.errcode;
				if(stat == 0){
					alert('activated');
					// window.location = 'SearchCustomerResult.html';
				}
				else{
					// alert(data.Errmsg);
				}
			}		
		});	
}