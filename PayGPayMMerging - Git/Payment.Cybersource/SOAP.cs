﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using CyberSource.Clients;
using CyberSource.Clients.SoapWebReference;
using System.Web.Services.Protocols;

namespace Payment.Cybersource
{
    public class SOAP
    {
        private Configuration _config;
        public SOAP() { _config = null; }
        public SOAP(Configuration fileConfig) 
        {
            _config = fileConfig;
        }

        /// <summary>
        /// Authorize a new card. It will not automatically charged
        /// </summary>
        /// <param name="refId"></param>
        /// <param name="defineData"></param>
        /// <param name="card"></param>
        /// <param name="billTo"></param>
        /// <param name="purchaseTotals"></param>
        /// <param name="checkEnroll"></param>
        /// <param name="decisionManager"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public ReplyMessage Card_Authorize(string refId, string defineData, Card card, BillTo billTo, PurchaseTotals purchaseTotals, bool checkEnroll, bool decisionManager, string merchantId)
        {
            RequestMessage request = new RequestMessage();

            // Merchant reference ID
            request.merchantReferenceCode = refId;

            // Merchant define data
            MerchantDefinedData mdd = new MerchantDefinedData();
            mdd.field1 = defineData;
            request.merchantDefinedData = mdd;

            // Payment information
            request.card = card;
            request.billTo = billTo;
            request.purchaseTotals = purchaseTotals;

            // Authorization service
            request.ccAuthService = new CCAuthService();
            request.ccAuthService.run = "true";

            // Business rules
            request.businessRules = new BusinessRules();
            request.businessRules.ignoreAVSResult = "false";
            request.businessRules.ignoreCVResult = "false";

            // Enrollment service
            if (checkEnroll)
            {
                request.payerAuthEnrollService = new PayerAuthEnrollService();
                request.payerAuthEnrollService.run = "true";
            }

            // Decision manager
            if (decisionManager)
            {
                request.decisionManager = new DecisionManager();
                request.decisionManager.enabled = "true";
            }

            // Return transaction result
            ReplyMessage reply = CallCyberSource(request, merchantId);
            reply.merchantReferenceCode = string.IsNullOrEmpty(reply.merchantReferenceCode) ? refId : reply.merchantReferenceCode;
            return reply;
        }

        /// <summary>
        /// Create new subscription 
        /// </summary>
        /// <param name="refId"></param>
        /// <param name="defineData"></param>
        /// <param name="purchaseTotals"></param>
        /// <param name="authRequestId"></param>
        /// <param name="authRequestToken"></param>
        /// <param name="decisionManager"></param>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public ReplyMessage Subcription_Create(string refId, string defineData, PurchaseTotals purchaseTotals, string authRequestId, string authRequestToken, bool decisionManager, string merchantId)
        {
            RequestMessage request = new RequestMessage();

            // Merchant reference ID
            request.merchantReferenceCode = refId;

            // Merchant define data
            MerchantDefinedData mdd = new MerchantDefinedData();
            mdd.field1 = defineData;
            request.merchantDefinedData = mdd;

            // Payment information
            request.purchaseTotals = purchaseTotals;

            // Recurring information
            request.recurringSubscriptionInfo = new RecurringSubscriptionInfo();
            request.recurringSubscriptionInfo.frequency = "on-demand";

            // Create subscription service
            request.paySubscriptionCreateService = new PaySubscriptionCreateService();
            request.paySubscriptionCreateService.paymentRequestID = authRequestId;
            request.paySubscriptionCreateService.paymentRequestToken = authRequestToken;
            request.paySubscriptionCreateService.run = "true";

            // Decision manager
            if (decisionManager)
            {
                request.decisionManager = new DecisionManager();
                request.decisionManager.enabled = "true";
            }

            // Transaction result
            ReplyMessage reply = CallCyberSource(request, merchantId);
            reply.merchantReferenceCode = string.IsNullOrEmpty(reply.merchantReferenceCode) ? refId : reply.merchantReferenceCode;
            return reply;
        }

        /// <summary>
        /// Check the authorization of credit card subscription.
        /// </summary>
        /// <param name="refId">Payment reference number</param>
        /// <param name="merchantId">Merchant ID (uk01login)</param>
        /// <param name="subscriptionId">Subscription ID</param>
        /// <param name="defineData">Merchant define data, can be mobileno or other information</param>
        /// <param name="purchaseTotals">Total payment amount</param>
        /// <param name="useDecisionManager">Use decision manager configuration</param>
        /// <param name="resultDescription">Transaction result description</param>
        /// <returns>Detail transaction result</returns>
        public ReplyMessage Subscription_Authorize(
            string refId,
            string merchantId,
            string subscriptionId,
            MerchantDefinedData defineData,
            PurchaseTotals purchaseTotals,
            bool useDecisionManager,
            ref string resultDescription)
        {
            RequestMessage request = new RequestMessage();

            // Merchant reference ID
            request.merchantReferenceCode = refId;

            // merchant define data
            request.merchantDefinedData = defineData;

            // Subscription data
            request.recurringSubscriptionInfo = new RecurringSubscriptionInfo();
            request.recurringSubscriptionInfo.subscriptionID = subscriptionId;

            // Authorize subscription
            request.ccAuthService = new CCAuthService();
            request.ccAuthService.run = "true";

            // Payment information
            request.purchaseTotals = purchaseTotals;

            // Use decision manager
            if (useDecisionManager)
            {
                request.decisionManager = new DecisionManager();
                request.decisionManager.enabled = "true";
            }

            // Return transaction result
            ReplyMessage reply = CallCyberSource(request, merchantId);
            resultDescription = ProcessReply(reply);
            return reply;
        }

        /// <summary>
        /// Capture/deduct the customer bank account.
        /// </summary>
        /// <param name="refId">Payment reference number</param>
        /// <param name="merchantId">Merchant ID (uk01login)</param>
        /// <param name="authRequestId">Request ID from previous Subscription_Authorize</param>
        /// <param name="authRequestToken">Request token from previous Subscription_Authorize</param>
        /// <param name="defineData">Merchant define data</param>
        /// <param name="purchaseTotals">Total payment amount</param>
        /// <param name="useDecisionManager">Use decision manager configuration</param>
        /// <param name="resultDescription">Transaction result description</param>
        /// <returns>Detail transaction result</returns>
        public ReplyMessage Subscription_Capture(
            string refId,
            string merchantId,
            string authRequestId,
            string authRequestToken,
            MerchantDefinedData defineData,
            PurchaseTotals purchaseTotals,
            bool useDecisionManager,
            ref string resultDescription)
        {
            RequestMessage request = new RequestMessage();

            // Merchant reference ID
            request.merchantReferenceCode = refId;

            // Capture service
            request.ccCaptureService = new CCCaptureService();
            request.ccCaptureService.authRequestID = authRequestId;
            request.ccCaptureService.authRequestToken = authRequestToken;
            request.ccCaptureService.run = "true";

            // merchant define data
            request.merchantDefinedData = defineData;

            // Payment information
            request.purchaseTotals = purchaseTotals;

            // Use decision manager
            if (useDecisionManager)
            {
                request.decisionManager = new DecisionManager();
                request.decisionManager.enabled = "true";
            }

            // Return transaction result
            ReplyMessage reply = CallCyberSource(request, merchantId);
            resultDescription = ProcessReply(reply);
            return reply;
        }

        #region Cyber Source transaction methods
        private ReplyMessage CallCyberSource(RequestMessage request, string merchantId)
        {
            // we will let the client pick up the merchantID from the config file.
            // In multi-merchant scenarios, you would set a merchantID in each request.
            request.merchantID = merchantId.Trim();

            // run transaction
            string errDesc = string.Empty;
            try
            {

                ReplyMessage reply = SoapClient.RunTransaction(request);

                reply.requestID = reply.requestID ?? "";
                reply.requestToken = reply.requestToken ?? "";

                return reply;
            }

            // transaction failed
            catch (SignException se) { errDesc = HandleSignException(se); }
            catch (SoapHeaderException she) { errDesc = HandleSoapHeaderException(she); }
            catch (SoapBodyException sbe) { errDesc = HandleSoapBodyException(sbe); }
            catch (WebException we) { errDesc = HandleWebException(we); }

            // throw exception when transaction failed
            if (!errDesc.Equals(string.Empty)) throw new Exception(errDesc);
            else throw new Exception("Unknown error!");
        }

        private void SaveOrderState()
        {
            /*
             * This is where you store the order state in your system for
             * post-transaction analysis.  Information to store include the
             * invoice, the values of the reply fields, or the details of the
             * exception that occurred, if any.
             */
        }

        private string ProcessReply(ReplyMessage reply)
        {
            string template = GetTemplate(reply.decision.ToUpper());
            string content = GetContent(reply);

            // Special condition
            if (reply.reasonCode.Equals("475"))
                template = template.Replace("Your order was not approved.", "").Trim();

            /*
             * Display result of transaction.  Being a console application,
             * this sample simply prints out some text on the screen.  Use
             * what is appropriate for your system (e.g. ASP.NET pages).
             */
            return string.Format(template, content);
        }

        private string GetTemplate(string decision)
        {
            /*
             * This is where you retrieve the HTML template that corresponds
             * to the decision.  This template has 'boiler-plate' wording and
             * can be stored in files or a database.  This is just one way to
             * retrieve feedback pages.  Use what is appropriate for your
             * system (e.g. ASP.NET pages).
             */

            if ("ACCEPT".Equals(decision))
            {
                return ("The transaction succeeded. {0}");
            }

            if ("REJECT".Equals(decision))
            {
                return ("Your order was not approved. {0}");
            }

            // ERROR
            return "Your order could not be completed at this time. {0}";
        }

        private string GetContent(ReplyMessage reply)
        {
            /*
             * This is where you retrieve the content that will be plugged
             * into the template.
             * 
             * The strings returned in this sample are mostly to demonstrate
             * how to retrieve the reply fields.  Your application should
             * display user-friendly messages.
             */

            int reasonCode = int.Parse(reply.reasonCode);
            switch (reasonCode)
            {
                // Success
                case 100:
                    return ("");

                // Missing field(s)
                case 101:
                    return string.Format("One or more fields in the request are missing{0}", EnumerateValues(reply.missingField));

                // Invalid field(s)
                case 102:
                    return string.Format("One or more fields in the request are invalid{0}", EnumerateValues(reply.invalidField));

                // Insufficient funds
                case 204:
                    return "Insufficient funds in the account. Please use a different card or select another form of payment.";

                // The customer is enrolled in Payer Authentication
                case 475:
                    return "The cardholder is enrolled in Payer Authentication. Please authenticate before proceeding with authorization.";

                // add additional reason codes here that you need to handle specifically.
                default:
                    // For all other reason codes, return an empty string,
                    // in which case, the template will be displayed with no
                    // specific content.
                    return (String.Empty);
            }
        }

        private string HandleSignException(SignException se)
        {
            string template = GetTemplate("ERROR");

            /*
             * The string returned in this sample is mostly to demonstrate
             * how to retrieve the exception properties.  Your application
             * should display user-friendly messages.
             */
            string content = String.Format("Failed to sign the request with error code '{0}' and " + "message '{1}'.", se.ErrorCode, se.Message);

            return string.Format(template, content);
        }

        private string HandleSoapHeaderException(SoapHeaderException she)
        {
            string template = GetTemplate("ERROR");

            /*
             * The string returned in this sample is mostly to demonstrate
             * how to retrieve the exception properties.  Your application
             * should display user-friendly messages.
             */
            string content = String.Format("A SOAP header exception was returned with fault code '{0}' and message '{1}'.", she.Code, she.Message);

            return string.Format(template, content);
        }

        private string HandleSoapBodyException(SoapBodyException sbe)
        {
            string template = GetTemplate("ERROR");

            /*
             * The string returned in this sample is mostly to demonstrate
             * how to retrieve the exception properties.  Your application
             * should display user-friendly messages.
             */
            string content = String.Format("A SOAP body exception was returned with fault code '{0}' and message '{1}'.", sbe.Code, sbe.Message);

            string result = string.Format(template, content);

            if (sbe.Code.Namespace.Equals(SoapClient.CYBS_NAMESPACE) &&
                sbe.Code.Name.Equals("CriticalServerError"))
            {
                /* The transaction may have been completed by CyberSource.
                 * If your request included a payment service, you should
                 * notify the appropriate department in your company (e.g. by
                 * sending an email) so that they can confirm if the request
                 * did in fact complete by searching the CyberSource Support
                 * Screens using the request id.
                 * 
                 * The line below demonstrates how to retrieve the request id.
                 */

                result += string.Format("Critical server error for Request ID: {0}.", sbe.RequestID);
            }

            return result;
        }

        private string HandleWebException(WebException we)
        {
            string template = GetTemplate("ERROR");

            /*
             * The string returned in this sample is mostly to demonstrate
             * how to retrieve the exception properties.  Your application
             * should display user-friendly messages.
             */
            string content = String.Format("Failed to get a response with status '{0}' and message '{1}'.", we.Status, we.Message);

            string result = string.Format(template, content);

            if (IsCriticalError(we))
            {
                /*
                 * The transaction may have been completed by CyberSource.
                 * If your request included a payment service, you should
                 * notify the appropriate department in your company (e.g. by
                 * sending an email) so that they can confirm if the request
                 * did in fact complete by searching the CyberSource Support
                 * Screens using the value of the merchantReferenceCode in
                 * your request.
                 */
                result += "Critical server error.";
            }

            return result;
        }

        private string EnumerateValues(string[] array)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            if (array != null)
            {
                sb.Append(" (");
                foreach (string val in array)
                    sb.Append(val + ", ");
                sb.Remove(sb.Length - 2, 2).Append(")");
            }
            sb.Append(".");

            return (sb.ToString());
        }

        private bool IsCriticalError(WebException we)
        {
            switch (we.Status)
            {
                case WebExceptionStatus.ProtocolError:
                    if (we.Response != null)
                    {
                        HttpWebResponse response
                            = (HttpWebResponse)we.Response;

                        // GatewayTimeout may be returned if you are
                        // connecting through a proxy server.
                        return (response.StatusCode ==
                            HttpStatusCode.GatewayTimeout);

                    }

                    // In case of ProtocolError, the Response property
                    // should always be present.  In the unlikely case 
                    // that it is not, we assume something went wrong
                    // along the way and to be safe, treat it as a
                    // critical error.
                    return (true);

                case WebExceptionStatus.ConnectFailure:
                case WebExceptionStatus.NameResolutionFailure:
                case WebExceptionStatus.ProxyNameResolutionFailure:
                case WebExceptionStatus.SendFailure:
                    return (false);

                default:
                    return (true);
            }
        }
        #endregion
    }
}
