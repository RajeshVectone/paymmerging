﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Payment.Cybersource
{
    public class RoutingPayment
    {
        public int ID {get;set;}
        public string PROVIDER_CODE	{get;set;}
        public string SITECODE {get;set;}
        public string PRODUCT_CODE_PREFIX {get;set;}
        public string MERCHANT_ID {get;set;}
    }
}
