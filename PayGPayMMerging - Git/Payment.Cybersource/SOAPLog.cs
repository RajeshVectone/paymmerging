﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using NLog;

namespace Payment.Cybersource
{
    public class SOAPLog
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public static int TransactionPayment(string sitecode, string productcode, string paymentagent, string servicetype,
            string paymentmode, int paymentstep, int paymentstatus, string accountid,
            string last6digitccno, double totalamount, string currency,
            string subscriptionid, string merchantid, string providercode, string csreasoncode, string generalerrorcode)
        {
            try
            {
                using (var conn = new SqlConnection(Properties.Settings.Default.PaymentConnection))
                {
                    conn.Open();

                    var result = conn.Query<int>(
                            "csPaymentInsert", new
                            {
                                @sitecode = sitecode,
                                @productcode = productcode,
                                @paymentagent = paymentagent,
                                @servicetype = servicetype,
                                @paymentmode = paymentagent,
                                @paymentstep = paymentstep,
                                @paymentstatus = paymentstatus,
                                @accountid = accountid,
                                @last6digitccno = last6digitccno,
                                @totalamount = totalamount,
                                @currency = currency,
                                @subscriptionid = subscriptionid,
                                @merchantid = merchantid,
                                @providercode = providercode,
                                @csreasoncode = csreasoncode,
                                @generalerrorcode = generalerrorcode
                            }, commandType: CommandType.StoredProcedure);

                    return result.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Log.Error("Payment.Cybersource.TransactionPayment: {0}", ex.Message);
                return 0;
            }
        }

        public static RoutingPayment GetRoutingData(string sitecode, string productcode)
        {
            return null;
        }
    }
}
