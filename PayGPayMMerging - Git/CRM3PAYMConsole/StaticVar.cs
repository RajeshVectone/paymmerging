﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM3PAYMConsole
{
    public class StaticVar
    {
        public static string ErrorToEmail
        {
            get
            {
                if (String.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["ErrorToEmail"]))
                    return "dony.isnandi@mundio.com";
                else
                    return System.Configuration.ConfigurationManager.AppSettings["ErrorToEmail"];
            }
        }
    }
}
