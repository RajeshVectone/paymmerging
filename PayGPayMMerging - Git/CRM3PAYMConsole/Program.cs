﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using NLog;


namespace CRM3PAYMConsole
{
    class Program
    {
        static readonly Logger Log = LogManager.GetCurrentClassLogger();
        static bool isRun = false;
        static string ApplicationText = "CRM3 PAYM Console Application";

        static void Main(string[] args)
        {
            Program p = new Program();

            Console.WriteLine(ApplicationText);
            Console.WriteLine("(c) 2014");
            Console.WriteLine("This application will do some jobs related to CRM3 PAYM");
            Console.WriteLine("");

            do
            {
                Console.WriteLine("-------------------------------------------------------------------------------");
                Console.WriteLine("Start Processing CRM3 PAYM.");
                StringBuilder warnText = new StringBuilder();

                try
                {
                    warnText.AppendLine(p.DoPaymentQueue());
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                    // Master Error Handling
                    warnText.AppendLine("-------------------------------------------------------------------------------");
                    warnText.AppendLine("Exception Error");
                    warnText.AppendLine(ex.Message);
                }

                Console.WriteLine("");
                Console.WriteLine(warnText.ToString());
                Console.WriteLine("");
                Console.WriteLine("Waiting for next interval..");
                System.Threading.Thread.Sleep(60000);
            } while (isRun);


            Console.WriteLine("");
            Console.WriteLine("Shutting down...");

#if DEBUG
            Console.ReadKey();
#endif
        }

        static void SendWarningEmail(string errBodyEmail)
        {
            try
            {
                if (errBodyEmail.Length > 0 || errBodyEmail == Environment.NewLine)
                {
                    MailMessage message = new MailMessage();
                    message.From = new MailAddress("noreply@vectone.com");
                    message.To.Add(new MailAddress(StaticVar.ErrorToEmail));
                    message.Subject = ApplicationText;
                    message.Body = errBodyEmail.ToString();

                    SmtpClient client = new SmtpClient();
                    client.Send(message);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("Failure in sending email: {0}", ex.Message));
            }
        }

        string DoPaymentQueue()
        {
            StringBuilder sb = new StringBuilder();

            try
            {
                var qCheck = CRM_API.DB.PAYMPayments.SProc.PaymentAutoQueueList();
                while (qCheck != null)
                {
                    Log.Debug("DoPaymentQueue", string.Format("{0},{1},{2},{3}", qCheck.idx, qCheck.pp_customer_id, qCheck.mobileno, qCheck.amount));
                    Console.WriteLine("Processing Payment Queue.");
                    string res = "";

                    CRM_API.Helpers.Payment payment = new CRM_API.Helpers.Payment();
                    var currency = CRM_API.DB.PAYMPlanBundles.SProc.GetCurrency("UK");
                    var subscriptionList = CRM_API.DB.Payment.GetSubscribtionList(qCheck.mobileno);
                    if (subscriptionList != null && subscriptionList.Count() > 0)
                    {
                        //string merchantid = string.Format("PMBO-DDPPCC{0:yyyyMMHHmmss}-{1}", DateTime.Now, qCheck.pp_customer_id);
                        //payment.CybersourceCharge(merchantid, qCheck.mobileno, "MCM", "PMBO", string.Format("CLN{0}-{0:yyMMddHHmmss}", qCheck.pp_customer_id, DateTime.Now), subscriptionList.FirstOrDefault().SubscriptionID, currency, qCheck.amount);
                        string referenceCode = string.Format("PMBO-DDPPCC{0:yyyyMMHHmmss}-{1}-RX", DateTime.Now, qCheck.pp_customer_id);
                        CRM_API.Models.Payment.CardDetails requestData = new CRM_API.Models.Payment.CardDetails();
                        payment.AuthorizeWithout3DS(requestData as CRM_API.Models.Payment.CardDetails, "MCM", qCheck.mobileno, "PMBO", "", currency, qCheck.amount, qCheck.mobileno, subscriptionList.FirstOrDefault().SubscriptionID, referenceCode,"VMUK");
                        if (payment.Decision == "ACCEPT" || payment.Decision == "APPROVAL")
                        {
                            Log.Debug("DoPaymentQueue: Payment Success");
                           // CRM_API.DB.PAYMPayments.SProc.PaymentAutoQueueUpdate(qCheck.idx, payment.MerchantReferenceCode, 1, "CRM3PAYMConsole");
                            //CRM_API.DB.PAYMPayments.SProc.PaymentInsert_ForPaymentQueued(DateTime.Now, qCheck.mobileno, merchantid, "credit or debit card", qCheck.amount, 1, "CRM3PAYMConsole");
                            CRM_API.DB.PAYMPayments.SProc.PaymentInsert_ForPaymentQueued(DateTime.Now, qCheck.mobileno, referenceCode, "credit or debit card", qCheck.amount, 1, "CRM3PAYMConsole");
                        }
                        else
                        {
                            Log.Debug("DoPaymentQueue: Payment Declined");
                            //sb.AppendLine(string.Format("{0},{1},{2}, Declined", qCheck.idx, qCheck.mobileno, qCheck.amount));
                            //CRM_API.DB.PAYMPayments.SProc.PaymentAutoQueueUpdate(qCheck.idx, payment.MerchantReferenceCode, 3, "CRM3PAYMConsole");
                            //CRM_API.DB.PAYMPayments.SProc.PaymentInsert_ForPaymentQueued(DateTime.Now, qCheck.mobileno, merchantid, "credit or debit card", qCheck.amount, 3, "CRM3PAYMConsole");
                            CRM_API.DB.PAYMPayments.SProc.PaymentInsert_ForPaymentQueued(DateTime.Now, qCheck.mobileno, referenceCode, "credit or debit card", qCheck.amount, 3, "CRM3PAYMConsole");
                        }
                        CRM_API.DB.PAYMPayments.SProc.ResetQueuedPayments(qCheck.pay_reference, qCheck.pp_customer_id);
                    }
                    else
                    {
                        res = "No valid subscription found";
                    }

                    Console.Write("Done: " + res);
                    Log.Debug("DoPaymentQueue", string.Format("Done: {0}", res));

                    qCheck = CRM_API.DB.PAYMPayments.SProc.PaymentAutoQueueList();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                Log.Debug("DoPaymentQueue", "Error: " + ex.Message);

                sb.AppendLine("-------------------------------------------------------------------------------");
                sb.AppendLine("Payment Queue Error Message");
                sb.AppendLine(ex.Message);
            }

            return sb.ToString();
        }
    }
}
