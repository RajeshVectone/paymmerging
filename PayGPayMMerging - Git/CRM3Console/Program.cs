﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using NLog;

namespace CRM3Console
{
    class Program
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        private readonly string ApplicationText = "CRM3 Console Application";
        private StringBuilder errBodyEmail = new StringBuilder();
        private const double charge2in1 = 5.00;

        static void Main(string[] args)
        {
            Program p = new Program();

            Console.WriteLine(p.ApplicationText);
            Console.WriteLine("(c) 2013");
            Console.WriteLine("This application will do some jobs related to CRM3");
            Console.WriteLine("");

            try
            {
                //p.Test();
                p.Do2in1Expired();
                p.DoCSBExpired();
            }
            catch (Exception ex)
            {
                // Master Error Handling
                p.errBodyEmail.AppendLine(ex.Message);
            }

            Console.WriteLine("");
            Console.WriteLine("Shutting down...");
            p.SendWarningEmail();

#if DEBUG
            Console.ReadKey();
#endif
        }

        void SendWarningEmail()
        {
            try
            {
                if (errBodyEmail.Length > 0 || errBodyEmail.ToString() == Environment.NewLine)
                {
                    MailMessage message = new MailMessage();
                    message.From = new MailAddress("noreply@vectone.com");
                    message.To.Add(new MailAddress(StaticVar.ErrorToEmail));
                    message.Subject = ApplicationText;
                    message.Body = errBodyEmail.ToString();

                    SmtpClient client = new SmtpClient();
                    client.Send(message);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("Failure in sending email: {0}", ex.Message));
            }
        }

        void Test()
        {
            string home_country = CRM_API.DB.Country.CountrySProc.GetCountryByMSIDN("33759052501");
            Console.WriteLine(home_country);
        }

        CRM_API.DB.Common.ErrCodeMsg CancelLocation(string mobileNo)
        {
            CRM_API.DB.Common.ErrCodeMsg resCodeMsg = new CRM_API.DB.Common.ErrCodeMsg();
            string[] resUpdate = new CRM_API.Helpers.HLR().CancelLocation(mobileNo);
            int status = -1;
            if (resUpdate.Length > 0)
                int.TryParse(resUpdate[0], out status);
            if (status == -1)
            {
                resCodeMsg.errcode = -1;
                resCodeMsg.errmsg = "Failed to send Location Update";
                Log.Debug(string.Format("{0} {1}", mobileNo, resCodeMsg.errmsg));
            }
            else
            {
                resCodeMsg.errcode = 0;
                resCodeMsg.errmsg = string.Format("{0} Location Update Success", mobileNo);
                Log.Debug(resCodeMsg.errmsg);
            }
            return resCodeMsg;
        }

        void Do2in1Cancel(int orderId, string imsi_mobileNo)
        {
            Console.Write("Cancellation ");
            int needCancelLoc = 0;
            CRM_API.DB.Common.ErrCodeMsg errResult = CRM_API.DB.Customer2in1.SProc.DeActivate(orderId, "CRM3 Job", out needCancelLoc);
            Console.Write(string.Format("| Result : {0}", errResult.errmsg));
            Log.Debug(string.Format("{0} | Cancellation | Result : {1}", orderId, errResult.errmsg));

            if (needCancelLoc == 1)
            {
                if (string.IsNullOrEmpty(imsi_mobileNo))
                {
                    Console.WriteLine("No Cancel Location with Empty MobileNo ");
                    Log.Debug(string.Format("{0} | No Cancel Location with Empty MobileNo ", orderId));
                    return;
                }

                errResult = CancelLocation(imsi_mobileNo);
                Console.Write(string.Format("| Cancel Location : {0}", errResult.errmsg));
                Log.Debug(string.Format("{0} | {1} - Cancel Location | Result : {2}", orderId, imsi_mobileNo, errResult.errmsg));
            }

            Console.WriteLine("");
        }
        void Do2in1Expired()
        {
            Console.WriteLine("CRM3 2in1 Start Processing Expired");
            Log.Debug("CRM3 2in1 Start Processing Expired");

            double lastCharge = 5;

            IEnumerable<CRM_API.Models.Customer2in1Records> expiredRecords = CRM_API.DB.Customer2in1.SProc.List2in1Expired();
            foreach (CRM_API.Models.Customer2in1Records expiredRecord in expiredRecords)
            {
                Console.Write(string.Format("-- Order ID : {0} => ", expiredRecord.order_id));

                // Set an empty charge
                if (expiredRecord.monthly_charge <= 0)
                    expiredRecord.monthly_charge = lastCharge;
                else
                    lastCharge = expiredRecord.monthly_charge;

                if (string.IsNullOrEmpty(expiredRecord.imsi_mobileno))
                {
                    Console.WriteLine(" Empty IMSI Mobile Number -- Skipped ");
                    Log.Debug(string.Format("{0}:{1} | Empty IMSI Mobile Number Skipped", expiredRecord.order_id, expiredRecord.mobileno));
                }
                else
                {
                    // Check the renewal method first
                    switch (expiredRecord.renewal_method)
                    {
                        case 3: // Cancellation
                            Do2in1Cancel(expiredRecord.order_id, expiredRecord.imsi_mobileno);
                            break;
                        case 2: // Saved Card
                            Console.Write("Card Renewal ");
                            CRM_API.Helpers.Payment payment = new CRM_API.Helpers.Payment();
                            string orderIdString = expiredRecord.order_id.ToString();
                            //int.TryParse(expiredRecord.order_id, out orderIdString);
                            payment.SubscriptionCharge(expiredRecord.mobileno, expiredRecord.card_verf_code, expiredRecord.currency, expiredRecord.monthly_charge); //.CybersourceCharge(orderIdString, expiredRecord.mobileno, expiredRecord.SubscriptionID, expiredRecord.currency, expiredRecord.monthly_charge);
                            if (payment.Decision != "ACCEPT")
                            {
                                Console.WriteLine(string.Format("| Result : {0}", payment.Decision));
                                Log.Debug(string.Format("{0}:{1} | Card Renewal | Result : {2}", expiredRecord.order_id, expiredRecord.mobileno, payment.Decision));

                                // If it failed to renew, cancel it
                                Do2in1Cancel(expiredRecord.order_id, expiredRecord.imsi_mobileno);
                            }
                            else
                            {
                                Console.Write(string.Format("| Payment Ref : {0}", payment.MerchantReferenceCode));
                                CRM_API.DB.Common.ErrCodeMsg err2 = CRM_API.DB.Customer2in1.SProc.Renew2in1Record(expiredRecord.order_id, "CRM3 Job");
                                if (err2.errcode == 0)
                                {
                                    Console.WriteLine(string.Format("| Result : {0}", payment.MerchantReferenceCode));
                                }
                                else
                                {
                                    Console.WriteLine(string.Format("| Result : {0} | Failed to renew {1}", payment.MerchantReferenceCode, err2.errmsg));
                                }
                                Log.Debug(string.Format("{0}:{1} | Card Renewal | Payment Ref : {2} | Result : {3} | Renew Result : {4}", expiredRecord.order_id, expiredRecord.mobileno, payment.MerchantReferenceCode, payment.Decision), err2.errmsg);
                            }
                            break;
                        case 1: // Balance
                        default:
                            Console.Write("Balance Renewal ");
                            CRM_API.DB.Common.ErrCodeMsg err1 = CRM_API.DB.Customer2in1.SProc.Renew2in1RecordByCredit(expiredRecord.order_id, "CRM3 Job", expiredRecord.monthly_charge);
                            Console.WriteLine(string.Format("| Result : {0}", err1.errmsg));
                            Log.Debug(string.Format("{0}:{1} | Balance Renewal | Result : {2}", expiredRecord.order_id, expiredRecord.mobileno, err1.errmsg));

                            // If it failed to renew, cancel it
                            if (err1.errcode < 0)
                            {
                                Do2in1Cancel(expiredRecord.order_id, expiredRecord.imsi_mobileno);
                            }
                            break;
                    }
                }
            }
            Console.WriteLine("End Processing Expired");
            Console.WriteLine("----------------------");
        }
        void DoCSBCancel(string mobileNo, string destNo, int action = 0)
        {
            if (string.IsNullOrEmpty(mobileNo))
            {
                Console.WriteLine("No Cancellation with Empty MobileNo ");
                Log.Debug("No Cancellation with Empty MobileNo ");
                return;
            }

            Console.Write("Cancellation ");
            int needCancelLoc = 0;
            CRM_API.DB.Common.ErrCodeMsg errResult = CRM_API.DB.CustomerCSB.SProc.Cancel(mobileNo,destNo,action);
            Console.Write(string.Format("| Result : {0}", errResult.errmsg));
            Log.Debug(string.Format("Cancellation | Result : {0}", errResult.errmsg));

            if (needCancelLoc == 1)
            {
                errResult = CancelLocation(mobileNo);
                Console.Write(string.Format("| Cancel Location : {0}", errResult.errmsg));
                Log.Debug(string.Format("Cancel Location | Result : {0}", errResult.errmsg));
            }

            Console.WriteLine("");
        }
        void DoCSBRenew(string mobileNo, string destNo, int action = 1)
        {
            if (string.IsNullOrEmpty(mobileNo))
            {
                Console.WriteLine("No Renew by Balance with Empty MobileNo ");
                Log.Debug("No Renew by Balance with Empty MobileNo ");
                return;
            }

            Console.Write("Renewal By Balance");
            int needCancelLoc = 0;
            CRM_API.DB.Common.ErrCodeMsg errResult = CRM_API.DB.CustomerCSB.SProc.Renew(mobileNo, destNo, action);
            Console.Write(string.Format("| Result : {0}", errResult.errmsg));
            Log.Debug(string.Format("Renew by Balance| Result : {0}", errResult.errmsg));

            if (needCancelLoc == 1)
            {
                errResult = CancelLocation(mobileNo);
                Console.Write(string.Format("| Renew by Balance Location : {0}", errResult.errmsg));
                Log.Debug(string.Format("Renew by Balance Location | Result : {0}", errResult.errmsg));
            }

            Console.WriteLine("");
        }
        void DoCSBExpired()
        {
            Console.WriteLine("CRM3 Country Saver Start Processing Expired");
            Log.Debug("CRM3 Country Saver Start Processing Expired");

            IEnumerable<CRM_API.Models.CustomerCSBRecords> expiredRecords = CRM_API.DB.CustomerCSB.SProc.ListCSBExpired();
            foreach (CRM_API.Models.CustomerCSBRecords expiredRecord in expiredRecords)
            {
                Console.Write(string.Format("-- Order ID : {0} => ", expiredRecord.order_id));
                switch (expiredRecord.renewal_method.ToLower())
                {
                    case "balance": // Balance
                        Console.Write("Balance Renewal ");
                        CRM_API.Models.CustomerCSBHistory errResult = CRM_API.DB.CustomerCSB.SProc.BalanceCharge(expiredRecord.order_id).FirstOrDefault();
                        Console.WriteLine(string.Format("| Result : {0}", errResult.errmsg));
                        Log.Debug(string.Format("Balance Renewal | Result : {0}", errResult.errmsg));

                        // If it failed to renew, cancel it
                        if (errResult.errcode < 0)
                        {
                            DoCSBCancel(expiredRecord.mundio_cli, expiredRecord.dest_msisdn);
                        }
                        else
                            DoCSBRenew(expiredRecord.mundio_cli, expiredRecord.dest_msisdn);
                        break;
                    case "card":
                        break;
                    //case 2: // Saved Card
                    //    Console.Write("Card Renewal ");
                    //    CRM_API.Helpers.Payment payment = new CRM_API.Helpers.Payment();
                    //    string orderIdString = expiredRecord.order_id.ToString();
                    //    //int.TryParse(expiredRecord.order_id, out orderIdString);
                    //    payment.CybersourceCharge(orderIdString, expiredRecord.SubscriptionID, expiredRecord.monthly_charge);
                    //    if (payment.Decision != "ACCEPT")
                    //    {
                    //        Console.WriteLine(string.Format("| Result : {0}", payment.Decision));
                    //        Log.Debug(string.Format("Card Renewal | Result : {0}", payment.Decision));

                    //        // If it failed to renew, cancel it
                    //        Do2in1Cancel(expiredRecord.order_id, expiredRecord.imsi_mobileno);
                    //    }
                    //    else
                    //    {
                    //        CRM_API.DB.Payment.InsertLog("MMI", "AUT050", "CRM3", "MVNO", "SUB", 7, 0, expiredRecord.mobileno, string.Empty,
                    //            expiredRecord.monthly_charge, "GBP", expiredRecord.SubscriptionID, "uk01login", "CS", "", "");
                    //        Console.Write(string.Format("| Payment Ref : {0}", payment.MerchantReferenceCode));
                    //        errResult = CRM_API.DB.Customer2in1.SProc.Renew2in1Record(expiredRecord.order_id, "CRM3 Job");
                    //        Console.WriteLine(string.Format("| Result : {0}", payment.MerchantReferenceCode));
                    //        Log.Debug(string.Format("Card Renewal | Payment Ref : {0} | Result : {1}", payment.MerchantReferenceCode, payment.Decision));
                    //    }
                    //    break;
                    case "3": // Cancellation
                        //Do2in1Cancel(expiredRecord.order_id, expiredRecord.imsi_mobileno);
                        DoCSBCancel(expiredRecord.mundio_cli, expiredRecord.dest_msisdn);
                        break;
                }
                
            }
            Console.WriteLine("End Processing Expired");
            Console.WriteLine("----------------------");
        }
    }
}
